<select id='page_item' name="rows" data-url="{{$item_per_page_url}}" class="form-control">
  <option value="10" {{$item_per_page == 10?'selected':''}}>10</option>
  <option value="25" {{$item_per_page == 25?'selected':''}}>25</option>
  <option value="50" {{$item_per_page == 50?'selected':''}}>50</option>
  <option value="100" {{$item_per_page == 100?'selected':''}}>100</option>
</select> 
        
