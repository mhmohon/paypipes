@if(Session::has('toastr_alert'))
    <div id="toast-container" class="toast-bottom-right" role="alert" aria-live="polite">
		<div class="alert toast 
			{!! Session::has('info') ? 'toast-info' : '' !!}
			{!! Session::has('success') ? 'toast-success' : '' !!}
			{!! Session::has('error') ? 'toast-error' : '' !!}">
			<button class="close toast-close-button" type="button" data-dismiss="alert" aria-hidden="true">&times;</button>
			<div class="toast-title"></div>
			<div class="toast-message">{!! Session::get('toastr_alert') !!}</div>
		</div>
    </div>
@endif