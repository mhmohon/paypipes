<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner">&copy; <?php echo date("Y") ?> {{ config('global.company_legal') }} All rights reserved.</div>
	<div class="scroll-to-top">
			<i class="icon-arrow-up"></i>
	</div>
</div>
<!-- END FOOTER -->

