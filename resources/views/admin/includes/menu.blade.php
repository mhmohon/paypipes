            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 10px">
                        <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
                        <li class="sidebar-toggler-wrapper hide">
                            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
                            <div class="sidebar-toggler"> </div>
                            <!-- END SIDEBAR TOGGLER BUTTON -->
                        </li>
                        <li class="nav-item start {{ Request::is('admin') ? 'active' : '' }}">
                            <a href="/admin" class="nav-link nav-toggle">
                                <i class="icon-speedometer"></i>
                                <span class="title">Dashboard</span>
                                <span class="{{ Request::is('admin') ? 'selected' : '' }}"></span>
                            </a>
                        </li>
                        <li class="nav-item
                            {{ Request::is('admin/accounts') ? 'active' : '' }}
                            {{ Request::is('admin/accounts/*') ? 'active' : '' }}">
                            <a href="/admin/accounts" class="nav-link nav-toggle">
                                <i class="icon-rocket"></i>
                                <span class="title">Accounts</span>
                                <span class="
                                    {{ Request::is('admin/accounts') ? 'selected' : '' }}
                                    {{ Request::is('admin/accounts/*') ? 'selected' : '' }}">
                                </span>
                            </a>
                        </li>
                        <li class="nav-item
                            {{ Request::is('admin/payments') ? 'active' : '' }}
                            {{ Request::is('admin/payments/*') ? 'active' : '' }}">
                            <a href="/admin/payments" class="nav-link nav-toggle">
                                <i class="fa-credit-card"></i>
                                <span class="title">Payments</span>
                                <span class="
                                    {{ Request::is('admin/payments') ? 'selected' : '' }}
                                    {{ Request::is('admin/payments/*') ? 'selected' : '' }}">
                                </span>
                            </a>
                        </li>

                        <li class="nav-item
                            {{ Request::is('admin/wallets') ? 'active' : '' }}
                            {{ Request::is('admin/wallets/*') ? 'active' : '' }}
                            {{ Request::is('admin/walletcompany') ? 'active' : '' }}
                            {{ Request::is('admin/wallettypes') ? 'active' : '' }}
                            {{ Request::is('admin/wallettypes/*') ? 'active' : '' }}
                            {{ Request::is('admin/walletfees') ? 'active' : '' }}
                            {{ Request::is('admin/walletfees/*') ? 'active' : '' }}
                            {{ Request::is('admin/walletlimits') ? 'active' : '' }}
                            {{ Request::is('admin/walletlimits/*') ? 'active' : '' }}
                            {{ Request::is('admin/walletsstatistics') ? 'active' : '' }}">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-wallet"></i>
                                <span class="title">+Wallet Management</span>
                                <span class="
                                    {{ Request::is('admin/wallets') ? 'selected' : '' }}
                                    {{ Request::is('admin/wallets/*') ? 'selected' : '' }}
                                    {{ Request::is('admin/walletcompany') ? 'selected' : '' }}
                                    {{ Request::is('admin/wallettypes') ? 'selected' : '' }}
                                    {{ Request::is('admin/wallettypes/*') ? 'selected' : '' }}
                                    {{ Request::is('admin/walletfees') ? 'selected' : '' }}
                                    {{ Request::is('admin/walletfees/*') ? 'selected' : '' }}
                                    {{ Request::is('admin/walletlimits') ? 'selected' : '' }}
                                    {{ Request::is('admin/walletlimits/*') ? 'selected' : '' }}
                                    {{ Request::is('admin/walletsstatistics') ? 'selected' : '' }}">
                                </span>
                                <span class="arrow
                                    {{ Request::is('admin/wallets') ? 'open' : '' }}
                                    {{ Request::is('admin/wallets/*') ? 'open' : '' }}
                                    {{ Request::is('admin/walletcompany') ? 'open' : '' }}
                                    {{ Request::is('admin/wallettypes') ? 'open' : '' }}
                                    {{ Request::is('admin/wallettypes/*') ? 'open' : '' }}
                                    {{ Request::is('admin/walletfees') ? 'open' : '' }}
                                    {{ Request::is('admin/walletfees/*') ? 'open' : '' }}
                                    {{ Request::is('admin/walletlimits') ? 'open' : '' }}
                                    {{ Request::is('admin/walletlimits/*') ? 'open' : '' }}
                                    {{ Request::is('admin/walletsstatistics') ? 'open' : '' }}">
                                </span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item
                                    {{ Request::is('admin/wallets') ? 'active' : '' }}
                                    {{ Request::is('admin/wallets/*') ? 'active' : '' }}">
                                    <a href="/admin/wallets" class="nav-link ">
                                        <span class="title">+Wallets</span>
                                    </a>
                                </li>
                                <li class="nav-item
                                    {{ Request::is('admin/wallettypes') ? 'active' : '' }}
                                    {{ Request::is('admin/wallettypes/*') ? 'active' : '' }}">
                                    <a href="/admin/wallettypes" class="nav-link ">
                                        <span class="title">+Wallet Types</span>
                                    </a>
                                </li>
                                <li class="nav-item
                                    {{ Request::is('admin/walletfees') ? 'active' : '' }}
                                    {{ Request::is('admin/walletfees/*') ? 'active' : '' }}">
                                    <a href="/admin/walletfees" class="nav-link ">
                                        <span class="title">+Wallet Fees</span>
                                    </a>
                                </li>
                                <li class="nav-item
                                    {{ Request::is('admin/walletlimits') ? 'active' : '' }}
                                    {{ Request::is('admin/walletlimits/*') ? 'active' : '' }}">
                                    <a href="/admin/walletlimits" class="nav-link ">
                                        <span class="title">+Wallet Limits</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item
                            {{ Request::is('admin/paymentmethods') ? 'active' : '' }}
                            {{ Request::is('admin/paymentmethods/*') ? 'active' : '' }}">
                            <a href="/admin/paymentmethods" class="nav-link nav-toggle">
                                <i class="fa fa-plug"></i>
                                <span class="title">Gateways</span>
                                <span class="
                                    {{ Request::is('admin/paymentmethods') ? 'selected' : '' }}
                                    {{ Request::is('admin/paymentmethods/*') ? 'selected' : '' }}">
                                </span>
                            </a>
                        </li>


                        <li class="nav-item
                            {{ Request::is('admin/invoices') ? 'active' : '' }}
                            {{ Request::is('admin/invoices/*') ? 'active' : '' }}
                            {{ Request::is('admin/proformainvoices') ? 'active' : '' }}
                            {{ Request::is('admin/proformainvoices/*') ? 'active' : '' }}">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa icon-paper-clip"></i>
                                <span class="title">Invoicing</span>
                                <span class="
                                    {{ Request::is('admin/invoices') ? 'selected' : '' }}
                                    {{ Request::is('admin/invoices/*') ? 'selected' : '' }}
                                    {{ Request::is('admin/proformainvoices') ? 'selected' : '' }}
                                    {{ Request::is('admin/proformainvoices/*') ? 'selected' : '' }}"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item
                                    {{ Request::is('admin/invoices') ? 'active' : '' }}
                                    {{ Request::is('admin/invoices/*') ? 'active' : '' }}">
                                    <a href="/admin/invoices" class="nav-link ">
                                        <span class="title">Invoices</span>
                                    </a>
                                </li>
                                <li class="nav-item
                                    {{ Request::is('admin/proformainvoices') ? 'active' : '' }}
                                    {{ Request::is('admin/proformainvoices/*') ? 'active' : '' }}">
                                    <a href="/admin/proformainvoices" class="nav-link ">
                                        <span class="title">Pro Forma</span>
                                    </a>
                                </li>
                            </ul>
                        </li>


                        <li class="nav-item
                            {{ Request::is('admin/activitylog') ? 'active' : '' }}
                            {{ Request::is('admin/activitylog/*') ? 'active' : '' }}
                            {{ Request::is('admin/activity') ? 'active' : '' }}
                            {{ Request::is('admin/activity/*') ? 'active' : '' }}">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-user"></i>
                                <span class="title">Activity Log</span>
                                <span class="
                                    {{ Request::is('admin/activitylog') ? 'selected' : '' }}
                                    {{ Request::is('admin/activitylog/*') ? 'selected' : '' }}
                                    {{ Request::is('admin/activity') ? 'selected' : '' }}
                                    {{ Request::is('admin/activity/*') ? 'selected' : '' }}"></span>
                                <span class="arrow
                                    {{ Request::is('admin/activitylog') ? 'open' : '' }}
                                    {{ Request::is('admin/activitylog/*') ? 'open' : '' }}
                                    {{ Request::is('admin/activity') ? 'open' : '' }}
                                    {{ Request::is('admin/activity/*') ? 'open' : '' }}
                                    {{ Request::is('admin/roles') ? 'open' : '' }}
                                    {{ Request::is('admin/roles/*') ? 'open' : '' }}"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item
                                    {{ Request::is('admin/activitylog') ? 'active' : '' }}
                                    {{ Request::is('admin/activitylog/*') ? 'active' : '' }}">
                                    <a href="/admin/activitylog" class="nav-link ">
                                        <span class="title">Activity Log</span>
                                    </a>
                                </li>
                                <li class="nav-item
                                    {{ Request::is('admin/activity') ? 'active' : '' }}
                                    {{ Request::is('admin/activity/*') ? 'active' : '' }}">
                                    <a href="/admin/activity" class="nav-link ">
                                        <span class="title">Activity Action</span>
                                    </a>
                                </li>
                            </ul>
                        </li>


                        <li class="nav-item
                            {{ Request::is('admin/usermanagement') ? 'active' : '' }}
                            {{ Request::is('admin/usermanagement/*') ? 'active' : '' }}
                            {{ Request::is('admin/roles') ? 'active' : '' }}
                            {{ Request::is('admin/roles/*') ? 'active' : '' }}">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="icon-user"></i>
                                <span class="title">User Management</span>
                                <span class="
                                    {{ Request::is('admin/usermanagement') ? 'selected' : '' }}
                                    {{ Request::is('admin/usermanagement/*') ? 'selected' : '' }}
                                    {{ Request::is('admin/roles') ? 'selected' : '' }}
                                    {{ Request::is('admin/roles/*') ? 'selected' : '' }}"></span>
                                <span class="arrow
                                    {{ Request::is('admin/usermanagement') ? 'open' : '' }}
                                    {{ Request::is('admin/usermanagement/*') ? 'open' : '' }}
                                    {{ Request::is('admin/roles') ? 'open' : '' }}
                                    {{ Request::is('admin/roles/*') ? 'open' : '' }}"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item
                                    {{ Request::is('admin/usermanagement/users') ? 'active' : '' }}
                                    {{ Request::is('admin/usermanagement/users/*') ? 'active' : '' }}">
                                    <a href="/admin/usermanagement/users" class="nav-link ">
                                        <span class="title">Users</span>
                                    </a>
                                </li>
                                <li class="nav-item
                                    {{ Request::is('admin/roles') ? 'active' : '' }}
                                    {{ Request::is('admin/roles/*') ? 'active' : '' }}">
                                    <a href="/admin/roles" class="nav-link ">
                                        <span class="title">User Roles</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item
                            {{ Request::is('admin/settings') ? 'active' : '' }}
                            {{ Request::is('admin/settings/*') ? 'active' : '' }}">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-cogs"></i>
                                <span class="title">Settings</span>
                                <span class="
                                    {{ Request::is('admin/settings') ? 'selected' : '' }}
                                    {{ Request::is('admin/settings/*') ? 'selected' : '' }}"></span>
                                <span class="arrow
                                    {{ Request::is('admin/settings') ? 'open' : '' }}
                                    {{ Request::is('admin/settings/*') ? 'open' : '' }}"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item
                                    {{ Request::is('admin/settings') ? 'active' : '' }}">
                                    <a href="/admin/settings" class="nav-link ">
                                        <span class="title">General</span>
                                    </a>
                                </li>
                                <li class="nav-item
                                    {{ Request::is('admin/settings/company_details') ? 'active' : '' }}">
                                    <a href="/admin/settings/company_details" class="nav-link ">
                                        <span class="title">Company Details</span>
                                    </a>
                                </li>
                                <li class="nav-item
                                    {{ Request::is('admin/settings/bank_accounts') ? 'active' : '' }}">
                                    <a href="/admin/settings/bank_accounts" class="nav-link ">
                                        <span class="title">Bank Accounts</span>
                                    </a>
                                </li>
                                <li class="nav-item
                                    {{ Request::is('admin/settings/api') ? 'active' : '' }}">
                                    <a href="/admin/settings/api" class="nav-link ">
                                        <span class="title">API</span>
                                    </a>
                                </li>
                                <li class="nav-item
                                    {{ Request::is('admin/settings/currencies/') ? 'active' : '' }}
                                    {{ Request::is('admin/settings/currencies/*') ? 'active' : '' }}">
                                    <a href="javascript:;" class="nav-link nav-toggle">
                                        <i class="fa fa-cogs"></i>
                                        <span class="title">Systems</span>
                                        <span class="
                                            {{ Request::is('admin/settings/currencies') ? 'selected' : '' }}
                                            {{ Request::is('admin/settings/currencies/*') ? 'selected' : '' }}
                                            {{ Request::is('admin/settings/crossrates') ? 'selected' : '' }}
                                            {{ Request::is('admin/settings/crossrates/*') ? 'selected' : '' }}
                                            {{ Request::is('admin/settings/failedjobs') ? 'selected' : '' }}
                                            {{ Request::is('admin/settings/failedjobs/*') ? 'selected' : '' }}"></span>
                                        <span class="arrow
                                            {{ Request::is('admin/settings/currencies') ? 'open' : '' }}
                                            {{ Request::is('admin/settings/currencies/*') ? 'open' : '' }}
                                            {{ Request::is('admin/settings/crossrates') ? 'open' : '' }}
                                            {{ Request::is('admin/settings/crossrates/*') ? 'open' : '' }}
                                            {{ Request::is('admin/settings/failedjobs') ? 'open' : '' }}
                                            {{ Request::is('admin/settings/failedjobs/*') ? 'open' : '' }}"></span>
                                    </a>
                                    <ul class="sub-menu">
                                        <li class="nav-item
                                            {{ Request::is('admin/settings/currencies') ? 'active' : '' }}">
                                            <a href="/admin/settings/currencies" class="nav-link ">
                                                <span class="title">Curerncy</span>
                                            </a>
                                        </li>
                                        <li class="nav-item
                                            {{ Request::is('admin/settings/crossrates') ? 'active' : '' }}">
                                            <a href="/admin/settings/crossrates" class="nav-link ">
                                                <span class="title">Crossrate</span>
                                            </a>
                                        </li>
                                        <li class="nav-item
                                            {{ Request::is('admin/settings/failedjobs') ? 'active' : '' }}">
                                            <a href="/admin/settings/failedjobs" class="nav-link ">
                                                <span class="title">Failed Job</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
