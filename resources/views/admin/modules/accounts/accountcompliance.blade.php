@extends('admin/html')

@section('content')
@include('admin/modules/accounts/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->	
<div class="tabbable tabbable-tabdrop">
    @include('admin/modules/accounts/includes/accounttabs')
    <div class="tab-content">
        <div class="tab-pane active">
            <h3 class="page-title">
                {!! $subpagetitle !!}
                <a class="btn blue bold" data-toggle="modal" href="#edit">Edit <i class="fa fa-plus"></i></a>
            </h3>
            <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Edit a Document</h4>
                        </div>
                        <div class="modal-body"> 
							{!! Form::model($compliance, ['method' => 'POST', 'action' => ['Admin\Modules\Accounts\AccountComplianceController@update', $account_id, $compliance_id ]]) !!}
								<div class="form-body">
									<div class="form-group @if ($errors->has('name')) has-error @endif">
                                        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Title']) !!}
                                        @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
                                    </div>
									<div class="form-group @if ($errors->has('status')) has-error @endif">
                                        {!! Form::select('status', ['Pending' => 'Pending','Approved' => 'Approved','Declined' => 'Declined'], null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
                                        @if ($errors->has('status')) <p class="help-block">{{ $errors->first('status') }}</p> @endif
                                    </div>
                                    <div class="form-group @if ($errors->has('description')) has-error @endif">
                                        {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Description']) !!}
                                        @if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
                                    </div>
									<div class="form-group">
                                        <span class="input-group-btn">
                                            {!! Form::submit('Edit', ['class' => 'btn btn-block green-jungle btn-lg'] ) !!}
                                        </span>
                                    </div>
								</div>
							{!! Form::close() !!}
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <div class="table-scrollable">
                <table class="table table-light table-hover">
                    <tbody>
                        <tr>
                            <td style="width:30%">Compliance</td>
                            <td><span class="label label-sm @if ($compliance->status === 'Approved') label-success @elseif ($compliance->status === 'Pending') label-info @elseif ($compliance->status === 'Locked') label-danger @elseif ($compliance->status === 'Declined') label-danger @else label-default @endif">{!! $compliance->status !!}</span></td>
                        </tr>
                        <tr>
                            <td>Name</td>
                            <td>{!! $compliance->name !!}</td>
                        </tr>
                        <tr>
                            <td>Description</td>
                            <td>{!! $compliance->description !!}</td>
                        </tr>
						<tr>
                            <td>File Type</td>
                            <td style="text-transform:uppercase">{!! $compliance->file_type !!}</td>
                        </tr>
                        <tr>
							@if ($compliance->file_type === 'pdf') <td colspan="2"><embed src="/{!! $compliance->file_path !!}" width="100%" height="600px" /></td>
							@elseif ($compliance->file_type === 'jpg') <td colspan="2"><img src="/{!! $compliance->file_path !!}" style="max-width:100%" /></td>
							@elseif ($compliance->file_type === 'png') <td colspan="2"><img src="/{!! $compliance->file_path !!}" style="max-width:100%" /></td>
							@else <td colspan="2"></td>
							@endif
						</tr>
                    </tbody>
                </table>
            </div>
        </div>					
    </div>
</div>
<!-- END PAGE CONTENT-->
@stop