@extends('admin/html')

@section('content')
@include('admin/modules/accounts/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->	
<div class="tabbable tabbable-tabdrop">
    @include('admin/modules/accounts/includes/accounttabs')
    <div class="tab-content">
        <div class="tab-pane active">
            <h3 class="page-title">
                {!! $subpagetitle !!}
            </h3>
            @include('admin/modules/accounts/includes/wallettabs')
            <div class="row">
                <div class="col-md-3">
                    Maximum Balance
                </div>
                <div class="col-md-8">
                    <div class="progress">
                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="{!! $current_balance_bar !!}" aria-valuemin="0" aria-valuemax="100" style="width: {!! $current_balance_bar !!}%">
                            <span class="sr-only"> {!! $current_balance_bar !!}% Complete (success) </span> € {!! number_format($wallet->balance) !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-1">
                    @if ($wallet->currency === 'EUR') € @elseif ($wallet->currency === 'USD') $ @elseif ($wallet->currency === 'GBP') £ @endif {!! number_format($walletlimit->max_balance) !!}
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    Maximum Monthly Turnover
                </div>
                <div class="col-md-8">
                    <div class="progress">
                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                            <span class="sr-only"> 40% Complete (success) </span> € 1,000
                        </div> 
                    </div>
                </div>
                <div class="col-md-1">
                    @if ($wallet->currency === 'EUR') € @elseif ($wallet->currency === 'USD') $ @elseif ($wallet->currency === 'GBP') £ @endif {!! number_format($walletlimit->max_turnover_per_month) !!}
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    Maximum Turnover
                </div>
                <div class="col-md-8">
                    <div class="progress">
                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="{!! $current_turnover_bar !!}" aria-valuemin="0" aria-valuemax="100" style="width: {!! $current_turnover_bar !!}%">
                            <span class="sr-only"> {!! $current_turnover_bar !!}% Complete (success) </span> € {!! number_format($current_turnover) !!}
                        </div> 
                    </div>
                </div>
                <div class="col-md-1">
                    @if ($wallet->currency === 'EUR') € @elseif ($wallet->currency === 'USD') $ @elseif ($wallet->currency === 'GBP') £ @endif {!! number_format($walletlimit->max_turnover) !!}
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    Maximum Withdrawal
                </div>
                <div class="col-md-8">
                    <div class="progress">
                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 20%">
                            <span class="sr-only"> 20% Complete (success) </span> € 200
                        </div> 
                    </div>
                </div>
                <div class="col-md-1">
                    @if ($wallet->currency === 'EUR') € @elseif ($wallet->currency === 'USD') $ @elseif ($wallet->currency === 'GBP') £ @endif {!! number_format($walletlimit->max_withdraw_amount) !!}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->
@stop