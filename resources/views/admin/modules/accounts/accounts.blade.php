@extends('admin/html')

@section('content')
@include('admin/modules/accounts/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->
<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add a Customer</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['url' => 'admin/accounts']) !!}
                    <div class="form-body">
                        <div class="form-group @if ($errors->has('first_name')) has-error @endif">
                            {!! Form::text('first_name', null, ['class' => 'form-control', 'placeholder' => 'First Name']) !!}
                            @if ($errors->has('first_name')) <p class="help-block">{{ $errors->first('first_name') }}</p> @endif
                        </div>
                        <div class="form-group @if ($errors->has('last_name')) has-error @endif">
                            {!! Form::text('last_name', null, ['class' => 'form-control', 'placeholder' => 'Last Name']) !!}
                            @if ($errors->has('last_name')) <p class="help-block">{{ $errors->first('last_name') }}</p> @endif
                        </div>
                        <div class="form-group @if ($errors->has('email')) has-error @endif">
                            {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
                            @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
                        </div>
                        <div class="form-group @if ($errors->has('phone')) has-error @endif">
                            {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Phone']) !!}
                            @if ($errors->has('phone')) <p class="help-block">{{ $errors->first('phone') }}</p> @endif
                        </div>
                        <div class="form-group @if ($errors->has('street')) has-error @endif">
                            {!! Form::text('street', null, ['class' => 'form-control', 'placeholder' => 'Street']) !!}
                            @if ($errors->has('street')) <p class="help-block">{{ $errors->first('street') }}</p> @endif
                        </div>
                        <div class="form-group @if ($errors->has('city')) has-error @endif">
                            {!! Form::text('city', null, ['class' => 'form-control', 'placeholder' => 'City']) !!}
                            @if ($errors->has('city')) <p class="help-block">{{ $errors->first('city') }}</p> @endif
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="@if ($errors->has('country')) has-error @endif">
										{!! Form::select('country', $countries, null, ['id' => 'country','class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
										@if ($errors->has('country')) <p class="help-block">{{ $errors->first('country') }}</p> @endif
									</div>
								</div>
                                <div class="col-md-6">
                                    <div class="@if ($errors->has('post_code')) has-error @endif">
                                        {!! Form::text('post_code', null, ['class' => 'form-control', 'placeholder' => 'Post Code']) !!}
                                        @if ($errors->has('post_code')) <p class="help-block">{{ $errors->first('post_code') }}</p> @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group @if ($errors->has('wallet_type_id')) has-error @endif">
                            <label>+Wallet Type</label>
                            {!! Form::select('wallet_type_id', $wallettypes, null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
                            @if ($errors->has('wallet_type_id')) <p class="help-block">{{ $errors->first('wallet_type_id') }}</p> @endif
                        </div>
                        <div class="form-group">
                            <span class="input-group-btn">
                                {!! Form::submit('Add', ['class' => 'btn btn-block green-jungle btn-lg'] ) !!}
                            </span>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="portlet-body">
    @if (count($accounts) == 0)
	<div class="row">
		<div class="col-sm-6">
			{!! Form::open(['action' => 'Admin\Modules\Accounts\AccountsController@index', 'method' => 'GET']) !!}
				<div class="input-group input-medium">
					{!! Form::text('search', Request::get('search'), ['class' => 'form-control input-medium', 'placeholder' => 'Search']) !!}
					<span class="input-group-btn">
						{!! Form::button('<i class="icon-magnifier"></i>', ['class' => 'btn green-jungle', 'type'=>'submit']) !!}
					</span>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
	<p class="text-center font-grey-silver margin-top-15">There are no accounts to display</p>
	@else
	<div class="row">
		<div class="col-sm-6">
			{!! Form::open(['action' => 'Admin\Modules\Accounts\AccountsController@index', 'method' => 'GET']) !!}
				<div class="input-group input-medium">
					{!! Form::text('search', Request::get('search'), ['class' => 'form-control input-medium', 'placeholder' => 'Search']) !!}
					<span class="input-group-btn">
						{!! Form::button('<i class="icon-magnifier"></i>', ['class' => 'btn green-jungle', 'type'=>'submit']) !!}
					</span>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
	<div class="table-scrollable">
		<table class="table table-condensed table-bordered table-striped table-hover">
			<thead>
				<tr>
					<th style="width:1%">#</th>
					<th style="width:1%">Account</br>ID</th>
					<th style="width:1%">First</br>Name</th>
					<th style="width:1%">Last</br>Name</th>
					<th>E-mail</th>
					<th>Phone</th>
                    <th style="width:1%">+Wallet Type</th>
					<th style="width:1%">Compliance</th>
					<th style="width:1%">Status</th>
					<th style="width:1%">Action</th>
				</tr>
			</thead>
			<tbody>
				<?php $i = 0 ?>
				@foreach ($accounts as $account)
				<?php $i++ ?>
				<tr>
					<td>{!! $i !!}</td>
					<td>{!! $account->account_id !!}</td>
					<td>{!! $account->first_name !!}</td>
					<td>{!! $account->last_name !!}</td>
					<td>{!! $account->email !!}</td>
					<td>{!! $account->phone !!}</td>
                    <td>{!! $account->type_name !!}</td>
					<td><span class="label label-sm @if ($account->compliance === 'Approved') label-success @elseif ($account->compliance === 'Pending') label-info @elseif ($account->compliance === 'Declined') label-danger @else label-default @endif">{!! $account->compliance !!}</span></td>
					<td><span class="label label-sm @if ($account->status === 'Active') label-success @elseif ($account->status === 'Inactive') label-default @elseif ($account->status === 'Locked') label-danger @else label-default @endif">{!! $account->status !!}</span></td>
					<td>
						<a href="/admin/accounts/{!! $account->account_id !!}" class="btn blue btn-xs"><i class="fa fa-pencil"></i></a>
						@if ($account->status === 'Active' || $account->status === 'Inactive')
							{!! Form::open(['method' => 'PATCH', 'action' => ['Admin\Modules\Accounts\AccountsController@lockunlock', $account->account_id], 'style' => 'display:inline-block;']) !!}
								{!! Form::hidden('status', 'Locked') !!}
								{!! Form::button('<i class="fa fa-lock"></i>', ['class' => 'btn red btn-xs', 'type'=>'submit', 'data-toggle' => 'confirmation', 'data-popout' => 'true', 'data-singleton' => 'true', 'data-original-title' => 'Lock! Are you sure?', 'title' => '']) !!}
							{!! Form::close() !!}
						@elseif ($account->status === 'Locked')
							{!! Form::open(['method' => 'PATCH', 'action' => ['Admin\Modules\Accounts\AccountsController@lockunlock', $account->account_id], 'style' => 'display:inline-block;']) !!}
								{!! Form::hidden('status', 'Active') !!}
								{!! Form::button('<i class="fa fa-unlock-alt"></i>', ['class' => 'btn green-jungle btn-xs', 'type'=>'submit', 'data-toggle' => 'confirmation', 'data-popout' => 'true', 'data-singleton' => 'true', 'data-original-title' => 'Unlock! Are you sure?', 'title' => '']) !!}
							{!! Form::close() !!}
						@endif
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	<div class="row">
		<div class="col-md-6 margin-top-10">
			Showing {!! $accounts->firstItem() !!} to {!! $accounts->lastItem() !!} of {!! $accounts->total() !!}
		</div>
		<div class="col-md-6 text-right">
			{!! $accounts->links() !!}
		</div>
	</div>
	@endif
</div>
<!-- END PAGE CONTENT-->
@stop

