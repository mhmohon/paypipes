@extends('admin/html')

@section('content')
@include('admin/modules/accounts/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->
<div class="tabbable tabbable-tabdrop">
    @include('admin/modules/accounts/includes/accounttabs')
    <div class="tab-content">
        <div class="tab-pane active">
            <div class="portlet-body">
				@if (count($payments) == 0)
				<p class="text-center font-grey-silver margin-top-15">There are no payments to display</p>
				@else
				<div class="table-scrollable">
					<table class="table table-light table-bordered table-striped table-hover order-column">
						<thead>
							<tr>
								<th style="width:1%">Settlement </br>Date</th>
								<th style="width:1%">Terminal ID</br>Website</th>
								<th>Description</th>
								<th style="width:1%">Method</br>Type</th>
								<th class="text-right" style="width:1%">Amount</th>
								<th style="width:1%">Action</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($payments as $payment)
							<tr>
								<td class="text-center sorting_1" tabindex="0"><span class="font-sm uppercase">{!! Carbon\Carbon::parse($payment->created_at)->format('M') !!}</span> </br> <span style="font-size:22px;font-weight:300;">{!! Carbon\Carbon::parse($payment->created_at)->format('d') !!}</span></td>
								<td>{!! $payment->account_id !!}</br>website</td>
								<td>From {!! $payment->from !!} to {!! $payment->to !!}</br><span style="font-size:18px;font-weight:300;">{!! $payment->description !!}</span></td>
								<td>{!! $payment->payment_method !!}</br>{!! $payment->payment_type !!}</td>
								<td class="text-right">
									<span style="font-size:22px;font-weight:300;" class="@if ($payment->to == $account->email)  font-green-jungle @elseif ($payment->from == $account->email) font-red-thunderbird @else @endif">
										@if ($payment->to == $account->email) +{!! $payment->total_amount !!} @elseif ($payment->from == $account->email) -{!! $payment->amount !!} @else @endif
									</span> </br>
									<span class="font-sm">{!! $payment->currency !!}</span>
								</td>
								<td>
									<a href="/admin/payments/{!! $payment->payment_id !!}" class="btn blue btn-xs"><i class="fa fa-pencil"></i></a>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				@endif
			</div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->
@stop
