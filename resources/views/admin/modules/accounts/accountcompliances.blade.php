@extends('admin/html')

@section('content')
@include('admin/modules/accounts/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->
<div class="tabbable tabbable-tabdrop">
    @include('admin/modules/accounts/includes/accounttabs')
    <div class="tab-content">
        <div class="tab-pane active">
            <h3 class="page-title">
                Compliance
                <a class="btn green-jungle bold" data-toggle="modal" href="#add">Add <i class="fa fa-plus"></i></a>
            </h3>
            <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Add a Document</h4>
                        </div>
                        <div class="modal-body">
                            {!! Form::model($account, ['method' => 'POST', 'files' => 'true', 'action' => ['Admin\Modules\Accounts\AccountComplianceController@store', $account->account_id ]]) !!}
                                <div class="form-body">
                                    <div class="form-group">
                                        {!! Form::file('upload_file') !!}
										@if ($errors->has('upload_file')) <p class="help-block">{{ $errors->first('upload_file') }}</p> @endif
                                    </div>
                                    <div class="form-group @if ($errors->has('compliance_name')) has-error @endif">
                                        {!! Form::text('compliance_name', null, ['class' => 'form-control', 'placeholder' => 'Title']) !!}
                                        @if ($errors->has('compliance_name')) <p class="help-block">{{ $errors->first('compliance_name') }}</p> @endif
                                    </div>
                                    <div class="form-group @if ($errors->has('status')) has-error @endif">
                                        {!! Form::select('status', ['Pending' => 'Pending','Approved' => 'Approved','Declined' => 'Declined'], null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
                                        @if ($errors->has('status')) <p class="help-block">{{ $errors->first('status') }}</p> @endif
                                    </div>
                                    <div class="form-group @if ($errors->has('description')) has-error @endif">
                                        {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Description']) !!}
                                        @if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
                                    </div>
                                    <div class="form-group">
                                        <span class="input-group-btn">
                                            {!! Form::submit('Add', ['class' => 'btn btn-block green-jungle btn-lg'] ) !!}
                                        </span>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            @if (count($compliances) == 0)
			<p class="text-center font-grey-silver margin-top-15">There are no documents to display</p>
			@else
			<div class="row">
				<div class="col-sm-6">
					{!! Form::open() !!}
						<div class="input-group input-medium">
							<input class="form-control input-medium" placeholder="Search" type="text">
							<span class="input-group-btn">
								<button class="btn green-jungle" type="submit"><i class="icon-magnifier"></i></button>
							</span>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
			<div class="table-scrollable">
				<table class="table table-condensed table-bordered table-striped table-hover">
					<thead>
						<tr>
							<th style="width:1%">#</th>
							<th style="width:1%">ID</th>
							<th style="width:1%">Name</th>
							<th>Description</th>
							<th style="width:1%">Compliance</th>
							<th style="width:1%">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php $i = 0 ?>
						@foreach ($compliances as $compliance)
						<?php $i++ ?>
						<tr>
							<td>{!! $i !!}</td>
							<td>{!! $compliance->compliance_id !!}</td>
							<td>{!! $compliance->name !!}</td>
							<td>{!! $compliance->description !!}</td>
							<td><span class="label label-sm @if ($compliance->status === 'Approved') label-success @elseif ($compliance->status === 'Pending') label-info @elseif ($compliance->status === 'Declined') label-danger @else label-default @endif">{!! $compliance->status !!}</span></td>
							<td>
								<a href="/admin/accounts/{!! $account->account_id !!}/customercompliances/{!! $compliance->compliance_id !!}" class="btn blue btn-xs"><i class="fa fa-pencil"></i></a>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="row">
				<div class="col-md-6 margin-top-10">
					Showing {!! $compliances->firstItem() !!} to {!! $compliances->lastItem() !!} of {!! $compliances->total() !!}
				</div>
				<div class="col-md-6 text-right">
					{!! $compliances->links() !!}
				</div>
			</div>
			@endif
        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->
@stop
