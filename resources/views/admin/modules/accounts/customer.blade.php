@extends('admin/html')

@section('content')
@include('admin/modules/accounts/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->
<div class="tabbable tabbable-tabdrop">
    @include('admin/modules/accounts/includes/customertabs')
    <div class="tab-content">
        <div class="tab-pane active">
            <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h4 class="modal-title">Edit a Customer</h4>
                        </div>
                        <div class="modal-body">
                            {!! Form::model($account, ['method' => 'POST', 'action' => ['Admin\Modules\Accounts\AccountsController@update', $account->account_id ]]) !!}
                                <div class="form-body">
                                    <div class="form-group @if ($errors->has('compliance')) has-error @endif">
                                        {!! Form::select('compliance', ['Pending' => 'Pending','Approved' => 'Approved','Declined' => 'Declined','Not Required' => 'Not Required'], null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
                                        @if ($errors->has('compliance')) <p class="help-block">{{ $errors->first('compliance') }}</p> @endif
                                    </div>
                                    <div class="form-group @if ($errors->has('first_name')) has-error @endif">
                                        {!! Form::text('first_name', null, ['class' => 'form-control', 'placeholder' => 'First Name']) !!}
                                        @if ($errors->has('first_name')) <p class="help-block">{{ $errors->first('first_name') }}</p> @endif
                                    </div>
                                    <div class="form-group @if ($errors->has('last_name')) has-error @endif">
                                        {!! Form::text('last_name', null, ['class' => 'form-control', 'placeholder' => 'Last Name']) !!}
                                        @if ($errors->has('last_name')) <p class="help-block">{{ $errors->first('last_name') }}</p> @endif
                                    </div>
                                    <div class="form-group @if ($errors->has('street')) has-error @endif">
                                        {!! Form::text('street', null, ['class' => 'form-control', 'placeholder' => 'Street']) !!}
                                        @if ($errors->has('street')) <p class="help-block">{{ $errors->first('street') }}</p> @endif
                                    </div>
                                    <div class="form-group @if ($errors->has('phone')) has-error @endif">
                                        {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Phone']) !!}
                                        @if ($errors->has('phone')) <p class="help-block">{{ $errors->first('phone') }}</p> @endif
                                    </div>
                                    <div class="form-group @if ($errors->has('email')) has-error @endif">
                                        {!! Form::text('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
                                        @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
                                    </div>
                                    <div class="form-group @if ($errors->has('city')) has-error @endif">
                                        {!! Form::text('city', null, ['class' => 'form-control', 'placeholder' => 'City']) !!}
                                        @if ($errors->has('city')) <p class="help-block">{{ $errors->first('city') }}</p> @endif
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-6">
											    <div class="@if ($errors->has('country')) has-error @endif">
													{!! Form::select('country', $countries, null, ['id' => 'country','class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
													@if ($errors->has('country')) <p class="help-block">{{ $errors->first('country') }}</p> @endif
											    </div>
										    </div>
                                            <div class="col-md-6">
                                                <div class="@if ($errors->has('post_code')) has-error @endif">
                                                    {!! Form::text('post_code', null, ['class' => 'form-control', 'placeholder' => 'Post Code']) !!}
                                                    @if ($errors->has('post_code')) <p class="help-block">{{ $errors->first('post_code') }}</p> @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>
									<div class="form-group @if ($errors->has('wallet_type_id')) has-error @endif">
                                        <label>+Wallet Type</label>
										{!! Form::select('wallet_type_id', $wallettypes, null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
                                        @if ($errors->has('wallet_type_id')) <p class="help-block">{{ $errors->first('wallet_type_id') }}</p> @endif
                                    </div>
                                    <div class="form-group">
                                        <span class="input-group-btn">
                                            {!! Form::submit('Edit', ['class' => 'btn btn-block green-jungle btn-lg'] ) !!}
                                        </span>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <div class="table-scrollable">
                <table class="table table-light table-hover">
                    <tbody>
                        <tr>
                            <td style="width:30%">Status</td>
                            <td><span class="label label-sm @if ($account->status === 'Active') label-success @elseif ($account->status === 'Locked') label-danger @elseif ($account->status === 'Inactive') label-default @else label-default @endif">{!! $account->status !!}</span></td>
                        </tr>
                        <tr>
                            <td>Compliance</td>
                            <td><span class="label label-sm @if ($account->compliance === 'Approved') label-success @elseif ($account->compliance === 'Pending') label-info @elseif ($account->compliance === 'Declined') label-danger @else label-default @endif">{!! $account->compliance !!}</span></td>
                        </tr>
                        <tr>
                            <td>First Name</td>
                            <td>{!! $account->first_name !!}</td>
                        </tr>
                        <tr>
                            <td>Last Name</td>
                            <td>{!! $account->last_name !!}</td>
                        </tr>
                        <tr>
                            <td>Phone</td>
                            <td>{!! $account->phone !!}</td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>{!! $account->email !!}</td>
                        </tr>
                        <tr>
                            <td>Street</td>
                            <td>{!! $account->street !!}</td>
                        </tr>
                        <tr>
                            <td>City</td>
                            <td>{!! $account->city !!}</td>
                        </tr>
                        <tr>
                            <td>Country</td>
                            <td>{!! $account->country_name !!}</td>
                        </tr>
                        <tr>
                            <td>Post Code</td>
                            <td>{!! $account->post_code !!}</td>
                        </tr>
						<tr>
                            <td>+Wallet Type</td>
                            <td><a href="/admin/wallettypes/{!! $account->wallet_type_id !!}">{!! $account->wallet_type_id !!}</a></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->
@stop
