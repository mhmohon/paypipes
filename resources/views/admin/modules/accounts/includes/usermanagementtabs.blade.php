<ul class="nav nav-tabs">
    <li class="@if (Request::is('admin/accounts/'.$account_id.'/users')) active
           @elseif (Request::is('admin/accounts/'.$account_id.'/users/*')) active
           @endif">
            <a href="/admin/accounts/{!! $account_id !!}/users">Users</a>
    </li>
    <li class="@if (Request::is('admin/accounts/'.$account_id.'/roles')) active
           @elseif (Request::is('admin/accounts/'.$account_id.'/roles/*')) active
           @endif">
            <a href="/admin/accounts/{!! $account_id !!}/roles">Roles</a>
    </li>
</ul>

