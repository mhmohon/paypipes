<ul class="nav nav-tabs">
    <li class="{{ Request::is('admin/accounts/'.$account_id) ? 'active' : '' }}">
        <a href="/admin/accounts/{!! $account_id !!}">Account</a>
    </li>

    <li class="@if (Request::is('admin/accounts/'.$account_id.'/wallets')) active @elseif (Request::is('admin/accounts/'.$account_id.'/wallets/*')) active @else @endif">
        <a href="/admin/accounts/{!! $account_id !!}/wallets">+Wallets</a>
    </li>

    <li class="{{ Request::is('admin/accounts/'.$account_id.'/payments') ? 'active' : '' }}">
        <a href="/admin/accounts/{!! $account_id !!}/payments">Payments</a>
    </li>

    <li class="{{ Request::is('admin/accounts/'.$account_id.'/cards') ? 'active' : '' }}">
        <a href="/admin/accounts/{!! $account_id !!}/cards">Cards</a>
    </li>
    <li class="@if (Request::is('admin/accounts/'.$account_id.'/statements')) active @elseif (Request::is('admin/accounts/'.$account_id.'/statements/*')) active @else @endif">
        <a href="/admin/accounts/{!! $account_id !!}/statements">Statements</a>
    </li>
    <li class="@if (Request::is('admin/accounts/'.$account_id.'/accountcompliances')) active @elseif (Request::is('admin/accounts/'.$account_id.'/accountcompliances/*')) active @else @endif">
        <a href="/admin/accounts/{!! $account_id !!}/accountcompliances">Compliance</a>
    </li>
    <li class="@if (Request::is('admin/accounts/'.$account_id.'/users')) active
               @elseif (Request::is('admin/accounts/'.$account_id.'/users/*')) active
               @elseif (Request::is('admin/accounts/'.$account_id.'/roles')) active
               @elseif (Request::is('admin/accounts/'.$account_id.'/roles/*')) active
               @endif">
            <a href="/admin/accounts/{!! $account_id !!}/users">User Management</a>
    </li>
</ul>

