<ul class="nav nav-tabs">
    <li class="{{ Request::is('admin/accounts/'.$account_id.'/wallets/'.$wallet_id) ? 'active' : '' }}">
        <a href="/admin/accounts/{!! $account_id !!}/wallets/{!! $wallet_id !!}">+Wallet</a>
    </li>
    <li class="{{ Request::is('admin/accounts/'.$account_id.'/wallets/'.$wallet_id.'/payments') ? 'active' : '' }}">
        <a href="/admin/accounts/{!! $account_id !!}/wallets/{!! $wallet_id !!}/payments">Payments</a>
    </li>
	<li class="{{ Request::is('admin/accounts/'.$account_id.'/wallets/'.$wallet_id.'/statements') ? 'active' : '' }}">
        <a href="/admin/accounts/{!! $account_id !!}/wallets/{!! $wallet_id !!}/statements">Statements</a>
    </li>
    <li class="{{ Request::is('admin/accounts/'.$account_id.'/wallets/'.$wallet_id.'/limits') ? 'active' : '' }}">
        <a href="/admin/accounts/{!! $account_id !!}/wallets/{!! $wallet_id !!}/limits">Limits</a>
    </li>
</ul>

