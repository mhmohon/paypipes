@extends('admin/html')

@section('content')
@include('admin/modules/accounts/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->
<div class="tabbable tabbable-tabdrop">
    @include('admin/modules/accounts/includes/accounttabs')
    <div class="tab-content">
        <div class="tab-pane active">
            @include('admin/modules/accounts/includes/usermanagementtabs')
            <h3 class="page-title">
                {!! $subpagetitle !!}
                <a class="btn green-jungle bold" data-toggle="modal" href="#add">Add <i class="fa fa-plus"></i></a>
            </h3>
            <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-hidden="true">
			    <div class="modal-dialog modal-lg">
			        <div class="modal-content">
			            <div class="modal-header">
			                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			                <h4 class="modal-title">Add a Role</h4>
			            </div>
			            <div class="modal-body">                                                         
			                {!! Form::open() !!}
			                    <div class="form-body">
			                        <div class="form-group @if ($errors->has('name')) has-error @endif">
			                            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Role Name']) !!}
			                            @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
			                        </div>  
			                        <div class="form-group @if ($errors->has('email')) has-error @endif">
			                            {!! Form::text('description', null, ['class' => 'form-control', 'placeholder' => 'Description']) !!}
			                            @if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
			                        </div>
			                        <div class="form-group">
			                            <label class="control-label label-required">Permissions</label>
			                            <div class="col-sm-12">
			                                <table class="table">
						                        <tbody>
						                        
						                        </tbody>
						                    </table>
			                            </div>
			                        </div>           
			                                    
			                        <div class="form-group">
			                            <span class="input-group-btn">
			                                {!! Form::submit('Add', ['class' => 'btn btn-block green-jungle btn-lg'] ) !!}
			                            </span>
			                        </div>
			                    </div>
			                {!! Form::close() !!}                   
			            </div>
			        </div>
			        <!-- /.modal-content -->
			    </div>
			    <!-- /.modal-dialog -->
			</div>
            <div class="portlet-body">
				@if (count($whitelistcustomers) == 0)
				<p class="text-center font-grey-silver margin-top-15">There are no accounts to display</p>
				@else
				<div class="table-scrollable">
					<table class="table table-condensed table-bordered table-striped table-hover">
						<thead>
							<tr>
								<th style="width:1%">#</th>
								<th style="width:1%">Role</th>
								<th>Description</th>
								<th style="width:1%">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php $i = 0 ?>    
							@foreach ($whitelistcustomers as $whitelistcustomer)
							<?php $i++ ?>
							<tr>    
								<td>{!! $i !!}</td>
								<td>{Role Name}</td>
								<td>{Description}</td>	
								<td>
									<a href="/admin/accounts/{!! $account->account_id !!}/roles/1" class="btn blue btn-xs"><i class="fa fa-pencil"></i></a>
									{!! Form::open(['method' => 'DELETE', 'action' => ['Admin\Modules\RiskManagement\WhitelistaccountsController@destroy', $whitelistcustomer->id], 'style' => 'display:inline-block;']) !!}
										{!! Form::button('<i class="fa fa-recycle"></i>', ['class' => 'btn dark btn-xs', 'type'=>'submit', 'data-toggle' => 'confirmation', 'data-popout' => 'true', 'data-singleton' => 'true', 'data-original-title' => 'Delete! Are you sure?', 'title' => '']) !!}
									{!! Form::close() !!}
								</td>									
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="row">
					<div class="col-md-6 margin-top-10">
						Showing {!! $whitelistcustomers->firstItem() !!} to {!! $whitelistcustomers->lastItem() !!} of {!! $whitelistcustomers->total() !!}
					</div>
					<div class="col-md-6 text-right">
						{!! $whitelistcustomers->links() !!}
					</div>
				</div>
				@endif
			</div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->
@stop

