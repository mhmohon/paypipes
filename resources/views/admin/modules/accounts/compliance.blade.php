@extends('admin/html')

@section('content')
@include('admin/modules/accounts/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->
<div class="portlet-body">
    @if (count($compliances) == 0)
	<p class="text-center font-grey-silver margin-top-15">There are no documents to display</p>
	@else
	<div class="row">
		<div class="col-sm-6">
			{!! Form::open() !!}
				<div class="input-group input-medium">
					<input class="form-control input-medium" placeholder="Search" type="text">
					<span class="input-group-btn">
						<button class="btn green-jungle" type="submit"><i class="icon-magnifier"></i></button>
					</span>
				</div>
			{!! Form::close() !!}
		</div>

	</div>
	<div class="table-scrollable">
		<table class="table table-condensed table-bordered table-striped table-hover">
			<thead>
				<tr>
					<th style="width:1%">#</th>
					<th style="width:1%">ID</th>
					<th style="width:1%">Name</th>
					<th>Description</th>
					<th style="width:1%">Compliance</th>
					<th style="width:1%">Action</th>
				</tr>
			</thead>
			<tbody>
				<?php $i = 0 ?>
				@foreach ($compliances as $compliance)
				<?php $i++ ?>
				<tr>
					<td>{!! $i !!}</td>
					<td>{!! $compliance->compliance_id !!}</td>
					<td>{!! $compliance->name !!}</td>
					<td>{!! $compliance->description !!}</td>
					<td><span class="label label-sm @if ($compliance->status === 'Approved') label-success @elseif ($compliance->status === 'Pending') label-info @elseif ($compliance->status === 'Declined') label-danger @else label-default @endif">{!! $compliance->status !!}</span></td>
					<td>
						<a href="/admin/accounts/{!! $compliance->account_id !!}/customercompliances/{!! $compliance->compliance_id !!}" class="btn blue btn-xs"><i class="fa fa-pencil"></i></a>
						<button class="btn red btn-xs" data-toggle="confirmation" data-popout="true" data-popout="true" data-singleton="true" data-popout="true" data-singleton="true" data-original-title="Are you sure ?" title=""><i class="fa fa-lock"></i></button>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	<div class="row">
		<div class="col-md-6 margin-top-10">
			Showing {!! $compliances->firstItem() !!} to {!! $compliances->lastItem() !!} of {!! $compliances->total() !!}
		</div>
		<div class="col-md-6 text-right">
			{!! $compliances->links() !!}
		</div>
	</div>
	@endif
</div>
<!-- END PAGE CONTENT-->
@stop

