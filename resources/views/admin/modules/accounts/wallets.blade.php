@extends('admin/html')

@section('content')
@include('admin/modules/accounts/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->
<!-- BEGIN PAGE CONTENT-->
<div class="tabbable tabbable-tabdrop">
    @include('admin/modules/accounts/includes/accounttabs')
    <div class="tab-content">
        <div class="tab-pane active">
            <h3 class="page-title">
                +Walets @if (count($currency_arr) > 0)<a class="btn green-jungle bold" data-toggle="modal" href="#add">Add <i class="fa fa-plus"></i></a> @endif
            </h3>
            <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Add +Wallet</h4>
                        </div>
                        <div class="modal-body">
                            {!! Form::model($account, ['method' => 'POST', 'action' => ['Admin\Modules\Accounts\WalletsController@store', $account->account_id ]]) !!}
                                <div class="form-body">
                                    <div class="form-group @if ($errors->has('currency')) has-error @endif">
										<label>Currency</label>
										{!! Form::select('currency', $currency_arr, null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
										@if ($errors->has('currency')) <p class="help-block">{{ $errors->first('currency') }}</p> @endif
									</div>
									<div class="form-group @if ($errors->has('wallet_fee_id')) has-error @endif">
                                        <label>Wallet Fees</label>
										{!! Form::select('wallet_fee_id', $walletfees, null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
                                        @if ($errors->has('wallet_fee_id')) <p class="help-block">{{ $errors->first('wallet_fee_id') }}</p> @endif
                                    </div>
									<div class="form-group @if ($errors->has('wallet_limit_id')) has-error @endif">
                                        <label>Wallet Limits</label>
										{!! Form::select('wallet_limit_id', $walletlimits, null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
                                        @if ($errors->has('wallet_limit_id')) <p class="help-block">{{ $errors->first('wallet_limit_id') }}</p> @endif
                                    </div>
                                    <div class="form-group @if ($errors->has('wallet_limit_id')) has-error @endif">
                                        <label>Wallet Type</label>
                                        {!! Form::select('wallet_type_id', $wallettypes, null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
                                        @if ($errors->has('wallet_type_id')) <p class="help-block">{{ $errors->first('wallet_type_id') }}</p> @endif
                                    </div>
                                    <div class="form-group">
                                        <span class="input-group-btn">
                                            {!! Form::submit('Add', ['class' => 'btn btn-block green-jungle btn-lg'] ) !!}
                                        </span>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <div class="portlet-body">
				@if (count($wallets) == 0)
				<p class="text-center font-grey-silver margin-top-15">There are no wallets to display</p>
				@else
				<div class="row">
					<div class="col-sm-6">
						{!! Form::open() !!}
							<div class="input-group input-medium">
								<input class="form-control input-medium" placeholder="Search" type="text">
								<span class="input-group-btn">
									<button class="btn green-jungle" type="submit"><i class="icon-magnifier"></i></button>
								</span>
							</div>
						{!! Form::close() !!}
					</div>

				</div>
				<div class="table-scrollable">
					<table class="table table-condensed table-bordered table-striped table-hover">
						<thead>
							<tr>
								<th style="width:1%">#</th>
								<th style="width:1%">Wallet ID</th>
								<th style="width:1%">Primary</th>
								<th style="width:1%">Email</th>
								<th>Fees</th>
								<th>Limits</th>
                                <th>Types</th>
								<th style="width:1%">Currency</th>
								<th style="width:1%" class="text-right">Balance</th>
								<th style="width:1%">Action</th>
							</tr>
						</thead>
						<tbody>
							<?php $i = 0; ?>
							@foreach ($wallets as $wallet)
							<?php $i++ ?>
							<tr>
								<td>{!! $i !!}</td>
								<td>{!! $wallet->wallet_id !!}</td>
								<td>@if ($wallet->primary === 1) <span class="label label-sm label-default">Primary</span> @else @endif</td>
								<td>{!! $wallet->email !!}</td>
								<td><a href="/admin/walletfees/{!! $wallet->wallet_fee_id !!}">{!! $wallet->fee_name !!}</a></td>
								<td><a href="/admin/walletlimits/{!! $wallet->wallet_limit_id !!}">{!! $wallet->limit_name !!}</a></td>
                                <td><a href="/admin/wallettypes/{!! $wallet->wallet_type_id !!}">{!! $wallet->type_name !!}</a></td>
								<td>{!! $wallet->currency !!}</td>
								<td class="text-right">{!! $wallet->balance !!}</td>
								<td>
									<a href="/admin/accounts/{!! $account->account_id !!}/wallets/{!! $wallet->wallet_id !!}" class="btn blue btn-xs"><i class="fa fa-pencil"></i></a>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="row">
					<div class="col-md-6 margin-top-10">
						Showing {!! $wallets->firstItem() !!} to {!! $wallets->lastItem() !!} of {!! $wallets->total() !!}
					</div>
					<div class="col-md-6 text-right">
						{!! $wallets->links() !!}
					</div>
				</div>
				@endif
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->
@stop

