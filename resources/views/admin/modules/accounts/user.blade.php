@extends('admin/html')

@section('content')
@include('admin/modules/accounts/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->
<div class="tabbable tabbable-tabdrop">
    @include('admin/modules/accounts/includes/accounttabs')
    <div class="tab-content">
        <div class="tab-pane active">
            @include('admin/modules/accounts/includes/usermanagementtabs')
            <h3 class="page-title">
                {!! $subpagetitle !!}
                <a class="btn blue bold" data-toggle="modal" href="#edit">Edit <i class="fa fa-pencil"></i></a>
            </h3>
            <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Edit a User</h4>
                        </div>
                        <div class="modal-body">
                         {!! Form::model($account, ['method' => 'POST', 'url' => url('admin/accounts/'.@$account->account_id.'/users/'. @$account->id)]) !!}
                            <div class="form-body">
                                <input name="_token" type="hidden" value="{{ csrf_token() }}">
                                <div class="form-group @if ($errors->has('first_name')) has-error @endif">
                                    <div class="controls">
                                        {!! Form::text('first_name', null, array('class' => 'form-control form-control-solid placeholder-no-fix', 'placeholder' => 'First Name')) !!}
                                        @if ($errors->has('first_name')) <p class="help-block">{{ $errors->first('first_name') }}</p> @endif
                                    </div>
                                </div>
                                <div class="form-group @if ($errors->has('last_name')) has-error @endif">
                                    <div class="controls">
                                        {!! Form::text('last_name', null, array('class' => 'form-control form-control-solid placeholder-no-fix', 'placeholder' => 'Last Name')) !!}
                                        @if ($errors->has('last_name')) <p class="help-block">{{ $errors->first('last_name') }}</p> @endif
                                    </div>
                                </div>
                                <div class="form-group @if ($errors->has('email')) has-error @endif">
                                    <div class="controls">
                                        {!! Form::text('email', null, array('class' => 'form-control form-control-solid placeholder-no-fix', 'placeholder' => 'Email', 'readonly' => '')) !!}
                                        @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
                                    </div>
                                </div>
                                <div class="form-group @if ($errors->has('email')) has-error @endif">
                                    <div class="controls">
                                        {!! Form::select('role', $roles, null, ['id' => 'role','class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
                                        @if ($errors->has('role')) <p class="help-block">{{ $errors->first('role') }}</p> @endif
                                    </div>
                                </div>
                                <div class="form-group @if ($errors->has('is_primary')) has-error @endif">
                                    <div class="controls">
                                        {!! Form::select('is_primary',  ['1' => 'Primary','0' => 'Optional'],  null, ['id' => 'role','class' => 'bs-select form-control']) !!}
                                        @if ($errors->has('is_primary')) <p class="help-block">{{ $errors->first('is_primary') }}</p> @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <span class="input-group-btn">
                                        {!! Form::submit('Edit', ['class' => 'btn btn-block blue btn-lg'] ) !!}
                                    </span>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <div class="portlet-body">
                <div class="row">
                    <div class="col-md-6">
                        <h4>{!! $account->first_name.' '.$account->last_name !!}</h4>
                        <div class="margin-bottom-10"><cite class="font-grey-silver">{!! Carbon\Carbon::parse($account->created_at)->format('jS \o\f F, Y') !!}</cite></div>
                        <h4>{!! $account->email !!}</h4>
                        <div class="margin-bottom-10"><cite class="font-grey-silver">Email</cite></div>
                    </div>
                    <div class="col-md-6">
                        <h4>{Admin}</h4>
                        <div class="margin-bottom-10"><cite class="font-grey-silver">Role</cite></div>
                        <div><span class="label label-sm @if ($account->status === 'Active') label-success @elseif ($account->status === 'Inactive') label-default @elseif ($account->status === 'Locked') label-danger @elseif ($account->status === 'TempLock') label-warning @else label-default @endif"">{!! $account->status !!}</span></div>
                        <div class="margin-bottom-10"><cite class="font-grey-silver">Status</cite></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->
@stop

