@extends('admin/html')

@section('content')
@include('admin/modules/accounts/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->	
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat blue">
            <div class="visual">
                <i class="icon-users"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="{!! $accountcount !!}">0</span>
                </div>
                <div class="desc"> Customers </div>
            </div>
            <a class="more" href="#accounts" data-toggle="tab"> View more
                <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat red">
            <div class="visual">
                <i class="icon-wallet"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="0">0</span></div>
                <div class="desc"> +Wallet Customers </div>
            </div>
            <a class="more" href="#2" data-toggle="tab"> View more
                <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat green">
            <div class="visual">
                <i class="icon-users"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="{!! $todaycustomercount !!}">0</span>
                </div>
                <div class="desc"> Today Customers </div>
            </div>
            <a class="more" href="#3" data-toggle="tab"> View more
                <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat purple">
            <div class="visual">
                <i class="fa fa-globe"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="{!! $accountcountrycount !!}">0</span>
                </div>
                <div class="desc"> Customers Countries </div>
            </div>
            <a class="more" href="#customercountries" data-toggle="tab"> View more
                <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div id="perf_div"></div>
@columnchart('Chart', 'perf_div')
<!-- END PAGE CONTENT-->
@stop

