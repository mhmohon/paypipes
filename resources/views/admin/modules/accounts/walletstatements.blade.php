@extends('admin/html')

@section('content')
@include('admin/modules/accounts/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->
<div class="tabbable tabbable-tabdrop">
    @include('admin/modules/accounts/includes/accounttabs')
    <div class="tab-content">
        <div class="tab-pane active">
            <h3 class="page-title">
                {!! $subpagetitle !!}
            </h3>
            @include('admin/modules/accounts/includes/wallettabs')
            <div class="portlet-body">
				@if (count($statements) == 0)
				<p class="text-center font-grey-silver margin-top-15">There are no statements to display</p>
				@else
				<div class="row">
					<div class="col-sm-6">
						{!! Form::open() !!}
							<div class="input-group input-medium">
								<input class="form-control input-medium" placeholder="Search" type="text">
								<span class="input-group-btn">
									<button class="btn green-jungle" type="submit"><i class="icon-magnifier"></i></button>
								</span>
							</div>
						{!! Form::close() !!}
					</div>

				</div>
				<div class="table-scrollable">
					<table class="table table-condensed table-bordered table-striped table-hover">
						<thead>
							<tr data-href="javascript:void(0);">
								<th style="width:1%">Period</th>
								<th></th>
								<th class="text-right" style="width:1%">Credits</th>
								<th class="text-right" style="width:1%">Debits</th>
								<th class="text-right" style="width:1%">Ending Balance</th>
							</tr>
						</thead>
						<tbody>
							@foreach ($statements as $statement)
							<tr class="clickable-row" data-href="/admin/accounts/{!! $account_id !!}/wallets/{!! $wallet_id !!}/statements/{!! $statement->wallet_statement_id !!}">
								<td><span class="font-sm uppercase">{!! Carbon\Carbon::createFromDate($statement->year, $statement->month)->format('F') !!}<span></br> <span style="font-size:22px;font-weight:300;">{!! $statement->year !!}</span></td>
								<td></td>
								<td class="text-right">
									<span style="font-size:22px;font-weight:300;" class="font-green-jungle">+{!! $statement->credits !!}</span></br>
									<span class="font-sm">{!! $statement->currency !!}</span>
								</td>
								<td class="text-right">
									<span style="font-size:22px;font-weight:300;" class="font-red-thunderbird">-{!! $statement->debits !!}</span></br>
									<span class="font-sm">{!! $statement->currency !!}</span>
								</td>
								<td class="text-right">
									<span style="font-size:22px;font-weight:300;">{!! $statement->ending_balance !!}</span></br>
									<span class="font-sm">{!! $statement->currency !!}</span>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="row">
					<div class="col-md-6 margin-top-10">
						Showing {!! $statements->firstItem() !!} to {!! $statements->lastItem() !!} of {!! $statements->total() !!}
					</div>
					<div class="col-md-6 text-right">
						{!! $statements->links() !!}
					</div>
				</div>
				@endif
			</div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->
@stop
