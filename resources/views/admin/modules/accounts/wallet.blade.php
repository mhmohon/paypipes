@extends('admin/html')

@section('content')
@include('admin/modules/accounts/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->
<div class="tabbable tabbable-tabdrop">
    @include('admin/modules/accounts/includes/accounttabs')
    <div class="tab-content">
        <div class="tab-pane active">
            <h3 class="page-title">
                {!! $subpagetitle !!}
                <a class="btn blue bold" data-toggle="modal" href="#edit">Edit <i class="fa fa-pencil"></i></a>
            </h3>
            <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Edit +Wallet</h4>
                        </div>
                        <div class="modal-body">
                            {!! Form::model($wallet, ['method' => 'POST', 'action' => ['Admin\Modules\Accounts\WalletsController@update', $account_id, $wallet_id ]]) !!}
                                <div class="form-body">
									<div class="form-group @if ($errors->has('wallet_fee_id')) has-error @endif">
                                        <label>Wallet Fees</label>
										{!! Form::select('wallet_fee_id', $walletfees, null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
                                        @if ($errors->has('wallet_fee_id')) <p class="help-block">{{ $errors->first('wallet_fee_id') }}</p> @endif
                                    </div>
									<div class="form-group @if ($errors->has('wallet_limit_id')) has-error @endif">
                                        <label>Wallet Limits</label>
										{!! Form::select('wallet_limit_id', $walletlimits, null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
                                        @if ($errors->has('wallet_limit_id')) <p class="help-block">{{ $errors->first('wallet_limit_id') }}</p> @endif
                                    </div>
                                    <div class="form-group @if ($errors->has('wallet_limit_id')) has-error @endif">
                                        <label>Wallet Type</label>
                                        {!! Form::select('wallet_type_id', $wallettypes, null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
                                        @if ($errors->has('wallet_type_id')) <p class="help-block">{{ $errors->first('wallet_type_id') }}</p> @endif
                                    </div>
                                    <div class="form-group">
                                        <span class="input-group-btn">
                                            {!! Form::submit('Edit', ['class' => 'btn btn-block blue btn-lg'] ) !!}
                                        </span>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            @include('admin/modules/accounts/includes/wallettabs')
			<div class="row">
				<div class="col-md-4">
					<table class="table table-hover table-light">
						<thead>
							<tr>
								<th></th>
								<th class="text-right" style="width:1%">Current</br>Balance</th>
								<th class="text-right" style="width:1%">Available</br>Balance</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>@if ($wallet->primary === '1') <span class="label label-sm label-default">Primary</span> @else @endif</td>
								<td class="text-right"><span class="font-green-jungle" style="font-size:22px;font-weight:300;">{!! $wallet->balance !!}</span></br> <span class="font-sm">{!! $wallet->currency !!}</span></td>
								<td class="text-right"><span class="font-green-jungle" style="font-size:22px;font-weight:300;">{!! $wallet->balance !!}</span></br> <span class="font-sm">{!! $wallet->currency !!}</span></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="col-md-8">
					<div class="table-scrollable">
						<table class="table table-light table-hover">
							<tbody>
								<tr>
									<td style="width:30%">Compliance</td>
									<td><span class="label label-sm @if ($account->compliance === 'Approved') label-success @elseif ($account->compliance === 'Pending') label-info @elseif ($account->compliance === 'Declined') label-danger @else label-default @endif">{!! $account->compliance !!}</span></td>
								</tr>
								<tr>
									<td>+Wallet ID</td>
									<td>{!! $wallet->wallet_id !!}</td>
								</tr>
								<tr>
									<td>Type</td>
									<td><a href="/admin/wallettypes/{!! $wallet->wallet_type_id !!}">{!! $wallet->wallet_type_id !!}</a></td>
								</tr>
								<tr>
									<td>Fees</td>
									<td><a href="/admin/walletfees/{!! $wallet->wallet_fee_id !!}">{!! $wallet->wallet_fee_id !!}</a></td>
								</tr>
								<tr>
									<td>Limits</td>
									<td><a href="/admin/walletlimits/{!! $wallet->wallet_limit_id !!}">{!! $wallet->wallet_limit_id !!}</a></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->
@stop
