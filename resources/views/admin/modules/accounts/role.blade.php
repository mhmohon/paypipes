@extends('admin/html')

@section('content')
@include('admin/modules/accounts/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->
<div class="tabbable tabbable-tabdrop">
    @include('admin/modules/accounts/includes/accounttabs')
    <div class="tab-content">
        <div class="tab-pane active">
            @include('admin/modules/accounts/includes/usermanagementtabs')
            <h3 class="page-title">
                {!! $subpagetitle !!}
                <a class="btn blue bold" data-toggle="modal" href="#add">Edit <i class="fa fa-pencil"></i></a>
            </h3>
            <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Edit a Role</h4>
                        </div>
                        <div class="modal-body">															                           
                            {!! Form::model($account, ['method' => 'POST', 'action' => ['Admin\Modules\Accounts\AccountWhitelistaccountsController@store', $account->account_id]]) !!}
                                <div class="form-body">
                                    <div class="form-group @if ($errors->has('first_name')) has-error @endif">
                                        <label>First Name</label>
										{!! Form::text('first_name', null, array('class' => 'form-control form-control-solid placeholder-no-fix', 'placeholder' => 'First Name')) !!}
		                                @if ($errors->has('first_name')) <p class="help-block">{{ $errors->first('first_name') }}</p> @endif
                                    </div>                                   
                                    <div class="form-group @if ($errors->has('last_name')) has-error @endif">
                                        <label>Last Name</label>
										{!! Form::text('last_name', null, array('class' => 'form-control form-control-solid placeholder-no-fix', 'placeholder' => 'Last Name')) !!}
		                                @if ($errors->has('last_name')) <p class="help-block">{{ $errors->first('last_name') }}</p> @endif
                                    </div>
                                    <div class="form-group @if ($errors->has('email')) has-error @endif">
                                        <label>Email</label>
										{!! Form::text('email', null, array('class' => 'form-control form-control-solid placeholder-no-fix', 'placeholder' => 'Email')) !!}
		                                @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
                                    </div>
                                    <div class="form-group">
                                        <span class="input-group-btn">
                                            {!! Form::submit('Edit', ['class' => 'btn btn-block blue btn-lg'] ) !!}
                                        </span>
                                    </div>
                                </div>
                            {!! Form::close() !!}                   
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <div class="portlet-body">
				Role
			</div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->
@stop

