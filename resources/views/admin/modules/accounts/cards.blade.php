@extends('admin/html')

@section('content')
@include('admin/modules/accounts/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->
<div class="tabbable tabbable-tabdrop">
    @include('admin/modules/accounts/includes/accounttabs')
	<div class="tab-content">
		<div class="tab-pane active">
			<div class="tiles">
				@if (count($linked_cards) == 0)
				<p class="text-center font-grey-silver margin-top-15">There are no cards to display</p>
				@else
				@foreach ($linked_cards as $linked_card)
				<div class="tile bg-green-jungle double">
					<div class="tile-body">
						<div class="text-right"><img src="/assets/global/img/payments/active/{{ $linked_card->file_name }}"></div>
						<div style="margin-top:-60px;"><span style="font-size:22px;font-weight:300;">{!! $linked_card->card_type !!}*{!! $linked_card->card_digit !!}</span></div>
						<div class="tile-object">
							<div class="name">{!! $linked_card->card_holder !!}</div>
							<div class="number">{!! $linked_card->exp_month !!}/{!! $linked_card->exp_year !!}</div>
						</div>
					</div>
				</div>
				@endforeach
				@endif
			</div>
		</div>
	</div>
</div>
<!-- END PAGE CONTENT-->
@stop
