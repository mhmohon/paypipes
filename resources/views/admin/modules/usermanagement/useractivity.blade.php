@extends('admin/html')

@section('content')
@include('admin/modules/usermanagement/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->
<div class="tabbable tabbable-tabdrop">
    @include('admin/modules/usermanagement/includes/usertabs')
    <div class="tab-content">
        <div class="tab-pane active">	
			<div class="portlet-body">
				@if (count($activities) == 0)
				<p class="text-center font-grey-silver margin-top-15">There are no activities to display</p>
				@else
				<div class="row">
					<div class="col-sm-6">
						{!! Form::open() !!}
							<div class="input-group input-medium">
								<input class="form-control input-medium" placeholder="Search" type="text">
								<span class="input-group-btn">
									<button class="btn green-jungle" type="submit"><i class="icon-magnifier"></i></button>
								</span>
							</div>
						{!! Form::close() !!}
					</div>
					<!--
					<div class="col-sm-6 text-right" style="margin-top:5px"> 
						<a href="#" class="btn btn-sm dark"><i class="fa fa-print"></i> Print</a>
						<a href="#" class="btn btn-sm red"><i class="fa fa-file-pdf-o"></i> PDF</a>
						<a href="#" class="btn btn-sm green-jungle"><i class="fa fa-file-excel-o"></i> XML</a>
					</div>
					-->
				</div>
				<div class="table-scrollable">
					<table class="table table-condensed table-bordered table-striped table-hover">
						<thead>
							<tr>
								<th style="width:1%">#</th>
								<th>Activity</th>
								<th style="width:1%">When</th>
							</tr>
						</thead>
						<tbody>
							<?php $i = 0 ?>    
							@foreach ($activities as $activity)
							<?php $i++ ?>
							<tr>    
								<td>{!! $i !!}</td>
								<td>
									@if ($activity->module === 'Customer') <button class="btn btn-xs green-jungle"><i class="icon-users"></i></button>
									@elseif ($activity->module === 'Merchant') <button class="btn btn-xs blue"><i class="icon-screen-desktop"></i></button>
									@elseif ($activity->module === 'Partner') <button class="btn btn-xs green"><i class="icon-user-following"></i></button>
									@elseif ($activity->module === 'Wallet') <button class="btn btn-xs purple"><i class="icon-wallet"></i></button>
									@elseif ($activity->module === 'Payment') <button class="btn btn-xs blue"><i class="fa fa-credit-card"></i></button>
									@elseif ($activity->module === 'RiskManagement') <button class="btn btn-xs red-thunderbird"><i class="icon-shield"></i></button>
									@elseif ($activity->module === 'Gateway') <button class="btn btn-xs purple-medium"><i class="fa fa-plug"></i></button>
									@elseif ($activity->module === 'Invoice') <button class="btn btn-xs yellow-lemon"><i class="icon-paper-clip"></i></button>               
									@elseif ($activity->module === 'Setting') <button class="btn btn-xs dark"><i class="fa fa-cogs"></i></button>
									@elseif ($activity->module === 'Usermanagement') <button class="btn btn-xs yellow-lemon"><i class="icon-user"></i></button>
									@else 
									@endif
									{!! $activity->activity !!}
								</td>
								<td>{!! Carbon\Carbon::parse($activity->created_at)->diffForHumans() !!}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<div class="row">
					<div class="col-md-6 margin-top-10">
						Showing {!! $activities->firstItem() !!} to {!! $activities->lastItem() !!} of {!! $activities->total() !!}
					</div>
					<div class="col-md-6 text-right">
						{!! $activities->links() !!}
					</div>
				</div>
				@endif
			</div>
		</div>
	</div>
</div>
<!-- END PAGE CONTENT-->
@stop