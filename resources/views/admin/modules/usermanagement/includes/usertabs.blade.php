<ul class="nav nav-tabs">
    <li class="{{ Request::is('admin/usermanagement/users/'.$admin_id) ? 'active' : '' }}">
        <a href="/admin/usermanagement/users/{!! $admin_id !!}">User</a>
    </li>
    <li class="{{ Request::is('admin/usermanagement/users/'.$admin_id.'/useractivity') ? 'active' : '' }}">
        <a href="/admin/usermanagement/users/{!! $admin_id !!}/useractivity">User Activity</a>
    </li>
	@if (Auth::guard('admin')->user()->admin_id == $admin_id)
    <li class="{{ Request::is('admin/usermanagement/users/'.$admin_id.'/security') ? 'active' : '' }}">
        <a href="/admin/usermanagement/users/{!! $admin_id !!}/security">Security</a>
    </li>
	@endif
</ul>

