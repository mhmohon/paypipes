<ul class="nav nav-tabs">
    <li class="{{ Request::is('admin/usermanagement/userroles/'.$rid) ? 'active' : '' }}">
        <a href="/admin/usermanagement/userroles/{!! $rid !!}">User Role</a>
    </li>
    <li class="{{ Request::is('admin/usermanagement/userroles/'.$rid.'/users') ? 'active' : '' }}">
        <a href="/admin/usermanagement/userroles/{!! $rid !!}/users">Users</a>
    </li>							
</ul>

