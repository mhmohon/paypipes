@extends('admin/html')

@section('content')
@include('admin/modules/usermanagement/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->	
<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add a User</h4>
            </div>
            <div class="modal-body">															                           
                {!! Form::open(['url' => 'admin/usermanagement/users']) !!}
                    <div class="form-body">
                        <input name="_token" type="hidden" value="{{ csrf_token() }}"> 
                        <div class="form-group @if ($errors->has('first_name')) has-error @endif">
                            <div class="controls">
                                {!! Form::text('first_name', null, array('class' => 'form-control form-control-solid placeholder-no-fix', 'placeholder' => 'First Name')) !!}
                                @if ($errors->has('first_name')) <p class="help-block">{{ $errors->first('first_name') }}</p> @endif
                            </div>
                        </div>
						<div class="form-group @if ($errors->has('last_name')) has-error @endif">
                            <div class="controls">
                                {!! Form::text('last_name', null, array('class' => 'form-control form-control-solid placeholder-no-fix', 'placeholder' => 'Last Name')) !!}
                                @if ($errors->has('last_name')) <p class="help-block">{{ $errors->first('last_name') }}</p> @endif
                            </div>
                        </div>
                        <div class="form-group @if ($errors->has('email')) has-error @endif">
                            <div class="controls">
                                {!! Form::text('email', null, array('class' => 'form-control form-control-solid placeholder-no-fix', 'placeholder' => 'Email')) !!}
                                @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
                            </div>
                        </div>
                        <div class="form-group @if ($errors->has('email')) has-error @endif">
                            <div class="controls">
                                {!! Form::select('role', $roles, null, ['id' => 'role','class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
								@if ($errors->has('role')) <p class="help-block">{{ $errors->first('role') }}</p> @endif
                            </div>
                        </div>
                        <!--<div class="form-group @if ($errors->has('password')) has-error @endif">
                            <div class="controls">
                                {!! Form::password('password', array('class' => 'form-control form-control-solid placeholder-no-fix', 'placeholder' => 'Password')) !!}
                                @if ($errors->has('password')) <p class="help-block">{{ $errors->first('password') }}</p> @endif
                            </div>
                        </div>
                        <div class="form-group @if ($errors->has('password_confirmation')) has-error @endif">
                            <div class="controls">
                                {!! Form::password('password_confirmation', array('class' => 'form-control form-control-solid placeholder-no-fix', 'placeholder' => 'Re-type Your Password')) !!}
                                @if ($errors->has('password_confirmation')) <p class="help-block">{{ $errors->first('password_confirmation') }}</p> @endif
                            </div>
                        </div>-->
                        <div class="form-group">
                            <span class="input-group-btn">
                                {!! Form::submit('Add', ['class' => 'btn btn-block green-jungle btn-lg'] ) !!}
                            </span>
                        </div>
                    </div>
                {!! Form::close() !!}                   
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="portlet-body">
	@if (count($admins) == 0)
	<div class="row">
		<div class="col-sm-6">
			{!! Form::open(['url' => 'admin/usermanagement/users', 'method' => 'GET']) !!}
				<div class="input-group input-medium">
					{!! Form::text('search', Request::get('search'), ['class' => 'form-control input-medium', 'placeholder' => 'Search']) !!}
					<span class="input-group-btn">
						{!! Form::button('<i class="icon-magnifier"></i>', ['class' => 'btn green-jungle', 'type'=>'submit']) !!}
					</span>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
	<p class="text-center font-grey-silver margin-top-15">There are no users to display</p>
	@else
	<div class="row">
		<div class="col-sm-6">
			{!! Form::open(['url' => 'admin/usermanagement/users', 'method' => 'GET']) !!}
				<div class="input-group input-medium">
					{!! Form::text('search', Request::get('search'), ['class' => 'form-control input-medium', 'placeholder' => 'Search']) !!}
					<span class="input-group-btn">
						{!! Form::button('<i class="icon-magnifier"></i>', ['class' => 'btn green-jungle', 'type'=>'submit']) !!}
					</span>
				</div>
			{!! Form::close() !!}
		</div>
		<!--
		<div class="col-sm-6 text-right" style="margin-top:5px"> 
			<a href="#" class="btn btn-sm dark"><i class="fa fa-print"></i> Print</a>
			<a href="#" class="btn btn-sm red"><i class="fa fa-file-pdf-o"></i> PDF</a>
			<a href="#" class="btn btn-sm green-jungle"><i class="fa fa-file-excel-o"></i> XML</a>
		</div>
		-->
	</div>
	<div class="table-scrollable">
		<table class="table table-condensed table-bordered table-striped table-hover">
			<thead>
				<tr>
					<th style="width:1%">#</th>
					<th style="width:1%">Admin ID</th>
					<th style="width:1%">Name</th>
					<th>Email</th>
					<th style="width:1%">Status</th>
					<th style="width:1%">Action</th>
				</tr>
			</thead>
			<tbody>
				<?php $i = 0 ?>    
				@foreach ($admins as $admin)
				<?php $i++ ?>
				<tr>    
					<td>{!! $i !!}</td>
					<td>{!! $admin->admin_id !!}</td>
					<td>{!! $admin->name !!}</td>
					<td>{!! $admin->email !!}</td>
					<td><span class="label label-sm @if ($admin->status === 'Active') label-success @elseif ($admin->status === 'Inactive') label-default @elseif ($admin->status === 'Locked') label-danger @elseif ($admin->status === 'TempLock') label-warning @else label-default @endif">{!! $admin->status !!}</span></td>
					<td>
						<a href="/admin/usermanagement/users/{!! $admin->admin_id !!}" class="btn blue btn-xs"><i class="fa fa-pencil"></i></a>
						@if ($admin->status === 'Active' || $admin->status === 'Inactive')
							{!! Form::open(['method' => 'PATCH', 'action' => ['Admin\Modules\UserManagement\UsersController@lockunlock', $admin->admin_id], 'style' => 'display:inline-block;']) !!}
								{!! Form::hidden('status', 'Locked') !!}
								{!! Form::button('<i class="fa fa-lock"></i>', ['class' => 'btn red btn-xs', 'type'=>'submit', 'data-toggle' => 'confirmation', 'data-popout' => 'true', 'data-singleton' => 'true', 'data-original-title' => 'Lock! Are you sure?', 'title' => '']) !!}
							{!! Form::close() !!} 
						@elseif ($admin->status === 'Locked')
							{!! Form::open(['method' => 'PATCH', 'action' => ['Admin\Modules\UserManagement\UsersController@lockunlock', $admin->admin_id], 'style' => 'display:inline-block;']) !!}
								{!! Form::hidden('status', 'Active') !!}
								{!! Form::button('<i class="fa fa-unlock-alt"></i>', ['class' => 'btn green-jungle btn-xs', 'type'=>'submit', 'data-toggle' => 'confirmation', 'data-popout' => 'true', 'data-singleton' => 'true', 'data-original-title' => 'Unlock! Are you sure?', 'title' => '']) !!}
							{!! Form::close() !!} 
						@elseif ($admin->status === 'TempLock')
							<a href="javascript:;" class="btn default btn-xs"><i class="fa fa-unlock-alt"></i></a>
						@endif
					</td>									
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	<div class="row">
		<div class="col-md-6 margin-top-10">
			Showing {!! $admins->firstItem() !!} to {!! $admins->lastItem() !!} of {!! $admins->total() !!}
		</div>
		<div class="col-md-6 text-right">
			{!! $admins->links() !!}
		</div>
	</div>
	@endif
</div>
<!-- END PAGE CONTENT-->
@stop