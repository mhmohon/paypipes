@extends('admin/html')

@section('content')
@include('admin/modules/usermanagement/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->	
<div class="tabbable tabbable-tabdrop">
    @include('admin/modules/usermanagement/includes/userroletabs')
    <div class="tab-content">
        <div class="tab-pane active">
            <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h4 class="modal-title">Edit a User Role</h4>
                        </div>
                        <div class="modal-body"> 

                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <div class="table-scrollable">
                <table class="table table-light table-hover">
                    <tbody>
                        <tr>
                            <td style="width:30%">Title</td>
                            <td>{!! $role->name !!}</td>
                        </tr>
                        <tr>
                            <td>Status</td>
                            <td><span class="label label-sm @if ($role->status === 'Active') label-success @elseif ($role->status === 'Pending') label-info @elseif ($role->status === 'Locked') label-danger @else label-default @endif">{!! $role->status !!}</span></td>
                        </tr>
                        <tr>
                            <td style="vertical-align:top;">Permissions</td>
                            <td><div id="tree_2" class="tree-demo"> </div></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->
@stop