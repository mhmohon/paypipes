@extends('admin/html')

@section('content')
@include('admin/modules/usermanagement/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->	
<div class="tabbable tabbable-tabdrop">
    @include('admin/modules/usermanagement/includes/usertabs')
    <div class="tab-content">
        <div class="tab-pane active">
			<h4>
				Password
				<a class="btn green-jungle bold" data-toggle="modal" href="#password" style="margin:10px;">Change Password</a>
			</h4>
			<div class="modal fade" id="password" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Change Password</h4>
						</div>
						<div class="modal-body">															                           
							{!! Form::open(['method' => 'PATCH', 'action' => ['Admin\Modules\UserManagement\SecurityController@update', $admin_id ]]) !!}						
								<div class="form-body">
									<div class="form-group {{ $errors->has('old_password') ? ' has-error' : '' }}">
										<label>Current Password</label>
										{!! Form::password('old_password', array('placeholder'=>'Current Password', 'required'=>'required', 'class'=>'form-control')) !!}
										@if ($errors->has('old_password')) <p class="help-block">{{ $errors->first('old_password') }}</p>@endif
									</div>
									<div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
										<label>New Password</label>
										{!! Form::password('password', array('placeholder'=>'New Password', 'required'=>'required', 'class'=>'form-control')) !!}
										@if ($errors->has('password')) <p class="help-block">{{ $errors->first('password') }}</p>@endif
									</div>
									<div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
										<label>Repeat New Password</label>
										{!! Form::password('password_confirmation', array('placeholder'=>'Repeat New Password', 'required'=>'required', 'class'=>'form-control')) !!}
										@if ($errors->has('password_confirmation')) <p class="help-block">{{ $errors->first('password_confirmation') }}</p>@endif
									</div>
									<div class="form-group">
										<span class="input-group-btn">
											{!! Form::submit('Change Password', ['class' => 'btn btn-block green-jungle btn-lg'] ) !!}
										</span>
									</div>
								</div>
							{!! Form::close() !!}  
						</div>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<h4>
				Two Factor Authentication
				<a class="btn green-jungle bold" data-toggle="modal" href="#two_step" style="margin:10px;">Change Authentication</a>
			</h4>
			<div class="modal fade" id="two_step" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Two Step Validation</h4>
						</div>
						<div class="modal-body">															                           
							{!! Form::open(['method' => 'PATCH', 'action' => ['Admin\Modules\UserManagement\SecurityController@two_step', $admin_id ]]) !!}						
								<div class="form-body">
									<div class="form-group {{ $errors->has('old_password') ? ' has-error' : '' }}">
										<label>Two Step validation type</label>
										<select id='two_step_type' class='form-control' name='two_step_type'>
											<option value='no' {{Auth::guard('admin')->user()->two_step == 'no'?'selected':''}}>No</option>
											<option value='email' {{Auth::guard('admin')->user()->two_step == 'email'?'selected':''}}>Email</option>
											<option value='qr' {{Auth::guard('admin')->user()->two_step == 'qr'?'selected':''}}>Authenticator QR</option>
											<option value='ip' {{Auth::guard('admin')->user()->two_step == 'ip'?'selected':''}}>IP</option>
										</select>
										@if ($errors->has('two_step_type')) <p class="help-block">{{ $errors->first('two_step_type') }}</p>@endif
									</div>
									<!--<div class="form-group">
										<p>
										<input class='form-control' type='checkbox' name='send-qr' value="yes">
										Send Qr code to my Email
										</p>
									</div>-->

									<div class="form-group" style='display: none;' id='ip_div'>
										<label>Insert your IP address</label>
										<input class='form-control' type="text" name="ip_address" id='ip_address'>
										@if ($errors->has('ip_address')) <p class="help-block">{{ $errors->first('ip_address') }}</p>@endif
									</div>
									
									<div class="form-group">
										<span class="input-group-btn">
											{!! Form::submit('Submit', ['class' => 'btn btn-block green-jungle btn-lg'] ) !!}
										</span>
									</div>
								</div>
							{!! Form::close() !!}  
						</div>
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
			<div class="clearfix"></div>
		</div>
    </div>
</div>
@push('scripts')
<script type="text/javascript">
  $(document).on('change', '#two_step_type', function(e){
  	$tw = $('#two_step_type').val(); 
    if($tw == 'ip'){
    	$('#ip_div').show();
    	$('#ip_address').attr('required', 'true');
    }else{
    	$('#ip_div').hide();
    	$('#ip_address').removeAttr('required');
    }
  });
</script>
@endpush
<!-- END PAGE CONTENT-->
@stop