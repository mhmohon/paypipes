@extends('admin/html')

@section('content')
@include('admin/modules/usermanagement/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->	
<div class="tabbable tabbable-tabdrop">
    @include('admin/modules/usermanagement/includes/usertabs')
    <div class="tab-content">
        <div class="tab-pane active">
            <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                    <h4 class="modal-title">Edit a User</h4>
                            </div>
                            <div class="modal-body"> 
                                {!! Form::model($admin, ['method' => 'POST', 'action' => ['Admin\Modules\UserManagement\UsersController@update', $admin->id ]]) !!}
                                    <div class="form-body">
                                        <div class="form-group @if ($errors->has('name')) has-error @endif">
                                            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name']) !!}
                                            @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
                                        </div>
                                        <div class="form-group @if ($errors->has('role')) has-error @endif">
                                            {!! Form::text('role', null, ['class' => 'form-control', 'placeholder' => 'User Role']) !!} 
                                            @if ($errors->has('role')) <p class="help-block">{{ $errors->first('role') }}</p> @endif
                                        </div>
                                        <div class="form-group">
                                            <span class="input-group-btn">
                                                {!! Form::submit('Edit', ['class' => 'btn btn-block green-jungle btn-lg'] ) !!}
                                            </span>
                                        </div>
                                    </div>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
            <div class="table-scrollable">
                <table class="table table-light table-hover">
                    <tbody>
                        <tr>
                            <td>Status</td>
                            <td><span class="label label-sm @if ($admin->status === 'Active') label-success @elseif ($admin->status === 'Pending') label-info @elseif ($admin->status === 'Locked') label-danger @else label-default @endif">{!! $admin->status !!}</span></td>
                        </tr>
                        <tr>
                            <td>Name</td>
                            <td>{!! $admin->name !!}</td>
                        </tr>
                        <tr>
                            <td>Email</td>
                            <td>{!! $admin->email !!}</td>
                        </tr>
                        <tr>
                            <td>Started on</td>
                            <td>{!! Carbon\Carbon::parse($admin->created_at)->format('jS \o\f F, Y') !!}</td>
                        </tr>
						<tr>
							<td>Added By</td>
							<td>{!! $admin->added_by !!} {!! Carbon\Carbon::parse($admin->created_at)->diffForHumans() !!}</td>
						</tr>
						<tr>
							<td>Updated By</td>
							<td>{!! $admin->updated_by !!} {!! Carbon\Carbon::parse($admin->updated_at)->diffForHumans() !!}</td>
						</tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->
@stop