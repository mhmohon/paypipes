@extends('admin/html')

@section('content')
@include('admin/modules/roles/includes/pageheader')
<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add a Role</h4>
            </div>
            <div class="modal-body">                                                         
                {!! Form::open(['url' => 'admin/roles']) !!}
                    <div class="form-body">
                        <div class="form-group @if ($errors->has('name')) has-error @endif">
                            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name']) !!}
                            @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
                        </div>  
                        <div class="form-group @if ($errors->has('display_name')) has-error @endif">
                            {!! Form::text('display_name', null, ['class' => 'form-control', 'placeholder' => 'Display Name']) !!}
                            @if ($errors->has('display_name')) <p class="help-block">{{ $errors->first('display_name') }}</p> @endif
                        </div> 
                        <div class="form-group @if ($errors->has('email')) has-error @endif">
                            {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Description']) !!}
                            @if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
                        </div>
                        <div class="form-group">
                            <label class="control-label label-required">Permissions</label>
                            <div class="col-sm-12">
                                <table class="table">
                        <tbody>
                        @foreach($permissions as $pr_name => $ar)
                            <tr style="padding: 0px;">
                                <td style="padding: 0px;">{{ucfirst(str_replace("_"," ",$pr_name))}} : </td>
                                @foreach($ar as $permission)
                                    <td style="padding: 0px;"><input type="checkbox" name="permission[]" value="{{ $permission['id'] }}"> {{ucfirst(str_replace("_"," ",$permission['name']))}}</td>
                                @endforeach
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                            </div>
                        </div>           
                                    
                        <div class="form-group">
                            <span class="input-group-btn">
                                {!! Form::submit('Add', ['class' => 'btn btn-block green-jungle btn-lg'] ) !!}
                            </span>
                        </div>
                    </div>
                {!! Form::close() !!}                   
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- BEGIN PAGE CONTENT-->  
<div class="portlet-body">
  @if (count($roles) == 0)
  <p class="text-center font-grey-silver margin-top-15">There are no Roles to display</p>
  @else
  <div class="row">
    <div class="col-sm-6">
      {!! Form::open(['action' => 'Admin\Modules\Roles\RolesController@index', 'method' => 'GET']) !!}
        <div class="input-group input-medium">
          {!! Form::text('search', Request::get('search'), ['class' => 'form-control input-medium', 'placeholder' => 'Search']) !!}
          <span class="input-group-btn">
            {!! Form::button('<i class="icon-magnifier"></i>', ['class' => 'btn green-jungle', 'type'=>'submit']) !!}
          </span>
        </div>
      {!! Form::close() !!}
    </div>
	<!--
    <div class="col-sm-6 text-right" style="margin-top:5px"> 
      <a href="#" class="btn btn-sm dark"><i class="fa fa-print"></i> Print</a>
      <a href="#" class="btn btn-sm red"><i class="fa fa-file-pdf-o"></i> PDF</a>
      <a href="#" class="btn btn-sm green-jungle"><i class="fa fa-file-excel-o"></i> XML</a>
    </div>
	-->
  </div>
  <div class="table-scrollable">
    <table class="table table-condensed table-bordered table-striped table-hover">
      <thead>
        <tr>
          <th style="width:1%">#</th>
          <th>Name</th>
          <th>Display Name</th>
          <th>Description</th>
          <th style="width:1%">Status</th>
          <th style="width:1%">Action</th>
        </tr>
      </thead>
      <tbody>
        <?php $i = 0 ?>    
        @foreach ($roles as $role)
        <?php $i++ ?>
        <tr>                                                             
          <td>{!! $i !!}</td>
          <td>{!! $role->name !!}</td>
          <td>{!! $role->display_name !!}</td>
          <td>{!! $role->description !!}</td>
          <td><span class="label label-sm @if ($role->status === 'Active') label-success @elseif ($role->status === 'Inactive') label-default @elseif ($role->status === 'Locked') label-danger @else label-default @endif">{!! $role->status !!}</span></td> 
          <td>
            <a href="/admin/roles/{!! $role->id !!}" class="btn blue btn-xs"><i class="fa fa-pencil"></i></a>
          </td>                                                                                                 
        </tr>
        @endforeach
      </tbody>
    </table>
  </div>
  <div class="row">
    <div class="col-md-6 margin-top-10">
      Showing {!! $roles->firstItem() !!} to {!! $roles->lastItem() !!} of {!! $roles->total() !!}
    </div>
    <div class="col-md-6 text-right">
      {!! $roles->links() !!}
    </div>
  </div>
  @endif
</div>
<!-- END PAGE CONTENT-->
@stop