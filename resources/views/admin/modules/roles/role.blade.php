@extends('admin/html')

@section('content')
@include('admin/modules/roles/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->  
                        
    {!! Form::model($role, ['method' => 'POST', 'action' => ['Admin\Modules\Roles\RolesController@update', $role->id ]]) !!}
        <div class="form-body">
            <div class="form-group @if ($errors->has('compliance')) has-error @endif">
                {!! Form::select('status', ['Inactive' => 'Inactive','Active' => 'Active'], null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
                @if ($errors->has('status')) <p class="help-block">{{ $errors->first('compliance') }}</p> @endif
            </div>
            <div class="form-group @if ($errors->has('display_name')) has-error @endif">
                {!! Form::text('display_name', null, ['class' => 'form-control', 'placeholder' => 'Display Name']) !!}
                @if ($errors->has('last_name')) <p class="help-block">{{ $errors->first('last_name') }}</p> @endif
            </div>
            <div class="form-group @if ($errors->has('email')) has-error @endif">
                {!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Description']) !!}
                @if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
            </div>
            <div class="form-group">
                <label class="control-label label-required">Permissions</label>
                <div class="col-sm-12">
                    <table class="table">
                        <tbody>
                        @foreach($permissions as $pr_name => $ar)
                            <tr>
                                <td>{{ucfirst(str_replace("_"," ",$pr_name))}} : </td>
                                @foreach($ar as $permission)
                                    <td><input type="checkbox" name="permission[]" value="{{ $permission['id'] }}" {{ (in_array($permission['id'], $role_permissions))?'checked':'' }}> {{ucfirst(str_replace("_"," ",$permission['name']))}}</td>
                                @endforeach
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                   
                </div>
                   
                
                
            </div>                    
        </div>
         {!! Form::submit('Edit', ['class' => 'btn btn-block green-jungle btn-lg'] ) !!}



    {!! Form::close() !!}
             
        
    
<!-- END PAGE CONTENT-->
@stop