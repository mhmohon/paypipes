<!-- BEGIN PAGE HEADER-->
<div class="page-bar">
    <h3 class="page-title" style="float:left;">{!! $pagetitle !!}</h3>
    @if (Request::is('admin/roles')) <a class="btn green-jungle bold" data-toggle="modal" href="#add" style="margin:10px;">Add <i class="fa fa-plus"></i></a>
    @endif
   <ul class="page-breadcrumb" style="float:right;padding-top:18px;">
        @if(isset($breadcrumb_level1) && !empty($breadcrumb_level1))
		<li>
            {!! $breadcrumb_level1 or '' !!}
			@if (Request::is('admin/roles')) <i class="fa fa-angle-right"></i>
			@elseif (Request::is('admin/roles/*')) <i class="fa fa-angle-right"></i>
			@endif
        </li>
		@endif
		@if(isset($breadcrumb_level2) && !empty($breadcrumb_level2))
        <li>
            {!! $breadcrumb_level2 or '' !!}
			@if (Request::is('admin/roles/*')) <i class="fa fa-angle-right"></i>
			@endif
        </li>
		@endif
		@if(isset($breadcrumb_level3) && !empty($breadcrumb_level3))
        <li>
            {!! $breadcrumb_level3 or '' !!}
        </li>
		@endif
    </ul>
</div>
<!-- END PAGE HEADER-->
