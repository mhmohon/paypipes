<!-- BEGIN PAGE HEADER-->
<div class="page-bar">
    <h3 class="page-title" style="float:left;">{!! $pagetitle !!}</h3>
    <ul class="page-breadcrumb" style="float:right;padding-top:18px;">
        <li><i class="icon-calendar"></i> {!! $today !!}</li>
    </ul>
</div>
<!-- END PAGE HEADER-->
