@extends('admin/html')

@section('content')
@include('admin/modules/dashboard/includes/pageheader')
<!-- BEGIN PAGE CONTENT	-->

<div class="row">
    <div class="col-md-6">
		<div class="portlet light bordered">
			<div class="portlet-title">
				<h4>Recent Activity</h4>
			</div>
			<div class="portlet-body">
				<div class="scroller" style="height: 250px;" data-always-visible="1" data-rail-visible="0">
					@foreach ($activities as $activity)
					<ul class="feeds">
						<li>
							<div class="col1">
								<div class="cont">
									<div class="cont-col1">                                        
										@if ($activity->module === 'Account') <button class="btn btn-xs green-jungle"><i class="icon-users"></i></button>
										@elseif ($activity->module === 'Partner') <button class="btn btn-xs green"><i class="icon-user-following"></i></button>
										@elseif ($activity->module === 'Payment') <button class="btn btn-xs blue"><i class="fa fa-credit-card"></i></button>
										@elseif ($activity->module === 'RiskManagement') <button class="btn btn-xs red-thunderbird"><i class="icon-shield"></i></button>
										@elseif ($activity->module === 'Gateway') <button class="btn btn-xs purple-medium"><i class="fa fa-plug"></i></button>
										@elseif ($activity->module === 'Invoice') <button class="btn btn-xs yellow-lemon"><i class="icon-paper-clip"></i></button>
										@elseif ($activity->module === 'Setting') <button class="btn btn-xs dark"><i class="fa fa-cogs"></i></button>
										@elseif ($activity->module === 'Usermanagement') <button class="btn btn-xs yellow-lemon"><i class="icon-user"></i></button>
										@elseif ($activity->module === 'payments') <button class="btn btn-xs yellow-soft"><i class="icon-docs"></i></button>
										@else 
										@endif
									</div>
									<div class="cont-col2">
										<div class="desc"> {!! $activity->activity !!} </div>
									</div>
								</div>
							</div>
							<div class="col2">
								<div class="date"> {!! $activity->created_at->diffForHumans() !!}</div>
							</div>
						</li>
					</ul>
					@endforeach
				</div>
				<div class="scroller-footer">
					<div class="btn-arrow-link pull-right">
						<a href="/admin/activitylog">See All Activity</a>
						<i class="icon-arrow-right"></i>
					</div>
				</div>
			</div>
		</div>
    </div>
	<div class="col-md-6">
		<div class="tiles">
			<a href="/admin/usermanagement/users/{!! Auth::guard('admin')->user()->admin_id !!}" class="tile double bg-yellow-lemon">
				<div class="tile-body">
					<i class="icon-user pull-right"></i>
					<h3>{!! Auth::guard('admin')->user()->name !!}</h3>
					<p>{!! Auth::guard('admin')->user()->email !!}</p>
				</div>
				<div class="tile-object">
					<div class="name">
						Last login
					</div>
					<div class="number"> {!! Auth::guard('admin')->user()->updated_at->diffForHumans() !!} </div>
				</div>
			</a>
			<a href="/admin/customers" class="tile bg-green-jungle">
				<div class="tile-body">
					<i class="icon-users"></i>
				</div>
				<div class="tile-object">
					<div class="name"> Accounts </div>
					<div class="number"> {!! $accountscount !!} </div>
				</div>
			</a>
			
			<a href="/admin/transactions" class="tile bg-blue">
				<div class="tile-body">
					<i class="fa fa-credit-card"></i>
				</div>
				<div class="tile-object">
					<div class="name"> Payments </div>
					<div class="number"> {!! $paymentscount !!} </div>
				</div>
			</a>
			<a href="/admin/paymentmethods" class="tile bg-blue-madison">
				<div class="tile-body">
					<i class="fa fa-plug"></i>
				</div>
				<div class="tile-object">
					<div class="name"> Gateways </div>
					<div class="number"> {!! $gatewayscount !!} </div>
				</div>
			</a>
			<a href="/admin/activitylog" class="tile bg-yellow-casablanca">
				<div class="tile-body">
					<i class="fa fa-book"></i>
				</div>
				<div class="tile-object">
					<div class="name"> Activity Log </div>
					<div class="number"> </div>
				</div>
			</a>
			<a href="/admin/settings" class="tile bg-dark">
				<div class="tile-body">
					<i class="fa fa-cogs"></i>
				</div>
				<div class="tile-object">
					<div class="name"> Settings </div>
					<div class="number"> </div>
				</div>
			</a>			
		</div>
    </div>
</div>
<!-- END PAGE CONTENT-->
@stop