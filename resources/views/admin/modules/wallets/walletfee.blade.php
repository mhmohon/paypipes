@extends('admin/html')

@section('content')
@include('admin/modules/wallets/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Edit a Wallet Fees</h4>
			</div>
			<div class="modal-body">															                           
				{!! Form::model($walletfee, ['method' => 'POST', 'action' => ['Admin\Modules\Wallets\WalletfeesController@update', $walletfee->wallet_fee_id ]]) !!}
					<div class="form-body">						
						<div class="form-group @if ($errors->has('name')) has-error @endif">
							<label>Wallet Fees Name</label>
							{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Wallet Fees Name']) !!}
							@if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
						</div>
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="@if ($errors->has('creation_fee')) has-error @endif">
										<label>Creation Fee</label>
										{!! Form::text('creation_fee', null, ['class' => 'form-control', 'placeholder' => 'Creation Fee']) !!}
										@if ($errors->has('creation_fee')) <p class="help-block">{{ $errors->first('creation_fee') }}</p> @endif
									</div>
								</div>
                                <div class="col-md-6">
                                    <div class="@if ($errors->has('monthly_fee')) has-error @endif">
										<label>Monthly Fee</label>
										{!! Form::text('monthly_fee', null, ['class' => 'form-control', 'placeholder' => 'Monthly Fee']) !!}
										@if ($errors->has('monthly_fee')) <p class="help-block">{{ $errors->first('monthly_fee') }}</p> @endif
									</div>
                                </div>												
                            </div>							
                        </div>
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="@if ($errors->has('send_fee')) has-error @endif">
										<label>Send Fee %</label>
										{!! Form::text('send_fee', null, ['class' => 'form-control', 'placeholder' => 'Send Fee %']) !!}
										@if ($errors->has('send_fee')) <p class="help-block">{{ $errors->first('send_fee') }}</p> @endif
									</div>
								</div>
                                <div class="col-md-6">
                                    <div class="@if ($errors->has('send_flat_fee')) has-error @endif">
										<label>Send Flat Fee</label>
										{!! Form::text('send_flat_fee', null, ['class' => 'form-control', 'placeholder' => 'Send Flat Fee']) !!}
										@if ($errors->has('send_flat_fee')) <p class="help-block">{{ $errors->first('send_flat_fee') }}</p> @endif
									</div>
                                </div>												
                            </div>							
                        </div>
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="@if ($errors->has('receive_fee')) has-error @endif">
										<label>Receive Fee %</label>
										{!! Form::text('receive_fee', null, ['class' => 'form-control', 'placeholder' => 'Receive Fee %']) !!}
										@if ($errors->has('receive_fee')) <p class="help-block">{{ $errors->first('receive_fee') }}</p> @endif
									</div>
								</div>
                                <div class="col-md-6">
                                    <div class="@if ($errors->has('receive_flat_fee')) has-error @endif">
										<label>Receive Flat Fee</label>
										{!! Form::text('receive_flat_fee', null, ['class' => 'form-control', 'placeholder' => 'Receive Flat Fee']) !!}
										@if ($errors->has('receive_flat_fee')) <p class="help-block">{{ $errors->first('receive_flat_fee') }}</p> @endif
									</div>
                                </div>												
                            </div>							
                        </div>
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="@if ($errors->has('withdraw_fee')) has-error @endif">
										<label>Withdrawal Fee %</label>
										{!! Form::text('withdraw_fee', null, ['class' => 'form-control', 'placeholder' => 'Withdrawal Fee %']) !!}
										@if ($errors->has('withdraw_fee')) <p class="help-block">{{ $errors->first('withdraw_fee') }}</p> @endif
									</div>
								</div>
                                <div class="col-md-6">
                                    <div class="@if ($errors->has('withdraw_flat_fee')) has-error @endif">
										<label>Withdrawal Flat Fee</label>
										{!! Form::text('withdraw_flat_fee', null, ['class' => 'form-control', 'placeholder' => 'Withdrawal Flat Fee']) !!}
										@if ($errors->has('withdraw_flat_fee')) <p class="help-block">{{ $errors->first('withdraw_flat_fee') }}</p> @endif
									</div>
                                </div>												
                            </div>							
                        </div>
						<div class="form-group">
							<span class="input-group-btn">
								{!! Form::submit('Edit', ['class' => 'btn btn-block green-jungle btn-lg'] ) !!}
							</span>
						</div>
					</div>
				{!! Form::close() !!}                   
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<div class="table-scrollable">
	<table class="table table-light table-hover">
		<tbody>
			<tr>
				<td style="width:30%">Status</td>
				<td><span class="label label-sm @if ($walletfee->status === 'Active') label-success @elseif ($walletfee->status === 'Inactive') label-default @else label-default @endif">{!! $walletfee->status !!}</span></td>
			</tr>
			<tr>
				<td>Wallet Fee Name</td>
				<td>{!! $walletfee->name !!}</td>
			</tr>
			<tr>
				<td>Currency</td>
				<td>{!! $walletfee->currency !!}</td>
			</tr>
			<tr>
				<td>Creation Fee</td>
				<td>{!! $walletfee->creation_fee !!} @if ($walletfee->currency === 'EUR') € @elseif ($walletfee->currency === 'USD') $ @elseif ($walletfee->currency === 'GBP') £ @else @endif</td>
			</tr>
			<tr>
				<td>Monthly Fee</td>
				<td>{!! $walletfee->monthly_fee !!} @if ($walletfee->currency === 'EUR') € @elseif ($walletfee->currency === 'USD') $ @elseif ($walletfee->currency === 'GBP') £ @else @endif</td>
			</tr>
			<tr>
				<td>Send Fee</td>
				<td>{!! $walletfee->send_fee !!} %</td>
			</tr>
			<tr>
				<td>Send Flat Fee</td>
				<td>{!! $walletfee->send_flat_fee !!} %</td>
			</tr>
			<tr>
				<td>Receive Fee</td>
				<td>{!! $walletfee->receive_fee !!} %</td>
			</tr>
			<tr>
				<td>Receive Flat Fee</td>
				<td>{!! $walletfee->receive_flat_fee !!} @if ($walletfee->currency === 'EUR') € @elseif ($walletfee->currency === 'USD') $ @elseif ($walletfee->currency === 'GBP') £ @else @endif</td>
			</tr>
			<tr>
				<td>Withdraw Fee</td>
				<td>{!! $walletfee->withdraw_flat_fee !!} %</td>
			</tr>
			<tr>
				<td>Withdraw Flat Fee</td>
				<td>{!! $walletfee->withdraw_flat_fee !!} @if ($walletfee->currency === 'EUR') € @elseif ($walletfee->currency === 'USD') $ @elseif ($walletfee->currency === 'GBP') £ @else @endif</td>
			</tr>
		</tbody>
	</table>
</div>
<!-- END PAGE CONTENT-->
@stop

