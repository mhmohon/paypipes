@extends('admin/html')

@section('content')
@include('admin/modules/wallets/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Edit a Wallet Type</h4>
			</div>
			<div class="modal-body">															                           
				{!! Form::model($wallettype, ['method' => 'POST', 'action' => ['Admin\Modules\Wallets\WallettypesController@update', $wallettype->wallet_type_id ]]) !!}
					<div class="form-body">
						<div class="form-group @if ($errors->has('name')) has-error @endif">
							{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Wallet Type Name']) !!}
							@if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
						</div>
						<h4>Enable/Disable +Wallet Actions</h4>
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="btn blue-madison btn-block margin-bottom-5">Add +Wallet</div>
									<div class="form-group @if ($errors->has('add_wallet')) has-error @endif">
										{!! Form::select('add_wallet', ['1' => 'Enable', '0' => 'Disable'], null, ['class' => 'bs-select form-control']) !!}
										@if ($errors->has('add_wallet')) <p class="help-block">{{ $errors->first('add_wallet') }}</p> @endif
									</div>
								</div>
								<div class="col-md-6">
									<div class="btn blue-madison btn-block margin-bottom-5">Add +Wallet Currency</div>
									<div class="form-group @if ($errors->has('add_currency')) has-error @endif">
										{!! Form::select('add_currency', ['1' => 'Enable', '0' => 'Disable'], null, ['class' => 'bs-select form-control']) !!}
										@if ($errors->has('add_currency')) <p class="help-block">{{ $errors->first('add_currency') }}</p> @endif
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
									<div class="light margin-bottom-10" style="padding:0;">
										<div class="btn blue-madison btn-block">
											<div class="text-center"  style="font-size:42px;"><i class="icon-action-redo"></i></div>
											<div class="text-center"  style="font-size:18px;font-weight:300;">Send</div>
										</div>
									</div>
									<div class="form-group @if ($errors->has('send')) has-error @endif">
										{!! Form::select('send', ['1' => 'Enable', '0' => 'Disable'], null, ['class' => 'bs-select form-control']) !!}
										@if ($errors->has('send')) <p class="help-block">{{ $errors->first('send') }}</p> @endif
									</div>
								</div>
                                <div class="col-md-4">
									<div class="light margin-bottom-10" style="padding:0;">
										<div class="btn blue-madison btn-block">
											<div class="text-center"  style="font-size:42px;"><i class="icon-basket-loaded"></i></div>
											<div class="text-center"  style="font-size:18px;font-weight:300;">Pay</div>
										</div>
									</div>
                                    <div class="form-group @if ($errors->has('pay')) has-error @endif">
										{!! Form::select('pay', ['1' => 'Enable', '0' => 'Disable'], null, ['class' => 'bs-select form-control']) !!}
										@if ($errors->has('pay')) <p class="help-block">{{ $errors->first('pay') }}</p> @endif
									</div>
                                </div>
								<div class="col-md-4">
									<div class="light margin-bottom-10" style="padding:0;">
										<div class="btn blue-madison btn-block">
											<div class="text-center"  style="font-size:42px;"><i class="icon-action-undo"></i></div>
											<div class="text-center"  style="font-size:18px;font-weight:300;">Request</div>
										</div>
									</div>
                                    <div class="form-group @if ($errors->has('request')) has-error @endif">
										{!! Form::select('request', ['1' => 'Enable', '0' => 'Disable'], null, ['class' => 'bs-select form-control']) !!}
										@if ($errors->has('request')) <p class="help-block">{{ $errors->first('request') }}</p> @endif
									</div>
                                </div>								
                            </div>							
                        </div>
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="btn blue-madison btn-block margin-bottom-5">Add Money</div>
									<div class="form-group @if ($errors->has('add')) has-error @endif">
										{!! Form::select('add', ['1' => 'Enable', '0' => 'Disable'], null, ['class' => 'bs-select form-control']) !!}
										@if ($errors->has('add')) <p class="help-block">{{ $errors->first('add') }}</p> @endif
									</div>
								</div>
								<div class="col-md-6">
									<div class="btn blue-madison btn-block margin-bottom-5">Withdraw Money</div>
									<div class="form-group @if ($errors->has('withdraw')) has-error @endif">
										{!! Form::select('withdraw', ['1' => 'Enable', '0' => 'Disable'], null, ['class' => 'bs-select form-control']) !!}
										@if ($errors->has('withdraw')) <p class="help-block">{{ $errors->first('withdraw') }}</p> @endif
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="btn blue-madison btn-block margin-bottom-5">Transfer between Currencies</div>
									<div class="form-group @if ($errors->has('transfer')) has-error @endif">
										{!! Form::select('transfer', ['1' => 'Enable', '0' => 'Disable'], null, ['class' => 'bs-select form-control']) !!}
										@if ($errors->has('transfer')) <p class="help-block">{{ $errors->first('transfer') }}</p> @endif
									</div>
								</div>
								<div class="col-md-6">
									<div class="btn blue-madison btn-block margin-bottom-5">Upgrade +Wallet</div>
									<div class="form-group @if ($errors->has('upgrade')) has-error @endif">
										{!! Form::select('upgrade', ['1' => 'Enable', '0' => 'Disable'], null, ['class' => 'bs-select form-control']) !!}
										@if ($errors->has('upgrade')) <p class="help-block">{{ $errors->first('upgrade') }}</p> @endif
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<span class="input-group-btn">
								{!! Form::submit('Edit', ['class' => 'btn btn-block green-jungle btn-lg'] ) !!}
							</span>
						</div>
					</div>
				{!! Form::close() !!}                   
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<div class="table-scrollable">
	<table class="table table-light table-hover">
		<tbody>
			<tr>
				<td style="width:30%">Status</td>
				<td><span class="label label-sm @if ($wallettype->status === 'Active') label-success @elseif ($wallettype->status === 'Inactive') label-default @else label-default @endif">{!! $wallettype->status !!}</span></td>
			</tr>
			<tr>
				<td>Wallet Type Name</td>
				<td>{!! $wallettype->name !!}</td>
			</tr>
			<tr>
				<td>Add +Wallet</td>
				<td>{!! $wallettype->add_wallet !!}</td>
			</tr>
			<tr>
				<td>Add +Wallet Currency</td>
				<td>{!! $wallettype->add_currency !!}</td>
			</tr>
			<tr>
				<td>Send</td>
				<td>{!! $wallettype->send !!}</td>
			</tr>
			<tr>
				<td>Pay</td>
				<td>{!! $wallettype->pay !!}</td>
			</tr>
			<tr>
				<td>Request</td>
				<td>{!! $wallettype->request !!}</td>
			</tr>
			<tr>
				<td>Add Money</td>
				<td>{!! $wallettype->add !!}</td>
			</tr>
			<tr>
				<td>Withdraw Money</td>
				<td>{!! $wallettype->withdraw !!}</td>
			</tr>
			<tr>
				<td>Transfer between Currencies</td>
				<td>{!! $wallettype->transfer !!}</td>
			</tr>
			<tr>
				<td>Upgrade</td>
				<td>{!! $wallettype->upgrade !!}</td>
			</tr>
		</tbody>
	</table>
</div>
<!-- END PAGE CONTENT-->
@stop

