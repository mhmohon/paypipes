@extends('admin/html')

@section('content')
@include('admin/modules/wallets/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->
<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Add a Wallet Limits</h4>
			</div>
			<div class="modal-body">
				{!! Form::open(['url' => 'admin/walletlimits']) !!}
					<div class="form-body">
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="@if ($errors->has('name')) has-error @endif">
										<label>Wallet Limits Name</label>
										{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Wallet Limits Name']) !!}
										@if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
									</div>
								</div>
                                <div class="col-md-6">
                                    <div class="@if ($errors->has('currency')) has-error @endif">
										<label>Currency</label>
										{!! Form::select('currency', $currencies, null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
										@if ($errors->has('currency')) <p class="help-block">{{ $errors->first('currency') }}</p> @endif
									</div>
                                </div>
                            </div>
                        </div>
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="@if ($errors->has('max_balance')) has-error @endif">
										<label>Maximum Balance</label>
										{!! Form::text('max_balance', null, ['class' => 'form-control', 'placeholder' => 'Maximum Balance']) !!}
										@if ($errors->has('max_balance')) <p class="help-block">{{ $errors->first('max_balance') }}</p> @endif
									</div>
								</div>
                                <div class="col-md-6">
                                    <div class="@if ($errors->has('max_turnover')) has-error @endif">
										<label>Maximum Turnover</label>
										{!! Form::text('max_turnover', null, ['class' => 'form-control', 'placeholder' => 'Maximum Turnover']) !!}
										@if ($errors->has('max_turnover')) <p class="help-block">{{ $errors->first('max_turnover') }}</p> @endif
									</div>
                                </div>
                            </div>
                        </div>
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="@if ($errors->has('max_turnover_per_month')) has-error @endif">
										<label>Maximum Turnover per Month</label>
										{!! Form::text('max_turnover_per_month', null, ['class' => 'form-control', 'placeholder' => 'Maximum Turnover per Month']) !!}
										@if ($errors->has('max_turnover_per_month')) <p class="help-block">{{ $errors->first('max_turnover_per_month') }}</p> @endif
									</div>
								</div>
                                <div class="col-md-6">
                                    <div class="@if ($errors->has('max_credit_amount_per_day')) has-error @endif">
										<label>Maximum Credit Amount per Day</label>
										{!! Form::text('max_credit_amount_per_day', null, ['class' => 'form-control', 'placeholder' => 'Maximum Credit Amount per Day']) !!}
										@if ($errors->has('max_credit_amount_per_day')) <p class="help-block">{{ $errors->first('max_credit_amount_per_day') }}</p> @endif
									</div>
                                </div>
                            </div>
                        </div>
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="@if ($errors->has('max_credit_payments_per_day')) has-error @endif">
										<label>Maximum Credit Payments per Day</label>
										{!! Form::text('max_credit_payments_per_day', null, ['class' => 'form-control', 'placeholder' => 'Maximum Credit Payments per Day']) !!}
										@if ($errors->has('max_credit_payments_per_day')) <p class="help-block">{{ $errors->first('max_credit_payments_per_day') }}</p> @endif
									</div>
								</div>
                                <div class="col-md-6">
                                    <div class="@if ($errors->has('max_topup_amount_per_day')) has-error @endif">
										<label>Maximum Topup Amount per Day</label>
										{!! Form::text('max_topup_amount_per_day', null, ['class' => 'form-control', 'placeholder' => 'Maximum Topup Amount per Day']) !!}
										@if ($errors->has('max_topup_amount_per_day')) <p class="help-block">{{ $errors->first('max_topup_amount_per_day') }}</p> @endif
									</div>
                                </div>
                            </div>
                        </div>
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="@if ($errors->has('max_topup_payments_per_day')) has-error @endif">
										<label>Maximum Topup Payments per Day</label>
										{!! Form::text('max_topup_payments_per_day', null, ['class' => 'form-control', 'placeholder' => 'Maximum Topup Payments per Day']) !!}
										@if ($errors->has('max_topup_payments_per_day')) <p class="help-block">{{ $errors->first('max_topup_payments_per_day') }}</p> @endif
									</div>
								</div>
                                <div class="col-md-6">
                                    <div class="@if ($errors->has('max_topup_amount_per_payment')) has-error @endif">
										<label>Maximum Topup Amount per Payment</label>
										{!! Form::text('max_topup_amount_per_payment', null, ['class' => 'form-control', 'placeholder' => 'Maximum Topup Amount per Payment']) !!}
										@if ($errors->has('max_topup_amount_per_payment')) <p class="help-block">{{ $errors->first('max_topup_amount_per_payment') }}</p> @endif
									</div>
                                </div>
                            </div>
                        </div>
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="@if ($errors->has('max_receive_amount_per_day')) has-error @endif">
										<label>Maximum Receive Amount per Day</label>
										{!! Form::text('max_receive_amount_per_day', null, ['class' => 'form-control', 'placeholder' => 'Maximum Receive Amount per Day']) !!}
										@if ($errors->has('max_receive_amount_per_day')) <p class="help-block">{{ $errors->first('max_receive_amount_per_day') }}</p> @endif
									</div>
								</div>
                                <div class="col-md-6">
                                    <div class="@if ($errors->has('max_receive_payments_per_day')) has-error @endif">
										<label>Maximum Receive Payments per Day</label>
										{!! Form::text('max_receive_payments_per_day', null, ['class' => 'form-control', 'placeholder' => 'Maximum Receive Payments per Day']) !!}
										@if ($errors->has('max_receive_payments_per_day')) <p class="help-block">{{ $errors->first('max_receive_payments_per_day') }}</p> @endif
									</div>
                                </div>
                            </div>
                        </div>
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="@if ($errors->has('max_receive_amount_per_payment')) has-error @endif">
										<label>Maximum Receive Amount per Payment</label>
										{!! Form::text('max_receive_amount_per_payment', null, ['class' => 'form-control', 'placeholder' => 'Maximum Receive Amount per Payment']) !!}
										@if ($errors->has('max_receive_amount_per_payment')) <p class="help-block">{{ $errors->first('max_receive_amount_per_payment') }}</p> @endif
									</div>
								</div>
                                <div class="col-md-6">
                                    <div class="@if ($errors->has('max_debit_amount_per_day')) has-error @endif">
										<label>Maximum Debit Amount per Day</label>
										{!! Form::text('max_debit_amount_per_day', null, ['class' => 'form-control', 'placeholder' => 'Maximum Debit Amount per Day']) !!}
										@if ($errors->has('max_debit_amount_per_day')) <p class="help-block">{{ $errors->first('max_debit_amount_per_day') }}</p> @endif
									</div>
                                </div>
                            </div>
                        </div>
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="@if ($errors->has('max_debit_payments_per_day')) has-error @endif">
										<label>Maximum Debit Payments per Day</label>
										{!! Form::text('max_debit_payments_per_day', null, ['class' => 'form-control', 'placeholder' => 'Maximum Debit Payments per Day']) !!}
										@if ($errors->has('max_debit_payments_per_day')) <p class="help-block">{{ $errors->first('max_debit_payments_per_day') }}</p> @endif
									</div>
								</div>
                                <div class="col-md-6">
                                    <div class="@if ($errors->has('max_send_amount_per_day')) has-error @endif">
										<label>Maximum Send Amount per Day</label>
										{!! Form::text('max_send_amount_per_day', null, ['class' => 'form-control', 'placeholder' => 'Maximum Send Amount per Day']) !!}
										@if ($errors->has('max_send_amount_per_day')) <p class="help-block">{{ $errors->first('max_send_amount_per_day') }}</p> @endif
									</div>
                                </div>
                            </div>
                        </div>
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="@if ($errors->has('max_send_payments_per_day')) has-error @endif">
										<label>Maximum Send Payments per Day</label>
										{!! Form::text('max_send_payments_per_day', null, ['class' => 'form-control', 'placeholder' => 'Maximum Send Payments per Day']) !!}
										@if ($errors->has('max_send_payments_per_day')) <p class="help-block">{{ $errors->first('max_send_payments_per_day') }}</p> @endif
									</div>
								</div>
                                <div class="col-md-6">
                                    <div class="@if ($errors->has('max_send_amount_per_payment')) has-error @endif">
										<label>Maximum Send Amount per Payment</label>
										{!! Form::text('max_send_amount_per_payment', null, ['class' => 'form-control', 'placeholder' => 'Maximum Send Amount per Payment']) !!}
										@if ($errors->has('max_send_amount_per_payment')) <p class="help-block">{{ $errors->first('max_send_amount_per_payment') }}</p> @endif
									</div>
                                </div>
                            </div>
                        </div>
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="@if ($errors->has('max_withdraw_amount_per_day')) has-error @endif">
										<label>Maximum Withdraw Amount per Day</label>
										{!! Form::text('max_withdraw_amount_per_day', null, ['class' => 'form-control', 'placeholder' => 'Maximum Withdraw Amount per Day']) !!}
										@if ($errors->has('max_withdraw_amount_per_day')) <p class="help-block">{{ $errors->first('max_withdraw_amount_per_day') }}</p> @endif
									</div>
								</div>
                                <div class="col-md-6">
                                    <div class="@if ($errors->has('max_withdraw_payments_per_day')) has-error @endif">
										<label>Maximum Withdraw Payments per Day</label>
										{!! Form::text('max_withdraw_payments_per_day', null, ['class' => 'form-control', 'placeholder' => 'Maximum Withdraw Payments per Day']) !!}
										@if ($errors->has('max_withdraw_payments_per_day')) <p class="help-block">{{ $errors->first('max_withdraw_payments_per_day') }}</p> @endif
									</div>
                                </div>
                            </div>
                        </div>
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="@if ($errors->has('max_withdraw_amount_per_payment')) has-error @endif">
										<label>Maximum Withdraw Amount per Payment</label>
										{!! Form::text('max_withdraw_amount_per_payment', null, ['class' => 'form-control', 'placeholder' => 'Maximum Withdraw Amount per Payment']) !!}
										@if ($errors->has('max_withdraw_amount_per_payment')) <p class="help-block">{{ $errors->first('max_withdraw_amount_per_payment') }}</p> @endif
									</div>
								</div>
                                <div class="col-md-6">
                                    <div class="@if ($errors->has('max_withdraw_amount')) has-error @endif">
										<label>Maximum Withdraw Amount</label>
										{!! Form::text('max_withdraw_amount', null, ['class' => 'form-control', 'placeholder' => 'Maximum Withdraw Amount']) !!}
										@if ($errors->has('max_withdraw_amount')) <p class="help-block">{{ $errors->first('max_withdraw_amount') }}</p> @endif
									</div>
                                </div>
                            </div>
                        </div>
						<div class="form-group">
							<span class="input-group-btn">
								{!! Form::submit('Add', ['class' => 'btn btn-block green-jungle btn-lg'] ) !!}
							</span>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<div class="portlet-body">
    @if (count($walletlimits) == 0)
	<p class="text-center font-grey-silver margin-top-15">There are no limits to display</p>
	@else
	<div class="row">
		<div class="col-sm-6">
			{!! Form::open() !!}
				<div class="input-group input-medium">
					<input class="form-control input-medium" placeholder="Search" type="text">
					<span class="input-group-btn">
						<button class="btn green-jungle" type="submit"><i class="icon-magnifier"></i></button>
					</span>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
	<div class="table-scrollable">
		<table class="table table-condensed table-bordered table-striped table-hover">
			<thead>
				<tr>
					<th style="width:1%">#</th>
					<th style="width:1%">Wallet</br>Limit ID</th>
					<th>Name</th>
					<th style="width:1%">Currency</th>
					<th style="width:1%">Maximum</br>Balance</th>
					<th style="width:1%">Maximum</br>Turnover</th>
					<th style="width:1%">Maximum</br>Monthly</br>Turnover</th>
					<th style="width:1%">Maximum</br>Withdrawal</th>
					<th style="width:1%">Status</th>
					<th style="width:1%">Action</th>
				</tr>
			</thead>
			<tbody>
				<?php $i = 0 ?>
				@foreach ($walletlimits as $walletlimit)
				<?php $i++ ?>
				<tr>
					<td>{!! $i !!}</td>
					<td>{!! $walletlimit->wallet_limit_id !!}</td>
					<td>{!! $walletlimit->name !!}</td>
					<td>{!! $walletlimit->currency !!}</td>
					<td>{!! $walletlimit->max_balance !!}</td>
					<td>{!! $walletlimit->max_turnover !!}</td>
					<td>{!! $walletlimit->max_turnover_per_month !!}</td>
					<td>{!! $walletlimit->max_withdraw_amount !!}</td>
					<td><span class="label label-sm @if ($walletlimit->status === 'Active') label-success @elseif ($walletlimit->status === 'Inactive') label-default @else label-default @endif">{!! $walletlimit->status !!}</span></td>
					<td>
						<a href="/admin/walletlimits/{!! $walletlimit->wallet_limit_id !!}" class="btn blue btn-xs"><i class="fa fa-pencil"></i></a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	<div class="row">
		<div class="col-md-6 margin-top-10">
			Showing {!! $walletlimits->firstItem() !!} to {!! $walletlimits->lastItem() !!} of {!! $walletlimits->total() !!}
		</div>
		<div class="col-md-6 text-right">
			{!! $walletlimits->links() !!}
		</div>
	</div>
	@endif
</div>
<!-- END PAGE CONTENT-->
@stop

