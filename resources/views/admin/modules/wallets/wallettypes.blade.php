@extends('admin/html')

@section('content')
@include('admin/modules/wallets/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->
<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Add a Wallet Type</h4>
			</div>
			<div class="modal-body">
				{!! Form::open(['url' => 'admin/wallettypes']) !!}
					<div class="form-body">
						<div class="form-group @if ($errors->has('name')) has-error @endif">
							{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Wallet Type Name']) !!}
							@if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
						</div>
						<h4>Enable/Disable +Wallet Actions</h4>
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="btn blue-madison btn-block margin-bottom-5">Add +Wallet</div>
									<div class="form-group @if ($errors->has('add_wallet')) has-error @endif">
										{!! Form::select('add_wallet', ['1' => 'Enable', '0' => 'Disable'], null, ['class' => 'bs-select form-control']) !!}
										@if ($errors->has('add_wallet')) <p class="help-block">{{ $errors->first('add_wallet') }}</p> @endif
									</div>
								</div>
								<div class="col-md-6">
									<div class="btn blue-madison btn-block margin-bottom-5">Add +Wallet Currency</div>
									<div class="form-group @if ($errors->has('add_currency')) has-error @endif">
										{!! Form::select('add_currency', ['1' => 'Enable', '0' => 'Disable'], null, ['class' => 'bs-select form-control']) !!}
										@if ($errors->has('add_currency')) <p class="help-block">{{ $errors->first('add_currency') }}</p> @endif
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-4">
									<div class="light margin-bottom-10" style="padding:0;">
										<div class="btn blue-madison btn-block">
											<div class="text-center"  style="font-size:42px;"><i class="icon-action-redo"></i></div>
											<div class="text-center"  style="font-size:18px;font-weight:300;">Send</div>
										</div>
									</div>
									<div class="form-group @if ($errors->has('send')) has-error @endif">
										{!! Form::select('send', ['1' => 'Enable', '0' => 'Disable'], null, ['class' => 'bs-select form-control']) !!}
										@if ($errors->has('send')) <p class="help-block">{{ $errors->first('send') }}</p> @endif
									</div>
								</div>
                                <div class="col-md-4">
									<div class="light margin-bottom-10" style="padding:0;">
										<div class="btn blue-madison btn-block">
											<div class="text-center"  style="font-size:42px;"><i class="icon-basket-loaded"></i></div>
											<div class="text-center"  style="font-size:18px;font-weight:300;">Pay</div>
										</div>
									</div>
                                    <div class="form-group @if ($errors->has('pay')) has-error @endif">
										{!! Form::select('pay', ['1' => 'Enable', '0' => 'Disable'], null, ['class' => 'bs-select form-control']) !!}
										@if ($errors->has('pay')) <p class="help-block">{{ $errors->first('pay') }}</p> @endif
									</div>
                                </div>
								<div class="col-md-4">
									<div class="light margin-bottom-10" style="padding:0;">
										<div class="btn blue-madison btn-block">
											<div class="text-center"  style="font-size:42px;"><i class="icon-action-undo"></i></div>
											<div class="text-center"  style="font-size:18px;font-weight:300;">Request</div>
										</div>
									</div>
                                    <div class="form-group @if ($errors->has('request')) has-error @endif">
										{!! Form::select('request', ['1' => 'Enable', '0' => 'Disable'], null, ['class' => 'bs-select form-control']) !!}
										@if ($errors->has('request')) <p class="help-block">{{ $errors->first('request') }}</p> @endif
									</div>
                                </div>
                            </div>
                        </div>
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="btn blue-madison btn-block margin-bottom-5">Add Money</div>
									<div class="form-group @if ($errors->has('add')) has-error @endif">
										{!! Form::select('add', ['1' => 'Enable', '0' => 'Disable'], null, ['class' => 'bs-select form-control']) !!}
										@if ($errors->has('add')) <p class="help-block">{{ $errors->first('add') }}</p> @endif
									</div>
								</div>
								<div class="col-md-6">
									<div class="btn blue-madison btn-block margin-bottom-5">Withdraw Money</div>
									<div class="form-group @if ($errors->has('withdraw')) has-error @endif">
										{!! Form::select('withdraw', ['1' => 'Enable', '0' => 'Disable'], null, ['class' => 'bs-select form-control']) !!}
										@if ($errors->has('withdraw')) <p class="help-block">{{ $errors->first('withdraw') }}</p> @endif
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="btn blue-madison btn-block margin-bottom-5">Transfer between Currencies</div>
									<div class="form-group @if ($errors->has('transfer')) has-error @endif">
										{!! Form::select('transfer', ['1' => 'Enable', '0' => 'Disable'], null, ['class' => 'bs-select form-control']) !!}
										@if ($errors->has('transfer')) <p class="help-block">{{ $errors->first('transfer') }}</p> @endif
									</div>
								</div>
								<div class="col-md-6">
									<div class="btn blue-madison btn-block margin-bottom-5">Upgrade +Wallet</div>
									<div class="form-group @if ($errors->has('upgrade')) has-error @endif">
										{!! Form::select('upgrade', ['1' => 'Enable', '0' => 'Disable'], null, ['class' => 'bs-select form-control']) !!}
										@if ($errors->has('upgrade')) <p class="help-block">{{ $errors->first('upgrade') }}</p> @endif
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<span class="input-group-btn">
								{!! Form::submit('Add', ['class' => 'btn btn-block green-jungle btn-lg'] ) !!}
							</span>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<div class="portlet-body">
    @if (count($wallettypes) == 0)
	<p class="text-center font-grey-silver margin-top-15">There are no types to display</p>
	@else
	<div class="row">
		<div class="col-sm-6">
			{!! Form::open() !!}
				<div class="input-group input-medium">
					<input class="form-control input-medium" placeholder="Search" type="text">
					<span class="input-group-btn">
						<button class="btn green-jungle" type="submit"><i class="icon-magnifier"></i></button>
					</span>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
	<div class="table-scrollable">
		<table class="table table-condensed table-bordered table-striped table-hover">
			<thead>
				<tr>
					<th style="width:1%">#</th>
					<th style="width:1%">Wallet</br>Type ID</th>
					<th>Name</th>
					<th style="width:1%">Add</br>+Wallet</th>
					<th style="width:1%">Add</br>Currency</th>
					<th style="width:1%">Send</th>
					<th style="width:1%">Pay</th>
					<th style="width:1%">Request</th>
					<th style="width:1%">Add</br>Money</th>
					<th style="width:1%">Withdraw</br>Money</th>
					<th style="width:1%">Transfer</th>
					<th style="width:1%">Upgrade</th>
					<th style="width:1%">Status</th>
					<th style="width:1%">Action</th>
				</tr>
			</thead>
			<tbody>
				<?php $i = 0 ?>
				@foreach ($wallettypes as $wallettype)
				<?php $i++ ?>
				<tr>
					<td>{!! $i !!}</td>
					<td>{!! $wallettype->wallet_type_id !!}</td>
					<td>{!! $wallettype->name !!}</td>
					<td>{!! $wallettype->add_wallet !!}</td>
					<td>{!! $wallettype->add_currency !!}</td>
					<td>{!! $wallettype->send !!}</td>
					<td>{!! $wallettype->pay !!}</td>
					<td>{!! $wallettype->request !!}</td>
					<td>{!! $wallettype->add !!}</td>
					<td>{!! $wallettype->withdraw !!}</td>
					<td>{!! $wallettype->transfer !!}</td>
					<td>{!! $wallettype->upgrade !!}</td>
					<td><span class="label label-sm @if ($wallettype->status === 'Active') label-success @elseif ($wallettype->status === 'Inactive') label-default @else label-default @endif">{!! $wallettype->status !!}</span></td>
					<td>
						<a href="/admin/wallettypes/{!! $wallettype->wallet_type_id !!}" class="btn blue btn-xs"><i class="fa fa-pencil"></i></a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	<div class="row">
		<div class="col-md-6 margin-top-10">
			Showing {!! $wallettypes->firstItem() !!} to {!! $wallettypes->lastItem() !!} of {!! $wallettypes->total() !!}
		</div>
		<div class="col-md-6 text-right">
			{!! $wallettypes->links() !!}
		</div>
	</div>
	@endif
</div>
<!-- END PAGE CONTENT-->
@stop

