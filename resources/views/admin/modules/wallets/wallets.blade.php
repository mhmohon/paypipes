@extends('admin/html')

@section('content')
@include('admin/modules/wallets/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->
<div class="portlet-body">
    @if (count($wallets) == 0)
	<div class="row">
		<div class="col-sm-6">
			{!! Form::open(['action' => 'Admin\Modules\Wallets\WalletsController@index', 'method' => 'GET']) !!}
				<div class="input-group input-medium">
					{!! Form::text('search', Request::get('search'), ['class' => 'form-control input-medium', 'placeholder' => 'Search']) !!}
					<span class="input-group-btn">
						{!! Form::button('<i class="icon-magnifier"></i>', ['class' => 'btn green-jungle', 'type'=>'submit']) !!}
					</span>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
	<p class="text-center font-grey-silver margin-top-15">There are no wallets to display</p>
	@else
	<div class="row">
		<div class="col-sm-6">
			{!! Form::open(['action' => 'Admin\Modules\Wallets\WalletsController@index', 'method' => 'GET']) !!}
				<div class="input-group input-medium">
					{!! Form::text('search', Request::get('search'), ['class' => 'form-control input-medium', 'placeholder' => 'Search']) !!}
					<span class="input-group-btn">
						{!! Form::button('<i class="icon-magnifier"></i>', ['class' => 'btn green-jungle', 'type'=>'submit']) !!}
					</span>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
	<div class="table-scrollable">
		<table class="table table-condensed table-bordered table-striped table-hover">
			<thead>
				<tr>
					<th style="width:1%">#</th>
					<th style="width:1%">Wallet ID</th>
					<th style="width:1%">Primary</th>
					<th style="width:1%">Email</th>
					<th>Type</th>
					<th>Fees</th>
					<th>Limits</th>
					<th style="width:1%">Currency</th>
					<th style="width:1%" class="text-right">Balance</th>
					<th style="width:1%">Status</th>
					<th style="width:1%">Action</th>
				</tr>
			</thead>
			<tbody>
				<?php $i = 0; ?>
				@foreach ($wallets as $wallet)
				<?php $i++ ?>
				<tr>
					<td>{!! $i !!}</td>
					<td>{!! $wallet->wallet_id !!}</td>
					<td>@if ($wallet->primary === 1) <span class="label label-sm label-default">Primary</span> @else @endif</td>
					<td>{!! $wallet->email !!}</td>
					<td>{!! $wallet->wtname !!}</td>
					<td>{!! $wallet->wfname !!}</td>
					<td>{!! $wallet->wlname !!}</td>
					<td>{!! $wallet->currency !!}</td>
					<td class="text-right">{!! $wallet->balance !!}</td>
					<td><span class="label label-sm @if ($wallet->status === 'Active') label-success @elseif ($wallet->status === 'Inactive') label-default @elseif ($wallet->status === 'Pending') label-info @else label-default @endif">{!! $wallet->status !!}</span></td>
					<td>
						<a href="#" class="btn blue btn-xs"><i class="fa fa-pencil"></i></a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	<div class="row">
		<div class="col-md-6 margin-top-10">
			Showing {!! $wallets->firstItem() !!} to {!! $wallets->lastItem() !!} of {!! $wallets->total() !!}
		</div>
		<div class="col-md-6 text-right">
			{!! $wallets->links() !!}
		</div>
	</div>
	@endif
</div>
<!-- END PAGE CONTENT-->
@stop

