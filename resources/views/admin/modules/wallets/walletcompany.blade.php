@extends('admin/html')

@section('content')
@include('admin/modules/wallets/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->
<div class="tabbable tabbable-tabdrop">
    @include('admin/modules/wallets/includes/walletmanagementtabs')
    <div class="tab-content">
        <div class="tab-pane active">
            <h3 class="page-title">
                + Wallet Company
                <a class="btn green-jungle bold" data-toggle="modal" href="#add">Add <i class="fa fa-plus"></i></a>
            </h3>
            <div class="modal fade" id="add" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Add a Wallet Type</h4>
                        </div>
                        <div class="modal-body">															                           
                            {!! Form::open(['url' => 'walletcompany']) !!}
                                <div class="form-body">
                                    <div class="form-group @if ($errors->has('mid')) has-error @endif">
                                        {!! Form::text('mid', null, ['class' => 'form-control', 'placeholder' => 'MID']) !!}
                                        @if ($errors->has('mid')) <p class="help-block">{{ $errors->first('mid') }}</p> @endif
                                    </div>  
                                    <div class="form-group">
                                        <span class="input-group-btn">
                                            {!! Form::submit('Add', ['class' => 'btn btn-block green-jungle btn-lg'] ) !!}
                                        </span>
                                    </div>
                                </div>
                            {!! Form::close() !!}                   
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <table class="table table-advance table-condensed table-bordered table-striped table-hover order-column" id="sample_1">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>MID</th>
                        <th>Company&nbsp;Name</th>
                        <th>Country</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Compliance</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 0 ?>    
                    @foreach ($walletcompany as $merchant)
                    <?php $i++ ?>
                    <tr>    
                        <td>{!! $i !!}</td>
                        <td>{!! $merchant->id !!}</td>
                        <td>{!! $merchant->companyname !!}</td>
                        <td>{!! $merchant->country !!}</td>
                        <td>{!! $merchant->email !!}</td>
                        <td>{!! $merchant->phone !!}</td>
                        <td><span class="label label-sm @if ($merchant->compliance === 'Approved') label-success @elseif ($merchant->compliance === 'Pending') label-info @elseif ($merchant->compliance === 'Declined') label-danger @else label-default @endif">{!! $merchant->compliance !!}</span></td>	
                        <td><span class="label label-sm @if ($merchant->status === 'Active') label-success @elseif ($merchant->status === 'Inactive') label-default @elseif ($merchant->status === 'Locked') label-danger @else label-default @endif">{!! $merchant->status !!}</span></td>	
                        <td>
                            <a href="/merchants/{!! $merchant->id !!}" class="btn blue btn-xs"><i class="fa fa-pencil"></i></a>
                            <button class="btn red btn-xs" data-toggle="confirmation" data-popout="true" data-popout="true" data-singleton="true" data-popout="true" data-singleton="true" data-original-title="Are you sure ?" title=""><i class="fa fa-lock"></i></button>
                        </td>									
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->
@stop

