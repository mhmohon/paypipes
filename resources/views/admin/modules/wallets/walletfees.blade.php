@extends('admin/html')

@section('content')
@include('admin/modules/wallets/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->
<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Add a Wallet Fees</h4>
			</div>
			<div class="modal-body">
				{!! Form::open(['url' => 'admin/walletfees']) !!}
					<div class="form-body">
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="@if ($errors->has('name')) has-error @endif">
										<label>Wallet Fees Name</label>
										{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Wallet Fees Name']) !!}
										@if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
									</div>
								</div>
                                <div class="col-md-6">
                                    <div class="@if ($errors->has('currency')) has-error @endif">
										<label>Currency</label>
										{!! Form::select('currency', $currencies, null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
										@if ($errors->has('currency')) <p class="help-block">{{ $errors->first('currency') }}</p> @endif
									</div>
                                </div>
                            </div>
                        </div>
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="@if ($errors->has('creation_fee')) has-error @endif">
										<label>Creation Fee</label>
										{!! Form::text('creation_fee', null, ['class' => 'form-control', 'placeholder' => 'Creation Fee']) !!}
										@if ($errors->has('creation_fee')) <p class="help-block">{{ $errors->first('creation_fee') }}</p> @endif
									</div>
								</div>
                                <div class="col-md-6">
                                    <div class="@if ($errors->has('monthly_fee')) has-error @endif">
										<label>Monthly Fee</label>
										{!! Form::text('monthly_fee', null, ['class' => 'form-control', 'placeholder' => 'Monthly Fee']) !!}
										@if ($errors->has('monthly_fee')) <p class="help-block">{{ $errors->first('monthly_fee') }}</p> @endif
									</div>
                                </div>
                            </div>
                        </div>
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="@if ($errors->has('send_fee')) has-error @endif">
										<label>Send Fee %</label>
										{!! Form::text('send_fee', null, ['class' => 'form-control', 'placeholder' => 'Send Fee %']) !!}
										@if ($errors->has('send_fee')) <p class="help-block">{{ $errors->first('send_fee') }}</p> @endif
									</div>
								</div>
                                <div class="col-md-6">
                                    <div class="@if ($errors->has('send_flat_fee')) has-error @endif">
										<label>Send Flat Fee</label>
										{!! Form::text('send_flat_fee', null, ['class' => 'form-control', 'placeholder' => 'Send Flat Fee']) !!}
										@if ($errors->has('send_flat_fee')) <p class="help-block">{{ $errors->first('send_flat_fee') }}</p> @endif
									</div>
                                </div>
                            </div>
                        </div>
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="@if ($errors->has('receive_fee')) has-error @endif">
										<label>Receive Fee %</label>
										{!! Form::text('receive_fee', null, ['class' => 'form-control', 'placeholder' => 'Receive Fee %']) !!}
										@if ($errors->has('receive_fee')) <p class="help-block">{{ $errors->first('receive_fee') }}</p> @endif
									</div>
								</div>
                                <div class="col-md-6">
                                    <div class="@if ($errors->has('receive_flat_fee')) has-error @endif">
										<label>Receive Flat Fee</label>
										{!! Form::text('receive_flat_fee', null, ['class' => 'form-control', 'placeholder' => 'Receive Flat Fee']) !!}
										@if ($errors->has('receive_flat_fee')) <p class="help-block">{{ $errors->first('receive_flat_fee') }}</p> @endif
									</div>
                                </div>
                            </div>
                        </div>
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="@if ($errors->has('withdraw_fee')) has-error @endif">
										<label>Withdrawal Fee %</label>
										{!! Form::text('withdraw_fee', null, ['class' => 'form-control', 'placeholder' => 'Withdrawal Fee %']) !!}
										@if ($errors->has('withdraw_fee')) <p class="help-block">{{ $errors->first('withdraw_fee') }}</p> @endif
									</div>
								</div>
                                <div class="col-md-6">
                                    <div class="@if ($errors->has('withdraw_flat_fee')) has-error @endif">
										<label>Withdrawal Flat Fee</label>
										{!! Form::text('withdraw_flat_fee', null, ['class' => 'form-control', 'placeholder' => 'Withdrawal Flat Fee']) !!}
										@if ($errors->has('withdraw_flat_fee')) <p class="help-block">{{ $errors->first('withdraw_flat_fee') }}</p> @endif
									</div>
                                </div>
                            </div>
                        </div>
						<div class="form-group">
							<span class="input-group-btn">
								{!! Form::submit('Add', ['class' => 'btn btn-block green-jungle btn-lg'] ) !!}
							</span>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<div class="portlet-body">
    @if (count($walletfees) == 0)
	<p class="text-center font-grey-silver margin-top-15">There are no fees to display</p>
	@else
	<div class="row">
		<div class="col-sm-6">
			{!! Form::open() !!}
				<div class="input-group input-medium">
					<input class="form-control input-medium" placeholder="Search" type="text">
					<span class="input-group-btn">
						<button class="btn green-jungle" type="submit"><i class="icon-magnifier"></i></button>
					</span>
				</div>
			{!! Form::close() !!}
		</div>

	</div>
	<div class="table-scrollable">
		<table class="table table-condensed table-bordered table-striped table-hover">
			<thead>
				<tr>
					<th style="width:1%">#</th>
					<th style="width:1%">Wallet</br>Fee ID</th>
					<th>Name</th>
					<th style="width:1%">Currency</th>
					<th style="width:1%">Creation</br>Fee</th>
					<th style="width:1%">Monthly</br>Fee</th>
					<th style="width:1%">Send</br>Fee</th>
					<th style="width:1%">Send</br>Flat Fee</th>
					<th style="width:1%">Receive</br>Fee</th>
					<th style="width:1%">Receive</br>Flat Fee</th>
					<th style="width:1%">Withdrawal</br>Fee</th>
					<th style="width:1%">Withdrawal</br>Flat Fee</th>
					<th style="width:1%">Status</th>
					<th style="width:1%">Action</th>
				</tr>
			</thead>
			<tbody>
				<?php $i = 0 ?>
				@foreach ($walletfees as $walletfee)
				<?php $i++ ?>
				<tr>
					<td>{!! $i !!}</td>
					<td>{!! $walletfee->wallet_fee_id !!}</td>
					<td>{!! $walletfee->name !!}</td>
					<td>{!! $walletfee->currency !!}</td>
					<td>{!! $walletfee->creation_fee !!}</td>
					<td>{!! $walletfee->monthly_fee !!}</td>
					<td>{!! $walletfee->send_fee !!} %</td>
					<td>{!! $walletfee->send_flat_fee !!}</td>
					<td>{!! $walletfee->receive_fee !!} %</td>
					<td>{!! $walletfee->receive_flat_fee !!}</td>
					<td>{!! $walletfee->withdraw_fee !!} %</td>
					<td>{!! $walletfee->withdraw_flat_fee !!}</td>
					<td><span class="label label-sm @if ($walletfee->status === 'Active') label-success @elseif ($walletfee->status === 'Inactive') label-default @else label-default @endif">{!! $walletfee->status !!}</span></td>
					<td>
						<a href="/admin/walletfees/{!! $walletfee->wallet_fee_id !!}" class="btn blue btn-xs"><i class="fa fa-pencil"></i></a>
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	<div class="row">
		<div class="col-md-6 margin-top-10">
			Showing {!! $walletfees->firstItem() !!} to {!! $walletfees->lastItem() !!} of {!! $walletfees->total() !!}
		</div>
		<div class="col-md-6 text-right">
			{!! $walletfees->links() !!}
		</div>
	</div>
	@endif
</div>
<!-- END PAGE CONTENT-->
@stop

