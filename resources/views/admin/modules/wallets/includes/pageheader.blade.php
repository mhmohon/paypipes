<!-- BEGIN PAGE HEADER-->
<div class="page-bar">
    <h3 class="page-title" style="float:left;">{!! $pagetitle !!}</h3>
	@if (Request::is('admin/wallettypes')) <a class="btn green-jungle bold" data-toggle="modal" href="#add" style="margin:10px;">Add <i class="fa fa-plus"></i></a>
	@elseif (Request::is('admin/wallettypes/*')) <a class="btn blue bold" data-toggle="modal" href="#edit" style="margin:10px;">Edit <i class="fa fa-pencil"></i></a> 
	@elseif (Request::is('admin/walletfees')) <a class="btn green-jungle bold" data-toggle="modal" href="#add" style="margin:10px;">Add <i class="fa fa-plus"></i></a>
	@elseif (Request::is('admin/walletfees/*')) <a class="btn blue bold" data-toggle="modal" href="#edit" style="margin:10px;">Edit <i class="fa fa-pencil"></i></a>
    @elseif (Request::is('admin/walletlimits')) <a class="btn green-jungle bold" data-toggle="modal" href="#add" style="margin:10px;">Add <i class="fa fa-plus"></i></a>
	@elseif (Request::is('admin/walletlimits/*')) <a class="btn blue bold" data-toggle="modal" href="#edit" style="margin:10px;">Edit <i class="fa fa-pencil"></i></a>
	@endif	
    <ul class="page-breadcrumb" style="float:right;padding-top:18px;">
        <li>
            {!! $breadcrumb_level1 or '' !!}
            @if (Request::is('admin/wallets')) <i class="fa fa-angle-right"></i>
            @elseif (Request::is('admin/wallets/*')) <i class="fa fa-angle-right"></i>
			@elseif (Request::is('admin/wallettypes')) <i class="fa fa-angle-right"></i>
            @elseif (Request::is('admin/wallettypes/*')) <i class="fa fa-angle-right"></i>
            @elseif (Request::is('admin/walletfees')) <i class="fa fa-angle-right"></i>
            @elseif (Request::is('admin/walletfees/*')) <i class="fa fa-angle-right"></i>
            @elseif (Request::is('admin/walletlimits')) <i class="fa fa-angle-right"></i>
            @elseif (Request::is('admin/walletlimits/*')) <i class="fa fa-angle-right"></i>
            @endif
        </li>
        <li>
            {!! $breadcrumb_level2 or '' !!}
            @if (Request::is('admin/wallets/*')) <i class="fa fa-angle-right"></i>
			@elseif (Request::is('admin/wallettypes/*')) <i class="fa fa-angle-right"></i>
            @elseif (Request::is('admin/walletfees/*')) <i class="fa fa-angle-right"></i>
            @elseif (Request::is('admin/walletlimits/*')) <i class="fa fa-angle-right"></i>
            @endif
        </li>
        <li>
            {!! $breadcrumb_level3 or '' !!}
        </li>
    </ul>
</div>
<!-- END PAGE HEADER-->
