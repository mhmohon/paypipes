<ul class="nav nav-tabs">
    <li class="{{ Request::is('admin/walletcompany') ? 'active' : '' }}">
        <a href="/admin/walletcompany">+Wallet Company</a>
    </li>
    <li class="{{ Request::is('admin/wallettypes') ? 'active' : '' }}">
        <a href="/admin/wallettypes">+Wallet Types</a>
    </li>
    <li class="
        {{ Request::is('admin/walletfees') ? 'active' : '' }}
        {{ Request::is('admin/walletfees/*') ? 'active' : '' }}">
        <a href="/admin/walletfees">+Wallet Fees</a>
    </li>
    <li class="
        {{ Request::is('admin/walletlimits') ? 'active' : '' }}
        {{ Request::is('admin/walletlimits/*') ? 'active' : '' }}">
        <a href="/admin/walletlimits">+Wallet Limits</a>
    </li>
    <li class="{{ Request::is('admin/walletsettings') ? 'active' : '' }}">
        <a href="/admin/walletsettings">+Wallet Settings</a>
    </li>
</ul>

