@extends('html')

@section('content')
@include('modules/wallets/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->	
<div class="tabbable tabbable-tabdrop">
    @include('modules/wallets/includes/wallettabs')
    <div class="tab-content">
        <div class="tab-pane active">
            <h3 class="page-title">
                title
            </h3>
            <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title">Edit +Wallet</h4>
                        </div>
                        <div class="modal-body"> 
                            {!! Form::model($wallet, ['method' => 'PATCH', 'action' => ['Modules\Wallets\WalletsController@update', $wallet->id ]]) !!}
                                <div class="form-body">
                                    <div class="form-group @if ($errors->has('wallet')) has-error @endif">
                                        {!! Form::select('wallet', ['Pending' => 'Pending', 'Active' => 'Active'], null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
                                        @if ($errors->has('wallet')) <p class="help-block">{{ $errors->first('wallet') }}</p> @endif
                                    </div>
                                    <div class="form-group @if ($errors->has('wallettype')) has-error @endif">
                                        {!! Form::select('wallettype', ['Personal' => 'Personal','Business' => 'Business'], null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
                                        @if ($errors->has('wallettype')) <p class="help-block">{{ $errors->first('wallettype') }}</p> @endif
                                    </div>
                                    <div class="form-group @if ($errors->has('walletfees')) has-error @endif">
                                        {!! Form::text('walletfees', null, ['class' => 'form-control', 'placeholder' => 'Wallet Fees']) !!}
                                        @if ($errors->has('walletfees')) <p class="help-block">{{ $errors->first('walletfees') }}</p> @endif
                                    </div>
                                    <div class="form-group @if ($errors->has('walletlimits')) has-error @endif">
                                        {!! Form::text('walletlimits', null, ['class' => 'form-control', 'placeholder' => 'Wallet Limits']) !!}
                                        @if ($errors->has('walletlimits')) <p class="help-block">{{ $errors->first('walletlimits') }}</p> @endif
                                    </div>
                                    <div class="form-group">
                                        <span class="input-group-btn">
                                            {!! Form::submit('Edit', ['class' => 'btn btn-block blue btn-lg'] ) !!}
                                        </span>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="dashboard-stat2 ">
                        <div class="display">
                            <div class="number">
                                <h3 class="font-green-jungle">
                                    € <span data-counter="counterup" data-value="120">0</span>
                                </h3>
                                <small>Balance</small>
                            </div>
                            <div class="icon">
                                
                            </div>
                        </div>
                        <div class="progress-info">
                            <div class="progress">
                                <span style="width: 100%;" class="progress-bar progress-bar-success green-jungle">
                                    <span class="sr-only"> % Available Balance </span>
                                </span> 
                            </div>
                            <div class="status">
                                <div class="status-title"> Available Balance </div>
                                <div class="status-number">€ <span data-counter="counterup" data-value="120"></span> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="dashboard-stat2 ">
                        <div class="display">
                            <div class="number">
                                <h3 class="font-green-jungle">
                                    $ <span data-counter="counterup" data-value="100">0</span>
                                </h3>
                                <small>Balance</small>
                            </div>
                            <div class="icon">
                                
                            </div>
                        </div>
                        <div class="progress-info">
                            <div class="progress">
                                <span style="width: 100%;" class="progress-bar progress-bar-success green-jungle">
                                    <span class="sr-only">% Available Balance</span>
                                </span>
                            </div>
                            <div class="status">
                                <div class="status-title"> Available Balance </div>
                                <div class="status-number">$ <span data-counter="counterup" data-value="100"></span> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="dashboard-stat2 ">
                        <div class="display">
                            <div class="number">
                                <h3 class="font-green-jungle">
                                    £ <span data-counter="counterup" data-value="65">0</span>
                                </h3>
                                <small>Balance</small>
                            </div>
                            <div class="icon">
                                
                            </div>
                        </div>
                        <div class="progress-info">
                            <div class="progress">
                                <span style="width: 80%;" class="progress-bar progress-bar-success green-jungle">
                                    <span class="sr-only">80% Available Balance</span>
                                </span>
                            </div>
                            <div class="status">
                                <div class="status-title"> Available Balance </div>
                                <div class="status-number">£ <span data-counter="counterup" data-value="55"></span> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-scrollable">                
                <table class="table table-light table-hover">
                    <tbody>
                        <tr>
                            <td style="width:30%">Compliance</td>
                            <td>customer</td>
                        </tr>
                        <tr>
                            <td>+Wallet</td>
                            <td><span class="label label-sm @if ($wallet->walletstatus === 'Active') label-success @elseif ($wallet->walletstatus === 'Pending') label-info @elseif ($wallet->waletstatus === 'Inactive') label-default @else label-default @endif">{!! $wallet->walletstatus !!}</span></td>
                        </tr>
                        <tr>
                            <td>Type</td>
                            <td>{!! $wallet->wallettype !!}</td>
                        </tr>
                        <tr>
                            <td>Fees</td>
                            <td>{!! $wallet->walletfees !!}</td>
                        </tr>
                        <tr>
                            <td>Limits</td>
                            <td>{!! $wallet->walletlimits !!}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->
@stop