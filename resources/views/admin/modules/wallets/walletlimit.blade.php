@extends('admin/html')

@section('content')
@include('admin/modules/wallets/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Edit a Wallet Limits</h4>
			</div>
			<div class="modal-body">															                           
				{!! Form::model($walletlimit, ['method' => 'POST', 'action' => ['Admin\Modules\Wallets\WalletlimitsController@update', $walletlimit->wallet_limit_id ]]) !!}
					<div class="form-body">
						<div class="form-group @if ($errors->has('name')) has-error @endif">
							<label>Wallet Limits Name</label>
							{!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Wallet Limits Name']) !!}
							@if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif						
                        </div>
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="@if ($errors->has('max_balance')) has-error @endif">
										<label>Maximum Balance</label>
										{!! Form::text('max_balance', null, ['class' => 'form-control', 'placeholder' => 'Maximum Balance']) !!}
										@if ($errors->has('max_balance')) <p class="help-block">{{ $errors->first('max_balance') }}</p> @endif
									</div>
								</div>
                                <div class="col-md-6">
                                    <div class="@if ($errors->has('max_turnover')) has-error @endif">
										<label>Maximum Turnover</label>
										{!! Form::text('max_turnover', null, ['class' => 'form-control', 'placeholder' => 'Maximum Turnover']) !!}
										@if ($errors->has('max_turnover')) <p class="help-block">{{ $errors->first('max_turnover') }}</p> @endif
									</div>
                                </div>												
                            </div>							
                        </div> 
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="@if ($errors->has('max_turnover_per_month')) has-error @endif">
										<label>Maximum Turnover per Month</label>
										{!! Form::text('max_turnover_per_month', null, ['class' => 'form-control', 'placeholder' => 'Maximum Turnover per Month']) !!}
										@if ($errors->has('max_turnover_per_month')) <p class="help-block">{{ $errors->first('max_turnover_per_month') }}</p> @endif
									</div>
								</div>
                                <div class="col-md-6">
                                    <div class="@if ($errors->has('max_credit_amount_per_day')) has-error @endif">
										<label>Maximum Credit Amount per Day</label>
										{!! Form::text('max_credit_amount_per_day', null, ['class' => 'form-control', 'placeholder' => 'Maximum Credit Amount per Day']) !!}
										@if ($errors->has('max_credit_amount_per_day')) <p class="help-block">{{ $errors->first('max_credit_amount_per_day') }}</p> @endif
									</div>
                                </div>												
                            </div>							
                        </div>  
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="@if ($errors->has('max_credit_payments_per_day')) has-error @endif">
										<label>Maximum Credit Payments per Day</label>
										{!! Form::text('max_credit_payments_per_day', null, ['class' => 'form-control', 'placeholder' => 'Maximum Credit Payments per Day']) !!}
										@if ($errors->has('max_credit_payments_per_day')) <p class="help-block">{{ $errors->first('max_credit_payments_per_day') }}</p> @endif
									</div>
								</div>
                                <div class="col-md-6">
                                    <div class="@if ($errors->has('max_topup_amount_per_day')) has-error @endif">
										<label>Maximum Topup Amount per Day</label>
										{!! Form::text('max_topup_amount_per_day', null, ['class' => 'form-control', 'placeholder' => 'Maximum Topup Amount per Day']) !!}
										@if ($errors->has('max_topup_amount_per_day')) <p class="help-block">{{ $errors->first('max_topup_amount_per_day') }}</p> @endif
									</div>
                                </div>												
                            </div>							
                        </div>
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="@if ($errors->has('max_topup_payments_per_day')) has-error @endif">
										<label>Maximum Topup Payments per Day</label>
										{!! Form::text('max_topup_payments_per_day', null, ['class' => 'form-control', 'placeholder' => 'Maximum Topup Payments per Day']) !!}
										@if ($errors->has('max_topup_payments_per_day')) <p class="help-block">{{ $errors->first('max_topup_payments_per_day') }}</p> @endif
									</div>
								</div>
                                <div class="col-md-6">
                                    <div class="@if ($errors->has('max_topup_amount_per_payment')) has-error @endif">
										<label>Maximum Topup Amount per Payment</label>
										{!! Form::text('max_topup_amount_per_payment', null, ['class' => 'form-control', 'placeholder' => 'Maximum Topup Amount per Payment']) !!}
										@if ($errors->has('max_topup_amount_per_payment')) <p class="help-block">{{ $errors->first('max_topup_amount_per_payment') }}</p> @endif
									</div>
                                </div>												
                            </div>							
                        </div>
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="@if ($errors->has('max_receive_amount_per_day')) has-error @endif">
										<label>Maximum Receive Amount per Day</label>
										{!! Form::text('max_receive_amount_per_day', null, ['class' => 'form-control', 'placeholder' => 'Maximum Receive Amount per Day']) !!}
										@if ($errors->has('max_receive_amount_per_day')) <p class="help-block">{{ $errors->first('max_receive_amount_per_day') }}</p> @endif
									</div>
								</div>
                                <div class="col-md-6">
                                    <div class="@if ($errors->has('max_receive_payments_per_day')) has-error @endif">
										<label>Maximum Receive Payments per Day</label>
										{!! Form::text('max_receive_payments_per_day', null, ['class' => 'form-control', 'placeholder' => 'Maximum Receive Payments per Day']) !!}
										@if ($errors->has('max_receive_payments_per_day')) <p class="help-block">{{ $errors->first('max_receive_payments_per_day') }}</p> @endif
									</div>
                                </div>												
                            </div>							
                        </div>
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="@if ($errors->has('max_receive_amount_per_payment')) has-error @endif">
										<label>Maximum Receive Amount per Payment</label>
										{!! Form::text('max_receive_amount_per_payment', null, ['class' => 'form-control', 'placeholder' => 'Maximum Receive Amount per Payment']) !!}
										@if ($errors->has('max_receive_amount_per_payment')) <p class="help-block">{{ $errors->first('max_receive_amount_per_payment') }}</p> @endif
									</div>
								</div>
                                <div class="col-md-6">
                                    <div class="@if ($errors->has('max_debit_amount_per_day')) has-error @endif">
										<label>Maximum Debit Amount per Day</label>
										{!! Form::text('max_debit_amount_per_day', null, ['class' => 'form-control', 'placeholder' => 'Maximum Debit Amount per Day']) !!}
										@if ($errors->has('max_debit_amount_per_day')) <p class="help-block">{{ $errors->first('max_debit_amount_per_day') }}</p> @endif
									</div>
                                </div>												
                            </div>							
                        </div>
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="@if ($errors->has('max_debit_payments_per_day')) has-error @endif">
										<label>Maximum Debit Payments per Day</label>
										{!! Form::text('max_debit_payments_per_day', null, ['class' => 'form-control', 'placeholder' => 'Maximum Debit Payments per Day']) !!}
										@if ($errors->has('max_debit_payments_per_day')) <p class="help-block">{{ $errors->first('max_debit_payments_per_day') }}</p> @endif
									</div>
								</div>
                                <div class="col-md-6">
                                    <div class="@if ($errors->has('max_send_amount_per_day')) has-error @endif">
										<label>Maximum Send Amount per Day</label>
										{!! Form::text('max_send_amount_per_day', null, ['class' => 'form-control', 'placeholder' => 'Maximum Send Amount per Day']) !!}
										@if ($errors->has('max_send_amount_per_day')) <p class="help-block">{{ $errors->first('max_send_amount_per_day') }}</p> @endif
									</div>
                                </div>												
                            </div>							
                        </div>
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="@if ($errors->has('max_send_payments_per_day')) has-error @endif">
										<label>Maximum Send Payments per Day</label>
										{!! Form::text('max_send_payments_per_day', null, ['class' => 'form-control', 'placeholder' => 'Maximum Send Payments per Day']) !!}
										@if ($errors->has('max_send_payments_per_day')) <p class="help-block">{{ $errors->first('max_send_payments_per_day') }}</p> @endif
									</div>
								</div>
                                <div class="col-md-6">
                                    <div class="@if ($errors->has('max_send_amount_per_payment')) has-error @endif">
										<label>Maximum Send Amount per Payment</label>
										{!! Form::text('max_send_amount_per_payment', null, ['class' => 'form-control', 'placeholder' => 'Maximum Send Amount per Payment']) !!}
										@if ($errors->has('max_send_amount_per_payment')) <p class="help-block">{{ $errors->first('max_send_amount_per_payment') }}</p> @endif
									</div>
                                </div>												
                            </div>							
                        </div> 
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="@if ($errors->has('max_withdraw_amount_per_day')) has-error @endif">
										<label>Maximum Withdraw Amount per Day</label>
										{!! Form::text('max_withdraw_amount_per_day', null, ['class' => 'form-control', 'placeholder' => 'Maximum Withdraw Amount per Day']) !!}
										@if ($errors->has('max_withdraw_amount_per_day')) <p class="help-block">{{ $errors->first('max_withdraw_amount_per_day') }}</p> @endif
									</div>
								</div>
                                <div class="col-md-6">
                                    <div class="@if ($errors->has('max_withdraw_payments_per_day')) has-error @endif">
										<label>Maximum Withdraw Payments per Day</label>
										{!! Form::text('max_withdraw_payments_per_day', null, ['class' => 'form-control', 'placeholder' => 'Maximum Withdraw Payments per Day']) !!}
										@if ($errors->has('max_withdraw_payments_per_day')) <p class="help-block">{{ $errors->first('max_withdraw_payments_per_day') }}</p> @endif
									</div>
                                </div>												
                            </div>							
                        </div> 
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="@if ($errors->has('max_withdraw_amount_per_payment')) has-error @endif">
										<label>Maximum Withdraw Amount per Payment</label>
										{!! Form::text('max_withdraw_amount_per_payment', null, ['class' => 'form-control', 'placeholder' => 'Maximum Withdraw Amount per Payment']) !!}
										@if ($errors->has('max_withdraw_amount_per_payment')) <p class="help-block">{{ $errors->first('max_withdraw_amount_per_payment') }}</p> @endif
									</div>
								</div>
                                <div class="col-md-6">
                                    <div class="@if ($errors->has('max_withdraw_amount')) has-error @endif">
										<label>Maximum Withdraw Amount</label>
										{!! Form::text('max_withdraw_amount', null, ['class' => 'form-control', 'placeholder' => 'Maximum Withdraw Amount']) !!}
										@if ($errors->has('max_withdraw_amount')) <p class="help-block">{{ $errors->first('max_withdraw_amount') }}</p> @endif
									</div>
                                </div>												
                            </div>							
                        </div> 	
						<div class="form-group">
							<span class="input-group-btn">
								{!! Form::submit('Edit', ['class' => 'btn btn-block green-jungle btn-lg'] ) !!}
							</span>
						</div>
					</div>
				{!! Form::close() !!}                   
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<div class="table-scrollable">
	<table class="table table-light table-hover">
		<tbody>
			<tr>
				<td style="width:30%">Status</td>
				<td><span class="label label-sm @if ($walletlimit->status === 'Active') label-success @elseif ($walletlimit->status === 'Inactive') label-default @else label-default @endif">{!! $walletlimit->status !!}</span></td>
			</tr>
			<tr>
				<td>Wallet Limits Name</td>
				<td>{!! $walletlimit->name !!}</td>
			</tr>
			<tr>
				<td>Currency</td>
				<td>{!! $walletlimit->currency !!}</td>
			</tr>
			<tr>
				<td>Maximum Balance</td>
				<td>€ {!! $walletlimit->max_balance !!}</td>
			</tr>
			<tr>
				<td>Maximum Turnover</td>
				<td>€ {!! $walletlimit->max_turnover !!}</td>
			</tr>
			<tr>
				<td>Maximum Turnover per Month</td>
				<td>€ {!! $walletlimit->max_turnover_per_month !!}</td>
			</tr>
			<tr>
				<td>Maximum Credit Amount per Day</td>
				<td>€ {!! $walletlimit->max_credit_amount_per_day !!}</td>
			</tr>
			<tr>
				<td>Maximum Credit Payments per Day</td>
				<td>€ {!! $walletlimit->max_credit_payments_per_day !!}</td>
			</tr>
			<tr>
				<td>Maximum Topup Amount per Day</td>
				<td>€ {!! $walletlimit->max_topup_amount_per_day !!}</td>
			</tr>
			<tr>
				<td>Maximum Topup Payments per Day</td>
				<td>€ {!! $walletlimit->max_topup_payments_per_day !!}</td>
			</tr>
			<tr>
				<td>Maximum Topup Amount per Payment</td>
				<td>€ {!! $walletlimit->max_topup_amount_per_payment !!}</td>
			</tr>
			<tr>
				<td>Maximum Receive Amount per Day</td>
				<td>€ {!! $walletlimit->max_receive_amount_per_day !!}</td>
			</tr>
			<tr>
				<td>Maximum Receive Payments per Day</td>
				<td>€ {!! $walletlimit->max_receive_payments_per_day !!}</td>
			</tr>
			<tr>
				<td>Maximum Receive Amount per Payment</td>
				<td>€ {!! $walletlimit->max_receive_amount_per_payment !!}</td>
			</tr>
			<tr>
				<td>Maximum Debit Amount per Day</td>
				<td>€ {!! $walletlimit->max_debit_amount_per_day !!}</td>
			</tr>
			<tr>
				<td>Maximum Debit Payments per Day</td>
				<td>€ {!! $walletlimit->max_debit_payments_per_day !!}</td>
			</tr>
			<tr>
				<td>Maximum Send Amount per Day</td>
				<td>€ {!! $walletlimit->max_send_amount_per_day !!}</td>
			</tr>
			<tr>
				<td>Maximum Send Payments per Day</td>
				<td>€ {!! $walletlimit->max_send_payments_per_day !!}</td>
			</tr>
			<tr>
				<td>Maximum Send Amount per Payment</td>
				<td>€ {!! $walletlimit->max_send_amount_per_payment !!}</td>
			</tr>
			<tr>
				<td>Maximum Withdraw Amount per Day</td>
				<td>€ {!! $walletlimit->max_withdraw_amount_per_day !!}</td>
			</tr>
			<tr>
				<td>Maximum Withdraw Payments per Day</td>
				<td>€ {!! $walletlimit->max_withdraw_payments_per_day !!}</td>
			</tr>
			<tr>
				<td>Maximum Withdraw Amount per Payment</td>
				<td>€ {!! $walletlimit->max_withdraw_amount_per_payment !!}</td>
			</tr>
			<tr>
				<td>Maximum Withdraw Amount</td>
				<td>€ {!! $walletlimit->max_withdraw_amount !!}</td>
			</tr>
		</tbody>
	</table>
</div>
<!-- END PAGE CONTENT-->
@stop

