@extends('admin/html')

@section('content')
@include('admin/modules/settings/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Edit General Settings</h4>
			</div>						
			<div class="modal-body"> 
				{!! Form::model($setting, ['method' => 'PATCH', 'files' => 'true', 'action' => ['Admin\Modules\Settings\SettingsController@store', $setting->id ]]) !!}
					<div class="form-body">						
						<div class="form-group @if ($errors->has('website')) has-error @endif">
							<label>Website</label>
							{!! Form::text('website', null, ['class' => 'form-control', 'placeholder' => 'Website']) !!}
							@if ($errors->has('website')) <p class="help-block">{{ $errors->first('website') }}</p> @endif
						</div>
						<div class="form-group @if ($errors->has('support_email')) has-error @endif">
							<label>Support E-mail</label>
							{!! Form::text('support_email', null, ['class' => 'form-control', 'placeholder' => 'Support E-mail']) !!}
							@if ($errors->has('support_email')) <p class="help-block">{{ $errors->first('support_email') }}</p> @endif
						</div>
						<div class="form-group @if ($errors->has('timezone')) has-error @endif">
							<label>Timezone</label>
							{!! Form::select('timezone', $timezones, null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
							@if ($errors->has('timezone')) <p class="help-block">{{ $errors->first('timezone') }}</p> @endif
						</div>
						<div class="form-group">
							<span class="input-group-btn">
								{!! Form::submit('Edit', ['class' => 'btn btn-block blue btn-lg'] ) !!}
							</span>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<div class="portlet">
	<div class="table-scrollable">
		<table class="table table-light table-hover">
			<tbody>
				<tr>
					<td width="30%">Website</td>
					<td>{!! $setting->website !!}</td>
				</tr>
				<tr>
					<td>Support E-mail</td>
					<td>{!! $setting->support_email !!}</td>
				</tr>
				<tr>
					<td>Time Zone</td>
					<td>{!! $setting->timezone !!}</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<!-- END PAGE CONTENT-->
@stop

