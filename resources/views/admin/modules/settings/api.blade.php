@extends('admin/html')

@section('content')
@include('admin/modules/settings/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->	
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Edit API Settings</h4>
			</div>						
			<div class="modal-body"> 
				{!! Form::model($setting, ['method' => 'PATCH', 'files' => 'true', 'action' => ['Admin\Modules\Settings\SettingsController@api', $setting->id ]]) !!}
					<div class="form-body">
						<div class="form-group @if ($errors->has('api_id')) has-error @endif">
							<label>Redirect API ID</label>
							{!! Form::text('api_id', null, ['class' => 'form-control', 'placeholder' => 'Redirect API ID']) !!}
							@if ($errors->has('api_id')) <p class="help-block">{{ $errors->first('api_id') }}</p> @endif
						</div>
						<div class="form-group @if ($errors->has('client_id')) has-error @endif">
							<label>Redirect Client ID</label>
							{!! Form::text('client_id', null, ['class' => 'form-control', 'placeholder' => 'Redirect Client ID']) !!}
							@if ($errors->has('client_id')) <p class="help-block">{{ $errors->first('client_id') }}</p> @endif
						</div>
						<div class="form-group @if ($errors->has('client_secret')) has-error @endif">
							<label>Redirect Client Secret</label>
							{!! Form::text('client_secret', null, ['class' => 'form-control', 'placeholder' => 'Redirect Client Secret']) !!}
							@if ($errors->has('client_secret')) <p class="help-block">{{ $errors->first('client_secret') }}</p> @endif
						</div>
						<div class="form-group @if ($errors->has('direct_api_id')) has-error @endif">
							<label>Direct API ID</label>
							{!! Form::text('direct_api_id', null, ['class' => 'form-control', 'placeholder' => 'Direct API ID']) !!}
							@if ($errors->has('direct_api_id')) <p class="help-block">{{ $errors->first('direct_api_id') }}</p> @endif
						</div>
						<div class="form-group @if ($errors->has('direct_client_id')) has-error @endif">
							<label>Direct Client ID</label>
							{!! Form::text('direct_client_id', null, ['class' => 'form-control', 'placeholder' => 'Direct Client ID']) !!}
							@if ($errors->has('direct_client_id')) <p class="help-block">{{ $errors->first('direct_client_id') }}</p> @endif
						</div>
						<div class="form-group @if ($errors->has('direct_client_secret')) has-error @endif">
							<label>Direct Client Secret</label>
							{!! Form::text('direct_client_secret', null, ['class' => 'form-control', 'placeholder' => 'Direct Client Secret']) !!}
							@if ($errors->has('direct_client_secret')) <p class="help-block">{{ $errors->first('direct_client_secret') }}</p> @endif
						</div>
						<div class="form-group @if ($errors->has('api_url')) has-error @endif">
							<label>API URL</label>
							{!! Form::text('api_url', null, ['class' => 'form-control', 'placeholder' => 'API URL']) !!}
							@if ($errors->has('api_url')) <p class="help-block">{{ $errors->first('api_url') }}</p> @endif
						</div>
						<div class="form-group">
							<span class="input-group-btn">
								{!! Form::submit('Edit', ['class' => 'btn btn-block blue btn-lg'] ) !!}
							</span>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<div class="portlet">
	<div class="table-scrollable">
		<table class="table table-light table-hover">
			<tbody>
				<tr>
					<td>Redirect API ID</td>
					<td>{!! $setting->api_id !!}</td>
				</tr>
				<tr>
					<td>Redirect Client ID</td>
					<td>{!! $setting->client_id !!}</td>
				</tr>
				<tr>
					<td>Redirect Client Secret</td>
					<td>{!! $setting->client_secret !!}</td>
				</tr>
				<tr>
					<td>Direct API ID</td>
					<td>{!! $setting->direct_api_id !!}</td>
				</tr>
				<tr>
					<td>Direct Client ID</td>
					<td>{!! $setting->direct_client_id !!}</td>
				</tr>
				<tr>
					<td>Direct Client Secret</td>
					<td>{!! $setting->direct_client_secret !!}</td>
				</tr>
				<tr>
					<td>API URL</td>
					<td>{!! $setting->api_url !!}</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<!-- END PAGE CONTENT-->
@stop

