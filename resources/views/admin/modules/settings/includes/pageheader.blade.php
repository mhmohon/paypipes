<!-- BEGIN PAGE HEADER-->
<div class="page-bar">
    <h3 class="page-title" style="float:left;">{!! $pagetitle !!}</h3>
    @if (Request::is('admin/settings')) <a class="btn blue bold" data-toggle="modal" href="#edit" style="margin:10px;">Edit <i class="fa fa-pencil"></i></a>
    @elseif (Request::is('admin/settings/api')) <a class="btn blue bold" data-toggle="modal" href="#edit" style="margin:10px;">Edit <i class="fa fa-pencil"></i></a>
    @elseif (Request::is('admin/settings/company_details')) <a class="btn blue bold" data-toggle="modal" href="#edit" style="margin:10px;">Edit <i class="fa fa-pencil"></i></a>
    @elseif (Request::is('admin/settings/crossrates')) <a class="btn green-jungle bold" data-toggle="modal" href="#add" style="margin:10px;">Add <i class="fa fa-plus"></i></a>
    @elseif (Request::is('admin/settings/bank_accounts')) <a class="btn green-jungle bold" data-toggle="modal" href="#add" style="margin:10px;">Add <i class="fa fa-plus"></i></a>
    @elseif (Request::is('admin/settings/bank_accounts/*')) <a class="btn green-jungle bold" data-toggle="modal" href="#edit" style="margin:10px;">Edit <i class="fa fa-pencil"></i></a>
	@endif
    <ul class="page-breadcrumb" style="float:right;padding-top:18px;">
        @if(isset($breadcrumb_level1) && !empty($breadcrumb_level1))
		<li>
            {!! $breadcrumb_level1 or '' !!}
            @if (Request::is('admin/settings')) <i class="fa fa-angle-right"></i>
            @elseif (Request::is('admin/settings/*')) <i class="fa fa-angle-right"></i>
            @elseif (Request::is('admin/settings/bank_accounts/*')) <i class="fa fa-angle-right"></i>
            @endif
        </li>
		@endif
		@if(isset($breadcrumb_level2) && !empty($breadcrumb_level2))
        <li>
            {!! $breadcrumb_level2 or '' !!}
            @if (Request::is('admin/settings/bank_accounts/*')) <i class="fa fa-angle-right"></i>
            @endif
        </li>
		@endif
		@if(isset($breadcrumb_level3) && !empty($breadcrumb_level3))
        <li>
            {!! $breadcrumb_level3 or '' !!}
        </li>
		@endif
    </ul>
</div>

<!-- END PAGE HEADER-->
