<ul class="nav nav-tabs">
    <li class="{{ Request::is('admin/settings') ? 'active' : '' }}">
        <a href="/admin/settings">General</a>
    </li>
    <li class="@if (Request::is('admin/settings/company_details')) active @else @endif">
        <a href="/admin/settings/company_details">Company Details</a>
    </li>
    <li class="@if (Request::is('admin/settings/bank_accounts')) active @else @endif">
        <a href="/admin/settings/bank_accounts">Bank Accounts</a>
    </li>

	<li class="@if (Request::is('admin/settings/api')) active @else @endif">
        <a href="/admin/settings/api">API</a>
    </li>
</ul>



