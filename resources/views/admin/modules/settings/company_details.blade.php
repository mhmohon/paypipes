@extends('admin/html')

@section('content')
@include('admin/modules/settings/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Edit Invoicing Settings</h4>
			</div>
			
			<div class="modal-body"> 
				{!! Form::model($setting, ['method' => 'PATCH', 'files' => 'true', 'action' => ['Admin\Modules\Settings\SettingsController@company_details', $setting->id ]]) !!}
					<div class="form-body">
						<div class="form-group @if ($errors->has('company_legal_name')) has-error @endif">
							<label>Company Legal Name</label>
							{!! Form::text('company_legal_name', null, ['class' => 'form-control', 'placeholder' => 'Company Legal Name']) !!}
							@if ($errors->has('company_legal_name')) <p class="help-block">{{ $errors->first('company_legal_name') }}</p> @endif
						</div>
						<div class="form-group @if ($errors->has('street')) has-error @endif">
							<label>Street</label>
							{!! Form::text('street', null, ['class' => 'form-control', 'placeholder' => 'Street']) !!} 
							@if ($errors->has('street')) <p class="help-block">{{ $errors->first('street') }}</p> @endif
						</div>				
						<div class="form-group">
							<div class="row">
								<div class="col-md-6">
									<div class="@if ($errors->has('city')) has-error @endif">
										<label>City</label>
										{!! Form::text('city', null, ['class' => 'form-control', 'placeholder' => 'City']) !!}
										@if ($errors->has('city')) <p class="help-block">{{ $errors->first('city') }}</p> @endif
									</div> 
								</div>
								<div class="col-md-6">
									<div class="@if ($errors->has('state')) has-error @endif">
										<label>State</label>
										{!! Form::text('state', null, ['class' => 'form-control', 'placeholder' => 'State']) !!}
										@if ($errors->has('state')) <p class="help-block">{{ $errors->first('state') }}</p> @endif
									</div>
								</div>												
							</div>									
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-6">
									<div class="@if ($errors->has('post_code')) has-error @endif">
										<label>Post Code</label>
										{!! Form::text('post_code', null, ['class' => 'form-control', 'placeholder' => 'Post Code']) !!}
										@if ($errors->has('post_code')) <p class="help-block">{{ $errors->first('post_code') }}</p> @endif
									</div>
								</div>	
								<div class="col-md-6">
									<div class="@if ($errors->has('country')) has-error @endif">								
										<label>Country</label>
										{!! Form::select('country', $countries, null, ['id' => 'country','class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
										@if ($errors->has('country')) <p class="help-block">{{ $errors->first('country') }}</p> @endif
									</div>
								</div>                                											
							</div>									
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-6">
									<div class="@if ($errors->has('company_id')) has-error @endif">
										<label>Company ID</label>
										{!! Form::text('company_id', null, ['class' => 'form-control', 'placeholder' => 'Company ID']) !!}
										@if ($errors->has('company_id')) <p class="help-block">{{ $errors->first('company_id') }}</p> @endif
									</div>
								</div>
								<div class="col-md-6">
									<div class="@if ($errors->has('vat_id')) has-error @endif">
										<label>VAT ID</label>
										{!! Form::text('vat_id', null, ['class' => 'form-control', 'placeholder' => 'VAT ID']) !!}
										@if ($errors->has('vat_id')) <p class="help-block">{{ $errors->first('vat_id') }}</p> @endif
									</div>
								</div>												
							</div>									
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-6">
									<div class="@if ($errors->has('invoice_email')) has-error @endif">
										<label>Invoice Email</label>
										{!! Form::text('invoice_email', null, ['class' => 'form-control', 'placeholder' => 'Invoice Email']) !!}
										@if ($errors->has('invoice_email')) <p class="help-block">{{ $errors->first('invoice_email') }}</p> @endif
									</div>
								</div>
								<div class="col-md-6">
									<div class="@if ($errors->has('invoice_website')) has-error @endif">
										<label>Invoice Website</label>
										{!! Form::text('invoice_website', null, ['class' => 'form-control', 'placeholder' => 'Invoice Website']) !!}
										@if ($errors->has('invoice_website')) <p class="help-block">{{ $errors->first('invoice_website') }}</p> @endif
									</div>
								</div>												
							</div>									
						</div>
						<div class="form-group">
							<span class="input-group-btn">
								{!! Form::submit('Edit', ['class' => 'btn btn-block blue btn-lg'] ) !!}
							</span>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<div class="portlet">
	<div class="table-scrollable">
		<table class="table table-light table-hover">
			<tbody>
				<tr>
					<td style="width:30%">Company Legal Name</td>
					<td>{!! $setting->company_legal_name !!}</td>
				</tr>
				<tr>
					<td>Street</td>
					<td>{!! $setting->street !!}</td>
				</tr>
				<tr>
					<td>City</td>
					<td>{!! $setting->city !!}</td>
				</tr>
				<tr>
					<td>State</td>
					<td>{!! $setting->state !!}</td>
				</tr>
				<tr>
					<td>Post Code</td>
					<td>{!! $setting->post_code !!}</td>
				</tr>
				<tr>
					<td>Country</td>
					<td>{!! $setting->country !!}</td>
				</tr>
				<tr>
					<td>Company ID</td>
					<td>{!! $setting->company_id !!}</td>
				</tr>
				<tr>
					<td>VAT ID</td>
					<td>{!! $setting->vat_id !!}</td>
				</tr>
				<tr>
					<td>Invoice Website</td>
					<td>{!! $setting->invoice_website !!}</td>
				</tr>
				<tr>
					<td>Invoice Reply Email</td>
					<td>{!! $setting->invoice_email !!}</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>
<!-- END PAGE CONTENT-->
@stop

