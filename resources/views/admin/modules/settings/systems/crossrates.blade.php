@extends('admin/html')

@section('content')
@include('admin/modules/settings/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->
@include('admin/modules/settings/systems/add_crossrates')
<div class="portlet-body">
	<div class="row">
		<div class="col-sm-6  no-print">
			<div class="row">
			 @if (count($crossrates) > 0)
				<div class="col-sm-3">
					@include('admin/includes/item_per_page')
				</div>
			@endif
				<div class="col-sm-9">
				{!! Form::open(['url' => 'admin/settings/crossrates', 'method' => 'GET']) !!}
					<input type="hidden" name='rows' value="{{old('rows')}}" >
					<div class="input-group input-medium">
						{!! Form::text('search', Request::get('search'), ['class' => 'form-control input-medium', 'placeholder' => 'Search']) !!}
						<span class="input-group-btn">
							{!! Form::button('<i class="icon-magnifier"></i>', ['class' => 'btn green-jungle', 'type'=>'submit']) !!}
						</span>
					</div>
				{!! Form::close() !!}
				</div>
			</div>
		</div>
		@if (count($crossrates) > 0)
		<div class="col-sm-6 text-right no-print" style="margin-top:5px">
			<a href="{{url('admin/settings/crossrates/pdf-convert'.$pg)}}" class="btn btn-sm red"><i class="fa fa-file-pdf-o"></i> PDF</a>
			<a href="{{url('admin/settings/crossrates/csv-convert'.$pg)}}" class="btn btn-sm green-jungle"><i class="fa fa-file-excel-o"></i> CSV</a>
		</div>
		@endif
	</div>
	 @if (count($crossrates) == 0)
		 <p class="text-center font-grey-silver margin-top-15">There are no Curency to display</p>
	@else
	<div class="table-scrollable">
		<table class="table table-condensed table-bordered table-striped table-hover">
			<thead>
				<tr>
					<th style="width:1%">#</th>
					<th >From Curency</th>
					<th >To Curency</th>
					<th >Crossrate</th>
					<th>Status</th>
					<th style="width:1%"  class='no-print'>Action</th>
				</tr>
			</thead>
			<tbody>
				@php
					$page = \Request::get('page');
					$i = ($page == '')?0:(($page*$item_per_page)-$item_per_page);
				@endphp
				@foreach ($crossrates as $crossrate)

				<?php $i++ ?>
				<tr>
					<td>{!! $i !!}</td>
					<td>{!! $crossrate->from_currency !!}</td>
					<td>{!! $crossrate->to_currency !!}</td>
					<td>{!! $crossrate->crossrate !!}</td>
					<td><span class="label label-sm @if ($crossrate->status == 'Active') label-success @elseif ($crossrate->status == 'Inactive') label-default  @else label-default @endif">{!! $crossrate->status !!}</span></td>
					<td  class='no-print'>
						<a  data-toggle="modal" href="#edit{!! $crossrate->id !!}" class="btn blue btn-xs"><i class="fa fa-pencil"></i></a>
						{!! Form::open(['method' => 'DELETE', 'url' => ['admin/settings/crossrates/'. $crossrate->id], 'style' => 'display:inline-block;']) !!}
                                {!! Form::button('<i class="fa fa-recycle"></i>', ['class' => 'btn dark btn-xs', 'type'=>'submit', 'data-toggle' => 'confirmation', 'data-popout' => 'true', 'data-singleton' => 'true', 'data-original-title' => 'Delete! Are you sure?', 'title' => '']) !!}
                        {!! Form::close() !!}
					</td>
				</tr>
				@include('admin/modules/settings/systems/edit_crossrates')
				@endforeach
			</tbody>
		</table>
	</div>
	<div class="row">
		<div class="col-md-6 margin-top-10">
			Showing {!! $crossrates->firstItem() !!} to {!! $crossrates->lastItem() !!} of {!! $crossrates->total() !!}
		</div>
		<div class="col-md-6 text-right  no-print">
		 {{ $crossrates->appends(Request::except('page'))->links() }}
		</div>
	</div>
	@endif
</div>
<!-- END PAGE CONTENT-->
@stop

