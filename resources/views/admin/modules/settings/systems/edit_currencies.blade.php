<div class="modal fade" id="edit{!! @$currency->currency_id !!}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Edit Currency</h4>
            </div>
            <div class="modal-body">
            	 {!! Form::model($currency, ['method' => 'PATCH', 'url' => url('admin/settings/currencies/'. @$currency->currency_id)]) !!}
                    <div class="form-body">
						<div class="form-group @if ($errors->has('currency')) has-error @endif">
								 <label>Currency</label>
								{!! Form::text('currency', $currency->currency, ['class' => 'form-control', 'placeholder' => 'Currency']) !!}
								 @if ($errors->has('currency')) <p class="help-block">{{ $errors->first('currency') }}</p> @endif
						</div>
						<div class="form-group @if ($errors->has('cy_code')) has-error @endif">
								 <label>Short Code</label>
								{!! Form::text('cy_code',  $currency->cy_code, ['class' => 'form-control', 'placeholder' => 'Short Code']) !!}
								@if ($errors->has('cy_code')) <p class="help-block">{{ $errors->first('cy_code') }}</p> @endif
						</div>
						<div class="form-group @if ($errors->has('iso_num')) has-error @endif">
								 <label>ISO Number</label>
								{!! Form::text('iso_num', $currency->iso_num, ['class' => 'form-control', 'placeholder' => 'ISO Number']) !!}
								 @if ($errors->has('iso_num')) <p class="help-block">{{ $errors->first('iso_num') }}</p> @endif
						</div>
                        <div class="form-group @if ($errors->has('cy_symbol')) has-error @endif">
                                 <label>Currency Symbol</label>
                                {!! Form::text('cy_symbol', $currency->cy_symbol, ['class' => 'form-control', 'placeholder' => 'Currency Symbol']) !!}
                                 @if ($errors->has('cy_symbol')) <p class="help-block">{{ $errors->first('cy_symbol') }}</p> @endif
                        </div>
                        <div class="form-group @if ($errors->has('unicode_decimal')) has-error @endif">
                                 <label>Unicode Decimal</label>
                                {!! Form::text('unicode_decimal', $currency->unicode_decimal, ['class' => 'form-control', 'placeholder' => 'Unicode Decimal']) !!}
                                 @if ($errors->has('unicode_decimal')) <p class="help-block">{{ $errors->first('unicode_decimal') }}</p> @endif
                        </div>
						<div class="form-group @if ($errors->has('active')) has-error @endif">
	                            {!! Form::select('active', ['1' => 'Active', '0' => 'Inactive' ],  $currency->active??null, ['class' => 'form-control']) !!}
	                            @if ($errors->has('active')) <p class="help-block">{{ $errors->first('active') }}</p> @endif
                        </div>
                        <div class="form-group">
                            <span class="input-group-btn">
                                {!! Form::submit('Edit', ['class' => 'btn btn-block green btn-lg'] ) !!}
                            </span>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
 </div>
