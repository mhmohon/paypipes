@extends('admin/html')

@section('content')
@include('admin/modules/settings/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->
<div class="portlet-body">
	<div class="row">
		<div class="col-sm-6  no-print">
			<div class="row">
			 @if (count($currencies) > 0)
				<div class="col-sm-3">
					@include('admin/includes/item_per_page')
				</div>
			@endif
				<div class="col-sm-9">
				{!! Form::open(['url' => 'admin/settings/currencies', 'method' => 'GET']) !!}
					<input type="hidden" name='rows' value="{{old('rows')}}" >
					<div class="input-group input-medium">
						{!! Form::text('search', Request::get('search'), ['class' => 'form-control input-medium', 'placeholder' => 'Search']) !!}
						<span class="input-group-btn">
							{!! Form::button('<i class="icon-magnifier"></i>', ['class' => 'btn green-jungle', 'type'=>'submit']) !!}
						</span>
					</div>
				{!! Form::close() !!}
				</div>
			</div>
		</div>
		@if (count($currencies) > 0)
		<div class="col-sm-6 text-right no-print" style="margin-top:5px">
			<a href="{{url('admin/settings/currencies/pdf-convert'.$pg)}}" class="btn btn-sm red"><i class="fa fa-file-pdf-o"></i> PDF</a>
			<a href="{{url('admin/settings/currencies/csv-convert'.$pg)}}" class="btn btn-sm green-jungle"><i class="fa fa-file-excel-o"></i> CSV</a>
		</div>
		@endif
	</div>
	 @if (count($currencies) == 0)
		 <p class="text-center font-grey-silver margin-top-15">There are no Curency to display</p>
	@else
	<div class="table-scrollable">
		<table class="table table-condensed table-bordered table-striped table-hover">
			<thead>
				<tr>
					<th style="width:1%">#</th>
					<th style="width:1%">Curency</th>
					<th style="width:1%">Curency</br>Code</th>
					<th style="width:1%">ISO</th>
					<th>Symbol</th>
					<th>Status</th>
					<th style="width:1%"  class='no-print'>Action</th>
				</tr>
			</thead>
			<tbody>
				@php
					$page = \Request::get('page');
					$i = ($page == '')?0:(($page*$item_per_page)-$item_per_page);
				@endphp
				@foreach ($currencies as $currency)
				<?php $i++ ?>
				<tr>
					<td>{!! $i !!}</td>
					<td>{!! $currency->currency !!}</td>
					<td>{!! $currency->cy_code !!}</td>
					<td>{!! $currency->iso_num !!}</td>
					<td>{!! $currency->cy_symbol !!}</td>
					<td><span class="label label-sm @if ($currency->active == '1') label-success @elseif ($currency->active == '0') label-default  @else label-default @endif">{!! ($currency->active==1?'Active':'Inactive') !!}</span></td>
					<td  class='no-print'>
						<a data-toggle="modal" href="#edit{!! $currency->currency_id !!}" class="btn blue btn-xs"><i class="fa fa-pencil"></i></a>
						@if ($currency->active == '1' )
							{!! Form::open(['method' => 'PATCH', 'url' => ['admin/settings/currencies/'. $currency->currency_id.'/lockunlock'], 'style' => 'display:inline-block;']) !!}
								{!! Form::hidden('active', '0') !!}
								{!! Form::button('<i class="fa fa-lock"></i>', ['class' => 'btn red btn-xs', 'type'=>'submit', 'data-toggle' => 'confirmation', 'data-popout' => 'true', 'data-singleton' => 'true', 'data-original-title' => 'Lock! Are you sure?', 'title' => '']) !!}
							{!! Form::close() !!}
						@elseif ($currency->active == '0')
							{!! Form::open(['method' => 'PATCH', 'url' => ['admin/settings/currencies/'. $currency->currency_id.'/lockunlock'], 'style' => 'display:inline-block;']) !!}
								{!! Form::hidden('active', '1') !!}
								{!! Form::button('<i class="fa fa-unlock-alt"></i>', ['class' => 'btn green-jungle btn-xs', 'type'=>'submit', 'data-toggle' => 'confirmation', 'data-popout' => 'true', 'data-singleton' => 'true', 'data-original-title' => 'Unlock! Are you sure?', 'title' => '']) !!}
							{!! Form::close() !!}
						@endif
					</td>
				</tr>
				@include('admin/modules/settings/systems/edit_currencies')
				@endforeach
			</tbody>
		</table>
	</div>
	<div class="row">
		<div class="col-md-6 margin-top-10">
			Showing {!! $currencies->firstItem() !!} to {!! $currencies->lastItem() !!} of {!! $currencies->total() !!}
		</div>
		<div class="col-md-6 text-right  no-print">
		 {{ $currencies->appends(Request::except('page'))->links() }}
		</div>
	</div>
	@endif
</div>
<!-- END PAGE CONTENT-->
@stop

