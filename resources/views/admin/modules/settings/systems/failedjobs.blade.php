@extends('admin/html')

@section('content')
@include('admin/modules/settings/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->
<div class="portlet-body">

	 @if (count($failedjobs) == 0)
		 <p class="text-center font-grey-silver margin-top-15">There are no Failed Job to display</p>
	@else
	<div class="table-scrollable">
		<table class="table table-condensed table-bordered table-striped table-hover">
			<thead>
				<tr>
					<th style="width:1%">#</th>
					<th style="width:1%">FailedAt</th>
					<th style="width:1%">Payload</th>
					<th style="width:1%">Exception</th>

				</tr>
			</thead>
			<tbody>
				@php
					$page = \Request::get('page');
					$i = ($page == '')?0:(($page*$item_per_page)-$item_per_page);
				@endphp
				@foreach ($failedjobs as $failedjob)
				<?php $i++ ?>
				<tr>
					<td>{!! $i !!}</td>
					<td>{!! $failedjob->failed_at !!}</td>
					<td>{!! $failedjob->payload->displayName !!}</td>
					<td>{!! substr($failedjob->exception,0, 150).'...' !!}</td>


				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	<div class="row">
		<div class="col-md-6 margin-top-10">
			Showing {!! $failedjobs->firstItem() !!} to {!! $failedjobs->lastItem() !!} of {!! $failedjobs->total() !!}
		</div>
		<div class="col-md-6 text-right  no-print">
		 {{ $failedjobs->appends(Request::except('page'))->links() }}
		</div>
	</div>
	@endif
</div>
<!-- END PAGE CONTENT-->
@stop

