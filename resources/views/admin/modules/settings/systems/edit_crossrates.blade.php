<div class="modal fade" id="edit{!! @$crossrate->id !!}" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Edit Crossrate</h4>
            </div>
            <div class="modal-body">
            	 {!! Form::model($crossrate, ['method' => 'PATCH', 'url' => url('admin/settings/crossrates/'. @$crossrate->id)]) !!}
                    <div class="form-body">
						<div class="form-group @if ($errors->has('from_currency')) has-error @endif">
								 <label>From Currency</label>
								{!! Form::text('from_currency', $crossrate->from_currency, ['class' => 'form-control', 'placeholder' => 'From Currency']) !!}
								 @if ($errors->has('from_currency')) <p class="help-block">{{ $errors->first('from_currency') }}</p> @endif
						</div>
						<div class="form-group @if ($errors->has('to_currency')) has-error @endif">
								 <label>To Currency</label>
								{!! Form::text('to_currency',  $crossrate->to_currency, ['class' => 'form-control', 'placeholder' => 'To Currency']) !!}
								@if ($errors->has('to_currency')) <p class="help-block">{{ $errors->first('to_currency') }}</p> @endif
						</div>
						<div class="form-group @if ($errors->has('to_currency')) has-error @endif">
								 <label>Crossrate</label>
								{!! Form::text('crossrate', $crossrate->crossrate, ['class' => 'form-control', 'placeholder' => 'Amount']) !!}
								 @if ($errors->has('crossrate')) <p class="help-block">{{ $errors->first('crossrate') }}</p> @endif
						</div>
						<div class="form-group @if ($errors->has('status')) has-error @endif">
	                            {!! Form::select('status', ['Active' => 'Active', 'Inactive' => 'Inactive', 'Pending' => 'Pending' ],  $crossrate->status??null, ['class' => 'form-control']) !!}
	                            @if ($errors->has('status')) <p class="help-block">{{ $errors->first('status') }}</p> @endif
                        </div>
                        <div class="form-group">
                            <span class="input-group-btn">
                                {!! Form::submit('Edit', ['class' => 'btn btn-block green btn-lg'] ) !!}
                            </span>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
 </div>
