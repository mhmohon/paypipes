<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add Crossrate</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['url' => url('admin/settings/crossrates')]) !!}
                    <div class="form-body">
						<div class="form-group @if ($errors->has('from_currency')) has-error @endif">
								 <label>From Currency</label>
								{!! Form::text('from_currency', null, ['class' => 'form-control', 'placeholder' => 'From Currency']) !!}
						</div>
						<div class="form-group @if ($errors->has('to_currency')) has-error @endif">
								 <label>To Currency</label>
								{!! Form::text('to_currency', null, ['class' => 'form-control', 'placeholder' => 'To Currency']) !!}
						</div>
						<div class="form-group @if ($errors->has('crossrate')) has-error @endif">
								 <label>Crossrate</label>
								{!! Form::text('crossrate', null, ['class' => 'form-control', 'placeholder' => 'Amount']) !!}
								 @if ($errors->has('crossrate')) <p class="help-block">{{ $errors->first('crossrate') }}</p> @endif
						</div>
						<div class="form-group @if ($errors->has('status')) has-error @endif">
                            {!! Form::select('status', ['Active' => 'Active','Inactive' => 'Inactive','Pending' => 'Pending'],  null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
                            @if ($errors->has('status')) <p class="help-block">{{ $errors->first('status') }}</p> @endif
                        </div>
                        <div class="form-group">
                            <span class="input-group-btn">
                                {!! Form::submit('Add', ['class' => 'btn btn-block green btn-lg'] ) !!}
                            </span>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
 </div>
