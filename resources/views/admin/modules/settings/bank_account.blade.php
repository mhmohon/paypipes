@extends('admin/html')

@section('content')
@include('admin/modules/settings/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Edit a Bank Account</h4>
			</div>			
			<div class="modal-body"> 
				{!! Form::model($bankaccount, ['method' => 'POST', 'action' => ['Admin\Modules\Settings\BankaccountsController@update', $bankaccount->id ]]) !!}
					<div class="form-body">			
						<div class="form-group @if ($errors->has('account_holder')) has-error @endif">
							<label>Account Holder</label>
							{!! Form::text('account_holder', null, ['class' => 'form-control', 'placeholder' => 'Account Holder']) !!}
							@if ($errors->has('account_holder')) <p class="help-block">{{ $errors->first('account_holder') }}</p> @endif
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-6">
									<div class="@if ($errors->has('account_number')) has-error @endif">
										<label>Account Number</label>
										{!! Form::text('account_number', null, ['class' => 'form-control', 'placeholder' => 'Account Number']) !!}
										@if ($errors->has('account_number')) <p class="help-block">{{ $errors->first('account_number') }}</p> @endif
									</div> 
								</div>
								<div class="col-md-6">
									<div class="@if ($errors->has('currency')) has-error @endif">
										<label>Currency</label>
										{!! Form::select('currency', $currencies, null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
										@if ($errors->has('currency')) <p class="help-block">{{ $errors->first('currency') }}</p> @endif
									</div>
								</div>												
							</div>									
						</div>
						<div class="form-group @if ($errors->has('bank_name')) has-error @endif">
							<label>Bank Name</label>
							{!! Form::text('bank_name', null, ['class' => 'form-control', 'placeholder' => 'Bank Name']) !!}
							@if ($errors->has('bank_name')) <p class="help-block">{{ $errors->first('bank_name') }}</p> @endif
						</div>
						<div class="form-group @if ($errors->has('bank_address')) has-error @endif">
							<label>Bank Address</label>
							{!! Form::text('bank_address', null, ['class' => 'form-control', 'placeholder' => 'Bank Address']) !!}
							@if ($errors->has('bank_address')) <p class="help-block">{{ $errors->first('bank_address') }}</p> @endif
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-8">
									<div class="@if ($errors->has('iban')) has-error @endif">
										<label>IBAN</label>
										{!! Form::text('iban', null, ['class' => 'form-control', 'placeholder' => 'IBAN']) !!}
										@if ($errors->has('iban')) <p class="help-block">{{ $errors->first('iban') }}</p> @endif
									</div>
								</div>	
								<div class="col-md-4">
									<div class="@if ($errors->has('swift_bic')) has-error @endif">								
										<label>SWIFT/BIC</label>
										{!! Form::text('swift_bic', null, ['class' => 'form-control', 'placeholder' => 'SWIFT/BIC']) !!}
										@if ($errors->has('swift_bic')) <p class="help-block">{{ $errors->first('swift_bic') }}</p> @endif
									</div>
								</div>                                											
							</div>									
						</div>
						<div class="form-group">
							<span class="input-group-btn">
								{!! Form::submit('Edit', ['class' => 'btn btn-block blue btn-lg'] ) !!}
							</span>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<div class="portlet">
	<div class="table-scrollable">
		<table class="table table-light table-hover">
			<tbody>	
				<tr>
					<td>Status</td>
					<td><span class="label label-sm @if ($bankaccount->status === 'Active') label-success @elseif ($bankaccount->status === 'Inactive') label-default @else label-default @endif">{!! $bankaccount->status !!}</span></td>
				</tr>
				<tr>
					<td>Account Holder</td>
					<td>{!! $bankaccount->account_holder !!}</td>
				</tr>
				<tr>
					<td style="width:30%">Account Number</td>
					<td>{!! $bankaccount->account_number !!}</td>
				</tr>
				<tr>
					<td>Currency</td>
					<td>{!! $bankaccount->currency !!}</td>
				</tr>
				<tr>
					<td>Bank Name</td>
					<td>{!! $bankaccount->bank_name !!}</td>
				</tr>
				<tr>
					<td>Bank Address</td>
					<td>{!! $bankaccount->bank_address !!}</td>
				</tr>
				<tr>
					<td>IBAN</td>
					<td>{!! $bankaccount->iban !!}</td>
				</tr>
				<tr>
					<td>SWIFT/BIC</td>
					<td>{!! $bankaccount->swift_bic !!}</td>
				</tr>
			</tbody>
		</table>
	</div> 
</div>
<!-- END PAGE CONTENT-->
@stop

