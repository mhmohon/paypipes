@extends('admin/html')

@section('content')
@include('admin/modules/settings/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->
<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Add a Bank Account</h4>
			</div>
			<div class="modal-body">
				{!! Form::open(['url' => 'admin/settings/bank_accounts']) !!}
					<div class="form-body">
						<div class="form-group @if ($errors->has('account_holder')) has-error @endif">
							<label>Account Holder</label>
							{!! Form::text('account_holder', null, ['class' => 'form-control', 'placeholder' => 'Account Holder']) !!}
							@if ($errors->has('account_holder')) <p class="help-block">{{ $errors->first('account_holder') }}</p> @endif
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-6">
									<div class="@if ($errors->has('account_number')) has-error @endif">
										<label>Account Number</label>
										{!! Form::text('account_number', null, ['class' => 'form-control', 'placeholder' => 'Account Number']) !!}
										@if ($errors->has('account_number')) <p class="help-block">{{ $errors->first('account_number') }}</p> @endif
									</div>
								</div>
								<div class="col-md-6">
									<div class="@if ($errors->has('currency')) has-error @endif">
										<label>Currency</label>
										{!! Form::select('currency', $currencies, null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
										@if ($errors->has('currency')) <p class="help-block">{{ $errors->first('currency') }}</p> @endif
									</div>
								</div>
							</div>
						</div>
						<div class="form-group @if ($errors->has('bank_name')) has-error @endif">
							<label>Bank Name</label>
							{!! Form::text('bank_name', null, ['class' => 'form-control', 'placeholder' => 'Bank Name']) !!}
							@if ($errors->has('bank_name')) <p class="help-block">{{ $errors->first('bank_name') }}</p> @endif
						</div>
						<div class="form-group @if ($errors->has('bank_address')) has-error @endif">
							<label>Bank Address</label>
							{!! Form::text('bank_address', null, ['class' => 'form-control', 'placeholder' => 'Bank Address']) !!}
							@if ($errors->has('bank_address')) <p class="help-block">{{ $errors->first('bank_address') }}</p> @endif
						</div>
						<div class="form-group">
							<div class="row">
								<div class="col-md-8">
									<div class="@if ($errors->has('iban')) has-error @endif">
										<label>IBAN</label>
										{!! Form::text('iban', null, ['class' => 'form-control', 'placeholder' => 'IBAN']) !!}
										@if ($errors->has('iban')) <p class="help-block">{{ $errors->first('iban') }}</p> @endif
									</div>
								</div>
								<div class="col-md-4">
									<div class="@if ($errors->has('swift_bic')) has-error @endif">
										<label>SWIFT/BIC</label>
										{!! Form::text('swift_bic', null, ['class' => 'form-control', 'placeholder' => 'SWIFT/BIC']) !!}
										@if ($errors->has('swift_bic')) <p class="help-block">{{ $errors->first('swift_bic') }}</p> @endif
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<span class="input-group-btn">
								{!! Form::submit('Add', ['class' => 'btn btn-block green-jungle btn-lg'] ) !!}
							</span>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
<div class="portlet">
	@if (count($bankaccounts) == 0)
		 <p class="text-center font-grey-silver margin-top-15">There are no bank accounts to display</p>
	@else
	<div class="table-scrollable">
		<table class="table table-condensed table-bordered table-striped table-hover">
			<thead>
				<tr>
					<th style="width:1%">#</th>
					<th style="width:1%">Account</br>Holder</th>
					<th style="width:1%">Account</br>Number</th>
					<th style="width:1%">Currency</th>
					<th>Bank</br>Name</th>
					<th style="width:1%">IBAN</th>
					<th style="width:1%">SWIFT</br>BIC</th>
					<th style="width:1%">Status</th>
					<th style="width:1%">Action</th>
				</tr>
			</thead>
			<tbody>
				@php
					$page = \Request::get('page');
					$i = ($page == '')?0:(($page*$item_per_page)-$item_per_page);
				@endphp
				@foreach ($bankaccounts as $bankaccount)
				<?php $i++ ?>
				<tr>
					<td>{!! $i !!}</td>
					<td>{!! $bankaccount->account_holder !!}</td>
					<td>{!! $bankaccount->account_number !!}</td>
					<td>{!! $bankaccount->currency !!}</td>
					<td>{!! $bankaccount->bank_name !!}</td>
					<td>{!! $bankaccount->iban !!}</td>
					<td>{!! $bankaccount->swift_bic !!}</td>
					<td><span class="label label-sm @if ($bankaccount->status === 'Active') label-success @elseif ($bankaccount->status === 'Inactive') label-default @else label-default @endif">{!! $bankaccount->status !!}</span></td>
					<td  class='no-print'>
						<a href="/admin/settings/bank_accounts/{!! $bankaccount->id !!}" class="btn blue btn-xs"><i class="fa fa-pencil"></i></a>
						@if ($bankaccount->status === 'Active')
							{!! Form::open(['method' => 'PATCH', 'action' => ['Admin\Modules\Settings\BankaccountsController@lockunlock', $bankaccount->id], 'style' => 'display:inline-block;']) !!}
								{!! Form::hidden('status', 'Inactive') !!}
								{!! Form::button('<i class="fa fa-lock"></i>', ['class' => 'btn red btn-xs', 'type'=>'submit', 'data-toggle' => 'confirmation', 'data-popout' => 'true', 'data-singleton' => 'true', 'data-original-title' => 'Deactivate! Are you sure?', 'title' => '']) !!}
							{!! Form::close() !!}
						@elseif ($bankaccount->status === 'Inactive')
							{!! Form::open(['method' => 'PATCH', 'action' => ['Admin\Modules\Settings\BankaccountsController@lockunlock', $bankaccount->id], 'style' => 'display:inline-block;']) !!}
								{!! Form::hidden('status', 'Active') !!}
								{!! Form::button('<i class="fa fa-unlock-alt"></i>', ['class' => 'btn green-jungle btn-xs', 'type'=>'submit', 'data-toggle' => 'confirmation', 'data-popout' => 'true', 'data-singleton' => 'true', 'data-original-title' => 'Activate! Are you sure?', 'title' => '']) !!}
							{!! Form::close() !!}
						@endif
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	<div class="row">
		<div class="col-md-6 margin-top-10">
			Showing {!! $bankaccounts->firstItem() !!} to {!! $bankaccounts->lastItem() !!} of {!! $bankaccounts->total() !!}
		</div>
		<div class="col-md-6 text-right  no-print">
			{!! $bankaccounts->links() !!}
		</div>
	</div>
	@endif
</div>
<!-- END PAGE CONTENT-->
@stop




