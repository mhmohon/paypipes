@extends('admin/html')
@section('content') 
@include('admin/modules/gateways/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->
<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add a Payment Method</h4>
            </div>
            <div class="modal-body">															                           
                {!! Form::open(['url' => 'admin/paymentmethods', 'files' => true]) !!}
                    <div class="form-body">
                        <div class="form-group @if ($errors->has('name')) has-error @endif">
                            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name']) !!}
                            @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
                        </div>
                        <div class="form-group @if ($errors->has('short_code')) has-error @endif">
                            {!! Form::text('short_code', null, ['class' => 'form-control', 'placeholder' => 'Short code']) !!}
                            @if ($errors->has('short_code')) <p class="help-block">{{ $errors->first('short_code') }}</p> @endif
                        </div>
                        <div class="form-group @if ($errors->has('description')) has-error @endif">
                            {!! Form::text('description', null, ['class' => 'form-control', 'placeholder' => 'Description']) !!}
                            @if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
                        </div>
						<div class="form-group @if ($errors->has('base_url')) has-error @endif">						
							{!! Form::text('base_url', null, ['class' => 'form-control', 'placeholder' => 'Base Url']) !!}
							@if ($errors->has('base_url')) <p class="help-block">{{ $errors->first('base_url') }}</p> @endif
						</div>	
						<div class="form-group">
							{!! Form::file('upload_file') !!}
							@if ($errors->has('upload_file')) <p class="help-block">{{ $errors->first('upload_file') }}</p> @endif
						</div>						
                        <div class="form-group">
                            <span class="input-group-btn">
                                {!! Form::submit('Add', ['class' => 'btn btn-block green-jungle btn-lg'] ) !!}
                            </span>
                        </div>
                    </div>
                {!! Form::close() !!}                   
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>	
<div class="tiles">
    @foreach ($payment_methods as $payment_method)
	<a href="/admin/paymentmethods/{!! $payment_method->payment_method_id !!}">
	<div class="tile @if ($payment_method->status === 'Active') selected bg-green-jungle @else bg-grey-silver @endif">
		<div class="corner"> </div>
		<div class="check"> </div>
		<div class="tile-body text-center">
			<p class="margin-top-20">
				<img src="{!! $payment_method->file_path !!}" style="height:45px; background-color:#fff; padding-bottom:0px; margin-bottom:10px;">
			</p>										
			<h4>{!! $payment_method->description !!}</h4>
		</div>
	</div>
	</a>
	@endforeach
</div>
<!-- END PAGE CONTENT-->
@stop

