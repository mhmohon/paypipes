@extends('admin/html')

@section('content')
@include('admin/modules/gateways/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->

@if(isset($payment_method))

@if(isset($payment_method->short_code))
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
     <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title">Edit a Payment Method</h4>
        </div>
        {!! Form::model($payment_method, ['method' => 'POST', 'files' => 'true', 'url' => url('admin/paymentmethods/'. @$payment_method->payment_method_id)]) !!}
        <div class="modal-body">
            {{csrf_field()}}
            <input type="hidden" name="payment_method_id" value="{{$payment_method->payment_method_id}}">

            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                         <div class="@if ($errors->has('name')) has-error @endif">
                            <label>Name</label>
                            {!! Form::text('name', $payment_method->name, ['class' => 'form-control', 'placeholder' => 'Name']) !!}
                            @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
                        </div>
                   </div>
                   <div class="col-md-6">
                       <div class="@if ($errors->has('short_code')) has-error @endif">
                            <label>Short Code</label>
                            {!! Form::text('short_code', $payment_method->short_code, ['class' => 'form-control', 'placeholder' => 'Short Code']) !!}
                            @if ($errors->has('short_code')) <p class="help-block">{{ $errors->first('short_code') }}</p> @endif
                       </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                       <div class="@if ($errors->has('file_name')) has-error @endif">
                          <label>File Name</label>
                          {!! Form::text('file_name', $payment_method->file_name, ['class' => 'form-control', 'placeholder' => 'File Name' , 'readpnly' => 'readonly']) !!}
                          @if ($errors->has('file_name')) <p class="help-block">{{ $errors->first('file_name') }}</p> @endif
                      </div>
                  </div>
                  <div class="col-md-6">
                     <div class="@if ($errors->has('description')) has-error @endif">
                        <label>Description</label>
                        {!! Form::text('description', $payment_method->description, ['class' => 'form-control', 'placeholder' => 'Description']) !!}
                        @if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
                    </div>
                  </div>
                </div>
            </div>
            <div class="form-group @if ($errors->has('base_url')) has-error @endif">
                    <label>Base Url</label>
                    {!! Form::text('base_url', $payment_method->base_url, ['class' => 'form-control', 'placeholder' => 'Base Url']) !!}
                    @if ($errors->has('base_url')) <p class="help-block">{{ $errors->first('base_url') }}</p> @endif
            </div>
            <div class="form-group @if ($errors->has('field_description')) has-error @endif">
                    <label>Field Descriptioin</label>
                    {!! Form::textarea('field_description', $payment_method->field_description, ['class' => 'form-control', 'placeholder' => 'Field Descriptioin']) !!}
                    @if ($errors->has('field_description')) <p class="help-block">{{ $errors->first('field_description') }}</p> @endif
            </div>
            <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group @if ($errors->has('purchase')) has-error @endif">
                                <label>Purchase Transaction</label>
                                {!! Form::select('purchase', ['1' => 'Yes','0' => 'No'], null, ['id' => 'purchase','class' => 'bs-select form-control', 'required' => 'required']) !!}
                                @if ($errors->has('purchase')) <p class="help-block">{{ $errors->first('purchase') }}</p> @endif
                            </div>
                        </div>
                         <div class="col-md-6">
                            <div class="form-group @if ($errors->has('authorize')) has-error @endif">
                                <label>Authorize Transaction</label>
                               {!! Form::select('authorize', ['1' => 'Yes','0' => 'No'], null, ['id' => 'is_test','class' => 'bs-select form-control', 'required' => 'required']) !!}
                                @if ($errors->has('authorize')) <p class="help-block">{{ $errors->first('authorize') }}</p> @endif
                            </div>
                        </div>
                    </div>
                </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                           {!! Form::file('upload_file') !!}
                           @if ($errors->has('upload_file')) <p class="help-block">{{ $errors->first('upload_file') }}</p> @endif
                    </div>
                    <div class="col-md-6">
                         @if(!empty($payment_method->file_name))<img src="/uploads/paymentmethods/{!! $payment_method->file_name !!}" style="max-height:55px;max-width:100px">@endif
                    </div>
                </div>
            </div>

            <div class="form-group @if ($errors->has('status')) has-error @endif">
                <label>Status</label>
                {!! Form::select('status', ['Active' => 'Active','Inactive' => 'Inactive'],  null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
                @if ($errors->has('status')) <p class="help-block">{{ $errors->first('status') }}</p> @endif
            </div>

            <div class="form-group">
               <span class="input-group-btn">
                  {!! Form::submit('Edit', ['class' => 'btn btn-block green-jungle btn-lg'] ) !!}
              </span>
          </div>
      </div>

      {!! Form::close() !!}
  </div>
</div>
<!-- /.modal-content -->
</div>
<!-- /.modal-dialog -->

<div class="table-scrollable">
  <table class="table table-light table-hover">
     <tbody>
        <tr>
           <td style="width:30%">Status</td>
           <td><span class="label label-sm @if ($payment_method->status === 'Active') label-success @elseif ($payment_method->status === 'Inactive') label-default @else label-default @endif">{!! $payment_method->status !!}</span></td>
       </tr>
       <tr>
           <td style="width:30%">Payment Method</td>
           <td>{!! $payment_method->name !!}</td>
       </tr>
       <tr>
           <td style="width:30%">Short Code</td>
           <td>{!! $payment_method->short_code !!}</td>
       </tr>

       <tr>
           <td style="width:30%">File Name</td>
           <td>{!! $payment_method->file_name !!}</td>
       </tr>

       <tr>
           <td style="width:30%">Description</td>
           <td>{!! $payment_method->description !!}</td>
       </tr>
       <tr>
           <td style="width:30%">Base Url</td>
           <td>{!! $payment_method->base_url !!}</td>
       </tr>
       <tr>
        <td style="width:30%">Purchase</td>
        <td>{!! Common::numberToString($payment_method->purchase) !!}</td>
    </tr>
    <tr>
        <td style="width:30%">Authorize</td>
        <td>{!! Common::numberToString($payment_method->authorize) !!}</td>
    </tr>
    <tr>
       <td style="width:30%">Field Description</td>
       <td>{!! $payment_method->field_description !!}</td>
   </tr>
   <tr>
       <td style="width:30%">Gateway Logo</td>
       <td>@if(!empty($payment_method->file_name))<img src="/uploads/paymentmethods/{!! $payment_method->file_name !!}" style="max-height:55px;max-width:100px">@endif</td>
   </tr>

</tbody>
</table>
</div>
@else
<h3 class="page-title">Not available</h3>
@endif
@else
<h3 class="page-title">Not available</h3>
@endif
<!-- END PAGE CONTENT-->
@stop

