<!-- BEGIN PAGE HEADER-->
<div class="page-bar">
    <h3 class="page-title" style="float:left;">{!! $pagetitle !!}</h3>
	@if (Request::is('admin/paymentmethods')) <a class="btn green-jungle bold" data-toggle="modal" href="#add" style="margin:10px;">Add <i class="fa fa-plus"></i></a>
    @elseif (Request::is('admin/paymentmethods/'.$payment_method_id)) <a class="btn blue bold" data-toggle="modal" href="#edit" style="margin:10px;">Edit <i class="fa fa-pencil"></i></a>
    @endif
    <ul class="page-breadcrumb" style="float:right;padding-top:18px;">
        @if(isset($breadcrumb_level1) && !empty($breadcrumb_level1))
		<li>
            {!! $breadcrumb_level1 or '' !!}
            {!! Request::is('admin/paymentmethods/*') ? '<i class="fa fa-angle-right"></i>' : '' !!}
        </li>
		@endif
		@if(isset($breadcrumb_level2) && !empty($breadcrumb_level2))
        <li>
            {!! $breadcrumb_level2 or '' !!}
        </li>
		@endif
		@if(isset($breadcrumb_level3) && !empty($breadcrumb_level3))
        <li>
            {!! $breadcrumb_level3 or '' !!}
        </li>
		@endif
    </ul>
</div>
<!-- END PAGE HEADER-->
