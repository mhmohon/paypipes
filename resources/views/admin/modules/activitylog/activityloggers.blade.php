@extends('admin/html')

@section('content')
@include('admin/modules/activitylog/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->
<div class="portlet-body">
    @if (count($activities) == 0)
    <div class="row">
        <div class="col-sm-6">
            {!! Form::open(['action' => 'Admin\Modules\Activitylog\ActivityLogController@index', 'method' => 'GET']) !!}
                <div class="input-group input-medium">
                    {!! Form::text('search', Request::get('search'), ['class' => 'form-control input-medium', 'placeholder' => 'Search']) !!}
                    <span class="input-group-btn">
                        {!! Form::button('<i class="icon-magnifier"></i>', ['class' => 'btn green-jungle', 'type'=>'submit']) !!}
                    </span>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
    <p class="text-center font-grey-silver margin-top-15">There are no activities to display</p>
    @else
    <div class="row">
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-3">
                    @include('admin/includes/item_per_page')
                </div>
                <div class="col-sm-9">
                    {!! Form::open(['url' => 'admin\activity', 'method' => 'GET']) !!}
                        <div class="input-group input-medium">
                            {!! Form::text('search', Request::get('search'), ['class' => 'form-control input-medium', 'placeholder' => 'Search']) !!}
                            <span class="input-group-btn">
                                {!! Form::button('<i class="icon-magnifier"></i>', ['class' => 'btn green-jungle', 'type'=>'submit']) !!}
                            </span>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <a class="btn red" data-toggle="confirmation" data-placement="bottom" data-popout="true" data-singleton="true" data-original-title="DELETE! Are you sure?" style="" href="{{url('admin/activity/cleared')}}">Cleared</a>
        </div>
    </div>

    <div class="table-scrollable">
        <table class="table table-condensed table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th style="width:1%">#</th>
                    <th style="width:3%">Time</th>
                    <th style="width:10%">Description</th>
                    <th style="width:1%">UserAccess</th>
                    <th style="width:1%">User</th>
                    <th style="width:1%">Method</th>
                    <th style="width:1%">Route</th>
                    <th style="width:3%">Ip</th>
                    <th style="width:3%">Agent</th>
                    <th style="width:3%">Action</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $page = \Request::get('page');
                    $i = ($page == '')?0:(($page*ROW_PER_PAGE)-ROW_PER_PAGE);
                @endphp
                @foreach ($activities as $activity)
                <?php $i++; ?>
                <tr>
                    <td>{!! $i !!}</td>
                    <td>{!! $activity->created_at->diffForHumans() !!}</td>
                    <td>{!! $activity->description !!}</td>
                    <td>{!! $activity->userAccess !!}</td>
                    <td>{!! $activity->userDetails->name??'' !!}</td>
                    <td>{!! $activity->methodType !!}</td>
                    <td>{!! showCleanRoutUrl($activity->route) !!}</td>
                    <td>{!! $activity->ipAddress !!}</td>
                    <td>{!! $activity->locale !!}</td>
                   <td  class="no-print">
                    <a href="/admin/activity/log/{!! $activity->id !!}" class="btn blue btn-xs"><i class="fa fa-pencil"></i></a>
                    {!! Form::open(['method' => 'DELETE', 'url' => 'admin/activity/log/'.$activity->id, 'style' => 'display:inline-block;']) !!}
                       {!! Form::button('<i class="fa fa-recycle"></i>', ['class' => 'btn dark btn-xs', 'type'=>'submit', 'data-toggle' => 'confirmation', 'data-popout' => 'true', 'data-singleton' => 'true', 'data-original-title' => 'DELETE! Are you sure?', 'title' => '']) !!}
                    {!! Form::close() !!}
                </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="row">
        <div class="col-md-6 margin-top-10">
            Showing {!! $activities->firstItem() !!} to {!! $activities->lastItem() !!} of {!! $activities->total() !!}
        </div>
        <div class="col-md-6 text-right">
            {!! $activities->links() !!}
        </div>
    </div>
    @endif
</div>
<!-- END PAGE CONTENT-->
@stop
