<!-- BEGIN PAGE HEADER-->
<div class="page-bar">
    <h3 class="page-title" style="float:left;">{!! $pagetitle !!}</h3>
    <ul class="page-breadcrumb" style="float:right;padding-top:18px;">
        <li>
            {!! $breadcrumb_level1 or '' !!}
            {!! Request::is('activitylog/*') ? '<i class="fa fa-angle-right"></i>' : '' !!}
        </li>
    </ul>
</div>
<!-- END PAGE HEADER-->
