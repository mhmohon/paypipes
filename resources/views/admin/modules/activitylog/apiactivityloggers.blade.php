@extends('admin/html')
<style>
.tooltip {
    position: relative;
    display: inline-block;
    border-bottom: 1px dotted black;
}

.tooltip .tooltiptext {
    visibility: hidden;
    width: 120px;
    background-color: black;
    color: #fff;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;

    /* Position the tooltip */
    position: absolute;
    z-index: 1;
}

.tooltip:hover .tooltiptext {
    visibility: visible;
}
</style>
@section('content')
@include('admin/modules/activitylog/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->
<div class="portlet-body">
  @if (count($activities) == 0)
    <div class="row">
        <div class="col-sm-6">
            {!! Form::open(['url' => 'admin\apiactivity', 'method' => 'GET']) !!}
                <div class="input-group input-medium">
                    {!! Form::text('search', Request::get('search'), ['class' => 'form-control input-medium', 'placeholder' => 'Search']) !!}
                    <span class="input-group-btn">
                        {!! Form::button('<i class="icon-magnifier"></i>', ['class' => 'btn green-jungle', 'type'=>'submit']) !!}
                    </span>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
    <p class="text-center font-grey-silver margin-top-15">There are no activities to display</p>
    @else
    <div class="row">
        <div class="col-sm-6">
            <div class="row">
                <div class="col-sm-3">
                    @include('admin/includes/item_per_page')
                </div>
                <div class="col-sm-9">
                    {!! Form::open(['url' => 'admin\apiactivity', 'method' => 'GET']) !!}
                        <div class="input-group input-medium">
                            {!! Form::text('search', Request::get('search'), ['class' => 'form-control input-medium', 'placeholder' => 'Search']) !!}
                            <span class="input-group-btn">
                                {!! Form::button('<i class="icon-magnifier"></i>', ['class' => 'btn green-jungle', 'type'=>'submit']) !!}
                            </span>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <a class="btn red" data-toggle="confirmation" data-placement="bottom" data-popout="true" data-singleton="true" data-original-title="DELETE! Are you sure?" style="" href="{{url('admin/apiactivity/cleared')}}">Cleared</a>
        </div>
    </div>

    <div class="table-scrollable">
        <table class="table table-condensed table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th style="width:1%">#</th>
                    <th style="width:3%">Time</th>
                    <th style="width:10%">Description</th>
                    <th style="width:1%">Order Id</th>
                    <th style="width:1%">Transaction Type</th>
                    <th style="width:1%">Body</th>
                    <th style="width:1%">Send To</th>
                     <th style="width:1%">Route</th>
                    <th style="width:3%">Ip</th>
                    <th style="width:3%">Action</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $page = \Request::get('page');
                    $i = ($page == '')?0:(($page*$item_per_page)-$item_per_page);
                @endphp
                @foreach ($activities as $activity)
                <?php $i++; ?>
                <tr>
                    <td>{!! $i !!}</td>
                    <td>{!! $activity->created_at->diffForHumans() !!}</td>
                    <td>{!! $activity->description !!}</td>
                    <td>{!! $activity->order_id??'' !!}</td>
                    <td>{!! $activity->transaction_type??'' !!}</td>
                    <td>{!! $activity->body !!}</td>
                    <td>{!! $activity->sendTo??'' !!}</td>
                    <td>{!! showCleanRoutUrl($activity->route) !!}</td>
                    <td>{!! $activity->ipAddress !!}</td>

                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="row">
        <div class="col-md-6 margin-top-10">
            Showing {!! $activities->firstItem() !!} to {!! $activities->lastItem() !!} of {!! $activities->total() !!}
        </div>
        <div class="col-md-6 text-right">
            {{ $activities->appends(Request::except('page'))->links() }}
        </div>
    </div>
    @endif
</div>
<!-- END PAGE CONTENT-->
@stop
