@extends('admin/html')

@section('content')
@include('admin/modules/activitylog/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->
<div class="portlet-body">
    <div class="row">
        <div class="col-sm-4">
            <div class="table-scrollable">
                <table class="table table-light table-hover">
                    <tbody>
                        <tr>
                            <td style="font-weight: bold;" colspan="2">Activity Details</td>
                        </tr>
                        <tr>
                            <td style="width:30%">Activity Log ID</td>
                            <td>{!! @$activity->id??'N/A' !!}</td>
                        </tr>
                        <tr>
                            <td style="width:30%">Description</td>
                            <td>{!! @$activity->description??'N/A' !!}</td>
                        </tr>
                        <tr>
                            <td style="width:30%">Route
</td>
                            <td>{!! showCleanRoutUrl($activity->route) !!}</td>
                        </tr>
                        <tr>
                            <td style="width:30%">Locale
</td>
                            <td>{!! @$langDetails??'N/A' !!}</td>
                        </tr>
                        <tr>
                            <td style="width:30%">Referer</td>
                            <td>{!! @$activity->Referer??'N/A' !!}</td>
                        </tr>
                        <tr>
                            <td style="width:30%">Method Type</td>
                            <td>{!! @$activity->methodType??'N/A' !!}</td>
                        </tr>
                        <tr>
                            <td style="width:30%">Time Passed</td>
                            <td>{!! @$timePassed??'N/A' !!}</td>
                        </tr>
                        <tr>
                            <td style="width:30%">Event Time
</td>
                            <td>{!! @$eventTime??'N/A' !!}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="table-scrollable">
                <table class="table table-light table-hover">
                    <tbody>
                        <tr>
                            <td style="font-weight: bold;" colspan="2">Ip Address Details</td>
                        </tr>
                        <tr>
                            <td style="width:30%">Ip Address
</td>
                            <td>{!! @$activity->ipAddress??'N/A' !!}</td>
                        </tr>
                        <tr>
                            <td style="width:30%">city</td>
                            <td>{!! @$ipAddressDetails['city']??'N/A' !!}</td>
                        </tr>
                        <tr>
                            <td style="width:30%">country</td>
                            <td>{!! @$ipAddressDetails['country']??'N/A' !!}</td>
                        </tr>
                        <tr>
                            <td style="width:30%">continent</td>
                            <td>{!! @$ipAddressDetails['continent']??'N/A' !!}</td>
                        </tr>
                        <tr>
                            <td style="width:30%">continent_code</td>
                            <td>{!! @$ipAddressDetails['continent_code']??'N/A' !!}</td>
                        </tr>
                        <tr>
                            <td style="width:30%">latitude</td>
                            <td>{!! @$ipAddressDetails['latitude']??'N/A' !!}</td>
                        </tr>
                        <tr>
                            <td style="width:30%">longitude</td>
                            <td>{!! @$ipAddressDetails['longitude']??'N/A' !!}</td>
                        </tr>
                        <tr>
                            <td style="width:30%">currencyCode</td>
                            <td>{!! @$ipAddressDetails['currencyCode']??'N/A' !!}</td>
                        </tr>
                        <tr>
                            <td style="width:30%">areaCode</td>
                            <td>{!! @$ipAddressDetails['areaCode']??'N/A' !!}</td>
                        </tr>
                        <tr>
                            <td style="width:30%">dmaCode</td>
                            <td>{!! @$ipAddressDetails['dmaCode']??'N/A' !!}</td>
                        </tr>
                        <tr>
                            <td style="width:30%">region</td>
                            <td>{!! @$ipAddressDetails['region']??'N/A' !!}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="table-scrollable">
                <table class="table table-light table-hover">
                    <tbody>
                        <tr>
                            <td style="font-weight: bold;" colspan="2">User Details</td>
                        </tr>
                        <tr>
                            <td style="width:30%">User Type</td>
                            <td>{!! @$activity->userType??'N/A' !!}</td>
                        </tr>
                        <tr>
                            <td style="width:30%">User Id</td>
                            <td>{!! @$activity->userId??'N/A' !!}</td>
                        </tr>
                        <tr>
                            <td style="width:30%">Username</td>
                            <td>{!! @$userDetails->name??'N/A' !!}</td>
                        </tr>
                        <tr>
                            <td style="width:30%">User Email
</td>
                            <td>{!! @$userDetails->email??'N/A' !!}</td>
                        </tr>
                        <tr>
                            <td style="width:30%">Full Name</td>
                            <td>{!! @$userDetails->first_name??''.@$userDetails->last_name??'' !!}</td>
                        </tr>
                        <tr>
                            <td style="width:30%">Created</td>
                            <td>{!! @$userDetails->created_at? $userDetails->created_at->format('D d M Y h:i:s P'):'N/A' !!}</td>
                        </tr>
                        <tr>
                            <td style="width:30%">Updated</td>
                            <td>{!! @$userDetails->created_at? $userDetails->created_at->format('D d M Y h:i:s P'):'N/A' !!}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->
@stop
