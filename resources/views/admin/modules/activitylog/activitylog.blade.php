@extends('admin/html')

@section('content')
@include('admin/modules/activitylog/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->	
<div class="portlet-body">
    @if (count($activities) == 0)
	<div class="row">
		<div class="col-sm-6">
			{!! Form::open(['action' => 'Admin\Modules\Activitylog\ActivityLogController@index', 'method' => 'GET']) !!}
				<div class="input-group input-medium">
					{!! Form::text('search', Request::get('search'), ['class' => 'form-control input-medium', 'placeholder' => 'Search']) !!}
					<span class="input-group-btn">
						{!! Form::button('<i class="icon-magnifier"></i>', ['class' => 'btn green-jungle', 'type'=>'submit']) !!}
					</span>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
	<p class="text-center font-grey-silver margin-top-15">There are no activities to display</p>
	@else
	<div class="row">
		<div class="col-sm-6">
			<div class="row">
				<div class="col-sm-3">
					@include('admin/includes/item_per_page')
				</div>
				<div class="col-sm-9">
					{!! Form::open(['action' => 'Admin\Modules\Activitylog\ActivityLogController@index', 'method' => 'GET']) !!}
						<div class="input-group input-medium">
							{!! Form::text('search', Request::get('search'), ['class' => 'form-control input-medium', 'placeholder' => 'Search']) !!}
							<span class="input-group-btn">
								{!! Form::button('<i class="icon-magnifier"></i>', ['class' => 'btn green-jungle', 'type'=>'submit']) !!}
							</span>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>
		<!--
		<div class="col-sm-6 text-right" style="margin-top:5px"> 
			<a href="#" class="btn btn-sm dark"><i class="fa fa-print"></i> Print</a>
			<a href="#" class="btn btn-sm red"><i class="fa fa-file-pdf-o"></i> PDF</a>
			<a href="#" class="btn btn-sm green-jungle"><i class="fa fa-file-excel-o"></i> XML</a>
		</div>
		-->
	</div>
	<div class="table-scrollable">
		<table class="table table-condensed table-bordered table-striped table-hover">
			<thead>
				<tr>
					<th style="width:1%">#</th>
					<th>Activity</th>
					<th style="width:1%">When</th>
				</tr>
			</thead>
			<tbody>
				@php 
					$page = \Request::get('page');
					$i = ($page == '')?0:(($page*ROW_PER_PAGE)-ROW_PER_PAGE); 
				@endphp    
				@foreach ($activities as $activity)
				<?php $i++ ?>
				<tr>    
					<td>{!! $i !!}</td>
					<td>
						@if ($activity->module === 'Customer') <button class="btn btn-xs green-jungle"><i class="icon-users"></i></button>
						@elseif ($activity->module === 'Merchant') <button class="btn btn-xs blue"><i class="icon-basket"></i></button>
						@elseif ($activity->module === 'Psp') <button class="btn btn-xs yellow-gold"><i class="icon-rocket"></i></button>
						@elseif ($activity->module === 'Partner') <button class="btn btn-xs green"><i class="icon-user-following"></i></button>
						@elseif ($activity->module === 'Wallet') <button class="btn btn-xs purple"><i class="icon-wallet"></i></button>
						@elseif ($activity->module === 'Payment') <button class="btn btn-xs blue"><i class="fa fa-credit-card"></i></button>
						@elseif ($activity->module === 'RiskManagement') <button class="btn btn-xs red-thunderbird"><i class="icon-shield"></i></button>
						@elseif ($activity->module === 'Gateway') <button class="btn btn-xs purple-medium"><i class="fa fa-plug"></i></button>
						@elseif ($activity->module === 'Invoice') <button class="btn btn-xs yellow-lemon"><i class="icon-paper-clip"></i></button>

						@elseif ($activity->module === 'Role') <button class="btn btn-xs yellow-lemon"><i class="icon-user"></i></button>
						@elseif ($activity->module === 'payments') <button class="btn btn-xs yellow-soft"><i class="icon-docs"></i></button>

						@elseif ($activity->module === 'Setting') <button class="btn btn-xs dark"><i class="fa fa-cogs"></i></button>
						@elseif ($activity->module === 'Usermanagement') <button class="btn btn-xs yellow-lemon"><i class="icon-user"></i></button>
						@else 
						@endif
						{!! $activity->activity !!}
					</td>
					<td>{!! $activity->created_at->diffForHumans() !!}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	<div class="row">
		<div class="col-md-6 margin-top-10">
			Showing {!! $activities->firstItem() !!} to {!! $activities->lastItem() !!} of {!! $activities->total() !!}
		</div>
		<div class="col-md-6 text-right">
			{!! $activities->links() !!}
		</div>
	</div>
	@endif
</div>
<!-- END PAGE CONTENT-->
@stop