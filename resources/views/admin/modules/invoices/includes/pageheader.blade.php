<!-- BEGIN PAGE HEADER-->
<div class="page-bar">
    <h3 class="page-title" style="float:left;">{!! $pagetitle !!}</h3>
    @if (Request::is('admin/invoices')) <a class="btn green-jungle bold" data-toggle="modal" href="#add" style="margin:10px;">Add <i class="fa fa-plus"></i></a>
    @elseif (Request::is('admin/proformainvoices')) <a class="btn green-jungle bold" data-toggle="modal" href="#add" style="margin:10px;">Add <i class="fa fa-plus"></i></a>
    @elseif (Request::is('admin/invoices/*') && @$invoice->review == 'Waiting') <a class="btn blue bold" data-toggle="modal" href="#edit" style="margin:10px;">Edit <i class="fa fa-pencil"></i></a>
    @elseif (Request::is('admin/proformainvoices/*') && @$proforma->review == 'Waiting') <a class="btn blue bold" data-toggle="modal" href="#edit" style="margin:10px;">Edit <i class="fa fa-pencil"></i></a>
    @endif
	<ul class="page-breadcrumb" style="float:right;padding-top:18px;">
        <li>
            {!! $breadcrumb_level1 or '' !!}
            {!! Request::is('admin/invoices/*') ? '<i class="fa fa-angle-right"></i>' : '' !!}
        </li>
		<li>
            {!! $breadcrumb_level2 or '' !!}
        </li>
    </ul>
</div>
<!-- END PAGE HEADER-->
