@extends('admin/html')

@section('content')
@include('admin/modules/invoices/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->
<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add an Invoice</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['url' => 'admin/invoices']) !!}
                    <div class="form-body">
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="@if ($errors->has('merchant_id')) has-error @endif">
										<label>Merchant ID</label>
										{!! Form::text('merchant_id', null, ['class' => 'form-control', 'placeholder' => 'Merchant ID']) !!}
										@if ($errors->has('merchant_id')) <p class="help-block">{{ $errors->first('merchant_id') }}</p> @endif
									</div>
                                </div>
                                <div class="col-md-6">
                                    <div class="@if ($errors->has('psp_id')) has-error @endif">
										<label>PSP ID</label>
										{!! Form::text('psp_id', null, ['class' => 'form-control', 'placeholder' => 'PSP ID']) !!}
										@if ($errors->has('psp_id')) <p class="help-block">{{ $errors->first('psp_id') }}</p> @endif
									</div>
                                </div>
                            </div>
                        </div>
						<div class="form-group">
							<div class="row">
                                <div class="col-md-8">
									<div class="@if ($errors->has('description')) has-error @endif">
										<label>Invoice Description</label>
										{!! Form::text('description', null, ['class' => 'form-control', 'placeholder' => 'Invoice Description']) !!}
										@if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
									</div>
                                </div>
                                <div class="col-md-4">
									<div class="@if ($errors->has('tax')) has-error @endif">
										<label>VAT %</label>
										{!! Form::text('tax', null, ['class' => 'form-control', 'placeholder' => 'VAT %']) !!}
										@if ($errors->has('tax')) <p class="help-block">{{ $errors->first('tax') }}</p> @endif
									</div>
                                </div>
                            </div>
                        </div>
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="@if ($errors->has('invoice_date')) has-error @endif">
										<label>Invoice Date</label>
										{!! Form::text('invoice_date', null, ['class' => 'form-control date-picker', 'placeholder' => 'Invoice Date']) !!}
										@if ($errors->has('invoice_date')) <p class="help-block">{{ $errors->first('invoice_date') }}</p> @endif
									</div>
								</div>
                                <div class="col-md-6">
									<div class="@if ($errors->has('due_date')) has-error @endif">
										<label>Due Date</label>
										{!! Form::text('due_date', null, ['class' => 'form-control date-picker', 'placeholder' => 'Due Date']) !!}
										@if ($errors->has('due_date')) <p class="help-block">{{ $errors->first('due_date') }}</p> @endif
									</div>
								</div>
                            </div>
                        </div>
						<div class="form-group @if ($errors->has('item_others_fee')) has-error @endif">
                            <label>Item Description</label>
							{!! Form::text('item_others_fee', null, ['class' => 'form-control', 'placeholder' => 'Item Description']) !!}
                            @if ($errors->has('item_others_fee')) <p class="help-block">{{ $errors->first('item_others_fee') }}</p> @endif
                        </div>
                        <div class="form-group">
                            <div class="row">
								<div class="col-md-4">
                                    <div class="@if ($errors->has('qty_others_fee')) has-error @endif">
                                        <label>Quantity</label>
										{!! Form::text('qty_others_fee', null, ['class' => 'form-control', 'placeholder' => 'Quantity']) !!}
                                        @if ($errors->has('qty_others_fee')) <p class="help-block">{{ $errors->first('qty_others_fee') }}</p> @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="@if ($errors->has('amount_others_fee')) has-error @endif">
                                        <label>Amount</label>
										{!! Form::text('amount_others_fee', null, ['class' => 'form-control', 'placeholder' => 'Amount']) !!}
                                        @if ($errors->has('amount_others_fee')) <p class="help-block">{{ $errors->first('amount_others_fee') }}</p> @endif
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="@if ($errors->has('currency')) has-error @endif">
										<label>Currency</label>
										{!! Form::select('currency', $currencies, null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
										@if ($errors->has('currency')) <p class="help-block">{{ $errors->first('currency') }}</p> @endif
									</div>
                                </div>
                            </div>
                        </div>

						<div class="form-group @if ($errors->has('bank_account_id')) has-error @endif">
							<label>Bank Account ID</label>
							{!! Form::select('bank_account_id', $bankaccounts, null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
							@if ($errors->has('bank_account_id')) <p class="help-block">{{ $errors->first('bank_account_id') }}</p> @endif
                        </div>
                        <div class="form-group">
                            <span class="input-group-btn">
                                {!! Form::submit('Add', ['class' => 'btn btn-block green-jungle btn-lg'] ) !!}
                            </span>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>

        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="row">
	<div class="col-sm-6  no-print">
		<div class="row">
		 @if (count($invoices) > 0)
			<div class="col-sm-3">
				@include('admin/includes/item_per_page')
			</div>
		@endif
			<div class="col-sm-9">
			{!! Form::open(['action' => 'Admin\Modules\Invoices\InvoicesController@index', 'method' => 'GET']) !!}
				<input type="hidden" name='rows' value="{{old('rows')}}" >
				<div class="input-group input-medium">
					{!! Form::text('search', Request::get('search'), ['class' => 'form-control input-medium', 'placeholder' => 'Search']) !!}
					<span class="input-group-btn">
						{!! Form::button('<i class="icon-magnifier"></i>', ['class' => 'btn green-jungle', 'type'=>'submit']) !!}
					</span>
				</div>
			{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@if (count($invoices)  == 0)
	<p class="font-grey-silver margin-top-15">There are no invoices to display</p>
@else
<div class="portlet-body">
    <div class="table-scrollable">
		<table class="table table-condensed table-bordered table-striped table-hover">
			<thead>
				<tr>
					<th style="width:1%">#</th>
					<th style="width:1%">Date</br>& Time</th>
					<th style="width:1%">Invoice</br>ID</th>
					<th style="width:1%">Merchant</br>ID</th>
					<th style="width:1%">PSP</br>ID</th>
					<th>Description</th>
					<th style="width:1%">Invoice Date</br>Due Date</th>
					<th style="width:1%">Total</th>
					<th style="width:1%">Review</th>
					<th style="width:1%">Email</th>
					<th style="width:1%">Status</th>
					<th style="width:1%">Action</th>
				</tr>
			</thead>
			<tbody>
				@php
					$page = \Request::get('page');
					$i = ($page == '')?0:(($page*$item_per_page)-$item_per_page);
				@endphp
				@foreach ($invoices as $invoice)

				<?php $i++ ?>
				<tr>
					<td>{!! $i !!}</td>
					<td style="white-space:nowrap;">{!! Carbon\Carbon::parse($invoice->created_at)->format('M d, Y') !!} </br> {!! Carbon\Carbon::parse($invoice->created_at)->format('H:i:s e') !!}</td>
					<td>{!! $invoice->invoice_id !!}</td>
					<td>{!! $invoice->merchant_id !!}</td>
					<td>{!! $invoice->psp_id !!}</td>
					<td>{!! $invoice->description !!}</td>
					<td>{!! Carbon\Carbon::parse($invoice->invoice_date)->format('M d, Y') !!}</br>{!! Carbon\Carbon::parse($invoice->due_date)->format('M d, Y') !!}</td>
					<td class="text-right"><b>{!! $invoice->total !!}</b></br>{!! $invoice->currency !!}</td>
					<td><span class="label label-sm @if ($invoice->review === 'Reviewed') label-success @elseif ($invoice->review === 'Waiting') label-warning @endif">{!! $invoice->review !!}</span></td>
					<td>@if ($invoice->email == '1') <span class="label label-sm label-success">Sent</span> @endif</td>
					<td><span class="label label-sm @if ($invoice->status === 'Paid') label-success @elseif ($invoice->status === 'Pending') label-info @elseif ($invoice->status === 'Partial') label-warning @elseif ($invoice->status === 'Past Due') label-danger @else label-default @endif">{!! $invoice->status !!}</span></td>
					<td>
						<a href="/admin/invoices/{!! $invoice->id !!}" class="btn blue btn-xs"><i class="fa fa-pencil"></i></a>
						@if ($invoice->review == 'Reviewed')
							<a href="{{url('admin/invoices/'.$invoice->invoice_id.'/pdf-convert')}}" class="btn red btn-xs"><i class="fa fa-file-pdf-o"></i></a>
							{!! Form::open(['url' => 'admin/invoices/'.$invoice->id.'/send', 'style' => 'display:inline-block;']) !!}
								<button type="submit" class="btn green-jungle btn-xs"><i class="fa fa-send"></i></button>
							{!! Form::close() !!}
						@else
							<a href="javascript:;" class="btn default btn-xs"><i class="fa fa-file-pdf-o"></i></a>
							<a href="javascript:;" class="btn default btn-xs"><i class="fa fa-send"></i></a>
						@endif
						@if ($invoice->status == 'Paid' || $invoice->review == 'Waiting')
							<a href="javascript:;" class="btn default btn-xs"><i class="fa fa-credit-card"></i></a>
						@else
						<a data-toggle="modal" href="#{!! $invoice->invoice_id !!}" class="btn blue btn-xs"><i class="fa fa-credit-card"></i></a>
						<div class="modal fade" id="{!! $invoice->invoice_id !!}" tabindex="-1" role="dialog" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
										<h4 class="modal-title">Log a Payment for Invoice #{{ $invoice->invoice_id }}</h4>
									</div>
									<div class="modal-body">
										{!! Form::open(['url' => 'admin/invoices/'.$invoice->invoice_id.'/log']) !!}

											<div class="form-body">
												<div class="form-group @if ($errors->has('payment_date')) has-error @endif">
													<label>Payment Date</label>
													{!! Form::text('payment_date', null, ['class' => 'form-control date-picker', 'placeholder' => 'Payment Date']) !!}
													@if ($errors->has('payment_date')) <p class="help-block">{{ $errors->first('payment_date') }}</p> @endif
												</div>
												<div class="form-group">
													<div class="row">
														<div class="col-md-8">
															<div class="@if ($errors->has('paid_amount')) has-error @endif">
																<label>Amount</label>
																{!! Form::text('paid_amount', null, ['class' => 'form-control', 'placeholder' => 'Amount']) !!}
																@if ($errors->has('paid_amount')) <p class="help-block">{{ $errors->first('paid_amount') }}</p> @endif
															</div>
														</div>
														<div class="col-md-4">
															<div class="">
																<label>Currency</label>
																<input type="text" name="currency" value="{!! $invoice->currency !!}" class="form-control" placeholder="{!! $invoice->currency !!}" readonly>
															</div>
														</div>
													</div>
												</div>
												<div class="form-group">
													<span class="input-group-btn">
														{!! Form::submit('Submit', ['class' => 'btn btn-block green-jungle btn-lg'] ) !!}
													</span>
												</div>
											</div>
										{!! Form::close() !!}
									</div>
								</div>

								<!-- /.modal-content -->
							</div>
							<!-- /.modal-dialog -->
						</div>
						@endif
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	<div class="row">
		<div class="col-md-6 margin-top-10">
			Showing {!! $invoices->firstItem() !!} to {!! $invoices->lastItem() !!} of {!! $invoices->total() !!}
		</div>
		<div class="col-md-6 text-right  no-print">
			{!! $invoices->appends(Request::except('page'))->links() !!}
		</div>
	</div>
</div>
@endif
<!-- END PAGE CONTENT-->
@stop
