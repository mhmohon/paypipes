@extends('admin/html')
@section('content')
@include('admin/modules/invoices/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->
<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add an Proforma</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['url' => 'admin/proformainvoices']) !!}
                    <div class="form-body">
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="@if ($errors->has('merchant_id')) has-error @endif">
										<label>Merchant ID</label>
										{!! Form::text('merchant_id', null, ['class' => 'form-control', 'placeholder' => 'Merchant ID']) !!}
										@if ($errors->has('merchant_id')) <p class="help-block">{{ $errors->first('merchant_id') }}</p> @endif
									</div>
                                </div>
                                <div class="col-md-6">
                                    <div class="@if ($errors->has('psp_id')) has-error @endif">
										<label>PSP ID</label>
										{!! Form::text('psp_id', null, ['class' => 'form-control', 'placeholder' => 'PSP ID']) !!}
										@if ($errors->has('psp_id')) <p class="help-block">{{ $errors->first('psp_id') }}</p> @endif
									</div>
                                </div>
                            </div>
                        </div>
						<div class="form-group">
							<div class="row">
                                <div class="col-md-8">
									<div class="@if ($errors->has('description')) has-error @endif">
										<label>Invoice Description</label>
										{!! Form::text('description', null, ['class' => 'form-control', 'placeholder' => 'Invoice Description']) !!}
										@if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
									</div>
                                </div>
                                <div class="col-md-4">
									<div class="@if ($errors->has('tax')) has-error @endif">
										<label>VAT %</label>
										{!! Form::text('tax', null, ['class' => 'form-control', 'placeholder' => 'VAT %']) !!}
										@if ($errors->has('tax')) <p class="help-block">{{ $errors->first('tax') }}</p> @endif
									</div>
                                </div>
                            </div>
                        </div>
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="@if ($errors->has('invoice_date')) has-error @endif">
										<label>Proforma Date</label>
										{!! Form::text('invoice_date', null, ['class' => 'form-control date-picker', 'placeholder' => 'Proforma Date']) !!}
										@if ($errors->has('invoice_date')) <p class="help-block">{{ $errors->first('invoice_date') }}</p> @endif
									</div>
								</div>
                                <div class="col-md-6">
									<div class="@if ($errors->has('due_date')) has-error @endif">
										<label>Due Date</label>
										{!! Form::text('due_date', null, ['class' => 'form-control date-picker', 'placeholder' => 'Due Date']) !!}
										@if ($errors->has('due_date')) <p class="help-block">{{ $errors->first('due_date') }}</p> @endif
									</div>
								</div>
                            </div>
                        </div>
						<div class="form-group @if ($errors->has('item_others_fee')) has-error @endif">
                            <label>Item Description</label>
							{!! Form::text('item_others_fee', null, ['class' => 'form-control', 'placeholder' => 'Item Description']) !!}
                            @if ($errors->has('item_others_fee')) <p class="help-block">{{ $errors->first('item_others_fee') }}</p> @endif
                        </div>
                        <div class="form-group">
                            <div class="row">
								<div class="col-md-4">
                                    <div class="@if ($errors->has('qty_others_fee')) has-error @endif">
                                        <label>Quantity</label>
										{!! Form::text('qty_others_fee', null, ['class' => 'form-control', 'placeholder' => 'Quantity']) !!}
                                        @if ($errors->has('qty_others_fee')) <p class="help-block">{{ $errors->first('qty_others_fee') }}</p> @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="@if ($errors->has('amount_others_fee')) has-error @endif">
                                        <label>Amount</label>
										{!! Form::text('amount_others_fee', null, ['class' => 'form-control', 'placeholder' => 'Amount']) !!}
                                        @if ($errors->has('amount_others_fee')) <p class="help-block">{{ $errors->first('amount_others_fee') }}</p> @endif
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="@if ($errors->has('currency')) has-error @endif">
										<label>Currency</label>
										{!! Form::select('currency', $currencies, null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
										@if ($errors->has('currency')) <p class="help-block">{{ $errors->first('currency') }}</p> @endif
									</div>
                                </div>
                            </div>
                        </div>

						<div class="form-group @if ($errors->has('bank_account_id')) has-error @endif">
							<label>Bank Account ID</label>
							{!! Form::select('bank_account_id', $bankaccounts, null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
							@if ($errors->has('bank_account_id')) <p class="help-block">{{ $errors->first('bank_account_id') }}</p> @endif
                        </div>
                        <div class="form-group">
                            <span class="input-group-btn">
                                {!! Form::submit('Add', ['class' => 'btn btn-block green-jungle btn-lg'] ) !!}
                            </span>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>

        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="row">
	<div class="col-sm-6  no-print">
		<div class="row">
		 @if (count($proformas) > 0)
			<div class="col-sm-3">
				@include('admin/includes/item_per_page')
			</div>
		@endif
			<div class="col-sm-9">
			{!! Form::open(['action' => 'Admin\Modules\Invoices\ProformainvoicesController@index', 'method' => 'GET']) !!}
				<input type="hidden" name='rows' value="{{old('rows')}}" >
				<div class="input-group input-medium">
					{!! Form::text('search', Request::get('search'), ['class' => 'form-control input-medium', 'placeholder' => 'Search']) !!}
					<span class="input-group-btn">
						{!! Form::button('<i class="icon-magnifier"></i>', ['class' => 'btn green-jungle', 'type'=>'submit']) !!}
					</span>
				</div>
			{!! Form::close() !!}
			</div>
		</div>
	</div>
</div>
@if (count($proformas)  == 0)
	<p class="font-grey-silver margin-top-15">There are no Proformas to display</p>
@else
<div class="portlet-body">
    <div class="table-scrollable">
		<table class="table table-condensed table-bordered table-striped table-hover">
			<thead>
				<tr>
					<th style="width:1%">#</th>
					<th style="width:1%">Date</br>& Time</th>
					<th style="width:1%">Proforma</br>ID</th>
					<th style="width:1%">Merchant</br>ID</th>
					<th style="width:1%">PSP</br>ID</th>
					<th>Description</th>
					<th style="width:1%">Proforma Date</br>Due Date</th>
					<th style="width:1%">Total</th>
					<th style="width:1%">Review</th>
					<th style="width:1%">Email</th>
					<th style="width:1%">Action</th>
				</tr>
			</thead>
			<tbody>
				@php
					$page = \Request::get('page');
					$i = ($page == '')?0:(($page*$item_per_page)-$item_per_page);
				@endphp
				@foreach ($proformas as $proforma)

				<?php $i++ ?>
				<tr>
					<td>{!! $i !!}</td>
					<td style="white-space:nowrap;">{!! Carbon\Carbon::parse($proforma->created_at)->format('M d, Y') !!} </br> {!! Carbon\Carbon::parse($proforma->created_at)->format('H:i:s e') !!}</td>
					<td>{!! $proforma->proforma_id !!}</td>
					<td>{!! $proforma->merchant_id !!}</td>
					<td>{!! $proforma->psp_id !!}</td>
					<td>{!! $proforma->description !!}</td>
					<td>{!! Carbon\Carbon::parse($proforma->invoice_date)->format('M d, Y') !!}</br>{!! Carbon\Carbon::parse($proforma->due_date)->format('M d, Y') !!}</td>
					<td class="text-right"><b>{!! $proforma->total !!}</b></br>{!! $proforma->currency !!}</td>
					<td><span class="label label-sm @if ($proforma->review === 'Reviewed') label-success @elseif ($proforma->review === 'Waiting') label-warning @endif">{!! $proforma->review !!}</span></td>
					<td>@if ($proforma->email == '1') <span class="label label-sm label-success">Sent</span> @endif</td>
					<td>
                        @if ($proforma->copy_proforma != '1')
						    <a href="/admin/proformainvoices/{!! $proforma->id !!}" class="btn blue btn-xs"><i class="fa fa-pencil"></i></a>
                        @else
                            <a href="javascript:;" class="btn default btn-xs"><i class="fa fa-pencil"></i></a>
                        @endif
						@if ($proforma->review == 'Reviewed')
							<a href="{{url('admin/proformainvoices/'.$proforma->proforma_id.'/pdf-convert')}}" class="btn red btn-xs"><i class="fa fa-file-pdf-o"></i></a>
                             @if ($proforma->copy_proforma != '1')
							{!! Form::open(['url' => 'admin/proformainvoices/'.$proforma->id.'/send', 'style' => 'display:inline-block;']) !!}
								<button type="submit" class="btn green-jungle btn-xs"><i class="fa fa-send"></i></button>
							{!! Form::close() !!}

                                {!! Form::open(['method' => 'POST', 'action' => ['Admin\Modules\Invoices\ProformainvoicesController@createInvoice', $proforma->proforma_id], 'style' => 'display:inline-block;']) !!}
                                {!! Form::button('<i class="fa fa-copy"></i>', ['class' => 'btn green-jungle btn-xs', 'type'=>'submit', 'data-toggle' => 'confirmation', 'data-popout' => 'true', 'data-singleton' => 'true', 'data-original-title' => 'Create Invoice! Are you sure?', 'title' => '']) !!}
                                {!! Form::close() !!}
                            @else
                                <a href="javascript:;" class="btn default btn-xs"><i class="fa fa-send"></i></a>
                                <a href="javascript:;" class="btn default btn-xs"><i class="fa fa-copy"></i></a>
                            @endif
						@else
							<a href="javascript:;" class="btn default btn-xs"><i class="fa fa-file-pdf-o"></i></a>
							<a href="javascript:;" class="btn default btn-xs"><i class="fa fa-send"></i></a>
                            <a href="javascript:;" class="btn default btn-xs"><i class="fa fa-copy"></i></a>
						@endif
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	<div class="row">
		<div class="col-md-6 margin-top-10">
			Showing {!! $proformas->firstItem() !!} to {!! $proformas->lastItem() !!} of {!! $proformas->total() !!}
		</div>
		<div class="col-md-6 text-right  no-print">
			{!! $proformas->appends(Request::except('page'))->links() !!}
		</div>
	</div>
</div>
@endif
<!-- END PAGE CONTENT-->
@stop
