@extends('admin/html')

@section('content')
@include('admin/modules/invoices/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Edit an Invoice</h4>
            </div>
            <div class="modal-body">
                {!! Form::model($invoice, ['method' => 'POST', 'action' => ['Admin\Modules\Invoices\InvoicesController@update', $invoice->id ]]) !!}
                    <div class="form-body">
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="@if ($errors->has('merchant_id')) has-error @endif">
										<label>Merchant ID</label>
										{!! Form::text('merchant_id', null, ['class' => 'form-control', 'placeholder' => 'Merchant ID']) !!}
										@if ($errors->has('merchant_id')) <p class="help-block">{{ $errors->first('merchant_id') }}</p> @endif
									</div>
                                </div>
                                <div class="col-md-6">
                                    <div class="@if ($errors->has('psp_id')) has-error @endif">
										<label>PSP ID</label>
										{!! Form::text('psp_id', null, ['class' => 'form-control', 'placeholder' => 'PSP ID']) !!}
										@if ($errors->has('psp_id')) <p class="help-block">{{ $errors->first('psp_id') }}</p> @endif
									</div>
                                </div>
                            </div>
                        </div>
						<div class="form-group">
							<div class="row">
                                <div class="col-md-8">
									<div class="@if ($errors->has('description')) has-error @endif">
										<label>Invoice Description</label>
										{!! Form::text('description', null, ['class' => 'form-control', 'placeholder' => 'Invoice Description']) !!}
										@if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
									</div>
                                </div>
                                <div class="col-md-4">
									<div class="@if ($errors->has('tax')) has-error @endif">
										<label>VAT %</label>
										{!! Form::text('tax', null, ['class' => 'form-control', 'placeholder' => 'VAT %']) !!}
										@if ($errors->has('tax')) <p class="help-block">{{ $errors->first('tax') }}</p> @endif
									</div>
                                </div>
                            </div>
                        </div>
						<div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
									<div class="@if ($errors->has('invoice_date')) has-error @endif">
										<label>Invoice Date</label>
										{!! Form::text('invoice_date', null, ['class' => 'form-control date-picker', 'placeholder' => 'Invoice Date']) !!}
										@if ($errors->has('invoice_date')) <p class="help-block">{{ $errors->first('invoice_date') }}</p> @endif
									</div>
								</div>
                                <div class="col-md-6">
									<div class="@if ($errors->has('due_date')) has-error @endif">
										<label>Due Date</label>
										{!! Form::text('due_date', null, ['class' => 'form-control date-picker', 'placeholder' => 'Due Date']) !!}
										@if ($errors->has('due_date')) <p class="help-block">{{ $errors->first('due_date') }}</p> @endif
									</div>
								</div>
                            </div>
                        </div>
						<div class="form-group @if ($errors->has('item_others_fee')) has-error @endif">
                            <label>Item Description</label>
							{!! Form::text('item_others_fee', null, ['class' => 'form-control', 'placeholder' => 'Item Description']) !!}
                            @if ($errors->has('item_others_fee')) <p class="help-block">{{ $errors->first('item_others_fee') }}</p> @endif
                        </div>
                        <div class="form-group">
                            <div class="row">
								<div class="col-md-4">
                                    <div class="@if ($errors->has('qty_others_fee')) has-error @endif">
                                        <label>Quantity</label>
										{!! Form::text('qty_others_fee', null, ['class' => 'form-control', 'placeholder' => 'Quantity']) !!}
                                        @if ($errors->has('qty_others_fee')) <p class="help-block">{{ $errors->first('qty_others_fee') }}</p> @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="@if ($errors->has('item_others_fee')) has-error @endif">
                                        <label>Amount</label>
										{!! Form::text('amount_others_fee', null, ['class' => 'form-control', 'placeholder' => 'Amount']) !!}
                                        @if ($errors->has('item_others_fee')) <p class="help-block">{{ $errors->first('item_others_fee') }}</p> @endif
                                    </div>
                                </div>
								<div class="col-md-4">
                                    <div class="@if ($errors->has('currency')) has-error @endif">
										<label>Currency</label>
										{!! Form::select('currency', $currencies, null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
										@if ($errors->has('currency')) <p class="help-block">{{ $errors->first('currency') }}</p> @endif
									</div>
                                </div>
                            </div>
                        </div>
						<div class="form-group @if ($errors->has('bank_account_id')) has-error @endif">
							<label>Bank Account ID</label>
							{!! Form::select('bank_account_id', $bankaccounts, null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
							@if ($errors->has('bank_account_id')) <p class="help-block">{{ $errors->first('bank_account_id') }}</p> @endif
                        </div>
                        <div class="form-group">
                            <span class="input-group-btn">
                                {!! Form::submit('Add', ['class' => 'btn btn-block blue btn-lg'] ) !!}
                            </span>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>

        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="portlet-body">
    <div class="invoice">
		<div class="row">
			<div class="col-xs-6">
				<img src="/img/{{ config('global.logo') }}" class="img-responsive" style="height:50px;" alt="" />
			</div>
			<div class="col-xs-6">
				<p class="text-right" style="font-size:22px;font-weight:300;">
					INVOICE #{!! $invoice->invoice_id !!}
				</p>
			</div>
		</div>
		<hr/>
		<div class="row">
			<div class="col-xs-4">
				<h3>Bill To:</h3>
				<ul class="list-unstyled">
					<li><b>{!! $client->company_name or '' !!}</b></li>
					<li> {!! $client->street or '' !!} </li>
					<li> {!! $client->city or '' !!}</li>
					<li> {!! $client->state or '' !!} </li>
					<li> {!! $client->post_code or '' !!} </li>
					<li> {!! $client->country_name or '' !!} </li>
					<li> {!! $client->email or '' !!} </li>
				</ul>
				<h3>For:</h3>
				<ul class="list-unstyled">
					<li><b>{!! $invoice->description !!}</b></li>
				</ul>
			</div>
			<div class="col-xs-4">
				<h3>Pay To:</h3>
				<ul class="list-unstyled">
					<li><b>{!! $setting->company_legal_name !!}</b></li>
					<li>{!! $setting->street !!}</li>
					<li>{!! $setting->city !!} {!! $setting->post_code !!}</li>
					<li>{!! $setting->state !!}</li>
					<li>{!! $setting->country_name !!}</li>
					<li>ID: {!! $setting->company_id !!}</li>
					<li>VAT ID: {!! $setting->vat_id !!}</li>
					<li>{!! $setting->invoice_website !!}</li>
					<li>{!! $setting->invoice_email !!}</li>
				</ul>
			</div>
			<div class="col-xs-4">
				<h3 class="text-right">Invoice Details:</h3>
				<table class="table">
					<tr>
						<td style="border:0px;">Invoice Number:</td>
						<td style="border:0px;" class="text-right">{!! $invoice->invoice_id !!}</td>
					</tr>
					<tr>
						<td style="border:0px;">Invoice Date:</td>
						<td style="border:0px;" class="text-right">{!! Carbon\Carbon::parse($invoice->invoice_date)->format('M d, Y') !!}</td>
					</tr>
					<tr>
						<td style="border:0px;">Due Date:</td>
						<td style="border:0px;" class="text-right">{!! Carbon\Carbon::parse($invoice->due_date)->format('M d, Y') !!}</td>
					</tr>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th style="width:1%"> # </th>
							<th> Item </th>
							<th style="width:1%" class="text-right"> Unit Cost </th>
							<th style="width:1%" class="text-center"> Qty </th>
							<th style="width:1%" class="text-right"> Amount </th>
						</tr>
					</thead>
					<tbody>
						<?php $i=1; ?>
						@if (isset($invoice->amount_creation_fee))
						<tr>
							<td> <?php echo $i ?> </td>
							<td> Creation Fee </td>
							<td class="text-right"> {!! $invoice->amount_creation_fee !!} {!! $invoice->currency !!}</td>
							<td class="text-center"> {!! $invoice->qty_creation_fee !!}</td>
							<td class="text-right"> {!! number_format($invoice->total_creation_fee, 2) !!} {!! $invoice->currency !!}</td>
							<?php $i++; ?>
						</tr>
						@endif
						@if (isset($invoice->amount_monthly_fee))
						<tr>
							<td> <?php echo $i ?> </td>
							<td> Monthly Fee </td>
							<td class="text-right"> {!! $invoice->amount_monthly_fee !!} {!! $invoice->currency !!}</td>
							<td class="text-center"> {!! $invoice->qty_monthly_fee !!}</td>
							<td class="text-right"> {!! number_format($invoice->total_monthly_fee, 2) !!} {!! $invoice->currency !!} </td>
							<?php $i++; ?>
						</tr>
						@endif
						@if (isset($invoice->amount_merchant_fee))
						<tr>
							<td> <?php echo $i ?> </td>
							<td> Merchant Fee </td>
							<td class="text-right"> {!! $invoice->amount_merchant_fee !!} {!! $invoice->currency !!}</td>
							<td class="text-center"> {!! $invoice->qty_merchant_fee !!}</td>
							<td class="text-right"> {!! number_format($invoice->total_merchant_fee, 2) !!} {!! $invoice->currency !!} </td>
							<?php $i++; ?>
						</tr>
						@endif
						@if (isset($invoice->amount_terminal_fee))
						<tr>
							<td> <?php echo $i ?> </td>
							<td> Terminal Fee </td>
							<td class="text-right"> {!! $invoice->amount_terminal_fee !!} {!! $invoice->currency !!}</td>
							<td class="text-center"> {!! $invoice->qty_terminal_fee !!}</td>
							<td class="text-right"> {!! number_format($invoice->total_terminal_fee, 2) !!} {!! $invoice->currency !!} </td>
							<?php $i++; ?>
						</tr>
						@endif
						@if (isset($invoice->amount_gateway_fee))
						<tr>
							<td> <?php echo $i ?> </td>
							<td> Gateway Fee </td>
							<td class="text-right"> {!! $invoice->amount_gateway_fee !!} {!! $invoice->currency !!}</td>
							<td class="text-center"> {!! $invoice->qty_gateway_fee !!}</td>
							<td class="text-right"> {!! number_format($invoice->total_gateway_fee, 2) !!} {!! $invoice->currency !!} </td>
							<?php $i++; ?>
						</tr>
						@endif
						@if (isset($invoice->amount_transaction_fee))
						<tr>
							<td> <?php echo $i ?> </td>
							<td> Transaction Fee </td>
							<td class="text-right"> {!! $invoice->amount_transaction_fee !!} {!! $invoice->currency !!}</td>
							<td class="text-center"> {!! $invoice->qty_transaction_fee !!}</td>
							<td class="text-right"> {!! number_format($invoice->total_transaction_fee, 2) !!} {!! $invoice->currency !!} </td>
							<?php $i++; ?>
						</tr>
						@endif
						@if (isset($invoice->amount_others_fee))
						<tr>
							<td> <?php echo $i ?> </td>
							<td> {!! $invoice->item_others_fee !!} </td>
							<td class="text-right"> {!! $invoice->amount_others_fee !!} {!! $invoice->currency !!}</td>
							<td class="text-center"> {!! $invoice->qty_others_fee !!}</td>
							<td class="text-right"> {!! number_format($invoice->total_others_fee, 2) !!} {!! $invoice->currency !!} </td>
							<?php $i++; ?>
						</tr>
						@endif
					</tbody>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-4">
				<div class="well">
					<address>
						Please transfer the amount of {!! number_format($invoice->total, 2) !!} {!! $invoice->currency !!} to:<br/>
						<br/>Account Holder: {!! $bankaccount->account_holder !!}
						<br/> Account Number: {!! $bankaccount->account_number !!}
						<br/> Bank Name: {!! $bankaccount->bank_name !!}
						@if (isset($bankaccount->bank_address)) <br/> Bank Address: {!! $bankaccount->bank_address !!} @endif
						<br/> IBAN: <b>{!! $bankaccount->iban !!}</b>
						<br/> SWIFT/BIC: <b>{!! $bankaccount->swift_bic !!}</b>
					</address>
				</div>
			</div>
			<div class="col-xs-4">
			</div>
			<div class="col-xs-4">
				<table class="table">
					<tr>
						<td style="border:0px;">Sub Total:</td>
						<td style="border:0px;" class="text-right">{!! number_format(($invoice->total-$invoice->total_tax), 2) !!} {!! $invoice->currency !!}</td>
					</tr>
					<tr>
						<td style="border:0px;">VAT ({!! $invoice->tax !!}%):</td>
						<td style="border:0px;" class="text-right"> {!! number_format($invoice->total_tax, 2) !!} {!! $invoice->currency !!}</td>
					</tr>
					<tr>
						<td style="border:0px;"><span style="font-size:22px;font-weight:300;">Total:</span></td>
						<td style="border:0px;" class="text-right"><span style="font-size:22px;font-weight:300;"> {!! number_format($invoice->total, 2) !!} {!! $invoice->currency !!} </span></td>
					</tr>
					<tr>
						<td style="border:0px;">Payment Status:</td>
						<td style="border:0px;" class="text-right"><span class="label label-lg @if ($invoice->status === 'Paid') label-success @elseif ($invoice->status === 'Pending') label-info @elseif ($invoice->status === 'Partial') label-warning @elseif ($invoice->status === 'Past Due') label-danger @else label-default @endif">{!! $invoice->status !!}</span></td>
					</tr>
				</table>
				@if ($invoice->review == 'Reviewed')
				<p class="text-right"><a href="javascript:;" class="btn btn-lg green-jungle"><i class="fa fa-check"></i> Reviewed</a></p>
				@else
				{!! Form::open(['method' => 'PATCH', 'action' => ['Admin\Modules\Invoices\InvoicesController@review', $invoice->id], 'class' => 'text-right']) !!}
					{!! Form::button('<i class="icon-hourglass"></i> Waiting Review', ['class' => 'btn btn-lg yellow-saffron', 'type'=>'submit', 'data-toggle' => 'confirmation', 'data-popout' => 'true', 'data-singleton' => 'true', 'data-original-title' => 'Reviewed! Are you sure?', 'title' => '']) !!}
				{!! Form::close() !!}
				@endif
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<h3 class="text-center">Thank you for your business!</h3>
			</div>
		</div>
	</div>
	@if (count($invoice_logs) > 0)
	<h4 class="page-title">Inovice Payment Log</h4>
	<div class="table-scrollable">
		<table class="table table-light table-bordered table-striped table-hover">
			<thead>
				<tr>
					<th style="width:1%">Inovice</br>ID</th>
					<th>Description</th>
					<th style="width:1%">Payment</br>Date</th>
					<th style="width:1%">Amount</th>
					<th style="width:1%">Payment</br>Status</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($invoice_logs as $invoice_log)
				<tr>
					<td>{!! $invoice_log->invoice_id !!}</td>
					<td>{!! $invoice->description !!}</td>
					<td>{!! Carbon\Carbon::parse($invoice_log->payment_date)->format('M d, Y') !!}</td>
					<td class="text-right"><b>{!! $invoice_log->amount !!}</b></br>{!! $invoice_log->currency !!}</td>
					<td><span class="label label-sm @if ($invoice_log->status === 'Paid') label-success @elseif ($invoice_log->status === 'Pending') label-info @elseif ($invoice_log->status === 'Partial') label-warning @elseif ($invoice_log->status === 'Past Due') label-danger @else label-default @endif">{!! $invoice_log->status !!}</span></td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	<div class="row">
		<div class="col-md-6 margin-top-10">
			Showing {!! $invoice_logs->firstItem() !!} to {!! $invoice_logs->lastItem() !!} of {!! $invoice_logs->total() !!}
		</div>
		<div class="col-md-6 text-right">
			{!! $invoice_logs->links() !!}
		</div>
	</div>
	@endif
</div>
<!-- END PAGE CONTENT-->
@stop
