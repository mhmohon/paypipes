<!-- BEGIN PAGE CONTENT-->
<div class="modal fade" id="credit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Credit Request</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['method' => 'POST', 'url' => url('admin/credits')]) !!}
                    <div class="form-body">
                        <div class="form-group @if ($errors->has('company_name')) has-error @endif">
                            <label>Transaction ID</label>
							{!! Form::text('transaction_id', null, ['class' => 'form-control', 'placeholder' => 'Transaction ID']) !!}
                            @if ($errors->has('transaction_id')) <p class="help-block">{{ $errors->first('transaction_id') }}</p> @endif
                        </div>
						<div class="form-group @if ($errors->has('amount')) has-error @endif">
                                <label>Amount</label>
								{!! Form::text('amount', @$payment->amount, ['class' => 'form-control', 'placeholder' => 'Amount']) !!}
                                @if ($errors->has('amount')) <p class="help-block">{{ $errors->first('amount') }}</p> @endif
						</div>
						<div class="form-group @if ($errors->has('description')) has-error @endif">
								<label>Description</label>
								{!! Form::text('description', null, ['class' => 'form-control', 'placeholder' => 'Description']) !!}
								@if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
						</div>
                        <div class="form-group">
                            <span class="input-group-btn">
                                {!! Form::submit('Credit Request', ['class' => 'btn btn-block red btn-lg'] ) !!}
                            </span>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
 </div>
