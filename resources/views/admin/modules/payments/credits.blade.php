@extends('admin/html')

@section('content')
@include('admin/modules/payments/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->
@include('admin/modules/payments/add_credit')
<div class="portlet-body">
    <div class="row">
        <div class="col-sm-6  no-print">
            <div class="row">
                <div class="col-sm-3">
                    @include('admin/includes/item_per_page')
                </div>
                <div class="col-sm-6">
                {!! Form::open(['action' => 'Admin\Modules\Payments\CreditController@index', 'method' => 'GET']) !!}
                    <input type="hidden" name='rows' value="{{old('rows')}}" >
                    <div class="input-group">
                        {!! Form::text('search', old('search'), ['class' => 'form-control', 'placeholder' => 'Search']) !!}
                        <span class="input-group-btn">
                            {!! Form::button('<i class="icon-magnifier"></i>', ['class' => 'btn green-jungle', 'type'=>'submit']) !!}
                        </span>
                    </div>
                {!! Form::close() !!}
                </div>
            </div>
        </div>
        @if (count($payments) > 0)
        <div class="col-sm-6 text-right  no-print" style="margin-top:5px">
            <a href="{{url('admin/credits/pdf-convert'.$pg)}}" class="btn btn-sm red"><i class="fa fa-file-pdf-o"></i> PDF</a>
            <a href="{{url('admin/credits/csv-convert'.$pg)}}" class="btn btn-sm green-jungle"><i class="fa fa-file-excel-o"></i> CSV</a>
        </div>
        @endif
         </div>
    </div>
    @if (count($payments) == 0)
    <p class="font-grey-silver margin-top-15">There are no transactions to display</p>
    @else
    <div class="table-scrollable">
        <table class="table table-condensed table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th style="width:1%">#</th>
                    <th style="width:1%">Date<br>& Time</th>
                    <th>Description</th>
                    <th style="width:1%">Risk<br>Score</th>
                    <th style="width:1%">Card</th>
                    <th style="width:1%">Method<br>& Type</th>
                    <th style="width:1%" class="text-right">Amount</th>
                    <th style="width:1%">Status</th>
                    <th style="width:1%" class="no-print">Action</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $page = \Request::get('page');
                    $i = ($page == '')?0:(($page*$item_per_page)-$item_per_page);
                @endphp
                @foreach ($payments as $payment)
                <?php $i++ ?>
                <tr>
                    <td>{!! $i !!}</td>
                    <td style="white-space:nowrap;">{!! Carbon\Carbon::parse($payment->created_at)->format('M d, Y') !!} </br> {!! Carbon\Carbon::parse($payment->created_at)->format('H:i:s e') !!}</td>
                    <td>
                        {!! $payment->description !!}</br>
                        <cite>From {!! $payment->first_name.' '.$payment->last_name.' ('.$payment->email.')' !!} to @if (!empty($payment->psp_id)) PSPID{!! $payment->psp_id !!}@elseif (!empty($payment->merchant_id))   MID{!! $payment->merchant_id !!} TID{!! $payment->terminal_id !!}@endif</cite><br><span class="small font-blue-madison">Transaction ID: {!! $payment->transaction_id !!}</span>
                    </td>
                    <td  class="no-print">
                        @if(!empty($payment->maxmind_id))
                        <a href="/admin/transactions/{!! $payment->id !!}/riskscore/{!! $payment->maxmind_id !!}" class="label label-sm @if ($payment->risk_score <= 59) label-success @else label-danger @endif">{!! $payment->risk_score !!}</a>
                        @endif
                    </td>
                    <td>
                        @if(isset($payment->card_digit) && !empty($payment->card_digit))
                            <img src="/uploads/paymentmethods/{!! $payment->card_type !!}.png" style="height:30px;"><br>*{!! $payment->card_digit !!}
                        @endif
                    </td>
                    <td>
                        @if(!empty($payment->gateway))
                        <img src="/uploads/paymentmethods/{!! $payment->gateway !!}.png" style="height:30px">
                        @endif
                        <br>{!! $payment->transaction_type !!}
                    </td>
                    <td class="text-right"><b>{!! $payment->amount !!}</b></br>{!! $payment->currency !!}</td>
                    <td><span class="label label-sm @if ($payment->transaction_status === 'Approved') label-success @elseif ($payment->transaction_status === 'Pending') label-info @elseif ($payment->transaction_status === 'Requested') label-info @elseif ($payment->transaction_status === 'Refunded') label-purple @elseif ($payment->transaction_status === 'Canceled') label-warning @elseif ($payment->transaction_status === 'Chargeback') label-black @elseif ($payment->transaction_status === 'Declined') label-danger @else label-default @endif">{!! $payment->transaction_status !!}</span></td>
                    <td  class="no-print"><a href="/admin/transactions/{!! $payment->id !!}" class="btn blue btn-xs"><i class="fa fa-pencil"></i></a></td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="row">
        <div class="col-md-6 margin-top-10">
            Showing {!! $payments->firstItem() !!} to {!! $payments->lastItem() !!} of {!! $payments->total() !!}
        </div>
        <div class="col-md-6 text-right  no-print">
         {{ $payments->appends(Request::except('page'))->links() }}
        </div>
    </div>
    @endif
</div>
<!-- END PAGE CONTENT-->
@stop

