@extends('admin/html')
@section('content')
@include('admin/modules/payments/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->		
@if ($payment->status === 'Pending' && $payment->payment_type === 'TopUp' || $payment->payment_type === 'Withdraw')
<div class="note note-info">
	<div style="float:left" class="font-grey-gallery">
		Payment request of {!! $payment->amount !!} {!! $payment->currency !!} for {!! $payment->description !!}  @if(!empty($payment->from))from <b>{!! $payment->from !!}</b>@endif @if(!empty($payment->to))to <b>{!! $payment->to !!}</b>@endif
		<a href="#approve" data-toggle="modal"  class="btn green-jungle btn-xs">Approve</a>
		<div class="modal fade" id="approve" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title">Approve</h4>
					</div>
					<div class="modal-body"> 
					   {!! Form::model($payment, ['method' => 'POST', 'action' => ['Admin\Modules\Payments\PaymentsController@forms', $payment->payment_id]]) !!}
							<div class="form-body">
								<div class="form-group @if ($errors->has('amount')) has-error @endif">
									{!! Form::text('amount', null, ['class' => 'form-control text-center', 'style' => 'border:0px;font-size:64px;height:80px;', 'placeholder' => '0,00']) !!}
									@if ($errors->has('amount')) <p class="help-block">{{ $errors->first('amount') }}</p> @endif
								</div>
								<div class="form-group @if ($errors->has('currency')) has-error @endif">
									<div style="margin:auto;width:145px;">
									{!! Form::text('currency', null, ['class' => 'form-control text-center', 'placeholder' => $payment->currency, 'readonly' => '']) !!}
									@if ($errors->has('currency')) <p class="help-block">{{ $errors->first('currency') }}</p> @endif
									</div>
								</div>
								<h4>Fee</h4>
								<div class="form-group">
									<div class="input-group">
										<span class="input-group-addon">{!! $payment->currency !!}</span>  
										<div class="form-group @if ($errors->has('fee')) has-error @endif">
											{!! Form::text('fee', null, ['class' => 'form-control', 'placeholder' => 'Fee']) !!}
											@if ($errors->has('fee')) <p class="help-block">{{ $errors->first('fee') }}</p> @endif
										</div>
									</div>
								</div>
								<h4>Reference Number</h4>
								<div class="form-group @if ($errors->has('reference_id')) has-error @endif">
									{!! Form::text('reference_id', null, ['class' => 'form-control', 'placeholder' => 'Reference Number']) !!}
									@if ($errors->has('reference_id')) <p class="help-block">{{ $errors->first('reference_id') }}</p> @endif
								</div>
								<div class="form-group">
									<span class="input-group-btn">
									   {!! Form::submit('Approve', ['class' => 'btn btn-block green-jungle btn-lg', 'name' => 'approve'] ) !!}
									</span>
								</div>
							</div>
					   {!! Form::close() !!}
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->
		{!! Form::open(['method' => 'POST', 'action' => ['Admin\Modules\Payments\PaymentsController@forms', $payment->payment_id], 'style' => 'display:inline-block;']) !!}
			{!! Form::hidden('amount', $payment->total_amount) !!}
			{!! Form::submit('Decline', ['name' => 'decline', 'class' => 'btn red btn-xs', 'data-toggle' => 'confirmation', 'data-popout' => 'true', 'data-singleton' => 'true', 'data-original-title' => 'Decline! Are you sure?', 'title' => '']) !!}
		{!! Form::close() !!}
	</div>
	<div style="float:right"><cite class="font-grey-silver">{!! Carbon\Carbon::parse($payment->created_at)->diffForHumans() !!}</cite></div>
	<div style="clear:both"></div>
</div>
@else 
@endif 
<div class="row">
	@if($payment->payment_type=='TransferIn')
	<div class="col-sm-4">		
		<h4> From: </h4>		
		<h4> {!! $payment->from !!} </h4>			
	</div>
	<div class="col-sm-4">		
		<h4> To: </h4>
		<h4> {!! $terminal->company_name  !!} </h4>
		<h4> {!! $terminal->email or ''!!} </h4>
		<h4> {!! $terminal->phone or '' !!}  </h4>
		<h4> {!! $terminal->street or '' !!}  </h4>
		<h4> {!! $terminal->city or '' !!}  </h4>
		<h4> {!! $terminal->state or '' !!}  </h4>
		<h4> {!! $terminal->post_code or '' !!}  </h4>
		<h4> {!! $terminal->country_name or '' !!}  </h4>
		<h4> {!! $terminal->term_website or '' !!}  </h4>
		<h4> {!! $terminal->term_email or '' !!}  </h4>
		<h4> {!! $terminal->term_phone or '' !!}  </h4>  
	</div>
   @endif
   @if($payment->payment_type=='TransferOut')	 		
	<div class="col-sm-4">		
		<h4> From: </h4>
		<h4> {!! $terminal->company_name  !!} </h4>
		<h4> {!! $terminal->email or ''!!} </h4>
		<h4> {!! $terminal->phone or '' !!}  </h4>
		<h4> {!! $terminal->street or '' !!}  </h4>
		<h4> {!! $terminal->city or '' !!}  </h4>
		<h4> {!! $terminal->state or '' !!}  </h4>
		<h4> {!! $terminal->post_code or '' !!}  </h4>
		<h4> {!! $terminal->country_name or '' !!}  </h4>
		<h4> {!! $terminal->term_website or '' !!}  </h4>
		<h4> {!! $terminal->term_email or '' !!}  </h4>
		<h4> {!! $terminal->term_phone or '' !!}  </h4>  
	</div>
	<div class="col-sm-4">		
		<h4> To: </h4>		
		<h4> {!! $payment->to !!} </h4>		
	</div>
   @endif
	<div class="col-sm-4 text-right">
		<h4 style="margin-bottom:0px">{!! Carbon\Carbon::parse($payment->created_at)->format('M d, Y') !!} at {!! Carbon\Carbon::parse($payment->created_at)->format('H:i:s e') !!}</h4>
		<div class="font-grey-silver">Date & Time</div>
		<h4 style="margin-bottom:0px">{!! $payment->payment_id !!}</h4>
		<div class="font-grey-silver">Payment ID</div>  
		<h4 style="margin-bottom:0px">
			@if(!empty($payment->payment_method))
			<img src="/uploads/paymentmethods/bank.png" style="height:45px">
			@endif
			</br>{!! $payment->payment_type !!}
		</h4>
		<div class="font-grey-silver">Method & Type</div> 
		<h4 style="margin-bottom:0px">{!! $payment->reference_id !!}</h4>
		<div class="font-grey-silver">Reference</div> 		
		<div class="margin-top-10"><span class="label label-sm @if ($payment->status === 'Approved') label-success @elseif ($payment->status === 'Pending') label-info @elseif ($payment->status === 'Requested') label-info @elseif ($payment->status === 'Refunded') label-purple @elseif ($payment->status === 'Canceled') label-warning @elseif ($payment->status === 'Chargeback') label-black @elseif ($payment->status === 'Declined') label-danger @else label-default @endif">{!! $payment->status !!}</span></div>
		<div class="font-grey-silver">Payment Status</div> 
	</div>
</div>
<div class="table-scrollable">
	<table class="table table-hover table-light">
		<thead>
			<tr>
				<th>Description</th>   
				<th style="width:1%"></th>
				<th class="text-right" style="width:1%">Amount</th>    
			</tr>
		</thead>
		<tbody>
			<tr> 
				<td><span style="font-size:18px;font-weight:300;">{!! $payment->description !!}</span></td>  
				<td></td>
				<td class="text-right" style="white-space:nowrap"><span style="font-size:18px;font-weight:300;">{!! $payment->amount !!}</span> <span style="font-size:18px;font-weight:300;">{!! $payment->currency !!}</span></td>          
			</tr>
			<tr>
				<td></td>
				<td><span style="font-size:18px;font-weight:300;">Fee</span></td>
				<td class="text-right" style="white-space:nowrap"><span style="font-size:18px;font-weight:300;">{!! $payment->fee !!} {!! $payment->currency !!}</span></td>
			</tr>
			<tr>
				<td></td>
				<td><span style="font-size:18px;font-weight:300;">Total</span></td>
				<td class="text-right" style="white-space:nowrap"><span style="font-size:18px;font-weight:300;">{!! $total !!} {!! $payment->currency !!}</span></td>
			</tr>
		</tbody>
	</table>
</div>
<h4 class="page-title">
	Documents
	<a class="btn green-jungle bold" data-toggle="modal" href="#document">Add <i class="fa fa-plus"></i></a>
</h4>
<div class="modal fade" id="document" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title">Add a Document</h4>
			</div>
			<div class="modal-body"> 
				{!! Form::model($payment, ['method' => 'POST', 'files' => 'true', 'action' => ['Admin\Modules\Payments\PaymentsController@document', $payment->payment_id ]]) !!}
					<div class="form-body">
						<div class="form-group @if ($errors->has('name')) has-error @endif">
                            <label>Title</label>
                            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Title']) !!}
                            @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
                        </div>
						<div class="form-group">
							{!! Form::file('upload_file') !!}
							@if ($errors->has('upload_file')) <p class="help-block">{{ $errors->first('upload_file') }}</p> @endif
						</div>						
						<div class="form-group">
							<span class="input-group-btn">
								{!! Form::submit('Add', ['class' => 'btn btn-block green-jungle btn-lg'] ) !!}
							</span>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
	<!-- /.modal-dialog -->
</div>
@if (count($documents) > 0)
<div class="table-scrollable">
	<table class="table table-light table-bordered table-striped table-hover">
		<thead>
			<tr>
				<th style="width:1%">Date</th>
				<th>Description</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($documents as $document)
			<tr>    
				<td class="text-center"><span class="font-sm uppercase">{!! Carbon\Carbon::parse($document->created_at)->format('M') !!}</span> </br> <span style="font-size:22px;font-weight:300;">{!! Carbon\Carbon::parse($document->created_at)->format('d') !!}</span></td>   
				<td><span style="font-size:18px;font-weight:300;"><a href="/admin/payments/{!! $payment->payment_id !!}/document/{!! $document->document_id !!}">{!! $document->name !!}</a></span></br>ID {!! $document->document_id !!}</td>										
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
<div class="row">
	<div class="col-md-6 margin-top-10">
		Showing {!! $documents->firstItem() !!} to {!! $documents->lastItem() !!} of {!! $documents->total() !!}
	</div>
	<div class="col-md-6 text-right">
		{!! $documents->links() !!}
	</div>
</div>
@endif
<!-- END PAGE CONTENT-->
@stop