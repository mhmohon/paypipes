
<div class="modal fade" id="editcomment{!! $note->id !!}" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Edit a Comment</h4>
				</div>
				<div class="modal-body">
					  {!! Form::model($note, ['method' => 'PATCH', 'url' => url('admin/transactions/'.@$payment->transaction_id.'/comment/'. @$note->id)]) !!}
					  {{ csrf_field() }}
						<div class="form-body">
							<div class="form-group @if ($errors->has('notes')) has-error @endif">
	                            {!! Form::textarea('notes', $note->notes, ['class' => 'form-control', 'placeholder' => 'Type your comment ...']) !!}
	                            @if ($errors->has('notes')) <p class="help-block">{{ $errors->first('notes') }}</p> @endif
	                        </div>
							<div class="form-group">
								<span class="input-group-btn">
									{!! Form::submit('Edit', ['class' => 'btn btn-block green-jungle btn-lg'] ) !!}
								</span>
							</div>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
	<!-- /.modal-dialog -->
</div>
