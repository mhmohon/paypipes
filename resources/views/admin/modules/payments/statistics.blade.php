@extends('html')

@section('content')
@include('modules/payments/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->	
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat blue">
            <div class="visual">
                <i class="fa fa-credit-card"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="{!! $paymentcount !!}">0</span>
                </div>
                <div class="desc"> Payments </div>
            </div>
            <a class="more" href="javascript:;"> View more
                <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat red">
            <div class="visual">
                <i class="fa fa-eur"></i>
            </div>
            <div class="details">
                <div class="number">
                    € <span data-counter="counterup" data-value="{!! $paymentturnovereur !!}">0</span>
                </div>
                <div class="desc"> Turnover </div>
            </div>
            <a class="more" href="javascript:;"> View more
                <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat green">
            <div class="visual">
                <i class="fa fa-line-chart"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="{!! number_format($paymentapproval, 0) !!}">0</span>%
                </div>
                <div class="desc"> Approval Rate</div>
            </div>
            <a class="more" href="javascript:;"> View more
                <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat purple">
            <div class="visual">
                <i class="fa fa-globe"></i>
            </div>
            <div class="details">
                <div class="number">
                    <span data-counter="counterup" data-value="{!! $paymentcountrycount !!}">0</span>
                </div>
                <div class="desc"> Payments Countries </div>
            </div>
            <a class="more" href="javascript:;"> View more
                <i class="m-icon-swapright m-icon-white"></i>
            </a>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<div id="pop_div"></div>
@areachart('Chart', 'pop_div')
<!-- END PAGE CONTENT-->
@stop

