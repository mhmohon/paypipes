@extends('html')

@section('content') 
@include('modules/payments/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->	
<div class="tabbable tabbable-tabdrop">
    @include('modules/payments/includes/paymenttabs')
    <div class="tab-content">
        <div class="tab-pane active">
            <table class="table table-advance table-condensed table-bordered table-striped table-hover order-column" id="sample_1">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>ID</th>
                        <th>MID</th>
                        <th>TID</th>
                        <th>FirstName</th>
                        <th>LastName</th>
                        <th>Email</th>
                        <th>Date</th>
                        <th>Description</th>
                        <th>PaymentMethod</th>
                        <th>Currency</th>
                        <th>Amount</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 0 ?>    
                    @foreach ($payments as $payment)
                    <?php $i++ ?>
                    <tr>                                                             
                        <td>{!! $i !!}</td>
                        <td>{!! $payment->id !!}</td>
                        <td>{!! $payment->mid !!}</td>
                        <td>{!! $payment->tid !!}</td>
                        <td>{!! $payment->firstname !!}</td>
                        <td>{!! $payment->lastname !!}</td>
                        <td>{!! $payment->email !!}</td>
                        <td style="white-space: nowrap;">{!! $payment->date !!}</td>
                        <td>{!! $payment->description !!}</td>
                        <td>{!! $payment->paymentmethod !!}</td>
                        <td>{!! $payment->currency !!}</td>
                        <td>{!! $payment->amount !!}</td>
                        <td><span class="label label-sm @if ($payment->status === 'Approved') label-success @elseif ($payment->status === 'Requested') label-info @elseif ($payment->status === 'Refunded') label-purple @elseif ($payment->status === 'Canceled') label-warning @elseif ($payment->status === 'Chargeback') label-black @elseif ($payment->status === 'Pending') label-info @elseif ($payment->status === 'Declined') label-danger @else label-default @endif">{!! $payment->status !!}</span></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->
@stop

