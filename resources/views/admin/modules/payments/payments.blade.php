@extends('admin/html')

@section('content')
@include('admin/modules/payments/includes/pageheader')
<div class="portlet-body">
	@if (count($payments) == 0)
	<div class="row">
		<div class="col-sm-6">
			{!! Form::open(['action' => 'Admin\Modules\Payments\PaymentsController@index', 'method' => 'GET']) !!}
				<div class="input-group input-medium">
					{!! Form::text('search', Request::get('search'), ['class' => 'form-control input-medium', 'placeholder' => 'Search']) !!}
					<span class="input-group-btn">
						{!! Form::button('<i class="icon-magnifier"></i>', ['class' => 'btn green-jungle', 'type'=>'submit']) !!}
					</span>
				</div>
			{!! Form::close() !!}
		</div>
	</div>
	<p class="text-center font-grey-silver margin-top-15">There are no payments to display</p>
	@else
	<div class="row">
		<div class="col-sm-6">
			{!! Form::open(['action' => 'Admin\Modules\Payments\PaymentsController@index', 'method' => 'GET']) !!}
				<div class="input-group input-medium">
					{!! Form::text('search', Request::get('search'), ['class' => 'form-control input-medium', 'placeholder' => 'Search']) !!}
					<span class="input-group-btn">
						{!! Form::button('<i class="icon-magnifier"></i>', ['class' => 'btn green-jungle', 'type'=>'submit']) !!}
					</span>
				</div>
			{!! Form::close() !!}
		</div>
		@if (count($payments) > 0)
            <div class="col-sm-6 text-right  no-print" style="margin-top:5px">
                <a href="{{url('admin/payments/pdf-convert'.$pg)}}" class="btn btn-sm red"><i class="fa fa-file-pdf-o"></i> PDF</a>
                <a href="{{url('admin/payments/csv-convert'.$pg)}}" class="btn btn-sm green-jungle"><i class="fa fa-file-excel-o"></i> CSV</a>
            </div>
        @endif
	</div>
	<div class="table-scrollable">
		<table class="table table-condensed table-bordered table-striped table-hover">
			<thead>
				<tr>
					<th style="width:1%">#</th>
					<th style="width:1%">Payment</br>ID</th>
					<th style="width:1%">From</br>To</th>
					<th style="width:1%">Date</br>& Time</th>
					<th>Description</th>
					<th style="width:1%" class="text-center">Method</br>Type</th>
					<th style="width:1%" class="text-right">Amount</th>
					<th style="width:1%">Status</th>
					<th style="width:1%">Action</th>
				</tr>
			</thead>
			<tbody>
				<?php $i = 0 ?>
				@foreach ($payments as $payment)
				<?php $i++ ?>
				<tr>
					<td>{!! $i !!}</td>
					<td>{!! $payment->payment_id !!}</td>
					<td>{!! $payment->from !!}</br>{!! $payment->to !!}</td>
					<td style="white-space:nowrap;">{!! $payment->created_at->format('M d, Y') !!}</br>{!! $payment->created_at->format('H:i:s e') !!}</td>
					<td>{!! $payment->description !!}</td>
					<td class="text-center">{!! $payment->payment_method !!}</br>{!! $payment->payment_type !!}</td>
					<td class="text-right">{!! $payment->amount !!}</br>{!! $payment->currency !!}</td>
					<td><span class="label label-sm @if ($payment->status === 'Approved') label-success @elseif ($payment->status === 'Pending') label-info @elseif ($payment->status === 'Requested') label-info @elseif ($payment->status === 'Refunded') label-purple @elseif ($payment->status === 'Canceled') label-warning @elseif ($payment->status === 'Chargeback') label-black @elseif ($payment->status === 'Declined') label-danger @else label-default @endif">{!! $payment->status !!}</span></td>
					<td><a href="/admin/payments/{!! $payment->payment_id !!}" class="btn blue btn-xs"><i class="fa fa-pencil"></i></a></td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	<div class="row">
		<div class="col-md-6 margin-top-10">
			Showing {!! $payments->firstItem() !!} to {!! $payments->lastItem() !!} of {!! $payments->total() !!}
		</div>
		<div class="col-md-6 text-right">
			{{ $payments->appends(Request::except('page'))->links() }}
		</div>
	</div>
	@endif
</div>
<!-- END PAGE CONTENT-->
@stop

