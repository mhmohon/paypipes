@extends('admin/html')

@section('content')
@include('admin/modules/payments/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->	
<div class="table-scrollable">
	<table class="table table-light table-hover">
		<tbody>
			<tr>
				<td style="width:30%">Name</td>
				<td>{!! $document->name !!}</td>
			</tr>
			<tr>
				<td>File Type</td>
				<td style="text-transform:uppercase">{!! $document->file_type !!}</td>
			</tr>
			<tr>
				@if ($document->file_type === 'pdf') <td colspan="2"><embed src="/{!! $document->file_path !!}" width="100%" height="600px" /></td>
				@elseif ($document->file_type === 'jpg') <td colspan="2"><img src="/{!! $document->file_path !!}" style="max-width:100%" /></td>
				@elseif ($document->file_type === 'png') <td colspan="2"><img src="/{!! $document->file_path !!}" style="max-width:100%" /></td>
				@else <td colspan="2"></td>
				@endif
			</tr>
		</tbody>
	</table>
</div>
<!-- END PAGE CONTENT-->
@stop