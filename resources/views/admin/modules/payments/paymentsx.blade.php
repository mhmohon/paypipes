@extends('admin/html')

@section('content') 
@include('admin/modules/payments/includes/pageheader')
<!-- BEGIN PAGE CONTENT-->
<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add Payment</h4>
            </div>
            <div class="modal-body"> 
                {!! Form::open(['url' => 'admin/payments/store','method' => 'POST']) !!}
				<input name="_token" id="token" value="{{ csrf_token() }}" type="hidden">
                    <div class="form-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="@if ($errors->has('orginal_date')) has-error @endif">
                                        <label>Payment Date</label>
                                        {!! Form::text('orginal_date', null, ['class' => 'form-control form-control-inline input-medium date-picker']) !!}
                                        @if ($errors->has('orginal_date')) <p class="help-block">{{ $errors->first('orginal_date') }}</p> @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="@if ($errors->has('reference_id')) has-error @endif">
                                        <label>Reference ID</label>
                                        {!! Form::text('reference_id', null, ['class' => 'form-control', 'placeholder' => 'Reference ID']) !!}
                                        @if ($errors->has('reference_id')) <p class="help-block">{{ $errors->first('reference_id') }}</p> @endif
                                    </div>
                                </div>											
                            </div>
                        </div>
						<div class="form-group">
                            <div class="row">
								<div class="col-md-6">
                                    <div class="@if ($errors->has('transafer_type')) has-error @endif">
                                        <label>Transafer Type</label>
                                       {!! Form::select('transfer_type', ['TransferIn' => 'TransferIn','TransferOut' => 'TransferOut'], null, [ 'class' => 'bs-select form-control payment_transaction_type'], ['data-live-search' => '']) !!}
                                        @if ($errors->has('transafer_type')) <p class="help-block">{{ $errors->first('transafer_type') }}</p> @endif
                                    </div>
                                </div>
                                <div class="col-md-6">								
                                    <div id="from_id"class="@if ($errors->has('from')) has-error @endif">
                                        <label>From</label>
										{!! Form::text('from', null, ['class' => 'form-control', 'placeholder' => 'From']) !!}
										@if ($errors->has('from')) <p class="help-block">{{ $errors->first('from') }}</p> @endif
                                    </div>
									
									<div id="to_id" class="hidden @if ($errors->has('to')) has-error @endif">
                                        <label>To</label>
										{!! Form::text('to', null, ['class' => 'form-control', 'placeholder' => 'To']) !!}
										@if ($errors->has('to')) <p class="help-block">{{ $errors->first('to') }}</p> @endif
                                    </div>
                                </div>                                											
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="@if ($errors->has('merchant_id')) has-error @endif">
                                        <label>Merchant ID</label>
                                        {!! Form::select('merchant_id', $merchantlist, null, ['class' => 'bs-select form-control merchant_terminal_list'], ['data-live-search' => '']) !!}
                                        @if ($errors->has('merchant_id')) <p class="help-block">{{ $errors->first('merchant_id') }}</p> @endif
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="@if ($errors->has('terminal_id')) has-error @endif">
                                        <label>Terminal ID</label>
										 <select id="terminal_list" name="terminal_id" data-placeholder="Please Select" class="form-control  terminal_currency_list"></select>              
                                         @if ($errors->has('terminal_id')) <p class="help-block">{{ $errors->first('terminal_id') }}</p> @endif
										
                                    </div>
                                </div>											
                            </div>
                        </div>
                        <div class="form-group @if ($errors->has('description')) has-error @endif">
                            <label>Description</label>
                            {!! Form::text('description', null, ['class' => 'form-control', 'placeholder' => 'Description']) !!}
                            @if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
                        </div>
                        <div class="form-group">
                            <div class="row">
								<div class="col-md-6">
                                    <div class="@if ($errors->has('amount')) has-error @endif">
                                        <label>Amount</label>
										{!! Form::text('amount', null, ['class' => 'form-control', 'placeholder' => 'Amount']) !!}
                                        @if ($errors->has('amount')) <p class="help-block">{{ $errors->first('amount') }}</p> @endif
                                    </div>
                                </div>	
                                <div class="col-md-6">
                                    <div class="@if ($errors->has('currency')) has-error @endif">
                                        <label>Currency</label>
                                        <select id="currency_list" name="currency" data-placeholder="Please Select" class="form-control"></select>
                                        @if ($errors->has('currency')) <p class="help-block">{{ $errors->first('currency') }}</p> @endif
                                    </div>
                                </div>                                										
                            </div>
                        </div>
                        <div class="form-group @if ($errors->has('fee')) has-error @endif">
                            {!! Form::text('fee', null, ['class' => 'form-control', 'placeholder' => 'Fee']) !!}
                            @if ($errors->has('fee')) <p class="help-block">{{ $errors->first('fee') }}</p> @endif
                        </div>
						<!--
                        <div class="form-group">
                            <label>Attach File</label>
                            {!! Form::file('upload_file') !!}
							@if ($errors->has('upload_file')) <p class="help-block">{{ $errors->first('upload_file') }}</p> @endif
                        </div>
						-->
                        <div class="form-group">
                            <span class="input-group-btn">
                                {!! Form::submit('Add', ['class' => 'btn btn-block green-jungle btn-lg'] ) !!}
                            </span>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="batch" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Upload Batch File</h4>
            </div>
            <div class="modal-body"> 
                {!! Form::open(['url' => 'admin/payments/documentcsv','files' => 'true','method' => 'POST']) !!}
				
                    <div class="form-body">
                        <div class="form-group">
                            {!! Form::file('upload_file') !!}
							@if ($errors->has('upload_file')) <p class="help-block">{{ $errors->first('upload_file') }}</p> @endif
                        </div>
						<p class="text-center">Upload csv file to send up to 200 records. Download <a href="{{url('admin/payments/download')}}">csv file template</a>.</p>
                        <div class="form-group">
                            <span class="input-group-btn">
                                {!! Form::submit('Upload', ['class' => 'btn btn-block green-jungle btn-lg'] ) !!}
                            </span>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="help" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Payments Activity Help</h4>
            </div>
            <div class="modal-body"> 
                <p>Did you know that you can resize the table to full screen with resize button <a href="#" class="btn blue btn-xs"><i class="fa fa-expand"></i></a> and update data with reload button <a href="#" class="btn blue btn-xs"><i class="fa fa-repeat"></i></a> ?</p>
                <p>In order to view details of the payment please click on the "<a href="#">ID number</a>" of the payment in the table.</p>
                <p>Payments activity table can be printed <button class="btn dark btn-xs"><i class="fa fa-print"></i> Print</button> saved as pdf <button class="btn red btn-xs"><i class="fa fa-file-pdf-o"></i> PDF</button> or exported in XML format <button class="btn green-jungle btn-xs"><i class="fa fa-file-excel-o"></i> XML</button> .</p>
                <p>Did you know that you can edit number of rows in the table by selecting number of entries you want to display?</p>
                <p>In the Search you can search for any information from any column of the table. For example "VI" for Visa or "Approved" for Approved. You can also combine "VI" and "Approved" to have more detailed result.</p>
                <p>Did you know that you can sort the data in the column ascending <i class="fa fa-sort-asc"></i> or descending <i class="fa fa-sort-desc"></i> by clinking on the title of the column.</p>
                <div class="table-scrollable">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th>Status</th>
                                <th>Description</th>
                            </tr>	
                        </thead>
                        <tbody>
                            <tr>
                                <td><span class="label label-sm label-success">Approved</span></td>
                                <td>Payment has been processed successfully!</td>
                            </tr>
                            <tr>
                                <td><span class="label label-sm label-danger">Declined</span></td>
                                <td>Payment has been declined. Please see <a href="#">declined codes</a> for more details.</td>
                            </tr>
                            <tr>
                                <td><span class="label label-sm label-info">Requested</span></td>
                                <td>Payment has been requested successfully!</td>
                            </tr>
                            <tr>
                                <td><span class="label label-sm label-warning">Canceled</span></td>
                                <td>Payment request has been canceled!</td>
                            </tr>
                            <tr>
                                <td><span class="label label-sm label-purple">Refunded</span></td>
                                <td>Payment has been refunded!</td>
                            </tr>
                            <tr>
                                <td><span class="label label-sm label-black">Chargeback</span></td>
                                <td>There was a chargeback!</td>
                            </tr>
                            <tr>
                                <td><span class="label label-sm label-default">Error</span></td>
                                <td>There was an error! Please see <a href="#">error codes</a> for more details.</td>
                            </tr>
                        </tbody>
                    </table>
                </div> 
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="portlet-body">
	
	<div class="row">
		<div class="col-sm-6">
			<div class="row">
				@if (count($payments)> 0)
					<div class="col-sm-3">
						@include('admin/includes/item_per_page')
					</div>
				@endif
				<div class="col-sm-6">
					  {!! Form::open(['url' => 'admin/payments','method' => 'get']) !!}
						<div class="input-group">
							{!! Form::text('search', Request::get('search'), ['class' => 'form-control', 'placeholder' => 'Search']) !!}
							<span class="input-group-btn">
								{!! Form::button('<i class="icon-magnifier"></i>', ['class' => 'btn green-jungle', 'type'=>'submit']) !!}
							</span>
						</div>
					{!! Form::close() !!}
				</div>
				<div class="col-sm-3">
				<a href="#advanced" data-toggle="modal" class="btn green-jungle">Advanced <i class="icon-magnifier"></i></a>
				<div class="modal fade" id="advanced" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
								<h4 class="modal-title">Advanced Search</h4>
							</div>
							<div class="modal-body">															                           
								 {!! Form::open(['url' => 'admin/payments/advanced','method' => 'post']) !!}
								<input type="hidden" name='rows' value="{{old('rows')}}" >
									<div class="form-body">
										<div class="form-group">
											<div class="row">
												<div class="col-md-6">
													<label>Date From</label>
													<div class="@if ($errors->has('from')) has-error @endif">
														{!! Form::text('date_from', old('date_from'), ['class' => 'form-control date-picker']) !!}
														@if ($errors->has('date_from')) <p class="help-block">{{ $errors->first('date_from') }}</p> @endif
													</div>
												</div>
												<div class="col-md-6">
													<label>Date To</label>
													<div class="@if ($errors->has('to')) has-error @endif">
														{!! Form::text('date_to', old('date_to'), ['class' => 'form-control date-picker']) !!}
														@if ($errors->has('date_to')) <p class="help-block">{{ $errors->first('date_to') }}</p> @endif
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<div class="col-md-4">
													<label>Amount From</label>
													<div class="@if ($errors->has('from')) has-error @endif">
														{!! Form::text('amount_from', old('amount_from'), ['class' => 'form-control', 'placeholder' => '0.00']) !!}
														@if ($errors->has('amount_from')) <p class="help-block">{{ $errors->first('amount_from') }}</p> @endif
													</div>
												</div>
												<div class="col-md-4">
													<label>Amount To</label>
													<div class="@if ($errors->has('to')) has-error @endif">
														{!! Form::text('amount_to', old('amount_to'), ['class' => 'form-control', 'placeholder' => '0.00']) !!}
														@if ($errors->has('amount_to')) <p class="help-block">{{ $errors->first('amount_to') }}</p> @endif
													</div>
												</div>
												<div class="col-md-4">
													<label>Currency</label>
													<div class="@if ($errors->has('currency')) has-error @endif">
														<select name='currency' class="bs-select form-control">
															<option value=''>Currency</option>
															@foreach($currency as $value)
															<option value='{{$value}}' {{old('currency') == $value ?'selected':''}}>{{$value}}</option>
															@endforeach						
														</select>
														@if ($errors->has('currency')) <p class="help-block">{{ $errors->first('currency') }}</p> @endif
													</div>
												</div>
											</div>
										</div>
										<div class="form-group @if ($errors->has('merchant_id')) has-error @endif">
											<label>Merchant ID</label>
											{!! Form::select('merchant_id', $merchantlist, old('merchant_id'), ['class' => 'bs-select form-control', 'data-live-search' => '']) !!}                                        
											@if ($errors->has('merchant_id')) <p class="help-block">{{ $errors->first('merchant_id') }}</p> @endif
										</div>
										<div class="form-group">
											<div class="row">
												<div class="col-md-12">													
													<label>Status</label>
													<div class="@if ($errors->has('status')) has-error @endif">
														<select name='status' class="bs-select form-control">
															<option value=''>Payment Status</option>
															@foreach($status as $value)
															<option value='{{$value}}' {{old('payment_method') == $value ?'selected':''}}>{{$value}}</option>
															@endforeach
														</select>
														@if ($errors->has('status')) <p class="help-block">{{ $errors->first('status') }}</p> @endif
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<span class="input-group-btn">
												{!! Form::submit('Search', ['class' => 'btn btn-block green-jungle btn-lg'] ) !!}
											</span>
										</div>
									</div>
								{!! Form::close() !!}                   
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dialog -->
				</div>	
			</div>
			</div>
		</div>
		@if (count($payments) > 0)
		<div class="col-sm-6 text-right  no-print" style="margin-top:5px">	
			<a href="{{url('admin/payments/pdf-convert'.$pg)}}" class="btn btn-sm red"><i class="fa fa-file-pdf-o"></i> PDF</a>
			<a href="{{url('admin/payments/csv-convert'.$pg)}}" class="btn btn-sm green-jungle"><i class="fa fa-file-excel-o"></i> CSV</a>
		</div>
		@endif
	</div>
	@if (count($payments) == 0)
	<p class="text-center font-grey-silver margin-top-15">There are no payments to display</p>
	@else
	<div class="table-scrollable">
		<table class="table table-condensed table-bordered table-striped table-hover">
			<thead>
				<tr>
					<th style="width:1%">#</th>
					<th style="width:1%">Date</br>& Time</th>
					<th>Description</th>
					<th style="width:1%">Gateway</th>
					<th style="width:1%">Payment</br>Type</th>
					<th style="width:1%" class="text-right">Amount</th>
					<th style="width:1%">Status</th>
					<th style="width:1%">Action</th>
				</tr>
			</thead>
			<tbody>
				<?php $i = 0 ?>    
				@foreach ($payments as $payment)
				<?php $i++ ?>
				<tr>                                                             
					<td>{!! $i !!}</td>
					<td style="white-space:nowrap;">{!! Carbon\Carbon::parse($payment->created_at)->format('M d, Y') !!} </br> {!! Carbon\Carbon::parse($payment->created_at)->format('H:i:s e') !!}</td>
					<td>
						{!! $payment->description !!}</br>
						<cite>
							@if ($payment->payment_type == "TransferIn")
								From {!! $payment->from !!} to @if (!empty($payment->psp_id)) PSPID{!! $payment->psp_id !!}@elseif (!empty($payment->merchant_id))   MID{!! $payment->merchant_id !!} TID{!! $payment->terminal_id !!}@endif
							@else
								From @if (!empty($payment->psp_id)) PSPID{!! $payment->psp_id !!}@elseif (!empty($payment->merchant_id))   MID{!! $payment->merchant_id !!} TID{!! $payment->terminal_id !!} to {!! $payment->to !!} @endif
							@endif
						</cite>
					</td>
					<td>
						@if(!empty($payment->payment_method))
						<img src="/uploads/paymentmethods/bank.png" style="height:30px">
						@endif
					</td>
					<td>{!! $payment->payment_type !!}</td>
					<td class="text-right"><b>{!! $payment->amount !!}</b></br>{!! $payment->currency !!}</td>
					<td><span class="label label-sm @if ($payment->status === 'Approved') label-success @elseif ($payment->status === 'Pending') label-info @elseif ($payment->status === 'Requested') label-info @elseif ($payment->status === 'Refunded') label-purple @elseif ($payment->status === 'Canceled') label-warning @elseif ($payment->status === 'Chargeback') label-black @elseif ($payment->status === 'Declined') label-danger @else label-default @endif">{!! $payment->status !!}</span></td>   
					<td><a href="/admin/payments/{!! $payment->payment_id !!}" class="btn blue btn-xs"><i class="fa fa-pencil"></i></a></td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	<div class="row">
		<div class="col-md-6 margin-top-10">
			Showing {!! $payments->firstItem() !!} to {!! $payments->lastItem() !!} of {!! $payments->total() !!}
		</div>
		<div class="col-md-6 text-right">
			{!! $payments->links() !!}
		</div>
	</div>
	@endif
</div>
<!-- END PAGE CONTENT-->
@stop

