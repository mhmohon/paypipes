<!-- BEGIN PAGE HEADER-->
<div class="page-bar">
    <h3 class="page-title" style="float:left;">{!! $pagetitle !!}</h3>
	<!--
	<a href="#help" data-toggle="modal" style="padding:15px 0px 0px 10px;font-size:24px;display:inline-block;" class="popovers" data-container="body" data-trigger="hover" data-placement="bottom" data-content="Help"><i class="icon-question font-green-jungle"></i></a>
	-->

    <ul class="page-breadcrumb" style="float:right;padding-top:18px;">
        @if(isset($breadcrumb_level1) && !empty($breadcrumb_level1))
		<li>
            {!! $breadcrumb_level1 or '' !!}
            @if (Request::is('admin/payments/*')) <i class="fa fa-angle-right"></i>
            @elseif (Request::is('admin/topuppayments')) <i class="fa fa-angle-right"></i>
            @elseif (Request::is('admin/withdrawpayments')) <i class="fa fa-angle-right"></i>
            @elseif (Request::is('admin/paymentsstatistics')) <i class="fa fa-angle-right"></i>
			@elseif (Request::is('admin/transactions/*')) <i class="fa fa-angle-right"></i>
            @endif
        </li>
		@endif
		@if(isset($breadcrumb_level2) && !empty($breadcrumb_level2))
        <li>
            {!! $breadcrumb_level2 or '' !!}
            @if (Request::is('admin/payments/*/*')) <i class="fa fa-angle-right"></i>
			@elseif (Request::is('admin/transactions/*/*')) <i class="fa fa-angle-right"></i>
			@endif
        </li>
		@endif
		@if(isset($breadcrumb_level3) && !empty($breadcrumb_level3))
        <li>
            {!! $breadcrumb_level3 or '' !!}
        </li>
		@endif
    </ul>
</div>
<!-- END PAGE HEADER-->
