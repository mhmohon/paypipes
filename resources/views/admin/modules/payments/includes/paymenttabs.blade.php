<ul class="nav nav-tabs">
    <li class="{{ Request::is('admin/payments/'.$payment_id) ? 'active' : '' }}">
        <a href="/admin/payments/{!! $payment_id !!}">Payment</a>
    </li>
    <li class="{{ Request::is('admin/payments/'.$payment_id.'/relatedpayments') ? 'active' : '' }}">
        <a href="/admin/payments/{!! $payment_id !!}/relatedpayments">Related Payments</a>
    </li>							
</ul>

