<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>PayPipes | PMS | 404 Error</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

		<meta content="PayPipes is a Payment Management System PMS developed based on open API architecture to simplify connectivity and management of Acquiring Banks, Alternative Payments, Risk Management Tools and other related Providers." name="description">
		<meta content="PayPipes" name="keywords">
		<meta content="PayPipes" name="author">

		<meta property="og:site_name" content="PayPipes">
		<meta property="og:title" content="PayPipes | Payment Management System">
		<meta property="og:description" content="PayPipes is a Payment Management System PMS developed based on open API architecture to simplify connectivity and management of Acquiring Banks, Alternative Payments, Risk Management Tools and other related Providers.">
		<meta property="og:type" content="website">
		<meta property="og:image" content="">
		<meta property="og:url" content="www.paypipes.com">

		<link rel="shortcut icon" href="favicon.ico">
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <!-- <link href="/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" /> -->
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="/assets/global/pages/css/error.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
	</head>
    <!-- END HEAD -->

    <body class=" page-500-full-page">
        <div class="row">
            <div class="col-md-12 page-500">
                <div class=" number font-red"> 500 </div>
                <div class=" details">
                    <h3>Oops! Something went wrong.</h3>
                    <p> We are fixing it! Please come back in a while.
                        <br/> </p>
                    <p>
                        <a href="{{ URL::previous() }}" class="btn red btn-outline"> Go Back </a>
                        <br> </p>
                </div>
            </div>
        </div>     
	</body>

</html>