@extends('account/html')

@section('pageheader')
    @include('account/modules/dashboard/includes/pageheader')
@stop
@section('content')
<meta name="_token" content="{!! csrf_token() !!}" />
<!-- BEGIN PAGE CONTENT	-->
@foreach ($request_payments as $request_payment)
<div class="note note-info">
	<div style="float:left" class="font-grey-gallery">
		Payment request of {!! $request_payment->amount !!} {!! $request_payment->currency !!} for {!! $request_payment->description !!} from {!! $request_payment->added_by !!} ({!! $request_payment->to !!})
		{!! Form::open(['method' => 'POST',  'url' => 'account/dashboard/pay/'.$request_payment->payment_id , 'class'=>'frmsubmit', 'style' => 'display:inline-block;']) !!}
			{!! Form::hidden('amount', $request_payment->total_amount) !!}
			{!! Form::submit('Approve', ['name' => 'pay', 'class' => 'btn green-jungle btn-xs', 'data-toggle' => 'confirmation', 'data-popout' => 'true', 'data-singleton' => 'true', 'data-original-title' => 'Approve! Are you sure?', 'title' => '']) !!}
		{!! Form::close() !!}
		{!! Form::open(['method' => 'POST', 'url' => 'account/dashboard/pay/'.$request_payment->payment_id , 'class'=>'frmsubmit', 'style' => 'display:inline-block;']) !!}
			{!! Form::hidden('amount', $request_payment->total_amount) !!}
			{!! Form::submit('Decline', ['name' => 'decline', 'class' => 'btn red btn-xs', 'data-toggle' => 'confirmation', 'data-popout' => 'true', 'data-singleton' => 'true', 'data-original-title' => 'Decline! Are you sure?', 'title' => '']) !!}
		{!! Form::close() !!}
	</div>
	<div style="float:right"><cite class="font-grey-silver">{!! Carbon\Carbon::parse($request_payment->created_at)->diffForHumans() !!}</cite></div>
	<div style="clear:both"></div>
</div>
@endforeach
<div class="row">
    <div class="col-md-4">
		@if (count($wallets) > 0)
		@else
			@if ($wallettype->add_wallet == '1')
			<h4>Wallet Currency</h4>
			<div class="font-grey-silver margin-bottom-10">Add Wallet Currency to send and receive payments</div>
			<a class="btn blue-madison btn-block btn-outline margin-bottom-10" href="#addwallet" data-toggle="modal">Add Wallet Currency</a>
			<div class="modal fade" id="addwallet" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Add Wallet Account</h4>
						</div>
						<div class="modal-body">
							 {!! Form::open(['url' => 'account/dashboard/forms' , 'class'=>'frmsubmit']) !!}
								<div class="form-body">
									<div class="margin-bottom-15 font-grey-silver">Select currency for your Wallet Account. You can add different currencies later.</div>
									<div class="form-group @if ($errors->has('currency')) has-error @endif">
										{!! Form::select('currency', $currencies, null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
										@if ($errors->has('currency')) <p class="help-block">{{ $errors->first('currency') }}</p> @endif
									</div>
									<div class="form-group">
										<span class="input-group-btn">
											{!! Form::submit('Add', ['class' => 'btn btn-block green-jungle btn-lg', 'name' => 'addwallet'] ) !!}
										</span>
									</div>
								</div>
							{!! Form::close() !!}
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			@else
			<h4>Wallet Account</h4>
			<div class="font-grey-silver margin-bottom-10">Wallet Account is disabled</div>
			@endif
		@endif
		@if (count($wallets) > 0)
        <h4>Wallet Balance</h4>
        @if ($wallettype->pay == '1' && $wallettype->request == '1')<div class="font-grey-silver">You don't need a balance to pay or request payments</div>@endif
		<div class="row margin-top-10">
			@if ($wallettype->send == '1')
			<div class="col-md-4">
				<div class="light margin-bottom-10" style="padding:0;">
					<a class="btn blue-madison btn-block" href="#send" data-toggle="modal">
						<div class="text-center"  style="font-size:42px;"><i class="icon-action-redo"></i></div>
						<div class="text-center"  style="font-size:18px;font-weight:300;">Send</div>
					</a>
				</div>
			</div>
			<div class="modal fade" id="send" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title"><i class="icon-action-redo"></i> Send Payments</h4>
						</div>
						<div class="modal-body">
							{!! Form::model($account, ['method' => 'POST', 'url' => 'account/dashboard/forms' , 'class'=>'frmsubmit']) !!}
								<div class="form-body">
									<div class="text-center margin-bottom-15 font-grey-silver">Send payments to almost anyone with an email address and it will go to their Wallet account. If they don’t have an account, we'll help them sign up for free.</div>
									<div class="form-group @if ($errors->has('to_email')) has-error @endif">
										{!! Form::text('to_email', null, ['class' => 'form-control input-lg text-center', 'placeholder' => 'Email']) !!}
										@if ($errors->has('to_email')) <p class="help-block">{{ $errors->first('to_email') }}</p> @endif
									</div>
									<div class="form-group @if ($errors->has('amount')) has-error @endif">
										{!! Form::text('amount', null, ['class' => 'form-control text-center', 'style' => 'border:0px;font-size:64px;height:80px;', 'placeholder' => '0,00']) !!}
										@if ($errors->has('amount')) <p class="help-block">{{ $errors->first('amount') }}</p> @endif
									</div>
									<div class="form-group @if ($errors->has('currency')) has-error @endif">
										<div style="margin:auto;width:145px;">
											{!! Form::select('currency', $wallets_currency, null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
											@if ($errors->has('currency')) <p class="help-block">{{ $errors->first('currency') }}</p> @endif
										</div>
									</div>
									<div class="form-group @if ($errors->has('description')) has-error @endif">
										{!! Form::text('description', null, ['class' => 'form-control input-lg text-center', 'placeholder' => 'Add a Note']) !!}
										@if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
									</div>
									<div class="form-group">
										<span class="input-group-btn">
											{!! Form::submit('Next', ['class' => 'btn btn-block green-jungle btn-lg', 'name' => 'send'] ) !!}
										</span>
									</div>
								</div>
							{!! Form::close() !!}
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			@endif
			@if ($wallettype->pay == '1')
			<div class="col-md-4">
				<div class="light margin-bottom-10" style="padding:0;">
					<a class="btn blue-madison btn-block" href="#pay" data-toggle="modal">
						<div class="text-center"  style="font-size:42px;"><i class="icon-basket-loaded"></i></div>
						<div class="text-center"  style="font-size:18px;font-weight:300;">Pay</div>
					</a>
				</div>
			</div>
			<div class="modal fade" id="pay" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title"><i class="icon-basket-loaded"></i> Pay for goods or services</h4>
						</div>
						<div class="modal-body">
							{!! Form::Model($account,['method' => 'POST','url' => 'account/dashboard/forms' , 'class'=>'frmsubmit']) !!}
								<div class="form-body">
									<div class="text-center margin-bottom-15 font-grey-silver">Pay for goods or services to a Merchant with a Wallet account. Enter Merchant email below and we'll we check the email for you.</div>
									<div class="form-group @if ($errors->has('to_email')) has-error @endif">
										{!! Form::text('to_email', null, ['class' => 'form-control text-center', 'placeholder' => 'Email']) !!}
										@if ($errors->has('to_email')) <p class="help-block">{{ $errors->first('to_email') }}</p> @endif
									</div>
									<div class="form-group @if ($errors->has('amount')) has-error @endif">
										{!! Form::text('amount', null, ['class' => 'form-control text-center', 'style' => 'border:0px;font-size:64px;height:80px;', 'placeholder' => '0,00']) !!}
										@if ($errors->has('amount')) <p class="help-block">{{ $errors->first('amount') }}</p> @endif
									</div>
									<div class="form-group @if ($errors->has('currency')) has-error @endif">
										<div style="margin:auto;width:145px;">
										{!! Form::select('currency', $wallets_currency, null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
										@if ($errors->has('currency')) <p class="help-block">{{ $errors->first('currency') }}</p> @endif
										</div>
									</div>
									<div class="form-group @if ($errors->has('description')) has-error @endif">
										{!! Form::text('description', null, ['class' => 'form-control text-center', 'placeholder' => 'Add a Note']) !!}
										@if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
									</div>
									<h4>Shipping Address</h4>
									<div class="form-group @if ($errors->has('street')) has-error @endif">
										{!! Form::text('street', null, ['class' => 'form-control', 'placeholder' => 'Street']) !!}
										@if ($errors->has('street')) <p class="help-block">{{ $errors->first('street') }}</p> @endif
									</div>
									<div class="form-group @if ($errors->has('city')) has-error @endif">
										{!! Form::text('city', null, ['class' => 'form-control', 'placeholder' => 'City',]) !!}
										@if ($errors->has('city')) <p class="help-block">{{ $errors->first('city') }}</p> @endif
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-md-6">
												<div class="@if ($errors->has('country')) has-error @endif">
													{!! Form::select('country', $countries, null, ['class' => 'bs-select form-control', 'data-live-search' => '']) !!}
													@if ($errors->has('country')) <p class="help-block">{{ $errors->first('country') }}</p> @endif
												</div>
											</div>
											<div class="col-md-6">
												<div class="@if ($errors->has('post_code')) has-error @endif">
													{!! Form::text('post_code', null, ['class' => 'form-control', 'placeholder' => 'Post Code']) !!}
													@if ($errors->has('post_code')) <p class="help-block">{{ $errors->first('post_code') }}</p> @endif
												</div>
											</div>
										</div>
									</div>
									<h4>Payment Method</h4>
									<div class="form-group @if ($errors->has('payment_method')) has-error @endif">
										{!! Form::select('payment_method', ['WalletBalance'	=> 'Wallet Balance','NewCard' => 'Add New Card'], null, ['class' => 'bs-select form-control','id' => 'payment_method', 'data-live-search' => '']) !!}
										@if ($errors->has('payment_method')) <p class="help-block">{{ $errors->first('payment_method') }}</p> @endif
									</div>

									<div id="frmNewCard" class="form-group" style="display:none">
										<h4>Card Information</h4>
										<div class="form-group @if ($errors->has('card_holder')) has-error @endif">
											{!! Form::text('card_holder', null, ['class' => 'form-control', 'placeholder' => 'Name (as it appears on your card)']) !!}
											@if ($errors->has('card_holder')) <p class="help-block">{{ $errors->first('card_holder') }}</p> @endif
										</div>
										<div class="form-group @if ($errors->has('card_number')) has-error @endif">
											{!! Form::text('card_number', null, ['class' => 'form-control', 'placeholder' => 'Card Number']) !!}
											@if ($errors->has('card_number')) <p class="help-block">{{ $errors->first('card_number') }}</p> @endif
										</div>

										<div class="form-group">
											<div class="row">
												<div class="col-md-6">
													<div class="@if ($errors->has('exp_month')) has-error @endif">
													{!! Form::select('exp_month', $exp_month, null, ['class' => 'bs-select form-control', 'data-live-search' => '']) !!}
														@if ($errors->has('exp_month')) <p class="help-block">{{ $errors->first('exp_month') }}</p> @endif
													</div>
												</div>
												<div class="col-md-6">
													<div class="@if ($errors->has('exp_year')) has-error @endif">
													{!! Form::select('exp_year', $exp_year, null, ['class' => 'bs-select form-control', 'data-live-search' => '']) !!}
														@if ($errors->has('exp_year')) <p class="help-block">{{ $errors->first('exp_year') }}</p> @endif
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<div class="col-md-6">
													<div class="@if ($errors->has('securecode')) has-error @endif">
														{!! Form::text('securecode', null, ['class' => 'form-control', 'placeholder' => 'Secure Code']) !!}
														@if ($errors->has('securecode')) <p class="help-block">{{ $errors->first('securecode') }}</p> @endif
													</div>
												</div>

											</div>
										</div>

										<h4>Billing Address</h4>
										<div class="form-group @if ($errors->has('street')) has-error @endif">
											{!! Form::text('bill_street', null, ['class' => 'form-control', 'placeholder' => 'Street']) !!}
											@if ($errors->has('bill_street')) <p class="help-block">{{ $errors->first('bill_street') }}</p> @endif
										</div>
										<div class="form-group @if ($errors->has('city')) has-error @endif">
											{!! Form::text('bill_city', null, ['class' => 'form-control', 'placeholder' => 'City',]) !!}
											@if ($errors->has('bill_city')) <p class="help-block">{{ $errors->first('bill_city') }}</p> @endif
										</div>
										<div class="form-group">
											<div class="row">
												<div class="col-md-6">
													<div class="@if ($errors->has('country')) has-error @endif">
														{!! Form::select('bill_country', $countries, null, ['class' => 'bs-select form-control', 'data-live-search' => '']) !!}
														@if ($errors->has('bill_country')) <p class="help-block">{{ $errors->first('bill_country') }}</p> @endif
													</div>
												</div>
												<div class="col-md-6">
													<div class="@if ($errors->has('postcode')) has-error @endif">
														{!! Form::text('bill_post_code', null, ['class' => 'form-control', 'placeholder' => 'Post Code']) !!}
														@if ($errors->has('bill_post_code')) <p class="help-block">{{ $errors->first('bill_post_code') }}</p> @endif
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="form-group">
											<span class="input-group-btn">
												{!! Form::submit('Pay for goods or services', ['class' => 'btn btn-block green-jungle btn-lg', 'name' => 'pay'] ) !!}
											</span>
									</div>
								</div>
							{!! Form::close() !!}
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			@endif
			@if ($wallettype->request == '1')
			<div class="col-md-4">
				<div class="light margin-bottom-10" style="padding:0;">
					<a class="btn blue-madison btn-block" href="#request" data-toggle="modal">
						<div class="text-center"  style="font-size:42px;"><i class="icon-action-undo"></i></div>
						<div class="text-center"  style="font-size:18px;font-weight:300;">Request</div>
					</a>
				</div>
			</div>
			<div class="modal fade" id="request" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title"><i class="icon-action-undo"></i> Request Payment</h4>
						</div>
						<div class="modal-body">
							{!! Form::model($account, ['method' => 'POST', 'url' => 'account/dashboard/forms', 'class'=>'frmsubmit']) !!}
								<div class="form-body">
									<div class="text-center margin-bottom-15 font-grey-silver">You can request payments from almost anyone with an email address, even if they don't have a Wallet account.</div>
									<div class="form-group @if ($errors->has('from_email')) has-error @endif">
										{!! Form::text('from_email', null, ['class' => 'form-control input-lg text-center', 'placeholder' => 'Email']) !!}
										@if ($errors->has('from_email')) <p class="help-block">{{ $errors->first('from_email') }}</p> @endif
									</div>
									<div class="form-group @if ($errors->has('amount')) has-error @endif">
										{!! Form::text('amount', null, ['class' => 'form-control input-lg text-center', 'style' => 'border:0px;font-size:64px;height:80px;', 'placeholder' => '0,00']) !!}
										@if ($errors->has('amount')) <p class="help-block">{{ $errors->first('amount') }}</p> @endif
									</div>
									<div class="form-group @if ($errors->has('currency')) has-error @endif">
										<div style="margin:auto;width:145px;">
										{!! Form::select('currency', $wallets_currency, null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
										@if ($errors->has('currency')) <p class="help-block">{{ $errors->first('currency') }}</p> @endif
										</div>
									</div>
									<div class="form-group @if ($errors->has('description')) has-error @endif">
										{!! Form::text('description', null, ['class' => 'form-control input-lg text-center', 'placeholder' => 'Add a Note']) !!}
										@if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
									</div>
									<div class="form-group">
										<span class="input-group-btn">
											{!! Form::submit('Request Payment', ['name' => 'request', 'class' => 'btn btn-block green-jungle btn-lg'] ) !!}
										</span>
									</div>
								</div>
							{!! Form::close() !!}
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- /.modal -->
			@endif
		</div>
        <div class="table margin-top-10">
            <table class="table table-hover table-light">
                <thead>
                    <tr>
                        <th></th>
                        <th class="text-right" style="width:1%">Current</br>Balance</th>
                        <th class="text-right" style="width:1%">Available</br>Balance</th>
                    </tr>
                </thead>
                <tbody>
					@foreach ($wallets as $wallet)
					<tr>
                        <td>@if ($wallet->primary == '1') <a class="label label-sm label-default" href="#primary" data-toggle="modal">Primary <i class="fa fa-pencil"></i></span> @else @endif</td>

						<td class="text-right"><span class="@if (@$wallet->balance > 0 )font-green-jungle @else font-red @endif" style="font-size:22px;font-weight:300;">{!! $wallet->balance !!}</span></br> <span class="font-sm">{!! $wallet->currency !!}</span></td>
                        <td class="text-right"><span class="@if (@$wallet->balance > 0 )font-green-jungle @else font-red @endif" style="font-size:22px;font-weight:300;">{!! $wallet->balance !!}</span></br> <span class="font-sm">{!! $wallet->currency !!}</span></td>
                    </tr>
					@endforeach
					<div class="modal fade" id="primary" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
									<h4 class="modal-title">Primary Currency</h4>
								</div>
								<div class="modal-body">
									{!! Form::model($account, ['method' => 'POST', 'url' => 'account/dashboard/forms' , 'class'=>'frmsubmit']) !!}
										<div class="form-body">
											<div class="margin-bottom-15 font-grey-silver">Select primary currency for your Wallet Account.</div>
											<div class="form-group @if ($errors->has('currency')) has-error @endif">
												{!! Form::select('currency', $wallets_currency, $primary_currency->currency??'', ['class' => 'form-control'], ['data-live-search' => '']) !!}
												@if ($errors->has('currency')) <p class="help-block">{{ $errors->first('currency') }}</p> @endif
											</div>
											<div class="form-group">
												<span class="input-group-btn">
													{!! Form::submit('Next', ['class' => 'btn btn-block green-jungle btn-lg', 'name' => 'primary'] ) !!}
												</span>
											</div>
										</div>
									{!! Form::close() !!}
								</div>
							</div>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>
					<!--
					<tr>
                        <td colspan="2"><b>Total available in EUR</b></td>
                        <td class="text-right"><span class="font-green-jungle" style="font-size:22px;font-weight:300;">155,50</span></br> <span class="font-sm">EUR</span></td>
                    </tr>
					-->
                </tbody>
            </table>
        </div>
		@if (count($currency_arr) > 0)
			@if ($wallettype->add_currency == '1')
			<a class="btn blue-madison btn-block btn-outline margin-bottom-5" href="#addcurrency" data-toggle="modal">Add Wallet Currency</a>
			<div class="modal fade" id="addcurrency" tabindex="-1" role="dialog" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Add Wallet Currency</h4>
						</div>
						<div class="modal-body">
							{!! Form::model($account, ['method' => 'POST', 'url' => 'account/dashboard/forms' , 'class'=>'frmsubmit']) !!}
								<div class="form-body">
									<div class="margin-bottom-15 font-grey-silver">Select currency for your Wallet Account.</div>
									<div class="form-group @if ($errors->has('currency')) has-error @endif">
										{!! Form::select('currency', $currency_arr, null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
										@if ($errors->has('currency')) <p class="help-block">{{ $errors->first('currency') }}</p> @endif
									</div>
									<div class="form-group">
										<span class="input-group-btn">
											{!! Form::submit('Add', ['class' => 'btn btn-block green-jungle btn-lg', 'name' => 'addcurrency'] ) !!}
										</span>
									</div>
								</div>
							{!! Form::close() !!}
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			@endif
		@endif
		@if ($wallettype->add == '1')
        <a class="btn blue-madison btn-block btn-outline margin-bottom-5" href="#topup" data-toggle="modal">Add Money</a>
		<div class="modal fade" id="topup" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title"><i class="icon-action-redo"></i> Add Money</h4>
					</div>
					<div class="modal-body">
						{!! Form::model($account, ['method' => 'POST', 'url' => 'account/dashboard/forms' , 'class'=>'frmsubmit']) !!}
							<div class="form-body">
								<div class="text-center margin-bottom-15 font-grey-silver">Declare an amount and currency in which you will transfer funds from your bank account to your Wallet account.</div>
								<div class="form-group @if ($errors->has('amount')) has-error @endif">
									{!! Form::text('amount', null, ['class' => 'form-control text-center', 'style' => 'border:0px;font-size:64px;height:80px;', 'placeholder' => '0,00']) !!}
									@if ($errors->has('amount')) <p class="help-block">{{ $errors->first('amount') }}</p> @endif
								</div>
								<div class="form-group @if ($errors->has('currency')) has-error @endif">
									<div style="margin:auto;width:145px;">
									{!! Form::select('currency', $wallets_currency, null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
									@if ($errors->has('currency')) <p class="help-block">{{ $errors->first('currency') }}</p> @endif
									</div>
								</div>
								<div class="form-group">
									<span class="input-group-btn">
										{!! Form::submit('Next', ['class' => 'btn btn-block green-jungle btn-lg', 'name' => 'topup'] ) !!}
									</span>
								</div>
							</div>
						{!! Form::close() !!}
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		@endif
		@if ($wallettype->withdraw == '1')
		<a class="btn blue-madison btn-block btn-outline margin-bottom-5" href="#withdraw" data-toggle="modal">Withdraw Money</a>
		<div class="modal fade" id="withdraw" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title"><i class="icon-action-redo"></i> Withdraw Money</h4>
					</div>
					<div class="modal-body">
						{!! Form::model($account, ['method' => 'POST', 'url' => 'account/dashboard/forms' , 'class'=>'frmsubmit']) !!}
							<div class="form-body">
								<div class="text-center margin-bottom-15 font-grey-silver">Declare an amount and currency in which you want withdraw funds from your Wallet account to your bank account.</div>
								<div class="form-group @if ($errors->has('amount')) has-error @endif">
									{!! Form::text('amount', null, ['class' => 'form-control text-center', 'style' => 'border:0px;font-size:64px;height:80px;', 'placeholder' => '0,00']) !!}
									@if ($errors->has('amount')) <p class="help-block">{{ $errors->first('amount') }}</p> @endif
								</div>
								<div class="form-group @if ($errors->has('currency')) has-error @endif">
									<div style="margin:auto;width:145px;">
									{!! Form::select('currency', $wallets_currency, null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
									@if ($errors->has('currency')) <p class="help-block">{{ $errors->first('currency') }}</p> @endif
									</div>
								</div>
								<h4>Bank Account Details</h4>
								<div class="form-group @if ($errors->has('account_holder_name')) has-error @endif">
									{!! Form::text('account_holder_name', null, ['class' => 'form-control', 'placeholder' => 'Account Holder Name']) !!}
									@if ($errors->has('account_holder_name')) <p class="help-block">{{ $errors->first('account_holder_name') }}</p> @endif
								</div>
								<div class="form-group @if ($errors->has('account_number')) has-error @endif">
									{!! Form::text('account_number', null, ['class' => 'form-control', 'placeholder' => 'Account Number']) !!}
									@if ($errors->has('account_number')) <p class="help-block">{{ $errors->first('account_number') }}</p> @endif
								</div>
								<div class="form-group @if ($errors->has('iban')) has-error @endif">
									{!! Form::text('iban', null, ['class' => 'form-control', 'placeholder' => 'IBAN']) !!}
									@if ($errors->has('iban')) <p class="help-block">{{ $errors->first('iban') }}</p> @endif
								</div>
								<div class="form-group @if ($errors->has('swift')) has-error @endif">
									{!! Form::text('swift', null, ['class' => 'form-control', 'placeholder' => 'SWIFT/BIC Code']) !!}
									@if ($errors->has('swift')) <p class="help-block">{{ $errors->first('swift') }}</p> @endif
								</div>
								<div class="form-group @if ($errors->has('bank_name')) has-error @endif">
									{!! Form::text('bank_name', null, ['class' => 'form-control', 'placeholder' => 'Bank Name']) !!}
									@if ($errors->has('bank_name')) <p class="help-block">{{ $errors->first('bank_name') }}</p> @endif
								</div>
								<div class="form-group @if ($errors->has('bank_address')) has-error @endif">
									{!! Form::text('bank_address', null, ['class' => 'form-control', 'placeholder' => 'Bank Address']) !!}
									@if ($errors->has('bank_address')) <p class="help-block">{{ $errors->first('bank_address') }}</p> @endif
								</div>
								<div class="form-group">
									<span class="input-group-btn">
										{!! Form::submit('Next', ['class' => 'btn btn-block green-jungle btn-lg', 'name' => 'withdraw'] ) !!}
									</span>
								</div>
							</div>
						{!! Form::close() !!}
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		@endif
		@endif
		@if (count($wallets) > 1)
		@if ($wallettype->transfer == '1')
		<a class="btn blue-madison btn-block btn-outline margin-bottom-5" href="#transfer" data-toggle="modal">Transfer between Currencies</a>
		<div class="modal fade" id="transfer" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title"><i class="icon-refresh "></i> Transfer between Currencies</h4>
					</div>
					<div class="modal-body">
						{!! Form::model($account, ['method' => 'POST', 'url' => 'account/dashboard/forms' , 'class'=>'frmsubmit']) !!}
							<div class="form-body">
								<div class="text-center margin-bottom-15 font-grey-silver">Currency conversion will be used for the transfer between currencies.</div>
								<div class="form-group">
									<div class="row">
										<div class="col-md-5">
											<h4 class="text-center">FROM</h4>
											<div class="@if ($errors->has('from_amount')) has-error @endif">
												{!! Form::text('from_amount', null, ['class' => 'form-control text-center','id' => 'from_amount', 'style' => 'border:0px;font-size:64px;height:80px;', 'placeholder' => '0,00']) !!}
												@if ($errors->has('from_amount')) <p class="help-block">{{ $errors->first('from_amount') }}</p> @endif
											</div>
											<div class="@if ($errors->has('currency')) has-error @endif">
												<div style="margin:auto;width:145px;">
													{!! Form::select('from_currency', $wallets_currency, null, ['class' => 'bs-select form-control','id' => 'from_currency'], ['data-live-search' => '']) !!}
													@if ($errors->has('from_currency')) <p class="help-block">{{ $errors->first('from_currency') }}</p> @endif
												</div>
											</div>
										</div>
										<div class="col-md-2">
											<div class="text-center font-blue-madison margin-top-20 margin-bottom-20" style="font-size:42px;"><i class="icon-control-play"></i></div>
										</div>
										<div class="col-md-5">
											<h4 class="text-center">TO</h4>
											<div class="@if ($errors->has('to_amount')) has-error @endif">
												{!! Form::text('to_amount', null, ['class' => 'form-control text-center','id' => 'to_amount', 'style' => 'border:0px;font-size:64px;height:80px;background-color:#fff;','readonly' => '','placeholder' => '0,00']) !!}
												@if ($errors->has('to_amount')) <p class="help-block">{{ $errors->first('to_amount') }}</p> @endif
											</div>
											<div class="@if ($errors->has('to_currency')) has-error @endif">
												<div style="margin:auto;width:145px;">
													{!! Form::select('to_currency', $wallets_currency, null, ['class' => 'bs-select form-control','id' => 'to_currency'], ['data-live-search' => '']) !!}
													@if ($errors->has('to_currency')) <p class="help-block">{{ $errors->first('to_currency') }}</p> @endif
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group @if ($errors->has('description')) has-error @endif">
									{!! Form::text('description', null, ['class' => 'form-control input-lg text-center', 'placeholder' => 'Add a Note']) !!}
									@if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
								</div>
								<div class="form-group">
									<span class="input-group-btn">
										{!! Form::submit('Next', ['class' => 'btn btn-block green-jungle btn-lg', 'name' => 'transfer'] ) !!}
									</span>
								</div>
							</div>
						{!! Form::close() !!}
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->
		@endif
		@endif
		@if ($account->compliance === 'Not Required' && count($wallets) > 0)
		@if ($wallettype->upgrade == '1')
		<a class="btn blue-madison btn-block btn-outline margin-bottom-5" href="#upgrade" data-toggle="modal">Upgrade Wallet</a>
		<div class="modal fade" id="upgrade" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title"><i class="fa fa-chevron-circle-up"></i> Upgrade Wallet</h4>
					</div>
					<div class="modal-body">
						{!! Form::model($account, ['method' => 'POST', 'url' => 'account/dashboard/forms' , 'class'=>'frmsubmit']) !!}
							<div class="form-body">
								<div class="text-center margin-bottom-15 font-grey-silver">To upgrade your Wallet Account you will be requested to provide personal documents. You will be able to upload your personal documents in settings under compliance tab.</div>
								<div class="form-group">
									<span class="input-group-btn">
										{!! Form::submit('Upgrade Wallet', ['class' => 'btn btn-block green-jungle btn-lg', 'name' => 'upgrade'] ) !!}
									</span>
								</div>
							</div>
						{!! Form::close() !!}
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<!-- /.modal -->
		@endif
		@endif
    </div>
    <div class="col-md-8">
		<div class="portlet">
			<div class="portlet-body">
				<div class="tabbable-custom tabbable-tabdrop">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#Payments" data-toggle="tab">Recent Payments</a>
						</li>
						<li>
							<a href="#Statements" data-toggle="tab">Recent Statements</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="Payments">
							@if (count($payments) == 0)
							<p class="text-center font-grey-silver margin-top-15">There are no payments to display</p>
							@else
							<div class="table-scrollable">
								<table class="table table-light table-hover table-bordered table-striped">
									<thead>
										<tr data-href="javascript:void(0);">
											<th style="width:1%">Date</th>
											<th>Description</th>
											<th style="width:1%">Method</br>Type</th>
											<th class="text-right" style="width:1%">Amount</th>
											<th style="width:1%">Status</th>
										</tr>
									</thead>
									<tbody>
										@foreach ($payments as $payment)
										<tr class='clickable-row' data-href='/account/payments/{!! $payment->payment_id !!}'>
											<td class="text-center sorting_1" tabindex="0"><span class="font-sm uppercase">{!! Carbon\Carbon::parse($payment->created_at)->format('M') !!}</span> </br> <span style="font-size:22px;font-weight:300;">{!! Carbon\Carbon::parse($payment->created_at)->format('d') !!}</span></td>
											<td>From {!! $payment->from !!} to {!! $payment->to !!}</br><span style="font-size:18px;font-weight:300;">{!! $payment->description !!}</span></td>
											<td>{!! $payment->payment_method !!}</br>{!! $payment->payment_type !!}</td>
											<td class="text-right">
												<span style="font-size:22px;font-weight:300;" class="@if ($payment->payment_type =='TransferOut') font-red-thunderbird @elseif ($payment->payment_type =='TransferIn')  font-green-jungle @elseif($payment->to === Auth::guard('account')->user()->email) font-green-jungle @elseif ($payment->from === Auth::guard('account')->user()->email) font-red-thunderbird @else @endif">
						@if ($payment->payment_type =='TransferOut') -@elseif ($payment->payment_type =='TransferIn')  +@elseif ($payment->to === Auth::guard('account')->user()->email) +@elseif ($payment->from === Auth::guard('account')->user()->email) -@else @endif{!! $payment->amount !!}
												</span> </br>
												<span class="font-sm">{!! $payment->currency !!}</span>
											</td>
											<td><span class="label label-sm @if ($payment->status === 'Approved') label-success @elseif ($payment->status === 'Pending') label-info @elseif ($payment->status === 'Requested') label-info @elseif ($payment->status === 'Refunded') label-purple @elseif ($payment->status === 'Canceled') label-warning @elseif ($payment->status === 'Chargeback') label-black @elseif ($payment->status === 'Declined') label-danger @else label-default @endif">{!! $payment->status !!}</span></td>
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
							<div class="scroller-footer">
								<div class="btn-arrow-link text-center">
									<a href="/account/payments">See All Payments</a>
								</div>
							</div>
							@endif
						</div>
						<div class="tab-pane" id="Statements">
							@if (count($wallet_statements) == 0)
							<p class="text-center font-grey-silver margin-top-15">There are no statements to display</p>
							@else
							<div class="table-scrollable">
								<table class="table table-light table-hover table-bordered table-striped">
									<thead>
										<tr data-href="javascript:void(0);">
											<th style="width:1%">Period</th>
											<th></th>
											<th class="text-right" style="width:1%">Credits</th>
											<th class="text-right" style="width:1%">Debits</th>
											<th class="text-right" style="width:1%">Ending Balance</th>
										</tr>
									</thead>
									<tbody>
										@foreach ($wallet_statements as $wallet_statement)
										<tr>
											<td><span class="font-sm uppercase">{!! Carbon\Carbon::createFromDate($wallet_statement->year, $wallet_statement->month)->format('F') !!}<span></br> <span style="font-size:22px;font-weight:300;">{!! $wallet_statement->year !!}</span></td>
											<td></td>
											<td class="text-right">
												<span style="font-size:22px;font-weight:300;" class="font-green-jungle">+{!! $wallet_statement->credits !!}</span></br>
												<span class="font-sm">{!! $wallet_statement->currency !!}</span>
											</td>
											<td class="text-right">
												<span style="font-size:22px;font-weight:300;" class="font-red-thunderbird">-{!! $wallet_statement->debits !!}</span></br>
												<span class="font-sm">{!! $wallet_statement->currency !!}</span>
											</td>
											<td class="text-right">
												<span style="font-size:22px;font-weight:300;">{!! $wallet_statement->ending_balance !!}</span></br>
												<span class="font-sm">{!! $wallet_statement->currency !!}</span>
											</td>
										</tr>
										@endforeach
									</tbody>
								</table>
							</div>
							<div class="scroller-footer">
								<div class="btn-arrow-link text-center">
									<a href="/account/statements">See All Statements</a>
								</div>
							</div>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
<script src="/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">

	$('#payment_method').change(function() {
		if ($(this).val()== 'NewCard')
		{
			$('#frmNewCard').show();
		}
		if ($(this).val()!= 'NewCard')
		{
			$('#frmNewCard').hide();
		}
	});
 $(document).ready(function(){

			$.ajaxSetup({
			   headers: { 'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content') }
			});

            $('#from_amount').keypress(function(e) {
                if (e.which == 13) {

                    var fromamount = $("#from_amount").val();
                    var fromcurrency = $("#from_currency").val();
                    var toCurrency = $("#to_currency").val();

                    CurrencyRequest(fromamount,fromcurrency,toCurrency)

                }
            });

            $("#from_amount").focusout(function() {

                var fromamount = $("#from_amount").val();
                var fromcurrency = $("#from_currency").val();
                var tocurrency = $("#to_currency").val();

                CurrencyRequest(fromamount,fromcurrency,tocurrency)

            });

            $("#from_currency").change(function(){

                var fromamount = $("#from_amount").val();
                var fromcurrency = $("#from_currency").val();
                var tocurrency = $("#to_currency").val();

                CurrencyRequest(fromamount,fromcurrency,tocurrency)

            });
			  $("#to_currency").change(function(){

                var fromamount = $("#from_amount").val();
                var fromcurrency = $("#from_currency").val();
                var tocurrency = $("#to_currency").val();

                CurrencyRequest(fromamount,fromcurrency,tocurrency)

            });

    });

    function CurrencyRequest(amount,from_curr,to_curr) {


        if(amount=='')
        {
            alert("Please Fill Amount To Convert");
            $("#to_amount").val('');
        }
        else if(from_curr == to_curr)
        {
            $("#to_amount").val(amount);
        }
        else
        {

            var dataString = 'fromcurrency='+ from_curr + '&fromamount='+ amount + '&tocurrency='+to_curr+'&_token = <?php echo csrf_token() ?>';


            // AJAX Code To Submit Form.
            $.ajax({
                type: "POST",
                url:'/account/crossrate',
                data: dataString,
                cache: false,
                success: function(result){
                    if(result != 'error')
                    {
						//alert(result);
                        $("#to_amount").val(result);
                    }

                }
            });
        }
    }


</script>

<!-- END PAGE CONTENT-->
@stop
