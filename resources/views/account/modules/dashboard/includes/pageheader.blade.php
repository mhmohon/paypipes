<!-- BEGIN PAGE TITLE -->
<div class="page-title">
    <h1>{!! $pagetitle !!}</h1>
</div>
<!-- END PAGE TITLE -->
<!-- BEGIN PAGE BREADCRUMBS -->
<div class="page-breadcrumbs" style="float:right; padding: 22px 0px 19px 0px; display: inline-block;">
    <ul class="page-breadcrumb breadcrumb" style="padding:0px;">
        <li><i class="icon-calendar"></i> {!! $today !!}</li>
    </ul>
</div>
<!-- END PAGE BREADCRUMBS -->