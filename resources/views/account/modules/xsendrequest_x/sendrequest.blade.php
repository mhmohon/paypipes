@extends('account/html')

@section('pageheader')
    @include('account/modules/sendrequest/includes/pageheader')
@stop
@section('content')
<!-- BEGIN PAGE CONTENT	-->
<div class="modal fade" id="send" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><i class="icon-action-redo"></i> Send Payment</h4>
            </div>
            <div class="modal-body">  
                {!! Form::model($account, ['method' => 'POST', 'action' => ['Account\Modules\Sendrequest\SendrequestController@forms', $account->account_id ]]) !!}
                    <div class="form-body">
                        <div class="text-center margin-bottom-15 font-grey-silver">Send payments to almost anyone with an email address and it will go to their +Wallet account. If they don’t have an account, we'll help them sign up for free.</div>
                        <div class="form-group @if ($errors->has('to_email')) has-error @endif">
                            {!! Form::text('to_email', null, ['class' => 'form-control input-lg text-center', 'placeholder' => 'Email']) !!}
                            @if ($errors->has('to_email')) <p class="help-block">{{ $errors->first('to_email') }}</p> @endif
                        </div>
                        <div class="form-group @if ($errors->has('amount')) has-error @endif">
                            {!! Form::text('amount', null, ['class' => 'form-control text-center', 'style' => 'border:0px;font-size:64px;height:80px;', 'placeholder' => '0,00']) !!}
                            @if ($errors->has('amount')) <p class="help-block">{{ $errors->first('amount') }}</p> @endif
                        </div>
                        <div class="form-group @if ($errors->has('currency')) has-error @endif">
                            <div style="margin:auto;width:145px;">
								{!! Form::select('currency', $currencies, null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
								@if ($errors->has('currency')) <p class="help-block">{{ $errors->first('currency') }}</p> @endif
                            </div>
                        </div>
                        <div class="form-group @if ($errors->has('description')) has-error @endif">
                            {!! Form::text('description', null, ['class' => 'form-control input-lg text-center', 'placeholder' => 'Add a Note']) !!}
                            @if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
                        </div>
                        <div class="form-group">
                            <span class="input-group-btn">
                                {!! Form::submit('Send Payment', ['class' => 'btn btn-block green-jungle btn-lg', 'name' => 'send'] ) !!}
                            </span>
                        </div>
                    </div>
                {!! Form::close() !!}  
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div class="modal fade" id="pay" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><i class="icon-basket-loaded"></i> Pay for goods or services</h4>
            </div>
            <div class="modal-body"> 
                {!! Form::model($account, ['method' => 'POST', 'action' => ['Account\Modules\Sendrequest\SendrequestController@forms', $account->id ]]) !!}
                    <div class="form-body">
                        <div class="text-center margin-bottom-15 font-grey-silver">Pay for goods or services to a Merchant with a +Wallet account. Enter Merchant email below and we'll we check the email for you.</div>
                        <div class="form-group @if ($errors->has('email')) has-error @endif">
                            {!! Form::text('email', null, ['class' => 'form-control text-center', 'placeholder' => 'Email']) !!}
                            @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
                        </div>
                        <div class="form-group @if ($errors->has('amount')) has-error @endif">
                            {!! Form::text('amount', null, ['class' => 'form-control text-center', 'style' => 'border:0px;font-size:64px;height:80px;', 'placeholder' => '0,00']) !!}
                            @if ($errors->has('amount')) <p class="help-block">{{ $errors->first('amount') }}</p> @endif
                        </div>
                        <div class="form-group @if ($errors->has('currency')) has-error @endif">
                            <div style="margin:auto;width:145px;">
                            {!! Form::select('currency', $currencies, null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
							@if ($errors->has('currency')) <p class="help-block">{{ $errors->first('currency') }}</p> @endif
                            </div>
                        </div>
                        <div class="form-group @if ($errors->has('description')) has-error @endif">
                            {!! Form::text('description', null, ['class' => 'form-control text-center', 'placeholder' => 'Add a Note']) !!}
                            @if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
                        </div>
                        <div class="form-group @if ($errors->has('shipping_address')) has-error @endif">
                            {!! Form::select('shipping_address', ['Your billing address, 123' => 'Your shipping address, 123','Add new shipping address' => 'Add new shipping address'], null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
                            @if ($errors->has('shipping_address')) <p class="help-block">{{ $errors->first('shipping_address') }}</p> @endif
                        </div>
                        <div class="portlet light bordered bg-inverse">
                            <div class="text-center margin-bottom-15">Your shipping address</div>
                            <div class="form-group @if ($errors->has('street')) has-error @endif">
                                {!! Form::text('street', null, ['class' => 'form-control', 'placeholder' => 'Street']) !!} 
                                @if ($errors->has('street')) <p class="help-block">{{ $errors->first('street') }}</p> @endif
                            </div>
                            <div class="form-group @if ($errors->has('city')) has-error @endif">
                                {!! Form::text('city', null, ['class' => 'form-control', 'placeholder' => 'City',]) !!}
                                @if ($errors->has('city')) <p class="help-block">{{ $errors->first('city') }}</p> @endif
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="@if ($errors->has('country')) has-error @endif">
                                            {!! Form::select('country', [
                                            'Select a Country' => 'Select a Country',
                                            'Afghanistan'	=>	'Afghanistan',
                                            'Åland Islands'	=>	'Åland Islands',
                                            ], null, ['class' => 'bs-select form-control', 'data-live-search' => '']) !!}
                                            @if ($errors->has('country')) <p class="help-block">{{ $errors->first('country') }}</p> @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="@if ($errors->has('postcode')) has-error @endif">
                                            {!! Form::text('postcode', null, ['class' => 'form-control', 'placeholder' => 'Post Code']) !!}
                                            @if ($errors->has('postcode')) <p class="help-block">{{ $errors->first('postcode') }}</p> @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <span class="input-group-btn">
                                {!! Form::submit('Pay for goods or services', ['class' => 'btn btn-block green-jungle btn-lg', 'name' => 'pay'] ) !!}
                            </span>
                        </div>
                    </div>
                {!! Form::close() !!} 
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div class="modal fade" id="request" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><i class="icon-action-undo"></i> Request Payment</h4>
            </div>
            <div class="modal-body"> 
                {!! Form::open(['method' => 'POST', 'action' => 'Account\Modules\Sendrequest\SendrequestController@forms']) !!}
                    <div class="form-body">
                        <div class="text-center margin-bottom-15 font-grey-silver">You can request payments from almost anyone with an email address, even if they don't have a +Wallet account.</div>
                        <div class="form-group @if ($errors->has('from_email')) has-error @endif">
                            {!! Form::text('from_email', null, ['class' => 'form-control input-lg text-center', 'placeholder' => 'Email']) !!}
                            @if ($errors->has('from_email')) <p class="help-block">{{ $errors->first('from_email') }}</p> @endif
                        </div>
                        <div class="form-group @if ($errors->has('amount')) has-error @endif">
                            {!! Form::text('amount', null, ['class' => 'form-control input-lg text-center', 'style' => 'border:0px;font-size:64px;height:80px;', 'placeholder' => '0,00']) !!}
                            @if ($errors->has('amount')) <p class="help-block">{{ $errors->first('amount') }}</p> @endif
                        </div>
                        <div class="form-group @if ($errors->has('currency')) has-error @endif">
                            <div style="margin:auto;width:145px;">
                            {!! Form::select('currency', $currencies, null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
							@if ($errors->has('currency')) <p class="help-block">{{ $errors->first('currency') }}</p> @endif
                            </div>
                        </div>
                        <div class="form-group @if ($errors->has('description')) has-error @endif">
                            {!! Form::text('description', null, ['class' => 'form-control input-lg text-center', 'placeholder' => 'Add a Note']) !!}
                            @if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
                        </div>
                        <div class="form-group">
                            <span class="input-group-btn">
                                {!! Form::submit('Request Payment', ['name' => 'request', 'class' => 'btn btn-block green-jungle btn-lg'] ) !!}
                            </span>
                        </div>
                    </div>
                {!! Form::close() !!} 
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
<div class="row">
    <div class="col-md-4">
        <div class="portlet light" style="padding:0;">
            <a class="btn blue-madison btn-block btn-outline" href="#send" data-toggle="modal">
                <h3 class="text-center">Send Payment</h3>
                <p class="text-center"  style="font-size:96px;"><i class="icon-action-redo"></i></p>
                <p class="text-center font-sm">Sender of the payment pays the fee</p>
            </a>
        </div>
    </div>
    <div class="col-md-4">
        <div class="portlet light" style="padding:0;">
            <a class="btn blue-madison btn-block btn-outline" href="#pay" data-toggle="modal">
                <h3 class="text-center">Pay for goods or services</h3>
                <p class="text-center"  style="font-size:96px;"><i class="icon-basket-loaded"></i></p>
                <p class="text-center font-sm">Receiver of the payment pays the fee</p>
            </a>
        </div>
    </div>
    <div class="col-md-4">
        <div class="portlet light" style="padding:0;">
            <a class="btn blue-madison btn-block btn-outline" href="#request" data-toggle="modal">
                <h3 class="text-center">Request Payment</h3>
                <p class="text-center"  style="font-size:96px;"><i class="icon-action-undo"></i></p>
                <p class="text-center font-sm">Receiver of the payment pays the fee</p>
            </a>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->
@stop