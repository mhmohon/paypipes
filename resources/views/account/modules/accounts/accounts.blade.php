@extends('account/html')

@section('pageheader')
    @include('account/modules/accounts/includes/pageheader')
@stop
@section('content')
<!-- BEGIN PAGE CONTENT	-->
<div class="modal fade" id="addaccount" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add an Account</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['url' => 'account/accounts', 'method' => 'POST']) !!}
                    <div class="form-body">
						<div class="@if ($errors->has('bank_id')) has-error @endif">
							<label>Banks</label>
							{!! Form::select('bank_id', $banks, null, ['id' => 'bank_id','class' => 'bs-select form-control', 'required' => 'required'], ['data-live-search' => '']) !!}
							@if ($errors->has('bank_id')) <p class="help-block">{{ $errors->first('bank_id') }}</p> @endif
						</div>

						<div class="form-group @if ($errors->has('account_number')) has-error @endif">
                            <label>Account Number</label>
							{!! Form::text('account_number', null, ['class' => 'form-control address', 'placeholder' => 'Account Number', 'required' => 'required']) !!}
                            @if ($errors->has('account_number')) <p class="help-block">{{ $errors->first('account_number') }}</p> @endif
                        </div>

                        <div class="form-group @if ($errors->has('iban')) has-error @endif">
                            <label>IBAN</label>
							{!! Form::text('iban', null, ['class' => 'form-control address', 'placeholder' => 'IBAN']) !!}
                            @if ($errors->has('iban')) <p class="help-block">{{ $errors->first('iban') }}</p> @endif
                        </div>

                        <div class="form-group">
                            <span class="input-group-btn">
                                {!! Form::submit('Add', ['class' => 'btn btn-block green-jungle btn-lg'] ) !!}
                            </span>
                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>

        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@if (count($accounts) == 0)
<div class="row">
	<div class="col-sm-6">
		{!! Form::open(['action' => 'Account\Modules\Accounts\AccountsController@index', 'method' => 'GET']) !!}
			<div class="input-group input-medium">
				{!! Form::text('search', Request::get('search'), ['class' => 'form-control input-medium', 'placeholder' => 'Search']) !!}
				<span class="input-group-btn">
					{!! Form::button('<i class="icon-magnifier"></i>', ['class' => 'btn green-jungle', 'type'=>'submit']) !!}
				</span>
			</div>
		{!! Form::close() !!}
	</div>
</div>
<p class="text-center font-grey-silver margin-top-15">There are no accounts to display</p>
@else
<div class="row">
	<div class="col-sm-6">
		{!! Form::open(['action' => 'Account\Modules\Accounts\AccountsController@index', 'method' => 'GET']) !!}
			<div class="input-group input-medium">
				{!! Form::text('search', Request::get('search'), ['class' => 'form-control input-medium', 'placeholder' => 'Search']) !!}
				<span class="input-group-btn">
					{!! Form::button('<i class="icon-magnifier"></i>', ['class' => 'btn green-jungle', 'type'=>'submit']) !!}
				</span>
			</div>
		{!! Form::close() !!}
	</div>
</div>
<div class="table-scrollable">
	<table class="table table-light table-bordered table-striped table-hover">
		<thead>
			<tr>
				<th style="width:1%">Date</th>
				<th>Description</th>
				<th style="width:1%">Method</br> Type</th>
				<th class="text-right" style="width:1%">Amount</th>
				<th style="width:1%">Status</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($accounts as $account)
			<tr class='clickable-row' data-href='/account/accounts/{!! $account->account_id !!}'>
				<td ><span class="font-sm uppercase">{!! Carbon\Carbon::parse($account->created_at)->format('M') !!}</span> </br> <span style="font-size:22px;font-weight:300;">{!! Carbon\Carbon::parse($account->created_at)->format('d') !!}</span></td>
				<td>From {!! $account->from !!} to {!! $account->to !!}</br><span style="font-size:18px;font-weight:300;">{!! $account->description !!}</span></td>
				<td>{!! $account->account_method !!}</br>{!! $account->account_type !!}</td>
				<td class="text-right">
					<span style="font-size:22px;font-weight:300;" class="@if ($account->account_type =='TransferOut') font-red-thunderbird @elseif ($account->account_type =='TransferIn')  font-green-jungle @elseif($account->to === Auth::guard('account')->user()->email) font-green-jungle @elseif ($account->from === Auth::guard('account')->user()->email) font-red-thunderbird @else @endif">
						@if ($account->account_type =='TransferOut') -@elseif ($account->account_type =='TransferIn')  +@elseif ($account->to === Auth::guard('account')->user()->email) +@elseif ($account->from === Auth::guard('account')->user()->email) -@else @endif{!! $account->amount !!}
					</span> </br>
					<span class="font-sm">{!! $account->currency !!}</span>
				</td>
				<td><span class="label label-sm @if ($account->status === 'Approved') label-success @elseif ($account->status === 'Pending') label-info @elseif ($account->status === 'Requested') label-info @elseif ($account->status === 'Refunded') label-purple @elseif ($account->status === 'Canceled') label-warning @elseif ($account->status === 'Chargeback') label-black @elseif ($account->status === 'Declined') label-danger @else label-default @endif">{!! $account->status !!}</span></td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
<div class="row">
	<div class="col-md-6 margin-top-10">
		Showing {!! $accounts->firstItem() !!} to {!! $accounts->lastItem() !!} of {!! $accounts->total() !!}
	</div>
	<div class="col-md-6 text-right">
		{!! $accounts->links() !!}
	</div>
</div>
@endif
<!-- END PAGE CONTENT-->
@stop
