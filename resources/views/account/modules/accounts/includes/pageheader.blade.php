<!-- BEGIN PAGE HEADER-->
<div class="page-bar">
	<!-- BEGIN PAGE TITLE -->
	<h3 class="page-title" style="float:left;margin:0px;">{!! $pagetitle !!}
	@if (Request::is('account/accounts')) <a class="btn green-jungle bold" data-toggle="modal" href="#addaccount" style="margin:10px;">Add <i class="fa fa-plus"></i></a>@endif
	</h3> 
	<!-- END PAGE TITLE -->
	<!-- BEGIN PAGE BREADCRUMBS -->
	<ul class="page-breadcrumbs page-breadcrumb breadcrumb" style="float:right; padding: 22px 0px 19px 0px; display: inline-block;">
	 <li><i class="icon-calendar"></i> {!! $today !!}</li>
    </ul>
	<!-- END PAGE BREADCRUMBS -->
</div>


