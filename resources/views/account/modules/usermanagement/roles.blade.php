@extends('account/html')

@section('pageheader')
    @include('account/modules/usermanagement/includes/pageheader')
@stop
@section('content')
<!-- BEGIN PAGE CONTENT	-->
@include('account/modules/usermanagement/includes/usermanagementtabs')
<!-- <div> -->
	<div class="tab-content">
		<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-hidden="true">
		    <div class="modal-dialog modal-lg">
		        <div class="modal-content">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		                <h4 class="modal-title">Add a Role</h4>
		            </div>
		            <div class="modal-body">                                                         
		                {!! Form::open() !!}
		                    <div class="form-body">
		                        <div class="form-group @if ($errors->has('name')) has-error @endif">
		                            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Role Name']) !!}
		                            @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
		                        </div>  
		                        <div class="form-group @if ($errors->has('email')) has-error @endif">
		                            {!! Form::text('description', null, ['class' => 'form-control', 'placeholder' => 'Description']) !!}
		                            @if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
		                        </div>
		                        <div class="form-group">
		                            <label class="control-label label-required">Permissions</label>
		                            <div class="col-sm-12">
		                                <table class="table">
					                        <tbody>
					                        
					                        </tbody>
					                    </table>
		                            </div>
		                        </div>           
		                                    
		                        <div class="form-group">
		                            <span class="input-group-btn">
		                                {!! Form::submit('Add', ['class' => 'btn btn-block green-jungle btn-lg'] ) !!}
		                            </span>
		                        </div>
		                    </div>
		                {!! Form::close() !!}                   
		            </div>
		        </div>
		        <!-- /.modal-content -->
		    </div>
		    <!-- /.modal-dialog -->
		</div>
		<h4 class="page-title">
			Roles
			<a class="btn green-jungle bold" data-toggle="modal" href="#add">Add <i class="fa fa-plus"></i></a>
		</h4>
		<div class="portlet-body">
			<div class="row">
				<div class="col-sm-6">
					{!! Form::open(['url' => 'admin/usermanagement/users', 'method' => 'GET']) !!}
						<div class="input-group input-medium">
							{!! Form::text('search', Request::get('search'), ['class' => 'form-control input-medium', 'placeholder' => 'Search']) !!}
							<span class="input-group-btn">
								{!! Form::button('<i class="icon-magnifier"></i>', ['class' => 'btn green-jungle', 'type'=>'submit']) !!}
							</span>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
			<div class="table-scrollable">
				<table class="table table-light table-bordered table-striped table-hover">
					<thead>
						<tr>
							<th style="width:1%">Role</th>
							<th>Description</th>
							<th style="width:1%">Action</th>
						</tr>
					</thead>
					<tbody>
						<tr>    
							<td>Admin</td>
							<td>All access</td>
							<td>
								<a href="/account/usermanagement/roles/1" class="btn blue btn-xs"><i class="fa fa-pencil"></i></a>
								<a href="#" class="btn dark btn-xs"><i class="fa fa-recycle"></i></a>
							</td>									
						</tr>
					</tbody>
				</table>
			</div>
			<div class="row">
				<div class="col-md-6 margin-top-10">
					Showing 
				</div>
				<div class="col-md-6 text-right">
					paging
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END PAGE CONTENT-->
@stop