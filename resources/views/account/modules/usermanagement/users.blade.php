@extends('account/html')

@section('pageheader')
    @include('account/modules/usermanagement/includes/pageheader')
@stop
@section('content')
<!-- BEGIN PAGE CONTENT	-->
@include('account/modules/usermanagement/includes/usermanagementtabs')
<!-- <div> -->
	<div class="tab-content">
		<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-hidden="true">
		    <div class="modal-dialog">
		        <div class="modal-content">
		            <div class="modal-header">
		                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
		                <h4 class="modal-title">Add a User</h4>
		            </div>
		            <div class="modal-body">
		                {!! Form::open() !!}
		                    <div class="form-body">
		                        <input name="_token" type="hidden" value="{{ csrf_token() }}">
		                        <div class="form-group @if ($errors->has('first_name')) has-error @endif">
		                            <div class="controls">
		                                {!! Form::text('first_name', null, array('class' => 'form-control form-control-solid placeholder-no-fix', 'placeholder' => 'First Name')) !!}
		                                @if ($errors->has('first_name')) <p class="help-block">{{ $errors->first('first_name') }}</p> @endif
		                            </div>
		                        </div>
								<div class="form-group @if ($errors->has('last_name')) has-error @endif">
		                            <div class="controls">
		                                {!! Form::text('last_name', null, array('class' => 'form-control form-control-solid placeholder-no-fix', 'placeholder' => 'Last Name')) !!}
		                                @if ($errors->has('last_name')) <p class="help-block">{{ $errors->first('last_name') }}</p> @endif
		                            </div>
		                        </div>
		                        <div class="form-group @if ($errors->has('email')) has-error @endif">
		                            <div class="controls">
		                                {!! Form::text('email', null, array('class' => 'form-control form-control-solid placeholder-no-fix', 'placeholder' => 'Email')) !!}
		                                @if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
		                            </div>
		                        </div>
		                        <div class="form-group @if ($errors->has('email')) has-error @endif">
		                            <div class="controls">
		                                {!! Form::select('role', $roles, null, ['id' => 'role','class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
										@if ($errors->has('role')) <p class="help-block">{{ $errors->first('role') }}</p> @endif
		                            </div>
		                        </div>
		                        <div class="form-group">
		                            <span class="input-group-btn">
		                                {!! Form::submit('Add', ['class' => 'btn btn-block green-jungle btn-lg'] ) !!}
		                            </span>
		                        </div>
		                    </div>
		                {!! Form::close() !!}
		            </div>
		        </div>
		        <!-- /.modal-content -->
		    </div>
		    <!-- /.modal-dialog -->
		</div>
		<h4 class="page-title">
			Users
			<a class="btn green-jungle bold" data-toggle="modal" href="#add">Add <i class="fa fa-plus"></i></a>
		</h4>
		<div class="portlet-body">
			<div class="row">
				<div class="col-sm-6">
					{!! Form::open(['url' => 'account/usermanagement/users', 'method' => 'GET']) !!}
						<div class="input-group input-medium">
							{!! Form::text('search', Request::get('search'), ['class' => 'form-control input-medium', 'placeholder' => 'Search']) !!}
							<span class="input-group-btn">
								{!! Form::button('<i class="icon-magnifier"></i>', ['class' => 'btn green-jungle', 'type'=>'submit']) !!}
							</span>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
			<div class="table-scrollable">
				<table class="table table-light table-bordered table-striped table-hover">
					<thead>
						<tr>
							<th style="width:1%">Name</th>
							<th>Email</th>
							<th style="width:1%">Primary</th>
							<th style="width:1%">Role</th>
							<th style="width:1%">Status</th>
							<th style="width:1%">Action</th>
						</tr>
					</thead>
					<tbody>
						<?php $i = 0 ?>
						@foreach ($accounts as $account)
						<?php $i++ ?>
						<tr>
							<td>{!! $account->first_name. ' '.$account->last_name !!}</td>
							<td>{!! $account->email !!}</td>
							<td>{!! $account->is_primary !!}</td>
							<td>Admin</td>
							<td><span class="label label-sm @if ($account->status === 'Active') label-success @elseif ($account->status === 'Inactive') label-default @elseif ($account->status === 'Locked') label-danger @elseif ($account->status === 'TempLock') label-warning @else label-default @endif">{!! $account->status !!}</span></td>
							<td>
								<a href="/account/usermanagement/users/{!! $account->id !!}" class="btn blue btn-xs"><i class="fa fa-pencil"></i></a>
								@if ($account->is_primary == '1')
									<a href="javascript:;" class="btn default btn-xs"><i class="fa fa-unlock-alt"></i></a>
									<a href="javascript:;" class="btn default btn-xs"><i class="fa fa-recycle"></i></a>
								@else
									@if ($account->status === 'Active' || $account->status === 'Inactive')
										{!! Form::open(['method' => 'PATCH', 'action' => ['Account\Modules\UserManagement\UsersController@lockunlock', $account->id], 'style' => 'display:inline-block;']) !!}
											{!! Form::hidden('status', 'Locked') !!}
											{!! Form::button('<i class="fa fa-lock"></i>', ['class' => 'btn red btn-xs', 'type'=>'submit', 'data-toggle' => 'confirmation', 'data-popout' => 'true', 'data-singleton' => 'true', 'data-original-title' => 'Lock! Are you sure?', 'title' => '']) !!}
										{!! Form::close() !!}
									@elseif ($account->status === 'Locked')
										{!! Form::open(['method' => 'PATCH', 'action' => ['Account\Modules\UserManagement\UsersController@lockunlock', $account->id], 'style' => 'display:inline-block;']) !!}
											{!! Form::hidden('status', 'Active') !!}
											{!! Form::button('<i class="fa fa-unlock-alt"></i>', ['class' => 'btn green-jungle btn-xs', 'type'=>'submit', 'data-toggle' => 'confirmation', 'data-popout' => 'true', 'data-singleton' => 'true', 'data-original-title' => 'Unlock! Are you sure?', 'title' => '']) !!}
										{!! Form::close() !!}
									@elseif ($account->status === 'TempLock')
										<a href="javascript:;" class="btn default btn-xs"><i class="fa fa-unlock-alt"></i></a>
									@endif
									{!! Form::open(['method' => 'DELETE', 'action' => ['Account\Modules\UserManagement\UsersController@destroy', $account->id], 'style' => 'display:inline-block;']) !!}
											{!! Form::button('<i class="fa fa-recycle"></i>', ['class' => 'btn dark btn-xs', 'type'=>'submit', 'data-toggle' => 'confirmation', 'data-popout' => 'true', 'data-singleton' => 'true', 'data-original-title' => 'Delete! Are you sure?', 'title' => '']) !!}
									{!! Form::close() !!}
								@endif
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<div class="row">
				<div class="col-md-6 margin-top-10">
					Showing {!! $accounts->firstItem() !!} to {!! $accounts->lastItem() !!} of {!! $accounts->total() !!}
				</div>
				<div class="col-md-6 text-right">
					{{ $accounts->appends(Request::except('page'))->links() }}
				</div>
			</div>
		</div>
	</div>
</div>
<!-- END PAGE CONTENT-->
@stop
