@extends('account/html')

@section('pageheader')
    @include('account/modules/activities/includes/pageheader')
@stop
@section('content')
<!-- BEGIN PAGE CONTENT	-->
@if (count($activities) == 0)
<div class="row">
	<div class="col-sm-6">
		{!! Form::open(['action' => 'Account\Modules\Activities\ActivitiesController@index', 'method' => 'GET']) !!}
			<div class="input-group input-medium">
				{!! Form::text('search', Request::get('search'), ['class' => 'form-control input-medium', 'placeholder' => 'Search']) !!}
				<span class="input-group-btn">
					{!! Form::button('<i class="icon-magnifier"></i>', ['class' => 'btn green-jungle', 'type'=>'submit']) !!}
				</span>
			</div>
		{!! Form::close() !!}
	</div>
</div>
<p class="text-center font-grey-silver margin-top-15">There are no activities to display</p>
@else
<div class="row">
	<div class="col-sm-6">
		{!! Form::open(['action' => 'Account\Modules\Activities\ActivitiesController@index', 'method' => 'GET']) !!}
			<div class="input-group input-medium">
				{!! Form::text('search', Request::get('search'), ['class' => 'form-control input-medium', 'placeholder' => 'Search']) !!}
				<span class="input-group-btn">
					{!! Form::button('<i class="icon-magnifier"></i>', ['class' => 'btn green-jungle', 'type'=>'submit']) !!}
				</span>
			</div>
		{!! Form::close() !!}
	</div>
</div>
<div class="table-scrollable">
	<table class="table table-light table-bordered table-striped table-hover">
		<thead>
			<tr>
				<th style="width:1%"></th>
				<th>Activity</th>
				<th style="width:1%">When</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($activities as $activity)
			<tr>
				<td class="text-center">
					@if ($activity->module === 'Customer') <button class="btn btn-xs green-jungle"><i class="icon-users"></i></button>
					@elseif ($activity->module === 'Merchant') <button class="btn btn-xs blue"><i class="icon-screen-desktop"></i></button>
					@elseif ($activity->module === 'Partner') <button class="btn btn-xs green"><i class="icon-user-following"></i></button>
					@elseif ($activity->module === 'Wallet') <button class="btn btn-xs purple"><i class="icon-wallet"></i></button>
					@elseif ($activity->module === 'Payment') <button class="btn btn-xs blue"><i class="fa fa-credit-card"></i></button>
					@elseif ($activity->module === 'RiskManagement') <button class="btn btn-xs red-thunderbird"><i class="icon-shield"></i></button>
					@elseif ($activity->module === 'Help') <button class="btn btn-xs yellow-lemon"><i class="icon-question"></i></button>
					@elseif ($activity->module === 'Settings') <button class="btn btn-xs dark"><i class="fa fa-cogs"></i></button>
					@else
					@endif
				</td>
				<td>{!! $activity->activity !!}</td>
				<td style="width:1%">{!! Carbon\Carbon::parse($activity->created_at)->diffForHumans() !!}</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
<div class="row">
	<div class="col-md-6 margin-top-10">
		Showing {!! $activities->firstItem() !!} to {!! $activities->lastItem() !!} of {!! $activities->total() !!}
	</div>
	<div class="col-md-6 text-right">
		{!! $activities->links() !!}
	</div>
</div>
@endif
<!-- END PAGE CONTENT-->
@stop
