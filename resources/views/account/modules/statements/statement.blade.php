@extends('account/html')

@section('pageheader')
    @include('account/modules/statements/includes/pageheader')
@stop
@section('content')
<!-- BEGIN PAGE CONTENT	-->
@if (count($payments) == 0)
<p class="text-center font-grey-silver margin-top-15">There are no payments to display</p>
@else
<div class="table-scrollable">
	<table class="table table-light table-bordered table-striped table-hover">
		<thead>
			<tr data-href="javascript:void(0);">
				<th style="width:1%">Period</th>
				<th style="width:1%">Currency</th>
				<th>Account Statement</th>
				<th class="text-right" style="width:1%">Credits</th>
				<th class="text-right" style="width:1%">Debits</th>
				<th class="text-right" style="width:1%">Ending Balance</th>
			</tr>
		</thead>
		<tbody>
			<tr class="clickable-row" data-href="/account/statements/{!! $statement->account_statement_id !!}">
				<td><span class="font-sm uppercase">{!! Carbon\Carbon::createFromDate($statement->year, $statement->month)->format('F') !!}<span></br> <span style="font-size:22px;font-weight:300;">{!! $statement->year !!}</span></td>
				<td><span style="font-size:18px;font-weight:300;">{!! $statement->currency !!}</span></td>
				<td><span style="font-size:18px;font-weight:300;">{!! $account->company_name !!}</span></td>
				<td class="text-right">
					<span style="font-size:22px;font-weight:300;" class="font-green-jungle">+{!! $statement->credits !!}</span></br>
					<span class="font-sm">{!! $statement->currency !!}</span>
				</td>
				<td class="text-right">
					<span style="font-size:22px;font-weight:300;" class="font-red-thunderbird">-{!! $statement->debits !!}</span></br>
					<span class="font-sm">{!! $statement->currency !!}</span>
				</td>
				<td class="text-right">
					<span style="font-size:22px;font-weight:300;">{!! $statement->ending_balance !!}</span></br>
					<span class="font-sm">{!! $statement->currency !!}</span>
				</td>
			</tr>
		</tbody>
	</table>
	<table class="table table-light table-bordered table-striped table-hover order-column">
		<thead>
			<tr>
				<th style="width:1%">Date</th>
				<th style="width:1%">Payment ID</th>
				<th>Description</th>
				<th style="width:1%">Method</br> Type</th>
				<th class="text-right" style="width:1%">Settlement</br> Amount</th>
				<th style="width:1%">Status</th>
			</tr>
		</thead>
		<tbody>
		@foreach ($payments as $payment)
			<tr>
				<td class="text-center sorting_1" tabindex="0"><span class="font-sm uppercase">{!! Carbon\Carbon::parse($payment->created_at)->format('M') !!}</span> </br> <span style="font-size:22px;font-weight:300;">{!! Carbon\Carbon::parse($payment->created_at)->format('d') !!}</span></td>
				<td>{!! $payment->payment_id !!}</td>
				<td>From {!! $payment->from !!} to {!! $payment->to !!}</br><span style="font-size:18px;font-weight:300;">{!! $payment->description !!}</span></td>
				<td>{!! $payment->payment_method !!}</br>{!! $payment->payment_type !!}</td>
				<td class="text-right">
					<span style="font-size:22px;font-weight:300;" class="@if ($payment->to == $account->email) font-green-jungle @elseif ($payment->from == $account->email) font-red-thunderbird @else @endif">
						@if ($payment->to == $account->email) +@elseif ($payment->from == $account->email) -@else @endif{!! $payment->total_amount !!}
					</span> </br>
					<span class="font-sm">{!! $payment->currency !!}</span>
				</td>
				<td><span class="label label-sm @if ($payment->status === 'Approved') label-success @elseif ($payment->status === 'Pending') label-info @elseif ($payment->status === 'Requested') label-info @elseif ($payment->status === 'Refunded') label-purple @elseif ($payment->status === 'Canceled') label-warning @elseif ($payment->status === 'Chargeback') label-black @elseif ($payment->status === 'Declined') label-danger @else label-default @endif">{!! $payment->status !!}</span></td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
@endif
<!-- END PAGE CONTENT-->
@stop
