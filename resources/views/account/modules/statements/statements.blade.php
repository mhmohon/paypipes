@extends('account/html')

@section('pageheader')
    @include('account/modules/statements/includes/pageheader')
@stop
@section('content')
<!-- BEGIN PAGE CONTENT	-->
@if (count($statements) == 0)
<div class="row">
	<div class="col-sm-6">
		{!! Form::model($account, [ 'method' => 'GET', 'action' => ['Account\Modules\Statements\StatementsController@index', $account->account_id]]) !!}
			<div class="input-group input-medium">
				{!! Form::text('search', Request::get('search'), ['class' => 'form-control input-medium', 'placeholder' => 'Search']) !!}
				<span class="input-group-btn">
					{!! Form::button('<i class="icon-magnifier"></i>', ['class' => 'btn green-jungle', 'type'=>'submit']) !!}
				</span>
			</div>
		{!! Form::close() !!}
	</div>
</div>
<p class="text-center font-grey-silver margin-top-15">There are no statements to display</p>
@else
<div class="row">
	<div class="col-sm-6">
		{!! Form::model($account, [ 'method' => 'GET', 'action' => ['Account\Modules\Statements\StatementsController@index', $account->account_id]]) !!}
			<div class="input-group input-medium">
				{!! Form::text('search', Request::get('search'), ['class' => 'form-control input-medium', 'placeholder' => 'Search']) !!}
				<span class="input-group-btn">
					{!! Form::button('<i class="icon-magnifier"></i>', ['class' => 'btn green-jungle', 'type'=>'submit']) !!}
				</span>
			</div>
		{!! Form::close() !!}
	</div>
	<div class="col-sm-6 text-right" style="margin-top:5px">
		<a href="#" class="btn btn-sm dark"><i class="fa fa-print"></i> Print</a>
		<a href="#" class="btn btn-sm red"><i class="fa fa-file-pdf-o"></i> PDF</a>
		<a href="#" class="btn btn-sm green-jungle"><i class="fa fa-file-excel-o"></i> XML</a>
	</div>
</div>
<div class="table-scrollable">
	<table class="table table-light table-bordered table-striped table-hover">
		<thead>
			<tr data-href="javascript:void(0);">
				<th style="width:1%">Period</th>
				<th></th>
				<th class="text-right" style="width:1%">Credits</th>
				<th class="text-right" style="width:1%">Debits</th>
				<th class="text-right" style="width:1%">Ending Balance</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($statements as $statement)
			<tr class="clickable-row" data-href="/account/statements/{!! $statement->account_statement_id !!}">
				<td><span class="font-sm uppercase">{!! Carbon\Carbon::createFromDate($statement->year, $statement->month)->format('F') !!}<span></br> <span style="font-size:22px;font-weight:300;">{!! $statement->year !!}</span></td>
				<td></td>
				<td class="text-right">
					<span style="font-size:22px;font-weight:300;" class="font-green-jungle">+{!! $statement->credits !!}</span></br>
					<span class="font-sm">{!! $statement->currency !!}</span>
				</td>
				<td class="text-right">
					<span style="font-size:22px;font-weight:300;" class="font-red-thunderbird">-{!! $statement->debits !!}</span></br>
					<span class="font-sm">{!! $statement->currency !!}</span>
				</td>
				<td class="text-right">
					<span style="font-size:22px;font-weight:300;">{!! $statement->ending_balance !!}</span></br>
					<span class="font-sm">{!! $statement->currency !!}</span>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
<div class="row">
	<div class="col-md-6 margin-top-10">
		Showing {!! $statements->firstItem() !!} to {!! $statements->lastItem() !!} of {!! $statements->total() !!}
	</div>
	<div class="col-md-6 text-right">
		{!! $statements->links() !!}
	</div>
</div>
@endif
<!-- END PAGE CONTENT-->
@stop
