<!-- BEGIN PAGE TITLE -->
<div class="page-title">
    <h1>{!! $pagetitle !!}</h1>
</div>
<!-- END PAGE TITLE -->
<!-- BEGIN PAGE BREADCRUMBS -->
<div class="page-breadcrumbs" style="float:right; padding: 22px 0px 19px 0px; display: inline-block;">
    <ul class="page-breadcrumb breadcrumb" style="padding:0px;">
        <li>
            <a href="/account/statements">{!! $breadcrumb_level1 or '' !!}</a>
            @if (Request::is('account/statements/*')) <i class="fa fa-chevron-right"></i>
            @endif
        </li>
        <li>
            <span>{!! $breadcrumb_level2 or '' !!}</span>
        </li>
    </ul>
</div>
<!-- END PAGE BREADCRUMBS -->
