@extends('account/html')

@section('pageheader')
    @include('account/modules/settings/includes/pageheader')
@stop
@section('content')
<!-- BEGIN PAGE CONTENT	-->
@include('account/modules/settings/includes/settingstabs')
	<div class="tab-content">
		<div class="row">
			<div class="col-md-6">
				<h4>Payments</h4>
				<div class="margin-bottom-10"><cite class="font-grey-silver">We'll let you know when you</cite></div>
				<table class="table table-light table-hover">
					<tbody>
						<tr>
							<td>Add money</td>
							<td class="text-right">
							<button class="btn btn-xs"><span class="glyphicon glyphicon-ok font-grey-silver font-lg"></span></button>
							<!--
							{!! Form::open(['method' => 'PATCH', 'action' => ['Account\Modules\Settings\NotificationsController@notifications', $notification->notification_id], 'style' => 'display:inline-block;']) !!}
							@if($notification->add_money == 1)
								{!! Form::hidden('add_money', 0) !!}
								{!! Form::button('<span class="glyphicon glyphicon-ok font-green-jungle font-lg"></span>', ['type'=>'submit', 'class'=>'btn btn-xs', 'data-singleton' => 'true', 'title' => '']) !!}
							@else
								{!! Form::hidden('add_money', 1) !!}
								{!! Form::button('<span class="glyphicon glyphicon-remove font-dark font-lg"></span>', ['type'=>'submit', 'class'=>'btn btn-xs', 'data-singleton' => 'true', 'title' => '']) !!}
							@endif
							{!! Form::close() !!}
							-->
							</td>
						</tr>
						<tr>
							<td>Withdraw money</td>
							<td class="text-right">
							<button class="btn btn-xs"><span class="glyphicon glyphicon-ok font-grey-silver font-lg"></span></button>
							<!--
							{!! Form::open(['method' => 'PATCH', 'action' => ['Account\Modules\Settings\NotificationsController@notifications', $notification->notification_id], 'style' => 'display:inline-block;']) !!}
							@if($notification->withdraw_money == 1)
								{!! Form::hidden('withdraw_money', 0) !!}
								{!! Form::button('<span class="glyphicon glyphicon-ok font-green-jungle font-lg"></span>', ['type'=>'submit', 'class'=>'btn btn-xs', 'data-singleton' => 'true', 'title' => '']) !!}
							@else
								{!! Form::hidden('withdraw_money', 1) !!}
								{!! Form::button('<span class="glyphicon glyphicon-remove font-dark font-lg"></span>', ['type'=>'submit', 'class'=>'btn btn-xs', 'data-singleton' => 'true', 'title' => '']) !!}
							@endif
							{!! Form::close() !!}	
							--> 
							</td>
						</tr>
						<tr>
							<td>Transfer between currencies</td>
							<td class="text-right">
							{!! Form::open(['method' => 'PATCH', 'action' => ['Account\Modules\Settings\NotificationsController@notifications', $notification->notification_id], 'style' => 'display:inline-block;']) !!}
							@if($notification->transfer_money == 1)
								{!! Form::hidden('transfer_money', 0) !!}
								{!! Form::button('<span class="glyphicon glyphicon-ok font-green-jungle font-lg"></span>', ['type'=>'submit', 'class'=>'btn btn-xs', 'data-singleton' => 'true', 'title' => '']) !!}
							@else
								{!! Form::hidden('transfer_money', 1) !!}
								{!! Form::button('<span class="glyphicon glyphicon-remove font-dark font-lg"></span>', ['type'=>'submit', 'class'=>'btn btn-xs', 'data-singleton' => 'true', 'title' => '']) !!}
							@endif
							{!! Form::close() !!}	
							</td>
						</tr>
						<tr>
							<td>Send a payment</td>
							<td class="text-right">
							<button class="btn btn-xs"><span class="glyphicon glyphicon-ok font-grey-silver font-lg"></span></button>
							<!--
							{!! Form::open(['method' => 'PATCH', 'action' => ['Account\Modules\Settings\NotificationsController@notifications', $notification->notification_id], 'style' => 'display:inline-block;']) !!}
							@if($notification->send_money == 1)
								{!! Form::hidden('send_money', 0) !!}
								{!! Form::button('<span class="glyphicon glyphicon-ok font-green-jungle font-lg"></span>', ['type'=>'submit', 'class'=>'btn btn-xs', 'data-singleton' => 'true', 'title' => '']) !!}
							@else
								{!! Form::hidden('send_money', 1) !!}
								{!! Form::button('<span class="glyphicon glyphicon-remove font-dark font-lg"></span>', ['type'=>'submit', 'class'=>'btn btn-xs', 'data-singleton' => 'true', 'title' => '']) !!}
							@endif
							{!! Form::close() !!}
							-->							
							</td>
						</tr>
						<tr>
							<td>Pay for goods or services</td>
							<td class="text-right">
							<button class="btn btn-xs"><span class="glyphicon glyphicon-ok font-grey-silver font-lg"></span></button>
							<!--
							{!! Form::open(['method' => 'PATCH', 'action' => ['Account\Modules\Settings\NotificationsController@notifications', $notification->notification_id], 'style' => 'display:inline-block;']) !!}
							@if($notification->pay_money == 1)
								{!! Form::hidden('pay_money', 0) !!}
								{!! Form::button('<span class="glyphicon glyphicon-ok font-green-jungle font-lg"></span>', ['type'=>'submit', 'class'=>'btn btn-xs', 'data-singleton' => 'true', 'title' => '']) !!}
							@else
								{!! Form::hidden('pay_money', 1) !!}
								{!! Form::button('<span class="glyphicon glyphicon-remove font-dark font-lg"></span>', ['type'=>'submit', 'class'=>'btn btn-xs', 'data-singleton' => 'true', 'title' => '']) !!}
							@endif
							{!! Form::close() !!}	
							-->
							</td>
						</tr>
						<tr>
							<td>Request a payment</td>
							<td class="text-right">
							{!! Form::open(['method' => 'PATCH', 'action' => ['Account\Modules\Settings\NotificationsController@notifications', $notification->notification_id], 'style' => 'display:inline-block;']) !!}
							@if($notification->request_money == 1)
								{!! Form::hidden('request_money', 0) !!}
								{!! Form::button('<span class="glyphicon glyphicon-ok font-green-jungle font-lg"></span>', ['type'=>'submit', 'class'=>'btn btn-xs', 'data-singleton' => 'true', 'title' => '']) !!}
							@else
								{!! Form::hidden('request_money', 1) !!}
								{!! Form::button('<span class="glyphicon glyphicon-remove font-dark font-lg"></span>', ['type'=>'submit', 'class'=>'btn btn-xs', 'data-singleton' => 'true', 'title' => '']) !!}
							@endif
							{!! Form::close() !!}	
							</td>
						</tr>
						<tr>
							<td>Receive a payment</td>
							<td class="text-right">
							<button class="btn btn-xs"><span class="glyphicon glyphicon-ok font-grey-silver font-lg"></span></button>
							<!--
							{!! Form::open(['method' => 'PATCH', 'action' => ['Account\Modules\Settings\NotificationsController@notifications', $notification->notification_id], 'style' => 'display:inline-block;']) !!}
							@if($notification->receive_money == 1)
								{!! Form::hidden('receive_money', 0) !!}
								{!! Form::button('<span class="glyphicon glyphicon-ok font-green-jungle font-lg"></span>', ['type'=>'submit', 'class'=>'btn btn-xs', 'data-singleton' => 'true', 'title' => '']) !!}
							@else
								{!! Form::hidden('receive_money', 1) !!}
								{!! Form::button('<span class="glyphicon glyphicon-remove font-dark font-lg"></span>', ['type'=>'submit', 'class'=>'btn btn-xs', 'data-singleton' => 'true', 'title' => '']) !!}
							@endif
							{!! Form::close() !!}
							-->
							</td>
						</tr>
						<tr>
							<td>Approve/Decline a payment</td>
							<td class="text-right">
							<button class="btn btn-xs"><span class="glyphicon glyphicon-ok font-grey-silver font-lg"></span></button>
							<!--
							{!! Form::open(['method' => 'PATCH', 'action' => ['Account\Modules\Settings\NotificationsController@notifications', $notification->notification_id], 'style' => 'display:inline-block;']) !!}
							@if($notification->approve_decline == 1)
								{!! Form::hidden('approve_decline', 0) !!}
								{!! Form::button('<span class="glyphicon glyphicon-ok font-green-jungle font-lg"></span>', ['type'=>'submit', 'class'=>'btn btn-xs', 'data-singleton' => 'true', 'title' => '']) !!}
							@else
								{!! Form::hidden('approve_decline', 1) !!}
								{!! Form::button('<span class="glyphicon glyphicon-remove font-dark font-lg"></span>', ['type'=>'submit', 'class'=>'btn btn-xs', 'data-singleton' => 'true', 'title' => '']) !!}
							@endif
							{!! Form::close() !!}
							-->
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-md-6">
				<h4>General</h4>
				<div class="margin-bottom-10"><cite class="font-grey-silver">We'll let you know when you</cite></div>
				<table class="table table-light table-hover">
					<tbody>
						<tr>
							<td>Add currency</td>
							<td class="text-right">
							{!! Form::open(['method' => 'PATCH', 'action' => ['Account\Modules\Settings\NotificationsController@notifications', $notification->notification_id], 'style' => 'display:inline-block;']) !!}
							@if($notification->add_currency == 1)
								{!! Form::hidden('add_currency', 0) !!}
								{!! Form::button('<span class="glyphicon glyphicon-ok font-green-jungle font-lg"></span>', ['type'=>'submit', 'class'=>'btn btn-xs', 'data-singleton' => 'true', 'title' => '']) !!}
							@else
								{!! Form::hidden('add_currency', 1) !!}
								{!! Form::button('<span class="glyphicon glyphicon-remove font-dark font-lg"></span>', ['type'=>'submit', 'class'=>'btn btn-xs', 'data-singleton' => 'true', 'title' => '']) !!}
							@endif
							{!! Form::close() !!}	
							</td>
						<tr>
							<td>Add/Edit a card</td>
							<td class="text-right">
							{!! Form::open(['method' => 'PATCH', 'action' => ['Account\Modules\Settings\NotificationsController@notifications', $notification->notification_id], 'style' => 'display:inline-block;']) !!}
							@if($notification->add_edit_card == 1)
								{!! Form::hidden('add_edit_card', 0) !!}
								{!! Form::button('<span class="glyphicon glyphicon-ok font-green-jungle font-lg"></span>', ['type'=>'submit', 'class'=>'btn btn-xs', 'data-singleton' => 'true', 'title' => '']) !!}
							@else
								{!! Form::hidden('add_edit_card', 1) !!}
								{!! Form::button('<span class="glyphicon glyphicon-remove font-dark font-lg"></span>', ['type'=>'submit', 'class'=>'btn btn-xs', 'data-singleton' => 'true', 'title' => '']) !!}
							@endif
							{!! Form::close() !!}	
							</td>
						</tr>
						<tr>
							<td>Add/Edit account details</td>
							<td class="text-right">
							{!! Form::open(['method' => 'PATCH', 'action' => ['Account\Modules\Settings\NotificationsController@notifications', $notification->notification_id], 'style' => 'display:inline-block;']) !!}
							@if($notification->add_edit_settings == 1)
								{!! Form::hidden('add_edit_settings', 0) !!}
								{!! Form::button('<span class="glyphicon glyphicon-ok font-green-jungle font-lg"></span>', ['type'=>'submit', 'class'=>'btn btn-xs', 'data-singleton' => 'true', 'title' => '']) !!}
							@else
								{!! Form::hidden('add_edit_settings', 1) !!}
								{!! Form::button('<span class="glyphicon glyphicon-remove font-dark font-lg"></span>', ['type'=>'submit', 'class'=>'btn btn-xs', 'data-singleton' => 'true', 'title' => '']) !!}
							@endif
							{!! Form::close() !!}	
							</td>
						</tr>
						<!--
						<tr>
							<td>Receive a message</td>
							<td class="text-right"><i class="icon-envelope-open font-lg font-blue-madison"></i></td>
						</tr>
						-->
					</tbody>
				</table>
				<h4>News and Updates</h4>
				<table class="table table-light table-hover">
					<tbody>
						<tr>
							<td>News and promotions</td>
							<td class="text-right">
							{!! Form::open(['method' => 'PATCH', 'action' => ['Account\Modules\Settings\NotificationsController@notifications', $notification->notification_id], 'style' => 'display:inline-block;']) !!}
							@if($notification->receive_offer == 1)
								{!! Form::hidden('receive_offer', 0) !!}
								{!! Form::button('<span class="glyphicon glyphicon-ok font-green-jungle font-lg"></span>', ['type'=>'submit', 'class'=>'btn btn-xs', 'data-singleton' => 'true', 'title' => '']) !!}
							@else
								{!! Form::hidden('receive_offer', 1) !!}
								{!! Form::button('<span class="glyphicon glyphicon-remove font-dark font-lg"></span>', ['type'=>'submit', 'class'=>'btn btn-xs', 'data-singleton' => 'true', 'title' => '']) !!}
							@endif
							{!! Form::close() !!}	
							</td>
						</tr>
						<tr>
							<td>Policy updates</td>
							<td class="text-right">
							<button class="btn btn-xs"><span class="glyphicon glyphicon-ok font-grey-silver font-lg"></span></button>
							<!--
							{!! Form::open(['method' => 'PATCH', 'action' => ['Account\Modules\Settings\NotificationsController@notifications', $notification->notification_id], 'style' => 'display:inline-block;']) !!}
							@if($notification->receive_message == 1)
								{!! Form::hidden('receive_message', 0) !!}
								{!! Form::button('<span class="glyphicon glyphicon-ok font-green-jungle font-lg"></span>', ['type'=>'submit', 'class'=>'btn btn-xs', 'data-singleton' => 'true', 'title' => '']) !!}
							@else
								{!! Form::hidden('receive_message', 1) !!}
								{!! Form::button('<span class="glyphicon glyphicon-remove font-dark font-lg"></span>', ['type'=>'submit', 'class'=>'btn btn-xs', 'data-singleton' => 'true', 'title' => '']) !!}
							@endif
							{!! Form::close() !!}	
							-->
							</td>
						</tr>
					</tbody>
				</table>
				<div class="margin-bottom-10"><cite class="font-grey-silver">Note: Policy updates and selected notificatios are mandatory: you can't opt out of receiving them.</div>
			</div>
		</div>
	</div>
</div>
<!-- END PAGE CONTENT-->
@stop