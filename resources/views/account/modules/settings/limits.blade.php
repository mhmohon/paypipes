@extends('account/html')

@section('pageheader')
    @include('account/modules/settings/includes/pageheader')
@stop
@section('content')
<!-- BEGIN PAGE CONTENT	-->
@include('account/modules/settings/includes/settingstabs')
	<div class="tab-content">
				
		<div class="row">
			<div class="col-md-3">
				Maximum Balance
			</div>
			<div class="col-md-8">
				<div class="progress">
					<div class="progress-bar progress-bar-info popovers" role="progressbar" aria-valuenow="{!! $current_balance_bar !!}" aria-valuemin="0" aria-valuemax="100" style="width:{!! $current_balance_bar !!}%" data-container="body" data-trigger="hover" data-placement="right" data-content="{!! $primary_symbol !!} {!! number_format($convarted_balance,2,'.','') !!}">
						<span class="sr-only"> {!! $current_balance_bar !!}% Complete (success) </span> {!! $primary_symbol !!} {!! number_format($convarted_balance,2,'.','') !!}
					</div>
				</div>
			</div>
			<div class="col-md-1">
				{!! $primary_symbol !!} {!! number_format($wallet_limit->max_balance,2,'.','') !!}
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				Maximum Monthly Turnover
			</div>
			<div class="col-md-8">
				<div class="progress">
					<div class="progress-bar progress-bar-info popovers" role="progressbar" aria-valuenow="{!! $monthly_turnover_bar !!}" aria-valuemin="0" aria-valuemax="100" style="width: {!! $monthly_turnover_bar !!}%" data-container="body" data-trigger="hover" data-placement="right" data-content="{!! $primary_symbol !!} {!! number_format($monthly_turnover,2,'.','') !!}">
						<span class="sr-only"> {!! $monthly_turnover_bar !!}% Complete (success) </span> {!! $primary_symbol !!} {!! number_format($monthly_turnover,2,'.','') !!} 
					</div> 
				</div>
			</div>
			<div class="col-md-1">{!! $primary_symbol !!} {!! number_format($wallet_limit->max_turnover_per_month,2,'.','') !!}</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				Maximum Turnover
			</div>
			<div class="col-md-8">
				<div class="progress">
					<div class="progress-bar progress-bar-info popovers" role="progressbar" aria-valuenow="{!! $current_turnover_bar !!}" aria-valuemin="0" aria-valuemax="100" style="width: {!! $current_turnover_bar !!}%" data-container="body" data-trigger="hover" data-placement="right" data-content="{!! $primary_symbol !!} {!! number_format($current_turnover,2,'.','') !!}">
						<span class="sr-only"> {!! $current_turnover_bar !!}% Complete (success) </span> {!! $primary_symbol !!} {!! number_format($current_turnover,2,'.','') !!} 
					</div> 
				</div>
			</div>
			<div class="col-md-1">
				{!! $primary_symbol !!} {!! number_format($wallet_limit->max_turnover,2,'.','') !!}
			</div>
		</div>
		<div class="row">
			<div class="col-md-3">
				Maximum Withdrawal
			</div>
			<div class="col-md-8">
				<div class="progress">
					<div class="progress-bar progress-bar-info popovers" role="progressbar" aria-valuenow="{!! $current_withdrawal_bar !!}" aria-valuemin="0" aria-valuemax="100" style="width: {!! $current_withdrawal_bar !!}%" data-container="body" data-trigger="hover" data-placement="right" data-content="{!! $primary_symbol !!} {!! number_format($current_withdrawal,2,'.','') !!}">
						<span class="sr-only"> {!! $current_withdrawal_bar !!}% Complete (success) </span> {!! $primary_symbol !!} {!! number_format($current_withdrawal,2,'.','') !!}
					</div> 
				</div>
			</div>
			<div class="col-md-1">{!! $primary_symbol !!} {!! number_format($wallet_limit->max_withdraw_amount,2,'.','') !!}</div>
		</div>
	</div>
</div>
<!-- END PAGE CONTENT-->
@stop