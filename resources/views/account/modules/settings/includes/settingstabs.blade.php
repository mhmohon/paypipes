<div class="tabbable-custom tabbable-tabdrop">
	<ul class="nav nav-tabs">
		<li class="{{ Request::is('account/settings/account') ? 'active' : '' }}">
			<a href="/account/settings/account">Account</a>
		</li>
		<li class="{{ Request::is('account/settings/security') ? 'active' : '' }}">
			<a href="/account/settings/security">Security</a>
		</li>

		<li class="{{ Request::is('account/settings/notifications') ? 'active' : '' }}">
			<a href="/account/settings/notifications">Notifications</a>
		</li>
		<li class="{{ Request::is('account/settings/limits') ? 'active' : '' }}">
			<a href="/account/settings/limits">Limits</a>
		</li>
		<li class="@if (Request::is('account/settings/compliances')) active @elseif (Request::is('account/settings/compliances/*')) active @else @endif">
			<a href="/account/settings/compliances">Compliance</a>
		</li>

	</ul>

