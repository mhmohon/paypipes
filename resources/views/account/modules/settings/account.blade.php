@extends('account/html')

@section('pageheader')
    @include('account/modules/settings/includes/pageheader')
@stop
@section('content')
<!-- BEGIN PAGE CONTENT	-->
@include('account/modules/settings/includes/settingstabs')
<!-- <div> -->
	<div class="tab-content">
		<div class="row">
			<div class="col-md-6">
				<h4>Profile  <a href="#editphone" class="btn blue btn-xs" data-toggle="modal"><i class="fa fa-pencil"></i></a></h4>
				<div style="font-size:22px;font-weight:300;">{!!  $account->first_name !!} {!!  $account->last_name !!} </div>
				<div class="margin-bottom-10"><cite class="font-grey-silver">Joined {!! Carbon\Carbon::parse( $account->created_at)->diffForHumans() !!}</cite></div>
				<div class="margin-bottom-5" style="font-size:18px;font-weight:300;">{!!  $account->email !!}</div>
				<div class="margin-bottom-5" style="font-size:18px;font-weight:300;">{!!  $account->phone !!} </div>
				<div class="modal fade" id="editphone" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
								<h4 class="modal-title">Edit Account</h4>
							</div>
							<div class="modal-body">
								{!! Form::model($account, ['method' => 'PATCH', 'action' => ['Account\Modules\Settings\AccountController@update', $account->account_id ]]) !!}
									<div class="form-body">
										<div class="form-group">
											<div class="row">
												<div class="col-md-6">
													<div class="@if ($errors->has('first_name')) has-error @endif">
														<label>First Name</label>
														{!! Form::text('first_name', null, ['class' => 'form-control', 'placeholder' => 'First Name', 'required' => 'required']) !!}
														@if ($errors->has('first_name')) <p class="help-block">{{ $errors->first('first_name') }}</p> @endif
													</div>
												</div>
												<div class="col-md-6">
													<div class="@if ($errors->has('last_name')) has-error @endif">
														<label>Last Name</label>
														{!! Form::text('last_name', null, ['class' => 'form-control', 'placeholder' => 'Last Name', 'required' => 'required']) !!}
														@if ($errors->has('last_name')) <p class="help-block">{{ $errors->first('last_name') }}</p> @endif
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<div class="col-md-6">
													<div class="@if ($errors->has('email')) has-error @endif">
														<label>Email</label>
														{!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email', 'required' => 'required','readonly'=>'readonly']) !!}
														@if ($errors->has('email')) <p class="help-block">{{ $errors->first('email') }}</p> @endif
													</div>
												</div>
												<div class="col-md-6">
													<div class="@if ($errors->has('phone')) has-error @endif">
														<label>Phone</label>
														{!! Form::text('phone', null, ['class' => 'form-control phone', 'placeholder' => 'Phone', 'required' => 'required']) !!}
														@if ($errors->has('phone')) <p class="help-block">{{ $errors->first('phone') }}</p> @endif
													</div>
												</div>
											</div>
										</div>
										<div class="form-group @if ($errors->has('street')) has-error @endif">
											<label>Street</label>
											{!! Form::text('street', null, ['class' => 'form-control address', 'placeholder' => 'Street', 'required' => 'required']) !!}
											@if ($errors->has('street')) <p class="help-block">{{ $errors->first('street') }}</p> @endif
										</div>
										<div class="form-group">
											<div class="row">
												<div class="col-md-6">
													<div class="@if ($errors->has('city')) has-error @endif">
														<label>City</label>
														{!! Form::text('city', null, ['class' => 'form-control address', 'placeholder' => 'City', 'required' => 'required']) !!}
														@if ($errors->has('city')) <p class="help-block">{{ $errors->first('city') }}</p> @endif
													</div>
												</div>
												<div class="col-md-6">
													<div class="@if ($errors->has('state')) has-error @endif">
														<label>State</label>
														{!! Form::text('state', null, ['class' => 'form-control address', 'placeholder' => 'State']) !!}
														@if ($errors->has('state')) <p class="help-block">{{ $errors->first('state') }}</p> @endif
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<div class="row">
												<div class="col-md-6">
													<div class="@if ($errors->has('post_code')) has-error @endif">
														<label>Post Code</label>
														{!! Form::text('post_code', null, ['class' => 'form-control address', 'placeholder' => 'Post Code', 'required' => 'required']) !!}
														@if ($errors->has('post_code')) <p class="help-block">{{ $errors->first('post_code') }}</p> @endif
													</div>
												</div>
												<div class="col-md-6">
													<div class="@if ($errors->has('country')) has-error @endif">
														<label>Country</label>
														{!! Form::select('country', $countries, null, ['id' => 'country','class' => 'bs-select form-control', 'required' => 'required'], ['data-live-search' => '']) !!}
														@if ($errors->has('country')) <p class="help-block">{{ $errors->first('country') }}</p> @endif
													</div>
												</div>
											</div>
										</div>
										<div class="form-group">
											<span class="input-group-btn">
												{!! Form::submit('Update Accounts', ['class' => 'btn btn-block green-jungle btn-lg', 'name' => 'editaccount'] ) !!}
											</span>
										</div>
									</div>
								{!! Form::close() !!}
							</div>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dialog -->
				</div>
				<!-- /.modal -->
			</div>
			<div class="col-md-6">
				<h4>Address</h4>
				<div style="font-size:18px;font-weight:300;">
					{!! $account->street !!},
					{!!  $account->city !!},
					{!!  $account->post_code !!},
					@if(!empty($account->state))
					{!!  $account->state !!},
					@endif
					{!!  $account->country_name !!}
				</div>
				<div class="margin-bottom-10"><cite class="font-grey-silver">Registered Address<cite></div>
				@if ( $account->compliance == 'Not Required')
				@else
				<h4>Compliance</h4>
				<span class="label label-sm @if ( $account->compliance === 'Approved') label-success @elseif ( $account->compliance === 'Pending') label-info @elseif ( $account->compliance === 'Declined') label-danger @else label-default @endif">{!!  $account->compliance !!}</span>
				@endif
			</div>
		</div>
	</div>
</div>
<!-- END PAGE CONTENT-->
@stop
