@extends('account/html')

@section('pageheader')
    @include('account/modules/settings/includes/pageheader')
@stop
@section('content')
<!-- BEGIN PAGE CONTENT	-->
@include('account/modules/settings/includes/settingstabs')
	<div class="tab-content">
		@if ($account->compliance === 'Not Required')
		<h4>There are no documents required for your +Wallet Account. To upgrade your +Wallet Account click <a href="/account#upgrade">here</a>.</h4>
		@else
		<h4 class="page-title">
			Compliance
			<a class="btn green-jungle bold" data-toggle="modal" href="#adddoc">Add <i class="fa fa-plus"></i></a>
		</h4>
		<div class="modal fade" id="adddoc" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title">Add a Document</h4>
					</div>
					<div class="modal-body">
						{!! Form::model($account, ['method' => 'POST', 'files' => 'true', 'action' => ['Account\Modules\Settings\ComplianceController@store', $account->account_id ]]) !!}
							<div class="form-body">
								<div class="form-group">
									{!! Form::file('upload_file') !!}
									@if ($errors->has('upload_file')) <p class="help-block">{{ $errors->first('upload_file') }}</p> @endif
								</div>
								<div class="form-group @if ($errors->has('compliance_name')) has-error @endif">
									{!! Form::text('compliance_name', null, ['class' => 'form-control', 'placeholder' => 'Title']) !!}
									@if ($errors->has('compliance_name')) <p class="help-block">{{ $errors->first('compliance_name') }}</p> @endif
								</div>
								<div class="form-group @if ($errors->has('description')) has-error @endif">
									{!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Description']) !!}
									@if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
								</div>
								<div class="form-group">
									<span class="input-group-btn">
										{!! Form::submit('Add', ['class' => 'btn btn-block green-jungle btn-lg'] ) !!}
									</span>
								</div>
							</div>
						{!! Form::close() !!}
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		@if (count($compliances) == 0)
		<p class="text-center font-grey-silver margin-top-15">There are no documents to display</p>
		@else
		<div class="table-scrollable">
			<table class="table table-light table-bordered table-striped table-hover">
				<thead>
					<tr>
						<th style="width:1%">Date</th>
						<th>Description</th>
						<th style="width:1%">Status</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($compliances as $compliance)
					<tr class='clickable-row' data-href='/account/settings/compliances/{!! $compliance->compliance_id !!}'>
						<td class="text-center"><span class="font-sm uppercase">{!! Carbon\Carbon::parse($compliance->created_at)->format('M') !!}</span> </br> <span style="font-size:22px;font-weight:300;">{!! Carbon\Carbon::parse($compliance->created_at)->format('d') !!}</span></td>
						<td><span style="font-size:18px;font-weight:300;">{!! $compliance->name !!}</span></br>ID {!! $compliance->compliance_id !!}, {!! $compliance->description !!}</td>
						<td><span class="label label-sm @if ($compliance->status === 'Approved') label-success @elseif ($compliance->status === 'Pending') label-info @elseif ($compliance->status === 'Declined') label-danger @else label-default @endif">{!! $compliance->status !!}</span></td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<div class="row">
			<div class="col-md-6 margin-top-10">
				Showing {!! $compliances->firstItem() !!} to {!! $compliances->lastItem() !!} of {!! $compliances->total() !!}
			</div>
			<div class="col-md-6 text-right">
				{!! $compliances->links() !!}
			</div>
		</div>
		@endif
		@endif
	</div>
</div>
<!-- END PAGE CONTENT-->
@stop
