@extends('account/html')

@section('pageheader')
    @include('account/modules/settings/includes/pageheader')
@stop
@section('content')
<!-- BEGIN PAGE CONTENT	-->
@include('account/modules/settings/includes/settingstabs')
	<div class="tab-content">
		<h4 class="page-title">
			{!! $subpagetitle !!}
		</h4>
		<div class="table-scrollable">
			<table class="table table-light table-hover">
				<tbody>
					<tr>
						<td style="width:30%">Compliance</td>
						<td><span class="label label-sm @if ($compliance->status === 'Approved') label-success @elseif ($compliance->status === 'Pending') label-info @elseif ($compliance->status === 'Locked') label-danger @elseif ($compliance->status === 'Declined') label-danger @else label-default @endif">{!! $compliance->status !!}</span></td>
					</tr>
					<tr>
						<td>Name</td>
						<td>{!! $compliance->name !!}</td>
					</tr>
					<tr>
						<td>Description</td>
						<td>{!! $compliance->description !!}</td>
					</tr>
					<tr>
						<td>File Type</td>
						<td style="text-transform:uppercase">{!! $compliance->file_type !!}</td>
					</tr>
					<tr>
						@if ($compliance->file_type === 'pdf') <td colspan="2"><embed src="/{!! $compliance->file_path !!}" width="100%" height="600px" /></td>
						@elseif ($compliance->file_type === 'jpg') <td colspan="2"><img src="/{!! $compliance->file_path !!}" style="max-width:100%" /></td>
						@elseif ($compliance->file_type === 'png') <td colspan="2"><img src="/{!! $compliance->file_path !!}" style="max-width:100%" /></td>
						@else <td colspan="2"></td>
						@endif
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>
<!-- END PAGE CONTENT-->
@stop