@extends('account/html')

@section('pageheader')
    @include('account/modules/sendrequest/includes/pageheader')
@stop
@section('content')
<!-- BEGIN PAGE CONTENT	-->
@foreach ($request_payments as $request_payment)
<div class="note note-info">
	<div style="float:left" class="font-grey-gallery">
		Payment request of {!! $request_payment->total_amount !!} {!! $request_payment->currency !!} for {!! $request_payment->description !!} from {!! $request_payment->added_by !!} ({!! $request_payment->to !!})
		{!! Form::open(['method' => 'POST', 'action' => ['Account\Modules\Sendrequest\SendrequestController@pay', $request_payment->payment_id], 'style' => 'display:inline-block;']) !!}
			{!! Form::hidden('amount', $request_payment->total_amount) !!}
			{!! Form::submit('Approve', ['name' => 'pay', 'class' => 'btn green-jungle btn-xs', 'data-toggle' => 'confirmation', 'data-popout' => 'true', 'data-singleton' => 'true', 'data-original-title' => 'Approve! Are you sure?', 'title' => '']) !!}
		{!! Form::close() !!}
		{!! Form::open(['method' => 'POST', 'action' => ['Account\Modules\Sendrequest\SendrequestController@pay', $request_payment->payment_id], 'style' => 'display:inline-block;']) !!}
			{!! Form::hidden('amount', $request_payment->total_amount) !!}
			{!! Form::submit('Decline', ['name' => 'decline', 'class' => 'btn red btn-xs', 'data-toggle' => 'confirmation', 'data-popout' => 'true', 'data-singleton' => 'true', 'data-original-title' => 'Decline! Are you sure?', 'title' => '']) !!}
		{!! Form::close() !!}
	</div>
	<div style="float:right"><cite class="font-grey-silver">{!! Carbon\Carbon::parse($request_payment->created_at)->diffForHumans() !!}</cite></div>
	<div style="clear:both"></div>
</div>
@endforeach
<div class="modal fade" id="wallet" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Add +Wallet Account</h4>
            </div>
            <div class="modal-body"> 
                {!! Form::open() !!}
                    <div class="form-body">
                        <div class="margin-bottom-15 font-grey-silver">Select currency for your +Wallet Account. You can add different currencies later.</div>
                        <div class="form-group @if ($errors->has('currency')) has-error @endif">
                            {!! Form::select('currency', $currencies, null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
                            @if ($errors->has('currency')) <p class="help-block">{{ $errors->first('currency') }}</p> @endif
                        </div>
                        <div class="form-group">
                            <span class="input-group-btn">
                                {!! Form::submit('Add', ['class' => 'btn btn-block green-jungle btn-lg'] ) !!}
                            </span>
                        </div>
                    </div>
                {!! Form::close() !!}  
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="email" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Verify Email Address</h4>
            </div>
            <div class="modal-body"> 
                {!! Form::open() !!}
                    <div class="form-body">
                        <div class="margin-bottom-15 font-grey-silver">Enter verification code from email you have received on xxx@xxx.xxx.</div>
                        <div class="form-group @if ($errors->has('email_verification_code')) has-error @endif">
                            {!! Form::text('email_verification_code', null, ['class' => 'form-control input-lg', 'placeholder' => 'Verification Code']) !!}
                            @if ($errors->has('email_verification_code')) <p class="help-block">{{ $errors->first('email_verification_code') }}</p> @endif
                        </div>
                        <div class="margin-bottom-15"><a href="#">Resend verification email</a></div>
                        <div class="form-group">
                            <span class="input-group-btn">
                                {!! Form::submit('Continue', ['class' => 'btn btn-block green-jungle btn-lg'] ) !!}
                            </span>
                        </div>
                    </div>
                {!! Form::close() !!}  
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!--
<div class="row margin-bottom-15">    
    <div class="col-md-4">
        <h4>Hi {!! Auth::guard('account')->user()->first_name !!}</h4>
        <div class="font-grey-silver">Your +Wallet is on <span class="font-green-jungle">25%</span>. Complete your +Wallet to get most of it.</div>
    </div>
    <div class="col-md-2 text-center">
        <a href="#wallet" data-toggle="modal"><i class="icon-check font-green-jungle" style="font-size:64px;line-height:64px;"></i></a></br>
        <span class="font-grey-silver">+Wallet Account </br>Created</span>
    </div>
    <div class="col-md-2 text-center">
        <a href="#email" data-toggle="modal"><i class="icon-close font-grey-silver" style="font-size:64px;line-height:64px;"></i></a></br>
        <span class="font-grey-silver">Email Address</br>Verified</span>
    </div>
    <div class="col-md-2 text-center">
        <i class="icon-close font-grey-silver" style="font-size:64px;line-height:64px;"></i></br>
        <span class="font-grey-silver">Phone Number</br>Verified</span>
    </div>
    <div class="col-md-2 text-center">
        <i class="icon-close font-grey-silver" style="font-size:64px;line-height:64px;"></i></br>
        <span class="font-grey-silver">Card </br>Added</span>
    </div>
</div>
<div class="margin-bottom-15" style="border-bottom:1px solid #F2F5F8;"></div>
-->
<div class="row">
    <div class="col-md-4">
		@if (count($wallets) > 0)
		@else
			<h4>+Wallet Account</h4>
			<div class="font-grey-silver margin-bottom-10">Add +Wallet Account to send and receive payments</div>
			<a class="btn blue-madison btn-block btn-outline" href="#wallet" data-toggle="modal">Add +Wallet Account</a>
		@endif
		@if (count($wallets) > 0)
        <h4>+Wallet Balance</h4>
        <div class="font-grey-silver">You don't need a balance to shop or send payments</div>
        <div class="table margin-top-10">
			
            <table class="table table-hover table-light">
                <thead>
                    <tr>
                        <th></th>
                        <th class="text-right" style="width:1%">Current</br>Balance</th>
                        <th class="text-right" style="width:1%">Available</br>Balance</th>
                    </tr>
                </thead>
                <tbody>
					@foreach ($wallets as $wallet)
					<tr>
                        <td><span class="label label-sm label-default">Primary</span></td>
                        <td class="text-right"><span class="font-green-jungle" style="font-size:22px;font-weight:300;">{!! $wallet->balance !!}</span></br> <span class="font-sm">{!! $wallet->currency !!}</span></td>
                        <td class="text-right"><span class="font-green-jungle" style="font-size:22px;font-weight:300;">{!! $wallet->balance !!}</span></br> <span class="font-sm">{!! $wallet->currency !!}</span></td>
                    </tr>
					@endforeach
                    <!--
					<tr>
                        <td colspan="2">Total available in EUR</td>                    
                        <td class="text-right"><span class="font-green-jungle" style="font-size:22px;font-weight:300;">155,50</span></br> <span class="font-sm">EUR</span></td>
                    </tr>
					-->
                </tbody>
            </table>			
        </div>
        <a class="btn blue-madison btn-block btn-outline margin-bottom-5" href="#addcurrency" data-toggle="modal">Add +Wallet Currency</a>
		<div class="modal fade" id="addcurrency" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title">Add +Wallet Currency</h4>
					</div>
					<div class="modal-body"> 
						{!! Form::model($account, ['method' => 'POST', 'action' => ['Account\Modules\Sendrequest\SendrequestController@addcurrency', $account->id ]]) !!}
							<div class="form-body">
								<div class="margin-bottom-15 font-grey-silver">Select currency for your +Wallet Account.</div>
								<div class="form-group @if ($errors->has('currency')) has-error @endif">
								{!! Form::select('currency', $currencies, null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
									@if ($errors->has('currency')) <p class="help-block">{{ $errors->first('currency') }}</p> @endif
								</div>
								<div class="form-group">
									<span class="input-group-btn">
										{!! Form::submit('Add', ['class' => 'btn btn-block green-jungle btn-lg'] ) !!}
									</span>
								</div>
							</div>
						{!! Form::close() !!}  
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
        <a class="btn blue-madison btn-block btn-outline margin-bottom-5" href="#topup" data-toggle="modal">TopUp Balance</a>
		<div class="modal fade" id="topup" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title"><i class="icon-action-redo"></i> TopUp Balance</h4>
					</div>
					<div class="modal-body"> 
						{!! Form::model($account, ['method' => 'POST', 'action' => ['Account\Modules\Sendrequest\SendrequestController@forms', $account->account_id ]]) !!}
							<div class="form-body">
								<div class="text-center margin-bottom-15 font-grey-silver">Declare an amount and currency in which you will transfer funds from your bank account to your +Wallet account.</div>
								<div class="form-group @if ($errors->has('amount')) has-error @endif">
									{!! Form::text('amount', null, ['class' => 'form-control text-center', 'style' => 'border:0px;font-size:64px;height:80px;', 'placeholder' => '0,00']) !!}
									@if ($errors->has('amount')) <p class="help-block">{{ $errors->first('amount') }}</p> @endif
								</div>
								<div class="form-group @if ($errors->has('currency')) has-error @endif">
									<div style="margin:auto;width:145px;">
									{!! Form::select('currency', $wallets_currency, null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
									@if ($errors->has('currency')) <p class="help-block">{{ $errors->first('currency') }}</p> @endif
									</div>
								</div>
								<div class="form-group">
									<span class="input-group-btn">
										{!! Form::submit('Next', ['class' => 'btn btn-block green-jungle btn-lg', 'name' => 'topup'] ) !!}
									</span>
								</div>
							</div>
						{!! Form::close() !!}  
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		
		<a class="btn blue-madison btn-block btn-outline margin-bottom-5" href="#withdraw" data-toggle="modal">Withdraw Funds</a>
		<div class="modal fade" id="withdraw" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title"><i class="icon-action-redo"></i> Withdraw Funds</h4>
					</div>
					<div class="modal-body"> 
						{!! Form::model($account, ['method' => 'POST', 'action' => ['Account\Modules\Sendrequest\SendrequestController@forms', $account->account_id ]]) !!}
							<div class="form-body">
								<div class="text-center margin-bottom-15 font-grey-silver">Declare an amount and currency in which you will transfer funds from your bank account to your +Wallet account.</div>
								<div class="form-group @if ($errors->has('amount')) has-error @endif">
									{!! Form::text('amount', null, ['class' => 'form-control text-center', 'style' => 'border:0px;font-size:64px;height:80px;', 'placeholder' => '0,00']) !!}
									@if ($errors->has('amount')) <p class="help-block">{{ $errors->first('amount') }}</p> @endif
								</div>
								<div class="form-group @if ($errors->has('currency')) has-error @endif">
									<div style="margin:auto;width:145px;">
									{!! Form::select('currency', $wallets_currency, null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
									@if ($errors->has('currency')) <p class="help-block">{{ $errors->first('currency') }}</p> @endif
									</div>
								</div>
								<div class="form-group">
									<span class="input-group-btn">
										{!! Form::submit('Next', ['class' => 'btn btn-block green-jungle btn-lg', 'name' => 'withdraw'] ) !!}
									</span>
								</div>
							</div>
						{!! Form::close() !!}  
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		<a class="btn blue-madison btn-block btn-outline margin-bottom-5" href="#sendrequest" data-toggle="modal">SendRequest Payments</a>
		<div class="modal fade" id="sendrequest" tabindex="-1" role="dialog" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
						<h4 class="modal-title"><i class="icon-action-redo"></i> SendRequest Payments</h4>
					</div>
					<div class="modal-body"> 
						{!! Form::model($account, ['method' => 'POST', 'action' => ['Account\Modules\Sendrequest\SendrequestController@forms', $account->account_id ]]) !!}
							<div class="form-body">
								<div class="text-center margin-bottom-15 font-grey-silver">Send payments to almost anyone with an email address and it will go to their +Wallet account. If they don’t have an account, we'll help them sign up for free.</div>
								<div class="form-group @if ($errors->has('to_email')) has-error @endif">
									{!! Form::text('to_email', null, ['class' => 'form-control input-lg text-center', 'placeholder' => 'Email']) !!}
									@if ($errors->has('to_email')) <p class="help-block">{{ $errors->first('to_email') }}</p> @endif
								</div>
								<div class="form-group @if ($errors->has('amount')) has-error @endif">
									{!! Form::text('amount', null, ['class' => 'form-control text-center', 'style' => 'border:0px;font-size:64px;height:80px;', 'placeholder' => '0,00']) !!}
									@if ($errors->has('amount')) <p class="help-block">{{ $errors->first('amount') }}</p> @endif
								</div>
								<div class="form-group @if ($errors->has('currency')) has-error @endif">
									<div style="margin:auto;width:145px;">
										{!! Form::select('currency', $currencies, null, ['class' => 'bs-select form-control'], ['data-live-search' => '']) !!}
										@if ($errors->has('currency')) <p class="help-block">{{ $errors->first('currency') }}</p> @endif
									</div>
								</div>
								<div class="form-group @if ($errors->has('description')) has-error @endif">
									{!! Form::text('description', null, ['class' => 'form-control input-lg text-center', 'placeholder' => 'Add a Note']) !!}
									@if ($errors->has('description')) <p class="help-block">{{ $errors->first('description') }}</p> @endif
								</div>
								<div class="form-group">
									<span class="input-group-btn">
										{!! Form::submit('Next', ['class' => 'btn btn-block green-jungle btn-lg', 'name' => 'sendrequest'] ) !!}
									</span>
								</div>
							</div>
						{!! Form::close() !!}  
					</div>
				</div>
				<!-- /.modal-content -->
			</div>
			<!-- /.modal-dialog -->
		</div>
		@else
		@endif
    </div>
    <div class="col-md-8">
		<div class="portlet">
			<div class="portlet-body">
				<div class="tabbable tabbable-tabdrop">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#Payments" data-toggle="tab">Recent Payments</a>
						</li>
						<li>
							<a href="#Activity" data-toggle="tab">Recent Activity</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="Payments">
							<div class="portlet-body">	
								<div class="table-scrollable">
									<table class="table table-light table-hover table-bordered table-striped">
										<thead>
											<tr data-href="javascript:void(0);">
												<th style="width:1%">Date</th>
												<th>Description</th>
												<th style="width:1%">Method</th>
												<th style="width:1%">Type</th>  								
												<th class="text-right" style="width:1%">Amount</th>    
												<th style="width:1%">Status</th>
											</tr>
										</thead>
										<tbody>  
											@foreach ($payments as $payment)
											<tr class='clickable-row' data-href='/account/payments/{!! $payment->payment_id !!}'>
												<td class="text-center sorting_1" tabindex="0"><span class="font-sm uppercase">{!! Carbon\Carbon::parse($payment->created_at)->format('M') !!}</span> </br> <span style="font-size:22px;font-weight:300;">{!! Carbon\Carbon::parse($payment->created_at)->format('d') !!}</span></td>
												<td>From {!! $payment->from !!} to {!! $payment->to !!}</br><span style="font-size:18px;font-weight:300;">{!! $payment->description !!}</span></td>
												<td>{!! $payment->payment_method !!}</td>
												<td>{!! $payment->payment_type !!}</td>
												<td class="text-right">
													<span style="font-size:22px;font-weight:300;" class="@if ($payment->to === Auth::guard('account')->user()->email) font-green-jungle @elseif ($payment->from === Auth::guard('account')->user()->email) font-red-thunderbird @else @endif">
														@if ($payment->to === Auth::guard('account')->user()->email) +@elseif ($payment->from === Auth::guard('account')->user()->email) -@else @endif{!! $payment->total_amount !!}
													</span> </br>
													<span class="font-sm">{!! $payment->currency !!}</span>
												</td>
											<td><span class="label label-sm @if ($payment->status === 'Approved') label-success @elseif ($payment->status === 'Pending') label-info @elseif ($payment->status === 'Requested') label-info @elseif ($payment->status === 'Refunded') label-purple @elseif ($payment->status === 'Canceled') label-warning @elseif ($payment->status === 'Chargeback') label-black @elseif ($payment->status === 'Declined') label-danger @else label-default @endif">{!! $payment->status !!}</span></td>
											</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
							<div class="scroller-footer">
								<div class="btn-arrow-link text-center">
									<a href="/account/payments">See All Payments</a>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="Activity">
							<div class="portlet">
								<div class="portlet-body">
									<div class="scroller" data-always-visible="1" data-rail-visible="0">
										@foreach ($activities as $activity)
										<ul class="feeds">
											<li>
												<div class="col1">
													<div class="cont">
														<div class="cont-col1">                                        
															@if ($activity->module === 'Customer') <button class="btn btn-xs green-jungle"><i class="icon-users"></i></button>
															@elseif ($activity->module === 'Merchant') <button class="btn btn-xs blue"><i class="icon-screen-desktop"></i></button>
															@elseif ($activity->module === 'Partner') <button class="btn btn-xs green"><i class="icon-user-following"></i></button>
															@elseif ($activity->module === 'Payment') <button class="btn btn-xs blue"><i class="fa fa-credit-card"></i></button>
															@elseif ($activity->module === 'RiskManagement') <button class="btn btn-xs red-thunderbird"><i class="icon-shield"></i></button>
															@elseif ($activity->module === 'Help') <button class="btn btn-xs yellow-lemon"><i class="icon-question"></i></button>
															@elseif ($activity->module === 'Wallet') <button class="btn btn-xs purple"><i class="icon-wallet"></i></button>
															@elseif ($activity->module === 'Settings') <button class="btn btn-xs dark"><i class="fa fa-cogs"></i></button>
															@else 
															@endif
														</div>
														<div class="cont-col2">
															<div class="desc"> {!! $activity->activity !!} </div>
														</div>
													</div>
												</div>
												<div class="col2">
													<div class="date"> {!! Carbon\Carbon::parse($activity->created_at)->diffForHumans() !!}</div>
												</div>
											</li>
										</ul>
										@endforeach
									</div>
									<div class="scroller-footer">
										<div class="btn-arrow-link text-center">
											<a href="/account/activitylog">See All Activity</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>								
		</div>
    </div>
</div>
<!-- END PAGE CONTENT-->
@stop