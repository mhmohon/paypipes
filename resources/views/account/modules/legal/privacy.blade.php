@extends('account/html')

@section('pageheader')
    @include('account/modules/legal/includes/pageheader')
@stop
@section('content')
<!-- BEGIN PAGE CONTENT	-->
<h3>Privacy Policy</h3>			
			
<p>This communication is provided on the basis of § 5 par. a) of Act No. 101/2000 Coll., on the Protection of Personal Data and on Amendments to Certain Acts, as amended, and with effect from 25 May 2018 pursuant to Article 6 (1) (c) Regulation (EU) 2016/679 on the protection of individuals with regard to the processing of personal data and on the free movement of such data and repealing Directive 95/46/ES.</p>

<h3>About us</h3>
<p>1. PayPipes (also referred to as the "Data Administrator") is the PayPipes company s., With registered office at 5. kvetna 720/20, 741 01 Novy Jicin, ID: 05307881, registered in the Commercial Register C 67031, kept at the Regional Court in Ostrava. The same meaning also includes concepts such as "we", "us", "our", "our", etc., which are used in these OS rules.</p>

<h3>Basic information</h3>
<p>1. These Privacy Policy ("OS Rules") govern the way we process and process personal data in the PayPipes Payment System. We are governed by Czech Act No. 101/2000 Coll., On the Protection of Personal Data and on Amendments to Certain Acts, as amended (hereinafter referred to as "ZOOÚ") and with effect from 25 May 2018 by Regulation (EU) 2016/679, on protection of natural persons with regard to the processing of personal data and on the free movement of such data and repealing Directive 95/46 / EC (hereinafter referred to as the "GDPR Regulation").</p>
<p>2. While processing personal data, we must take care not to harm the rights of individuals in connection with the processing of personal data. At the same time, we are obliged to comply with the provisions of the relevant legal regulations and to apply reasonable measures to protect the legal order, the democratic society and its particular important interests, as outlined below.</p>

<h3>What personal data we process</h3>
<p>Pursuant to Act No. 101/2000 Coll., on the Protection of Personal Data and on Amendments to Certain Acts, as amended (hereinafter referred to as the "ZOOU"), and with effect from 25 May 2018 within the meaning of the GDPR Regulation, we are authorized to process your personal data (hereinafter referred to as "OU") in the scope of:</p>
<ul>
<li>name, surname, title, birth number, date of birth, place of birth, sex,</li>
<li>permanent residence or other residence and nationality, telephone number, e-mail delivery address,</li>
<li>a copy of personal documents proving your identity,</li>
<li>Business entrepreneur: Business firm, distinctive addendum or other designation, place of business and personal identification number, further</li>
<li>bank account number incl. account owner names,</li>
<li>data on executed and canceled payment transactions,</li>
<li>details of any credit, debit or other credit card, including PAN numbers, expiration dates, and the name of the credit card holder,</li>
<li>about all communication,</li>
<li>information obtained from questionnaires or similar forms that you may be asked to fill in,</li>
<li>IP address and connection times for your device,</li>
<li>data about your visits to our website, especially traffic data, location data, weblogs, etc., as well as data about your Internet behavior
(hereinafter referred to as "Personal Data").</li>
</ul>
<p>The processing time of the OO is 10 years from the trade or the termination of the business relationship between you and the data controller (whichever comes later). You are aware that you can not withdraw this consent as a data subject.</p>
<p>The Data Administrator is entitled to provide these data on request to the competent governmental authorities and you understand it.</p>
<p>If a case of personal data breach is likely to result in a high risk to the rights and freedoms of the data subject, the data controller will notify you of this violation without undue delay.</p>
<p>As a data subject, you acknowledge and acknowledge the above</p>
</ul>

<h3>Lessons learned about your rights</h3>
<p>1. If a data subject requests information on the processing of his or her personal data, the data controller shall be obliged to transmit this information without undue delay to the data controller pursuant to Section 12 of the ZOO. The content of the information is always a message about the purpose of processing personal data, personal data, or the categories of personal data being processed, including any available information about their source; the nature of the automated processing in connection with its use for decision-making if acts or decisions involving the interference with the law and the legitimate interests of the data subject are made by such processing; beneficiaries or categories of beneficiaries. The Data Administrator has the right to request reasonable compensation for the provision of the information, not exceeding the costs necessary to provide the information. The controller's obligation to provide information to the data subject, as regulated in Section 12 of the ZOO, may be performed by the processor.</p>
<p>2. Any data subject who discovers or considers that the data controller or processor is processing his or her personal data in conflict with the privacy or privacy of the data subject or in contravention of the law, in particular if personal data are inaccurate with respect to for the purpose of their processing, may, in accordance with Section 21 of the ZOOÚ a) request the data controller or the processor for explanation; b) require the data controller or the processor to remove the resulting situation; in particular, it may be blocking, repairing, supplementing or disposing of personal data. If the data subject's request under the preceding sentence is found to be justified, the data controller or the processor shall immediately remove the defective condition. If, as a result of the processing of personal data of the data subject other than property damage, the claim is exercised under a special law. If the data controller has violated the obligations imposed by the law on the data controller or the processor, they are jointly and severally liable for the processing of personal data.</p>
<p>3. You acknowledge that in addition to the above-mentioned rights regulated in the ZOOU for you since the effective date of the GDPR Regulation, additional rights under Articles 15 to 23 of the GDPR Regulation arise, namely the right to request from the data controller access to the personal data concerning you including the confirmation that the personal data concerning you are processed or not), the right to request their correction or deletion, or limitation of the processing, and the right to object processing, as well as the right to data portability. You further acknowledge that you have the right to file a complaint with the supervisory authority, which the Office for Personal Data Protection, established Pplk. Sochora 27, 170 00 Praha 7.</p>
<p>4. The contact details of the Data Protection Officer's Data Protection Officer are:
o Zdenek Pokorny, email: z.pokorny@paypipes.com, tel: +420607803355</p>
<p>5. You acknowledge that the OS rules contain all information provided to the data subject pursuant to Article 13 of the GDPR Regulation.</p>

<h3>Final Provisions</h3>
<p>1. By continuing to use the PayPipes Payment System features, you express your free, concrete, informed and unambiguous manifestation of willingness to understand your processing of your personal data under these OS rules and that you have been properly trained and / informed.</p>
<p>2. We are authorized to unilaterally modify these OSs under the applicable laws and you agree to this authorization.</p>
<p>3. By modifying the OS rules, we are required to notify you in advance of this change in the form of an e-mail message that will contain a link to the new OS rules, from which you can print or save it in electronic form.</p>
<p>4. The OS rules are published in electronic form and are available on the PayPipes website.</p>
<p>The 5th Rule takes effect on 24.05.2018.</p>

<hr>

<h3>Ochrana Soukromí</h3>
<p>Toto sdělení je poskytnuto na základě § 5 odst. 2 písm. a) zákona č. 101/2000 Sb., o ochraně osobních údajů a o změně některých zákonů, ve znění pozdějších předpisů, a s účinností od 25. 5. 2018 dle čl. 6 odst. 1 písm. c) Nařízení (EU) 2016/679, o ochraně fyzických osob v souvislosti se zpracováním osobních údajů a o volném pohybu těchto údajů a o zrušení směrnice 95/46/ES.</p>
<h3>O nás</h3>
<p>1.	Společností PayPipes (dále také „správce údajů“) se rozumí obchodní společnost PayPipes s. r. o., se sídlem 5. května 720/20, 741 01 Nový Jičín, IČ: 05307881, zapsaná v obchodním rejstříku C 67031 vedená u Krajského soudu v Ostravě. Stejný význam mají též pojmy jako “my”, “nás”, “náš”, “naše” apod., které se používají v těchto OS pravidlech.</p>
<h3>Základní informace</h3>
<p>1.	Tato pravidla ochrany soukromí (dále jen “OS pravidla”) upravují způsob, jakým přistupujeme k zpracování osobních údajů a zabezpečujeme jejich ochranu v Platebním systému PayPipes. Řídíme se českým zákonem č. 101/2000 Sb., o ochraně osobních údajů a o změně některých zákonů, v platném znění (dále jen „ZOOÚ“), a s účinností od 25. 5. 2018 Nařízením (EU) 2016/679, o ochraně fyzických osob v souvislosti se zpracováním osobních údajů a o volném pohybu těchto údajů a o zrušení směrnice 95/46/ES (dále jen „Nařízení GDPR“).</p>
<p>2.	Během zpracování osobních údajů dbáme na to, aby nedošlo k újmě na právech fyzických osob v souvislosti se zpracováním osobních údajů. Zároveň jsme povinni plnit ustanovení příslušných právních předpisů a aplikovat přiměřená opatření na ochranu právního pořádku, demokratické společnosti a jejích konkrétních důležitých zájmů, jak je uvedeno dále.</p>
<h3>Jaké osobní údaje zpracováváme</h3>
<p>Ve smyslu zákona č. 101/2000 Sb., o ochraně osobních údajů a o změně některých zákonů, ve znění pozdějších předpisů (dále jen „ZOOÚ“), a s účinností od 25. 5. 2018 ve smyslu Nařízení GDPR jsme oprávněni zpracovávat Vaše osobní údaje (dále jen “OÚ”) v rozsahu:</p>
<ul>
<li>jméno, příjmení, titul, rodné číslo, případně datum narození, místo narození, pohlaví,</li>
<li>trvalý pobyt nebo jiný pobyt a státní občanství, číslo telefonu, adresa pro doručování elektronické pošty,</li>
<li>kopie osobních dokladů prokazujících Vaši totožnost,</li>
<li>u podnikající fyzické osoby: obchodní firma, odlišující dodatek nebo další označení, místo podnikání a identifikační číslo osoby, dále</li>
<li>číslo bankovního účtu vč. jména majitele účtu,</li>
<li>údaje o realizovaných i zrušených platebních transakcích,</li>
<li>údaje o jakékoli kreditní, debetní nebo jiné platební kartě, včetně PAN čísla, data expirace a jméno držitele platební karty,</li>
<li>veškerou realizovanou komunikaci,</li>
<li>informace získané z dotazníků nebo obdobných formulářů, o jejichž vyplnění můžete být požádán,</li>
<li>IP adresu a časy připojení Vašeho zařízení,</li>
<li>údaje o Vašich návštěvách na našich webových stránkách, zejména provozní data, lokalizační data, weblogy, apod., jakož i údaje o Vašem chování v prostředí internetu</li>
</ul>
<p>Doba zpracování OÚ je 10 let od uskutečnění obchodu nebo od ukončení obchodního vztahu mezi Vámi a správcem údajů (podle toho, co nastane později). Jste si vědom, že jako subjekt údajů nemůžete tento souhlas odvolat.</p>
<p>Správce údajů je oprávněn na vyžádání tyto údaje poskytnout příslušným státním orgánům a Vy jste s tím srozuměn/a.</p>
<p>Pokud je pravděpodobné, že určitý případ porušení zabezpečení osobních údajů bude mít za následek vysoké riziko pro práva a svobody subjektu údajů, oznámí Vám správce údajů toto porušení bez zbytečného odkladu.</p>
<p>Jako subjekt údajů berete výše uvedené na vědomí a dále berete na vědomí následující poučení o svých právech.</p>
<h3>Poučení o Vašich právech</h3>
<p>1.	Požádá-li subjekt údajů o informaci o zpracování svých osobních údajů, je mu správce údajů dle § 12 ZOOÚ povinen tuto informaci bez zbytečného odkladu předat. Obsahem informace je vždy sdělení o účelu zpracování osobních údajů, osobních údajích, případně kategoriích osobních údajů, které jsou předmětem zpracování, včetně veškerých dostupných informací o jejich zdroji; povaze automatizovaného zpracování v souvislosti s jeho využitím pro rozhodování, jestliže jsou na základě tohoto zpracování činěny úkony nebo rozhodnutí, jejichž obsahem je zásah do práva a oprávněných zájmů subjektu údajů; příjemci, případně kategoriích příjemců. Správce údajů má právo za poskytnutí informace požadovat přiměřenou úhradu nepřevyšující náklady nezbytné na poskytnutí informace. Povinnost správce údajů poskytnout informace subjektu údajů upravenou v § 12 ZOOÚ může za správce plnit zpracovatel.</p>
<p>2.	Každý subjekt údajů, který zjistí nebo se domnívá, že správce údajů nebo zpracovatel provádí zpracování jeho osobních údajů, které je v rozporu s ochranou soukromého a osobního života subjektu údajů nebo v rozporu se zákonem, zejména jsou-li osobní údaje nepřesné s ohledem na účel jejich zpracování, může dle § 21 ZOOÚ a) požádat správce údajů nebo zpracovatele o vysvětlení, b) požadovat, aby správce údajů nebo zpracovatel odstranil takto vzniklý stav; zejména se může jednat o blokování, provedení opravy, doplnění nebo likvidaci osobních údajů. Je-li žádost subjektu údajů podle předchozí věty shledána oprávněnou, správce údajů nebo zpracovatel odstraní neprodleně závadný stav. Pokud vznikla v důsledku zpracování osobních údajů subjektu údajů jiná než majetková újma, postupuje se při uplatňování jeho nároku podle zvláštního zákona. Došlo-li při zpracování osobních údajů k porušení povinností uložených zákonem u správce údajů nebo u zpracovatele, odpovídají za ně společně a nerozdílně.</p>
<p>3.	Berete na vědomí, že vedle výše uvedených práv upravených v ZOOÚ pro Vás od doby účinnosti Nařízení GDPR vyplývají další práva z čl. 15 až 23 Nařízení GDPR, a sice právo požadovat od správce údajů přístup k osobním údajům, které se Vás týkají (včetně potvrzení, zda osobní údaje, které se Vás týkají, jsou či nejsou zpracovávány), právo požadovat jejich opravu nebo výmaz, popřípadě omezení zpracování, a právo vznést námitku zpracování, jakož i právo na přenositelnost údajů. Dále berete na vědomí, že máte právo podat stížnost u dozorového úřadu, kterým je Úřad pro ochranu osobních údajů, se sídlem Pplk. Sochora 27, 170 00 Praha 7.</p>
<p>4.	Kontaktní údaje pověřence pro ochranu osobních údajů správce údajů jsou: 
o	Zdenek Pokorny, email: z.pokorny@paypipes.com, tel: +420607803355</p>
<p>5.	Berete na vědomí, že OS pravidla obsahují všechny informace poskytované subjektu údajů dle čl. 13 Nařízení GDPR.</p>
<h3>Závěrečná ustanovení</h3>
<p>1.	Pokračováním využívání funkcí Platebního systému PayPipes vyjadřujete svůj svobodný, konkrétní, informovaný a jednoznačný projev vůle o tom, že jste srozuměn/a se zpracováním Vašich osobních údajů podle těchto OS pravidel a že jste byl/a o zpracování Vašich osobních údajů řádně poučen/a a informován/a.</p>
<p>2.	Jsme oprávněni jednostranně měnit tato OS pravidla podle platné legislativy a Vy souhlasíte s tímto oprávněním.</p>
<p>3.	Změníme-li OS pravidla, jsme povinni Vás o této změně vyrozumět s předstihem formou e-mailové zprávy, jenž bude obsahovat odkaz na nová OS pravidla, odkud si je můžete vytisknout či uložit v elektronické podobě.</p>
<p>4.	OS pravidla jsou vydána v elektronické podobě a jsou přístupná na webových stránkách společnosti PayPipes.</p>
<p>5.	OS pravidla nabývají účinnosti dnem 24.05.2018.</p>
<!-- END PAGE CONTENT-->
@stop