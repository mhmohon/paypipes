@extends('account/html')

@section('pageheader')
    @include('account/modules/wallet/includes/pageheader')
@stop
@section('content')
<!-- BEGIN PAGE CONTENT	-->
<div class="row">
    <div class="col-md-4">
		@if (count($wallets) == 0)
		<h4>+Wallet Account</h4>
		<div class="font-grey-silver margin-bottom-10">Add +Wallet Account to send and receive payments</div>
		<a class="btn blue-madison btn-block btn-outline margin-bottom-10" href="/account#addwallet">Add +Wallet Account</a>
		@else
        <h4>+Wallet Balance</h4>
        <div class="font-grey-silver">You don't need a balance to pay or request payments</div>
        <div class="table margin-top-10">
            <table class="table table-hover table-light">
                <thead>
                    <tr>
                        <th></th>
                        <th class="text-right" style="width:1%">Current</br>Balance</th>
                        <th class="text-right" style="width:1%">Available</br>Balance</th>
                    </tr>
                </thead>
                <tbody>
					@foreach ($wallets as $wallet)
					<tr>
                        <td>@if ($wallet->primary === '1') <a class="label label-sm label-default" href="/account#primary">Primary</span> @else @endif</td>
                        <td class="text-right"><span class="font-green-jungle" style="font-size:22px;font-weight:300;">{!! $wallet->balance !!}</span></br> <span class="font-sm">{!! $wallet->currency !!}</span></td>
                        <td class="text-right"><span class="font-green-jungle" style="font-size:22px;font-weight:300;">{!! $wallet->balance !!}</span></br> <span class="font-sm">{!! $wallet->currency !!}</span></td>
                    </tr>
					@endforeach
					<!--
					<tr>
                        <td colspan="2"><b>Total available in EUR</b></td>                    
                        <td class="text-right"><span class="font-green-jungle" style="font-size:22px;font-weight:300;">155,50</span></br> <span class="font-sm">EUR</span></td>
                    </tr>
					-->
                </tbody>
            </table>
        </div>
		@endif
    </div>
    <div class="col-md-8">
		<div class="portlet">
			<div class="portlet-body">
				<div class="tabbable-custom tabbable-tabdrop">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#Cards" data-toggle="tab">Linked Cards</a>
						</li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active">
							<a class="btn blue-madison btn-block btn-outline" href="#addcard" data-toggle="modal">Add new Card to your +Wallet</a>
							<div class="modal fade" id="addcard" tabindex="-1" role="dialog" aria-hidden="true">
								<div class="modal-dialog">
									<div class="modal-content">
										<div class="modal-header">
											<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
											<h4 class="modal-title">Add new Card to your +Wallet</h4>
										</div>
										<div class="modal-body"> 
											{!! Form::model(['method' => 'POST', 'action' => ['Account\Modules\Wallet\WalletController@store']]) !!}
												<div class="form-body">
													<h4>Card Information</h4>
													<div class="form-group @if ($errors->has('card_holder')) has-error @endif">
														{!! Form::text('card_holder', null, ['class' => 'form-control', 'placeholder' => 'Name (as it appears on your card)']) !!}
														@if ($errors->has('card_holder')) <p class="help-block">{{ $errors->first('card_holder') }}</p> @endif
													</div>
													<div class="form-group @if ($errors->has('card_number')) has-error @endif">
														{!! Form::text('card_number', null, ['class' => 'form-control', 'placeholder' => 'Card Number','maxlength' => 16]) !!}
														@if ($errors->has('card_number')) <p class="help-block">{{ $errors->first('card_number') }}</p> @endif
													</div>
													<div class="form-group">
														<div class="row">
															<div class="col-md-6">
																<div class="@if ($errors->has('exp_month')) has-error @endif">
																{!! Form::select('exp_month', $exp_month, null, ['class' => 'bs-select form-control', 'data-live-search' => '']) !!}
																	@if ($errors->has('exp_month')) <p class="help-block">{{ $errors->first('exp_month') }}</p> @endif
																</div>
															</div>
															<div class="col-md-6">
																<div class="@if ($errors->has('exp_year')) has-error @endif">
																{!! Form::select('exp_year', $exp_year, null, ['class' => 'bs-select form-control', 'data-live-search' => '']) !!}
																	@if ($errors->has('exp_year')) <p class="help-block">{{ $errors->first('exp_year') }}</p> @endif
																</div>
															</div>
														</div>
													</div>				
													<h4>Billing Address</h4>
													<div class="form-group @if ($errors->has('street')) has-error @endif">
														{!! Form::text('street', null, ['class' => 'form-control', 'placeholder' => 'Street']) !!} 
														@if ($errors->has('street')) <p class="help-block">{{ $errors->first('street') }}</p> @endif
													</div>
													<div class="form-group @if ($errors->has('city')) has-error @endif">
														{!! Form::text('city', null, ['class' => 'form-control', 'placeholder' => 'City',]) !!}
														@if ($errors->has('city')) <p class="help-block">{{ $errors->first('city') }}</p> @endif
													</div>
													<div class="form-group">
														<div class="row">
															<div class="col-md-6">
																<div class="@if ($errors->has('country')) has-error @endif">
																	{!! Form::select('country', $countries, null, ['class' => 'bs-select form-control', 'data-live-search' => '']) !!}
																	@if ($errors->has('country')) <p class="help-block">{{ $errors->first('country') }}</p> @endif
																</div>
															</div>
															<div class="col-md-6">
																<div class="@if ($errors->has('post_code')) has-error @endif">
																	{!! Form::text('post_code', null, ['class' => 'form-control', 'placeholder' => 'Post Code']) !!}
																	@if ($errors->has('post_code')) <p class="help-block">{{ $errors->first('post_code') }}</p> @endif
																</div>
															</div>
														</div>
													</div>
													<div class="form-group">
														<span class="input-group-btn">
															{!! Form::submit('Add', ['class' => 'btn btn-block green-jungle btn-lg'] ) !!}
														</span>
													</div>
												</div>
											{!! Form::close() !!} 
										</div>
									</div>
									<!-- /.modal-content -->
								</div>
								<!-- /.modal-dialog -->
							</div>
							<!-- /.modal -->
							@if (count($linked_cards) == 0)
							@else
							<div class="table-scrollable">
								<table class="table table-light table-hover table-bordered table-striped">
									<tbody>  
										@foreach ($linked_cards as $linked_card)
										<tr data-href="javascript:void(0);">
											<td style="width:1%">@if ($linked_card->primary === '1') <a class="label label-sm label-default" href="#primary" data-toggle="modal">Primary <i class="fa fa-pencil"></i></span> @else @endif</td>
											<td style="width:1%"><img src="/assets/global/img/payments/active/{{ $linked_card->file_name }}"></td>
											<td><span style="font-size:18px;font-weight:300;">{{ $linked_card->card_type }}*{{ substr($linked_card->card_number,-4) }}</span></br>{{ $linked_card->card_holder }}</td>
											<td style="width:1%" class="text-center"><span style="font-size:18px;font-weight:300;">{{ $linked_card->exp_month }}/{{ $linked_card->exp_year }}</span></br>Exp.</td>
											<td style="width:1%"><span class="label label-sm label-success">{{ $linked_card->status }}</span></td>
											<td style="width:1%">
												<a href="/account/wallet/{!! $linked_card->linked_card_id !!}" class="btn btn-xs blue-madison"><i class="fa fa-pencil"></i></a>
											</td>
										</tr> 
										@endforeach
									</tbody>
								</table>
							</div>	
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
<!-- END PAGE CONTENT-->
@stop