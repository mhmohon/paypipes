@extends('account/html')

@section('pageheader')
    @include('account/modules/payments/includes/pageheader')
@stop
@section('content')
<!-- BEGIN PAGE CONTENT	-->
@if (count($payments) == 0)
<div class="row">
	<div class="col-sm-6">
		{!! Form::open(['action' => 'Account\Modules\Payments\PaymentsController@index', 'method' => 'GET']) !!}
			<div class="input-group input-medium">
				{!! Form::text('search', Request::get('search'), ['class' => 'form-control input-medium', 'placeholder' => 'Search']) !!}
				<span class="input-group-btn">
					{!! Form::button('<i class="icon-magnifier"></i>', ['class' => 'btn green-jungle', 'type'=>'submit']) !!}
				</span>
			</div>
		{!! Form::close() !!}
	</div>
</div>
<p class="text-center font-grey-silver margin-top-15">There are no payments to display</p>
@else
<div class="row">
	<div class="col-sm-6">
		{!! Form::open(['action' => 'Account\Modules\Payments\PaymentsController@index', 'method' => 'GET']) !!}
			<div class="input-group input-medium">
				{!! Form::text('search', Request::get('search'), ['class' => 'form-control input-medium', 'placeholder' => 'Search']) !!}
				<span class="input-group-btn">
					{!! Form::button('<i class="icon-magnifier"></i>', ['class' => 'btn green-jungle', 'type'=>'submit']) !!}
				</span>
			</div>
		{!! Form::close() !!}
	</div>
</div>
<div class="table-scrollable">
	<table class="table table-light table-bordered table-striped table-hover">
		<thead>
			<tr>
				<th style="width:1%">Date</th>
				<th>Description</th>
				<th style="width:1%">Method</br> Type</th>
				<th class="text-right" style="width:1%">Amount</th>
				<th style="width:1%">Status</th>
			</tr>
		</thead>
		<tbody>
			@foreach ($payments as $payment)
			<tr class='clickable-row' data-href='/account/payments/{!! $payment->payment_id !!}'>
				<td ><span class="font-sm uppercase">{!! Carbon\Carbon::parse($payment->created_at)->format('M') !!}</span> </br> <span style="font-size:22px;font-weight:300;">{!! Carbon\Carbon::parse($payment->created_at)->format('d') !!}</span></td>
				<td>From {!! $payment->from !!} to {!! $payment->to !!}</br><span style="font-size:18px;font-weight:300;">{!! $payment->description !!}</span></td>
				<td>{!! $payment->payment_method !!}</br>{!! $payment->payment_type !!}</td>
				<td class="text-right">
					<span style="font-size:22px;font-weight:300;" class="@if ($payment->payment_type =='TransferOut') font-red-thunderbird @elseif ($payment->payment_type =='TransferIn')  font-green-jungle @elseif($payment->to === Auth::guard('account')->user()->email) font-green-jungle @elseif ($payment->from === Auth::guard('account')->user()->email) font-red-thunderbird @else @endif">
						@if ($payment->payment_type =='TransferOut') -@elseif ($payment->payment_type =='TransferIn')  +@elseif ($payment->to === Auth::guard('account')->user()->email) +@elseif ($payment->from === Auth::guard('account')->user()->email) -@else @endif{!! $payment->amount !!}
					</span> </br>
					<span class="font-sm">{!! $payment->currency !!}</span>
				</td>
				<td><span class="label label-sm @if ($payment->status === 'Approved') label-success @elseif ($payment->status === 'Pending') label-info @elseif ($payment->status === 'Requested') label-info @elseif ($payment->status === 'Refunded') label-purple @elseif ($payment->status === 'Canceled') label-warning @elseif ($payment->status === 'Chargeback') label-black @elseif ($payment->status === 'Declined') label-danger @else label-default @endif">{!! $payment->status !!}</span></td>
			</tr>
			@endforeach
		</tbody>
	</table>
</div>
<div class="row">
	<div class="col-md-6 margin-top-10">
		Showing {!! $payments->firstItem() !!} to {!! $payments->lastItem() !!} of {!! $payments->total() !!}
	</div>
	<div class="col-md-6 text-right">
		{{ $payments->appends(Request::except('page'))->links() }}
	</div>
</div>
@endif
<!-- END PAGE CONTENT-->
@stop
