@extends('account/html')

@section('pageheader')
    @include('account/modules/payments/includes/pageheader')
@stop
@section('content')
<!-- BEGIN PAGE CONTENT	-->
@if ($payment->status === 'Pending' && $payment->from === $account->email && $payment->payment_type === 'Request')
<div class="note note-info">
    <div style="float:left" class="font-grey-gallery">
        Payment request of {!! $payment->amount !!} {!! $payment->currency !!} for {!! $payment->description !!} from {!! $payment->added_by !!} ({!! $payment->to !!})
        {!! Form::open(['method' => 'POST', 'action' => ['Account\Modules\Payments\PaymentsController@pay', $payment->payment_id], 'style' => 'display:inline-block;']) !!}
            {!! Form::hidden('amount', $payment->total_amount) !!}
            {!! Form::submit('Approve', ['name' => 'pay', 'class' => 'btn green-jungle btn-xs', 'data-toggle' => 'confirmation', 'data-popout' => 'true', 'data-singleton' => 'true', 'data-original-title' => 'Approve! Are you sure?', 'title' => '']) !!}
        {!! Form::close() !!}
        {!! Form::open(['method' => 'POST', 'action' => ['Account\Modules\Payments\PaymentsController@pay', $payment->payment_id], 'style' => 'display:inline-block;']) !!}
            {!! Form::hidden('amount', $payment->total_amount) !!}
            {!! Form::submit('Decline', ['name' => 'decline', 'class' => 'btn red btn-xs', 'data-toggle' => 'confirmation', 'data-popout' => 'true', 'data-singleton' => 'true', 'data-original-title' => 'Decline! Are you sure?', 'title' => '']) !!}
        {!! Form::close() !!}
    </div>
    <div style="float:right"><cite class="font-grey-silver">{!! Carbon\Carbon::parse($payment->created_at)->diffForHumans() !!}</cite></div>
    <div style="clear:both"></div>
</div>
@else
@endif
<div class="row">
    <div class="col-sm-4">
		<h4> From: </h4>
		<h4> {!! $payment->from !!} </h4>
		<h4> {!! $from->website or '' !!} </h4>
		<h4> {!! $from->first_name or '' !!} {!! $from->last_name or '' !!}</h4>
		<h4> {!! $from->phone !!} </h4>
		<h4> {!! $from->street or '' !!}</h4>
		<h4> {!! $from->city or '' !!}</h4>
		<h4> {!! $from->post_code or '' !!}</h4>
		<h4> {!! $from->state or '' !!} </h4>
		<h4> {!! $from->country or '' !!} </h4>
	</div>
	<div class="col-sm-4">
		<h4> To: </h4>
		<h4> {!! $payment->to !!} </h4>
		<h4> {!! $to->website or '' !!} </h4>
		<h4> {!! $to->first_name or '' !!} {!! $to->last_name or '' !!}</h4>
		<h4> {!! $to->phone !!} </h4>
		<h4> {!! $to->street or '' !!}</h4>
		<h4> {!! $to->city or '' !!}</h4>
		<h4> {!! $to->post_code or '' !!}</h4>
		<h4> {!! $to->state or '' !!} </h4>
		<h4> {!! $to->country or '' !!} </h4>
    </div>
    <div class="col-sm-4 text-right">
        <h4 style="margin-bottom:0px">{!! Carbon\Carbon::parse($payment->created_at)->format('M d, Y') !!} at {!! Carbon\Carbon::parse($payment->created_at)->format('H:i:s e') !!}</h4>
        <div class="font-grey-silver">Date & Time</div>
        <h4 style="margin-bottom:0px">{!! $payment->payment_id !!}</h4>
        <div class="font-grey-silver">Payment ID</div>
        <h4 style="margin-bottom:0px">{!! $payment->payment_method !!} {!! $payment->payment_type !!}</h4>
        <div class="font-grey-silver">Method & Type</div>
        <h4 style="margin-bottom:0px">{!! $payment->reference_id?? '' !!}</h4>
        <div class="font-grey-silver">Reference ID</div>
		<div class="margin-top-10"><span class="label label-sm @if ($payment->status === 'Approved') label-success @elseif ($payment->status === 'Pending') label-info @elseif ($payment->status === 'Requested') label-info @elseif ($payment->status === 'Refunded') label-purple @elseif ($payment->status === 'Canceled') label-warning @elseif ($payment->status === 'Chargeback') label-black @elseif ($payment->status === 'Declined') label-danger @else label-default @endif">{!! $payment->status !!}</span></div>
		<div class="font-grey-silver">Payment Status</div>
    </div>
</div>
<div class="table-scrollable">
    <table class="table table-hover table-light">
        <thead>
            <tr>
                <th>Description</th>
                <th style="width:1%"></th>
                <th class="text-right" style="width:1%">Amount</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><span style="font-size:18px;font-weight:300;">{!! $payment->description !!}</span></td>
                <td></td>
                <td class="text-right" style="white-space:nowrap"><span style="font-size:18px;font-weight:300;">{!! $payment->amount !!}</span> <span style="font-size:18px;font-weight:300;">{!! $payment->currency !!}</span></td>
            </tr>
            <tr>
                <td></td>
                <td><span style="font-size:18px;font-weight:300;">Fee</span></td>
                <td class="text-right" style="white-space:nowrap"><span style="font-size:18px;font-weight:300;">{!! $payment->fee !!} {!! $payment->currency !!}</span></td>
            </tr>
            <tr>
                <td></td>
                <td><span style="font-size:18px;font-weight:300;">Total</span></td>
                <td class="text-right" style="white-space:nowrap">
                    <!--
					@if ($payment->to === $account->email && $payment->payment_type =='TransferIn')
					<span class="font-green-jungle" style="font-size:18px;font-weight:300;">+{!! $payment->total_amount !!}</span>
					@elseif ($payment->from === $account->email && $payment->payment_type =='TransferOut')
					<span class="font-red" style="font-size:18px;font-weight:300;">-{!! $payment->total_amount !!}</span>
					@elseif ($payment->to === $account->email)
					<span class="font-green-jungle" style="font-size:18px;font-weight:300;">+{!! $payment->total_amount !!}</span>
					@elseif ($payment->from === $account->email)
					<span class="font-red" style="font-size:18px;font-weight:300;">-{!! $payment->total_amount !!}</span>
					@else
                    <span class="font-red" style="font-size:18px;font-weight:300;">-{!! $payment->total_amount !!}</span>
					@endif//-->
                    <span  style="font-size:18px;font-weight:300;">{!! $payment->total_amount !!}</span>
					<span style="font-size:18px;font-weight:300;">{!! $payment->currency !!}</span>
				</td>
            </tr>
        </tbody>
    </table>
</div>
<!--
<div class="margin-bottom-10 margin-top-15" style="border-bottom:1px solid #F2F5F8;"></div>
<div class="font-grey-silver">
    Need help? First contact the seller through the contact details above to resolve the problem. If it's still not resolved, open a dispute <a href="#">here</a>.
</div>
-->
<!-- END PAGE CONTENT-->
@stop
