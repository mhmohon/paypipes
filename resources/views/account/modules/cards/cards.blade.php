@extends('account/html')
@section('pageheader')
    @include('account/modules/cards/includes/pageheader')
@stop
@section('content')
<!-- BEGIN PAGE CONTENT	-->
<div class="tiles">

	<a class="tile bg-blue-madison" data-toggle="modal" href="#addcard">
		<div class="tile-body">
			<h4 class="text-center">Add new Card</h4>
			<div class="text-center margin-top-20"><span style="font-size:64px;font-weight:300;">+</span></div>
			<div class="tile-object">
				<div class="name"></div>
				<div class="number"></div>
			</div>
		</div>
	</a>
	<div class="modal fade" id="addcard" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
					<h4 class="modal-title">Add new Card to your +Wallet</h4>
				</div>
				<div class="modal-body">
					 {!! Form::open(['url' => 'account/cards']) !!}
						<div class="form-body">
							<h4>Card Information</h4>
							<div class="form-group @if ($errors->has('card_holder')) has-error @endif">
								{!! Form::text('card_holder', null, ['class' => 'form-control', 'placeholder' => 'Name (as it appears on your card)']) !!}
								@if ($errors->has('card_holder')) <p class="help-block">{{ $errors->first('card_holder') }}</p> @endif
							</div>
							<div class="form-group @if ($errors->has('card_number')) has-error @endif">
								{!! Form::text('card_number', null, ['class' => 'form-control', 'placeholder' => 'Card Number','maxlength' => 16]) !!}
								@if ($errors->has('card_number')) <p class="help-block">{{ $errors->first('card_number') }}</p> @endif
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-6">
										<div class="@if ($errors->has('exp_month')) has-error @endif">
										{!! Form::select('exp_month', $exp_month, null, ['class' => 'bs-select form-control', 'data-live-search' => '']) !!}
											@if ($errors->has('exp_month')) <p class="help-block">{{ $errors->first('exp_month') }}</p> @endif
										</div>
									</div>
									<div class="col-md-6">
										<div class="@if ($errors->has('exp_year')) has-error @endif">
										{!! Form::select('exp_year', $exp_year, null, ['class' => 'bs-select form-control', 'data-live-search' => '']) !!}
											@if ($errors->has('exp_year')) <p class="help-block">{{ $errors->first('exp_year') }}</p> @endif
										</div>
									</div>
								</div>
							</div>
							<h4>Billing Address</h4>
							<div class="form-group @if ($errors->has('street')) has-error @endif">
								{!! Form::text('street', null, ['class' => 'form-control', 'placeholder' => 'Street']) !!}
								@if ($errors->has('street')) <p class="help-block">{{ $errors->first('street') }}</p> @endif
							</div>
							<div class="form-group @if ($errors->has('city')) has-error @endif">
								{!! Form::text('city', null, ['class' => 'form-control', 'placeholder' => 'City',]) !!}
								@if ($errors->has('city')) <p class="help-block">{{ $errors->first('city') }}</p> @endif
							</div>
							<div class="form-group">
								<div class="row">
									<div class="col-md-6">
										<div class="@if ($errors->has('country')) has-error @endif">
											{!! Form::select('country', $countries, null, ['class' => 'bs-select form-control', 'data-live-search' => '']) !!}
											@if ($errors->has('country')) <p class="help-block">{{ $errors->first('country') }}</p> @endif
										</div>
									</div>
									<div class="col-md-6">
										<div class="@if ($errors->has('post_code')) has-error @endif">
											{!! Form::text('post_code', null, ['class' => 'form-control', 'placeholder' => 'Post Code']) !!}
											@if ($errors->has('post_code')) <p class="help-block">{{ $errors->first('post_code') }}</p> @endif
										</div>
									</div>
								</div>
							</div>
							<div class="form-group">
								<span class="input-group-btn">
									{!! Form::submit('Add', ['class' => 'btn btn-block green-jungle btn-lg'] ) !!}
								</span>
							</div>
						</div>
					{!! Form::close() !!}
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	@if (count($linked_cards) == 0)
	@else
	@foreach ($linked_cards as $linked_card)
	<a class="tile bg-green-jungle double" href="/account/cards/{!! $linked_card->linked_card_id !!}">
		<div class="tile-body">
			<div class="text-right"><img src="/assets/global/img/payments/active/{{ $linked_card->file_name }}"></div>
			<div style="margin-top:-60px;"><span style="font-size:22px;font-weight:300;">{!! $linked_card->card_type !!}*{!! $linked_card->card_digit !!}</span></div>
			<div class="tile-object">
				<div class="name">{!! $linked_card->card_holder !!}</div>
				<div class="number">{!! $linked_card->exp_month !!}/{!! $linked_card->exp_year !!}</div>
			</div>
		</div>
	</a>
	@endforeach
	@endif

</div>
<!-- END PAGE CONTENT-->
@stop
