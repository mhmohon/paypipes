@extends('account/html')

@section('pageheader')
    @include('account/modules/cards/includes/pageheader')
@stop
@section('content')
<!-- BEGIN PAGE CONTENT	-->
{!! Form::model($linked_card,['method' => 'PATCH', 'action' => ['Account\Modules\Cards\CardsController@update', $linked_card->linked_card_id]]) !!}
	<div class="form-body">
		<div class="form-group @if ($errors->has('card_holder')) has-error @endif">
			{!! Form::text('card_holder', null, ['class' => 'form-control', 'readonly' => '', 'placeholder' => $linked_card->card_holder ]) !!}
			@if ($errors->has('card_holder')) <p class="help-block">{{ $errors->first('card_holder') }}</p> @endif
		</div>
		<div class="form-group @if ($errors->has('card_number')) has-error @endif">
			{!! Form::text('card_number', null, ['class' => 'form-control', 'readonly' => '', 'placeholder' => $linked_card->card_number ]) !!}
			@if ($errors->has('card_number')) <p class="help-block">{{ $errors->first('card_number') }}</p> @endif
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-6">
					<div class="@if ($errors->has('exp_month')) has-error @endif">
					{!! Form::select('exp_month', $exp_month, $linked_card->exp_month, ['class' => 'bs-select form-control', 'data-live-search' => '']) !!}
						@if ($errors->has('exp_month')) <p class="help-block">{{ $errors->first('exp_month') }}</p> @endif
					</div>
				</div>
				<div class="col-md-6">
					<div class="@if ($errors->has('exp_year')) has-error @endif">
					{!! Form::select('exp_year', $exp_year, $linked_card->exp_year, ['class' => 'bs-select form-control', 'data-live-search' => '']) !!}
						@if ($errors->has('exp_year')) <p class="help-block">{{ $errors->first('exp_year') }}</p> @endif
					</div>
				</div>
			</div>
		</div>
		<h4>Billing Address</h4>
		<div class="form-group @if ($errors->has('street')) has-error @endif">
			{!! Form::text('street', null, ['class' => 'form-control', 'placeholder' => $linked_card->street]) !!} 
			@if ($errors->has('street')) <p class="help-block">{{ $errors->first('street') }}</p> @endif
		</div>
		<div class="form-group @if ($errors->has('city')) has-error @endif">
			{!! Form::text('city', null, ['class' => 'form-control', 'placeholder' => $linked_card->city]) !!}
			@if ($errors->has('city')) <p class="help-block">{{ $errors->first('city') }}</p> @endif
		</div>
		<div class="form-group">
			<div class="row">
				<div class="col-md-6">
					<div class="@if ($errors->has('country')) has-error @endif">
						{!! Form::select('country', $countries, null, ['class' => 'bs-select form-control', 'data-live-search' => '']) !!}
						@if ($errors->has('country')) <p class="help-block">{{ $errors->first('country') }}</p> @endif
					</div>
				</div>
				<div class="col-md-6">
					<div class="@if ($errors->has('post_code')) has-error @endif">
						{!! Form::text('post_code', null, ['class' => 'form-control', 'placeholder' => $linked_card->post_code]) !!}
						@if ($errors->has('post_code')) <p class="help-block">{{ $errors->first('post_code') }}</p> @endif
					</div>
				</div>
			</div>
		</div>
		<div class="form-group">
			<span class="input-group-btn">
				{!! Form::submit('Update', ['class' => 'btn btn-block green-jungle btn-lg', 'name' => 'update'] ) !!}
			</span>
		</div>
	</div>
{!! Form::close() !!} 
<!-- END PAGE CONTENT-->
@stop