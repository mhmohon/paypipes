	<!-- BEGIN FOOTER -->
        <!-- BEGIN INNER FOOTER -->
        <div class="page-footer">
            <div class="container-fluid">
                <div style="float:left;">&copy; <?php echo date("Y") ?> {{ config('global.company_legal') }} All rights reserved.</div>
               <div style="float:right;"><a href="{{ config('global.terms_url') }}" target="_blank">Terms & Conditions</a> | <a href="{{ config('global.privacy_url') }}"  target="_blank">Privacy</a></div>
            </div>
        </div>
        <div class="scroll-to-top">
            <i class="icon-arrow-up"></i>
        </div>
        <!-- END INNER FOOTER -->
        <!-- END FOOTER -->
