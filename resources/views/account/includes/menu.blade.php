<!-- BEGIN HEADER MENU -->
<div class="page-header-menu">
    <div class="container-fluid">
        <!-- BEGIN MEGA MENU -->
        <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
        <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
        <div class="hor-menu hor-menu-light">
            <ul class="nav navbar-nav">

                <li class="menu-dropdown mega-menu-dropdown
                    {{ Request::is('account') ? 'active' : '' }}">
                    <a href="/account"> Summary
                        <span class="../arrow"></span>
                    </a>
                </li>
                <!--
                <li class="menu-dropdown mega-menu-dropdown
                    {{ Request::is('account/accounts') ? 'active' : '' }}
                    {{ Request::is('account/accounts/*') ? 'active' : '' }}">
                    <a href="/account/accounts">  Accounts
                        <span class="../arrow"></span>
                    </a>
                </li>//-->

                <li class="menu-dropdown mega-menu-dropdown
                    {{ Request::is('account/payments') ? 'active' : '' }}
                    {{ Request::is('account/payments/*') ? 'active' : '' }}">
                    <a href="/account/payments"> Payments
                        <span class="../arrow"></span>
                    </a>
                </li>


                <li class="menu-dropdown mega-menu-dropdown
                    {{ Request::is('account/statements') ? 'active' : '' }}
                    {{ Request::is('account/statements/*') ? 'active' : '' }}">
                    <a href="/account/statements"> Statements
                        <span class="../arrow"></span>
                    </a>
                </li>

                <li class="menu-dropdown mega-menu-dropdown {{ Request::is('account/cards') ? 'active' : '' }}">
                    <a href="/account/cards">Cards
                        <span class="../arrow"></span>
                    </a>
                </li>

            </ul>
        </div>
        <!-- END MEGA MENU -->
    </div>
</div>
<!-- END HEADER MENU -->
