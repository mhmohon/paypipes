<!-- BEGIN HEADER TOP -->
<div class="page-header-top">
    <div class="container-fluid">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="/account">
               <img src="/img/{{ config('global.logo') }}" alt="logo" class="logo-default" style="margin-top:10px; height:50px;">
            </a>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler"></a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
				<!-- BEGIN NOTIFICATION DROPDOWN -->
				<!--
				<li class="dropdown" id="header_notification_bar">
					<a href="/account/notifications" class="dropdown-toggle">
						<i class="icon-bell"></i>
						<span class="badge badge-default">7</span>
					</a>
				</li>
				<!-- END NOTIFICATION DROPDOWN -->
				<!-- BEGIN INBOX DROPDOWN -->
				<!--
				<li class="dropdown dropdown-extended dropdown-tasks dropdown-light" id="header_task_bar">
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<i class="icon-envelope-open"></i>
						<span class="badge badge-default">3</span>
					</a>
					<ul class="dropdown-menu">
                        <li class="external">
                            <h3>You have
                                <strong>3 New</strong> Messages</h3>
                            <a href="pp_inbox.php">view all</a>
                        </li>
                        <li>
                            <ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
                                <li>
                                    <a href="#">
                                        <span class="photo">
                                            <img src="../assets/layouts/layout3/img/avatar2.jpg" class="img-circle" alt=""> </span>
                                        <span class="subject">
                                            <span class="from"> Lisa Wong </span>
                                            <span class="time">Just Now </span>
                                        </span>
                                        <span class="message"> Vivamus sed auctor nibh congue nibh. auctor nibh auctor nibh... </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="photo">
                                            <img src="../assets/layouts/layout3/img/avatar3.jpg" class="img-circle" alt=""> </span>
                                        <span class="subject">
                                            <span class="from"> Richard Doe </span>
                                            <span class="time">16 mins </span>
                                        </span>
                                        <span class="message"> Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="photo">
                                            <img src="../assets/layouts/layout3/img/avatar1.jpg" class="img-circle" alt=""> </span>
                                        <span class="subject">
                                            <span class="from"> Bob Nilson </span>
                                            <span class="time">2 hrs </span>
                                        </span>
                                        <span class="message"> Vivamus sed nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="photo">
                                            <img src="../assets/layouts/layout3/img/avatar2.jpg" class="img-circle" alt=""> </span>
                                        <span class="subject">
                                            <span class="from"> Lisa Wong </span>
                                            <span class="time">40 mins </span>
                                        </span>
                                        <span class="message"> Vivamus sed auctor 40% nibh congue nibh... </span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#">
                                        <span class="photo">
                                            <img src="../assets/layouts/layout3/img/avatar3.jpg" class="img-circle" alt=""> </span>
                                        <span class="subject">
                                            <span class="from"> Richard Doe </span>
                                            <span class="time">46 mins </span>
                                        </span>
                                        <span class="message"> Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
				</li>
				-->
				<!-- END INBOX DROPDOWN -->
				<!-- BEGIN TOP ICONS -->
				<li class="dropdown" id="header_task_bar">
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
						<i class="icon-settings @if (Request::is('account/settings/*')) font-blue-madison @elseif (Request::is('account/activities')) font-blue-madison @else @endif""></i>
					</a>
					<ul class="dropdown-menu dropdown-menu-default">
                        <li>
                            <a href="/account/settings/account">
                                 Account
                            </a>
                        </li>
						<li>
                            <a href="/account/settings/security">
                                Security
							</a>
                        </li>

						<li>
                            <a href="/account/settings/notifications">
                                Notifications
							</a>
                        </li>
                        <li>
                            <a href="/account/settings/limits">
								Limits
							</a>
                        </li>
						<li>
                            <a href="/account/settings/compliances">
                                Compliance
							</a>
                        </li>

						<li class="divider"> </li>
						<li>
                            <a href="/account/activities">
                                Activities
							</a>
                        </li>
                        <li class="divider"> </li>
                        <li>
                            <a href="/account/usermanagement/users">
                                User Management
                            </a>
                        </li>
                    </ul>
				</li>
				<li class="droddown dropdown-separator">
					<span class="separator"></span>
				</li>
				<!-- END TOP ICONS -->
                <!-- BEGIN USER LOGIN DROPDOWN -->
                <li class="dropdown dropdown-user dropdown-light">
                    <a href="/account/settings/account" class="dropdown-toggle">
                        <img alt="" class="img-circle" src="/assets/layouts/layout3/img/avatar.png">
                        <span class="username username-hide-mobile"> {!! Auth::guard('account')->user()->first_name !!} {!! Auth::guard('account')->user()->last_name !!}</span>
                    </a>
					<!--
                    <ul class="dropdown-menu dropdown-menu-default">
                        <li>
                            <a href="pp_inbox.php">
                                <i class="icon-envelope-open"></i> Inbox
                                <span class="badge badge-danger"> 3 </span>
                            </a>
                        </li>
						<li>
                            <a href="/account/settings">
                                <i class="fa fa-book"></i>Activity Log </a>
                        </li>
						<li>
                            <a href="/account/settings">
                                <i class="icon-settings"></i>Settings </a>
                        </li>
                        <li class="divider"> </li>
                        <li>
                            <a href="{{ url('/account/logout') }}">
								<i class="icon-key"></i> Log Out
							</a>
                        </li>
                    </ul>
					-->
                </li>
                <!-- END USER LOGIN DROPDOWN -->
				<li class="dropdown" id="header_task_bar">
					<a href="/account/logout" class="dropdown-toggle popovers" data-container="body" data-trigger="hover" data-placement="bottom" data-content="LogOut">
						<i class="icon-logout"></i>
					</a>
				</li>
            </ul>
        </div>
        <!-- END TOP NAVIGATION MENU -->
    </div>
</div>
<!-- END HEADER TOP -->
