<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Log In Failed. Please Check Your Email/Password.',
    'throttle' => 'Your account Temporary Locked. Please try again after :seconds min.',

];
