<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Redirect::to(config('global.front_url'));
});


Route::group(['prefix' => 'admin'], function () {
    /*Route::get('/login', 'AdminAuth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'AdminAuth\LoginController@login');
    Route::post('/logout', 'AdminAuth\LoginController@logout')->name('logout');

    Route::get('/register', 'AdminAuth\RegisterController@showRegistrationForm')->name('register');
    Route::post('/register', 'AdminAuth\RegisterController@register');

    Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
    Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset')->name('password.email');
    Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
    Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');*/

    Route::get('/password/setup/{token}', 'AdminAuth\LoginController@setup_password');
    Route::post('/password/setup', 'AdminAuth\LoginController@setup_password');
    Route::get('/verify-email/{code}', 'AdminAuth\LoginController@email_verify');

    // //Admin Login
    Route::get('/login', 'AdminAuth\LoginController@showLoginForm');
    Route::post('/login', 'AdminAuth\LoginController@login');
    Route::get('/logout', 'AdminAuth\LoginController@logout');
    // Route::get('/register', 'AdminAuth\RegisterController@showRegistrationForm');
    // Route::post('/register', 'AdminAuth\RegisterController@register');

    //Admin Passwords
    Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset');
    Route::get('/password/change', 'AdminAuth\ResetPasswordController@changeView');
    Route::post('/password/change', 'AdminAuth\ResetPasswordController@change');
    Route::get('/password/two-step', 'AdminAuth\LoginController@twoStepView');
    Route::post('/password/two-step', 'AdminAuth\LoginController@twoStep');
    Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm');
    Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');

    Route::get('', 'Admin\Modules\Dashboard\DashboardController@index');

    Route::get('/activity', 'Admin\Modules\Activitylog\ActivityLoggerController@index');
    Route::get('/activity/log/{id}', 'Admin\Modules\Activitylog\ActivityLoggerController@view');
    Route::delete('/activity/log/{id}', 'Admin\Modules\Activitylog\ActivityLoggerController@destroy');
    Route::get('/activity/cleared', 'Admin\Modules\Activitylog\ActivityLoggerController@destroyAllRecords');


    Route::get('/apiactivity', 'Admin\Modules\Activitylog\ActivityApiLoggerController@index');
    Route::get('/apiactivity/log/{id}', 'Admin\Modules\Activitylog\ActivityApiLoggerController@view');
    Route::delete('/apiactivity/log/{id}', 'Admin\Modules\Activitylog\ActivityApiLoggerController@destroy');
    Route::get('/apiactivity/cleared', 'Admin\Modules\Activitylog\ActivityApiLoggerController@destroyAllRecords');


    Route::group(['middleware' => ['web', 'activity', 'admin', 'auth:admin']], function () {
        Route::get('/accounts', 'Admin\Modules\Accounts\AccountsController@index');
        Route::post('/accounts', 'Admin\Modules\Accounts\AccountsController@store');
        Route::get('/accounts/{account_id}', 'Admin\Modules\Accounts\AccountsController@view');
        Route::post('/accounts/{account_id}', 'Admin\Modules\Accounts\AccountsController@update');
        Route::patch('/accounts/{account_id}', 'Admin\Modules\Accounts\AccountsController@lockunlock');
        Route::get('/accounts/{account_id}/compliance', 'Admin\Modules\Accounts\AccountsController@editcompliance');
        Route::post('/accounts/{account_id}/compliance', 'Admin\Modules\Accounts\AccountsController@updatecompliance');
        Route::get('/accounts/{account_id}/wallets', 'Admin\Modules\Accounts\WalletsController@index');
        Route::post('/accounts/{account_id}/wallets', 'Admin\Modules\Accounts\WalletsController@store');
        Route::get('/accounts/{account_id}/wallets/{wallet_id}', 'Admin\Modules\Accounts\WalletsController@view');
        Route::post('/accounts/{account_id}/wallets/{wallet_id}', 'Admin\Modules\Accounts\WalletsController@update');
        Route::get('/accounts/{account_id}/wallets/{wallet_id}/payments', 'Admin\Modules\Accounts\WalletPaymentsController@index');
        Route::get('/accounts/{account_id}/wallets/{wallet_id}/statements', 'Admin\Modules\Accounts\WalletStatementsController@index');
        Route::get('/accounts/{account_id}/wallets/{wallet_id}/limits', 'Admin\Modules\Accounts\WalletLimitsController@index');
        Route::get('/accounts/{account_id}/payments', 'Admin\Modules\Accounts\PaymentsController@index');
        Route::get('/accounts/{account_id}/accountstatistics', 'Admin\Modules\Accounts\AccountStatisticsController@index');
        Route::get('/accounts/{account_id}/cards', 'Admin\Modules\Accounts\CardsController@index');
        Route::get('/accounts/{account_id}/accountcompliances', 'Admin\Modules\Accounts\AccountComplianceController@index');
        Route::get('/accounts/{account_id}/accountcompliances', 'Admin\Modules\Accounts\AccountComplianceController@create');
        Route::post('/accounts/{account_id}/accountcompliances', 'Admin\Modules\Accounts\AccountComplianceController@store');
        Route::get('/accounts/{account_id}/accountcompliances/{compliance_id}', 'Admin\Modules\Accounts\AccountComplianceController@view');
        Route::get('/accounts/{account_id}/accountcompliances/{compliance_id}', 'Admin\Modules\Accounts\AccountComplianceController@edit');
        Route::post('/accounts/{account_id}/accountcompliances/{compliance_id}', 'Admin\Modules\Accounts\AccountComplianceController@update');


        Route::get('/accounts/{account_id}/statements', 'Admin\Modules\Accounts\StatementsController@index');
        Route::get('/accounts/{account_id}/statements/{statement_id}', 'Admin\Modules\Accounts\StatementsController@view');



        Route::get('/accountsstatistics', 'Admin\Modules\Accounts\StatisticsController@index');
        Route::get('/accountscompliance', 'Admin\Modules\Accounts\ComplianceController@index');



        Route::get('/wallets', 'Admin\Modules\Wallets\WalletsController@index');
        Route::get('/wallets/{wid}', 'Admin\Modules\Wallets\WalletsController@view');
        Route::get('/wallets/{wid}', 'Admin\Modules\Wallets\WalletsController@edit');
        Route::post('/wallets/{wid}', 'Admin\Modules\Wallets\WalletsController@update');

        Route::get('/wallettypes', 'Admin\Modules\Wallets\WallettypesController@index');
        Route::get('/wallettypes', 'Admin\Modules\Wallets\WallettypesController@create');
        Route::post('/wallettypes', 'Admin\Modules\Wallets\WallettypesController@store');
        Route::get('/wallettypes/{wallet_type_id}', 'Admin\Modules\Wallets\WallettypesController@view');
        Route::get('/wallettypes/{wallet_type_id}', 'Admin\Modules\Wallets\WallettypesController@edit');
        Route::post('/wallettypes/{wallet_type_id}', 'Admin\Modules\Wallets\WallettypesController@update');

        Route::get('/walletfees', 'Admin\Modules\Wallets\WalletfeesController@index');
        Route::get('/walletfees', 'Admin\Modules\Wallets\WalletfeesController@create');
        Route::post('/walletfees', 'Admin\Modules\Wallets\WalletfeesController@store');
        Route::get('/walletfees/{wallet_fee_id}', 'Admin\Modules\Wallets\WalletfeesController@view');
        Route::get('/walletfees/{wallet_fee_id}', 'Admin\Modules\Wallets\WalletfeesController@edit');
        Route::post('/walletfees/{wallet_fee_id}', 'Admin\Modules\Wallets\WalletfeesController@update');

        Route::get('/walletlimits', 'Admin\Modules\Wallets\WalletlimitsController@index');
        Route::get('/walletlimits', 'Admin\Modules\Wallets\WalletlimitsController@create');
        Route::post('/walletlimits', 'Admin\Modules\Wallets\WalletlimitsController@store');
        Route::get('/walletlimits/{wallet_limit_id}', 'Admin\Modules\Wallets\WalletlimitsController@view');
        Route::get('/walletlimits/{wallet_limit_id}', 'Admin\Modules\Wallets\WalletlimitsController@edit');
        Route::post('/walletlimits/{wallet_limit_id}', 'Admin\Modules\Wallets\WalletlimitsController@update');






        Route::get('/accounts/{account_id}/users', 'Admin\Modules\Accounts\AccountUsersController@index');
        Route::post('/accounts/{account_id}/users', 'Admin\Modules\Accounts\AccountUsersController@store');
        Route::get('/accounts/{account_id}/users/{id}', 'Admin\Modules\Accounts\AccountUsersController@view');
        Route::post('/accounts/{account_id}/users/{id}', 'Admin\Modules\Accounts\AccountUsersController@update');
        Route::patch('/accounts/{account_id}/users/{id}', 'Admin\Modules\Accounts\AccountUsersController@lockunlock');
        Route::delete('/accounts/{account_id}/users/{id}', 'Admin\Modules\Accounts\AccountUsersController@destroy');



        Route::get('/accounts/{account_id}/roles', 'Admin\Modules\Accounts\AccountRolesController@index');
        Route::post('/accounts/{account_id}/roles', 'Admin\Modules\Accounts\AccountRolesController@store');
        Route::get('/accounts/{account_id}/roles/{role_id}', 'Admin\Modules\Accounts\AccountRolesController@view');
        Route::post('/accounts/{account_id}/roles/{role_id}', 'Admin\Modules\Accounts\AccountRolesController@update');

        Route::get('/accounts/pdf-convert', 'Admin\Modules\Accounts\AccountsController@pdf_convert');
        Route::get('/accounts/csv-convert', 'Admin\Modules\Accounts\AccountsController@csv_convert');




        Route::get('/transactionhistory', 'Admin\Modules\Payments\TransactionHistoryController@index');
        Route::post('/transactionhistory', 'Admin\Modules\Payments\TransactionHistoryController@index');
        Route::get('/transactionhistory/csv-convert', 'Admin\Modules\Payments\TransactionHistoryController@csv_convert');
        Route::get('/transactionhistory/pdf-convert', 'Admin\Modules\Payments\TransactionHistoryController@pdf_convert');




        Route::get('/topuppayments', 'Admin\Modules\Payments\TopUpController@index');
        Route::get('/withdrawpayments', 'Admin\Modules\Payments\WithdrawController@index');

        Route::get('/payments/csv-convert', 'Admin\Modules\Payments\PaymentsController@csv_convert');
        Route::get('/payments/pdf-convert', 'Admin\Modules\Payments\PaymentsController@pdf_convert');

        Route::get('/payments', 'Admin\Modules\Payments\PaymentsController@index');
        Route::post('/payments', 'Admin\Modules\Payments\PaymentsController@store');
        Route::get('/payments/{payment_id}', 'Admin\Modules\Payments\PaymentsController@view');
        Route::post('/payments/{payment_id}/form', 'Admin\Modules\Payments\PaymentsController@forms');
        Route::post('/payments/{payment_id}/document', 'Admin\Modules\Payments\PaymentsController@document');
        Route::post('/payments/documntcsv', 'Admin\Modules\Payments\PaymentsController@documentcsv');
        Route::get('/payments/{payment_id}/document/{document_id}', 'Admin\Modules\Payments\PaymentsController@documentview');
        Route::get('/payments/{payment_id}/relatedpayments', 'Admin\Modules\Payments\RelatedPaymentsController@index');

        Route::get('/paymentsstatistics', 'Admin\Modules\Payments\StatisticsController@index');

        Route::get('/disputes', 'Admin\Modules\Disputes\DisputesController@index');
        Route::get('/disputes/{dispute_id}', 'Admin\Modules\Disputes\DisputesController@view');




        Route::post('/payments/{payment_id}/comment', 'Admin\Modules\Payments\PaymentsController@comment');
        Route::patch('/payments/{payment_id}/comment/{id}', 'Admin\Modules\Payments\PaymentsController@commentUpdate');


        //Route::post('/payments/terminallist', 'Admin\Modules\Payments\PaymentsController@terminallist');
        //Route::post('/payments/terminalcurrlist', 'Admin\Modules\Payments\PaymentsController@terminalcurrlist');
        // Route::post('/payments/documentcsv', 'Admin\Modules\Payments\PaymentsController@documentcsv');

        //Route::post('/payments/advanced', 'Admin\Modules\Payments\PaymentsController@index')->middleware(['permission:bank_payment_list']);
        //Route::get('/payments', 'Admin\Modules\Payments\PaymentsController@index')->middleware(['permission:bank_payment_list']);
        //Route::post('/payments/store', 'Admin\Modules\Payments\PaymentsController@store')->middleware(['permission:bank_paymen_add']);
        //Route::get('/payments/download', 'Admin\Modules\Payments\PaymentsController@download');

        //Route::get('/payments/{payment_id}', 'Admin\Modules\Payments\PaymentsController@view')->middleware(['permission:bank_paymen_edit']);
        //Route::post('/payments/{payment_id}', 'Admin\Modules\Payments\PaymentsController@document');

        //Route::get('/payments/{payment_id}/document/{document_id}', 'Admin\Modules\Payments\PaymentsController@documentview');
        //Route::get('/payments/{payment_id}/relatedpayments', 'Admin\Modules\Payments\RelatedPaymentsController@index');





        Route::post('/transactions/{payment_id}/comment', 'Admin\Modules\Payments\TransactionsController@comment');
        Route::patch('/transactions/{payment_id}/comment/{id}', 'Admin\Modules\Payments\TransactionsController@commentUpdate');

        Route::get('/credits', 'Admin\Modules\Payments\CreditController@index');
        Route::get('/credits/csv-convert', 'Admin\Modules\Payments\CreditController@csv_convert');
        Route::get('/credits/pdf-convert', 'Admin\Modules\Payments\CreditController@pdf_convert');
        Route::post('/credits', 'Admin\Modules\Payments\TransactionsController@credit');







        Route::get('/paymentmethods', 'Admin\Modules\Gateways\PaymentMethodsController@index')->middleware(['permission:payment_methods_list']);
        Route::get('/paymentmethods', 'Admin\Modules\Gateways\PaymentMethodsController@create')->middleware(['permission:payment_methods_add']);
        Route::post('/paymentmethods', 'Admin\Modules\Gateways\PaymentMethodsController@store')->middleware(['permission:payment_methods_add']);
        Route::get('/paymentmethods/{id}', 'Admin\Modules\Gateways\PaymentMethodsController@view');//->middleware(['permission:payment_methods_view']);
    Route::get('/paymentmethods/{id}', 'Admin\Modules\Gateways\PaymentMethodsController@edit');//->middleware(['permission:payment_methods_edit']);
    Route::post('/paymentmethods/{id}', 'Admin\Modules\Gateways\PaymentMethodsController@update');//->middleware(['permission:payment_methods_edit']);

    Route::get('/banks', 'Admin\Modules\Gateways\BanksController@index');



        Route::get('/invoices', 'Admin\Modules\Invoices\InvoicesController@index');
        Route::post('/invoices', 'Admin\Modules\Invoices\InvoicesController@store');
        Route::get('/invoices/{id}', 'Admin\Modules\Invoices\InvoicesController@view');
        Route::post('/invoices/{id}', 'Admin\Modules\Invoices\InvoicesController@update');
        Route::post('/invoices/{id}/send', 'Admin\Modules\Invoices\InvoicesController@send');
        Route::post('/invoices/{invoice_id}/log', 'Admin\Modules\Invoices\InvoicesController@logpayment');
        Route::get('/invoices/{invoice_id}/pdf-convert', 'Admin\Modules\Invoices\InvoicesController@pdf_convert');
        Route::patch('/invoices/{id}', 'Admin\Modules\Invoices\InvoicesController@review');


        Route::get('/proformainvoices', 'Admin\Modules\Invoices\ProformainvoicesController@index');
        Route::post('/proformainvoices', 'Admin\Modules\Invoices\ProformainvoicesController@store');
        Route::get('/proformainvoices/{id}', 'Admin\Modules\Invoices\ProformainvoicesController@view');
        Route::post('/proformainvoices/{id}', 'Admin\Modules\Invoices\ProformainvoicesController@update');
        Route::post('/proformainvoices/{id}/send', 'Admin\Modules\Invoices\ProformainvoicesController@send');
        Route::post('/proformainvoices/{proforma_id}/copy', 'Admin\Modules\Invoices\ProformainvoicesController@createInvoice');

        Route::get('/proformainvoices/{proforma_id}/pdf-convert', 'Admin\Modules\Invoices\ProformainvoicesController@pdf_convert');
        Route::patch('/proformainvoices/{id}', 'Admin\Modules\Invoices\ProformainvoicesController@review');


        Route::get('/activitylog', 'Admin\Modules\Activitylog\ActivityLogController@index')->middleware(['permission:activity_list']);

        Route::get('/usermanagement/users', 'Admin\Modules\UserManagement\UsersController@index')->middleware(['permission:user_list']);
        Route::get('/usermanagement/users', 'Admin\Modules\UserManagement\UsersController@create')->middleware(['permission:user_add']);
        Route::post('/usermanagement/users', 'Admin\Modules\UserManagement\UsersController@store')->middleware(['permission:user_add']);
        Route::get('/usermanagement/users/{admin_id}', 'Admin\Modules\UserManagement\UsersController@view')->middleware(['permission:user_view']);
        Route::get('/usermanagement/users/{admin_id}', 'Admin\Modules\UserManagement\UsersController@edit')->middleware(['permission:user_edit']);
        Route::post('/usermanagement/users/{admin_id}', 'Admin\Modules\UserManagement\UsersController@update')->middleware(['permission:user_edit']);
        Route::patch('/usermanagement/users/{admin_id}', 'Admin\Modules\UserManagement\UsersController@lockunlock')->middleware(['permission:user_edit']);
        Route::get('/usermanagement/users/{admin_id}/useractivity', 'Admin\Modules\UserManagement\UserActivityController@index')->middleware(['permission:user_edit']);
        Route::get('/usermanagement/users/{admin_id}/security', 'Admin\Modules\UserManagement\SecurityController@index')->middleware(['permission:user_security_view']);
        Route::patch('/usermanagement/users/{admin_id}/security', 'Admin\Modules\UserManagement\SecurityController@update')->middleware(['permission:user_security_update']);
        Route::patch('/usermanagement/users/{admin_id}/two-step', 'Admin\Modules\UserManagement\SecurityController@two_step')->middleware(['permission:user_security_update']);

        // Route::get('/usermanagement/userroles', 'Admin\Modules\UserManagement\UserRolesController@index');
        // Route::get('/usermanagement/userroles', 'Admin\Modules\UserManagement\UserRolesController@create');
        // Route::post('/usermanagement/userroles', 'Admin\Modules\UserManagement\UserRolesController@store');
        // Route::get('/usermanagement/userroles/{id}', 'Admin\Modules\UserManagement\UserRolesController@view');
        // Route::get('/usermanagement/userroles/{id}', 'Admin\Modules\UserManagement\UserRolesController@edit');
        // Route::get('/usermanagement/userroles/{id}/users', 'Admin\Modules\UserManagement\UsersRolesController@index');

        Route::get('/settings', 'Admin\Modules\Settings\SettingsController@index')->middleware(['permission:settings']);
        Route::patch('/settings', 'Admin\Modules\Settings\SettingsController@store')->middleware(['permission:settings']);
        Route::get('/settings/company_details', 'Admin\Modules\Settings\SettingsController@index');
        Route::patch('/settings/company_details', 'Admin\Modules\Settings\SettingsController@company_details');

        Route::get('/settings/bank_accounts', 'Admin\Modules\Settings\BankaccountsController@index');
        Route::post('/settings/bank_accounts', 'Admin\Modules\Settings\BankaccountsController@store');
        Route::get('/settings/bank_accounts/{id}', 'Admin\Modules\Settings\BankaccountsController@view');
        Route::post('/settings/bank_accounts/{id}', 'Admin\Modules\Settings\BankaccountsController@update');
        Route::patch('/settings/bank_accounts/{id}', 'Admin\Modules\Settings\BankaccountsController@lockunlock');



        Route::get('/settings/api', 'Admin\Modules\Settings\SettingsController@index')->middleware(['permission:settings']);
        Route::patch('/settings/api', 'Admin\Modules\Settings\SettingsController@api')->middleware(['permission:settings']);


        //Admin Settings System
        Route::get('/settings/currencies', 'Admin\Modules\Settings\Systems\CurrencyController@index')->middleware(['permission:settings']);
        Route::post('/settings/currencies', 'Admin\Modules\Settings\Systems\CurrencyController@store')->middleware(['permission:settings']);
        Route::get('/settings/currencies/{currency_id}', 'Admin\Modules\Settings\Systems\CurrencyController@view')->middleware(['permission:settings']);
        Route::patch('/settings/currencies/{currency_id}', 'Admin\Modules\Settings\Systems\CurrencyController@update')->middleware(['permission:settings']);
        Route::delete('/settings/currencies/{currency_id}', 'Admin\Modules\Settings\Systems\CurrencyController@destroy')->middleware(['permission:settings']);
        Route::patch('/settings/currencies/{currency_id}/lockunlock', 'Admin\Modules\Settings\Systems\CurrencyController@lockunlock');

        Route::get('/settings/crossrates', 'Admin\Modules\Settings\Systems\CrossrateController@index')->middleware(['permission:settings']);
        Route::post('/settings/crossrates', 'Admin\Modules\Settings\Systems\CrossrateController@store')->middleware(['permission:settings']);
        Route::get('/settings/crossrates/{id}', 'Admin\Modules\Settings\Systems\CrossrateController@view')->middleware(['permission:settings']);
        Route::patch('/settings/crossrates/{id}', 'Admin\Modules\Settings\Systems\CrossrateController@update')->middleware(['permission:settings']);
        Route::delete('/settings/crossrates/{id}', 'Admin\Modules\Settings\Systems\CrossrateController@destroy')->middleware(['permission:settings']);


        Route::get('/settings/failedjobs', 'Admin\Modules\Settings\Systems\FailedJobController@index')->middleware(['permission:settings']);


        Route::get('/roles', 'Admin\Modules\Roles\RolesController@index');
        Route::post('/roles', 'Admin\Modules\Roles\RolesController@store');
        Route::get('/roles/{role_id}', 'Admin\Modules\Roles\RolesController@edit');
        Route::post('/roles/{role_id}', 'Admin\Modules\Roles\RolesController@update');
    });
});

Route::group(['prefix' => 'account'], function () {
    /*Route::get('/login', 'AccountAuth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'AccountAuth\LoginController@login');
    Route::post('/logout', 'AccountAuth\LoginController@logout')->name('logout');

    Route::get('/register', 'AccountAuth\RegisterController@showRegistrationForm')->name('register');
    Route::post('/register', 'AccountAuth\RegisterController@register');

    Route::post('/password/email', 'AccountAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
    Route::post('/password/reset', 'AccountAuth\ResetPasswordController@reset')->name('password.email');
    Route::get('/password/reset', 'AccountAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
    Route::get('/password/reset/{token}', 'AccountAuth\ResetPasswordController@showResetForm');*/

    Route::get('/password/setup/{token}', 'AccountAuth\LoginController@setup_password');
    Route::post('/password/setup', 'AccountAuth\LoginController@setup_password');
    Route::get('/verify-email/{code}', 'AccountAuth\LoginController@email_verify');
    Route::get('/resend-verify-email/{code}', 'AccountAuth\LoginController@resend_verification_code');
    Route::post('/crossrate', 'Account\Modules\Dashboard\DashboardController@crossrate');

    //Customer Login
    //Route::get('', 'AccountAuth\LoginController@showLoginForm');
    Route::get('/login', 'AccountAuth\LoginController@showLoginForm');
    Route::post('/login', 'AccountAuth\LoginController@login');
    Route::get('/logout', 'AccountAuth\LoginController@logout');

    //Customer Register
    Route::get('/register', 'AccountAuth\RegisterController@showRegistrationForm');
    Route::post('/register', 'AccountAuth\RegisterController@register');

    //Customer Passwords
    Route::post('/password/email', 'AccountAuth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('/password/reset', 'AccountAuth\ResetPasswordController@reset');
    Route::get('/password/change', 'AccountAuth\ResetPasswordController@changeView');
    Route::post('/password/change', 'AccountAuth\ResetPasswordController@change');
    Route::get('/password/two-step', 'AccountAuth\LoginController@twoStepView');
    Route::post('/password/two-step', 'AccountAuth\LoginController@twoStep');
    Route::get('/password/reset', 'AccountAuth\ForgotPasswordController@showLinkRequestForm');
    Route::get('/password/reset/{token}', 'AccountAuth\ResetPasswordController@showResetForm');

    Route::get('', 'Account\Modules\Dashboard\DashboardController@index');

    Route::group(['middleware' => ['web', 'activity', 'account', 'auth:account']], function () {
        Route::post('/dashboard/forms', 'Account\Modules\Dashboard\DashboardController@forms');
        Route::post('/dashboard/pay/{payment_id}', 'Account\Modules\Dashboard\DashboardController@pay');

        Route::get('/cards', 'Account\Modules\Cards\CardsController@index');

        // Route::post('psd2redirect', 'Account\Modules\Accounts\AccountsController@psd2redirect');
        // Route::get('psd2redirect', 'Account\Modules\Accounts\AccountsController@psd2redirect');

        //Route::get('/accounts', 'Account\Modules\Accounts\AccountsController@index');
        //Route::post('/accounts', 'Account\Modules\Accounts\AccountsController@store');
        //Route::get('/accounts/{account_id}', 'Account\Modules\Accounts\AccountsController@view');
        //


        Route::get('/payments/csv-convert', 'Account\Modules\Payments\PaymentsController@csv_convert');
        Route::get('/payments/pdf-convert', 'Account\Modules\Payments\PaymentsController@pdf_convert');

        Route::get('/payments', 'Account\Modules\Payments\PaymentsController@index');
        Route::get('/payments/{payment_id}', 'Account\Modules\Payments\PaymentsController@view');
        Route::post('/payments/{payment_id}', 'Account\Modules\Payments\PaymentsController@pay');


        Route::get('/statements', 'Account\Modules\Statements\StatementsController@index');
        Route::get('/statements/{statement_id}', 'Account\Modules\Statements\StatementsController@view');


        Route::patch('/cards/{linked_card_id}', 'Account\Modules\Cards\CardsController@update');
        Route::post('/cards', 'Account\Modules\Cards\CardsController@store');
        Route::get('/cards/{linked_card_id}', 'Account\Modules\Cards\CardsController@edit');
        Route::delete('/cards/{linked_card_id}', 'Account\Modules\Cards\CardsController@destroy');

        Route::get('/settings/account', 'Account\Modules\Settings\AccountController@index');
        Route::patch('/settings/account/{account_id}', 'Account\Modules\Settings\AccountController@update');
        Route::patch('/settings/security/two-step', 'Account\Modules\Settings\SecurityController@two_step');
        Route::get('/settings/security', 'Account\Modules\Settings\SecurityController@index');
        Route::patch('/settings/security', 'Account\Modules\Settings\SecurityController@update');


        Route::patch('/settings/notifications/{notification_id}', 'Account\Modules\Settings\NotificationsController@notifications');

        Route::get('/settings/notifications', 'Account\Modules\Settings\NotificationsController@index');
        Route::get('/settings/limits', 'Account\Modules\Settings\LimitsController@index');
        Route::get('/settings/compliances', 'Account\Modules\Settings\ComplianceController@index');
        Route::get('/settings/compliances', 'Account\Modules\Settings\ComplianceController@create');
        Route::post('/settings/compliances', 'Account\Modules\Settings\ComplianceController@store');
        Route::get('/settings/compliances/{compliance_id}', 'Account\Modules\Settings\ComplianceController@view');

        Route::get('/activities', 'Account\Modules\Activities\ActivitiesController@index');

        Route::get('/usermanagement/users', 'Account\Modules\UserManagement\UsersController@index');
        Route::post('/usermanagement/users', 'Account\Modules\UserManagement\UsersController@store');
        Route::get('/usermanagement/users/{id}', 'Account\Modules\UserManagement\UsersController@view');
        Route::post('/usermanagement/users/{id}', 'Account\Modules\UserManagement\UsersController@update');
        Route::patch('/usermanagement/users/{id}', 'Account\Modules\UserManagement\UsersController@lockunlock');
        Route::delete('/usermanagement/users/{id}', 'Account\Modules\UserManagement\UsersController@destroy');
        Route::get('/usermanagement/users/{id}/useractivity', 'Account\Modules\UserManagement\UserActivityController@index');
        Route::get('/usermanagement/users/{id}/security', 'Account\Modules\UserManagement\SecurityController@index');
        Route::patch('/usermanagement/users/{id}/security', 'Account\Modules\UserManagement\SecurityController@update');
        Route::patch('/usermanagement/users/{id}/two-step', 'Account\Modules\UserManagement\SecurityController@two_step');



        Route::get('/usermanagement/roles', 'Account\Modules\UserManagement\RolesController@index');
        Route::post('/usermanagement/roles', 'Account\Modules\UserManagement\RolesController@store');
        Route::get('/usermanagement/roles/{role_id}', 'Account\Modules\UserManagement\RolesController@view');
        Route::post('/usermanagement/roles/{role_id}', 'Account\Modules\UserManagement\RolesController@update');
    });
    Route::get('/terms', 'Account\Modules\Legal\TermsController@index');
    Route::get('/privacy', 'Account\Modules\Legal\PrivacyController@index');
});
