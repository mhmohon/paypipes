<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rule_criteria extends Model
{
    protected $fillable =[
        
    ];
	protected $connection = 'mysqlfraud';
	
	public static function _all()
    {
		return Rule_criteria::where('status', '=', 'Active')->get();
	}
	public static function details($criteria_id)
    {
		return Rule_criteria::where('status', '=', 'Active')->where('criteria_id', '=', $criteria_id)->get();
	}
	public static function groupname($group_name)
    {
		return Rule_criteria::where('status', '=', 'Active')->where('group_name', '=', $group_name)->get()->toArray();
	}
	
}