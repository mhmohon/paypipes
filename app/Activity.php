<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Activity extends Model
{
    protected $fillable =[
        'activity_id',
        'admin_id',
        'account_id',
        'activity',
        'interface',
        'module',
        'activity_type',
        'status',
    ];

    public function admin()
    {
        return $this->belongsTo('App\Admin');
    }

    public function account()
    {
        return $this->belongsTo('App\Account');
    }
}
