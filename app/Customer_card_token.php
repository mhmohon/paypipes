<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer_card_token extends Model
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'card_token', 
		'pm_card_token', 
		'payment_method'
    ];

	
	public function admin()
    {
        return $this->belongsTo('App\Admin');
    }
	
}
