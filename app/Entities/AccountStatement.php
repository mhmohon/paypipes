<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class AccountStatement extends Model
{
    protected $table = 'account_statements';
}
