<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MaxmindScore.
 *
 * @package namespace App\Entities;
 */
class FailedJob extends Model
{
    protected $table = 'failed_jobs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [

    ];

    public function getPayloadAttribute($value)
    {
        return json_decode($value);
    }
}
