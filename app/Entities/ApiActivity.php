<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class MaxmindScore.
 *
 * @package namespace App\Entities;
 */
class ApiActivity extends Model implements Transformable
{
    use TransformableTrait;

    protected $table = 'transaction_logs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'transaction_id',
       'order_id',
       'transaction_type',
       'description',
       'sendTo',
       'body',
       'status',
       'status_code',
       'route',
       'ipAddress',
       'userAgent',
       'locale',
       'referer'
    ];

    public function transaction()
    {
        return $this->belongsTo('App\Transaction', 'order_id', 'order_id');
    }
}
