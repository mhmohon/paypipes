<?php
namespace App\Entities;
use Illuminate\Database\Eloquent\Model;

class CardType extends Model
{
    protected $table = 'card_types';

    protected $fillable =[
        'name',
        'file_name',
        'file_path',
        'description',
        'status',
    ];


    public static function  cardlist()
    {
        return CardType::where('status', '=', 'Active')->orderBy('name','desc')->pluck('description', 'name');
    }
}
