<?php

namespace App\Entities;

use DB;
use Illuminate\Notifications\Notifiable;
use App\Notifications\MerchantResetPassword;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class MerchantUser extends Authenticatable
{
    use Notifiable, SoftDeletes;

    protected $table = 'merchant_users';

    protected $dates = ['deleated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'merchant_id',
        'name',
        'first_name',
        'last_name',
        'email',
        'password',
        'phone',
        'street',
        'city',
        'state',
        'country',
        'post_code',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new MerchantResetPassword($token));
    }

    public function merchant()
    {
        return $this->belongsTo('App\Merchant', 'merchant_id', 'merchant_id');
    }

    public static function merchantid()
    {
        $merchant_startid= config('global.merchant_startid');
        $merchant_id=(int)$merchant_id=DB::table('merchants')->orderBy('merchant_id', 'desc')->value('merchant_id');
        if ($merchant_id < 1) {
            $merchant_id=$merchant_startid;
        } else {
            $merchant_id = $merchant_id + 1;
        }
        return $merchant_id;
    }


    public function getCountryNameAttribute()
    {
        $code = $this->attributes['country'];
        $country = DB::table('countries')->where('country_code', $code)->first();
        if (isset($country->country_name)) {
            return $country->country_name;
        } else {
            return '';
        }
    }

    public function getCompanyNameAttribute()
    {
        $merchant_id = $this->attributes['merchant_id'];
        $company_name = DB::table('merchants')->where('merchant_id', $merchant_id)->value('company_name');
        if (!empty($company_name)) {
            return $company_name;
        } else {
            return "";
        }
    }
}
