<?php
namespace App\Entities;

use App\Libraries\EncryptDecrypt;
use DB;
use Illuminate\Database\Eloquent\Model;

class ApiEncrypt extends Model
{
    protected $crypto;
    public $crypto_key;

    public function __construct()
    {
        $main_key = DB::table('main_key')->where('status', '=', 'Active')->orderBy('id', 'DESC')->first();
        $this->crypto_key = DB::table('crypto_key')->where('status', '=', 'Active')->orderBy('id', 'DESC')->first();
        $this->crypto = new EncryptDecrypt($main_key->main_key);
    }

    public function cardEncrypt($card_number)
    {
        return $this->crypto->encrypt($card_number, $this->crypto_key->key_card);
    }
    public function cardDecrypt($card_number_encrypt)
    {
        return $this->crypto->decrypt($card_number_encrypt, $this->crypto_key->key_card);
    }
}
