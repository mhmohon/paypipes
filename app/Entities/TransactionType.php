<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MaxmindScore.
 *
 * @package namespace App\Entities;
 */
class TransactionType extends Model
{
    protected $table = 'transaction_type';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'type_name',

    ];

    public function transaction()
    {
        return $this->belongsTo('App\Entities\Transaction\Transaction', 'transaction_type', 'type_name');
    }
}
