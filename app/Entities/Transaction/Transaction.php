<?php

namespace App\Entities\Transaction;

use App\Psp;
use App\Merchant;
use App\Terminal;
use App\Entities\TransactionNote;
use App\Entities\Maxmind\MaxmindScore;
use App\Entities\Tokenize\TokenizeCard;
use Illuminate\Database\Eloquent\Model;
use App\Entities\Tokenize\TokenizeCustomer;
use App\Transformers\TransactionTransformer;

class Transaction extends Model
{
    public $transformer = TransactionTransformer::class;

    protected $table = 'transactions';
    //protected $primaryKey = 'customer_token';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'transaction_id',
        'customer_token',
        'card_token',
        'merchant_id',
        'terminal_id',
        'psp_id',
        'order_id',
        'amount',
        'currency',
        'description',
        'transaction_type',
        'gateway',
        'transaction_status',
        'settelment_date',
        'parent_transaction_id',
        'return_url',
        'ip_address',
        'added_by',
        'updated_by',
        'batch_id',
        'wallet_id',
        'referer',
        'reference_number',
        'auth_code',
        'error_code',
        'error_description',
        'token',
        'bank_tokenization',
        'stage',
        'test_id',
        'response_type',
        'gateway_token',
        'risk_fee',
        'maxmind_id',
        'descriptor',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */


    public function cards()
    {
        return $this->hasOne(TokenizeCard::class, 'card_token', 'card_token');
    }
    public function customers()
    {
        return $this->hasOne(TokenizeCustomer::class, 'customer_token', 'customer_token');
    }
    public function merchants()
    {
        return $this->hasOne(Merchant::class, 'merchant_id', 'merchant_id');
    }
    public function terminals()
    {
        return $this->hasOne(Terminal::class, 'terminal_id', 'terminal_id');
    }
    public function psps()
    {
        return $this->hasOne(Psp::class, 'psp_id', 'psp_id');
    }
    public function maxminds()
    {
        return $this->hasOne(MaxmindScore::class, 'id', 'maxmind_id');
    }
    public function notes()
    {
        return $this->hasMany(TransactionNote::class, 'transaction_id', 'transaction_id');
    }
}
