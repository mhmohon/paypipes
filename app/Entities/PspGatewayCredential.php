<?php
namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Transformers\PspGatewayCredentialTransformer;

class PspGatewayCredential extends Model
{
    public $transformer = PspGatewayCredentialTransformer::class;

    protected $table = 'psp_gateway_credentials';

    protected $fillable =[
        'gateway_token',
        'field',
        'value'
    ];

    public function gateway()
    {
        return $this->belongsTo('App\Entities\PspGateway', 'gateway_token', 'gateway_token');
    }
}
