<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MaxmindScore.
 *
 * @package namespace App\Entities;
 */
class TransactionNote extends Model
{
    protected $table = 'transaction_notes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'transaction_id',
        'notes',
        'added_by',
        'updated_by',
    ];

    public function transaction()
    {
        return $this->belongsTo('App\Entities\Transaction\Transaction', 'transaction_id', 'transaction_id');
    }
}
