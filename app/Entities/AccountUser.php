<?php

namespace App\Entities;

use DB;
use Illuminate\Notifications\Notifiable;
use App\Notifications\AccountResetPassword;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Events\UserRegisterEvent;

class AccountUser extends Authenticatable
{
    use Notifiable, SoftDeletes;

    protected $table = 'account_users';

    protected $dates = ['deleated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'account_id',
        'name',
        'first_name',
        'last_name',
        'email',
        'password',
        'phone',
        'street',
        'city',
        'state',
        'country',
        'post_code',
        'status',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => UserRegisterEvent::class,
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new AccountResetPassword($token));
    }

    public function account()
    {
        return $this->belongsTo('App\Account', 'account_id', 'account_id');
    }

    public static function accountid()
    {
        $account_startid= config('global.account_startid');
        $account_id=(int)$account_id=DB::table('accounts')->orderBy('account_id', 'desc')->value('account_id');
        if ($account_id < 1) {
            $account_id=$account_startid;
        } else {
            $account_id = $account_id + 1;
        }
        return $account_id;
    }


    public function getCountryNameAttribute()
    {
        $code = $this->attributes['country'];
        $country = DB::table('countries')->where('country_code', $code)->first();
        if (isset($country->country_name)) {
            return $country->country_name;
        } else {
            return '';
        }
    }

    public function getCompanyNameAttribute()
    {
        $account_id = $this->attributes['account_id'];
        $company_name = DB::table('accounts')->where('account_id', $account_id)->value('company_name');
        if (!empty($company_name)) {
            return $company_name;
        } else {
            return "";
        }
    }
}
