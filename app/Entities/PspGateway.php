<?php
namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use App\Transformers\PspGatewayTransformer;

class PspGateway extends Model
{
    public $transformer = PspGatewayTransformer::class;

    protected $table = 'psp_gateways';

    protected $fillable =[
        'psp_id',
        'order_id',
        'merchant_name',
        'gateway',
        'gateway_type',
        'payment_method',
        'gateway_token',
        'threed',
        'status',
        'test_id',
        'created_at',
        'added_by',
        'updated_at',
        'updated_by',
    ];

    public function psp()
    {
        return $this->belongsTo('App\Psp', 'psp_id', 'psp_id');
    }
    public function credentials()
    {
        return $this->hasMany('App\Entities\PspGatewayCredential', 'gateway_token', 'gateway_token');
    }
}
