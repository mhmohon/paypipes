<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Class MaxmindScore.
 *
 * @package namespace App\Entities;
 */
class PaymentNote extends Model
{
    protected $table = 'payment_notes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'payment_id',
        'notes',
        'added_by',
        'updated_by',
    ];

    public function payment()
    {
        return $this->belongsTo('App\Payment', 'payment_id', 'payment_id');
    }
}
