<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bin extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'bin';

    public $timestamps = false;
	
	
}
