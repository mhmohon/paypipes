<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rule_cvv_code extends Model
{
    protected $fillable =[
        
    ];
	protected $connection = 'mysqlfraud';
	
	public static function _all()
    {
		return Rule_cvv_code::where('status', '=', 'Active')->get();
	}
	
	public static function details($code)
    {
		return Rule_cvv_code::where('status', '=', 'Active')->where('code', '=', $code)->get();
	}
 
}