<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rule_card_type extends Model
{
    protected $fillable =[
        
    ];
	protected $connection = 'mysqlfraud';
	
	
	public static function _all()
    {
		return Rule_card_type::where('status', '=', 'Active')->get();
	}
	
	public static function details($id)
    {
		return Rule_card_type::where('status', '=', 'Active')->where('cc_id', '=', $id)->get();
	}
 
}