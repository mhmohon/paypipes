<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Walletlimit extends Model
{
    protected $fillable =[
        'wallet_limit_id',
		'name',
        'currency',
        'max_balance',
        'max_turnover',
        'max_turnover_per_month',
        'max_credit_amount_per_day',
        'max_credit_payments_per_day',
        'max_topup_amount_per_day',
        'max_topup_payments_per_day',
        'max_topup_amount_per_payment',
        'max_receive_amount_per_day',
        'max_receive_payments_per_day',
        'max_receive_amount_per_payment',
        'max_debit_amount_per_day',
        'max_debit_payments_per_day',
        'max_send_amount_per_day',
        'max_send_payments_per_day',
        'max_send_amount_per_payment',
        'max_withdraw_amount_per_day',
        'max_withdraw_payments_per_day',
        'max_withdraw_amount_per_payment',
        'max_withdraw_amount',
        'status',
    ];
    
    public function wallets()
    {
        return $this->hasMany('App\Wallet', 'wallet_limit_id', 'id');
    }
}
