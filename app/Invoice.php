<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    protected $fillable =[
        'invoice_id',
        'proforma_id',
        'bank_account_id',
        'merchant_id',
        'psp_id',
        'invoice_date',
        'due_date',
        'description',
        'item_others_fee',
        'amount_others_fee',
        'qty_others_fee',
        'tax',
        'total',
        'currency',
        'status',
        'review',
    ];

    public static function searchAdmin($request)
    {
        $item_per_page = !empty($request->rows)?$request->rows:ROW_PER_PAGE;


        $invoices = Invoice::where(function ($query) use ($request) {
            if (($search = $request->get('search'))) {
                $query->orWhere('merchant_id', 'like', '%' . $search . '%');
                $query->orWhere('psp_id', 'like', '%' . $search . '%');
                $query->orWhere('invoice_id', 'like', '%' . $search . '%');
                $query->orWhere('proforma_id', 'like', '%' . $search . '%');
                $query->orWhere('review', 'like', '%' . $search . '%');
                $query->orWhere('currency', 'like', '%' . $search . '%');
                $query->orWhere('status', 'like', '%' . $search . '%');
                $query->orWhere('description', 'like', '%' . $search . '%');
            }
        })
        ->orderBy('id', 'desc')
        ->paginate($item_per_page);

        return $invoices;
    }
    public static function search($request, $where)
    {
        $item_per_page = !empty($request->rows)?$request->rows:ROW_PER_PAGE;


        $invoices = Invoice::where(function ($query) use ($request) {
            if (($search = $request->get('search'))) {
                $query->orWhere('merchant_id', 'like', '%' . $search . '%');
                $query->orWhere('psp_id', 'like', '%' . $search . '%');
                $query->orWhere('invoice_id', 'like', '%' . $search . '%');
                $query->orWhere('proforma_id', 'like', '%' . $search . '%');
                $query->orWhere('review', 'like', '%' . $search . '%');
                $query->orWhere('currency', 'like', '%' . $search . '%');
                $query->orWhere('status', 'like', '%' . $search . '%');
                $query->orWhere('description', 'like', '%' . $search . '%');
            }
        })
        ->where($where)
        ->orderBy('id', 'desc')
        ->paginate($item_per_page);

        return $invoices;
    }
    public static function invoiceid()
    {
        $invoice_id=date('Y');
        $invoiceid =(int)$invoiceid=Invoice::where('invoice_id', 'like', $invoice_id . '%')->orderBy('invoice_id', 'desc')->value('invoice_id');
        if ($invoiceid < 1) {
            $invoice_id.='001';
        } else {
            $invoice_id = $invoiceid + 1;
        }
        return $invoice_id;
    }
}
