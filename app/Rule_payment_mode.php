<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rule_payment_mode extends Model
{
    protected $fillable =[
        
    ];
	protected $connection = 'mysqlfraud';
	
	public static function _all()
    {
		return Rule_payment_mode::where('status', '=', 'Active')->get();
	}
	
	public static function details($id)
    {
		return Rule_payment_mode::where('status', '=', 'Active')->where('id', '=', $id)->get();
	}
 
}