<?php
namespace App\Libraries;
use Illuminate\Encryption\Encrypter;

class EncryptDecrypt
{
   
    private  $key='';	
	private  $cipher_ecb = "AES-256-ECB";
	private  $cipher_cbc = "AES-256-CBC";
	private  $cipher_gcm = "AES-256-GCM";
			
	function __construct($key)
	{
		$this->key=$key;      
	}	
	
	
	public function encrypt($text,$key2)
	{         
		$key2=$this->decrypt_core($key2,$this->key);
		return trim(openssl_encrypt($text, $this->cipher_ecb, $key2, 0));
	} 
	
	public function decrypt($text,$key2) 
	{   
		$key2=$this->decrypt_core($key2,$this->key);
		return trim(openssl_decrypt($text, $this->cipher_ecb, $key2, 0));
	}
	
	public function encrypt_ecb($text,$key2)
	{ 	
		return trim(openssl_encrypt($text, $this->cipher_ecb, $key2, 0));
	} 
	
	public function decrypt_ecb($text,$key2) 
	{  
		return trim(openssl_decrypt($text, $this->cipher_ecb, $key2, 0));
	}
	

    public function encrypt_cbc($text,$key2)
    {   

		$key2=$this->decrypt_core($key2,$this->key);
		
		$encrypter = new Encrypter($key2,$this->cipher_cbc);
		$ciphertext=$encrypter->encryptString($text);		

		return $ciphertext;
    }

    public function decrypt_cbc($text,$key2)
    {   
		$key2=$this->decrypt_core($key2,$this->key);
	
		$encrypter = new Encrypter($key2,$this->cipher_cbc);
		$ciphertext=$encrypter->decryptString($text);		

		return $ciphertext;
    }
	
	
    public function decrypt_core($text,$key2)
	{   
		$encrypter = new Encrypter($key2,$this->cipher_cbc);
		$ciphertext=$encrypter->decryptString($text);
		return $ciphertext;		
	} 
	
	public function encrypt_core($text,$key2)
    {   
		$encrypter = new Encrypter($key2,$this->cipher_cbc);
		$ciphertext=$encrypter->encryptString($text);		
		return $ciphertext;
    }
	
	public function card_markup($card) 
	{   

		if(strlen($card)>10)
		{
			$card_digit=substr($card,-4);
			$card_bin=substr($card,0,6);
			$card_number=$card_bin;
			$cardX="";
			for($i=10; $i<strlen($card); $i++)
			{
				$cardX=$cardX."X";
				
			}
			$card_number=$card_number.$cardX.$card_digit;
		}
		else
		{
			$card_digit=substr($card,-4);
			$card_bin=substr($card,0,1);
			$card_number=$card_bin;
			$cardX="";
			for($i=5; $i<strlen($card); $i++)
			{
				$cardX=$cardX."X";
				
			}
			$card_number=$card_number.$cardX.$card_digit;
		}	
		return $card_number;
	}
	public function getEncryptedPass($len){
		
			return $this->encrypt($this->makeRandomPassword($len));
	}
    
	function makeRandomPassword($len) 
    { 
 	 	$salt = "ABCDEFGHJKLMNPRSTUVWXYZ0123456789abchefghjkmnpqrstuvwxyz"; 
 	 	$pass = '';
 	 	srand((double)microtime()*1000000); 
		for($i = 0;$i < $len;$i++) { 
				$num = rand() % 56;	$tmp = substr($salt, $num, 1); $pass = $pass . $tmp; 
		} 
		return $pass; 
	}

	function makeRandomPassword2($len) 
    { 
 	 	$salt = "ABCDEFGHJKLMNPRSTUVWXYZ0123456789"; 
 	 	$pass = '';
 	 	srand((double)microtime()*1000000); 
		for($i = 0;$i < $len;$i++) { 
				$num = rand() % 33;	$tmp = substr($salt, $num, 1); $pass = $pass . $tmp; 
		} 
		return $pass; 
	}
}
