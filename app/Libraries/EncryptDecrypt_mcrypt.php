<?php
namespace App\Libraries;

class EncryptDecrypt2
{
    public  $id ='';
    public  $key='';	
			
	function __construct($key)
	{
		$this->key=$key;      
	}	
	
	
	public function encrypt($text,$key2)
	{         
		$key2=$this->decrypt_core($key2,$this->key);   
	 	return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key2, $text, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)))); 
	} 
	
	public function decrypt($text,$key2) 
	{   
		$key2=$this->decrypt_core($key2,$this->key);
		
		return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key2, base64_decode($text), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))); 
	}

    public function encrypt_cbc($text='',$key2='')
    {
        $key2=$this->decrypt_core($key2,$this->key);
        # create a random IV to use with CBC encoding
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $ciphertext = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key2, utf8_encode($text), MCRYPT_MODE_CBC, $iv);
        return trim(base64_encode($iv.$ciphertext));

    }

    public function decrypt_cbc($text='',$key2='')
    {
        $key2=$this->decrypt_core($key2,$this->key);
        # create a random IV to use with CBC encoding
        $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC);
        $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
        $ciphertext_dec = base64_decode($text);
        # retrieves the IV, iv_size should be created using mcrypt_get_iv_size()
        $iv_dec = substr($ciphertext_dec, 0, $iv_size);
        # retrieves the cipher text (everything except the $iv_size in the front)
        $ciphertext_dec = substr($ciphertext_dec, $iv_size);
        # may remove 00h valued characters from end of plain text
        return trim( mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key2,$ciphertext_dec, MCRYPT_MODE_CBC, $iv_dec));
    }

    public function decrypt_core($text,$key2)
	{   
		return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key2, base64_decode($text), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))); 
	} 
	
	public function card_markup($card) 
	{   

		if(strlen($card)>10)
		{
			$card_digit=substr($card,-4);
			$card_bin=substr($card,0,6);
			$card_number=$card_bin;
			$cardX="";
			for($i=10; $i<strlen($card); $i++)
			{
				$cardX=$cardX."X";
				
			}
			$card_number=$card_number.$cardX.$card_digit;
		}
		else
		{
			$card_digit=substr($card,-4);
			$card_bin=substr($card,0,1);
			$card_number=$card_bin;
			$cardX="";
			for($i=5; $i<strlen($card); $i++)
			{
				$cardX=$cardX."X";
				
			}
			$card_number=$card_number.$cardX.$card_digit;
		}	
		return $card_number;
	}
	public function getEncryptedPass($len){
		
			return $this->encrypt($this->makeRandomPassword($len));
	}
    
	function makeRandomPassword($len) 
    { 
 	 	$salt = "ABCDEFGHJKLMNPRSTUVWXYZ0123456789abchefghjkmnpqrstuvwxyz"; 
 	 	$pass = '';
 	 	srand((double)microtime()*1000000); 
		for($i = 0;$i < $len;$i++) { 
				$num = rand() % 56;	$tmp = substr($salt, $num, 1); $pass = $pass . $tmp; 
		} 
		return $pass; 
	}

	function makeRandomPassword2($len) 
    { 
 	 	$salt = "ABCDEFGHJKLMNPRSTUVWXYZ0123456789"; 
 	 	$pass = '';
 	 	srand((double)microtime()*1000000); 
		for($i = 0;$i < $len;$i++) { 
				$num = rand() % 33;	$tmp = substr($salt, $num, 1); $pass = $pass . $tmp; 
		} 
		return $pass; 
	}
}
