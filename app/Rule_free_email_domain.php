<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rule_free_email_domain extends Model
{
    protected $fillable =[
        
    ];
	protected $connection = 'mysqlfraud';
	
	public static function _all()
    {
		return Rule_free_email_domain::where('status', '=', 'Active')->get();
	}
	
	public static function details($email_domain)
    {
		return Rule_free_email_domain::where('status', '=', 'Active')->where('email_domain', '=', $email_domain)->get();
	}
}