<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $fillable =[
        
    ];
	
	public function merchant()
    {
        return $this->hasMany('App\Merchant');
    }
	public function city() 
	{
		return $this->hasMany('App\City');
	}
	
	public static function  countrylist()
    {
		return Country::where('active', '=', 1)->orderBy('country_name','asc')->pluck('country_name', 'country_code');
	}
	public static function  countrycode($country_name)
    {
		return Country::where('active', '=', 1)->where('country_name', '=', $country_name)->value('country_code');
	}
 
}