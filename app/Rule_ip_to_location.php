<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rule_ip_to_location extends Model
{
    protected $fillable =[
        
    ];
	protected $connection = 'mysqlfraud';
	
	public static function _all()
    {
		return Rule_ip_to_location::where('status', '=', 'Active')->get();
	}
	
	public static function details($id)
    {
		return Rule_ip_to_location::where('status', '=', 'Active')->where('id', '=', $id)->get();
	}
 
	public static function detailsbycountry($country_code)
    {
		return Rule_ip_to_location::where('status', '=', 'Active')->where('country_code', '=', $country_code)->get();
	}
	
	public static function detailsbywhere($where)
    {
		return Rule_ip_to_location::where('status', '=', 'Active')->where($where)->get();
	}
 
}