<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Timezone extends Model
{
    protected $table = 'timezones';

    protected $fillable =[
        'offset',
        'name',
        'timezone',
        'status'
    ];

    public function merchant()
    {
        return $this->hasMany('App\Timezone', 'timezone', 'timezone');
    }


    public static function timezonelist()
    {
        return ['0'=>'Select a TimeZone'] + Timezone::where('status', '=', 'Active')->orderBy('id', 'asc')->pluck('name', 'timezone')->all();
    }
}
