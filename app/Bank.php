<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $fillable =[
        
    ];
	
	
	public static function  banklist()
    {
		return Bank::where('status', '=', 'Active')->orderBy('bank_name','asc')->pluck('bank_name', 'bank_id');
	}
	public static function  bankid($bank_name)
    {
		return Bank::where('active', '=', 'Active')->where('bank_name', '=', $bank_name)->value('bank_id');
	}
 
}