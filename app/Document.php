<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $fillable =[
        'document_id',
        'name', 
        'description',
        'file_name',
        'file_path',
        'status',
    ];
    
    public function admin()
    {
        return $this->belongsTo('App\Admin');
    }
}
