<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rule_map_condition_to_criteria extends Model
{
    protected $fillable =[
        
    ];
	protected $connection = 'mysqlfraud';
	
	public static function _all()
    {
		return Rule_map_condition_to_criteria::where('status', '=', 'Active')->get();
	}
	
	public static function details($condition_id)
    {
		return Rule_map_condition_to_criteria::where('status', '=', 'Active')->where('condition_id', '=', $condition_id)->get();
	}
 
	public static function detailsbycriteria($criteria_id)
    {
		return Rule_map_condition_to_criteria::where('status', '=', 'Active')->where('criteria_id', '=', $criteria_id)->get();
	}
 
}