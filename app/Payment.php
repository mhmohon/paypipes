<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable =[
        'payment_id',
        'account_id',
        'wallet_id',
        'description',
        'payment_method',
        'payment_type',
        'currency',
        'amount',
        'fee',
        'total_amount',
        'from',
        'to',
        'reference_id',
        'status',
        'setlement_date',
        'account_holder_name',
        'account_number',
        'iban',
        'swift',
        'bank_name',
        'bank_address',
    ];

    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }



    public static function search($request, $data)
    {
        $item_per_page = !empty($request->rows)?$request->rows:ROW_PER_PAGE;


        $req=$search="";
        if (!empty($request->all())) {
            $req= $request->all();
        } elseif (!empty($request->old())) {
            $req= $request->old();
        }


        //dd($request,$request->old(),$request->old('page'));
        if (!empty($req['search'])) {
            $search = $req['search'];
        }


        if (empty($search)) {
            $payments = Payment::leftJoin('terminals', 'payments.terminal_id', '=', 'terminals.terminal_id')
            ->where(function ($query) use ($req,$data) {
                if (!empty($req)) {

                        //filter by keyword
                    if (!empty($req['currency'])) {
                        $query->where('payments.currency', $req['currency']);
                    }
                    if (!empty($req['psp_id'])) {
                        $query->where('payments.psp_id', $req['psp_id']);
                    }
                    if (!empty($req['merchant_id'])) {
                        $query->where('payments.merchant_id', $req['merchant_id']);
                    }
                    if (!empty($req['terminal_id'])) {
                        $query->where('payments.terminal_id', $req['terminal_id']);
                    }

                    if (!empty($req['status'])) {
                        $query->where('payments.status', $req['status']);
                    }

                    if (!empty($req['date_from']) && !empty($req['date_to'])) {
                        $query->where(function ($q) use ($req) {
                            $q->whereDate('payments.created_at', '>=', date('Y-m-d H:i:s', strtotime($req['date_from'])))
                                  ->whereDate('payments.created_at', '<=', date('Y-m-d H:i:s', strtotime($req['date_to'])));
                        });
                    } elseif (!empty($req['date_from'])) {
                        $query->whereDate('payments.created_at', '>=', date('Y-m-d H:i:s', strtotime($req['date_from'])));
                    } elseif (!empty($req['date_to'])) {
                        $query->whereDate('payments.created_at', '<=', date('Y-m-d H:i:s', strtotime($req['date_to'])));
                    }

                    if (!empty($req['amount_from']) && !empty($req['amount_to'])) {
                        $query->where(function ($q) use ($req) {
                            $q->where('payments.amount', '>=', $req['amount_from'])
                                  ->where('payments.amount', '<=', $req['amount_to']);
                        });
                    } elseif (!empty($req['amount_from'])) {
                        $query->where('payments.amount', '>=', $req['amount_from']);
                    } elseif (!empty($req['amount_to'])) {
                        $query->where('payments.amount', '<=', $req['amount_to']);
                    }
                }
            });
        } else {
            $payments = Payment::leftJoin('terminals', 'payments.terminal_id', '=', 'terminals.terminal_id')
            ->where(function ($query) use ($search,$data) {
                //filter by keyword
                if (!empty($search)) {
                    $query->orWhere('payment_id', 'like', '%' . $search . '%');
                    $query->orWhere('terminal_id', 'like', '%' . $search . '%');
                    $query->orWhere('merchant_id', 'like', '%' . $search . '%');
                    $query->orWhere('from', 'like', '%' . $search . '%');
                    $query->orWhere('to', 'like', '%' . $search . '%');
                    $query->orWhere('description', 'like', '%' . $search . '%');
                    $query->orWhere('payment_method', 'like', '%' . $search . '%');
                    $query->orWhere('payment_type', 'like', '%' . $search . '%');
                    $query->orWhere('amount', 'like', '%' . $search . '%');
                    $query->orWhere('total_amount', 'like', '%' . $search . '%');
                    $query->orWhere('currency', 'like', '%' . $search . '%');
                    $query->orWhere('status', 'like', $search . '%');
                }
            });
        }
        if (!empty($data['merchant_id'])) {
            $payments = $payments->where('payments.merchant_id', $data['merchant_id']);
        }
        if (!empty($data['terminal_id'])) {
            $payments = $payments->where('payments.terminal_id', $data['terminal_id']);
        }
        if (!empty($data['psp_id'])) {
            $payments = $payments->where('payments.psp_id', $data['psp_id']);
        }


        $payments = $payments->select('payments.*', 'terminals.name')->orderBy('payments.created_at', 'desc')->paginate($item_per_page);

        return $payments;
    }
}
