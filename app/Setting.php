<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Setting extends Model
{
   protected $fillable =[
        'company_legal_name',
		'street',
		'city',
		'post_code',   
		'country',
		'company_id',
		'vat_id',
		'invoice_email',
		'invoice_website',
		'account_holder',
		'account_number',
		'bank_name',
		'iban',
		'swift_bic',
    ];
	
	public function country() 
	{
		return $this->belongsTo('App\Country');
	}
	public function getCountryNameAttribute(){
        $code = $this->attributes['country'];
        $country = DB::table('countries')->where('country_code', $code)->first();
        if(isset($country->country_name)) return $country->country_name;
        else return '';
    }
}
