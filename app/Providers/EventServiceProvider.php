<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\UserRegisterEvent' => [
            'App\Listeners\UserRegisterListener',
            // 'App\Listeners\SendNewUserNotification',
        ],
        'App\Events\ConfirmNewAccount' => [
            'App\Listeners\ConfirmNewAccountListener',
        ],
        'App\Events\SendTwoFactorCode' => [
            'App\Listeners\SendTwoFactorCodeListener',
        ],
        'App\Events\ResendVerificationCode' => [
            'App\Listeners\ResendVerificationCodeListener',
        ],
        
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
