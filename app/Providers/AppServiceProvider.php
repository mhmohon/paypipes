<?php

namespace App\Providers;
use Validator;
use App\Entities\FailedJob;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (!defined('ROW_PER_PAGE')) {
            define('ROW_PER_PAGE', 20);
        }

        if ($this->app->environment() != 'local') {
            \Illuminate\Support\Facades\URL::forceScheme('https');
        }

        Validator::extend('alpha_spaces', function ($attribute, $value) {           
            return preg_match('/^[\pL\s]+$/u', $value);
        });

        View::share('failed_count', FailedJob::count());
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
