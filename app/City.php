<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $fillable =[
        
    ];
	
	public function merchant()
    {
        return $this->hasMany('App\Merchant');
    }
	public function country() 
	{
		return $this->belongsTo('App\Country');
	}
 
}