<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rule_blacklistbin extends Model
{
    protected $fillable =[
        'customer_id',
        'merchant_id',
        'terminal_id',
        'card_type',
        'card_bin',
        'type',
        'status',
    ];
    
    protected $connection = 'mysqlfraud';
	
	public static function search($request,$where)
    {
	   $item_per_page = !empty($request->rows)?$request->rows:ROW_PER_PAGE;
		
       $search = Rule_blacklistbin::where(function($query) use ($request) {
			//filter by keyword
			if (($search = $request->get('search'))) {
				$query->orWhere('card_type', 'like', '%' . $search . '%');
				$query->orWhere('card_bin', 'like', '%' . $search . '%');
				$query->orWhere('type', 'like', $search . '%');
				$query->orWhere('status', 'like', $search . '%');
			}
		})		
		->where($where)		
		->orderBy('created_at','desc')
		->paginate($item_per_page);	
//->orWhere('type', '=', 'Core')		
		return $search;
    }
	
	public static function searchadmin($request)
    {
	   $item_per_page = !empty($request->rows)?$request->rows:ROW_PER_PAGE;
		
       $search = Rule_blacklistbin::where(function($query) use ($request) {
			//filter by keyword
			if (($search = $request->get('search'))) {
				$query->orWhere('psp_id', 'like', '%' . $search . '%');
				$query->orWhere('merchant_id', 'like', '%' . $search . '%');
				$query->orWhere('card_type', 'like', '%' . $search . '%');
				$query->orWhere('card_bin', 'like', '%' . $search . '%');
				$query->orWhere('type', 'like', $search . '%');
				$query->orWhere('status', 'like', $search . '%');
			}
		})
		->orderBy('created_at','desc')
		->paginate($item_per_page);
		
		return $search;
    }
}
