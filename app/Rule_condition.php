<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rule_condition extends Model
{
    protected $fillable =[
        
    ];
	protected $connection = 'mysqlfraud';
	
	public static function _all()
    {
		return Rule_condition::where('status', '=', 'Active')->get();
	}
	public static function details($condition_id)
    {
		return Rule_condition::where('status', '=', 'Active')->where('condition_id', '=', $condition_id)->get();
	}
	
}