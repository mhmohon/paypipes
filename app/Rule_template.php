<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Rule_template extends Model
{
    protected $fillable =[
        
    ];	
	protected $connection = 'mysqlfraud';
	
	public static function _all()
    {
		return Rule_template::where('status', '=', 'Active')->get();
	}
	public static function details($template_id)
    {
		return Rule_template::where('status', '=', 'Active')->where('template_id', '=', $template_id)->get();
	}	
 
}