<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rule_phone_type extends Model
{
    protected $fillable =[
        
    ];
	protected $connection = 'mysqlfraud';
	
	public static function _all()
    {
		return Rule_phone_type::where('status', '=', 'Active')->get();
	}
	
	public static function details($id)
    {
		return Rule_phone_type::where('status', '=', 'Active')->where('id', '=', $id)->get();
	}
 
}