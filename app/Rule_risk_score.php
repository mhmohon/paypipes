<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rule_risk_score extends Model
{
    protected $fillable =[
        
    ];
	protected $connection = 'mysqlfraud';
	
	public static function _all()
    {
		return Rule_risk_score::where('status', '=', 'Active')->get();
	}
	public static function details($risk_rule_id)
    {
		return Rule_risk_score::where('status', '=', 'Active')->where('risk_rule_id', '=', $risk_rule_id)->get();
	}
	public static function detailsbycriteria($criteria_id)
    {
		return Rule_risk_score::where('status', '=', 'Active')->where('criteria_id', '=', $criteria_id)->get();
	}
 
}