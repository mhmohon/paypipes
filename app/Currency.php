<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $table = 'currencies';
    protected $primaryKey = 'currency_id';

    protected $fillable =[
        'currency',
        'cy_code',
        'iso_num',
        'cy_symbol',
        'unicode_decimal',
        'active'
    ];

    public function merchant()
    {
        return $this->hasMany('App\Merchant', 'active', '1');
    }
    public function customer()
    {
        return $this->hasMany('App\Customer', 'active', '1');
    }


    public static function currencylist()
    {
        return Currency::where('active', '=', 1)->orderBy('cy_code', 'asc')->pluck('cy_code', 'cy_code');
    }
}
