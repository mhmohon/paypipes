<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rule_condition_option extends Model
{
    protected $fillable =[
        
    ];
	protected $connection = 'mysqlfraud';
	
	public static function _all()
    {
		return Rule_condition_option::where('status', '=', 'Active')->get();
	}
	public static function details($condition_option_id)
    {
		return Rule_condition_option::where('status', '=', 'Active')->where('condition_option_id', '=', $condition_option_id)->get();
	}	
 
}