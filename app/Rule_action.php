<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rule_action extends Model
{
    protected $fillable =[
        
    ];
	protected $connection = 'mysqlfraud';
	
	public static function _all()
    {
		return Rule_action::where('status', '=', 'Active')->get();
	}
	public static function details($action_id)
    {
		return Rule_action::where('status', '=', 'Active')->where('action_id', '=', $action_id)->get();
	}
 
}