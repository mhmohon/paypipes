<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Compliance_switch extends Model
{
    protected $fillable =[
        'compliance_id',
        'account_id',

    ];

    public function admin()
    {
        return $this->belongsTo('App\Admin');
    }
}
