<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;


class SendTwoFactorCode
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $user;
    public $qrCode;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user, $qrCode=null)
    {
        $this->user = $user;
        $this->qrCode = $qrCode;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return [];
    }
}
