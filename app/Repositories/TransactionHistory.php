<?php

namespace App\Repositories;

use DB;
use App\Merchant;

class TransactionHistory
{
    public function transaction_history($request)
    {
        $merchant_id=$terminal_id=$psp_id=$currency=$start_date=$end_date=$gateway="";

        if (!empty($request->all())) {
            $req= $request->all();
        } elseif (!empty($request->old())) {
            $req= $request->old();
        }


        if (isset($req['merchant_id']) || isset($req['terminal_id']) || isset($req['psp_id'])) {
            $data['start_date']=$start_date = date('Y-m-d', strtotime($req['start_date']));
            $data['end_date']=$end_date = date('Y-m-d', strtotime($req['end_date']));
            $data['currency']= $currency = $req['currency'];


            if (isset($req['merchant_id']) && !empty($req['merchant_id'])) {
                $where['transactions.merchant_id'] = $req['merchant_id'];
                $merchant = Merchant::where('merchant_id', '=', $req['merchant_id'])->first();
                $company_name=@$merchant->company_name;
                $email=@$merchant->email;
                $id_type_key='Merchant ID';
                $id_type_value='merchant_id';
            }

            if (isset($req['terminal_id']) && !empty($req['terminal_id'])) {
                $where['transactions.terminal_id'] = $req['terminal_id'];

                $terminal = DB::table('terminals')
                            ->leftJoin('merchants', 'terminals.merchant_id', '=', 'merchants.merchant_id')
                            ->where('terminals.terminal_id', '=', $req['terminal_id'])
                            ->select('merchants.company_name', 'terminals.email')
                            ->first();
                $company_name=@$terminal->company_name;
                $email=@$terminal->email;

                $id_type_key='Terminal ID';
                $id_type_value='terminal_id';
            }
            if (isset($req['psp_id']) && !empty($req['psp_id'])) {
                $where['transactions.psp_id'] = $req['psp_id'];
                $psp = DB::table('psps')->where('psp_id', '=', $req['psp_id'])->first();
                $company_name=@$psp->company_name;
                $email=@$psp->email;

                $id_type_key='Psp ID';
                $id_type_value='psp_id';
            }
            if (isset($req['gateway']) && !empty($req['gateway'])) {
                $where['transactions.gateway'] = $req['gateway'];
            }

            $where['transactions.transaction_status'] = 'Approved';

            $where['transactions.currency'] = $currency;


            $not_in = ['Authorize', 'ReverseAuthorize'];
            $where_in_credit = ['Purchase', 'Capture', 'Dispute'];
            $where_in_debit = ['Chargeback', 'Credit', 'Refund', 'ReverseCapture', 'ReversePurchase'];


            $data['credit'] =   $credit = DB::table('transactions')->whereDate('created_at', '>=', $start_date)->whereDate('created_at', '<=', $end_date)->where($where)->whereIn('transaction_type', $where_in_credit)->whereNotIn('transaction_type', $not_in)->sum('amount');
            $data['debit'] =   $debit = DB::table('transactions')->whereDate('created_at', '>=', $start_date)->whereDate('created_at', '<=', $end_date)->where($where)->whereIn('transaction_type', $where_in_debit)->whereNotIn('transaction_type', $not_in)->sum('amount');


            $queryResult=$statements= DB::table('transactions')->leftJoin('customer_billings', 'transactions.customer_token', '=', 'customer_billings.customer_token')->leftJoin('customer_cards', 'transactions.card_token', '=', 'customer_cards.card_token')->whereNotIn('transactions.transaction_type', $not_in)->whereDate('transactions.created_at', '>=', $start_date)->whereDate('transactions.created_at', '<=', $end_date)->where($where)
            ->select('transactions.*', 'customer_billings.email', 'customer_billings.first_name', 'customer_billings.last_name', 'customer_billings.phone', 'customer_billings.city', 'customer_billings.country', 'customer_cards.card_type', 'customer_cards.card_bin', 'customer_cards.card_digit');

            $data['queryResult'] = $queryResult;
            $data['statements'] = $statements;

            $data['company_name'] =  $company_name;
            $data['email'] =   $email;

            $data['id_type_key'] =$id_type_key;
            $data['id_type_value'] =$id_type_value;

            return $data;
        }

        $data['company_name']=[];
        $data['email']=[];
        $data['statements']=[];
        $data['credit']=[];
        $data['debit']=[];
        $data['currency']=[];
        $data['start_date']=[];
        $data['end_date']=[];
        return $data;
    }

    public function pagetitle()
    {
        $pagetitle = 'Transactions History';

        return $pagetitle;
    }

    public function download($request, $common, $csv)
    {
        $data=$this->transaction_history($request);

        if (!empty($data['queryResult'])) {
            $queryResult = $data['queryResult'];
            $queryResult = json_decode(json_encode((array) $queryResult->get()->toArray()), true);

            $fields = ['created_at','transaction_id', $data['id_type_value'], 'first_name','last_name','email', 'gateway','transaction_type','currency', 'amount'];
            $headers = ['Datetime','Transaction ID',$data['id_type_key'],'FirstName','LastName', 'Email','Gateway','Transaction Type', 'Currency','amount'];

            $name = 'Transaction_history_'.time();
            if ($csv==1) {
                return $common->dataToCSV($queryResult, $fields, $name, $headers);
            } else {
                $pagetitle=$this->pagetitle();
                $print_type='landscape';
                $mstatement=0;

                return $common->dataToPdf($queryResult, $fields, $name, $headers, $pagetitle, $print_type, $data, $mstatement);
            }
        }
    }
}
