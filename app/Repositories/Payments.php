<?php

namespace App\Repositories;

use App\Transaction;
use Illuminate\Support\Facades\DB;

class Payments
{
    public function payment($request, $data)
    {
        $item_per_page = $data['item_per_page'];


        $req=$search="";
        if (!empty($request->all())) {
            $req= $request->all();
        } elseif (!empty($request->old())) {
            $req= $request->old();
        }


        //dd($request,$request->old(),$request->old('page'));
        if (!empty($req['search'])) {
            $search = $req['search'];
        }

        //DB::enableQueryLog();

        if (empty($search)) {
            $transactions = Transaction::leftJoin('customer_billings', 'transactions.customer_token', '=', 'customer_billings.customer_token')
                    ->leftJoin('maxmind_scores', 'transactions.maxmind_id', '=', 'maxmind_scores.id')
                    ->leftJoin('customer_cards', 'transactions.card_token', '=', 'customer_cards.card_token')
                    ->leftJoin('terminals', 'transactions.terminal_id', '=', 'terminals.terminal_id')
                    ->where(function ($query) use ($req,$data) {
                        if (!empty($req)) {

                        //filter by keyword
                            if (!empty($req['currency'])) {
                                $query->where('transactions.currency', $req['currency']);
                            }
                            if (!empty($req['psp_id'])) {
                                $query->where('transactions.psp_id', $req['psp_id']);
                            }
                            if (!empty($req['merchant_id'])) {
                                $query->where('transactions.merchant_id', $req['merchant_id']);
                            }
                            if (!empty($req['terminal_id'])) {
                                $query->where('transactions.terminal_id', $req['terminal_id']);
                            }
                            if (!empty($req['gateway'])) {
                                $query->where('transactions.gateway', $req['gateway']);
                            }
                            if (!empty($req['status'])) {
                                $query->where('transactions.transaction_status', $req['status']);
                            }
                            if (!empty($req['transaction_type'])) {
                                $query->where('transactions.transaction_type', $req['transaction_type']);
                            }

                            if (!empty($req['date_from']) && !empty($req['date_to'])) {
                                $query->where(function ($q) use ($req) {
                                    $q->whereDate('transactions.created_at', '>=', date('Y-m-d H:i:s', strtotime($req['date_from'])))
                                  ->whereDate('transactions.created_at', '<=', date('Y-m-d H:i:s', strtotime($req['date_to'])));
                                });
                            } elseif (!empty($req['date_from'])) {
                                $query->whereDate('transactions.created_at', '>=', date('Y-m-d H:i:s', strtotime($req['date_from'])));
                            } elseif (!empty($req['date_to'])) {
                                $query->whereDate('transactions.created_at', '<=', date('Y-m-d H:i:s', strtotime($req['date_to'])));
                            }

                            if (!empty($req['amount_from']) && !empty($req['amount_to'])) {
                                $query->where(function ($q) use ($req) {
                                    $q->where('transactions.amount', '>=', $req['amount_from'])
                                  ->where('transactions.amount', '<=', $req['amount_to']);
                                });
                            } elseif (!empty($req['amount_from'])) {
                                $query->where('transactions.amount', '>=', $req['amount_from']);
                            } elseif (!empty($req['amount_to'])) {
                                $query->where('transactions.amount', '<=', $req['amount_to']);
                            }
                        }
                    });
        } else {
            $transactions = Transaction::leftJoin('customer_billings', 'transactions.customer_token', '=', 'customer_billings.customer_token')
            ->leftJoin('maxmind_scores', 'transactions.maxmind_id', '=', 'maxmind_scores.id')
            ->leftJoin('customer_cards', 'transactions.card_token', '=', 'customer_cards.card_token')
            ->leftJoin('terminals', 'transactions.terminal_id', '=', 'terminals.terminal_id')
            ->where(function ($query) use ($search,$data) {
                //filter by keyword
                if (!empty($search)) {
                    $query->orWhere('transactions.transaction_id', 'like', '%'. $search . '%');
                    $query->orWhere('transactions.reference_number', 'like', '%'. $search . '%');
                    $query->orWhere('transactions.request_id', 'like', '%'. $search . '%');
                    $query->orWhere('transactions.order_id', 'like', '%'. $search . '%');
                    if (empty($data['psp_id'])) {
                        $query->orWhere('transactions.psp_id', 'like', '%'. $search . '%');
                    }
                    if (empty($data['merchant_id'])) {
                        $query->orWhere('transactions.merchant_id', 'like', '%'. $search . '%');
                    }
                    if (empty($data['terminal_id'])) {
                        $query->orWhere('transactions.terminal_id', 'like', '%'. $search . '%');
                    }


                    $query->orWhere('transactions.description', 'like', '%' . $search . '%');
                    $query->orWhere('transactions.gateway', 'like', '%' . $search . '%');
                    $query->orWhere('transactions.currency', 'like', '%' . $search . '%');
                    $query->orWhere('transactions.transaction_status', 'like', $search . '%');
                    $query->orWhere('transactions.transaction_type', 'like', $search . '%');
                    $query->orWhere('customer_billings.first_name', 'like', '%' . $search . '%');
                    $query->orWhere('customer_billings.last_name', 'like', '%' . $search . '%');
                    $query->orWhere('customer_billings.email', 'like', '%' . $search . '%');
                    $query->orWhere('customer_cards.card_type', '=', $search);
                    $query->orWhere('customer_cards.card_digit', '=', $search);
                    $query->orWhere('customer_cards.card_bin', '=', $search);
                }
            });
        }
        if (!empty($data['merchant_id'])) {
            $transactions = $transactions->where('transactions.merchant_id', $data['merchant_id']);
        }
        if (!empty($data['terminal_id'])) {
            $transactions = $transactions->where('transactions.terminal_id', $data['terminal_id']);
        }
        if (!empty($data['psp_id'])) {
            $transactions = $transactions->where('transactions.psp_id', $data['psp_id']);
        }
        if (!empty($data['transaction_type'])) {
            $transactions = $transactions->where('transactions.transaction_type', $data['transaction_type']);
        }


        $transactions = $transactions->select('transactions.*', 'maxmind_scores.risk_score', 'customer_billings.email', 'customer_billings.first_name', 'customer_billings.last_name', 'customer_billings.ip_address as customer_ip', 'customer_cards.card_type', 'customer_cards.card_bin', 'customer_cards.card_digit', 'terminals.name')
                ->orderBy('transactions.created_at', 'desc')->paginate($item_per_page);

        //dd(DB::getQueryLog($transactions));
        //die();
        //
        return $transactions;
    }



    public function pagetitle()
    {
        $pagetitle = 'Transaction';

        return $pagetitle;
    }

    public function download($request, $common, $csv, $data)
    {
        $data['item_per_page'] = !empty($request->rows)?$request->rows:ROW_PER_PAGE;


        $queryResult=$this->payment($request, $data);

        if (!empty($queryResult)) {
            $queryResult = json_decode(json_encode((array) $queryResult->toArray()), true);

            $fields = ['created_at','transaction_id',  'first_name','last_name','email', 'gateway','transaction_type','transaction_status','currency', 'amount', 'card_type', 'mask_number'];
            $headers = ['Datetime','Transaction ID','FirstName','LastName', 'Email','Gateway','Transaction Type','Status', 'Currency','Amount','Brand','Number'];

            if (!empty($data['fields'])) {
                $fields=array_merge($data['fields'], $fields);
            }
            if (!empty($data['headers'])) {
                $headers=array_merge($data['headers'], $headers);
            }

            $name = 'Payments_'.time();

            if ($csv==1) {
                return $common->dataToCSV($queryResult['data'], $fields, $name, $headers);
            } else {
                $pagetitle=$this->pagetitle();
                $print_type='landscape';
                $mstatement=0;
                return $common->dataToPdf($queryResult['data'], $fields, $name, $headers, $pagetitle, $print_type, $data, $mstatement);
            }
        }
    }
}
