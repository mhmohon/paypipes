<?php

namespace App\Repositories\Criteria\Maxmind;

use Illuminate\Support\Facades\Input;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class MaxmindAdvSearchCriteria.
 *
 * @package namespace App\Repositories\Criteria\Maxmind;
 */
class MaxmindAdvSearchCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param string              $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $email = Input::get('email')?? '';

        if (!empty(Input::get('date_from'))) {
            $model = $model->where('created_at', '>=', date('Y-m-d H:i:s', strtotime(Input::get('date_from'))));
        }

        if (!empty(Input::get('date_to'))) {
            $model = $model->where('created_at', '<=', date('Y-m-d H:i:s', strtotime(Input::get('date_to'))));
        }

        if (!empty(Input::get('score_from'))) {
            $model = $model->where('risk_score', '>=', Input::get('score_from'));
        }

        if (!empty(Input::get('score_to'))) {
            $model = $model->where('risk_score', '<=', Input::get('score_to'));
        }

        if (!empty($email)) {
            $model = $model->whereHas('customerBilling', function ($query) use ($email) {
                $query->where('email', '=', $email);
            });
        }
        return $model;
    }
}
