<?php
namespace App\Repositories;

use DB;
use App\Transaction;
use App\Psp;
use App\Merchant;
use App\Terminal;

class Statements
{
    public function statementlist($request, $data)
    {
        if (!empty($request->all())) {
            $req= $request->all();
        } elseif (!empty($request->old())) {
            $req= $request->old();
        }

        if (!empty($request->psp_id)) {
            $fld='psp';
            $val=$request->psp_id;
            $name='company_name';
        } elseif (!empty($request->terminal_id)) {
            $fld='terminal';
            $val=$request->terminal_id;
            $name='website';
        } elseif (!empty($request->merchant_id)) {
            $fld='merchant';
            $val=$request->merchant_id;
            $name='company_name';
        }
        $month="";

        if (!empty($req['search'])) {
            $search = $req['search'];
        } else {
            $search = "";
        }


        if (!empty($search) && !is_numeric($search)) {
            $month=(int)date('m', strtotime($search));
        }


        $statements = DB::table($fld.'_statements')
        ->leftJoin($fld.'s', $fld.'_statements.'.$fld.'_id', '=', $fld.'s.'.$fld.'_id')
        ->where(function ($query) use ($search,$month) {
            if (!empty($search)) {
                $query->orWhere('currency', 'like', '%' . $search . '%');
                $query->orWhere('year', 'like', '%' . $search . '%');
                if (!empty($month)) {
                    $query->orWhere('month', 'like', '%' . $month . '%');
                }
            }
        })
        ->where($fld.'s.'.$fld.'_id', '=', $val)->select($fld.'_statements.*', $fld.'s.'.$name, $fld.'s.email', $fld.'s.name')->orderBy('created_at', 'desc');

        return $statements;
    }
    public function statement($request, $data)
    {
        if (!empty($request->psp_id)) {
            $fld='psp';
            $val=$request->psp_id;
        } elseif (!empty($request->terminal_id)) {
            $fld='terminal';
            $val=$request->terminal_id;
            $name='website';
        } elseif (!empty($request->merchant_id)) {
            $fld='merchant';
            $val=$request->merchant_id;
            $name='company_name';
        }


        $stment['statement']=$statement = DB::table($fld.'_statements')->where('id', '=', $request->statement_id)->first();

        $where['transactions.'.$fld.'_id'] = $val;
        $where['transactions.transaction_status'] = 'Approved';
        $where['transactions.currency'] = $statement->currency;

        $stment['statements']=Transaction::statementdetails($where, $statement->start_date, $statement->end_date);

        return $stment;
    }






    public function download($request, $common, $csv, $data)
    {
        $fname=$hname="";
        if (!empty($request->psp_id)) {
            $fld='psp';
            $val=$request->psp_id;
            $tbl='psps';
            $fname="company_name";
            $hname="Company Name";
        } elseif (!empty($request->terminal_id)) {
            $fld='terminal';
            $val=$request->terminal_id;
            $tbl='terminals';
            $fname="website";
            $hname="Website";
        } elseif (!empty($request->merchant_id)) {
            $fld='merchant';
            $val=$request->merchant_id;
            $tbl='merchants';
            $fname="company_name";
            $hname="Company Name";
        }

        if (empty($data)) {
            $data = [];
        }
        if (isset($request->statement_id)) {
            $stment=$this->statement($request, $data);

            $queryResult=$stment['statements'];
            $statement=$stment['statement'];
            $data['statement'] = $statement;

            if (!empty($request->terminal_id)) {
                $data['company']=Merchant::where('merchant_id', '=', $statement->merchant_id)->first();
                $data['websites']= DB::table('terminals')->where('terminal_id', '=', $statement->terminal_id)->value('website');
            } else {
                $data['company']=$company=DB::table($tbl)->leftJoin('countries', 'countries.country_code', '=', $tbl.'.country')->where($fld.'_id', '=', $val)->select($tbl.'.*', 'countries.country_name')->first();
                if (!empty($request->merchant_id)) {
                    $websites = DB::table('terminals')
                    ->leftJoin('merchants', 'terminals.merchant_id', '=', 'merchants.merchant_id')
                    ->where('merchants.merchant_id', '=', $statement->merchant_id)
                    ->select('terminals.website')
                    ->get();
                    $data['websites']=$websites->pluck('website');
                }
                if (!empty($request->psp_id)) {
                    $data['websites']=$company->website;
                }
            }



            $queryResult = json_decode(json_encode((array) $queryResult->get()->toArray()), true);
            $fields = ['created_at','transaction_id',  'first_name','last_name','email', 'gateway','transaction_type','currency', 'amount'];
            $headers = ['Datetime','Transaction ID','FirstName','LastName', 'Email','Gateway','Transaction Type', 'Currency','Credit','Debit'];

            $name = 'statement_details_'.time();
            $pagetitle="Monthly Statement";
            $print_type='landscape';
            $mstatement=1;
            $debit=1;
        } else {
            $queryResult=$this->statementlist($request, $data);

            $queryResult = json_decode(json_encode((array) $queryResult->get()->toArray()), true);

            $fields = ['year','month', $fname, 'email', 'currency', 'credits','debits','total'];
            $headers = ['Year','Month',$hname, 'Email', 'Currency','Credit','Debit','Total'];
            $name = 'statements_'.time();
            $pagetitle="Statement Summary";
            $print_type='portrait';
            $mstatement=0;
            $debit=0;
        }

        if ($csv==1) {
            return $common->dataToCSV($queryResult, $fields, $name, $headers, $debit);
        } else {
            return $common->dataToPdf($queryResult, $fields, $name, $headers, $pagetitle, $print_type, $data, $mstatement, $debit);
        }
    }
}
