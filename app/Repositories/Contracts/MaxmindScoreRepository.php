<?php

namespace App\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface MaxmindScoreRepository.
 *
 * @package namespace App\Repositories\Contracts;
 */
interface MaxmindScoreRepository extends RepositoryInterface
{
    //
}
