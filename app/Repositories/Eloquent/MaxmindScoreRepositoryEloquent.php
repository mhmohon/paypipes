<?php

namespace App\Repositories\Eloquent;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use App\Repositories\Contracts\MaxmindScoreRepository;
use App\Entities\Maxmind\MaxmindScore;

/**
 * Class MaxmindScoreRepositoryEloquent.
 *
 * @package namespace App\Repositories\Eloquent;
 */
class MaxmindScoreRepositoryEloquent extends BaseRepository implements MaxmindScoreRepository
{
    protected $fieldSearchable = [
        'max_id'=>'like',
        'risk_score'=>'like', // Default Condition "="
        'ipaddress'=>'like',
        'created_at'=>'like',
        'transaction.transaction_id'=>'like',
        'customerCard.card_bin'=>'like',
        'customerCard.card_digit'=>'like',
        'customerBilling.email'=>'like'
    ];
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return MaxmindScore::class;
    }

    public function getShow($transactionid, $id)
    {
        return $this->with(['maxmindBillingAddress','maxmindCard','maxmindEmail','maxmindIpCity','maxmindIpContinent','maxmindIpCountry','maxmindIpLocation','maxmindIpPostal','maxmindIpRegisteredCountry','maxmindIpSubdivision','maxmindIpTrait','maxmindWarning','customerBilling','customerCard','transaction' => function ($query) use ($transactionid) {
            $query->where('id', $transactionid);
        }
        ])->find($id);
    }


    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
}
