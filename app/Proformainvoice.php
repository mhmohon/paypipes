<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proformainvoice extends Model
{
    protected $table = 'proforma_invoices';

    protected $fillable =[
        'proforma_id',
        'bank_account_id',
        'merchant_id',
        'psp_id',
        'invoice_date',
        'due_date',
        'description',
        'item_others_fee',
        'amount_others_fee',
        'qty_others_fee',
        'tax',
        'total',
        'currency',
        'status',
        'review',
    ];

    public static function searchAdmin($request)
    {
        $item_per_page = !empty($request->rows)?$request->rows:ROW_PER_PAGE;


        $invoices = Proformainvoice::where(function ($query) use ($request) {
            if (($search = $request->get('search'))) {
                $query->orWhere('account_id', 'like', '%' . $search . '%');
                $query->orWhere('merchant_id', 'like', '%' . $search . '%');
                $query->orWhere('psp_id', 'like', '%' . $search . '%');
                $query->orWhere('proforma_id', 'like', '%' . $search . '%');
                $query->orWhere('review', 'like', '%' . $search . '%');
                $query->orWhere('currency', 'like', '%' . $search . '%');
                $query->orWhere('status', 'like', '%' . $search . '%');
                $query->orWhere('description', 'like', '%' . $search . '%');
            }
        })
        ->orderBy('id', 'desc')
        ->paginate($item_per_page);

        return $invoices;
    }
    public static function search($request, $where)
    {
        $item_per_page = !empty($request->rows)?$request->rows:ROW_PER_PAGE;


        $invoices = Proformainvoice::where(function ($query) use ($request) {
            if (($search = $request->get('search'))) {
                $query->orWhere('merchant_id', 'like', '%' . $search . '%');
                $query->orWhere('psp_id', 'like', '%' . $search . '%');
                $query->orWhere('proforma_id', 'like', '%' . $search . '%');
                $query->orWhere('review', 'like', '%' . $search . '%');
                $query->orWhere('currency', 'like', '%' . $search . '%');
                $query->orWhere('status', 'like', '%' . $search . '%');
                $query->orWhere('description', 'like', '%' . $search . '%');
            }
        })
        ->where($where)
        ->orderBy('id', 'desc')
        ->paginate($item_per_page);

        return $invoices;
    }
    public static function proformaid()
    {
        $proforma_id=date('y');
        $proformaid =(int)$invoiceid=Proformainvoice::where('proforma_id', 'like', $proforma_id . '%')->orderBy('proforma_id', 'desc')->value('proforma_id');
        if ($proformaid < 1) {
            $proforma_id.='0001';
        } else {
            $proforma_id = $proformaid + 1;
        }

        return $proforma_id;
    }
}
