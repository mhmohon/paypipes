<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rule_currency extends Model
{
    protected $fillable =[
        
    ];
	protected $connection = 'mysqlfraud';
	
	public function merchant()
    {
        return $this->hasMany('App\Merchant','active','1');
    }
	public function customer()
    {
        return $this->hasMany('App\Customer','active','1');
    }
	
	
	public static function  currencylist()
    {
		return Rule_currency::where('active', '=', 1)->orderBy('cy_code','asc')->pluck('cy_code', 'cy_code');
	}
	
 
}