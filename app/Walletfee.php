<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Walletfee extends Model
{
    protected $fillable =[
        'wallet_fee_id',
		'name',
        'currency',
        'creation_fee',
        'monthly_fee',
        'send_fee',
		'send_flat_fee',
        'receive_fee',
		'receive_flat_fee',
        'withdraw_fee',
		'withdraw_flat_fee',
        'status',
    ];
    
    public function wallets()
    {
        return $this->hasMany('App\Wallet', 'wallet_fee_id', 'id');
    }
}
