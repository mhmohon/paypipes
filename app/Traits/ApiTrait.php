<?php

namespace App\Traits;

use Illuminate\Http\Request;

trait ApiTrait
{

    /**
     * Determines if request is an api call.
     *
     * If the request URI contains '/api/v'.
     *
     * @param Request $request
     * @return bool
     */
    protected function isApiCall(Request $request)
    {
        if ($request->ajax()) {
            return true;
        } elseif (strpos($request->getUri(), '/api/') == true || strpos($request->getUri(), '/oauth/') == true) {
            return true;
        } else {
            return false;
        }
    }
}
