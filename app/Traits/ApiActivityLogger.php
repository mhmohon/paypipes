<?php

namespace App\Traits;

use App\Entities\ApiActivity;
use Symfony\Component\HttpKernel\DataCollector\collect;

trait ApiActivityLogger
{
    /**
    * Laravel Logger Log Activity
    * @param  string $description
    * @return void
    *
    *      transaction_id  => date('Ymdhis'),
    */

    public static function apiActivity($requestdata, $description = '', $sendTo = 'PayPipes', $order_id='', $transaction_id='')
    {
        if (is_object($requestdata)) {
            $requestdata = collect($requestdata)->map(function ($x) {
                return (array) $x;
            })->toArray();
        } elseif (!is_array($requestdata)) {
            $result = json_decode($requestdata, true);
            if (json_last_error() === JSON_ERROR_NONE) {
                $requestdata = $result;
            }
        } else {
            if (isset($requestdata['card']['number'])) {
                $requestdata['card']['number'] = substr($requestdata['card']['number'], -4);
            }
            if (isset($requestdata['number'])) {
                $requestdata['number'] = substr($requestdata['number'], -4);
            }
            if (isset($requestdata['security_code'])) {
                $requestdata['security_code'] = 'XXX';
            }
        }


        $data = [
            'transaction_id'  => $requestdata['transaction_id']??$transaction_id??null,
            'order_id'        => $requestdata['order_id']??$order_id??null,
            'description'     => (isset($requestdata['description']) && !empty($requestdata['status_code']))?$requestdata['description']:$description,
            'transaction_type'=> \Request::segment(3),
            'sendTo'          => $sendTo,
            'body'            => is_array($requestdata)?http_build_query($requestdata):$requestdata,
            'status'          => $requestdata['status']??null,
            'status_code'     => $requestdata['status_code']??null,
            'route'           => \Request::fullUrl(),
            'ipAddress'       => \Request::ip(),//self::getIP(),//
            'userAgent'       => \Request::userAgent(),
            'locale'          => \Request::header('accept-language'),
            'referer'         => \Request::header('referer')
        ];

        self::storeApiActivity($data);
    }

    /**
     * Store activity entry to database
     * @param  array $data
     * @return void
     */
    private static function storeApiActivity($data)
    {
        ApiActivity::create($data);
    }
}
