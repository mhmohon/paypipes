<?php

namespace App\Traits;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request as Psr7Request;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;

trait HttpRequest
{
    public function httpOauthRequest($url, $param)
    {
        $headers = [
                        'Content-type'  => 'application/json',
                        'Accept'        => 'application/json',
                        'Cache-Control' => 'no-cache',
                        'User-Agent'    => $_SERVER['HTTP_USER_AGENT'],
                    ];



        try {
            $http = new Client(['curl' => [ CURLOPT_SSL_VERIFYPEER => false],  'verify' => false ]);

            $request = new Psr7Request('POST', $url, $headers, $param);
            $response = $http->send($request, ['http_errors' => false]);
            $result = $response->getBody()->getContents();
            $auth = json_decode((string) $response->getBody());

            return $auth->access_token?? null;
        } catch (ClientException $e) {
            return $auth->access_token = $e->getCode."\r\n". $e->getMessage()."\r\n";
        } catch (ServerException $e) {
            return $auth->access_token = $e->getCode."\r\n". $e->getMessage()."\r\n";
        }
    }

    public function httpPostRequest($url, $param, $access_token = '', $returnyes = 1)
    {
        $headers = [
                        'Content-type'  => 'application/json',
                        'Accept'        => 'application/json',
                        'Cache-Control' => 'no-cache',
                        'User-Agent'    => $_SERVER['HTTP_USER_AGENT'],
                        'Authorization' => 'Bearer '. $access_token,
                    ];

        try {
            //
            $http = new Client([ 'curl' => [ CURLOPT_SSL_VERIFYPEER => false], 'verify' => false ]);

            $request = new Psr7Request('POST', $url, $headers, $param);
            $response = $http->send($request, ['http_errors' => false]);
            $result = $response->getBody()->getContents();
            $response_array = json_decode((string) $result, true);

            if (!$response_array) {
                echo $result;
            } else {
                return $response_array['data']?? $response_array;
            }
        } catch (ClientException $e) {
            echo  $e->getCode."\r\n". $e->getMessage()."\r\n";
        } catch (ServerException $e) {
            echo $e->getCode."\r\n". $e->getMessage()."\r\n";
        }
    }


    public function httpGetRequest($url, $param, $access_token = '', $returnyes = 1)
    {
        $headers = [
                        'Content-type'  => 'application/json',
                        'Accept'        => 'application/json',
                        'Cache-Control' => 'no-cache',
                        'User-Agent'    => $_SERVER['HTTP_USER_AGENT'],
                        'Authorization' => 'Bearer '. $access_token,
                    ];

        try {
            //'curl' => [ CURLOPT_SSL_VERIFYPEER => false],
            $http = new Client([ 'verify' => false ]);

            $request = new Psr7Request('GET', $url, $headers, $param);
            $response = $http->send($request, ['http_errors' => false]);
            $result = $response->getBody()->getContents();
            $response_array = json_decode((string) $result, true);

            if (!$response_array) {
                echo $result;
            } else {
                return $response_array['data']?? $response_array;
            }
        } catch (ClientException $e) {
            echo  $e->getCode."\r\n". $e->getMessage()."\r\n";
        } catch (ServerException $e) {
            echo $e->getCode."\r\n". $e->getMessage()."\r\n";
        }
    }
}
