<?php

namespace App\Traits\auth;

use Illuminate\Http\Request;
use Validator;
use App\Bin;
use DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

trait CustomResetPassword
{
    public function resetValidation(Request $request)
    {
        $rules = [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|regex:/^.*(?=.{3,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$/|between:8,16',
        ];

        $niceNames = [
                'token' => 'Token',
                'email' => 'Email',
                'password' => 'Password',
            ];

        $customMessages = [
            "password.between" => "The password must contain at least 1 capital letter, 1 small letter and 1 number. The password must be 8 to 16 characters long.",
            "password.regex" => "The password must contain at least 1 capital letter, 1 small letter and 1 number. The password must be 8 to 16 characters long.",
            'password.required' => 'The password must contain at least 1 capital letter, 1 small letter and 1 number. The password must be 8 to 16 characters long.',
        ];

        $validator = Validator::make($request->all(), $rules, $customMessages);
        $validator->setAttributeNames($niceNames);

        if($validator->fails()){

            return $validator;

        }else{

            return true;
        }

    }

    public function changePassValidation(Request $request)
    {
        $rules = [
            'old_password' => 'required|between:6,16',
            'password' => 'required|regex:/^.*(?=.{3,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$/|between:8,16|confirmed'
        ];

        $niceNames = [
            'old_password' => 'Old Password',
            'password' => 'Password',
        ];

        $customMessages = [
            "password.between" => "The password must contain at least 1 capital letter, 1 small letter and 1 number. The password must be 8 to 16 characters long.",
            "password.regex" => "The password must contain at least 1 capital letter, 1 small letter and 1 number. The password must be 8 to 16 characters long.",
            'password.required' => 'The password must contain at least 1 capital letter, 1 small letter and 1 number. The password must be 8 to 16 characters long.',
        ];


        $validator = Validator::make($request->all(), $rules, $customMessages);
        $validator->setAttributeNames($niceNames);

        if($validator->fails()){

            return $validator;

        }else{

            return true;
        }

    }

    public function attemptResetPassword(Request $request, $user, $user_id)
    {

        if($this->checkPasswordHistory($request, $user_id)){

            $password = bcrypt($request->password);
        
            //Change Password
            $user->password = $password;

            $user->password_date = date('Y-m-d');
            if ($user->status == 'TempLock') {
                $user->status = 'Active';
            }
            $user->bad_login = 0;
            $user->save();

            // DB::table('password_resets')->where('email', $request->email)
            //     ->where('token', $request->token)->delete();

            $this->savePasswordHistory($user_id, $password);

            $this->common->flash_message('success', 'Password changed successfully');
            return redirect("$this->userToRedirect/login")->send();

        }else{
            // The passwords matches
            $this->common->flash_message('danger', 'Your new password can not be same as any of your recent passwords. Please choose a new password.');
            return redirect()->back()->send();
        }

    }
    // For change password
    public function attemptChangePassword(Request $request, $user, $user_id)
    {
        //Match for old password
        if (!password_verify($request->old_password, $user->password)) {

            session()->flash('toastr_alert', '<b>Failed!</b> Incorrect old password');
            session()->flash('failed', true);
            return redirect()->back()->withError('Incorrect old password')->send();
                
        }elseif($this->checkPasswordHistory($request, $user_id)){

            $password = bcrypt($request->password);
        
            //Change Password
            $user->password = $password;

            $user->password_date = date('Y-m-d');
            
            $user->bad_login = 0;

            $user->save();

            $this->savePasswordHistory($user_id, $password);

            session()->flash('toastr_alert', '<b>Great!</b> Password has been changed');
            session()->flash('success', true);
            return Redirect::to(URL::previous())->send();

        }else{

            session()->flash('toastr_alert', '<b>Failed!</b> Your new password can not be same as any of your recent passwords. Please choose a new password');
            session()->flash('failed', true);
            return Redirect()->back()->withError('Your new password can not be same as any of your recent passwords. Please choose a new password')->send();
            
        }

    }

    public function checkPasswordHistory(Request $request, $user_id)
    {
        $passwordHistories = Bin::where(['user_id' => $user_id, 'user_type'=> "$this->userToRedirect", 'field'=>'password'])->orderBy('id', 'desc')->take(env("PASSWORD_HISTORY_NUM", 5))->pluck('value')->toArray();

        //Check Password History
        foreach($passwordHistories as $passwordHistory){
            if(password_verify($request->password, $passwordHistory)){

                return false;
            }
        }

        return true;
    }

    public function savePasswordHistory($user_id, $password)
    {
        return DB::table('bin')->insert(['user_id' => $user_id, 'field' => 'password', 'value' => $password, 'user_type' => "$this->userToRedirect", 'date' => date('Y-m-d')]);

    }
}