<?php

namespace App\Traits\auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Auth;
use Session;


trait CustomAuthenticatesUser
{
    use RedirectsUsers;

    public $user_id;
    public $userInfo;
    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function checkUserLogin(Request $request, $user, $user_id)
    {
        $this->userInfo = $user;
        $this->user_id = $user_id;
        // Get the minutes of user update
        $diff_min = $this->diff_min($user);

        $this->validateLogin($request);
        
        if(!password_verify($request->password, $user->password)) {
            
            // Check if user was successfully loaded, that the password matches
            $this->common->flash_message('danger', 'Your password is not match.');

            if($user->bad_login >= 5){
                
                $this->checkBadLogin($user, $diff_min);

            }else{

                $this->incrementBadLogins($user);
            }
            
            return redirect()->intended($this->redirectLogin());
            
        }else{
            $this->checkBadLogin($user, $diff_min);
            return $this->userLogin($user, $request);
        }
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|string',
            'password' => 'required|string',
        ]);
    }

    /**
     * Clear the bad login of authenticated user.
     *
     * @return string
     */
    public function userLogin($user, Request $request)
    {
        if ($this->checkUserStatus($user)) {

            if (password_verify($request->password, $user->password)) {

                return $this->makeLoginAttempt($user, $request);
                
            } 
        } else {

            return redirect()->intended($this->redirectLogin());
        }
    }

    /**
     * Clear the bad login of authenticated user.
     *
     * @return string
     */
    public function makeLoginAttempt($user, Request $request)
    {
        $this->changTempUserStatus($user, $this->diff_min($user));
        
        if($this->userToRedirect == 'account'){
            if ($user->verify_token != '') {
                $code = $user->verify_token;
                $data['message'] = 'Please verify your email first. You can send resend verification link by clicking below button';
                $data['resend_link'] = url('account/resend-verify-email/'.$code);
                return view('account.auth.resend_verify_email', $data);
                exit();
            } 
        }
        if($this->userToRedirect == 'account'){

            $rep = $this->common->password_expired($this->userToRedirect, $this->userInfo->id);

        }else{

            $rep = $this->common->password_expired($this->userToRedirect, $this->user_id);

        }
        
        if ($rep) {

            Session::put('uid', $this->user_id);
            return redirect($this->userToRedirect.'/password/change');

        } elseif ($user->two_step != 'no') {

            Session::put('uid', $this->user_id);
            return $this->twoStepInfoSend($request);
            

        } elseif($this->LoginAttempt($request)) {

            $this->clearBadLogin($user, $this->diff_min($user));
            return redirect()->intended($this->redirectTo);
        }
    }

    
    /**
     * Change Status of authenticated user TempLock to active.
     *
     * @return string
     */
    public function checkUserStatus($user)
    {
        if($user->status == 'TempLock' && $this->diff_min($user) <= '30'){

            $this->common->flash_message('danger', 'Your Account Temporary Locked. Please try again after 30 min');

            $this->incrementBadLogins($user);
            return false;

        }elseif($user->status == 'Locked'){
            
            $this->common->flash_message('danger', 'Your Account is Locked. Please contact with support');
            return false;

        }elseif($user->status == 'Inactive'){

            $this->common->flash_message('danger', 'Log In Failed. Your Account is Inactive');
            return false;

        }else {

            return true;
        }

    }
    /**
     * Change Status of authenticated user TempLock to active.
     *
     * @return string
     */
    public function changTempUserStatus($user, $diff_min)
    {
        if ($user->status == 'TempLock' && $diff_min >= '30') {

            $user->status = 'Active';

            $user->updated_by = "System";
            $user->bad_login = 0;
            $user->save();
            return true;
        }

        return false;

    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    public function LoginAttempt(Request $request)
    {
        $this->guard()->attempt(['email' => $request->email, 'password' => $request->password]);
        
        return redirect($this->userToRedirect);
    }

    /**
     * Return minutes of user login attempt
     *
     * @return string
     */
    public function diff_min($user)
    {
        return $this->common->calculate_min($user->updated_at);
    }

    /**
     * Increase the bad login of authenticated user.
     *
     * @return string
     */
    public function incrementBadLogins($user)
    {
        $user->bad_login = $user->bad_login + 1;
        $user->updated_by = "System";
        $user->save();
    }


    /**
     * Clear the bad login of authenticated user.
     *
     * @return string
     */
    public function checkBadLogin($user, $diff_min)
    {
        if ($user->bad_login >= 5 && $diff_min <='5') {
            if ($user->bad_login >= 10) {
                $user->status = 'Locked';
                $this->common->flash_message('danger', 'Your Account is Locked. Please contact with support');
            } else {
                $user->status = 'TempLock';
                $this->common->flash_message('danger', 'Your Account Temporary Locked. Please try again after 30 min');
            }

            $this->incrementBadLogins($user);
        }
    }

    /**
     * Clear the bad login of authenticated user.
     *
     * @return string
     */
    public function clearBadLogin($user, $diff_min)
    {
        
        if ($diff_min > '5') {
            $user->bad_login = 0;
            $user->updated_by = "System";
            $user->save();
        }
    }


    
}