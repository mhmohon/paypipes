<?php

namespace App\Traits\auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use PragmaRX\Google2FA\Google2FA;
use Session;
use App\Events\SendTwoFactorCode;
use Illuminate\Support\Facades\Validator;
use App\Traits\RealIP;

trait CustomTwoStepAuthenticates
{
    use RealIP;

    public function setTwoStep(Request $request, $user)
    {
        //Save user two step type
        $user->two_step = $request->two_step_type;

        if ($request->two_step_type == 'ip') {

            $user->ip = $request->ip_address;

        } elseif ($request->two_step_type == 'qr') {

            $secret = $this->sendTwoStepQrCode($user);

            $user->secret = $secret;
            
        }
        $user->save();

        session()->flash('toastr_alert', '<b>Great!</b> Two Factor Authentication successfully set.');
        session()->flash('success', true);
        return Redirect::to(URL::previous())->send();
    }

    public function sendTwoStepCode($user)
    {
        if ($user->two_step == 'ip') {

            return $this->checkTwoFactorIp($user);

        }elseif ($user->two_step == 'qr' || $user->two_step == 'email') {

            if($user->two_step == 'qr'){

                $this->sendTwoStepQrCode($user);

            }else{
                
                $this->sendTwoStepEmailCode($user);
            }

            $user->save();
            return redirect($this->userToRedirect.'/password/two-step');
            
        }
    }

    public function checkTwoFactorCode(Request $request, $user)
    {
        $td = $user->secret_date+300 - time();
        if ($user->two_step == 'qr') {
            
            return $this->checkTwoFactorQrCode($request, $user);

        } elseif ($td < 0) {

            $this->common->flash_message('danger', 'Time exceeded');
            return redirect($this->redirectToLogin);

        } elseif ($user->two_step == 'email') {

            return $this->checkTwoFactorEmailCode($request, $user);
            
        } else {

            return redirect($this->redirectToLogin);
        }
    }

    public function fieldValidation(Request $request)
    {
        $rules = [
            'code' => 'required|min:6',
        ];

        $niceNames = [
            'code' => 'Code'
        ];

        $validator = Validator::make($request->all(), $rules);
        $validator->setAttributeNames($niceNames);

        if($validator->fails()){

            return $validator;

        }else{

            return true;
        }

    }

    //Send TwoFactor email authentication code function
    public function sendTwoStepEmailCode($user)
    {
        $code = $this->encdec->makeRandomPassword2(6);
        $user->secret = $code;
        $user->secret_date = time();

        //Send email with TwoFactor authentication code
        event(new SendTwoFactorCode($user));
        
        return true;
    }

    //Send TwoFactor Google authentication code function
    public function sendTwoStepQrCode($user)
    {
        $google2fa = new Google2FA();
        $secret = $google2fa->generateSecretKey();//32
        $google2fa_url = $google2fa->getQRCodeGoogleUrl(
            config('global.company'),
            $user->email,
            $secret
        );

        $user->secret = $secret;
        //Send email with TwoFactor authentication code
        event(new SendTwoFactorCode($user, $google2fa_url));
        
        return true;
    }

    //Check TwoFactor IP authentication function
    public function checkTwoFactorIp($user)
    {
        $now_ip = $this->getIP();
        $user_ip = explode(',', $user->ip,);

        if (in_array($now_ip, $user_ip)) {

            Session::flush();
            $this->guard()->loginUsingId($user->id);
            return redirect()->intended($this->userToRedirect);
            
        } else {

            $this->common->flash_message('danger', 'Ip Mismatched');
            return redirect($this->redirectToLogin);
        }
    }

    //Check TwoFactor Google authentication code function
    public function checkTwoFactorQrCode(Request $request, $user)
    {
        $window = 8;
        $code = $request->code;
        $google2fa = new Google2FA();
        $valid = $google2fa->verifyKey($user->secret, $code, $window);
        if ($valid) {
            Session::flush();
            $this->guard()->loginUsingId($user->id);
            return redirect()->intended($this->userToRedirect);

        } else {
            $this->common->flash_message('danger', 'Invalid validation code');
            return redirect($this->userToRedirect.'/password/two-step');
        }
    }
    //Check TwoFactor Google authentication code function
    public function checkTwoFactorEmailCode(Request $request, $user)
    {
        $code = $request->code;
        if ($code == $user->secret) {
            Session::flush();
            
            $this->guard()->loginUsingId($user->id);

            return redirect()->intended($this->userToRedirect);
        } else {
            $this->common->flash_message('danger', 'Invalid Code');
            return redirect($this->userToRedirect.'/password/two-step');
        }
    }

}