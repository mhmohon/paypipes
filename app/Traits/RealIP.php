<?php

namespace App\Traits;

trait RealIP
{
    public function getRealIP()
    {
        $url='https://api.ipify.org';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);

        $ip = curl_exec($ch);

        if (curl_error($ch)) {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        curl_close($ch);

        return $ip;
    }

    public function getIP()
    {
        if (config('app.env') != 'local') {
            return $this->getRealIpAddr();
        } else {
            return $this->getCurlIP();
        }
    }

    public function getCurlIP()
    {
        $url='https://api.ipify.org';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);

        $ip = curl_exec($ch);

        if (curl_error($ch)) {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        curl_close($ch);

        return $ip;
    }

    public function getRealIpAddr()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip=$_SERVER['HTTP_CLIENT_IP']; // share internet
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip=$_SERVER['HTTP_X_FORWARDED_FOR']; // pass from proxy
        } else {
            $ip=$_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }
}
