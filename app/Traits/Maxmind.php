<?php
namespace App\Traits;

use DB;
use Carbon\Carbon;

trait Maxmind
{
    protected $maxmind_id;
    protected $transDetails;

    public function createMaxmindScore($maxResponse, $transDetails)
    {
        $this->maxmind_id = 0;

        if (isset($maxResponse->riskScore)) {
            $score=[];
            $score['customer_token']=$transDetails->customer_token;
            $score['card_token']=$transDetails->card_token;
            $score['risk_score']=$maxResponse->riskScore;
            $score['max_id'] = $maxResponse->id;
            $score['ip_risk_score']=$maxResponse->ipAddress->risk;
            $score['ipaddress']=$transDetails->ip_address;
            $score['created_at']=Carbon::now();

            $this->maxmind_id =  DB::table('maxmind_scores')->insertGetId($score);
        }

        return $this->maxmind_id;
    }

    public function createMaxmindWarning($maxResponse)
    {
        foreach ($maxResponse as $warnings) {
            $warning=[];
            $warning['maxmind_id']=$this->maxmind_id;
            $warning['created_at']=Carbon::now();
            $warning['code']=$warnings->code;
            $warning['warning']=$warnings->warning;

            DB::table('maxmind_warnings')->insert($warning);
        }
    }

    public function createMaxmindBillingAddress($maxResponse)
    {
        foreach ($maxResponse as $maxBillings) {
            $maxBilling=[];
            $maxBilling['maxmind_id']=$this->maxmind_id;
            $maxBilling['created_at']=Carbon::now();
            $maxBilling['latitude']=$maxBillings['latitude']?? null;
            $maxBilling['longitude']=$maxBillings['longitude']?? null;
            $maxBilling['distance_to_ip_location']=$maxBillings['distance_to_ip_location']?? null;
            $maxBilling['is_in_ip_country']=$maxBillings['is_in_ip_country']?? null;
            $maxBilling['is_postal_in_city']=$maxBillings['is_postal_in_city']?? null;

            DB::table('maxmind_billing_addresses')->insert($maxBilling);
        }
    }

    public function createMaxmindCard($maxResponse)
    {
        foreach ($maxResponse as $maxCards) {
            $maxCard=[];
            $maxCard['maxmind_id']=$this->maxmind_id;
            $maxCard['created_at']=Carbon::now();
            $maxCard['issuer']=$maxCards['issuer']['name']?? null;
            $maxCard['brand']=$maxCards['brand']?? null;
            $maxCard['country']=$maxCards['country']?? null;
            $maxCard['is_issued_in_billing_address_country']=$maxCards['is_issued_in_billing_address_country']?? null;
            $maxCard['is_prepaid']=$maxCards['is_prepaid']?? null;
            $maxCard['type']=$maxCards['type']?? null;
            DB::table('maxmind_cards')->insert($maxCard);
        }
    }

    public function createMaxmindEmail($maxResponse)
    {
        foreach ($maxResponse as $maxEmails) {
            $maxEmail=[];
            $maxEmail['maxmind_id']=$this->maxmind_id;
            $maxEmail['created_at']=Carbon::now();
            $maxEmail['first_seen']=$maxEmails['first_seen']?? null;
            $maxEmail['is_free']=$maxEmails['is_free']?? null;
            $maxEmail['is_high_risk']=$maxEmails['is_high_risk']?? null;
            DB::table('maxmind_emails')->insert($maxEmail);
        }
    }

    public function createMaxmindIpCity($cities)
    {
        $city=[];
        $city['maxmind_id']=$this->maxmind_id;
        $city['created_at']=Carbon::now();
        $city['name']=$cities['names']['en']?? null;
        $city['confidence']=$cities['confidence']?? null;
        $city['geoname_id']=$cities['geoname_id']?? null;

        DB::table('maxmind_ip_cities')->insert($city);
    }

    public function createMaxmindIpLocation($locations)
    {
        $location=[];
        $location['maxmind_id']=$this->maxmind_id;
        $location['created_at']=Carbon::now();
        $location['local_time']=$locations['local_time']?? null;
        $location['accuracy_radius']=$locations['accuracy_radius']?? null;
        $location['latitude']=$locations['latitude']?? null;
        $location['longitude']=$locations['longitude']?? null;
        $location['time_zone']=$locations['time_zone']?? null;

        DB::table('maxmind_ip_locations')->insert($location);
    }

    public function createMaxmindIpPostal($postals)
    {
        $postal=[];
        $postal['maxmind_id']=$this->maxmind_id;
        $postal['created_at']=Carbon::now();
        $postal['confidence']=$postals['confidence']?? null;
        $postal['code']=$postals['code']?? null;

        DB::table('maxmind_ip_postals')->insert($postal);
    }

    public function createMaxmindIpSubdivision($subdivisions)
    {
        $subdivision=[];
        $subdivision['maxmind_id']=$this->maxmind_id;
        $subdivision['created_at']=Carbon::now();
        $subdivision['confidence']=$subdivisions['confidence']?? null;
        $subdivision['iso_code']=$subdivisions['iso_code']?? null;
        $subdivision['geoname_id']=$subdivisions['geoname_id']?? null;
        $subdivision['name']=$subdivisions['name']?? null;

        DB::table('maxmind_ip_subdivisions')->insert($subdivision);
    }

    public function createMaxmindIpContinent($continents)
    {
        $continent=[];
        $continent['maxmind_id']=$this->maxmind_id;
        $continent['created_at']=Carbon::now();
        $continent['code']=$continents['code']?? null;
        $continent['geoname_id']=$continents['geoname_id']?? null;
        $continent['name']=$continents['name']?? null;

        DB::table('maxmind_ip_continents')->insert($continent);
    }

    public function createMaxmindIpCountry($countries)
    {
        $country=[];
        $country['maxmind_id']=$this->maxmind_id;
        $country['created_at']=Carbon::now();
        $country['is_high_risk']=$countries['is_high_risk']?? null;
        $country['confidence']=$countries['confidence']?? null;
        $country['iso_code']=$countries['iso_code']?? null;
        $country['geoname_id']=$countries['geoname_id']?? null;
        $country['name']=$countries['name']?? null;

        DB::table('maxmind_ip_countries')->insert($country);
    }

    public function createMaxmindIpRegisteredCountry($rcountries)
    {
        $rcountry=[];
        $rcountry['maxmind_id']=$this->maxmind_id;
        $rcountry['created_at']=Carbon::now();
        $rcountry['iso_code']=$rcountries['iso_code']?? null;
        $rcountry['geoname_id']=$rcountries['geoname_id']?? null;
        $rcountry['name']=$rcountries['name']?? null;

        DB::table('maxmind_ip_registered_countries')->insert($rcountry);
    }

    public function createMaxmindIpTrait($traits)
    {
        $trait=[];
        $trait['maxmind_id']=$this->maxmind_id;
        $trait['created_at']=Carbon::now();
        $trait['user_type']=$traits['user_type']?? null;
        $trait['autonomous_system_number']=$traits['autonomous_system_number']?? null;
        $trait['domain']=$traits['domain']?? null;
        $trait['isp']=$traits['isp']?? null;
        $trait['organization']=$traits['organization']?? null;
        $trait['ip_address']=$traits['ip_address']?? null;
        $trait['autonomous_system_organization']=$traits['autonomous_system_organization']?? null;
        $trait['connection_type']=$traits['connection_type']?? null;
        $trait['is_anonymous']=$traits['is_anonymous']?? null;
        $trait['is_anonymous_proxy']=$traits['is_anonymous_proxy']?? null;
        $trait['is_anonymous_vpn']=$traits['is_anonymous_vpn']?? null;
        $trait['is_hosting_provider']=$traits['is_hosting_provider']?? null;
        $trait['is_legitimate_proxy']=$traits['is_legitimate_proxy']?? null;
        $trait['is_public_proxy']=$traits['is_public_proxy']?? null;
        $trait['is_satellite_provider']=$traits['is_satellite_provider']?? null;
        $trait['is_tor_exit_node']=$traits['is_tor_exit_node']?? null;

        DB::table('maxmind_ip_traits')->insert($trait);
    }
}
