<?php

namespace App\Traits;

use Exception;
use App\Exceptions\render;
use Asm89\Stack\CorsService;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

trait ApiExceptionHandler
{
    use ApiResponser;

    protected function getApiExceptionHandler(Request $request, Exception $e)
    {
        $response = $this->getJsonResponseForException($request, $e);
        app(CorsService::class)->addActualRequestHeaders($response, $request);

        return $response;
    }

    /**
     * Creates a new JSON response based on exception type.
     *
     * @param Request $request
     * @param Exception $e
     * @return \Illuminate\Http\JsonResponse
     */
    protected function getJsonResponseForException(Request $request, Exception $e)
    {
        switch (true) {
            case $this->isModelNotFoundException($e):
                $response = $this->modelNotFoundException($e, $request);
                break;
            case $this->isValidationException($e):
                $response = $this->validationException($e, $request);
                break;
            case $this->isAuthenticationException($e):
                $response = $this->authenticationException($e, $request);
                break;
            case $this->isAuthorizationException($e):
                $response = $this->authorizationException($e, $request);
                break;
            case $this->isNotFoundHttpException($e):
                $response = $this->notFoundHttpException($e, $request);
                break;
            case $this->isMethodNotAllowedHttpException($e):
                $response = $this->methodNotAllowedHttpException($e, $request);
                break;
            case $this->isTokenMismatchException($e):
                $response = $this->tokenMismatchException($e, $request);
                break;
            case $this->isQueryException($e):
                $response = $this->queryException($e, $request);
                break;
            case $this->isHttpException($e):
                $response = $this->httpException($e, $request);
                break;
            default:
                $response = $this->badRequest($e, $request);
        }



        return $response;
    }



    /**
     * Determines if the given exception is an Eloquent model not found.
     *
     * @param Exception $e
     * @return bool
     */
    protected function isModelNotFoundException(Exception $e)
    {
        return $e instanceof ModelNotFoundException;
    }

    /**
     * Determines if the given exception is an Validation error.
     *
     * @param Exception $e
     * @return bool
     */
    protected function isValidationException(Exception $e)
    {
        return $e instanceof ValidationException;
    }

    /**
     * Determine if the given exception is an HTTP exception.
     *
     * @param  \Exception  $e
     * @return bool
     */
    protected function isHttpException(Exception $e)
    {
        return $e instanceof HttpException;
    }

    /**
     * Determine if the given exception is an HTTP exception.
     *
     * @param  \Exception  $e
     * @return bool
     */
    protected function isTokenMismatchException(Exception $e)
    {
        return $e instanceof TokenMismatchException;
    }

    /**
     * Determine if the given exception is an HTTP exception.
     *
     * @param  \Exception  $e
     * @return bool
     */
    protected function isAuthenticationException(Exception $e)
    {
        return $e instanceof AuthenticationException;
    }


    /**
     * Determine if the given exception is an HTTP exception.
     *
     * @param  \Exception  $e
     * @return bool
     */
    protected function isAuthorizationException(Exception $e)
    {
        //var_dump($e instanceof AuthorizationException);
        // die();
        return $e instanceof AuthorizationException;
    }


    /**
     * Determine if the given exception is an HTTP exception.
     *
     * @param  \Exception  $e
     * @return bool
     */
    protected function isNotFoundHttpException(Exception $e)
    {
        return $e instanceof NotFoundHttpException;
    }

    /**
     * Determine if the given exception is an MethodNotAllowed Http Exception.
     *
     * @param  \Exception  $e
     * @return bool
     */
    protected function isMethodNotAllowedHttpException(Exception $e)
    {
        return $e instanceof MethodNotAllowedHttpException;
    }

    /**
     * Determine if the given exception is an Query Exception.
     *
     * @param  \Exception  $e
     * @return bool
     */
    protected function isQueryException(Exception $e)
    {
        return $e instanceof QueryException;
    }


    /**
     * Returns json response for generic bad request.
     *
     * @param string $message
     * @param int $statusCode
     * @return \Illuminate\Http\JsonResponse
     */
    protected function badRequest($exception, $request)
    {
        if (config('app.debug')) {
            return parent::render($request, $exception);
        }
        return $this->errorResponse('Bad request', 400);
    }

    /**
     * Returns json response for Eloquent model not found exception.
     *
     * @param string $message
     * @param int $statusCode
     * @return \Illuminate\Http\JsonResponse
     */
    protected function modelNotFoundException(ModelNotFoundException $exception, $request)
    {
        return $this->errorResponse('Record not found', 404);
    }

    /**
    * Create a response object from the given validation exception.
    *
    * @param  \Illuminate\Validation\ValidationException  $e
    * @param  \Illuminate\Http\Request  $request
    * @return \Symfony\Component\HttpFoundation\Response
    */
    protected function validationException(ValidationException $exception, $request)
    {
        return $this->errorResponse($exception->errors(), '422');
    }

    /**
    * Create a response object from the given Authentication exception.
    *
    * @param  \Illuminate\Auth\AuthenticationException  $e
    * @param  \Illuminate\Http\Request  $request
    * @return \Symfony\Component\HttpFoundation\Response
    */
    protected function authenticationException(AuthenticationException $exception, $request)
    {
        return $this->errorResponse('Unauthenticated', '401');
    }

    /**
    * Create a response object from the given Authentication exception.
    *
    * @param  \Illuminate\Auth\AuthenticationException  $e
    * @param  \Illuminate\Http\Request  $request
    * @return \Symfony\Component\HttpFoundation\Response
    */
    protected function authorizationException(AuthorizationException $exception, $request)
    {
        return $this->errorResponse($exception->getMessage(), '403');
    }

    /**
    * Create a response object from the given Authentication exception.
    *
    * @param  \Illuminate\Auth\AuthenticationException  $e
    * @param  \Illuminate\Http\Request  $request
    * @return \Symfony\Component\HttpFoundation\Response
    */
    protected function notFoundHttpException(NotFoundHttpException $exception, $request)
    {
        return $this->errorResponse('The specific URL cannot be found', '404');
    }

    /**
    * Create a response object from the given Authentication exception.
    *
    * @param  \Illuminate\Auth\AuthenticationException  $e
    * @param  \Illuminate\Http\Request  $request
    * @return \Symfony\Component\HttpFoundation\Response
    */
    protected function methodNotAllowedHttpException(MethodNotAllowedHttpException $exception, $request)
    {
        if (config('app.debug')) {
            return parent::render($request, $exception);
        }
        return $this->errorResponse('The specific method for the request is invalid', '405');
    }

    /**
    * Create a response object from the given Authentication exception.
    *
    * @param  \Illuminate\Auth\AuthenticationException  $e
    * @param  \Illuminate\Http\Request  $request
    * @return \Symfony\Component\HttpFoundation\Response
    */
    protected function httpException(HttpException $exception, $request)
    {
        return $this->errorResponse($exception->getMessage(), $exception->getStatusCode());
    }

    /**
    * Create a response object from the given Authentication exception.
    *
    * @param  \Illuminate\Auth\AuthenticationException  $e
    * @param  \Illuminate\Http\Request  $request
    * @return \Symfony\Component\HttpFoundation\Response
    */
    protected function tokenMismatchException(TokenMismatchException $exception, $request)
    {
        //$exception->getMessage()
        return $this->errorResponse('TokenMismatchException', 419);
    }


    /**
    * Create a response object from the given Authentication exception.
    *
    * @param  \Illuminate\Auth\QueryException  $e
    * @param  \Illuminate\Http\Request  $request
    * @return \Symfony\Component\HttpFoundation\Response
    */
    protected function queryException(QueryException $exception, $request)
    {
        $code = $exception->errorInfo[1];
        if ($code == 1451) {
            return $this->errorResponse('Cannot remove this resource permanently. It is related with any other resource', 409);
        }
        if (config('app.debug')) {
            return parent::render($request, $exception);
        }
        return $this->errorResponse('Unexpected Exception. Try later', 500);
    }
}
