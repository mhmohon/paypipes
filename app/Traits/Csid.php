<?php
namespace App\Traits;

use GuzzleHttp\Client;

trait Csid
{
    /**
     * Store activity entry to database
     * @param  array $data
     * @return void
     */
    public static function getCsid()
    {
        $url = "https://shop.paypipes.com/csid/csid.php";
        $client= new Client();
        $response = $client->request(
            'GET',
            $url
        );
        $csid =  $response->getBody();
        return $csid;
    }
}
