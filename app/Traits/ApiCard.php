<?php

namespace App\Traits;

use CreditCard;
use App\Entities\Tokenize\TokenizeCard;

trait ApiCard
{
    /**
    ***check valid card number, expiration date and secure code  and return error code
    **/
    public function validCreditCard($number, $year = '', $month = '', $cvc = '', $type = '')
    {
        if (empty($type)) {
            $type = CreditCard::creditCardType($number);
        }
        if ($type) {
            $validcards = CreditCard::validCard($number, $type);
            if (!$validcards) {
                return 'Invalid Card';
            }

            $validdate = CreditCard::validDate($year, $month);
            if (!$validdate) {
                return 'Invalid expiration date';
            }
            if (!empty($cvc)) {
                $validcvc = CreditCard::validCvc($cvc, $type);
                if (!$validcvc) {
                    return 'Invalid Security Code';
                }
            }
            return 0;
        } else {
            return 'Invalid Card';
        }
    }

    public function getBinInfo($bin)
    {
        $error = [];

        $url = "https://api.bincodes.com/bin/json/".config('global.bincodes_key')."/$bin/";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        $response = curl_exec($ch);
        curl_close($ch);
        $result = json_decode($response, true);

        return $result;
    }

    public function getBinCards($card_encrypt, $card_bin = '')
    {
        $bincards = TokenizeCard::where(['card_number'=>$card_encrypt])->whereNotNull('card_country')->get()->last();
        $binCardCount = is_object($bincards) ? count($bincards) : 0;
        $bincard= [];
        if ($binCardCount > 0) {
            $bincard['card_bank']=$bincards->card_bank;
            $bincard['credit']=$bincards->credit;
            $bincard['card_country']=$bincards->card_country;
            $bincard['card_level']=$bincards->card_level;
            $bincard['card_phone']=$bincards->card_phone;
            $bincard['card_website']=$bincards->card_website;
        } else {
            $cardBinInfo=$this->getBinInfo($card_bin);
            if (isset($cardBinInfo['countrycode']) && !empty($cardBinInfo['countrycode'])) {
                $bincard['card_bank'] = $cardBinInfo['bank']?? '';
                $bincard['credit'] = ($cardBinInfo['type']=="CREDIT")?1:0;
                $bincard['card_country'] = $cardBinInfo['countrycode']?? '';
                $bincard['card_level'] = $cardBinInfo['level']?? '';
                $bincard['card_phone'] = $cardBinInfo['phone']?? '';
                $bincard['card_website'] = $cardBinInfo['website']?? '';
            }
        }

        return $bincard;
    }

    /**
    ***return card type
    **/
    public function cardType($card_number)
    {
        $type = CreditCard::creditCardType($card_number);
        return $type;
    }

    /**
    ***return card digit and bin
    **/
    public function cardDigit($card_number)
    {
        $card[]=substr($card_number, 0, 6);
        $card[]=substr($card_number, -4);

        return $card;
    }


    public function cardMarkup($carddetails)
    {
        $card_digit=$carddetails->card_digit;
        $card_bin=$carddetails->card_bin;
        $card_number=$card_bin;
        $cardX="";
        for ($i=strlen($card_bin.$card_digit); $i<$carddetails->card_length; $i++) {
            $cardX=$cardX."X";
        }
        $card_number=$card_number.$cardX.$card_digit;

        $card_number= wordwrap($card_number, 4, " ", true);
        return $card_number;
    }

    public function cardMask($card_number)
    {
        $card_digit=substr($card_number, -4);
        $card_bin=substr($card_number, 0, 6);
        $card_length = strlen($card_number);
        $mask=strlen($card_bin.$card_digit);

        $cardX="";
        for ($i=$mask; $i<$card_length; $i++) {
            $cardX=$cardX."X";
        }
        $card_number=$card_bin.$cardX.$card_digit;

        $card_number= wordwrap($card_number, 4, " ", true);
        return $card_number;
    }

    public function getCardInfo($card_token)
    {
        $cardInfo = TokenizeCard::where(['card_token'=>$card_token])->firstOrFail();
        $cardInfo->card_number=$this->cardMarkup($cardInfo);
        return $cardInfo;
    }
}
