<?php

namespace App\Traits;

trait ApiCommon
{

    /**
    ***retrive IP Address
    **/
    public function getIP()
    {
        $url='https://api.ipify.org';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);

        $ip = curl_exec($ch);

        if (curl_error($ch)) {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        curl_close($ch);

        return $ip;
    }

    /**
    ***return unique ID
    **/

    public function uniqidReal($length = 32)
    {
        // uniqid gives 32 chars, but you could adjust it to your needs.
        if (function_exists("random_bytes")) {
            $bytes = random_bytes(ceil($length / 2));
        } elseif (function_exists("openssl_random_pseudo_bytes")) {
            $bytes = openssl_random_pseudo_bytes(ceil($length / 2));
        } else {
            throw new Exception("no cryptographically secure random function available");
        }
        return substr(bin2hex($bytes), 0, $length);
    }

    public function responseType($request)
    {
        if ($request->wantsJson()) {
            $response_type='json_redirect';
        } else {
            $response_type='nvp_redirect';
        }
        return $response_type;
    }

    /**
    ***receive transaction request from json or any formate
    **/
    public function apiData($request)
    {
        $data=[];
        if ($request->getContentType() === 'xml') {
            // Returns the xml input from a request
            $xml = simplexml_load_string($request->getContent(), null, LIBXML_NOCDATA);
            $json = json_encode($xml);
            $data=json_decode($json, true);
            $data['oauth_client_id']=$request['oauth_client_id'];
        } else {
            $data=$request->all();
        }

        return $data;
    }

    /**
    ***retrive transaction type from URI
    **/
    public function currentSegment($request)
    {
        $uri=$request->getRequestUri();
        $segments = explode('/', trim($uri));
        $numSegments = count($segments);
        $currentSegment = $segments[$numSegments - 1];

        return $currentSegment;
    }
}
