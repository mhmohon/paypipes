<?php

namespace App\Traits;

use DB;
use App\Entities\ApiEncrypt;

trait TransactionDetails
{
    /**
    ***return transaction and billing and card from database
    **/
    public function getTransDetails($tid)
    {
        $transDetails = DB::table('customer_billings as cb')
                ->leftjoin('transactions as trx', 'trx.customer_token', '=', 'cb.customer_token')
                ->leftjoin('customer_cards as cc', 'trx.card_token', '=', 'cc.card_token')
                ->select('cb.first_name', 'cb.last_name', 'cb.street', 'cb.city', 'cb.state', 'cb.country', 'cb.post_code', 'cb.email', 'cb.phone', 'cb.phone_code', 'cc.card_holder', 'cc.card_number', 'cc.card_bin', 'cc.card_digit', 'cc.exp_month', 'cc.exp_year', 'cc.card_type', 'cc.card_bank', 'trx.*')
                ->where(['trx.id'=>$tid])
                ->first();

        if (empty($transDetails)) {
            return 0;
        } else {
            $crypto=new ApiEncrypt();

            $transDetails->card_number=$crypto->cardDecrypt($transDetails->card_number);

            return $transDetails;
        }
    }
}
