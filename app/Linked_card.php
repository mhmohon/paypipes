<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Linked_card extends Model
{
    protected $fillable =['card_holder', 'card_number', 'exp_month', 'exp_year', 'street', 'city', 'country', 'postcode', 'created_at', 'updated_at'];
	
	public function admin()
    {
		return $this->belongsTo('App\Admin');
    }
	
	public function customer()
    {
        return $this->belongsTo('App\Customer');
    }
	
	public function merchant()
    {
        return $this->belongsTo('App\Merchant');
    }
	
}