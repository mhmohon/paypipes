<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dispute extends Model
{
    protected $fillable =[
        'dispute_id',
        'payment_id',
        'account_id',
        'reason',
        'description',
        'status',
    ];
}
