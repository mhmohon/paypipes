<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer_card extends Model
{
    protected $fillable =[
        'card_type',
        'card_bin',
        'card_digit',
        'card_number',
        'card_holder',
        'exp_month',
        'exp_year',
        'updated_at',
        'created_at',
        'added_by',
        'updated_by',
        'card_token',
        'card_length',
        'card_bank',
        'credit',
        'card_country',
        'card_level',
        'card_phone',
        'card_website',
        'status',

    ];



    public function admin()
    {
        return $this->belongsTo('App\Admin');
    }

    public function getCreditTypeAttribute()
    {
        $credit = $this->attributes['credit'];

        if ($credit==0) {
            return 'Debit' ;
        } else {
            return 'Credit';
        }
    }

    public function getCountryNameAttribute()
    {
        $code = $this->attributes['card_country'];
        $country = Country::where('country_code', $code)->first();

        if (isset($country->country_name)) {
            return $country->country_name;
        } else {
            return '';
        }
    }
}
