<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wallet_old extends Model
{
    protected $fillable =[
        'wallet_id',
		'wallet_type_id',
        'wallet_fee_id',
        'wallet_limit_id',
        'status',
        'balance',
        'currency',
    ];
    
    public function wallettype()
    {
        return $this->belongsTo('App\Wallettype');
    }
	
	public function walletfee()
    {
        return $this->belongsTo('App\Walletfee');
    }
	
	public function walletlimit()
    {
        return $this->belongsTo('App\Walletlimit');
    }
}
