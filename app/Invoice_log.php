<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoice_log extends Model
{
    protected $fillable =[
        'invoice_id',
		'amount',
		'currency',
		'payment_date',
    ];
}
