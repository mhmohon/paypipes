<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable =[
        'transaction_id',
        'currency',
        'status',
    ];

    public function account()
    {
        return $this->belongsTo('App\Account');
    }

    public function maxmindScore()
    {
        return $this->belongsTo('App\Entities\MaxmindScore');
    }
    public function apiActivity()
    {
        return $this->hasMany('App\Entities\ApiActivityLogger', 'order_id', 'order_id');
    }

    public static function details($payment_id)
    {
        $payment = Transaction::leftJoin('customer_billings as cb', 'transactions.customer_token', '=', 'cb.customer_token')
                ->leftJoin('customer_cards as cc', 'transactions.card_token', '=', 'cc.card_token')
                ->leftJoin('countries', 'countries.country_code', '=', 'cb.country')
                ->where('transactions.id', '=', $payment_id)
                ->select('transactions.*', 'cb.first_name', 'cb.last_name', 'cb.street', 'cb.city', 'cb.state', 'cb.country', 'cb.post_code', 'cb.email', 'cb.phone', 'cb.ip_address as customer_ip', 'countries.country_name', 'cc.card_type', 'cc.card_bin', 'cc.card_digit')
                ->first();
        return $payment;
    }

    public static function status_css($transaction_status)
    {
        $payment_status =  'label-default';
        switch ($transaction_status) {
                case "Approved":
                    $payment_status =  'label-success'; break;
                case "Pending":
                    $payment_status =  'label-info';    break;
                case "Requested":
                    $payment_status =  'label-info';    break;
                case "Refunded":
                    $payment_status =  'label-purple';  break;
                case "Canceled":
                    $payment_status =  'label-warning'; break;
                case "Chargeback":
                    $payment_status =  'label-black';   break;
                case "Declined":
                    $payment_status =  'label-danger';  break;
                default:
                    $payment_status =  'label-default'; break;
        }
        return $payment_status;
    }
    public static function reverse_type($transaction_type)
    {
        $reverse_type =  '';
        switch (trim($transaction_type)) {
                case "Authorize":
                    $reverse_type =  'authorize';   break;
                case "Capture":
                    $reverse_type =  'capture'; break;
                case "Purchase":
                    $reverse_type =  'purchase';    break;
                case "TokenizerAuthorize":
                    $reverse_type =  'authorize';   break;
                case "TokenizerPurchase":
                    $reverse_type =  'purchase';    break;
                default:
                    $reverse_type =  'purchase';    break;
        }
        return $reverse_type;
    }
    public static function statementdetails($where, $start_date, $end_date)
    {
        $not_in = ['Authorize', 'ReverseAuthorize', 'TokenizerAuthorize'];
        $statement = Transaction::leftJoin('customer_billings', 'transactions.customer_token', '=', 'customer_billings.customer_token')
                        ->leftJoin('terminals', 'transactions.terminal_id', '=', 'terminals.terminal_id')
                        ->whereNotIn('transactions.transaction_type', $not_in)
                        ->whereDate('transactions.created_at', '>=', $start_date)
                        ->whereDate('transactions.created_at', '<=', $end_date)
                        ->where($where)
                        ->select('transactions.*', 'customer_billings.email', 'customer_billings.first_name', 'customer_billings.last_name', 'customer_billings.ip_address as customer_ip', 'terminals.name');
        return $statement;
    }
}
