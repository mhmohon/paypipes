<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Rule_transaction_trigger extends Model
{
    protected $fillable =[
        
    ];	
	protected $connection = 'mysqlfraud';
	
	public static function _all()
    {
		return Rule_transaction_trigger::where('status', '=', 'Active')->get();
	}
	
	public static function details($transaction_id)
    {
		return Rule_transaction_trigger::where('status', '=', 'Active')->where('transaction_id', '=', $transaction_id)->get();
	}
 
	public static function detailsbyrule($rule_id)
    {
		return Rule_transaction_trigger::where('status', '=', 'Active')->where('rule_id', '=', $rule_id)->get();
	}
}