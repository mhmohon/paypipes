<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;
use App\Libraries\EncryptDecrypt;

class Rule_blacklistcard extends Model
{
    protected $fillable =[
        'customer_id',
        'merchant_id',
        'terminal_id',
        'card_type',
        'card_number',
        'type',
        'status',
    ];
    
    protected $connection = 'mysqlfraud';
	
	public static function search($request,$where)
    {
	    $item_per_page = !empty($request->rows)?$request->rows:ROW_PER_PAGE;
		
        $main_key = DB::table('main_key')->where('status', '=', 'Active')->orderBy('id','DESC')->first();
		$crypto_key = DB::table('crypto_key')->where('status', '=', 'Active')->orderBy('id','DESC')->first();
		
		$crypto = new EncryptDecrypt($main_key->main_key);  
		$key_card=$crypto_key->key_card;
		
		$blacklistcards = Rule_blacklistcard::where(function($query) use ($request,$crypto,$key_card) {
			//filter by keyword
			if (($search = $request->get('search'))) {
				$query->orWhere('card_type', 'like', '%' . $search . '%');
				if(is_numeric($search) && strlen($search) >= 13)
				{
					$card_encrypt=$crypto->encrypt($search,$key_card);
					$query->orWhere('card_number', 'like', '%' . $card_encrypt . '%');
				}				
				if(is_numeric($search) && strlen($search) == 6)
				{		
					$query->orWhere('card_bin', 'like', '%' . $search . '%');
				}
				
				if(is_numeric($search) && strlen($search) == 4)
				{	
					$query->orWhere('card_digit', 'like', '%' . $search . '%');
				}
				$query->orWhere('type', 'like', $search . '%');
				$query->orWhere('status', 'like', $search . '%');
			}
		})		
		->where($where)		
		->orderBy('created_at','desc')
		->paginate($item_per_page);
		
		//->orWhere('type', '=', 'Core')
		foreach($blacklistcards as $bCards)
		{
			$card_decrypt="";
			$card_decrypt=$crypto->decrypt($bCards->card_number,$crypto_key->key_card);
			$bCards->card_number=$crypto->card_markup($card_decrypt);
		}
		
		return $blacklistcards;
    }
	
	public static function searchadmin($request)
    {
	    $item_per_page = !empty($request->rows)?$request->rows:ROW_PER_PAGE;
		
        $main_key = DB::table('main_key')->where('status', '=', 'Active')->orderBy('id','DESC')->first();
		$crypto_key = DB::table('crypto_key')->where('status', '=', 'Active')->orderBy('id','DESC')->first();
		
		$crypto = new EncryptDecrypt($main_key->main_key);  
		$key_card=$crypto_key->key_card;
		
		$blacklistcards = Rule_blacklistcard::where(function($query) use ($request,$crypto,$key_card) {
			//filter by keyword
			if (($search = $request->get('search'))) {
			
				$query->orWhere('merchant_id', 'like', '%' . $search . '%');
				$query->orWhere('psp_id', 'like', '%' . $search . '%');
				$query->orWhere('card_type', 'like', '%' . $search . '%');
				if(is_numeric($search) && strlen($search) >= 13)
				{
					$card_encrypt=$crypto->encrypt($search,$key_card);
					$query->orWhere('card_number', 'like', '%' . $card_encrypt . '%');
				}				
				if(is_numeric($search) && strlen($search) == 6)
				{		
					$query->orWhere('card_bin', 'like', '%' . $search . '%');
				}
				
				if(is_numeric($search) && strlen($search) == 4)
				{	
					$query->orWhere('card_digit', 'like', '%' . $search . '%');
				}
				$query->orWhere('type', 'like', $search . '%');
				$query->orWhere('status', 'like', $search . '%');
			}
		})
		->orderBy('created_at','desc')
		->paginate($item_per_page);
		
		foreach($blacklistcards as $bCards)
		{
			$card_decrypt="";
			$card_decrypt=$crypto->decrypt($bCards->card_number,$crypto_key->key_card);
			$bCards->card_number=$crypto->card_markup($card_decrypt);
		}
		
		return $blacklistcards;
    }
}
