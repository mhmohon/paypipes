<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Walletcompany extends Model
{
    protected $fillable =[
        'merchant_id',
    ];
    
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
