<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Country;
class Blacklistcountry extends Model
{
    protected $fillable =[
        'account_id',
        'merchant_id',
        'terminal_id',
        'country',
        'type',
        'status',
    ];
    
    public function admin()
    {
        return $this->belongsTo('App\Admin');
    }
	
	public function merchant()
    {
        return $this->belongsTo('App\Merchant');
    }
	public function psp()
    {
        return $this->belongsTo('App\Psp');
    }
	public static function search($request,$where)
    {
	   $item_per_page = !empty($request->rows)?$request->rows:ROW_PER_PAGE;
	   
	   $country_code=$request->get('search');
	   if(!empty($country_code))$country_code= Country::countrycode($country_code);
	   //leftjoin('countries', 'blacklistcountries.country', '=', 'countries.country_code')
       $search = Blacklistcountry::leftjoin('countries', 'blacklistcountries.country', '=', 'countries.country_code')
			->where(function($query) use ($request,$country_code) {
			//filter by keyword
			if (($search = $request->get('search'))) {
				if(!empty($country_code))$query->orWhere('country', 'like', '%' . $country_code . '%');
				$query->orWhere('type', 'like', $search . '%');
				$query->orWhere('status', 'like', $search . '%');
			}
		})		
		->where($where)		
		->select('blacklistcountries.*','countries.country_name')
		->orderBy('created_at','desc')
		->paginate($item_per_page);	
//->orWhere('type', '=', 'Core')		
		return $search;
    }
	
	public static function searchadmin($request)
    {
	   $item_per_page = !empty($request->rows)?$request->rows:ROW_PER_PAGE;
	   
	   $country_code=$request->get('search');	   
	   if(!empty($country_code))$country_code= Country::countrycode($country_code);
	   
       $search = Blacklistcountry::leftjoin('countries', 'blacklistcountries.country', '=', 'countries.country_code')
	   ->where(function($query) use ($request,$country_code) {
			//filter by keyword
			if (($search = $request->get('search'))) {
				if(!empty($country_code))$query->orWhere('country', 'like', '%' . $country_code . '%');
				$query->orWhere('psp_id', 'like', '%' . $search . '%');
				$query->orWhere('merchant_id', 'like', '%' . $search . '%');				
				$query->orWhere('type', 'like', $search . '%');
				$query->orWhere('status', 'like', $search . '%');
			}
		})
		->select('blacklistcountries.*','countries.country_name')
		->orderBy('created_at','desc')
		->paginate($item_per_page);
		
		return $search;
    }

    public function getCountryNameAttribute(){
        $code = $this->attributes['country'];
        $country = DB::table('countries')->where('country_code', $code)->first();
        if(isset($country->country_name)) return $country->country_name;
        else return '';
    }
}
