<?php

namespace App;

use App\Notifications\AdminResetPassword;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'admin_id', 'first_name', 'last_name', 'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    /*public function setPasswordAttribute($password)
    {
        $this->attributes['password'] = \Hash::make($password);;
    }*/

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new AdminResetPassword($token));
    }

    public function activities()
    {
        return $this->hasMany('App\Activity');
    }

    public function compliances()
    {
        return $this->hasMany('App\Compliance');
    }

    public function accounts()
    {
        return $this->hasMany('App\Account');
    }



    public function wallets()
    {
        return $this->hasMany('App\Wallet');
    }

    public function whitelistcustomers()
    {
        return $this->hasMany('App\Whitelistcustomer');
    }

    public function blacklistemails()
    {
        return $this->hasMany('App\Blacklistemail');
    }

    public function blacklistcards()
    {
        return $this->hasMany('App\Blacklistcard');
    }

    public function blacklistbins()
    {
        return $this->hasMany('App\Blacklistbin');
    }

    public function blacklistips()
    {
        return $this->hasMany('App\Blacklistip');
    }

    public function blacklistcountries()
    {
        return $this->hasMany('App\Blacklistcountry');
    }

    public function velocitylimits()
    {
        return $this->hasMany('App\Velocitylimit');
    }

    public function userroles()
    {
        return $this->hasMany('App\Userrole');
    }
}
