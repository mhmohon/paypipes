<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Crossrate extends Model
{
    protected $fillable =[
         'from_currency', 'to_currency', 'crossrate', 'status'
    ];
}
