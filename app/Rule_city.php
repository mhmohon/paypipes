<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rule_city extends Model
{
    protected $fillable =[
        
    ];
	protected $connection = 'mysqlfraud';
	
	public function merchant()
    {
        return $this->hasMany('App\Merchant');
    }
	public function country() 
	{
		return $this->belongsTo('App\Country');
	}
 
}