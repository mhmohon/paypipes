<?php

namespace App;

use App\Notifications\CustomerResetPassword;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;

class Customer extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'first_name',
        'last_name',
        'email',
        'phone',
        'street',
        'city',
        'country',
        'post_code',
        'compliance',
        'status',
        'wallet_type_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    //protected $appends = ['country_name'];
    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new CustomerResetPassword($token));
    }

    public function admin()
    {
        return $this->belongsTo('App\Admin');
    }

    public function activities()
    {
        return $this->hasMany('App\Activity');
    }

    public function wallets()
    {
        return $this->hasMany('App\Wallet');
    }

    public function payments()
    {
        return $this->hasMany('App\Payment');
    }

    public static function customerid()
    {
        $customer_startid= config('global.customer_startid');
        $customer_id=(int)$psp_id=DB::table('customers')->orderBy('customer_id', 'desc')->value('customer_id');
        if ($customer_id < 1) {
            $customer_id=$customer_startid;
        } else {
            $customer_id = $customer_id + 1;
        }
        return $customer_id;
    }

    public function getCountryNameAttribute()
    {
        $code = $this->attributes['country'];
        $country = DB::table('countries')->where('country_code', $code)->first();
        if (isset($country->country_name)) {
            return $country->country_name;
        } else {
            return '';
        }
    }
}
