<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wallet_switch extends Model
{
    protected $fillable =[
        'wallet_id',
        'account_id',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
