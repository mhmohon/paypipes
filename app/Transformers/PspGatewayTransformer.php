<?php

namespace App\Transformers;

use App\Entities\PspGateway;
use League\Fractal\TransformerAbstract;

class PspGatewayTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'credentials'
    ];
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(PspGateway $gateway)
    {
        return [
            'order_id'        => (string)$gateway->order_id,
            'gateway'         => (string)$gateway->gateway,
            'gateway_token'   => (string)$gateway->gateway_token,
            'status'          => (string)$gateway->status,
            'created'         => (string)$gateway->created_at,
            'updated'         => (string)$gateway->updated_at,
        ];
    }

    public function includeCredentials(PspGateway $gateway)
    {
        if (count($gateway->credentials) > 0) {
            return $this->collection($gateway->credentials, new PspGatewayCredentialTransformer());
        }
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'order_id'          =>  'order_id',
            'gateway'           =>  'gateway',
            'gateway_token'     =>  'gateway_token',
            'status'            =>  'status',
            'created_at'        =>  'created',
            'updated_at'        =>  'updated',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'order_id'          =>  'order_id',
            'gateway'           =>  'gateway',
            'gateway_token'     =>  'gateway_token',
            'status'            =>  'status',
            'created'           =>  'created_at',
            'created'           =>  'created_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}
