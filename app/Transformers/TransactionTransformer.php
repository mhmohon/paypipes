<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Entities\Transaction\Transaction;

class TransactionTransformer extends TransformerAbstract
{

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(Transaction $transaction)
    {
        return [
            'order_id'        => (string)$transaction->order_id,
            'transaction_id'   => (string)$transaction->transaction_id,
            'transaction_type' => (string)$transaction->transaction_type,
            'amount'         => (string)$transaction->amount,
            'currency'       => (string)$transaction->currency,
            'status'         => (string)$transaction->transaction_status,
            'status_code'    => (string)$transaction->error_code,
            'status_description'=> (string)$transaction->error_description,
            'created'       => (string) $transaction->created_at,
            'card_token'     => (string)$transaction->card_token??'',
            'mask_number'     => (string)$transaction->mask_number??'',
        ];
    }


    public static function originalAttribute($index)
    {
        $attributes = [
            'order_id'       =>  'order_id',
            'transaction_id' =>  'transaction_id',
            'transaction_type'=> 'transaction_type',
            'amount'         =>  'amount',
            'currency'       =>  'currency',
            'status'         =>  'status',
            'status_code'    =>  'status_code',
            'description'    =>  'description',
            'datetime'       =>  'datetime',
            'customer_token' =>  'customer_token',
            'card_token'     =>  'card_token',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'order_id'       =>  'order_id',
            'transaction_id' =>  'transaction_id',
            'transaction_type'=> 'transaction_type',
            'amount'         =>  'amount',
            'currency'       =>  'currency',
            'status'         =>  'status',
            'status_code'    =>  'status_code',
            'description'    =>  'description',
            'datetime'       =>  'datetime',
            'customer_token' =>  'customer_token',
            'card_token'     =>  'card_token',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}
