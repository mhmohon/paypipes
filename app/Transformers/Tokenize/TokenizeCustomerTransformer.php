<?php

namespace App\Transformers\Tokenize;

use League\Fractal\TransformerAbstract;
use App\Entities\Tokenize\TokenizeCustomer;

class TokenizeCustomerTransformer extends TransformerAbstract
{
    protected $defaultIncludes = [
        'cards'
    ];
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(TokenizeCustomer $customer)
    {
        return [
            'customer_token' => (string)$customer->customer_token,
            'order_id' => (string)$customer->order_id,
            'first_name' => (string)$customer->first_name,
            'last_name' => (string)$customer->last_name,
            'street' => (string)$customer->street,
            'city' => (string)$customer->city,
            'state' => (string)$customer->state,
            'post_code' => (string)$customer->post_code,
            'country' => (string)$customer->country_name,
            'phone_code' => (string)$customer->phone_code,
            'phone' => (string)$customer->phone,
            'email' => (string)$customer->email,
            'status' => (string)$customer->status,
            'created' => (string)$customer->created_at,
            'updated' => (string)$customer->updated_at,
            'expired' => (string)$customer->expired_at,
        ];
    }
    public function includeCards(TokenizeCustomer $customer)
    {
        if (count($customer->cards) > 0) {
            return $this->collection($customer->cards, new TokenizeCardTransformer());
        }
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'customer_token' => 'customer_token',
            'merchant_id' => 'merchant_id',
            'psp_id' => 'psp_id',
            'first_name' => 'first_name',
            'last_name' => 'last_ame',
            'street' => 'street',
            'city' => 'city',
            'state' => 'state',
            'post_code' => 'post_code',
            'country' => 'country',
            'phone_code' => 'phone_code',
            'phone' => 'phone',
            'email' => 'email',
            'status' => 'status',
            'created_at' => 'created_at',
            'updated_at' => 'updated_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'customer_token' => 'customer_token',
            'merchant_id' => 'merchant_id',
            'psp_id' => 'psp_id',
            'first_name' => 'first_name',
            'last_name' => 'last_name',
            'street' => 'street',
            'city' => 'city',
            'state' => 'state',
            'post_code' => 'post_code',
            'country' => 'country',
            'phone_code' => 'phone_code',
            'phone' => 'phone',
            'email' => 'email',
            'status' => 'status',
            'createdAt' => 'created_at',
            'updatedAt' => 'updated_at',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}
