<?php

namespace App\Transformers\Tokenize;

use League\Fractal\TransformerAbstract;
use App\Entities\Tokenize\TokenizeCard;

class TokenizeCardTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(TokenizeCard $card)
    {
        return [
            'card_token' => (string)$card->card_token,
            'holder' => (string)$card->card_holder,
            'bin' => (string)$card->card_bin,
            'digit' => (string)$card->card_digit,
            'brand' => (string)$card->card_type,
            'created' => (string)$card->created_at,
            'updated' => (string)$card->updated_at,
            'expired' => (string)$card->expired_at,
        ];
    }

    public static function originalAttribute($index)
    {
        $attributes = [
            'billing_id' => 'billing_id',
            'merchant_id' => 'merchant_id',
            'psp_id' => 'psp_id',
            'card_type' => 'card_type',
            'card_bin' => 'card_bin',
            'card_digit' => 'digit',
            'card_number' => 'card_number',
            'card_holder' => 'holder',
            'exp_month' => 'exp_month',
            'exp_year' => 'exp_year',
            'added_by' => 'added_by',
            'updated_by' => 'updated_by',
            'card_token' => 'card_token',
            'card_length' => 'card_length',
            'card_bank' => 'bank_name',
            'credit' => 'credit',
            'card_country' => 'card_country',
            'card_level' => 'card_level',
            'card_phone' => 'card_phone',
            'card_website' => 'card_website',
            'status' => 'status',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'customer_token' => 'customer_token',
            'merchant_id' => 'merchant_id',
            'psp_id' => 'psp_id',
            'card_type' => 'card_type',
            'card_bin' => 'card_bin',
            'digit' => 'card_digit',
            'card_number' => 'card_number',
            'holder' => 'card_holder',
            'exp_month' => 'exp_month',
            'exp_year' => 'exp_year',
            'added_by' => 'added_by',
            'updated_by' => 'updated_by',
            'card_token' => 'card_token',
            'card_length' => 'card_length',
            'card_bank' => 'card_bank',
            'credit' => 'credit',
            'card_country' => 'card_country',
            'card_level' => 'card_level',
            'card_phone' => 'card_phone',
            'card_website' => 'card_website',
            'status' => 'status',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}
