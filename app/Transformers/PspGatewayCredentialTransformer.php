<?php

namespace App\Transformers;

use App\Entities\PspGatewayCredential;
use League\Fractal\TransformerAbstract;

class PspGatewayCredentialTransformer extends TransformerAbstract
{

    /**
     * A Fractal transformer.
     *
     * @return array
     */
    public function transform(PspGatewayCredential $credential)
    {
        return [
            'name'   => (string)$credential->field,
            'value'  => (string)$credential->value
        ];
    }


    public static function originalAttribute($index)
    {
        $attributes = [
            'name'            =>  'field',
            'value'           =>  'value',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }

    public static function transformedAttribute($index)
    {
        $attributes = [
            'field'          =>  'name',
            'value'          =>  'value',
        ];

        return isset($attributes[$index]) ? $attributes[$index] : null;
    }
}
