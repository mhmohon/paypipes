<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    protected $fillable =[
        'wallet_id',
        'email',
        'wallet_type_id',
        'wallet_fee_id',
        'wallet_limit_id',
        'status',
        'primary',
        'balance',
        'currency',
    ];

    public function admin()
    {
        return $this->belongsTo('App\Admin');
    }

    public function account()
    {
        return $this->belongsTo('App\Account');
    }

    public function wallettype()
    {
        return $this->belongsTo('App\Wallettype', 'wallet_type_id');
    }

    public function walletfee()
    {
        return $this->belongsTo('App\Walletfee', 'wallet_fee_id');
    }

    public function walletlimit()
    {
        return $this->belongsTo('App\Walletlimit', 'wallet_limit_id');
    }
    public static function walletid()
    {
        $wallet_id=(int)$wallet_id=DB::table('wallets')->orderBy('wallet_id', 'desc')->value('wallet_id');
        if ($wallet_id < 1) {
            $wallet_id=101011;
        } else {
            $wallet_id = $wallet_id + 1;
        }

        return $wallet_id;
    }
}
