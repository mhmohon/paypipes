<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable =[
        'notification_id',
        'account_id',
        'email',

    ];

    public static function notificationid()
    {
        $notification_id=(int)$notification_id=DB::table('notifications')->orderBy('notification_id', 'desc')->value('notification_id');
        if ($notification_id < 1) {
            $notification_id=1010;
        } else {
            $notification_id = $notification_id + 1;
        }
    }
}
