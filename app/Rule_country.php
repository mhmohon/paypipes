<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rule_country extends Model
{
    protected $fillable =[
        
    ];
	protected $connection = 'mysqlfraud';
	
		
	public static function  countrylist()
    {
		return Rule_country::where('active', '=', 1)->orderBy('country_name','asc')->pluck('country_name', 'country_code');
	}
	public static function  countrycode($country_name)
    {
		return Rule_country::where('active', '=', 1)->where('country_name', '=', $country_name)->value('country_code');
	}
 
}