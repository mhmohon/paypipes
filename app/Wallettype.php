<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wallettype extends Model
{
    protected $fillable =[
        'wallet_type_id',
		'name',
		'add_wallet',
        'send',
        'pay',
        'request',
		'add_currency',
        'add',
		'withdraw',
		'transfer',
		'upgrade',
        'status',
    ];
    
    public function wallets()
    {
        return $this->hasMany('App\Wallet', 'wallet_type_id', 'id');
    }
}
