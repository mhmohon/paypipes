<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rule_criteria_group extends Model
{
    protected $fillable =[
        
    ];
	protected $connection = 'mysqlfraud';
	
	public static function _all()
    {
		return Rule_criteria_group::where('status', '=', 'Active')->get();
	}
	public static function details($group_id)
    {
		return Rule_criteria_group::where('status', '=', 'Active')->where('group_id', '=', $group_id)->get();
	}
	 
}