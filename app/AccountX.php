<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AccountX extends Model
{
    protected $fillable =[
        'account_id',
        'customer_id',
        'merchant_id',
        'terminal_id',	
        'description',       
        'account_type',
        'currency',
        'amount',
        'fee',
        'total_amount',
		'from',
		'to',
		'reference_id',
        'status',
		'setlement_date',
		'account_holder_name',
		'account_number',
		'iban',
		'swift',
		'bank_name',
		'bank_address',
    ];
	
	public function customer()
    {
        return $this->belongsTo('App\Customer');
    }
	
	
	
	public static function search($request,$data)
	{	 
		$item_per_page = !empty($request->rows)?$request->rows:ROW_PER_PAGE;
	
		
		$req=$search="";
		if(!empty($request->all())) $req= $request->all();
		else if(!empty($request->old())) $req= $request->old();
		

		//dd($request,$request->old(),$request->old('page'));	
		if(!empty($req['search']))
        $search = $req['search'];
		

        if(empty($search)){
            $accounts = Payment::where(function($query) use ($req,$data) {
					if(!empty($req))
					{
						
						//filter by keyword
						if(!empty($req['currency'])){
							$query->where('accounts.currency', $req['currency']);
						}						
						if(!empty($req['merchant_id'])){
							$query->where('accounts.merchant_id', $req['merchant_id']);
						}
						if(!empty($req['terminal_id'])){
							$query->where('accounts.terminal_id', $req['terminal_id']);
						}
						
						if(!empty($req['status'])){
							$query->where('accounts.status', $req['status']);
						}
						
						if(!empty($req['date_from']) && !empty($req['date_to'])){
							$query->where(function ($q) use ($req){
								$q->whereDate('accounts.created_at', '>=', date('Y-m-d H:i:s', strtotime($req['date_from'])))
								  ->whereDate('accounts.created_at', '<=', date('Y-m-d H:i:s', strtotime($req['date_to'])));
							});
						}
						else if( !empty($req['date_from'])){
							$query->whereDate('accounts.created_at', '>=', date('Y-m-d H:i:s', strtotime($req['date_from'])));
						}
						else if(!empty($req['date_to'])){
							$query->whereDate('accounts.created_at', '<=', date('Y-m-d H:i:s', strtotime($req['date_to'])));
						}
						
						if(!empty($req['amount_from']) && !empty($req['amount_to'])){
							$query->where(function ($q) use ($req){
								$q->where('accounts.amount', '>=', $req['amount_from'])
								  ->where('accounts.amount', '<=', $req['amount_to']);
							});
						}
						else if(!empty($req['amount_from'])){
							$query->where('accounts.amount', '>=', $req['amount_from']);
						}
						else if(!empty($req['amount_to'])){
							$query->where('accounts.amount', '<=', $req['amount_to']);
						}	
					}				
                });                

        }else{
    		$accounts = Payment::where(function($query) use ($search,$data) {				
					//filter by keyword    		
				if(!empty($search))
				{		
                    $query->orWhere('account_id', 'like', '%' . $search . '%');
					$query->orWhere('terminal_id', 'like', '%' . $search . '%');
					$query->orWhere('merchant_id', 'like', '%' . $search . '%');
					$query->orWhere('from', 'like', '%' . $search . '%');
					$query->orWhere('to', 'like', '%' . $search . '%');
					$query->orWhere('description', 'like', '%' . $search . '%');
					
					$query->orWhere('account_type', 'like', '%' . $search . '%');
					$query->orWhere('amount', 'like', '%' . $search . '%');
					$query->orWhere('total_amount', 'like', '%' . $search . '%');
					$query->orWhere('currency', 'like', '%' . $search . '%');
					$query->orWhere('status', 'like', $search . '%');				
				}
    		});    		
        }
		if(!empty($data['merchant_id']))$accounts = $accounts->where('accounts.merchant_id', $data['merchant_id']);
		if(!empty($data['terminal_id']))$accounts = $accounts->where('accounts.terminal_id', $data['terminal_id']);
		if(!empty($data['customer_id']))$accounts = $accounts->where('accounts.customer_id', $data['customer_id']);
					
						
		$accounts = $accounts->select('accounts.*')->orderBy('accounts.created_at','desc')->paginate($item_per_page);
		
		return $accounts;
		
     }
}
