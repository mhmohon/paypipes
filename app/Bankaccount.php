<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Bankaccount extends Model
{
   protected $fillable =[
		'account_holder',
		'account_number',
		'currency',
		'bank_name',
		'bank_address',
		'iban',
		'swift_bic',
		'status',
    ];
	
	public function country() 
	{
	return $this->belongsTo('App\Country');
	}
	
	public static function  accountlist()
    {
		return Bankaccount::select('id', DB::raw("concat(account_number, ' (',currency, ')') as account_currency"))->where('status', '=', 'Active')->orderBy('currency','asc')->pluck('account_currency', 'id');
	}
}
