<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Rule extends Model
{
    protected $fillable =[
        'criteria_id',
        'condition_id',
        'condition_option',
        'action',
        'status',
    ];

    protected $connection = 'mysqlfraud';

    public static function _all()
    {
        return Rule::where('status', '=', 'Active')->get();
    }
    public static function details($rule_id)
    {
        $rules = Rule::join('rule_criterias', 'rules.criteria_id', '=', 'rule_criterias.criteria_id')
        ->join('rule_conditions', 'rules.condition_id', '=', 'rule_conditions.condition_id')
        ->join('rule_condition_options', 'rules.condition_option_id', '=', 'rule_condition_options.condition_option_id')
        ->join('rule_actions', 'rules.action', '=', 'rule_actions.action_id')
        ->leftjoin('merchants', 'rules.merchant_id', '=', 'merchants.merchant_id')
        ->where('rules.rule_id', '=', $rule_id)
        ->select('rules.*', 'rule_criterias.criteria_name', 'rule_condition_options.condition_option_name', 'rule_conditions.condition_name', 'rule_actions.action_name', 'merchants.client_id', 'merchants.client_type')->first();

        return $rules;
    }
    public static function detailsbycriteria($criteria_id)
    {
        return Rule::where('status', '=', 'Active')->where('criteria_id', '=', $criteria_id)->get();
    }

    public static function detailsbycondition($condition_id)
    {
        return Rule::where('status', '=', 'Active')->where('condition_id', '=', $condition_id)->get();
    }

    public static function detailsbycoption($condition_option_id)
    {
        return Rule::where('status', '=', 'Active')->where('condition_option_id', '=', $condition_option_id)->get();
    }
    public static function ruleslist($request, $where='')
    {
        //$database1 =config('database.connections.mysql.database');
        //$database2 =config('database.connections.mysqlfraud.database');

        $item_per_page = !empty($request->rows)?$request->rows:ROW_PER_PAGE;

        $rules = Rule::join('rule_criterias', 'rules.criteria_id', '=', 'rule_criterias.criteria_id')
        ->join('rule_conditions', 'rules.condition_id', '=', 'rule_conditions.condition_id')
        ->join('rule_condition_options', 'rules.condition_option_id', '=', 'rule_condition_options.condition_option_id')
        ->join('rule_actions', 'rules.action', '=', 'rule_actions.action_id')
        ->leftjoin('merchants', 'rules.merchant_id', '=', 'merchants.merchant_id')
        //->leftjoin($database1.'.merchants as mmerchants', 'mmerchants.client_fraud_id', '=', 'merchants.merchant_id')
        //->leftjoin($database1.'.psps as mpsps', 'mpsps.client_fraud_id', '=', 'merchants.merchant_id')
        ->where(function ($query) use ($request) {
            //filter by keyword
            if (($search = $request->get('search'))) {
                $query->orWhere('rule_criterias.criteria_name', 'like', '%' . $search . '%');
                $query->orWhere('rule_conditions.condition_name', 'like', '%' . $search . '%');
                $query->orWhere('rule_actions.action_name', 'like', '%' . $search . '%');
                $query->orWhere('merchants.client_type', 'like', '%' . $search . '%');
                $query->orWhere('merchants.client_id', 'like', '%' . $search . '%');
                $query->orWhere('rules.condition_value', 'like', '%' . $search . '%');
                $query->orWhere('rules.status', 'like', '%' . $search . '%');
            }
        });
        if (!empty($where)) {
            $rules=$rules->where($where);
        }
        $rules=$rules->where('merchants.platform', '=', config('global.company'));
        $rules = $rules->select('rules.*', 'rule_criterias.criteria_name', 'rule_condition_options.condition_option_name', 'rule_conditions.condition_name', 'rule_actions.action_name', 'merchants.client_id', 'merchants.client_type', 'merchants.platform')->orderBy('created_at', 'desc')->paginate($item_per_page);

        return $rules;
    }

    public static function condition($criteria_id)
    {
        $conditions = DB::connection('mysqlfraud')->table('rule_conditions as o')
                ->join('rule_map_condition_to_criterias as map', 'o.condition_id', '=', 'map.condition_id')
                ->where('map.criteria_id', '=', $criteria_id)
                ->select('o.condition_id', 'o.condition_name')->get()->toArray();

        return $conditions;
    }

    public static function conditionOption($criteria_id)
    {
        $condition_options = DB::connection('mysqlfraud')->table('rule_condition_options as o')
                ->join('rule_map_condition_option_to_criterias as map', 'o.condition_option_id', '=', 'map.condition_option_id')
                ->where('map.criteria_id', '=', $criteria_id)
                ->select('o.condition_option_id', 'o.condition_option_name')->get()->toArray();

        return $condition_options;
    }
    public static function criteriaCondition($condition_option_id, $value=0)
    {
        $isp_types =  Rule_isp_type::_all()->toArray();
        $avsCodes =   Rule_avs_code::_all()->toArray();
        $cvvCodes =   Rule_cvv_code::_all()->toArray();
        $phoneTypes = Rule_phone_type::_all()->toArray();
        $cardTypes =  Rule_card_type::_all()->toArray();
        $paymentModes = Rule_payment_mode::_all()->toArray();
        $countries = Rule_country::countrylist();


        $response_json= [];

        $response_json['html'] = '<input class="form-control" value="N/A" disabled />';
        $response_json['has_value'] = 0;
        $condition_option_id = $condition_option_id;


        $condition_details = Rule_condition_option::details($condition_option_id)->toArray();


        if (isset($condition_details[0]) && !empty($condition_details[0])) {
            $response_json['has_value'] = $condition_details[0]['has_value'];
            if ($condition_details[0]['has_value'] == 1) {
                $details = json_decode(base64_decode($condition_details[0]['input_details']), true);

                if ($condition_details[0]['input_type'] == 'input') {
                    $response_json['html'] = '<input class="form-control"  id="condition_input" name="condition_input" placeholder="'.ucfirst(str_replace('_', ' ', $details['name'])).'" '.(!empty($value)? "value='".$value."'" : '').' />';
                } elseif ($condition_details[0]['input_type'] == 'selection') {
                    $selection_data = '';
                    $multiple = '';

                    if ($details['name'] == 'countries_list') {
                        foreach ($countries as $country_code=>$country_name) {
                            $selection_data.= '<option value="'.$country_code.'" '.(!empty($value) && $value == $country_code ? "selected='selected'" : '').' >'.$country_name.' </option>';
                        }
                        $multiple = 'multiple="multiple"';
                    } elseif ($details['name'] == 'isp_types') {
                        foreach ($isp_types as $isp) {
                            $selection_data.= '<option value="'.$isp['name'].'" '.(!empty($value) && $value == $isp['name'] ? "selected='selected'" : '').'>'.$isp['name'].' </option>';
                        }
                        $multiple = 'multiple="multiple"';
                    } elseif ($details['name'] == 'avs_code') {
                        foreach ($avsCodes as $avs) {
                            $selection_data.= '<option value="'.$avs['code'].'" '.(!empty($value) && $value == $avs['code'] ? "selected='selected'" : '').'>'.$avs['code'].' </option>';
                        }
                        $multiple = 'multiple="multiple"';
                    } elseif ($details['name'] == 'cvv_code') {
                        foreach ($cvvCodes as $cvv) {
                            $selection_data.= '<option value="'.$cvv['code'].'" '.(!empty($value) && $value == $cvv['code'] ? "selected='selected'" : '').'>'.$cvv['code'].' </option>';
                        }
                        $multiple = 'multiple="multiple"';
                    } elseif ($details['name'] == 'card_type') {
                        foreach ($cardTypes as $cards) {
                            $selection_data.= '<option value="'.$cards['cc_code'].'" '.(!empty($value) && $value == $cards['cc_code'] ? "selected='selected'" : '').'>'.$cards['cc_name'].' </option>';
                        }
                        $multiple = 'multiple="multiple"';
                    } elseif ($details['name'] == 'payment_mode') {
                        foreach ($paymentModes as $modes) {
                            $selection_data.= '<option value="'.$modes['payment_mode'].'" '.(!empty($value) && $value == $modes['payment_mode'] ? "selected='selected'" : '').'>'.$modes['payment_mode'].' </option>';
                        }
                        $multiple = 'multiple="multiple"';
                    }

                    $response_json['html'] = '<select class="form-control select2Dom" data-placeholder="Please Select" id="condition_input" name="condition_input[]" '.$multiple.' >'.$selection_data.'</select>';
                }
            }
        }

        return $response_json;
    }
}
