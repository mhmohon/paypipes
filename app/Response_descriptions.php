<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Response_descriptions extends Model
{
	 protected $fillable =[        
        'response_code',
		'response_description',
		'gateway',
        'status',
    ];
	
	public function account()
    {
        return $this->belongsTo('App\Account');
    }
	public function merchant()
    {
        return $this->belongsTo('App\Merchant');
    }
	public function admin()
    {
        return $this->belongsTo('App\Admin');
    }
   
}

