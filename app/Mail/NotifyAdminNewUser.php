<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyAdminNewUser extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data=[];
        $data['subject'] = config('global.company').' - Account Registration';
        $data['preview_text'] = 'Account Registration ...';
        $data['main_message'] = 'New Account '.$this->user->email. '';
        $data['note'] =
        'email:'. $this->user->email .'<br />'.

        $data['email'] = config('global.company_email');
        $data['first_name'] = $this->user->email;

        return $this->subject($data['subject'])->view('emails.app_email', $data);
    }
}
