<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendQrTwoFactorCode extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $qrCode;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $qrCode)
    {
        $this->user = $user;
        $this->qrCode = $qrCode;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data['subject'] = config('global.company').' - Google Authenticator';
        $data['preview_text'] = 'Setting up Google Authenticator';
        $data['main_message'] = '<img src="'.$this->qrCode.'" alt="">';
        $data['p1'] = 'Google Authenticator';
        $data['note'] = '<b>Setting up Google Authenticator</b><br>1. Visit the Apple Store or Google Play<br>2. Search for Google Authenticator<br>3. Downoad and install the application<br>4. Open the application and scan above QR Code<br>5. When login and asked to enter verification code open the application to get the 6 digit number from the application';
        $data['email'] = $this->user->email;
        $data['first_name'] = $this->user->first_name;
        return $this->subject($data['subject'])->view('emails.app_email', $data);
    }
}
