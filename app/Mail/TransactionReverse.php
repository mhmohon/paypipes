<?php

namespace App\Mail;

use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Entities\Transaction\Transaction;

class TransactionReverse extends Mailable
{
    use Queueable, SerializesModels;

    public $transaction;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Transaction $transaction)
    {
        $this->transaction =  $transaction;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if (count($this->transaction) > 0) {
            $this->transaction->terminals->website = preg_replace("(^https?://)", "", $this->transaction->terminals->website);


            $data['subject'] = config('global.company')." - Payment Canceled";
            $data['preview_text'] = $this->transaction->description;
            $data['main_message'] = 'Payment Canceled';
            $data['p1'] = 'Payment of <b>'.$this->transaction->amount.' '.$this->transaction->currency.'</b> for <b>'.$this->transaction->description.'</b> has been canceled by <b>'.$this->transaction->merchants->company_name.'</b> ('.$this->transaction->terminals->website.')';
            $data['p2_title'] = 'Payment';
            $data['p2'] = ' Amount: <b>'.$this->transaction->amount.' '.$this->transaction->currency.'</b><br>
                            Description: <b>'.$this->transaction->description.'</b><br>
                            Transaction ID: <b>'.$this->transaction->transaction_id.'</b><br>
                            Date: <b>'. Carbon::parse($this->transaction->created_at)->format('M d, Y \a\t H:i:s e').'</b><br>
                            '. ($this->transaction->merchants->descriptor? 'Payment will appear on your credit card statement as '.$this->transaction->merchants->descriptor:'');
            $data['p3_title'] = 'Merchant';
            $data['p3'] = $this->transaction->merchants->company_name.'<br>
                            '.$this->transaction->terminals->website.'<br>
                            '.$this->transaction->terminals->email.'<br>
                            '.$this->transaction->terminals->phone;
            $data['p4_title'] = 'Issues with this transaction?';
            $data['p4'] = 'Please contact with the merchant on '.$this->transaction->terminals->email.' to resolve the issue with the transaction. In case you are not able to resolve it with the merchant report this transaction within 180 days from the date of the transaction to payment@paypipes.com. For any communication use Transactins ID: '. $this->transaction->transaction_id;
            $data['note'] = 'Please do not reply to this email. This mailbox is not monitored and you will not receive a response.';
            $data['email'] = $this->transaction->customers->email;
            $data['name'] = $this->transaction->customers->first_name.' '.$this->transaction->customers->last_name;

            return $this->subject($data['subject'])->markdown('emails.transactions.transaction', $data);
        }
    }
}
