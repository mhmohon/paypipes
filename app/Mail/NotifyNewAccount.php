<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyNewAccount extends Mailable
{
    use Queueable, SerializesModels;

    public $user;
    public $token;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $token)
    {
        $this->user = $user;
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data['subject'] = config('global.company').' - Account User Account Registration';
        $data['preview_text'] = 'Thank you for joining us ...';
        $data['main_message'] = 'Welcome to '.config('global.company').'!';
        $data['p1'] = 'Thank you for joining us, '.$this->user->name??$this->user->email.'.';
        $data['p2_title'] = 'Getting Started';

        $data['p4'] = 'If you already setup you password please <a href="'.url('account/login').'">login</a>. Otherwise click on the button below.';
        $data['button_link'] = url('account/password/setup/'.$this->token);
        $data['button'] = 'Setup Password';
        $data['email'] = $this->user->email;
        $data['first_name'] = $this->user->name??$this->user->email;

        return $this->subject($data['subject'])->view('emails.app_email', $data);

        
    }
}
