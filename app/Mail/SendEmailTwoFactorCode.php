<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendEmailTwoFactorCode extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data['subject'] = config('global.company').' - Verification Code';
        $data['preview_text'] = 'Extra Security';
        $data['main_message'] = $this->user->secret;
        $data['p1'] = 'Enter above Verification Code in your Login.';
        $data['note'] = 'Extra Security! You can enable/disable two factor authentication in your account settings. ';
        $data['email'] = $this->user->email;
        $data['first_name'] = $this->user->first_name;
        
        return $this->subject($data['subject'])->view('emails.app_email', $data);

    }
}
