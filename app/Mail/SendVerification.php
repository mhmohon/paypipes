<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;


class SendVerification extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The user instance.
     *
     * @var user
     */
    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        
        $data['subject'] = config('global.company').' - Account Activation';
        $data['preview_text'] = 'Just one more step ...';
        $data['main_message'] = 'Just one more step ...';
        $data['p1'] = 'Click the button below to activate your account.';
        $data['button_link'] = url('account/verify-email/'.$this->user->verify_token);
        $data['button'] = 'Activate Account';
        $data['email'] = $this->user->email;
        $data['first_name'] = $this->user->name;

        return $this->subject($data['subject'])->view('emails.app_email', $data);


    }
}
