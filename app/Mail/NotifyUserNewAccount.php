<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyUserNewAccount extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data['subject'] = config('global.company').' - User Account Registration';
        $data['preview_text'] = 'Thank you for joining us ...';
        $data['main_message'] = 'Welcome to '.config('global.company').'!';
        $data['p1'] = 'Thank you for joining us, '.$this->user->name.'.';

        $data['button_link'] = url('account/login');
        $data['button'] = 'Get Started';
        $data['email'] = $this->user->email;
        $data['first_name'] = $this->user->name??$this->user->email;
        
        return $this->subject($data['subject'])->view('emails.app_email', $data);
    }
}
