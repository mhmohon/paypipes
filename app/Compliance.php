<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Compliance extends Model
{
    protected $fillable =[
        'compliance_id',
        'name', 
        'description',
        'file_name',
        'file_path',
        'status',
    ];
    
    public function admin()
    {
        return $this->belongsTo('App\Admin');
    }
}
