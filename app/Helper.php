<?php
namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;

//use CreditCard;

class Helper extends Model
{

    public function getIP()
    {
        $url='https://api.ipify.org';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);

        $ip = curl_exec($ch);

        if (curl_error($ch)) {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        curl_close($ch);

        return $ip;
    }
    /*
    * Debug public function
    */

    public function uniqidReal($length = 32)
    {
        // uniqid gives 32 chars, but you could adjust it to your needs.
        if (function_exists("random_bytes")) {
            $bytes = random_bytes(ceil($length / 2));
        } elseif (function_exists("openssl_random_pseudo_bytes")) {
            $bytes = openssl_random_pseudo_bytes(ceil($length / 2));
        } else {
            throw new Exception("no cryptographically secure random function available");
        }
        return substr(bin2hex($bytes), 0, $length);
    }

    public function currentSegment(Request $request = null)
    {
        $uri=$request->getRequestUri();
        $segments = explode('/', trim($request->getRequestUri()));
        $numSegments = count($segments);
        $currentSegment = $segments[$numSegments - 1];

        return $currentSegment;
    }


    public function getSettings()
    {
        $settings = \DB::table('settings')->get()->last();
        return $settings;
    }

    public function clientParam($transaction_id)
    {
        $setting = \DB::table('settings')->get()->last();
        $data=[];

        $integration_type = DB::table('terminals as tr')
                ->leftjoin('transactions as trx', 'trx.terminal_id', '=', 'tr.terminal_id')
                ->where(['trx.transaction_id'=>$transaction_id])
                ->value('tr.integration_type');


        if (isset($integration_type) && $integration_type=='Redirect') {
            $data['api_id']=$setting->api_id;
            $data['client_id']=$setting->client_id;
            $data['client_secret']=$setting->client_secret;
        } else {
            $data['api_id']=$setting->direct_api_id;
            $data['client_id']=$setting->direct_client_id;
            $data['client_secret']=$setting->direct_client_secret;
        }

        return [$data['api_id'],$data['client_id'],$data['client_secret']];
    }
    public function getTransDetails($tid)
    {
        $transDetails = DB::table('customer_billings as cb')
                ->leftjoin('transactions as trx', 'trx.billing_id', '=', 'cb.id')
                ->leftjoin('customer_cards as cc', 'trx.card_id', '=', 'cc.id')
                ->select('cb.first_name', 'cb.last_name', 'cb.street', 'cb.city', 'cb.state', 'cb.country', 'cb.post_code', 'cb.email', 'cb.phone', 'cc.card_holder', 'cc.card_number', 'cc.exp_month', 'cc.exp_year', 'cc.card_type', 'trx.*')
                ->where(['trx.id'=>$tid])
                ->first();

        if (count($transDetails) == 0) {
            return 0;
        } else {
            $crypto=new ApiEncrypt();

            $transDetails->card_number=$crypto->cardDecrypt($transDetails->card_number);

            return $transDetails;
        }
    }


    public function exp_monthyear()
    {
        $exp=[];

        $exp_month=[];
        for ($i=1; $i<=12; $i++) {
            if ($i<10) {
                $expm="0$i";
            } else {
                $expm=$i;
            }
            $exp_month[$expm]=$expm;
        }
        $exp[]=$exp_month;

        $start_exp_year=Carbon::now()->year;
        $exp_year=[];
        for ($i=0; $i<5; $i++) {
            $exp_year[$start_exp_year]=$start_exp_year;
            $start_exp_year=$start_exp_year+1;
        }
        $exp[]=$exp_year;

        return $exp;
    }
}
