<?php

namespace App\Listeners;

use App\Events\ConfirmNewAccount;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use App\Mail\NotifyUserNewAccount;
use App\Mail\NotifyNewAccount;

class ConfirmNewAccountListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ConfirmNewAccount  $event
     * @return void
     */
    public function handle(ConfirmNewAccount $event)
    {
        $email = $event->user->email;
        if($event->user->added_by != 'user')
        {
            Mail::to($email)->send(new NotifyNewAccount($event->user, $event->token));

        } else {

            Mail::to($email)->send(new NotifyUserNewAccount($event->user));

        }
    }   
}
