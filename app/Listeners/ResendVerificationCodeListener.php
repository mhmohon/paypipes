<?php

namespace App\Listeners;

use App\Events\ResendVerificationCode;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendVerification;

class ResendVerificationCodeListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ResendVerificationCode  $event
     * @return void
     */
    public function handle(ResendVerificationCode $event)
    {
        $email = $event->user->email;
        Mail::to($email)->send(new SendVerification($event->user));
    }
}
