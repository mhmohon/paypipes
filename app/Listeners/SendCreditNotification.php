<?php

namespace App\Listeners;

use Mail;
use App\Mail\TransactionCredit;
use App\Events\TransactionsCredit;
use Illuminate\Support\Facades\Log;
use App\Entities\Transaction\Transaction;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendCreditNotification implements ShouldQueue
{
    public $transaction;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Transaction $transaction)
    {
        $this->transaction =  $transaction;
    }

    /**
     * Handle the event.
     *
     * @param  TransactionsCredit  $event
     * @return void
     */
    public function handle(TransactionsCredit $event)
    {
        Mail::to($event->transaction->customers->email, $event->transaction->customers->first_name)->send(new TransactionCredit($event->transaction));
    }

    public function failed(Exception $exception)
    {
        Log::info('Failed Request: '.$exception);
        // Send user notification of failure, etc...
    }
}
