<?php

namespace App\Listeners;

use App\Events\SendTwoFactorCode;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendEmailTwoFactorCode;
use App\Mail\SendQrTwoFactorCode;

class SendTwoFactorCodeListener implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SendTwoFactorCode  $event
     * @return void
     */
    public function handle(SendTwoFactorCode $event)
    {
        $email = $event->user->email;
        $two_step = $event->user->two_step;

        if($two_step == 'qr'){
            Mail::to($email)->send(new SendQrTwoFactorCode($event->user, $event->qrCode));

        }elseif($two_step == 'email'){

            Mail::to($email)->send(new SendEmailTwoFactorCode($event->user));
        }
    }
}
