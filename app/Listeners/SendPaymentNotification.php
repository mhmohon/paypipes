<?php

namespace App\Listeners;

use Mail;
use App\Mail\TransactionPayment;
use App\Events\TransactionsPayment;
use App\Entities\Transaction\Transaction;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendPaymentNotification implements ShouldQueue
{
    public $transaction;


    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Transaction $transaction)
    {
        $this->transaction =  $transaction;
    }

    /**
     * Handle the event.
     *
     * @param  TransactionsPayment  $event
     * @return void
     */
    public function handle(TransactionsPayment $event)
    {
        Mail::to($event->transaction->customers->email, $event->transaction->customers->first_name)->send(new TransactionPayment($event->transaction));
    }
}
