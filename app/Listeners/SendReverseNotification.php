<?php

namespace App\Listeners;

use Mail;
use App\Mail\TransactionReverse;
use App\Events\TransactionsReverse;
use App\Entities\Transaction\Transaction;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendReverseNotification implements ShouldQueue
{
    public $transaction;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Transaction $transaction)
    {
        $this->transaction =  $transaction;
    }

    /**
     * Handle the event.
     *
     * @param  TransactionsCredit  $event
     * @return void
     */
    public function handle(TransactionsReverse $event)
    {
        Mail::to($event->transaction->customers->email, $event->transaction->customers->first_name)->send(new TransactionReverse($event->transaction));
    }
}
