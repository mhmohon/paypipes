<?php

namespace App\Listeners;

use Mail;
use App\Mail\TransactionRefund;
use App\Events\TransactionsRefund;
use App\Entities\Transaction\Transaction;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendRefundNotification implements ShouldQueue
{
    public $transaction;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Transaction $transaction)
    {
        $this->transaction =  $transaction;
    }

    /**
     * Handle the event.
     *
     * @param  TransactionsRefund  $event
     * @return void
     */
    public function handle(TransactionsRefund $event)
    {
        Mail::to($event->transaction->customers->email, $event->transaction->customers->first_name)->send(new TransactionRefund($event->transaction));
    }
}
