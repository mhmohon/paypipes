<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment_method extends Model
{
    protected $fillable =[
        'payment_method_id',
        'name',
        'description',
        'file_name',
        'file_path',
        'base_url',
        'authorize',
        'purchase',
        'reverse',
        'refund',
        'status',
    ];

    public static function gatewaylist()
    {
        return Payment_method::where('status', '=', 'active')->orderBy('name', 'asc')->pluck('description', 'name');
    }
}
