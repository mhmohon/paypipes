<?php

namespace App\Http\Helper;

use DB;
use Session;
use PDF;
use DateTime;
use App\Libraries\EncryptDecrypt;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Merchant;
use App\Psp;

class Common
{
    public static function numberToString($n)
    {
        return $n === 1 ? 'YES' : 'NO';
    }

    public function crypto_key()
    {
        $main_key = DB::table('main_key')->where('status', '=', 'Active')->orderBy('id', 'DESC')->first();
        $crypto_key = DB::table('crypto_key')->where('status', '=', 'Active')->orderBy('id', 'DESC')->first();
        $crypto = new EncryptDecrypt($main_key->main_key);
        return [$crypto,$crypto_key];
    }

    public function generateTestToken()
    {
        $mkey=config('global.testtoken_mainkey');
        $nkey=config('global.testtoken_key');
        $str = 'pp_paypies-'.bin2hex(random_bytes(8));

        $enc = new EncryptDecrypt($mkey);
        $token = $enc->encrypt_ecb($str, $nkey);

        return $token;
    }

    public function calculate_min($date)
    {
        $startdate = new DateTime();
        $todate = new DateTime($date);
        $interval = $startdate->diff($todate);
        $hours   = $interval->format('%h');
        $minutes = $interval->format('%i');
        $days = $interval->format("%d");
        $months = $interval->format("%m");
        $years = $interval->format("%y");

        if ($years > 0) {
            return ($hours * 60 + $minutes) + ($days * 24 * 60) + ($months * 30 *24 * 60) + ($years * 12 * 30 *24 * 60);
        } elseif ($months > 0) {
            return ($hours * 60 + $minutes) + ($days * 24 * 60) + ($months * 30 *24 * 60);
        }
        if ($days > 0) {
            return ($hours * 60 + $minutes) + ($days * 24 * 60);
        } else {
            return ($hours * 60 + $minutes);
        }
    }
    public function sqlToXml($fields, $queryResult, $rootElementName, $childElementName)
    {
        $xmlData = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>\n";
        $xmlData .= "<" . $rootElementName . ">";

        foreach ($queryResult as $key => $value) {
            $xmlData .= "<" . $childElementName . ">";

            for ($i = 0; $i < count($fields); $i++) {
                $fieldName = $fields[$i];
                $xmlData .= "<" . $fieldName . ">";


                if (!empty($value[$fieldName])) {
                    $xmlData .= $value[$fieldName];
                } else {
                    $xmlData .= "null";
                }

                $xmlData .= "</" . $fieldName . ">";
            }
            $xmlData .= "</" . $childElementName . ">";
        }
        $xmlData .= "</" . $rootElementName . ">";

        return $xmlData;
    }
    public function invoicePdf($data, $pdf_name, $title, $landscape, $name = 'Invoice')
    {
        $now = new DateTime("now");
        $invoice=$data['invoice'];
        $setting=$data['setting'];
        $bankaccount=$data['bankaccount'];
        $client=$data['client'];
        $i=1;

        $id = $invoice->invoice_id??$invoice->proforma_id;

        $str = "
		<!DOCTYPE html>
		<html lang='en'>
			<head>
				<style>
					html {
					  font-family: sans-serif;
					  -webkit-text-size-adjust: 100%;
					  -ms-text-size-adjust: 100%;
					  font-size: 10px;
					  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
					}
					body {
					  font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
					  font-size: 10px;
					  line-height: 1.42857143;
					  color: #333;
					  background-color: #fff;
					  margin: 0;
					  padding-top:45px;
					  page: main;
					}
					table {
					  border-spacing: 0;
					  border-collapse: collapse;
					}
					td,
					th {
					  padding: 0;
					}
					p {
					  margin: 0 0 10px;
					}
					.small {
					  font-size: 85%;
					}
					.text-right {
					  text-align: right;
					}
					.text-center {
					  text-align: center;
					}
					table {
					  background-color: transparent;
					}
					caption {
					  padding-top: 8px;
					  padding-bottom: 8px;
					  color: #777;
					  text-align: left;
					}
					th {
					  text-align: left;
					}
					.table {
					  width: 100%;
					  max-width: 100%;
					  margin-bottom: 20px;
					}
					.table > thead > tr > th,
					.table > tbody > tr > th,
					.table > tfoot > tr > th,
					.table > thead > tr > td,
					.table > tbody > tr > td,
					.table > tfoot > tr > td {
					  padding: 8px;
					  line-height: 1.42857143;
					  vertical-align: top;
					  border-top: 1px solid #ddd;
					}
					.table > thead > tr > th {
					  vertical-align: bottom;
					  border-bottom: 2px solid #ddd;
					}
					.table > caption + thead > tr:first-child > th,
					.table > colgroup + thead > tr:first-child > th,
					.table > thead:first-child > tr:first-child > th,
					.table > caption + thead > tr:first-child > td,
					.table > colgroup + thead > tr:first-child > td,
					.table > thead:first-child > tr:first-child > td {
					  border-top: 0;
					}
					.table > tbody + tbody {
					  border-top: 2px solid #ddd;
					}
					.table .table {
					  background-color: #fff;
					}
					.table-condensed > thead > tr > th,
					.table-condensed > tbody > tr > th,
					.table-condensed > tfoot > tr > th,
					.table-condensed > thead > tr > td,
					.table-condensed > tbody > tr > td,
					.table-condensed > tfoot > tr > td {
					  padding: 5px;
					}
					.table-bordered {
					  border: 1px solid #ddd;
					}
					.table-bordered > thead > tr > th,
					.table-bordered > tbody > tr > th,
					.table-bordered > tfoot > tr > th,
					.table-bordered > thead > tr > td,
					.table-bordered > tbody > tr > td,
					.table-bordered > tfoot > tr > td {
					  border: 1px solid #ddd;
					}
					.table-bordered > thead > tr > th,
					.table-bordered > thead > tr > td {
					  border-bottom-width: 2px;
					}
					.table-striped > tbody > tr:nth-of-type(odd) {
					  background-color: #f9f9f9;
					}
					.table-hover > tbody > tr:hover {
					  background-color: #f5f5f5;
					}
					table col[class*='col-'] {
					  position: static;
					  display: table-column;
					  float: none;
					}
					table td[class*='col-'],
					table th[class*='col-'] {
					  position: static;
					  display: table-cell;
					  float: none;
					}
					.table > thead > tr > td.active,
					.table > tbody > tr > td.active,
					.table > tfoot > tr > td.active,
					.table > thead > tr > th.active,
					.table > tbody > tr > th.active,
					.table > tfoot > tr > th.active,
					.table > thead > tr.active > td,
					.table > tbody > tr.active > td,
					.table > tfoot > tr.active > td,
					.table > thead > tr.active > th,
					.table > tbody > tr.active > th,
					.table > tfoot > tr.active > th {
					  background-color: #f5f5f5;
					}
					.table-hover > tbody > tr > td.active:hover,
					.table-hover > tbody > tr > th.active:hover,
					.table-hover > tbody > tr.active:hover > td,
					.table-hover > tbody > tr:hover > .active,
					.table-hover > tbody > tr.active:hover > th {
					  background-color: #e8e8e8;
					}
					.table > thead > tr > td.success,
					.table > tbody > tr > td.success,
					.table > tfoot > tr > td.success,
					.table > thead > tr > th.success,
					.table > tbody > tr > th.success,
					.table > tfoot > tr > th.success,
					.table > thead > tr.success > td,
					.table > tbody > tr.success > td,
					.table > tfoot > tr.success > td,
					.table > thead > tr.success > th,
					.table > tbody > tr.success > th,
					.table > tfoot > tr.success > th {
					  background-color: #dff0d8;
					}
					.table-hover > tbody > tr > td.success:hover,
					.table-hover > tbody > tr > th.success:hover,
					.table-hover > tbody > tr.success:hover > td,
					.table-hover > tbody > tr:hover > .success,
					.table-hover > tbody > tr.success:hover > th {
					  background-color: #d0e9c6;
					}
					.table > thead > tr > td.info,
					.table > tbody > tr > td.info,
					.table > tfoot > tr > td.info,
					.table > thead > tr > th.info,
					.table > tbody > tr > th.info,
					.table > tfoot > tr > th.info,
					.table > thead > tr.info > td,
					.table > tbody > tr.info > td,
					.table > tfoot > tr.info > td,
					.table > thead > tr.info > th,
					.table > tbody > tr.info > th,
					.table > tfoot > tr.info > th {
					  background-color: #d9edf7;
					}
					.table-hover > tbody > tr > td.info:hover,
					.table-hover > tbody > tr > th.info:hover,
					.table-hover > tbody > tr.info:hover > td,
					.table-hover > tbody > tr:hover > .info,
					.table-hover > tbody > tr.info:hover > th {
					  background-color: #c4e3f3;
					}
					.table > thead > tr > td.warning,
					.table > tbody > tr > td.warning,
					.table > tfoot > tr > td.warning,
					.table > thead > tr > th.warning,
					.table > tbody > tr > th.warning,
					.table > tfoot > tr > th.warning,
					.table > thead > tr.warning > td,
					.table > tbody > tr.warning > td,
					.table > tfoot > tr.warning > td,
					.table > thead > tr.warning > th,
					.table > tbody > tr.warning > th,
					.table > tfoot > tr.warning > th {
					  background-color: #fcf8e3;
					}
					.table-hover > tbody > tr > td.warning:hover,
					.table-hover > tbody > tr > th.warning:hover,
					.table-hover > tbody > tr.warning:hover > td,
					.table-hover > tbody > tr:hover > .warning,
					.table-hover > tbody > tr.warning:hover > th {
					  background-color: #faf2cc;
					}
					.table > thead > tr > td.danger,
					.table > tbody > tr > td.danger,
					.table > tfoot > tr > td.danger,
					.table > thead > tr > th.danger,
					.table > tbody > tr > th.danger,
					.table > tfoot > tr > th.danger,
					.table > thead > tr.danger > td,
					.table > tbody > tr.danger > td,
					.table > tfoot > tr.danger > td,
					.table > thead > tr.danger > th,
					.table > tbody > tr.danger > th,
					.table > tfoot > tr.danger > th {
					  background-color: #f2dede;
					}
					.table-hover > tbody > tr > td.danger:hover,
					.table-hover > tbody > tr > th.danger:hover,
					.table-hover > tbody > tr.danger:hover > td,
					.table-hover > tbody > tr:hover > .danger,
					.table-hover > tbody > tr.danger:hover > th {
					  background-color: #ebcccc;
					}
					.table-responsive {
					  min-height: .01%;
					  overflow-x: auto;
					}
					@media screen and (max-width: 767px) {
					  .table-responsive {
						width: 100%;
						margin-bottom: 15px;
						overflow-y: hidden;
						-ms-overflow-style: -ms-autohiding-scrollbar;
						border: 1px solid #ddd;
					  }
					  .table-responsive > .table {
						margin-bottom: 0;
					  }
					  .table-responsive > .table > thead > tr > th,
					  .table-responsive > .table > tbody > tr > th,
					  .table-responsive > .table > tfoot > tr > th,
					  .table-responsive > .table > thead > tr > td,
					  .table-responsive > .table > tbody > tr > td,
					  .table-responsive > .table > tfoot > tr > td {
						white-space: nowrap;
					  }
					  .table-responsive > .table-bordered {
						border: 0;
					  }
					  .table-responsive > .table-bordered > thead > tr > th:first-child,
					  .table-responsive > .table-bordered > tbody > tr > th:first-child,
					  .table-responsive > .table-bordered > tfoot > tr > th:first-child,
					  .table-responsive > .table-bordered > thead > tr > td:first-child,
					  .table-responsive > .table-bordered > tbody > tr > td:first-child,
					  .table-responsive > .table-bordered > tfoot > tr > td:first-child {
						border-left: 0;
					  }
					  .table-responsive > .table-bordered > thead > tr > th:last-child,
					  .table-responsive > .table-bordered > tbody > tr > th:last-child,
					  .table-responsive > .table-bordered > tfoot > tr > th:last-child,
					  .table-responsive > .table-bordered > thead > tr > td:last-child,
					  .table-responsive > .table-bordered > tbody > tr > td:last-child,
					  .table-responsive > .table-bordered > tfoot > tr > td:last-child {
						border-right: 0;
					  }
					  .table-responsive > .table-bordered > tbody > tr:last-child > th,
					  .table-responsive > .table-bordered > tfoot > tr:last-child > th,
					  .table-responsive > .table-bordered > tbody > tr:last-child > td,
					  .table-responsive > .table-bordered > tfoot > tr:last-child > td {
						border-bottom: 0;
					  }
					}
					@page {
						counter-reset: page;
					}
					.header { position: fixed; top: 0px; height:40px;}
					.footer { position: fixed; bottom: 0px;}
					.pagenum:before { content: 'Page: ' counter(page); }

				</style>
			</head>
			<body>
				<div class='header'>
					<table class='table'>
						<tr>
							<td style='border:0;'><p><img src='img/logo.png' style='height:30px;'></p></td>
							<td class='text-right' style='border:0;'><span style='font-size:18px;font-weight:300;'>#".$id."</span></td>
						</tr>
					</table>
				</div>
				<div class='footer'>
					<table class='table'>
						<tr>
							<td>". config('global.company_legal') ."  ". config('global.company_url') ."</td>
							<td class='text-right' ><span class='pagenum'></span></td>
						</tr>
					</table>
				</div>
				<div>
					<table class='table'>
						<tr>
							<td>
								<h3>Bill To:</h3>
								".$client->company_name."<br/>
								". $client->street  ." <br/>
								". $client->city;
        if (isset($client->state)) {
            $str.="<br/>
									". $client->state;
        }
        $str.="<br/>
								". $client->post_code  ." <br/>
								". $client->country_name  ." <br/>
								". $client->email  ." <br/>
								<br/>
								<h3>For:</h3>
								". $invoice->description ."
							</td>
							<td>
								<h3>Pay To:</h3>
								". $setting->company_legal_name ."<br/>
								". $setting->street ."<br/>
								". $setting->city ." ". $setting->post_code;
        if (isset($setting->state)) {
            $str.="<br/>
									". $setting->state;
        }
        $str.="<br/>
								". $setting->country_name ."<br/>
								ID: ". $setting->company_id ."<br/>
								VAT ID: ". $setting->vat_id ."<br/>
								". $setting->invoice_website ."<br/>
								". $setting->invoice_email ."<br/>
							</td>
							<td style='width:30%'>
								<h3 class='text-right'>".$name." Details:</h3>
								<table class='table table-condensed'>
									<tbody>";
        if (isset($invoice->invoice_id)) {
            $str.="<tr>
											<td style='border:0px;'>Invoice Number:</td>
											<td style='border:0px;' class='text-right'>". $invoice->invoice_id ."</td>
										</tr>";
        }
        $str.="<tr>
											<td style='border:0px;'>Proforma Number:</td>
											<td style='border:0px;' class='text-right'>". $invoice->proforma_id ."</td>
										</tr>";

        $str.="<tr>
											<td style='border:0px;'>".$name." Date:</td>
											<td style='border:0px;' class='text-right'>". date('M d, Y', strtotime($invoice->invoice_date)) ."</td>
										</tr>
										<tr>
											<td style='border:0px;'>Due Date:</td>
											<td style='border:0px;' class='text-right'>".date('M d, Y', strtotime($invoice->due_date)) ."</td>
										</tr>
									</tbody>
								</table>
							</td>
						</tr>
					</table>
				</div>
				<div>
					<table class='table table-striped'>
						<thead>
							<tr>
								<th style='width:1%'> # </th>
								<th> Item </th>
								<th style='width:1%' class='text-right'> Unit Cost </th>
								<th style='width:1%' class='text-center'> Qty </th>
								<th style='width:1%' class='text-right'> Amount </th>
							</tr>
						</thead>
						<tbody>";
        if (isset($invoice->amount_creation_fee)) {
            $str.="<tr>
								<td> ".$i." </td>
								<td> Creation Fee </td>
								<td class='text-right' nowrap> ". $invoice->amount_creation_fee ." ". $invoice->currency ."</td>
								<td class='text-center'> ". $invoice->qty_creation_fee ."</td>
								<td class='text-right' nowrap> ". number_format($invoice->total_creation_fee, 2) ." ". $invoice->currency ."</td>
							</tr>";
            $i++;
        }
        if (isset($invoice->amount_monthly_fee)) {
            $str.="<tr>
								<td> ".$i." </td>
								<td> Monthly Fee </td>
								<td class='text-right' nowrap> ". $invoice->amount_monthly_fee ." ". $invoice->currency ."</td>
								<td class='text-center'> ". $invoice->qty_monthly_fee ."</td>
								<td class='text-right' nowrap> ". number_format($invoice->total_monthly_fee, 2) ." ". $invoice->currency ." </td>
							</tr>";
            $i++;
        }
        if (isset($invoice->amount_merchant_fee)) {
            $str.="<tr>
								<td> ".$i." </td>
								<td> Merchant Fee </td>
								<td class='text-right' nowrap> ". $invoice->amount_merchant_fee ." ". $invoice->currency ."</td>
								<td class='text-center'> ". $invoice->qty_merchant_fee ."</td>
								<td class='text-right' nowrap> ". number_format($invoice->total_merchant_fee, 2) ." ". $invoice->currency ." </td>
							</tr>";
            $i++;
        }
        if (isset($invoice->amount_terminal_fee)) {
            $str.="<tr>
								<td> ".$i." </td>
								<td> Terminal Fee </td>
								<td class='text-right'> ". $invoice->amount_terminal_fee ." ". $invoice->currency ."</td>
								<td class='text-center'> ". $invoice->qty_terminal_fee ."</td>
								<td class='text-right' nowrap> ". number_format($invoice->total_terminal_fee, 2) ." ". $invoice->currency ." </td>
							</tr>";
            $i++;
        }
        if (isset($invoice->amount_gateway_fee)) {
            $str.="<tr>
								<td> ".$i." </td>
								<td> Gateway Fee </td>
								<td class='text-right' nowrap> ". $invoice->amount_gateway_fee ." ". $invoice->currency ."</td>
								<td class='text-center'> ". $invoice->qty_gateway_fee ."</td>
								<td class='text-right' nowrap> ". number_format($invoice->total_gateway_fee, 2) ." ". $invoice->currency ." </td>
							</tr>";
            $i++;
        }
        if (isset($invoice->amount_transaction_fee)) {
            $str.="<tr>
								<td> ".$i." </td>
								<td> Transaction Fee </td>
								<td class='text-right' nowrap> ". $invoice->amount_transaction_fee ." ". $invoice->currency ."</td>
								<td class='text-center'> ". $invoice->qty_transaction_fee ."</td>
								<td class='text-right' nowrap> ". number_format($invoice->total_transaction_fee, 2) ." ". $invoice->currency ." </td>
							</tr>";
            $i++;
        }
        if (isset($invoice->amount_others_fee)) {
            $str.="<tr>
								<td> ".$i." </td>
								<td> ". $invoice->item_others_fee ." </td>
								<td class='text-right' nowrap> ". $invoice->amount_others_fee ." ". $invoice->currency ."</td>
								<td class='text-center'> ". $invoice->qty_others_fee ."</td>
								<td class='text-right' nowrap> ". number_format($invoice->total_others_fee, 2) ." ". $invoice->currency ." </td>
							</tr>";
            $i++;
        }
        $str.="</tbody>
					</table>
				</div>
				<div>
					<table class='table'>
						<tbody>
							<tr>
								<td style='border:0px;'>
									Please transfer the amount of ". number_format($invoice->total, 2) ." ". $invoice->currency ." to:<br/>
									<br/>Account Holder: ". $bankaccount->account_holder ."
									<br/> Account Number: ". $bankaccount->account_number ."
									<br/> Bank Name: ". $bankaccount->bank_name;
        if (isset($bankaccount->bank_address)) {
            $str.="<br/>
									Bank Address: ". $bankaccount->bank_address;
        }
        $str.="<br/> IBAN: ". $bankaccount->iban ."
									<br/> SWIFT/BIC: ". $bankaccount->swift_bic ."
								</td>
								<td style='width:45%; border:0px;'>
									<table class='table'>
										<tr>
											<td style='border:0px;'>Sub Total:</td>
											<td style='border:0px;' class='text-right'>". number_format(($invoice->total-$invoice->total_tax), 2) ." ". $invoice->currency ."</td>
										</tr>
										<tr>
											<td style='border:0px;'>VAT (". $invoice->tax ."%):</td>
											<td style='border:0px;' class='text-right'> ". number_format($invoice->total_tax, 2) ." ". $invoice->currency ."</td>
										</tr>
										<tr>
											<td style='border:0px;'><span style='font-size:22px;font-weight:300;'>Total:</span></td>
											<td style='border:0px;' class='text-right'><span style='font-size:22px;font-weight:300;'> ". number_format($invoice->total, 2) ." ". $invoice->currency ." </span></td>
										</tr>
									</table>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div>
					<h3 class='text-center'>Thank you for your business!</h3>
				</div>
				";



        $str .= "</body>
		</html>";

        $pdf = PDF::loadHTML($str)->setPaper('a4', $landscape);
        $pdf_name = $pdf_name.'.pdf';
        return $pdf->download($pdf_name);
    }
    public function turnoverToPdf($dataResult, $fields, $pdf_name, $headers, $title='Title', $landscape='portrait', $company, $websites)
    {
        $data =  $dataResult['result'];

        $turnover="<tr>
						<td style='border:0;width:50%'>
							<table>
								<tbody style='border:0;'>
									<tr>
										<td colspan='2' style='border:0;'>
											<p>
												Period: <b>".date('d.m.Y', strtotime($dataResult['start_date']))." - ".date('d.m.Y', strtotime($dataResult['end_date']))."</b><br>
											</p>
										</td>
									</tr>

								</tbody>
							</table>
						</td>
						<td style='border:0;width:50%'>
							<p>	Company: ".$company->company_name."<br>
								Address: ".$company->street.", ".$company->city.", ".$company->state.", ".$company->post_code.", ".$company->country_name."<br>
								Website(s): ";
        $i =0;
        foreach ($websites as $website) {
            $i++;
            if (($i % 3) == 1) {
                $turnover.="<br>";
            }
            $turnover.= $website.' ';
        }

        $turnover.="<br>";

        if (isset($company->merchant_id)) {
            $turnover.=" Merchant ID ".$company->merchant_id;
        }
        /*if (isset($statement->terminal_id)) {
            $monthly_statement.=" Terminal ID ".$statement->terminal_id;
        }*/
        $turnover.="				</p>
						</td>
					</tr>";


        $str = "
		<!DOCTYPE html>
		<html lang='en'>
			<head>
				<style>
					html {
					  font-family: sans-serif;
					  -webkit-text-size-adjust: 100%;
					  -ms-text-size-adjust: 100%;
					  font-size: 10px;
					  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
					}
					body {
					  font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
					  font-size: 10px;
					  line-height: 1.42857143;
					  color: #333;
					  background-color: #fff;
					  margin: 0;
					  padding-top:45px;
					  page: main;
					}
					table {
					  border-spacing: 0;
					  border-collapse: collapse;
					}
					td,
					th {
					  padding: 0;
					}
					p {
					  margin: 0 0 10px;
					}
					.small {
					  font-size: 85%;
					}
					.text-right {
					  text-align: right;
					}
					.text-center {
					  text-align: center;
					}
					table {
					  background-color: transparent;
					}
					caption {
					  padding-top: 8px;
					  padding-bottom: 8px;
					  color: #777;
					  text-align: left;
					}
					th {
					  text-align: left;
					}
					.table {
					  width: 100%;
					  max-width: 100%;
					  margin-bottom: 20px;
					}
					.table > thead > tr > th,
					.table > tbody > tr > th,
					.table > tfoot > tr > th,
					.table > thead > tr > td,
					.table > tbody > tr > td,
					.table > tfoot > tr > td {
					  padding: 8px;
					  line-height: 1.42857143;
					  vertical-align: top;
					  border-top: 1px solid #ddd;
					}
					.table > thead > tr > th {
					  vertical-align: bottom;
					  border-bottom: 2px solid #ddd;
					}
					.table > caption + thead > tr:first-child > th,
					.table > colgroup + thead > tr:first-child > th,
					.table > thead:first-child > tr:first-child > th,
					.table > caption + thead > tr:first-child > td,
					.table > colgroup + thead > tr:first-child > td,
					.table > thead:first-child > tr:first-child > td {
					  border-top: 0;
					}
					.table > tbody + tbody {
					  border-top: 2px solid #ddd;
					}
					.table .table {
					  background-color: #fff;
					}
					.table-condensed > thead > tr > th,
					.table-condensed > tbody > tr > th,
					.table-condensed > tfoot > tr > th,
					.table-condensed > thead > tr > td,
					.table-condensed > tbody > tr > td,
					.table-condensed > tfoot > tr > td {
					  padding: 5px;
					}
					.table-bordered {
					  border: 1px solid #ddd;
					}
					.table-bordered > thead > tr > th,
					.table-bordered > tbody > tr > th,
					.table-bordered > tfoot > tr > th,
					.table-bordered > thead > tr > td,
					.table-bordered > tbody > tr > td,
					.table-bordered > tfoot > tr > td {
					  border: 1px solid #ddd;
					}
					.table-bordered > thead > tr > th,
					.table-bordered > thead > tr > td {
					  border-bottom-width: 2px;
					}
					.table-striped > tbody > tr:nth-of-type(odd) {
					  background-color: #f9f9f9;
					}
					.table-hover > tbody > tr:hover {
					  background-color: #f5f5f5;
					}
					table col[class*='col-'] {
					  position: static;
					  display: table-column;
					  float: none;
					}
					table td[class*='col-'],
					table th[class*='col-'] {
					  position: static;
					  display: table-cell;
					  float: none;
					}
					.table > thead > tr > td.active,
					.table > tbody > tr > td.active,
					.table > tfoot > tr > td.active,
					.table > thead > tr > th.active,
					.table > tbody > tr > th.active,
					.table > tfoot > tr > th.active,
					.table > thead > tr.active > td,
					.table > tbody > tr.active > td,
					.table > tfoot > tr.active > td,
					.table > thead > tr.active > th,
					.table > tbody > tr.active > th,
					.table > tfoot > tr.active > th {
					  background-color: #f5f5f5;
					}
					.table-hover > tbody > tr > td.active:hover,
					.table-hover > tbody > tr > th.active:hover,
					.table-hover > tbody > tr.active:hover > td,
					.table-hover > tbody > tr:hover > .active,
					.table-hover > tbody > tr.active:hover > th {
					  background-color: #e8e8e8;
					}
					.table > thead > tr > td.success,
					.table > tbody > tr > td.success,
					.table > tfoot > tr > td.success,
					.table > thead > tr > th.success,
					.table > tbody > tr > th.success,
					.table > tfoot > tr > th.success,
					.table > thead > tr.success > td,
					.table > tbody > tr.success > td,
					.table > tfoot > tr.success > td,
					.table > thead > tr.success > th,
					.table > tbody > tr.success > th,
					.table > tfoot > tr.success > th {
					  background-color: #dff0d8;
					}
					.table-hover > tbody > tr > td.success:hover,
					.table-hover > tbody > tr > th.success:hover,
					.table-hover > tbody > tr.success:hover > td,
					.table-hover > tbody > tr:hover > .success,
					.table-hover > tbody > tr.success:hover > th {
					  background-color: #d0e9c6;
					}
					.table > thead > tr > td.info,
					.table > tbody > tr > td.info,
					.table > tfoot > tr > td.info,
					.table > thead > tr > th.info,
					.table > tbody > tr > th.info,
					.table > tfoot > tr > th.info,
					.table > thead > tr.info > td,
					.table > tbody > tr.info > td,
					.table > tfoot > tr.info > td,
					.table > thead > tr.info > th,
					.table > tbody > tr.info > th,
					.table > tfoot > tr.info > th {
					  background-color: #d9edf7;
					}
					.table-hover > tbody > tr > td.info:hover,
					.table-hover > tbody > tr > th.info:hover,
					.table-hover > tbody > tr.info:hover > td,
					.table-hover > tbody > tr:hover > .info,
					.table-hover > tbody > tr.info:hover > th {
					  background-color: #c4e3f3;
					}
					.table > thead > tr > td.warning,
					.table > tbody > tr > td.warning,
					.table > tfoot > tr > td.warning,
					.table > thead > tr > th.warning,
					.table > tbody > tr > th.warning,
					.table > tfoot > tr > th.warning,
					.table > thead > tr.warning > td,
					.table > tbody > tr.warning > td,
					.table > tfoot > tr.warning > td,
					.table > thead > tr.warning > th,
					.table > tbody > tr.warning > th,
					.table > tfoot > tr.warning > th {
					  background-color: #fcf8e3;
					}
					.table-hover > tbody > tr > td.warning:hover,
					.table-hover > tbody > tr > th.warning:hover,
					.table-hover > tbody > tr.warning:hover > td,
					.table-hover > tbody > tr:hover > .warning,
					.table-hover > tbody > tr.warning:hover > th {
					  background-color: #faf2cc;
					}
					.table > thead > tr > td.danger,
					.table > tbody > tr > td.danger,
					.table > tfoot > tr > td.danger,
					.table > thead > tr > th.danger,
					.table > tbody > tr > th.danger,
					.table > tfoot > tr > th.danger,
					.table > thead > tr.danger > td,
					.table > tbody > tr.danger > td,
					.table > tfoot > tr.danger > td,
					.table > thead > tr.danger > th,
					.table > tbody > tr.danger > th,
					.table > tfoot > tr.danger > th {
					  background-color: #f2dede;
					}
					.table-hover > tbody > tr > td.danger:hover,
					.table-hover > tbody > tr > th.danger:hover,
					.table-hover > tbody > tr.danger:hover > td,
					.table-hover > tbody > tr:hover > .danger,
					.table-hover > tbody > tr.danger:hover > th {
					  background-color: #ebcccc;
					}
					.table-responsive {
					  min-height: .01%;
					  overflow-x: auto;
					}
					@media screen and (max-width: 767px) {
					  .table-responsive {
						width: 100%;
						margin-bottom: 15px;
						overflow-y: hidden;
						-ms-overflow-style: -ms-autohiding-scrollbar;
						border: 1px solid #ddd;
					  }
					  .table-responsive > .table {
						margin-bottom: 0;
					  }
					  .table-responsive > .table > thead > tr > th,
					  .table-responsive > .table > tbody > tr > th,
					  .table-responsive > .table > tfoot > tr > th,
					  .table-responsive > .table > thead > tr > td,
					  .table-responsive > .table > tbody > tr > td,
					  .table-responsive > .table > tfoot > tr > td {
						white-space: nowrap;
					  }
					  .table-responsive > .table-bordered {
						border: 0;
					  }
					  .table-responsive > .table-bordered > thead > tr > th:first-child,
					  .table-responsive > .table-bordered > tbody > tr > th:first-child,
					  .table-responsive > .table-bordered > tfoot > tr > th:first-child,
					  .table-responsive > .table-bordered > thead > tr > td:first-child,
					  .table-responsive > .table-bordered > tbody > tr > td:first-child,
					  .table-responsive > .table-bordered > tfoot > tr > td:first-child {
						border-left: 0;
					  }
					  .table-responsive > .table-bordered > thead > tr > th:last-child,
					  .table-responsive > .table-bordered > tbody > tr > th:last-child,
					  .table-responsive > .table-bordered > tfoot > tr > th:last-child,
					  .table-responsive > .table-bordered > thead > tr > td:last-child,
					  .table-responsive > .table-bordered > tbody > tr > td:last-child,
					  .table-responsive > .table-bordered > tfoot > tr > td:last-child {
						border-right: 0;
					  }
					  .table-responsive > .table-bordered > tbody > tr:last-child > th,
					  .table-responsive > .table-bordered > tfoot > tr:last-child > th,
					  .table-responsive > .table-bordered > tbody > tr:last-child > td,
					  .table-responsive > .table-bordered > tfoot > tr:last-child > td {
						border-bottom: 0;
					  }
					}
					@page {
						counter-reset: page;
					}
					.header { position: fixed; top: 0px; height:40px;}
					.footer { position: fixed; bottom: 0px;}
					.pagenum:before { content: 'Page: ' counter(page); }

				</style>
			</head>
			<body>
				<div class='header'>
					<table >
						<tr>
							<td colspan='2' style='border:0;'><p><img src='img/logo.png' style='height:40px;'></p></td>
						</tr>
					</table>
				</div>
				<div class='footer'>
					<table class='table'>
						<tr>
							<td>". config('global.company_legal') ."  ". config('global.company_url') ."</td>
							<td class='text-right' ><span class='pagenum'></span></td>
						</tr>
					</table>
				</div>
				<table class='table'>";


        $str.=$turnover;


        $str.="</table>
				<h3> ".$title." </h3>
				<table class='table table-sm table-bordered'>
					<tr>";
        $str .= '<th>#</th>';
        foreach ($headers as $header) {
            $str .= '<th>'.$header.'</th>';
        }
        $str .="</tr>";


        $i=1;
        foreach ($data as $key => $value) {
            $str .= '<tr>';
            $str .= '<td>'.$i++.'</td>';
            foreach ($fields as $field) {
                $str .= '<td>'.$value[$field].'</td>';
            }
            $str .= '.</tr>';
        }


        $str .= "</table></body>
		</html>";

        $pdf = PDF::loadHTML($str)->setPaper('a4', $landscape);
        $pdf_name = $pdf_name.'.pdf';
        return $pdf->download($pdf_name);
    }
    public function dataToPdf($data, $fields, $pdf_name, $headers, $title='Title', $landscape='portrait', $monthlystate=[], $mstatement=0, $debit=0)
    {
        $dt=Carbon::now();
        if ($mstatement==1) {
            $statement=$monthlystate['statement'];
            $company=$monthlystate['company'];
            $websites=$monthlystate['websites'];

            $monthly_statement="<tr>
						<td style='border:0;width:50%'>
							<table>
								<tbody style='border:0;'>
									<tr>
										<td colspan='2' style='border:0;'>
											<p>
												".$dt->createFromDate($statement->year, $statement->month)->format('F')." ".$statement->year."<br>
												Period: <b>".date('d.m.Y', strtotime($statement->start_date))." - ".date('d.m.Y', strtotime($statement->end_date))."</b><br>
											</p>
										</td>
									</tr>
									<tr>
										<td style='border:0;'>Credit:</td>
										<td style='border:0;' class='text-right'><b>".number_format($statement->credits, 2, '.', '')."</b></td>
									</tr>
									<tr>
										<td style='border:0;'>Debit:</td>
										<td style='border:0;' class='text-right'><b>".number_format($statement->debits, 2, '.', '')."</b></td>
									</tr>
									<tr>
										<td style='border:0;'>Total:</td>
										<td style='border:0;' class='text-right'><b>".number_format($statement->total, 2, '.', '')."</b></td>
									</tr>
									<tr>
										<td style='border:0;'>Currency:</td>
										<td style='border:0;' class='text-right'><b>".$statement->currency."</b></td>
									</tr>
								</tbody>
							</table>
						</td>
						<td style='border:0;width:50%'>
							<p>	Company: ".$company->company_name."<br>
								Address: ".$company->street.", ".$company->city.", ".$company->state.", ".$company->post_code.", ".$company->country_name."<br>
								Website(s): ".$websites."<br>";
            if (isset($statement->psp_id)) {
                $monthly_statement.=" Psp ID ".$statement->psp_id;
            }
            if (isset($statement->merchant_id)) {
                $monthly_statement.=" Merchant ID ".$statement->merchant_id;
            }
            if (isset($statement->terminal_id)) {
                $monthly_statement.=" Terminal ID ".$statement->terminal_id;
            }
            $monthly_statement.="				</p>
						</td>
					</tr>";
        }

        $str = "
		<!DOCTYPE html>
		<html lang='en'>
			<head>
				<style>
					html {
					  font-family: sans-serif;
					  -webkit-text-size-adjust: 100%;
					  -ms-text-size-adjust: 100%;
					  font-size: 10px;
					  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
					}
					body {
					  font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
					  font-size: 10px;
					  line-height: 1.42857143;
					  color: #333;
					  background-color: #fff;
					  margin: 0;
					  padding-top:45px;
					  page: main;
					}
					table {
					  border-spacing: 0;
					  border-collapse: collapse;
					}
					td,
					th {
					  padding: 0;
					}
					p {
					  margin: 0 0 10px;
					}
					.small {
					  font-size: 85%;
					}
					.text-right {
					  text-align: right;
					}
					.text-center {
					  text-align: center;
					}
					table {
					  background-color: transparent;
					}
					caption {
					  padding-top: 8px;
					  padding-bottom: 8px;
					  color: #777;
					  text-align: left;
					}
					th {
					  text-align: left;
					}
					.table {
					  width: 100%;
					  max-width: 100%;
					  margin-bottom: 20px;
					}
					.table > thead > tr > th,
					.table > tbody > tr > th,
					.table > tfoot > tr > th,
					.table > thead > tr > td,
					.table > tbody > tr > td,
					.table > tfoot > tr > td {
					  padding: 8px;
					  line-height: 1.42857143;
					  vertical-align: top;
					  border-top: 1px solid #ddd;
					}
					.table > thead > tr > th {
					  vertical-align: bottom;
					  border-bottom: 2px solid #ddd;
					}
					.table > caption + thead > tr:first-child > th,
					.table > colgroup + thead > tr:first-child > th,
					.table > thead:first-child > tr:first-child > th,
					.table > caption + thead > tr:first-child > td,
					.table > colgroup + thead > tr:first-child > td,
					.table > thead:first-child > tr:first-child > td {
					  border-top: 0;
					}
					.table > tbody + tbody {
					  border-top: 2px solid #ddd;
					}
					.table .table {
					  background-color: #fff;
					}
					.table-condensed > thead > tr > th,
					.table-condensed > tbody > tr > th,
					.table-condensed > tfoot > tr > th,
					.table-condensed > thead > tr > td,
					.table-condensed > tbody > tr > td,
					.table-condensed > tfoot > tr > td {
					  padding: 5px;
					}
					.table-bordered {
					  border: 1px solid #ddd;
					}
					.table-bordered > thead > tr > th,
					.table-bordered > tbody > tr > th,
					.table-bordered > tfoot > tr > th,
					.table-bordered > thead > tr > td,
					.table-bordered > tbody > tr > td,
					.table-bordered > tfoot > tr > td {
					  border: 1px solid #ddd;
					}
					.table-bordered > thead > tr > th,
					.table-bordered > thead > tr > td {
					  border-bottom-width: 2px;
					}
					.table-striped > tbody > tr:nth-of-type(odd) {
					  background-color: #f9f9f9;
					}
					.table-hover > tbody > tr:hover {
					  background-color: #f5f5f5;
					}
					table col[class*='col-'] {
					  position: static;
					  display: table-column;
					  float: none;
					}
					table td[class*='col-'],
					table th[class*='col-'] {
					  position: static;
					  display: table-cell;
					  float: none;
					}
					.table > thead > tr > td.active,
					.table > tbody > tr > td.active,
					.table > tfoot > tr > td.active,
					.table > thead > tr > th.active,
					.table > tbody > tr > th.active,
					.table > tfoot > tr > th.active,
					.table > thead > tr.active > td,
					.table > tbody > tr.active > td,
					.table > tfoot > tr.active > td,
					.table > thead > tr.active > th,
					.table > tbody > tr.active > th,
					.table > tfoot > tr.active > th {
					  background-color: #f5f5f5;
					}
					.table-hover > tbody > tr > td.active:hover,
					.table-hover > tbody > tr > th.active:hover,
					.table-hover > tbody > tr.active:hover > td,
					.table-hover > tbody > tr:hover > .active,
					.table-hover > tbody > tr.active:hover > th {
					  background-color: #e8e8e8;
					}
					.table > thead > tr > td.success,
					.table > tbody > tr > td.success,
					.table > tfoot > tr > td.success,
					.table > thead > tr > th.success,
					.table > tbody > tr > th.success,
					.table > tfoot > tr > th.success,
					.table > thead > tr.success > td,
					.table > tbody > tr.success > td,
					.table > tfoot > tr.success > td,
					.table > thead > tr.success > th,
					.table > tbody > tr.success > th,
					.table > tfoot > tr.success > th {
					  background-color: #dff0d8;
					}
					.table-hover > tbody > tr > td.success:hover,
					.table-hover > tbody > tr > th.success:hover,
					.table-hover > tbody > tr.success:hover > td,
					.table-hover > tbody > tr:hover > .success,
					.table-hover > tbody > tr.success:hover > th {
					  background-color: #d0e9c6;
					}
					.table > thead > tr > td.info,
					.table > tbody > tr > td.info,
					.table > tfoot > tr > td.info,
					.table > thead > tr > th.info,
					.table > tbody > tr > th.info,
					.table > tfoot > tr > th.info,
					.table > thead > tr.info > td,
					.table > tbody > tr.info > td,
					.table > tfoot > tr.info > td,
					.table > thead > tr.info > th,
					.table > tbody > tr.info > th,
					.table > tfoot > tr.info > th {
					  background-color: #d9edf7;
					}
					.table-hover > tbody > tr > td.info:hover,
					.table-hover > tbody > tr > th.info:hover,
					.table-hover > tbody > tr.info:hover > td,
					.table-hover > tbody > tr:hover > .info,
					.table-hover > tbody > tr.info:hover > th {
					  background-color: #c4e3f3;
					}
					.table > thead > tr > td.warning,
					.table > tbody > tr > td.warning,
					.table > tfoot > tr > td.warning,
					.table > thead > tr > th.warning,
					.table > tbody > tr > th.warning,
					.table > tfoot > tr > th.warning,
					.table > thead > tr.warning > td,
					.table > tbody > tr.warning > td,
					.table > tfoot > tr.warning > td,
					.table > thead > tr.warning > th,
					.table > tbody > tr.warning > th,
					.table > tfoot > tr.warning > th {
					  background-color: #fcf8e3;
					}
					.table-hover > tbody > tr > td.warning:hover,
					.table-hover > tbody > tr > th.warning:hover,
					.table-hover > tbody > tr.warning:hover > td,
					.table-hover > tbody > tr:hover > .warning,
					.table-hover > tbody > tr.warning:hover > th {
					  background-color: #faf2cc;
					}
					.table > thead > tr > td.danger,
					.table > tbody > tr > td.danger,
					.table > tfoot > tr > td.danger,
					.table > thead > tr > th.danger,
					.table > tbody > tr > th.danger,
					.table > tfoot > tr > th.danger,
					.table > thead > tr.danger > td,
					.table > tbody > tr.danger > td,
					.table > tfoot > tr.danger > td,
					.table > thead > tr.danger > th,
					.table > tbody > tr.danger > th,
					.table > tfoot > tr.danger > th {
					  background-color: #f2dede;
					}
					.table-hover > tbody > tr > td.danger:hover,
					.table-hover > tbody > tr > th.danger:hover,
					.table-hover > tbody > tr.danger:hover > td,
					.table-hover > tbody > tr:hover > .danger,
					.table-hover > tbody > tr.danger:hover > th {
					  background-color: #ebcccc;
					}
					.table-responsive {
					  min-height: .01%;
					  overflow-x: auto;
					}
					@media screen and (max-width: 767px) {
					  .table-responsive {
						width: 100%;
						margin-bottom: 15px;
						overflow-y: hidden;
						-ms-overflow-style: -ms-autohiding-scrollbar;
						border: 1px solid #ddd;
					  }
					  .table-responsive > .table {
						margin-bottom: 0;
					  }
					  .table-responsive > .table > thead > tr > th,
					  .table-responsive > .table > tbody > tr > th,
					  .table-responsive > .table > tfoot > tr > th,
					  .table-responsive > .table > thead > tr > td,
					  .table-responsive > .table > tbody > tr > td,
					  .table-responsive > .table > tfoot > tr > td {
						white-space: nowrap;
					  }
					  .table-responsive > .table-bordered {
						border: 0;
					  }
					  .table-responsive > .table-bordered > thead > tr > th:first-child,
					  .table-responsive > .table-bordered > tbody > tr > th:first-child,
					  .table-responsive > .table-bordered > tfoot > tr > th:first-child,
					  .table-responsive > .table-bordered > thead > tr > td:first-child,
					  .table-responsive > .table-bordered > tbody > tr > td:first-child,
					  .table-responsive > .table-bordered > tfoot > tr > td:first-child {
						border-left: 0;
					  }
					  .table-responsive > .table-bordered > thead > tr > th:last-child,
					  .table-responsive > .table-bordered > tbody > tr > th:last-child,
					  .table-responsive > .table-bordered > tfoot > tr > th:last-child,
					  .table-responsive > .table-bordered > thead > tr > td:last-child,
					  .table-responsive > .table-bordered > tbody > tr > td:last-child,
					  .table-responsive > .table-bordered > tfoot > tr > td:last-child {
						border-right: 0;
					  }
					  .table-responsive > .table-bordered > tbody > tr:last-child > th,
					  .table-responsive > .table-bordered > tfoot > tr:last-child > th,
					  .table-responsive > .table-bordered > tbody > tr:last-child > td,
					  .table-responsive > .table-bordered > tfoot > tr:last-child > td {
						border-bottom: 0;
					  }
					}
					@page {
						counter-reset: page;
					}
					.header { position: fixed; top: 0px; height:40px;}
					.footer { position: fixed; bottom: 0px;}
					.pagenum:before { content: 'Page: ' counter(page); }

				</style>
			</head>
			<body>
				<div class='header'>
					<table >
						<tr>
							<td colspan='2' style='border:0;'><p><img src='img/logo.png' style='height:40px;'></p></td>
						</tr>
					</table>
				</div>
				<div class='footer'>
					<table class='table'>
						<tr>
							<td>". config('global.company_legal') ."  ". config('global.company_url') ."</td>
							<td class='text-right' ><span class='pagenum'></span></td>
						</tr>
					</table>
				</div>
				<table class='table'>";

        if ($mstatement==1) {
            $str.=$monthly_statement;
        }

        $str.="</table>
				<h3> ".$title." </h3>
				<table class='table table-sm table-bordered'>
					<tr>";
        $str .= '<th>#</th>';
        foreach ($headers as $header) {
            $str .= '<th>'.$header.'</th>';
        }
        $str .="</tr>";

        if ($debit==1) {
            $str.=$this->debitcredit($data, $fields);
        } else {
            $i=1;
            foreach ($data as $key => $value) {
                $str .= '<tr>';
                $str .= '<td>'.$i++.'</td>';
                foreach ($fields as $field) {
                    $str .= '<td>'.$value[$field].'</td>';
                }
                $str .= '.</tr>';
            }
        }

        $str .= "</table></body>
		</html>";

        $pdf = PDF::loadHTML($str)->setPaper('a4', $landscape);
        $pdf_name = $pdf_name.'.pdf';
        return $pdf->download($pdf_name);
    }

    public function debitcredit($data, $fields)
    {
        $where_in_credit = ['Purchase', 'Capture', 'Dispute', 'TokenizerPurchase'];

        $str="";
        $i=1;
        foreach ($data as $key => $value) {
            $str .= '<tr>';
            $str .= '<td>'.$i++.'</td>';
            foreach ($fields as $field) {
                if ($field=='amount') {
                    $debit=$credit="";
                    if (in_array($value['transaction_type'], $where_in_credit)) {
                        $credit=$value[$field];
                    } else {
                        $debit=$value[$field];
                    }

                    $str .= '<td>'.$credit.'</td><td>'.$debit.'</td>';
                } else {
                    $str .= '<td>'.$value[$field].'</td>';
                }
            }
            $str .= '.</tr>';
        }
        return $str;
    }

    public function debitcreditCSV($data, $fields)
    {
        $where_in_credit = ['Purchase', 'Capture', 'Dispute', 'TokenizerPurchase'];
        $str="";
        foreach ($data as $key => $value) {
            foreach ($fields as $field) {
                if ($field=='amount') {
                    $debit=$credit="";
                    if (in_array($value['transaction_type'], $where_in_credit)) {
                        $credit=$value[$field];
                    } else {
                        $debit=$value[$field];
                    }
                    $str .= $credit.','.$debit.',';
                } else {
                    $str .= $value[$field].',';
                }
            }
            $str .= "\r\n";
        }
        return $str;
    }

    public function dataToCSV($data, $fields, $csv_name, $headers, $debit=0)
    {
        $str = "";
        foreach ($headers as $header) {
            $str .= $header.',';
        }
        $str .="\r\n";
        if ($debit==0) {
            foreach ($data as $key => $value) {
                foreach ($fields as $field) {
                    $str .= $value[$field].',';
                }
                $str .= "\r\n";
            }
        } else {
            $str .=$this->debitcreditCSV($data, $fields);
        }
        $csv_name = $csv_name.'.csv';

        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="'.$csv_name.'"');
        header('Pragma: no-cache');
        echo $str;
    }

    public function bin_store($field, $value, $user_id=0)
    {
        $inser['field'] = $field;
        $inser['value'] = $value;
        $inser['date'] = date('Y-m-d');
        $inser['user_id'] = $user_id;

        DB::insert($inser);
    }

    public function crypto_encrypt($value, $user_type)
    {
        $main_key = DB::table('main_key')->where('status', '=', 'Active')->orderBy('id', 'DESC')->first();
        $crypto_key = DB::table('crypto_key')->where('status', '=', 'Active')->orderBy('id', 'DESC')->first();
        $crypto = new EncryptDecrypt($main_key->main_key);

        $res = $crypto->encrypt($value, $crypto_key->$user_type);

        return $res;
    }


    public function verification_code()
    {
        return $this->uniqidReal(40);
    }
    /*

    public function verification_code($verifyuser)
    {
        $code = $this->uniqidReal(32);
        $verifyuser_has = $verifyuser::where('verify_token', $code)->count();
        while ($verifyuser_has) {
            $code = $this->uniqidReal(32);
            $verifyuser_has = $verifyuser::where('verify_token', $code)->count();
        }
        return $code;
    }

     */



    public function flash_message($type, $message)
    {
        Session::flash('alert-class', 'alert-'.$type);
        Session::flash('message', $message);
    }

    public function password_expired($user_type, $user_id)
    {
        $request = new Request;
        $field = $user_type.'_id';
        $table = $user_type.'s';
        if ($user_type == 'account') {
            $table = $user_type.'_users';
            $db = DB::table($table)->where('id', $user_id)->first();
            if (is_null($db->password_date) || $db->password_date == '') {
                DB::table($table)->where('id', $user_id)->update(['password_date' => date('Y-m-d')]);
                return 0;
            }
        } else {
            $db = DB::table($table)->where($field, $user_id)->first();
            if (is_null($db->password_date) || $db->password_date == '') {
                DB::table($table)->where($field, $user_id)->update(['password_date' => date('Y-m-d')]);
                return 0;
            }
        }



        $prv_date = date_create($db->password_date);
        $now_date = date_create(date('Y-m-d'));
        $diff=date_diff($prv_date, $now_date, 1);
        $res = $diff->format("%a");
        if ($res > 90) {
            return 1;
        } else {
            return 0;
        }
    }

    public function getRealIpAddr()
    {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip=$_SERVER['HTTP_CLIENT_IP']; // share internet
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip=$_SERVER['HTTP_X_FORWARDED_FOR']; // pass from proxy
        } else {
            $ip=$_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    /**
    ***return unique ID
    **/

    public function uniqidReal($length = 32)
    {
        // uniqid gives 32 chars, but you could adjust it to your needs.
        if (function_exists("random_bytes")) {
            $bytes = random_bytes(ceil($length / 2));
        } elseif (function_exists("openssl_random_pseudo_bytes")) {
            $bytes = openssl_random_pseudo_bytes(ceil($length / 2));
        } else {
            throw new Exception("no cryptographically secure random function available");
        }
        return substr(bin2hex($bytes), 0, $length);
    }
}
