<?php

namespace App\Http\Helper;

use View;
use Session;
use App\Models\Metas;
use App\Permission;
use App\Role_user;
use App\Permission_role;
class RolePermission
{
	//this function checks the permission level of the user
	public static function has_permission($user_id, $permissions = '')
	{
		$permissions = explode('|', $permissions);
		$user_permissions = Permission::whereIn('permission_name', $permissions)->get();
		$permission_id = [];
		$i = 0;
		foreach ($user_permissions as $value) {
			$permission_id[$i++] = $value->id;
		}
		$role = Role_user::where('user_id', $user_id)->first();

		if(count($permission_id) && isset($role->role_id)){
			$has_permit = Permission_role::where('role_id', $role->role_id)->whereIn('permission_id', $permission_id);
			return $has_permit->count();
		}
		else return 0;
	}

}
