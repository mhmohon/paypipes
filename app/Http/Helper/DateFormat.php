<?php

namespace App\Http\Helper;

use Carbon\Carbon;

class DateFormat
{
    public static function dateFormatMD($date, $merchant)
    {
        return '<span class="font-sm uppercase">'.Carbon::parse($date)->setTimezone($merchant->timezone)->format('M').'</span> </br> <span style="font-size:22px;font-weight:300;">'.Carbon::parse($date)->setTimezone($merchant->timezone)->format('d').'</span>';
    }

    public static function dateFormatAT($payment, $merchant)
    {
        return Carbon::parse($payment->created_at)->setTimezone($merchant->timezone)->format('M d, Y \a\t H:i:s').' '.$merchant->timezones->offset;
    }

    public static function dateFormatApi($date, $merchant)
    {
        return Carbon::parse($date)->setTimezone($merchant->timezone)->format('M d, Y - H:i:s').' '.$merchant->timezones->offset;
    }

    public static function dateFormatForHumans($note, $merchant)
    {
        return Carbon::parse($note->created_at)->setTimezone($merchant->timezone)->diffForHumans();
        //return $note->created_at->timezone($merchant->timezone)->diffForHumans();
    }

    public static function dateFormatMDY($statement, $merchant)
    {
        return Carbon::parse($statement->created_at)->setTimezone($merchant->timezone)->format('M d, Y').' </br> '.Carbon::parse($statement->created_at)->setTimezone($merchant->timezone)->format('H:i:s').' </br> '.$merchant->timezones->offset;
    }
}
