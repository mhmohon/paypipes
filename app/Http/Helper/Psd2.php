<?php

namespace App\Http\Helper;

use DB;
use Session;
use Auth;
use DateTime;
use Illuminate\Http\Request;
use Carbon\Carbon;


class Psd2
{
    public $response;
    public $request = array();
    public $curl;
	
	public function httpHeader($curlparam,$content_type='json')
    {
	 return array(	"Content-type: application/$content_type;charset=UTF-8",
					"Accept: application/xml",
					"Connection: Keep-Alive",
					"Cache-Control: no-cache",												
					"Pragma: no-cache",												
					"User-Agent: ".$_SERVER['HTTP_USER_AGENT']);
														
	}	
	
	public function doRequest($url)
    {
        $curl = curl_init($url);
       curl_setopt($curl, CURLOPT_FAILONERROR, true); 
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true); 
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); 
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false); 
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);  
        $output = curl_exec($curl);
        curl_close($curl);
        return $output;
        // reset URL do default
        //$this->setEnvironment();
    }
	
	/**
     * check request status
     * @return bool
     */
    public function requestIsOk()
    {
        if (($this->curl["error_nr"] == 0) && ($this->curl["http_status"] < 300)) {
            return true;
        } else {
            return false;
        }
    }
	

}
