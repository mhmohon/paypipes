<?php

namespace App\Http\Controllers\AccountAuth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;
use App\PasswordResets;
use Illuminate\Http\Request;
use App\Entities\AccountUser;
use Mail;
use App\Http\Helper\Common;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $common;
    public function __construct()
    {
        $this->middleware('guest');
        $this->common = new Common;
    }

    /**
     * Display the form to request a password reset link.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLinkRequestForm()
    {
        return view('account.auth.passwords.email');
    }

    public function sendResetLinkEmail(Request $request)
    {
        //$email = $request->email;
        //$data['first_name'] = $user->first_name;
        $user = AccountUser::where('email', $request->email);

        if ($user->count()) {
            PasswordResets::where('email', $request->email)->delete();
            $user = $user->first();
            $token = str_random(100); // Generate random string values - limit 100
            $chk = PasswordResets::where('token', $token)->count();
            while ($chk) {
                $token = str_random(100);
                $chk = PasswordResets::where('token', $token)->count();
            }

            $password_resets = new PasswordResets;
            $password_resets->email      = $user->email;
            $password_resets->token      = $token;
            $password_resets->created_at = date('Y-m-d H:i:s');
            $password_resets->save();

            $data['subject'] = config('global.company').' - Reset Password';
            $data['preview_text'] = 'Forgot your Password?';
            $data['main_message'] = 'Forgot your Password?';
            $data['p1'] = 'Lets get you a new one.';
            $data['button_link'] = url('account/password/reset/'.$token);
            $data['button'] = 'Reset Your Password';
            $data['note'] = 'If you didnt request a password reset, you can safely ignore this email. Only a person with access to your email can reset your account password.';
            $data['email'] = $user->email;
            $data['first_name'] = $user->first_name;
            @Mail::send('emails.app_email', $data, function ($message) use ($data) {
                $message->to($data['email'], $data['first_name'])->subject(config('global.company').' - Reset Password');
            });
            $this->common->flash_message('success', 'Reset password link send to your email.');
            return redirect('account/password/reset');
        } else {
            $this->common->flash_message('danger', 'Invalid Email Address.');
            return redirect('account/password/reset');
        }
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function broker()
    {
        return Password::broker('account_users');
    }
}
