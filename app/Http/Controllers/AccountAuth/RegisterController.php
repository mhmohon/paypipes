<?php

namespace App\Http\Controllers\AccountAuth;

use DB;
use Mail;
use Validator;
use App\Account;
use App\Country;
use App\Activity;
use App\Notification;
use App\Http\Helper\Common;
use Illuminate\Http\Request;
use App\Entities\AccountUser;
use App\Libraries\EncryptDecrypt;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Events\UserRegisterEvent;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/account/home';
    protected $common;
    protected $wallet_type_id = '1011';
    protected $wallet_fee_id = '1012';
    protected $wallet_limit_id = '1012';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->common = new Common;
        $this->encdec = new EncryptDecrypt('paypies');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:account_users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return Account
     */
    protected function create(array $data)
    {
        return AccountUser::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $countries = [''=>'Select a Country'] + Country::pluck('country_name', 'country_code')->all();
        return view('account.auth.register', compact('countries'));
    }

    public function register(Request $request)
    {
        $rules = [
            'email'           => 'required|max:255|email|unique:account_users',
            'password'        => 'required|confirmed|regex:/^.*(?=.{3,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$/|between:8,16',
            'terms'           => 'required'
        ];

        $niceNames = [
            'email'           => 'Email',
            'password'        => 'Password',
            'terms'           => 'Terms & Conditions'
        ];
        $customMessages = [
            "password.between" => "The password must contain at least 1 capital letter, 1 small letter and 1 number. The password must be 8 to 16 characters long.",
            "password.regex" => "The password must contain at least 1 capital letter, 1 small letter and 1 number. The password must be 8 to 16 characters long.",
            'password.required' => 'The password must contain at least 1 capital letter, 1 small letter and 1 number. The password must be 8 to 16 characters long.',
        ];
        $validator = Validator::make($request->all(), $rules, $customMessages);
        $validator->setAttributeNames($niceNames);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
        } else {
            $password = bcrypt($request->password);

            $account_id = AccountUser::accountid();

            $account = new Account($request->all());
            $account->account_id  =   $account_id;
            $account->wallet_type_id   = $this->wallet_type_id;
            $account->status       =   'Active';
            $account->compliance   =   'Pending';
            $account->added_by     =   'user';
            $account->save();

            $code = $this->common->verification_code();

            $user = new AccountUser($request->all());
            $user->password     =   $password;
            $user->account_id   =   $account_id;
            $user->is_primary   =   '1';
            $user->two_step     =   'email';
            $user->status       =   'Inactive';
            $user->added_by     =   'user';
            $user->verify_token =    $code;
            $user->token_time   =   time();
            $user->save();


            DB::table('bin')->insert(['user_id' => $account_id, 'field' => 'password', 'value' => $password, 'user_type' => 'account', 'date' => date('Y-m-d')]);


            $notification = new Notification($request->all());
            $notification_id = Notification::notificationid();
            $notification['account_id'] = $account_id;
            $notification['notification_id'] = $notification_id;
            $notification['added_by'] = 'user';
            $notification->save();


            $activity = new Activity();
            $activity['added_by'] = $request->name;
            $activity['interface'] = 'Account';
            $activity['module'] = 'Account';
            $activity['activity'] = 'New Account '. $request->name .' Registered';
            $activity->save();

            

            $this->common->flash_message('success', 'Registered! Check Your Email To Activate Your Account');
            return redirect('account/login');
        }
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('account');
    }
}
