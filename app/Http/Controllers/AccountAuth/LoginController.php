<?php

namespace App\Http\Controllers\AccountAuth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use App\Entities\AccountUser;
use App\Http\Helper\Common;
use Illuminate\Http\Request;
use App\Libraries\EncryptDecrypt;
use Session;
use Mail;
use Validator;
use App\PasswordResets;
use DateTime;
use DB;
use App\Events\ConfirmNewAccount;
use App\Events\SendTwoFactorCode;
use App\Events\ResendVerificationCode;
use App\Traits\auth\CustomAuthenticatesUser;
use App\Traits\auth\CustomTwoStepAuthenticates;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use CustomAuthenticatesUser, CustomTwoStepAuthenticates;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    public $redirectTo = '/account';
    public $redirectToLogin = '/account/login';
    public $userToRedirect = 'account';

    protected $common;
    protected $encdec;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
        $this->common = new Common;
        $this->encdec = new EncryptDecrypt('paypies');
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('account.auth.login');
    }

    public function logout()
    {
        Auth::guard('account')->logout();
        return redirect('account/login');
    }



    public function login(Request $request)
    {

        $account = AccountUser::where('email', $request->email)->first();

        if(!empty($account)){
            
            return $this->checkUserLogin($request, $account, $account->account_id);

        }else {

            $this->common->flash_message('danger', 'Log in Failed. Please Check Your Email/Password');
            return redirect()->back();
        }
    }

    public function twoStepInfoSend(Request $request)
    {
        $account_id = Session::get('uid');
        $account = AccountUser::where('account_id', '=', $account_id)->first();

        if(!empty($account)){

            return $this->sendTwoStepCode($account);

        }else {
            abort(403);
        }

    }

    public function twoStepView()
    {
        if (!Session::has('uid')) {
            abort(403);
        }
        return view('account.auth.two_step');
    }

    public function twoStep(Request $request)
    {
        $account_id = Session::get('uid');
        $account = AccountUser::where('account_id', '=', $account_id)->first();

        if ($this->fieldValidation($request) !== true) {

            return back()->withErrors($this->fieldValidation($request))->withInput(); // Form calling with Errors and Input values

        } else {

            return $this->checkTwoFactorCode($request, $account);
            
        }
        
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('account');
    }

    public function email_verify(Request $request)
    {
        $code = $request->code;
        $account_has = AccountUser::where('verify_token', $code);

        if ($account_has->count()) {
            $account = $account_has->first();
            $valid_time = $account->token_time+900;
            $now = time();
            $token = str_random(100); // Generate random string values - limit 100

            if ($valid_time >= $now) {
                if ($account->added_by != 'user') {
                    //$pass = new EncryptDecrypt('paypipes');
                    //$password  = $pass->makeRandomPassword(8);
                    //$account->password = $this->common->crypto_encrypt($password, 'key_account');
                    $account->status         = 'Active';
                    $account->verify_token   = '';
                    $account->token_time     = 0;
                    $account->save();


                    $password_resets = new PasswordResets;
                    $password_resets->email      = $account->email;
                    $password_resets->token      = $token;
                    $password_resets->created_at = date('Y-m-d H:i:s');
                    $password_resets->save();

                    //Send Welcome Email With password reset token
                    event(new ConfirmNewAccount($account, $token));

                    $this->common->flash_message('success', 'Email successfully verified. Please Setup your password.');
                    return redirect('account/password/setup/'.$token);

                } else {
                    $account->status         = 'Active';
                    $account->verify_token   = '';
                    $account->token_time     = 0;
                    $account->save();

                    //Send Welcome Email
                    event(new ConfirmNewAccount($account, $token));

                    $this->common->flash_message('success', 'Email successfully verified you can now log in with your information');
                    return redirect('account/login');
                }
            } else {
                $code = $this->common->verification_code();
                $t_account = $account_has->first();
                $t_account->verify_token = $code;
                $t_account->token_time = time();
                $t_account->save();
                $data['message'] = 'Your verification code got expired you can resend it with below button';
                $data['resend_link'] = url('account/resend-verify-email/'.$code);
                return view('account.auth.resend_verify_email', $data);
            }
        } else {
            $this->common->flash_message('danger', 'Invalid Code.');
            return redirect('account/login');
        }
    }

    public function setup_password(Request $request)
    {
        if (!$_POST) {
            $password_resets = PasswordResets::whereToken($request->token);

            if ($password_resets->count()) {
                $password_result = $password_resets->first();

                $datetime1 = new DateTime();
                $datetime2 = new DateTime($password_result->created_at);
                $interval  = $datetime1->diff($datetime2);
                $hours     = $interval->format('%h');

                if ($hours >= 24) {
                    // Delete used token from password_resets table
                    $password_resets->delete();

                    $this->common->flash_message('danger', 'Your token got expired'); // Call flash message function
                    return redirect('account/login');
                }

                $data['result'] = AccountUser::whereEmail($password_result->email)->first();
                $data['token']  = $request->token;

                return view('account.auth.setup_password', $data);
            } else {
                $this->common->flash_message('danger', 'Invalid Request');
                return redirect('account/login');
            }
        } else {
            // Password validation rules
            $rules = [
            'password'              => 'required|confirmed|regex:/^.*(?=.{3,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$/|between:8,16',
            'password_confirmation' => 'required'
            ];

            // Password validation custom Fields name
            $niceNames = [
            'password'              => 'New Password',
            'password_confirmation' => 'Confirm Password'
            ];

            $customMessages = [
                "password.between" => "The password must contain at least 1 capital letter, 1 small letter and 1 number. The password must be 8 to 16 characters long.",
                "password.regex" => "The password must contain at least 1 capital letter, 1 small letter and 1 number. The password must be 8 to 16 characters long.",
                'password.required' => 'The password must contain at least 1 capital letter, 1 small letter and 1 number. The password must be 8 to 16 characters long.',
            ];

            $validator = Validator::make($request->all(), $rules, $customMessages);
            $validator->setAttributeNames($niceNames);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
            } else {
                // Delete used token from password_resets table
                $password_resets = PasswordResets::whereToken($request->token)->first();

                $user = AccountUser::where('email', $password_resets->email)->first();

                $user->password = $this->common->crypto_encrypt($request->password, 'key_account');

                $user->save(); // Update Password in users table
                DB::table('bin')->insert(['user_id' => $user->account_id, 'field' => 'password', 'value' => $user->password, 'user_type' => 'account', 'date' => date('Y-m-d')]);

                PasswordResets::whereToken($request->token)->delete();
                $this->common->flash_message('success', 'Password successfully set. You can now login'); // Call flash message function
                return redirect('account/login');
            }
        }
    }

    public function resend_verification_code(Request $request)
    {
        $code = $request->code;
        $account_has = AccountUser::where('verify_token', $code);

        if ($account_has->count()) {
            $code = $this->common->verification_code();
            $t_account =  $account_has->first();
            $t_account->verify_token = $code;
            $t_account->token_time = time();
            $t_account->save();

            //Send email with verification code
            event(new ResendVerificationCode($t_account));


            $this->common->flash_message('success', 'Verification email sent.');
            return redirect('account/login');
        } else {
            $this->common->flash_message('danger', 'Invalid Code.');
            return redirect('account/login');
        }
    }
}
