<?php
namespace App\Http\Controllers\AccountAuth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use Illuminate\Http\Request;
use App\Entities\AccountUser;
use App\Bin;
use App\Http\Helper\Common;
use Validator;
use DB;
use Session;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    public $redirectTo = '/account';
    protected $common;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->common = new Common;
    }

    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $token
     * @return \Illuminate\Http\Response
     */
    public function showResetForm(Request $request, $token = null)
    {
        //Session::flush();
        $token_test = DB::table('password_resets')->where('token', $request->token)
                    ->count();
        if ($token_test) {
            return view('account.auth.passwords.reset')->with(
            ['token' => $token, 'email' => $request->email]
        );
        } else {
            $this->common->flash_message('danger', 'Invalid Request');
            return view('account.auth.passwords.invalid');
        }
    }

    /**
    * Reset the given user's password.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\RedirectResponse
    */
    public function reset(Request $request)
    {
        $rules = [
                'token' => 'required',
                'email' => 'required|email',
                'password' => 'required|confirmed|regex:/^.*(?=.{3,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$/|between:8,16',
            ];

        $niceNames = [
                'token' => 'Token',
                'email' => 'Email',
                'password' => 'Password',
            ];

        $customMessages = [
            "password.between" => "The password must contain at least 1 capital letter, 1 small letter and 1 number. The password must be 8 to 16 characters long.",
            "password.regex" => "The password must contain at least 1 capital letter, 1 small letter and 1 number. The password must be 8 to 16 characters long.",
            'password.required' => 'The password must contain at least 1 capital letter, 1 small letter and 1 number. The password must be 8 to 16 characters long.',
        ];

        $validator = Validator::make($request->all(), $rules, $customMessages);
        $validator->setAttributeNames($niceNames);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
        } else {
            $token_test = DB::table('password_resets')->where('email', $request->email)
                    ->where('token', $request->token)
                    ->count();
            $account = AccountUser::where('email', $request->email)->first();

            if ($token_test && isset($account->account_id)) {
                $prv_pass = Bin::where(['user_id' => $account->account_id, 'user_type'=> 'account', 'field'=>'password'])->orderBy('id', 'desc')->take(5)->pluck('value')->toArray();

                $password = $this->common->crypto_encrypt($request->password, 'key_account');
                if (!in_array($password, $prv_pass)) {
                    $account->password = $this->common->crypto_encrypt($request->password, 'key_account');
                    if (@$account->status == 'TempLock') {
                        $account->status = 'Active';
                    }
                    $account->bad_login = 0;
                    $account->password_date = date('Y-m-d');
                    $account->save();
                    DB::table('password_resets')->where('email', $request->email)->where('token', $request->token)->delete();
                    DB::table('bin')->insert(['user_id' => $account->account_id, 'field' => 'password', 'value' => $password, 'user_type' => 'account', 'date' => date('Y-m-d')]);

                    $this->common->flash_message('success', 'Password changed successfully');
                    return redirect('account/login');
                } else {
                    $this->common->flash_message('danger', 'Password used previously');
                    return redirect('account/password/reset/'.$request->token);
                }
            } else {
                $this->common->flash_message('danger', 'Invalid Request');
                return redirect('account/password/reset/'.$request->token);
            }
        }
    }

    public function changeView()
    {
        if (!Session::has('uid')) {
            abort(403);
        }
        return view('account.auth.passwords.change');
    }

    /**
    * Reset the given user's password.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\RedirectResponse
    */
    public function change(Request $request)
    {
        $account_id = Session::get('uid');
        $account = AccountUser::where('account_id', '=', $account_id)->first();

        $rules = [
            'old_password' => 'required|between:6,16',
            'password' => 'required|regex:/^.*(?=.{3,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$/|between:8,16|confirmed'
        ];

        $niceNames = [
            'old_password' => 'Old Password',
            'password' => 'Password',
        ];

        $customMessages = [
            "password.between" => "The password must contain at least 1 capital letter, 1 small letter and 1 number. The password must be 8 to 16 characters long.",
            "password.regex" => "The password must contain at least 1 capital letter, 1 small letter and 1 number. The password must be 8 to 16 characters long.",
            'password.required' => 'The password must contain at least 1 capital letter, 1 small letter and 1 number. The password must be 8 to 16 characters long.',
        ];

        $validator = Validator::make($request->all(), $rules, $customMessages);
        $validator->setAttributeNames($niceNames);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
        } else {
            $old_password = $this->common->crypto_encrypt($request->old_password, 'key_account');
            if (trim($old_password) != trim($account->password)) {
                $this->common->flash_message('danger', '<b>Failed!</b> Incorrect old password');
                return redirect('account/password/change');
            } else {
                $prv_pass = Bin::where(['user_id' => $account_id, 'user_type'=> 'account', 'field'=>'password'])->orderBy('id', 'desc')->take(5)->pluck('value')->toArray();

                $password = $this->common->crypto_encrypt($request->password, 'key_account');
                if (!in_array($password, $prv_pass)) {
                    $customer = AccountUser::where('account_id', '=', $account_id)
                    ->update(['password' => $password, 'bad_login' => 0 , 'password_date' => date('Y-m-d')]);

                    Session::flush();

                    DB::table('bin')->insert(['user_id' => $account_id, 'field' => 'password', 'value' => $password, 'user_type' => 'account', 'date' => date('Y-m-d')]);

                    $this->common->flash_message('success', 'Password changed successfully');
                    return redirect('account/login');
                } else {
                    $this->common->flash_message('danger', 'Password used previously');
                    return redirect('account/password/change');
                }
            }
        }
    }
    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function broker()
    {
        return Password::broker('account_users');
    }

    /**
     * Get the guard to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('account');
    }
}
