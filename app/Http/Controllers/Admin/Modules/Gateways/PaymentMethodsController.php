<?php
namespace App\Http\Controllers\Admin\Modules\Gateways;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

use DB;
use Validator;
use Auth;
use App\Payment_method;
use App\Activity;
use App\Http\Controllers\Controller;

class PaymentMethodsController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $payment_methods = DB::table('payment_methods')->orderBy('created_at', 'asc')->get();

        $pagetitle = 'Payment Methods';
        $breadcrumb_level1 = '<a href="/admin/paymentmethods">Payment Methods</a>';

        return view('admin/modules/gateways/paymentmethods', compact('payment_methods', 'pagetitle', 'breadcrumb_level1'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $payment_methods = DB::table('payment_methods')->orderBy('created_at', 'asc')->get();

        $pagetitle = 'Gateways';
        $breadcrumb_level1 = '<a href="/admin/paymentmethods">Gateways</a>';

        return view('admin/modules/gateways/paymentmethods', compact('payment_methods', 'pagetitle', 'breadcrumb_level1'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // getting all of the post data
        $file = ['upload_file' => Input::file('upload_file')];

        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'short_code' => 'required|max:8',
            'upload_file' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#add")
                ->withErrors($validator)
                ->withInput();
        }

        if ($request->hasFile('upload_file')) {
            if (Input::file('upload_file')->isValid()) {
                $destinationPath = 'uploads/paymentmethods';
                $extension = Input::file('upload_file')->getClientOriginalExtension();
                $fileName = strtolower($request->name).'.'.$extension;
                Input::file('upload_file')->move($destinationPath, $fileName);
            }
        } else {
            $fileName = "";
            $destinationPath ="";
        }


        $payment_method = new Payment_method($request->all());

        $payment_method_id=(int)$payment_method_id=DB::table('payment_methods')->orderBy('payment_method_id', 'desc')->value('payment_method_id');
        if ($payment_method_id < 1) {
            $payment_method_id=1010;
        } else {
            $payment_method_id = $payment_method_id + 1;
        }
        $payment_method['name'] = strtolower($request->name);
        $payment_method['payment_method_id'] = $payment_method_id;
        $payment_method['file_name'] = $fileName;
        $payment_method['file_path'] = '/'.$destinationPath.'/'.$fileName;
        $payment_method['short_code'] = $request->short_code;
        $payment_method['status'] = 'Inactive';
        $payment_method['base_url'] = $request->base_url??'';
        $payment_method['description'] = $request->description??'';
        $payment_method['added_by'] = Auth::guard('admin')->user()->name;
        $payment_method->save();

        $activity = new Activity();
        $activity['admin_id'] = Auth::guard('admin')->user()->id;
        $activity['added_by'] = Auth::guard('admin')->user()->name;
        $activity['interface'] = 'Admin';
        $activity['module'] = 'Gateway';
        $activity['activity'] = 'New Payment Method '. $payment_method->name .' has been added by '. Auth::guard('admin')->user()->name .'';
        $activity->save();

        session()->flash('toastr_alert', '<b>Added!</b> New Payment Method '. $payment_method->name .' has been added by '. Auth::guard('admin')->user()->name .'');
        session()->flash('success', true);

        return Redirect::to(URL::previous());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($payment_method_id)
    {
        $payment_method = DB::table('payment_methods')->where('payment_method_id', '=', $payment_method_id)->first();

        $pagetitle = $payment_method->description;
        $breadcrumb_level1 = '<a href="/admin/paymentmethods">Gateways</a>';
        $breadcrumb_level2 = '<a href="/admin/paymentmethods/'.$payment_method_id.'">'.$payment_method->description.'</a>';

        return view('admin/modules/gateways/paymentmethod', compact('payment_methods', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'payment_method_id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($payment_method_id)
    {
        $payment_method = DB::table('payment_methods')->where('payment_method_id', '=', $payment_method_id)->first();

        $pagetitle = $payment_method->description;
        $breadcrumb_level1 = '<a href="/admin/paymentmethods">Payment Methods</a>';
        $breadcrumb_level2 = '<a href="/admin/paymentmethods/'.$payment_method_id.'">'.$payment_method->description.'</a>';
        //dd($payment_method);
        return view('admin/modules/gateways/paymentmethod', compact('payment_method', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'payment_method_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $payment_method_id)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'short_code' => 'required',
            'field_description' => 'required',
            'status' => 'required',
            'purchase' => 'required',
            'authorize' => 'required',
            'description' => 'required',
            'base_url' => 'required|url',
        ]);


        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#edit")
                ->withErrors($validator)
                ->withInput();
        }

        if ($request->hasFile('upload_file')) {
            if (Input::file('upload_file')->isValid()) {
                $destinationPath = 'uploads/paymentmethods';
                $extension = Input::file('upload_file')->getClientOriginalExtension();
                $fileName = strtolower($request->name).'.'.$extension;
                $filePath = $destinationPath.'/'.$fileName;
                $this->unlinkFile($filePath);

                Input::file('upload_file')->move($destinationPath, $fileName); // uploading file to given path
                $data['file_path'] = '/'.$destinationPath.'/'.$fileName;
                $data['file_name'] = $fileName;
            }
        }





        $data['name'] = strtolower($request->name);
        $data['short_code'] = $request->short_code;
        $data['field_description'] = $request->field_description;
        $data['description'] = $request->description;
        $data['base_url'] = $request->base_url;
        $data['purchase'] = $request->purchase;
        $data['authorize'] = $request->authorize;
        $data['status'] = $request->status;

        DB::table('payment_methods')->where(['payment_method_id'=>$payment_method_id])->update($data);

        return Redirect::to(URL::previous());
    }

    private function unlinkFile($filePath = '')
    {
        if (!empty($filePath) && file_exists($filePath)) {
            unlink($filePath);
        }
    }
}
