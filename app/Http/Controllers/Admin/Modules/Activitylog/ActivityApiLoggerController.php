<?php

namespace App\Http\Controllers\Admin\Modules\Activitylog;

use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Entities\ApiActivity;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use jeremykenedy\LaravelLogger\App\Http\Traits\IpAddressDetails;
use jeremykenedy\LaravelLogger\App\Http\Traits\UserAgentDetails;

class ActivityApiLoggerController extends Controller
{
    use IpAddressDetails, UserAgentDetails;

    private $_rolesEnabled;
    private $_rolesMiddlware;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Add additional details to a collections
     *
     * @param  collection $collectionItems
     *
     * @return collection
     */
    private function mapAdditionalDetails($collectionItems)
    {
        $collectionItems->map(function ($collectionItem) {
            $eventTime = Carbon::parse($collectionItem->updated_at);
            $collectionItem['timePassed'] = $eventTime->diffForHumans();
            $collectionItem['userAgentDetails'] = UserAgentDetails::details($collectionItem->useragent);
            $collectionItem['langDetails'] = UserAgentDetails::localeLang($collectionItem->locale);
            //$userdetails= '\App\\'.ucfirst($collectionItem->userAccess);
            //$collectionItem['userDetails'] = $userdetails::find($collectionItem->userId);

            return $collectionItem;
        });

        return $collectionItems;
    }

    /**
     * Show the activities log dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $item_per_page = ROW_PER_PAGE;

        $data['item_per_page']=$item_per_page = !empty($request->rows)?$request->rows:ROW_PER_PAGE;
        $activities = ApiActivity::where(function ($query) use ($request) {
            //filter by keyword
            if (($search = $request->get('search'))) {
                $query->orWhere('transaction_id', 'like', '%' . $search . '%');
                $query->orWhere('order_id', 'like', '%' . $search . '%');
                $query->orWhere('transaction_type', 'like', '%' . $search . '%');
                $query->orWhere('description', 'like', '%' . $search . '%');
                $query->orWhere('body', 'like', '%' . $search . '%');
                $query->orWhere('status', 'like', '%' . $search . '%');
                $query->orWhere('status_code', 'like', '%' . $search . '%');
                $query->orWhere('route', 'like', '%' . $search . '%');
            }
        })->orderBy('created_at', 'desc')->paginate($item_per_page);
        $totalActivities = $activities->total();


        self::mapAdditionalDetails($activities);

        $data['activities'] = $activities;
        $data['totalActivities'] = $totalActivities;

        $data['pagetitle'] = 'ApiActivity Logger';
        $data['breadcrumb_level1'] = 'ApiActivity Logger';
        $data['item_per_page_url'] = url('admin/apiactivity');

        return view('admin/modules/activitylog/apiactivityloggers', $data);
    }

    /**
     * Show an individual apiactivity log entry.
     *
     * @return \Illuminate\Http\Response
     */
    public function view(Request $request, $id)
    {
        $apiactivity = ApiActivity::findOrFail($id);
        $userdetails= '\App\\'.ucfirst($apiactivity->userAccess);
        $userDetails = $userdetails::find($apiactivity->userId);
        $userAgentDetails = UserAgentDetails::details($apiactivity->useragent);
        $ipAddressDetails = IpAddressDetails::checkIP($apiactivity->ipAddress);
        $langDetails = UserAgentDetails::localeLang($apiactivity->locale);
        $eventTime = Carbon::parse($apiactivity->created_at);
        $timePassed = $eventTime->diffForHumans();


        $data  = [
            'activity'              => $apiactivity,
            'userDetails'           => $userDetails,
            'ipAddressDetails'      => $ipAddressDetails,
            'timePassed'            => $timePassed,
            'userAgentDetails'      => $userAgentDetails,
            'langDetails'           => $langDetails,
            'isClearedEntry'        => false,
        ];

        $data['pagetitle'] = 'Apiactivity Logger';
        $data['breadcrumb_level1'] = 'Apiactivity Logger';

        return view('admin/modules/activitylog/apiactivitylogger', $data);
    }

    /**
     * Get Cleared (Soft Deleted) Apiactivity - Helper Method.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $apiactivity = ApiActivity::find($id);
        if (count($apiactivity) === 1) {
            $apiactivity->destroy($id);
        }



        session()->flash('toastr_alert', '<b>Delete!</b> ApiActivity Logger has been deleted by '. Auth::guard('admin')->user()->name .'.');
        session()->flash('success', true);

        return Redirect::back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function destroyAllRecords(Request $request)
    {
        $activities = ApiActivity::all();

        foreach ($activities as $activity) {
            $activity->delete();
        }

        session()->flash('toastr_alert', '<b>Delete!</b> All Activity Logger has been deleted by '. Auth::guard('admin')->user()->name .'.');
        session()->flash('success', true);

        return Redirect::back();
    }
}
