<?php

namespace App\Http\Controllers\Admin\Modules\Activitylog;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

use DB;
use Validator;
use App\User;
use App\Activity;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ActivityLogController extends Controller
{   
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$item_per_page = ROW_PER_PAGE;
        if(isset($_GET['rows'])){
           $item_per_page = $request->get('rows');
        }
		
        $activities = Activity::where(function($query) use ($request) {
			//filter by keyword
			if (($search = $request->get('search'))) {
				$query->orWhere('activity_id', 'like', '%' . $search . '%');
				$query->orWhere('activity', 'like', '%' . $search . '%');
				$query->orWhere('interface', 'like', '%' . $search . '%');
				$query->orWhere('module', 'like', '%' . $search . '%');
			}
		})
		->orderBy('created_at','desc')
		->paginate($item_per_page);

        $pagetitle = 'Activity Log';
        $breadcrumb_level1 = 'Activity Log';
        $uid = '0';
        $rid = '0';
		$item_per_page_url = url('admin/activitylog');
        
        return view('admin/modules/activitylog/activitylog', compact('activities', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'uid', 'rid', 'item_per_page', 'item_per_page_url'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
          
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
  
    }
}
