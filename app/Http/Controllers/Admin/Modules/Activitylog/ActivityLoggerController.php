<?php

namespace App\Http\Controllers\Admin\Modules\Activitylog;

use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use jeremykenedy\LaravelLogger\App\Models\Activity;
use jeremykenedy\LaravelLogger\App\Http\Traits\IpAddressDetails;
use jeremykenedy\LaravelLogger\App\Http\Traits\UserAgentDetails;

class ActivityLoggerController extends Controller
{
    use IpAddressDetails, UserAgentDetails;

    private $_rolesEnabled;
    private $_rolesMiddlware;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Add additional details to a collections
     *
     * @param  collection $collectionItems
     *
     * @return collection
     */
    private function mapAdditionalDetails($collectionItems)
    {
        $collectionItems->map(function ($collectionItem) {
            $eventTime = Carbon::parse($collectionItem->updated_at);
            $collectionItem['timePassed'] = $eventTime->diffForHumans();
            $collectionItem['userAgentDetails'] = UserAgentDetails::details($collectionItem->useragent);
            $collectionItem['langDetails'] = UserAgentDetails::localeLang($collectionItem->locale);
            $userdetails= '\App\\'.ucfirst($collectionItem->userAccess);
            $collectionItem['userDetails'] = $userdetails::find($collectionItem->userId);

            return $collectionItem;
        });

        return $collectionItems;
    }

    /**
     * Show the activities log dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['item_per_page']=$item_per_page = !empty($request->rows)?$request->rows:ROW_PER_PAGE;
        $activities = Activity::orderBy('created_at', 'desc')->paginate($item_per_page);
        $totalActivities = $activities->total();


        self::mapAdditionalDetails($activities);

        $data['activities'] = $activities;
        $data['totalActivities'] = $totalActivities;

        $data['pagetitle'] = 'Activity Logger';
        $data['breadcrumb_level1'] = 'Activity Logger';
        $data['item_per_page_url'] = url('admin/activity');


        return view('admin/modules/activitylog/activityloggers', $data);
    }

    /**
     * Show an individual activity log entry.
     *
     * @return \Illuminate\Http\Response
     */
    public function view(Request $request, $id)
    {
        $activity = Activity::findOrFail($id);
        $userdetails= '\App\\'.ucfirst($activity->userAccess);
        $userDetails = $userdetails::find($activity->userId);
        $userAgentDetails = UserAgentDetails::details($activity->useragent);
        $ipAddressDetails = IpAddressDetails::checkIP($activity->ipAddress);
        $langDetails = UserAgentDetails::localeLang($activity->locale);
        $eventTime = Carbon::parse($activity->created_at);
        $timePassed = $eventTime->diffForHumans();


        $data  = [
            'activity'              => $activity,
            'userDetails'           => $userDetails,
            'ipAddressDetails'      => $ipAddressDetails,
            'timePassed'            => $timePassed,
            'userAgentDetails'      => $userAgentDetails,
            'langDetails'           => $langDetails,
            'isClearedEntry'        => false,
        ];

        $data['pagetitle'] = 'Activity Logger';
        $data['breadcrumb_level1'] = 'Activity Logger';

        return view('admin/modules/activitylog/activitylogger', $data);
    }

    /**
     * Get Cleared (Soft Deleted) Activity - Helper Method.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $activity = Activity::find($id);
        if (count($activity) === 1) {
            $activity->destroy($id);
        }



        session()->flash('toastr_alert', '<b>Delete!</b> Activity Logger has been deleted by '. Auth::guard('admin')->user()->name .'.');
        session()->flash('success', true);

        return Redirect::back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function destroyAllRecords(Request $request)
    {
        $activities = Activity::all();

        foreach ($activities as $activity) {
            $activity->delete();
        }

        session()->flash('toastr_alert', '<b>Delete!</b> All Activity Logger has been deleted by '. Auth::guard('admin')->user()->name .'.');
        session()->flash('success', true);

        return Redirect::back();
    }
}
