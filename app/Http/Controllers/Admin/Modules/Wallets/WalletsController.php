<?php

namespace App\Http\Controllers\Admin\Modules\Wallets;

use Illuminate\Http\Request;

use App\Wallet;
use App\Http\Controllers\Controller;

class WalletsController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $wallets = Wallet::where(function ($query) use ($request) {
            //filter by keyword
            if (($search = $request->get('search'))) {
                $query->orWhere('wallet_id', 'like', '%' . $search . '%');
                $query->orWhere('primary', 'like', '%' . $search . '%');
                $query->orWhere('email', 'like', '%' . $search . '%');
                $query->orWhere('status', 'like', $search . '%');
            }
        })
        ->leftjoin('wallettypes', 'wallettypes.wallet_type_id', '=', 'wallets.wallet_type_id')
        ->leftjoin('walletfees', 'walletfees.wallet_fee_id', '=', 'wallets.wallet_fee_id')
        ->leftjoin('walletlimits', 'walletlimits.wallet_limit_id', '=', 'wallets.wallet_limit_id')
        ->select('wallets.*', 'wallettypes.name as wtname', 'walletlimits.name as wlname', 'walletfees.name as wfname')
        ->orderBy('created_at', 'desc')
        ->paginate(10);

        $pagetitle = '+Wallets';
        $breadcrumb_level1 = '+Wallets Management';
        $breadcrumb_level2 = '<a href="/admin/wallets">+Wallets</a>';
        $wallet_id = '0';



        return view('admin/modules/wallets/wallets', compact('wallets', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'wallet_id'));
    }
}
