<?php

namespace App\Http\Controllers\Admin\Modules\Wallets;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

use DB;
use Validator;
use Auth;
use Toastr;
use App\Activity;
use App\Wallet;
use App\Wallettype;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class WallettypesController extends Controller
{   
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
        $wallettypes = Wallettype::latest()->paginate(10);
		
        $pagetitle = '+Wallet Types';
        $breadcrumb_level1 = '+Wallet Management';
        $breadcrumb_level2 = '<a href="/admin/wallettypes">+Wallet Types</a>';
        
        return view('admin/modules/wallets/wallettypes', compact('wallettypes', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'breadcrumb_level3'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $wallettypes = Wallettype::latest()->paginate(10);
		
        $pagetitle = '+Wallet Types';
        $breadcrumb_level1 = '+Wallet Management';
        $breadcrumb_level2 = '<a href="/admin/wallettypes">+Wallet Types</a>';
        
        return view('admin/modules/wallets/wallettypes', compact('wallettypes', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'breadcrumb_level3'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
			'add_wallet' => 'required',
			'send' => 'required',
			'pay' => 'required',
			'request' => 'required',
			'add_currency' => 'required',
			'add' => 'required',
			'withdraw' => 'required',
			'transfer' => 'required',
			'upgrade' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#add")
                ->withErrors($validator)
                ->withInput();
        }
        
        $wallettype = new Wallettype($request->all());
		
		$wallet_type_id=(int)$wallet_type_id=DB::table('wallettypes')->orderBy('wallet_type_id','desc')->value('wallet_type_id');
		if($wallet_type_id < 1) $wallet_type_id=1010;
		else $wallet_type_id = $wallet_type_id + 1;
		
		$wallettype['wallet_type_id'] = $wallet_type_id;
		$wallettype['status'] = 'Active';
		$walletfee['added_by'] = Auth::guard('admin')->user()->name;
		$wallettype->save();
        
        $activity = new Activity ();
        $activity['admin_id'] = Auth::guard('admin')->user()->admin_id;
		$activity['added_by'] = Auth::guard('admin')->user()->name;
		$activity['interface'] = 'Admin';
        $activity['module'] = 'Wallet';
        $activity['activity'] = 'New Wallet Type '.$request->name.' has been added by '. Auth::guard('admin')->user()->name .'.';
        $activity->save();
		
		session()->flash('toastr_alert','<b>Added!</b> New Wallet Fees '.$request->name.' has been added by '. Auth::guard('admin')->user()->name .'.');
		session()->flash('success', true);
		
        return Redirect::to(URL::previous());
     
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {

    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($wallet_type_id)
    {
		$wallettype = DB::table('wallettypes')->where('wallet_type_id', '=', $wallet_type_id)->first();
        $pagetitle = $wallettype->name;
        $breadcrumb_level1 = '+Wallet Management';
        $breadcrumb_level2 = '<a href="/admin/wallettypes">+Wallet Types</a>';
        $breadcrumb_level3 = '<a href="/admin/wallettypes/'.$wallet_type_id.'">+Wallet Type</a>';
        
        return view('admin/modules/wallets/wallettype', compact('wallettype', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'breadcrumb_level3'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $wallet_type_id)
    {
		$validator = Validator::make($request->all(), [
            'name' => 'required',
			'add_wallet' => 'required',
			'send' => 'required',
			'pay' => 'required',
			'request' => 'required',
			'add_currency' => 'required',
			'add' => 'required',
			'withdraw' => 'required',
			'transfer' => 'required',
			'upgrade' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#edit")
                ->withErrors($validator)
                ->withInput();
        }
        
        $wallettype = Wallettype::where('wallet_type_id', '=', $wallet_type_id)->firstOrFail();
        $wallettype->update($request->all());
		$wallettype->save();
        
        $activity = new Activity ();
        $activity['admin_id'] = Auth::guard('admin')->user()->admin_id;
		$activity['added_by'] = Auth::guard('admin')->user()->name;
		$activity['interface'] = 'Admin';
        $activity['module'] = 'Wallet';
        $activity['activity'] = 'Wallet Type '.$request->name.' has been updated by '. Auth::guard('admin')->user()->name .'.';
		$activity->save();
		
		session()->flash('toastr_alert','<b>Updated!</b> Wallet Fees '.$request->name.' has been updated by '. Auth::guard('admin')->user()->name .'.');
		session()->flash('info', true);
        
        return Redirect::to(URL::previous());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
