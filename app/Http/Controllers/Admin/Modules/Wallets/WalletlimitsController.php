<?php

namespace App\Http\Controllers\Admin\Modules\Wallets;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

use DB;
use Validator;
use Auth;
use Toastr;
use App\Activity;
use App\Walletlimit;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class WalletlimitsController extends Controller
{   
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
        $walletlimits = Walletlimit::latest()->paginate(10);
		$currencies = DB::table('currencies')->where('active', '=', 1)->orderBy('cy_code','asc')->pluck('cy_code','cy_code');
        
		$pagetitle = '+Wallet Limits';
        $breadcrumb_level1 = '+Wallet Management';
        $breadcrumb_level2 = '<a href="/admin/walletlimits">+Wallet Limits</a>';
        
        return view('admin/modules/wallets/walletlimits', compact('walletlimits', 'currencies', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $walletlimits = Walletlimit::latest()->paginate(10);
		$currencies = DB::table('currencies')->where('active', '=', 1)->orderBy('cy_code','asc')->pluck('cy_code','cy_code');
        
		$pagetitle = '+Wallet Limits';
        $breadcrumb_level1 = '+Wallet Management';
        $breadcrumb_level2 = '<a href="/admin/walletlimits">+Wallet Limits</a>';
        
        return view('admin/modules/wallets/walletlimits', compact('walletlimits', 'currencies', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'currency' => 'required',
            'max_balance' => 'required',
            'max_turnover' => 'required',
            'max_turnover_per_month' => 'required',
            'max_credit_amount_per_day' => 'required',
            'max_credit_payments_per_day' => 'required',
            'max_topup_amount_per_day' => 'required',
            'max_topup_payments_per_day' => 'required',
            'max_topup_amount_per_payment' => 'required',
            'max_receive_amount_per_day' => 'required',
            'max_receive_payments_per_day' => 'required',
            'max_receive_amount_per_payment' => 'required',
            'max_debit_amount_per_day' => 'required',
            'max_debit_payments_per_day' => 'required',
            'max_send_amount_per_day' => 'required',
            'max_send_payments_per_day' => 'required',
            'max_send_amount_per_payment' => 'required',
            'max_withdraw_amount_per_day' => 'required',
            'max_withdraw_payments_per_day' => 'required',
            'max_withdraw_amount_per_payment' => 'required',
            'max_withdraw_amount' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#add")
                ->withErrors($validator)
                ->withInput();
        }
        
        $walletlimit = new Walletlimit($request->all());     
		
		$wallet_limit_id=(int)$wallet_limit_id=DB::table('walletlimits')->orderBy('wallet_limit_id','desc')->value('wallet_limit_id');
		if($wallet_limit_id < 1) $wallet_limit_id=1010;
		else $wallet_limit_id = $wallet_limit_id + 1;
		        
		$walletlimit['wallet_limit_id'] = $wallet_limit_id;
        $walletlimit['name'] = $walletlimit->name.'('.$walletlimit->currency.')';
		$walletlimit['status'] = 'Active';
		$walletlimit['added_by'] = Auth::guard('admin')->user()->name;
		$walletlimit->save();
        
		$activity = new Activity ();
        $activity['admin_id'] = Auth::guard('admin')->user()->id;
		$activity['added_by'] = Auth::guard('admin')->user()->name;
		$activity['interface'] = 'Admin';
        $activity['module'] = 'Wallet';
        $activity['activity'] = 'New Wallet Limits '.$walletlimit->name.' has been added by '. Auth::guard('admin')->user()->name .'.';
        $activity->save();
		
		session()->flash('toastr_alert','<b>Added!</b> New Wallet Limits '.$walletlimit->name.' has been added by '. Auth::guard('admin')->user()->name .'.');
		session()->flash('success', true);
		
        return Redirect::to(URL::previous());
     
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($wallet_limit_id)
    {
        $walletlimit = DB::table('walletlimits')->where('wallet_limit_id', '=', $wallet_limit_id)->first();
		$pagetitle = $walletlimit->name;
        $breadcrumb_level1 = '+Wallet Management';
        $breadcrumb_level2 = '<a href="/admin/walletlimits">+Wallet Limits</a>';
        $breadcrumb_level3 = '<a href="/admin/walletlimits/'.$wallet_limit_id.'">+Wallet Limit</a>';        
        
        return view('admin/modules/wallets/walletlimit', compact('walletlimit', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'breadcrumb_level3'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($wallet_limit_id)
    {
        $walletlimit = DB::table('walletlimits')->where('wallet_limit_id', '=', $wallet_limit_id)->first();
		$pagetitle = $walletlimit->name;
        $breadcrumb_level1 = '+Wallet Management';
        $breadcrumb_level2 = '<a href="/admin/walletlimits">+Wallet Limits</a>';
        $breadcrumb_level3 = '<a href="/admin/walletlimits/'.$wallet_limit_id.'">+Wallet Limit</a>';        
        
        return view('admin/modules/wallets/walletlimit', compact('walletlimit', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'breadcrumb_level3'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $wallet_limit_id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'max_balance' => 'required',
            'max_turnover' => 'required',
            'max_turnover_per_month' => 'required',
            'max_credit_amount_per_day' => 'required',
            'max_credit_payments_per_day' => 'required',
            'max_topup_amount_per_day' => 'required',
            'max_topup_payments_per_day' => 'required',
            'max_topup_amount_per_payment' => 'required',
            'max_receive_amount_per_day' => 'required',
            'max_receive_payments_per_day' => 'required',
            'max_receive_amount_per_payment' => 'required',
            'max_debit_amount_per_day' => 'required',
            'max_debit_payments_per_day' => 'required',
            'max_send_amount_per_day' => 'required',
            'max_send_payments_per_day' => 'required',
            'max_send_amount_per_payment' => 'required',
            'max_withdraw_amount_per_day' => 'required',
            'max_withdraw_payments_per_day' => 'required',
            'max_withdraw_amount_per_payment' => 'required',
            'max_withdraw_amount' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#edit")
                ->withErrors($validator)
                ->withInput();
        }
        
        $walletlimit = Walletlimit::where('wallet_limit_id', '=', $wallet_limit_id)->firstOrFail();
        $walletlimit->update($request->all());
		$walletlimit->save();
        
        $activity = new Activity ();
        $activity['admin_id'] = Auth::guard('admin')->user()->id;
		$activity['added_by'] = Auth::guard('admin')->user()->name;
		$activity['interface'] = 'Admin';
        $activity['module'] = 'Wallet';
        $activity['activity'] = 'Wallet Limits '.$walletlimit->name.' has been updated by '. Auth::guard('admin')->user()->name .'.';
		$activity->save();
		
		session()->flash('toastr_alert','<b>Updated!</b> Wallet Limits '.$walletlimit->name.' has been updated by '. Auth::guard('admin')->user()->name .'.');
		session()->flash('info', true);
        
        return Redirect::to(URL::previous());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
