<?php

namespace App\Http\Controllers\Admin\Modules\Wallets;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

use DB;
use Validator;
use Auth;
use Toastr;
use App\Activity;
use App\Walletfee;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class WalletfeesController extends Controller
{   
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
        $walletfees = Walletfee::latest()->paginate(10);
		$currencies = DB::table('currencies')->where('active', '=', 1)->orderBy('cy_code','asc')->pluck('cy_code','cy_code');
		
        $pagetitle = '+Wallet Fees';
        $breadcrumb_level1 = '+Wallet Management';
        $breadcrumb_level2 = '<a href="/admin/walletfees">+Wallet Fees</a>';
        
        return view('admin/modules/wallets/walletfees', compact('walletfees', 'currencies', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $walletfees = Walletfee::latest()->paginate(10);
		$currencies = DB::table('currencies')->where('active', '=', 1)->orderBy('cy_code','asc')->pluck('cy_code','cy_code');
		
        $pagetitle = '+Wallet Fees';
        $breadcrumb_level1 = '+Wallet Management';
        $breadcrumb_level2 = '<a href="/admin/walletfees">+Wallet Fees</a>';
        
        return view('admin/modules/wallets/walletfees', compact('walletfees', 'currencies', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'currency' => 'required',
            'creation_fee' => 'required',
            'monthly_fee' => 'required',
            'send_fee' => 'required',
			'send_flat_fee' => 'required',
            'receive_fee' => 'required',
			'receive_flat_fee' => 'required',
            'withdraw_fee' => 'required',
			'withdraw_flat_fee' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#add")
                ->withErrors($validator)
                ->withInput();
        }
        
        $walletfee = new Walletfee($request->all());
		
		$wallet_fee_id=(int)$wallet_fee_id=DB::table('walletfees')->orderBy('wallet_fee_id','desc')->value('wallet_fee_id');
		if($wallet_fee_id < 1) $wallet_fee_id=1010;
		else $wallet_fee_id = $wallet_fee_id + 1;
		        
		$walletfee['wallet_fee_id'] = $wallet_fee_id;
        $walletfee['name'] = $walletfee->name.'('.$walletfee->currency.')';
		$walletfee['status'] = 'Active';
		$walletfee['added_by'] = Auth::guard('admin')->user()->name;
		$walletfee->save();
        
		$activity = new Activity ();
        $activity['admin_id'] = Auth::guard('admin')->user()->admin_id;
		$activity['added_by'] = Auth::guard('admin')->user()->name;
		$activity['interface'] = 'Admin';
        $activity['module'] = 'Wallet';
        $activity['activity'] = 'New Wallet Fees '.$walletfee->name.' has been added by '. Auth::guard('admin')->user()->name .'.';
        $activity->save();
		
		session()->flash('toastr_alert','<b>Added!</b> New Wallet Fees '.$walletfee->name.' has been added by '. Auth::guard('admin')->user()->name .'.');
		session()->flash('success', true);
		
        return Redirect::to(URL::previous());
     
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($wallet_fee_id)
    {
        $walletfee = DB::table('walletfees')->where('wallet_fee_id', '=', $wallet_fee_id)->first();
        $pagetitle = '+Wallet Fees';
        $breadcrumb_level1 = '+Wallet Management';
        $breadcrumb_level2 = '<a href="/admin/walletfees">+Wallet Fees</a>';
        $breadcrumb_level3 = '<a href="/admin/walletfees/'.$wallet_fee_id.'">+Wallet Fee</a>';
        
        return view('admin/modules/wallets/walletfee', compact('walletfee', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'breadcrumb_level3'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($wallet_fee_id)
    {
        $walletfee = DB::table('walletfees')->where('wallet_fee_id', '=', $wallet_fee_id)->first();
        $pagetitle = $walletfee->name;
        $breadcrumb_level1 = '+Wallet Management';
        $breadcrumb_level2 = '<a href="/admin/walletfees">+Wallet Fees</a>';
        $breadcrumb_level3 = '<a href="/admin/walletfees/'.$wallet_fee_id.'">+Wallet Fee</a>';
        
        return view('admin/modules/wallets/walletfee', compact('walletfee', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'breadcrumb_level3'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $wallet_fee_id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'creation_fee' => 'required',
            'monthly_fee' => 'required',
            'send_fee' => 'required',
			'send_flat_fee' => 'required',
            'receive_fee' => 'required',
			'receive_flat_fee' => 'required',
            'withdraw_fee' => 'required',
			'withdraw_flat_fee' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#edit")
                ->withErrors($validator)
                ->withInput();
        }
        
        $walletfee = Walletfee::where('wallet_fee_id', '=', $wallet_fee_id)->firstOrFail();
        $walletfee->update($request->all());
		$walletfee->save();
        
        $activity = new Activity ();
        $activity['admin_id'] = Auth::guard('admin')->user()->admin_id;
		$activity['added_by'] = Auth::guard('admin')->user()->name;
		$activity['interface'] = 'Admin';
        $activity['module'] = 'Wallet';
        $activity['activity'] = 'Wallet Fees '.$walletfee->name.' has been updated by '. Auth::guard('admin')->user()->name .'.';
		$activity->save();
		
		session()->flash('toastr_alert','<b>Updated!</b> Wallet Fees '.$walletfee->name.' has been updated by '. Auth::guard('admin')->user()->name .'.');
		session()->flash('info', true);
        
        return Redirect::to(URL::previous());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
