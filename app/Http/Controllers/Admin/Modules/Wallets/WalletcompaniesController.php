<?php

namespace App\Http\Controllers\Admin\Modules\Wallets;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

use DB;
use Validator;
use Auth;
use Toastr;
use App\Activity;
use App\Walletcompany;
use App\Merchant;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class WalletcompaniesController extends Controller
{   
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
        $mid = DB::table('wallet_companies')->where('id', '=', '1')->value('merchant_id');
        $walletcompany = DB::table('merchants')->where('id', '=', $mid)->get();
        $pagetitle = '+Wallet Management';
        $breadcrumb_level1 = '<a href="/wallets">+Wallets</a>';
        $breadcrumb_level2 = '<a href="/walletcompany">+Wallet Management</a>';
        $breadcrumb_level3 = '<a href="/walletcompany">+Wallet Company</a>';
        $wid = '0';
        
        return view('admin/modules/wallets/walletcompany', compact('walletcompany', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'breadcrumb_level3', 'wid'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $mid = DB::table('wallet_companies')->where('id', '=', '1')->value('merchant_id');
        $walletcompany = DB::table('merchants')->where('id', '=', $mid)->get();
        $pagetitle = '+Wallet Management';
        $breadcrumb_level1 = '<a href="/wallets">+Wallets</a>';
        $breadcrumb_level2 = '<a href="/walletcompany">+Wallet Management</a>';
        $breadcrumb_level3 = '<a href="/walletcompany">+Wallet Company</a>';
        $wid = '0';
        
        return view('admin/modules/wallets/walletcompany', compact('walletcompany', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'breadcrumb_level3', 'wid'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mid' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#add")
                ->withErrors($validator)
                ->withInput();
        }
        
        $walletcompany = new Walletcompany($request->all());
        $walletcompanyname = DB::table('merchants')->where('id', '=', $request->mid)->value('companyname');
        
        $activity = new Activity ();
        $activity['uid'] = Auth::user()->id;
        $activity['module'] = 'Wallet';
        $activity['activity'] = 'New Wallet Company <a href="/walletcompany">'. $walletcompanyname .'</a> has been added by <a href="/usermanagement/users/'. Auth::user()->id .'">'. Auth::user()->firstname .' '. Auth::user()->lastname .'</a>.';
        
        Auth::user()->walletcompanies()->save($walletcompany);
        Auth::user()->activities()->save($activity);
        
        Toastr::success('Added!','New Wallet Company <a href="/walletcompany">'. $walletcompanyname .'</> has been added by <a href="/usermanagement/users/'. Auth::user()->id .'">'. Auth::user()->firstname .' '. Auth::user()->lastname .'</a>.')->push();
        
        return Redirect::to(URL::previous());
     
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($wid)
    {

    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($wid)
    {
    
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
    
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function lockunlock(Request $request, $wtid)
    {
        $wallettype = Wallettype::findOrFail($id = $wtid);   
        $wallettype->update($request->all());
        
        $activity = new Activity ();
        $activity['uid'] = Auth::user()->id;
        $activity['module'] = 'Wallet';
        if ($wallettype->status === 'Active') {$activity['activity'] = 'Wallet Type <a href="/wallettypes">'. $wallettype->type .'</a> has been activate by <a href="/usermanagement/users/'. Auth::user()->id .'">'. Auth::user()->firstname .' '. Auth::user()->lastname .'</a>.';}
        elseif ($wallettype->status === 'Inactive') {$activity['activity'] = 'Wallet Type <a href="/wallettypes">'. $wallettype->type .'</a> has been deactivated by <a href="/usermanagement/users/'. Auth::user()->id .'">'. Auth::user()->firstname .' '. Auth::user()->lastname .'</a>.';}
        
        Auth::user()->wallettypes()->save($wallettype);
        Auth::user()->activities()->save($activity);
        
        if ($wallettype->status === 'Active') {Toastr::info('Activated!','Wallet Type <a href="/wallettypes">'. $wallettype->type .'</a> has been activated by <a href="/usermanagement/users/'. Auth::user()->id .'">'. Auth::user()->firstname .' '. Auth::user()->lastname .'</a>.')->push();}
        elseif ($wallettype->status === 'Inactive') {Toastr::info('Deactivated!','Wallet Type <a href="/wallettypes">'. $wallettype->type .'</a> has been deactivated by <a href="/usermanagement/users/'. Auth::user()->id .'">'. Auth::user()->firstname .' '. Auth::user()->lastname .'</a>.')->push();}
        
        return Redirect::to(URL::previous());
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function topup(Request $request, $wtid)
    {
        $wallettype = Wallettype::findOrFail($id = $wtid);   
        $wallettype->update($request->all());
        
        $activity = new Activity ();
        $activity['uid'] = Auth::user()->id;
        $activity['module'] = 'Wallet';
        $activity['activity'] = 'Wallet Type <a href="/wallettypes">'. $wallettype->type .'</a> has been updated by <a href="/usermanagement/users/'. Auth::user()->id .'">'. Auth::user()->firstname .' '. Auth::user()->lastname .'</a>.';
        
        Auth::user()->wallettypes()->save($wallettype);
        Auth::user()->activities()->save($activity);
        
        Toastr::info('Updated!','Wallet Type <a href="/wallettypes">'. $wallettype->type .'</a> has been updated by <a href="/usermanagement/users/'. Auth::user()->id .'">'. Auth::user()->firstname .' '. Auth::user()->lastname .'</a>.')->push();
        
        return Redirect::to(URL::previous());
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function send(Request $request, $wtid)
    {
        $wallettype = Wallettype::findOrFail($id = $wtid);   
        $wallettype->update($request->all());
        
        $activity = new Activity ();
        $activity['uid'] = Auth::user()->id;
        $activity['module'] = 'Wallet';
        $activity['activity'] = 'Wallet Type <a href="/wallettypes">'. $wallettype->type .'</a> has been updated by <a href="/usermanagement/users/'. Auth::user()->id .'">'. Auth::user()->firstname .' '. Auth::user()->lastname .'</a>.';
        
        Auth::user()->wallettypes()->save($wallettype);
        Auth::user()->activities()->save($activity);
        
        Toastr::info('Updated!','Wallet Type <a href="/wallettypes">'. $wallettype->type .'</a> has been updated by <a href="/usermanagement/users/'. Auth::user()->id .'">'. Auth::user()->firstname .' '. Auth::user()->lastname .'</a>.')->push();
        
        return Redirect::to(URL::previous());
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function receive(Request $request, $wtid)
    {
        $wallettype = Wallettype::findOrFail($id = $wtid);   
        $wallettype->update($request->all());
        
        $activity = new Activity ();
        $activity['uid'] = Auth::user()->id;
        $activity['module'] = 'Wallet';
        $activity['activity'] = 'Wallet Type <a href="/wallettypes">'. $wallettype->type .'</a> has been updated by <a href="/usermanagement/users/'. Auth::user()->id .'">'. Auth::user()->firstname .' '. Auth::user()->lastname .'</a>.';
        
        Auth::user()->wallettypes()->save($wallettype);
        Auth::user()->activities()->save($activity);
        
        Toastr::info('Updated!','Wallet Type <a href="/wallettypes">'. $wallettype->type .'</a> has been updated by <a href="/usermanagement/users/'. Auth::user()->id .'">'. Auth::user()->firstname .' '. Auth::user()->lastname .'</a>.')->push();
        
        return Redirect::to(URL::previous());
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function withdraw(Request $request, $wtid)
    {
        $wallettype = Wallettype::findOrFail($id = $wtid);   
        $wallettype->update($request->all());
        
        $activity = new Activity ();
        $activity['uid'] = Auth::user()->id;
        $activity['module'] = 'Wallet';
        $activity['activity'] = 'Wallet Type <a href="/wallettypes">'. $wallettype->type .'</a> has been updated by <a href="/usermanagement/users/'. Auth::user()->id .'">'. Auth::user()->firstname .' '. Auth::user()->lastname .'</a>.';
        
        Auth::user()->wallettypes()->save($wallettype);
        Auth::user()->activities()->save($activity);
        
        Toastr::info('Updated!','Wallet Type <a href="/wallettypes">'. $wallettype->type .'</a> has been updated by <a href="/usermanagement/users/'. Auth::user()->id .'">'. Auth::user()->firstname .' '. Auth::user()->lastname .'</a>.')->push();
        
        return Redirect::to(URL::previous());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
