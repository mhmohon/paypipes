<?php

namespace App\Http\Controllers\Admin\Modules\Settings;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Input;

use DB;
use Auth;
use Validator;
use App\Activity;
use App\Bankaccount;
use App\Setting;
use App\Country;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class BankaccountsController extends Controller
{   
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['bankaccounts']= Bankaccount::orderBy('created_at','desc')->paginate(10);
		
		$data['currencies'] = DB::table('currencies')->where('active', '=', 1)->orderBy('cy_code','asc')->pluck('cy_code', 'cy_code');	
       
		$data['pagetitle'] = 'Bank Accounts';
		$data['breadcrumb_level1'] = 'Settings';
		$data['breadcrumb_level2'] = '<a href="/admin/settings/bank_accounts">Bank Accounts</a>';
		
		return view('admin/modules/settings/bank_accounts', $data);
    }
	 
	/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {		
		$validator = Validator::make($request->all(), [
			'account_holder' => 'required|max:255',
			'account_number' => 'required|max:255',
			'currency' => 'required|max:3',
			'bank_name' => 'required|max:255',  
			'iban' => 'required|max:255',
			'swift_bic' => 'required|max:255',
		]);

		if ($validator->fails()) {
			return Redirect::to(URL::previous() . "#add")
				->withErrors($validator)
				->withInput();
		}
		
		$bankaccount = new Bankaccount($request->all());
	
		$bankaccount['status'] = 'Active';		
		$bankaccount->save();
		
		$activity = new Activity ();
		$activity['added_by'] = Auth::guard('admin')->user()->name;
		$activity['interface'] = 'Admin';
        $activity['module'] = 'Setting';
        $activity['activity'] = 'New Bank Account '. $request->account_number .' has been added by '. Auth::guard('admin')->user()->name .'.';
		$activity->save();
		
		session()->flash('toastr_alert','<b>Added!</b> New Bank Account '. $request->account_number .' has been added by '. Auth::guard('admin')->user()->name .'.');
		session()->flash('success', true);
		
		return Redirect::to(URL::previous());
		
	}
	
	/**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
		$data['bankaccount'] = Bankaccount::where('id', '=', $id)->first();	

		$data['currencies'] = DB::table('currencies')->where('active', '=', 1)->orderBy('cy_code','asc')->pluck('cy_code', 'cy_code');				

        $data['pagetitle'] = 'Bank Account';
		$data['breadcrumb_level1'] = 'Settings';
        $data['breadcrumb_level2'] = '<a href="/admin/settings/bank_accounts">Bank Accounts</a>';
        $data['breadcrumb_level3'] = '<a href="/admin/settings/bank_accounts/'.$id.'">Bank Account</a>';

        
        return view('admin/modules/settings/bank_account', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$validator = Validator::make($request->all(), [
            'account_holder' => 'required|max:255',
			'account_number' => 'required|max:255',
			'currency' => 'required|max:3',
			'bank_name' => 'required|max:255',  
			'iban' => 'required|max:255',
			'swift_bic' => 'required|max:255',
            
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#edit")
                ->withErrors($validator)
                ->withInput();
        }
        
        $bankaccount = Bankaccount::findOrFail($id = $id);
        $bankaccount->update($request->all());
        
        $activity = new Activity ();
        $activity['admin_id'] = Auth::guard('admin')->user()->id;
        $activity['added_by'] = Auth::guard('admin')->user()->name;
        $activity['interface'] = 'Admin';
        $activity['module'] = 'Setting';
        $activity['activity'] = 'Bank Account '. $bankaccount->account_number .' has been updated by '. Auth::guard('admin')->user()->name .'.';
        $activity->save();
       
        session()->flash('toastr_alert','<b>Updated!</b> Bank Account '. $bankaccount->account_number .' has been updated by '. Auth::guard('admin')->user()->name .'.');
        session()->flash('info', true);
        
        return Redirect::to(URL::previous());
    }
	
	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function lockunlock(Request $request, $id)
    {
        $bankaccount = Bankaccount::findOrFail($id);   
        
        $bankaccount['status']= $request->status;  
		$bankaccount->save();		
		
		$activity = new Activity ();
        $activity['admin_id'] = Auth::guard('admin')->user()->id;
		$activity['added_by'] = Auth::guard('admin')->user()->name;
		$activity['interface'] = 'Admin';
        $activity['module'] = 'Setting';
		if ($bankaccount->status === 'Active') {$activity['activity'] = 'Bank Account '. $bankaccount->account_number .' has been activated by '. Auth::guard('admin')->user()->name .'.';}
        elseif ($bankaccount->status === 'Inactive') {$activity['activity'] = 'Bank Account '. $bankaccount->account_number .' has been deactivated by '. Auth::guard('admin')->user()->name .'.';}
		$activity->save();	
      
		if ($bankaccount->status === 'Active') {
			session()->flash('toastr_alert','<b>Activated!</b> Bank Account '. $bankaccount->account_number .' has been activated by '. Auth::guard('admin')->user()->name .'.');
			session()->flash('info', true); 
		}
		elseif ($bankaccount->status === 'Inactive') {
			session()->flash('toastr_alert','<b>Deactivated!</b> Bank Account '. $bankaccount->account_number .' has been deactivated by '. Auth::guard('admin')->user()->name .'.');
			session()->flash('info', true);
		}
		
        return Redirect::to(URL::previous());
    }
}
