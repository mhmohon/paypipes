<?php

namespace App\Http\Controllers\Admin\Modules\Settings;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

use DB;
use Auth;
use Validator;
use App\Activity;
use App\Setting;
use App\Country;
use App\Timezone;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['setting']=DB::table('settings')->where('id', '=', 1)->first();

        $data['countries'] = ['0'=>'Select a Country'] + Country::pluck('country_name', 'country_code')->all();
        $data['timezones'] = Timezone::timezonelist();

        if ($request->is('admin/settings')) {
            $data['pagetitle'] = 'General';
            $data['breadcrumb_level1'] = 'Settings';
            $data['breadcrumb_level2'] = '<a href="/admin/settings">General</a>';
            return view('admin/modules/settings/settings', $data);
        } elseif ($request->is('admin/settings/company_details')) {
            $data['pagetitle'] = 'Company Details';
            $data['breadcrumb_level1'] = 'Settings';
            $data['breadcrumb_level2'] = '<a href="/admin/settings/company_details">Company Details</a>';
            return view('admin/modules/settings/company_details', $data);
        } elseif ($request->is('admin/settings/api')) {
            $data['pagetitle'] = 'API';
            $data['breadcrumb_level1'] = 'Settings';
            $data['breadcrumb_level2'] = '<a href="/admin/settings/api">API</a>';
            return view('admin/modules/settings/api', $data);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $setting = Setting::where('id', '=', 1)->firstOrFail();

        $validator = Validator::make($request->all(), [
            'website' => 'required|max:100',
            'support_email' => 'required|email',
            'timezone' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#edit")
                ->withErrors($validator)
                ->withInput();
        }

        $setting['website'] = $request->website ;
        $setting['support_email'] = $request->support_email ;
        $setting['timezone'] = $request->timezone ;
        $setting->save();

        $activity = new Activity();
        $activity['admin_id'] = Auth::guard('admin')->user()->id;
        $activity['added_by'] = Auth::guard('admin')->user()->name;
        $activity['interface'] = 'Admin';
        $activity['module'] = 'Setting';
        $activity['activity'] = 'General Settings has been updated by '. Auth::guard('admin')->user()->name .'.';
        $activity->save();

        session()->flash('toastr_alert', '<b>Updated!</b> General Settings has been updated by '. Auth::guard('admin')->user()->name .'.');
        session()->flash('info', true);

        return Redirect::to(URL::previous());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function company_details(Request $request)
    {
        $setting = Setting::where('id', '=', 1)->firstOrFail();

        $validator = Validator::make($request->all(), [
            'company_legal_name' => 'required|max:100',
            'street' => 'required|max:100',
            'city' => 'required|max:100',
            'post_code' => 'required|max:100',
            'country' => 'required|max:100',
            'company_id' => 'required|max:100',
            'vat_id' => 'required|max:100',
            'invoice_email' => 'required|max:100',
            'invoice_website' => 'required|max:100',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#edit")
                ->withErrors($validator)
                ->withInput();
        }

        $setting['company_legal_name'] = $request->company_legal_name ;
        $setting['street'] = $request->street ;
        $setting['city'] = $request->city ;
        $setting['state'] = $request->state ;
        $setting['post_code'] = $request->post_code ;
        $setting['country'] = $request->country ;
        $setting['company_id'] = $request->company_id ;
        $setting['vat_id'] = $request->vat_id ;
        $setting['invoice_email'] = $request->invoice_email ;
        $setting['invoice_website'] = $request->invoice_website ;
        $setting->save();

        $activity = new Activity();
        $activity['admin_id'] = Auth::guard('admin')->user()->id;
        $activity['added_by'] = Auth::guard('admin')->user()->name;
        $activity['interface'] = 'Admin';
        $activity['module'] = 'Setting';
        $activity['activity'] = 'Company Details '. $request->company_legal_name .' has been updated by '. Auth::guard('admin')->user()->name .'.';
        $activity->save();

        session()->flash('toastr_alert', '<b>Updated!</b> Company Details '. $request->company_legal_name .' has been updated by '. Auth::guard('admin')->user()->name .'.');
        session()->flash('info', true);

        return Redirect::to(URL::previous());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function api(Request $request)
    {
        $setting = Setting::where('id', '=', 1)->firstOrFail();

        $validator = Validator::make($request->all(), [
            'api_id' => 'required',
            'client_id' => 'required',
            'client_secret' => 'required',
            'direct_api_id' => 'required',
            'direct_client_id' => 'required',
            'direct_client_secret' => 'required',
            'api_url' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#edit")
                ->withErrors($validator)
                ->withInput();
        }

        $setting['api_id'] = $request->api_id ;
        $setting['client_id'] = $request->client_id ;
        $setting['client_secret'] = $request->client_secret ;
        $setting['direct_api_id'] = $request->direct_api_id ;
        $setting['direct_client_id'] = $request->direct_client_id ;
        $setting['direct_client_secret'] = $request->direct_client_secret ;
        $setting['api_url'] = $request->api_url ;
        $setting->save();

        $activity = new Activity();
        $activity['admin_id'] = Auth::guard('admin')->user()->id;
        $activity['added_by'] = Auth::guard('admin')->user()->name;
        $activity['interface'] = 'Admin';
        $activity['module'] = 'Setting';
        $activity['activity'] = 'API Settings has been updated by '. Auth::guard('admin')->user()->name .'.';
        $activity->save();

        session()->flash('toastr_alert', '<b>Updated!</b> API Settings has been updated by '. Auth::guard('admin')->user()->name .'.');
        session()->flash('info', true);

        return Redirect::to(URL::previous());
    }
}
