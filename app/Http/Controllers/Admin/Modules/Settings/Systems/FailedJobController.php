<?php

namespace App\Http\Controllers\Admin\Modules\Settings\Systems;

use App\Entities\FailedJob;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FailedJobController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['item_per_page']=$item_per_page = !empty($request->rows)?$request->rows:ROW_PER_PAGE;
        $data['pg']='?'.http_build_query($request->all());

        $data['failedjobs']=FailedJob::orderBy('failed_at', 'desc')
        ->paginate($item_per_page);

        $data['pagetitle'] = 'Failed Job';
        $data['breadcrumb_level1'] = 'Settings';
        $data['breadcrumb_level2'] = '<a href="/admin/settings/failedjobs">Failed Job</a>';
        return view('admin/modules/settings/systems/failedjobs', $data);
    }
}
