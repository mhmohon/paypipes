<?php

namespace App\Http\Controllers\Admin\Modules\Settings\Systems;

use Auth;
use Validator;
use App\Activity;
use App\Currency;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class CurrencyController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['item_per_page']=$item_per_page = !empty($request->rows)?$request->rows:ROW_PER_PAGE;
        $data['item_per_page_url'] = url('admin/settings/currencies');
        $data['pg']='?'.http_build_query($request->all());
        if (!empty($request->get('search')) && ($request->get('search') == 'Active')) {
            $status = 1;
        } else {
            $status = 0;
        }
        $data['currencies']=Currency::where(function ($query) use ($request, $status) {
            //filter by keyword
            if ($search = $request->get('search')) {
                $query->orWhere('currency', 'like', '%' . $search . '%');
                $query->orWhere('cy_code', 'like', '%' . $search . '%');
                $query->orWhere('iso_num', 'like', '%' . $search . '%');
                $query->orWhere('cy_symbol', 'like', '%' . $search . '%');
                if ($search == 'Active' || $search == 'Inactive') {
                    $query->orWhere('active', '=', $status);
                }
            }
        })
        ->orderBy('active', 'desc')
        ->paginate($item_per_page);

        $data['pagetitle'] = 'Currency';
        $data['breadcrumb_level1'] = 'Settings';
        $data['breadcrumb_level2'] = '<a href="/admin/settings/currencies">Currency</a>';
        return view('admin/modules/settings/systems/currencies', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'currency' => 'required|max:100',
            'cy_code' => 'required|min:3|max:3',
            'iso_num' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#add")
                ->withErrors($validator)
                ->withInput();
        }

        $curerncy = new Currency($request->all());
        $curerncy->save();

        $activity = new Activity();
        $activity['admin_id'] = Auth::guard('admin')->user()->id;
        $activity['added_by'] = Auth::guard('admin')->user()->name;
        $activity['interface'] = 'Admin';
        $activity['module'] = 'System - Curerncy';
        $activity['activity'] = 'Currency has been updated by '. Auth::guard('admin')->user()->name .'.';
        $activity->save();

        session()->flash('toastr_alert', '<b>Updated!</b> General Settings has been updated by '. Auth::guard('admin')->user()->name .'.');
        session()->flash('info', true);

        return Redirect::to(URL::previous());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $currency_id)
    {
        $validator = Validator::make($request->all(), [
            'currency' => 'required|max:100',
            'cy_code' => 'required|min:3|max:3',
            'iso_num' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#edit")
                ->withErrors($validator)
                ->withInput();
        }

        $curerncy = Currency::where('currency_id', '=', $currency_id)->firstOrFail();
        $curerncy['updated_by'] = Auth::guard('admin')->user()->name;
        $curerncy->update($request->all());

        $activity = new Activity();
        $activity['admin_id'] = Auth::guard('admin')->user()->id;
        $activity['added_by'] = Auth::guard('admin')->user()->name;
        $activity['interface'] = 'Admin';
        $activity['module'] = 'System - Curerncy';
        $activity['activity'] = 'Currency has been updated by '. Auth::guard('admin')->user()->name .'.';
        $activity->save();

        session()->flash('toastr_alert', '<b>Updated!</b> General Settings has been updated by '. Auth::guard('admin')->user()->name .'.');
        session()->flash('info', true);

        return Redirect::to(URL::previous());
    }


    /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
    public function lockunlock(Request $request, $currency_id)
    {
        $currency = Currency::where('currency_id', '=', $currency_id)->firstOrFail();
        $update = Currency::where('currency_id', '=', $currency_id)->update(['active' =>$request->active]);

        $activity = new Activity();
        $activity['admin_id'] = Auth::guard('admin')->user()->id;
        $activity['added_by'] = Auth::guard('admin')->user()->name;
        $activity['interface'] = 'Admin';
        $activity['module'] = 'System - Currency';
        if ($currency->active == '1') {
            $activity['activity'] = 'Currency '. $currency->currency  .' ('.$currency->cy_code.') has been Inactivate '. Auth::guard('admin')->user()->name .'.';
        } elseif ($currency->active == '0') {
            $activity['activity'] = 'Currency '. $currency->currency  .' ('.$currency->cy_code.') has been Activate '. Auth::guard('admin')->user()->name .'.';
        }
        $activity->save();

        if ($currency->active == '1') {
            session()->flash('toastr_alert', '<b>Inactivate!</b> Currency '. $currency->currency  .' ('.$currency->cy_code.') has been Inactivate '. Auth::guard('admin')->user()->name .'.');
            session()->flash('info', true);
        } elseif ($currency->active == '0') {
            session()->flash('toastr_alert', '<b>Activate!</b> Currency '. $currency->currency  .' ('.$currency->cy_code.') has been Activate '. Auth::guard('admin')->user()->name .'.');
            session()->flash('info', true);
        }

        return Redirect::to(URL::previous());
    }
}
