<?php

namespace App\Http\Controllers\Admin\Modules\Settings\Systems;

use Auth;
use Validator;
use App\Activity;
use App\Crossrate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class CrossrateController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['item_per_page']=$item_per_page = !empty($request->rows)?$request->rows:ROW_PER_PAGE;
        $data['item_per_page_url'] = url('admin/settings/currencies');
        $data['pg']='?'.http_build_query($request->all());

        $data['crossrates']=Crossrate::where(function ($query) use ($request) {
            //filter by keyword
            if ($search = $request->get('search')) {
                $query->orWhere('from_currency', 'like', '%' . $search . '%');
                $query->orWhere('to_currency', 'like', '%' . $search . '%');
                $query->orWhere('status', 'like', '%' . $search . '%');
            }
        })
        ->orderBy('status', 'desc')
        ->paginate($item_per_page);

        $data['pagetitle'] = 'Crossrate';
        $data['breadcrumb_level1'] = 'Settings';
        $data['breadcrumb_level2'] = '<a href="/admin/settings/crossrate">crossrate</a>';
        return view('admin/modules/settings/systems/crossrates', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'from_currency' => 'required|min:3|max:3',
            'to_currency' => 'required|min:3|max:3',
            'crossrate' => 'required|min:3|max:12',
            'status' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#add")
                ->withErrors($validator)
                ->withInput();
        }

        $curerncy = new Crossrate($request->all());
        $curerncy['added_by'] = Auth::guard('admin')->user()->name;
        $curerncy->save();

        $activity = new Activity();
        $activity['admin_id'] = Auth::guard('admin')->user()->id;
        $activity['added_by'] = Auth::guard('admin')->user()->name;
        $activity['interface'] = 'Admin';
        $activity['module'] = 'System - Crossrate';
        $activity['activity'] = 'Crossrate has been updated by '. Auth::guard('admin')->user()->name .'.';
        $activity->save();

        session()->flash('toastr_alert', '<b>Updated!</b> General Settings has been updated by '. Auth::guard('admin')->user()->name .'.');
        session()->flash('info', true);

        return Redirect::to(URL::previous());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'from_currency' => 'required|min:3|max:3',
            'to_currency' => 'required|min:3|max:3',
            'crossrate' => 'required|min:3|max:12',
            'status' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#edit".$id)
                ->withErrors($validator)
                ->withInput();
        }

        $curerncy = Crossrate::where('id', '=', $id)->firstOrFail();
        $curerncy['updated_by'] = Auth::guard('admin')->user()->name;
        $curerncy->update($request->all());

        $activity = new Activity();
        $activity['admin_id'] = Auth::guard('admin')->user()->id;
        $activity['added_by'] = Auth::guard('admin')->user()->name;
        $activity['interface'] = 'Admin';
        $activity['module'] = 'System - Crossrate';
        $activity['activity'] = 'Crossrate has been updated by '. Auth::guard('admin')->user()->name .'.';
        $activity->save();

        session()->flash('toastr_alert', '<b>Updated!</b> Crossrates has been updated by '. Auth::guard('admin')->user()->name .'.');
        session()->flash('info', true);

        return Redirect::to(URL::previous());
    }


    /**
         * Update the specified resource in storage.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  int  $id
         * @return \Illuminate\Http\Response
         */
    public function lockunlock(Request $request, $id)
    {
        $currency = Crossrate::where('id', '=', $id)->firstOrFail();
        $curerncy->update($request->all());

        $activity = new Activity();
        $activity['admin_id'] = Auth::guard('admin')->user()->id;
        $activity['added_by'] = Auth::guard('admin')->user()->name;
        $activity['interface'] = 'Admin';
        $activity['module'] = 'System - Crossrate';
        if ($currency->active == '1') {
            $activity['activity'] = 'Crossrate '. $currency->from_currency  .' - '.$currency->to_currency.' has been Inactivate '. Auth::guard('admin')->user()->name .'.';
        } elseif ($currency->active == '0') {
            $activity['activity'] = 'Crossrate '. $currency->from_currency  .' - '.$currency->to_currency.' has been Activate '. Auth::guard('admin')->user()->name .'.';
        }
        $activity->save();

        if ($currency->active == '1') {
            session()->flash('toastr_alert', '<b>Inactivate!</b> Crossrate '. $currency->from_currency  .' - '.$currency->to_currency.' has been Inactivate '. Auth::guard('admin')->user()->name .'.');
            session()->flash('info', true);
        } elseif ($currency->active == '0') {
            session()->flash('toastr_alert', '<b>Activate!</b> Crossrate '.$currency->from_currency  .' - '.$currency->to_currency.' has been Activate '. Auth::guard('admin')->user()->name .'.');
            session()->flash('info', true);
        }

        return Redirect::to(URL::previous());
    }
    /*
    **
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $currency = Crossrate::where('id', '=', $id)->firstOrFail();
        $currency->deleted_by = Auth::guard('admin')->user()->name;
        $currency->forceDelete();


        $activity = new Activity();
        $activity['admin_id'] = Auth::guard('admin')->user()->id;
        $activity['added_by'] = Auth::guard('admin')->user()->name;
        $activity['interface'] = 'Admin';
        $activity['module'] = 'Accounts';
        $activity['activity'] = 'Crossrate '. $currency->from_currency  .' - '.$currency->to_currency.' has been deleted by '. Auth::guard('admin')->user()->name .'.';

        $activity->save();


        session()->flash('toastr_alert', '<b>Deleted!</b> Crossrate '. $currency->from_currency  .' - '.$currency->to_currency.' has been deleted by '. Auth::guard('admin')->user()->name .'.');
        session()->flash('info', true);


        return Redirect::to(URL::previous());
    }
}
