<?php

namespace App\Http\Controllers\Admin\Modules\Accounts;

use DB;
use Auth;
use Mail;
use App\City;
use Validator;
use App\Account;
use App\Country;
use App\Activity;
use App\Compliance;
use App\Wallettype;
use App\Notification;
use App\Http\Helper\Common;
use Illuminate\Http\Request;
use App\Entities\AccountUser;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class AccountsController extends Controller
{
    public $common;
    public function __construct()
    {
        $this->middleware('admin');
        $this->common = new Common;
    }

    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {
        $data['item_per_page']=$item_per_page = !empty($request->rows)?$request->rows:ROW_PER_PAGE;
        $data['item_per_page_url'] = url('admin/accounts');

        $data['accounts'] = Account::leftjoin('countries', 'accounts.country', '=', 'countries.country_code')
        ->leftjoin('wallettypes', 'accounts.wallet_type_id', '=', 'wallettypes.wallet_type_id')
        ->where(function ($query) use ($request) {
            //filter by keyword
            if (($search = $request->get('search'))) {
                $query->orWhere('account_id', 'like', '%' . $search . '%');
                $query->orWhere('first_name', 'like', '%' . $search . '%');
                $query->orWhere('last_name', 'like', '%' . $search . '%');
                $query->orWhere('email', 'like', '%' . $search . '%');
                $query->orWhere('phone', 'like', '%' . $search . '%');
                $query->orWhere('compliance', 'like', '%' . $search . '%');
                $query->orWhere('status', 'like', $search . '%');
            }
        })
         ->select('accounts.*', 'countries.country_name', 'wallettypes.name as type_name')
        ->orderBy('created_at', 'desc')
        ->paginate(10);


        //$accounts = DB::select('select accounts.*, countries.country_name from accounts left join
        //countries on countries.country_id=accounts.country');
        $data['countries'] = ['0'=>'Select a Country'] + Country::pluck('country_name', 'country_name')->all();
        $data['wallettypes'] = Wallettype::pluck('name', 'wallet_type_id');

        $data['pagetitle'] = 'Accounts';
        $data['breadcrumb_level1'] = '<a href="/admin/accounts">Accounts</a>';

        $data['account_id'] = '0';
        $data['search_accounts'] = '0';

        return view('admin/modules/accounts/accounts', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|max:60',
            'last_name' => 'required|max:60',
            'email' => 'required|email|max:100',
            'phone' => 'required|max:20',
            'street' => 'required|max:100',
            'city' => 'required|max:40',
            'country' => 'required|max:20',
            'post_code' => 'required|max:20',
            'wallet_type_id' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#add")
                ->withErrors($validator)
                ->withInput();
        }

        $account_id=AccountUser::accountid();
        $password = bcrypt($request->first_name.' '.$request->last_name);

        $account = new Account($request->all());
        $account->account_id   = $account_id;
        $account->country      = $request->country;
        $account->status       = 'Active';
        $account->compliance   = 'Pending';
        $account->wallet_type_id   = $request->wallet_type_id;
        $account->name         = $request->first_name.' '.$request->last_name;
        $account->added_by     =   Auth::guard('admin')->user()->name;
        $account->save();

        $user = new AccountUser($request->all());
        $user->name      = $request->first_name.' '.$request->last_name;
        $user->password     =   $password;
        $user->account_id   =   $account_id;
        $user->is_primary   =   '1';
        $user->two_step     =   'email';
        $user->status       =   'Inactive';
        $user->added_by     =   Auth::guard('admin')->user()->name;
        $user->save();


        DB::table('bin')->insert(['user_id' => $account_id, 'field' => 'password', 'value' => $password, 'user_type' => 'account', 'date' => date('Y-m-d')]);


        $notification = new Notification($request->all());

        $notification_id=(int)$notification_id=DB::table('notifications')->orderBy('notification_id', 'desc')->value('notification_id');
        if ($notification_id < 1) {
            $notification_id=1010;
        } else {
            $notification_id = $notification_id + 1;
        }
        $notification['account_id'] = $account_id;
        $notification['notification_id'] = $notification_id;
        $notification['added_by'] = Auth::guard('admin')->user()->name;
        $notification->save();

        $activity = new Activity();
        $activity['admin_id'] = Auth::guard('admin')->user()->admin_id;
        $activity['added_by'] = $request->name;
        $activity['interface'] = 'Admin';
        $activity['module'] = 'Account';
        $activity['activity'] = 'New Account '. $request->name .' Registered';
        $activity->save();

        $code = $this->common->verification_code();
        $t_account =  AccountUser::where('account_id', $account_id)->first();
        $t_account->verify_token = $code;
        $t_account->token_time = time();
        $t_account->save();




        $data['subject'] = config('global.company').' - Account Activation';
        $data['preview_text'] = 'Just one more step ...';
        $data['main_message'] = 'Just one more step ...';
        $data['p1'] = 'Click the button below to activate your account.';
        $data['button_link'] = url('account/verify-email/'.$code);
        $data['button'] = 'Activate Account';
        $data['email'] = $request->email;
        $data['first_name'] = $request->name;
        @Mail::send('emails.app_email', $data, function ($message) use ($data) {
            $message->to($data['email'], $data['first_name'])->subject(config('global.company').' - Account Activation');
        });





        session()->flash('toastr_alert', '<b>Added!</b> New Customer <a href="/admin/accounts/'.$account_id.'">'. $account->first_name .' '. $account->last_name .'</a> has been added by <a href="/admin/usermanagement/users/'. Auth::guard('admin')->user()->id .'">'. Auth::guard('admin')->user()->name .'</a>.');
        session()->flash('success', true);

        return Redirect::to(URL::previous());
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($account_id)
    {
        $account = Account::leftjoin('wallettypes', 'accounts.wallet_type_id', '=', 'wallettypes.wallet_type_id')
                ->where('accounts.account_id', '=', $account_id)
                ->select('accounts.*', 'wallettypes.name as type_name')->first();
        $countries = ['0'=>'Select a Country'] + Country::pluck('country_name', 'country_code')->all();

        $wallettypes = Wallettype::pluck('name', 'wallet_type_id');

        $pagetitle = $account->first_name . ' ' . $account->last_name;
        $breadcrumb_level1 = '<a href="/admin/accounts">Accounts</a>';
        $breadcrumb_level2 = '<a href="/admin/accounts/'.$account_id.'">Customer</a>';

        return view('admin/modules/accounts/account', compact('account', 'countries', 'wallettypes', 'countryname', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'account_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $account_id)
    {
        $validator = Validator::make($request->all(), [
            'compliance' => 'required',
            'first_name' => 'required|max:60',
            'last_name' => 'required|max:60',
            'email' => 'required|email|max:100',
            'phone' => 'required|max:20',
            'street' => 'required|max:100',
            'city' => 'required|max:40',
            'country' => 'required|max:20',
            'post_code' => 'required|max:20',
            'wallet_type_id' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#edit")
                ->withErrors($validator)
                ->withInput();
        }

        $account = Account::where('account_id', '=', $account_id)->firstOrFail();
        $account->update($request->all());

        $activity = new Activity();
        $activity['admin_id'] = Auth::guard('admin')->user()->id;
        $activity['added_by'] = Auth::guard('admin')->user()->name;
        $activity['interface'] = 'Admin';
        $activity['module'] = 'Customer';
        $activity['activity'] = 'Customer <a href="/admin/accounts/'. $account_id .'">'. $account->first_name .' '. $account->last_name .'</a> has been updated by <a href="/admin/usermanagement/users/'. Auth::guard('admin')->user()->id .'">'. Auth::guard('admin')->user()->name .'</a>.';

        Auth::guard('admin')->user()->accounts()->save($account);
        Auth::guard('admin')->user()->activities()->save($activity);

        session()->flash('toastr_alert', '<b>Updated!</b> Customer <a href="/admin/accounts/'. $account_id .'">'. $account->first_name .' '. $account->last_name .'</a> has been updated by <a href="/admin/usermanagement/users/'. Auth::guard('admin')->user()->id .'">'. Auth::guard('admin')->user()->name .'</a>.');
        session()->flash('info', true);

        return Redirect::to(URL::previous());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function lockunlock(Request $request, $account_id)
    {
        $account = Account::where('account_id', '=', $account_id)->firstOrFail();
        $account->update($request->all());

        $activity = new Activity();
        $activity['admin_id'] = Auth::guard('admin')->user()->id;
        $activity['added_by'] = Auth::guard('admin')->user()->name;
        $activity['interface'] = 'Admin';
        $activity['module'] = 'Customer';
        if ($account->status === 'Active') {
            $activity['activity'] = 'Customer <a href="/admin/accounts/'. $account_id .'">'. $account->first_name .' '. $account->last_name .'</a> has been unlocked by <a href="/admin/usermanagement/users/'. Auth::guard('admin')->user()->id .'">'. Auth::guard('admin')->user()->name .'</a>.';
        } elseif ($account->status === 'Locked') {
            $activity['activity'] = 'Customer <a href="/admin/accounts/'. $account_id .'">'. $account->first_name .' '. $account->last_name .'</a> has been locked by <a href="/admin/usermanagement/users/'. Auth::guard('admin')->user()->id .'">'. Auth::guard('admin')->user()->name .'</a>.';
        }

        Auth::guard('admin')->user()->accounts()->save($account);
        Auth::guard('admin')->user()->activities()->save($activity);

        if ($account->status === 'Active') {
            session()->flash('toastr_alert', '<b>Unlocked!</b> Customer <a href="/admin/accounts/'. $account_id .'">'. $account->first_name .' '. $account->last_name .'</a> has been unlocked by <a href="/admin/usermanagement/users/'. Auth::guard('admin')->user()->id .'">'. Auth::guard('admin')->user()->name .'</a>.');
            session()->flash('info', true);
        } elseif ($account->status === 'Locked') {
            session()->flash('toastr_alert', '<b>Locked!</b> Customer <a href="/admin/accounts/'. $account_id .'">'. $account->first_name .' '. $account->last_name .'</a> has been locked by <a href="/admin/usermanagement/users/'. Auth::guard('admin')->user()->id .'">'. Auth::guard('admin')->user()->name .'</a>.');
            session()->flash('info', true);
        }

        return Redirect::to(URL::previous());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
