<?php

namespace App\Http\Controllers\Admin\Modules\Accounts;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Khill\Lavacharts\Lavacharts;

use DB;
use Lava;
use Validator;
use Carbon\Carbon;
use App\Account;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class StatisticsController extends Controller
{   
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $accountcount = Account::count();
        $todayaccountcount = Account::select(DB::raw('*'))
                  ->whereRaw('Date(created_at) = CURDATE()')->count();
        $accountcountrycount = Account::count(DB::raw('DISTINCT country'));
        $startDate = Carbon::now()->subDays(30);
        $days = Input::get('days', 30);
        $range = Carbon::now()->subDays('days', 30);
        $stats = Account::where('created_at', '>=', $range)
                ->groupBy('date')
                ->orderBy('date', 'DESC')
                ->get([
                    DB::raw('Date(created_at) as date'),
                    DB::raw('COUNT(*) as value')
                ]);
        
        $pagetitle = 'Accounts Statistics';
        $breadcrumb_level1 = '<a href="/admin/accounts">Accounts</a>';
        $breadcrumb_level2 = '<a href="/admin/accountsstatistics">Statistics</a>';
        $account_id = '0';

        $chart = Lava::DataTable();

        $chart->addDateColumn('Day')
              ->addNumberColumn('Number of Accounts per day in last 30 days')
              ->addRow(['Jun1', 100])
              ->addRow(['Jun2', 117])
              ->addRow(['Jun3', 66])
              ->addRow(['Jun4', 103])
              ->addRow(['Jun5', 100])
              ->addRow(['Jun6', 117])
              ->addRow(['Jun7', 66])
              ->addRow(['Jun8', 103])
              ->addRow(['Jun9', 100])
              ->addRow(['Jun10', 117])
              ->addRow(['Jun11', 66])
              ->addRow(['Jun12', 103])
              ->addRow(['Jun13', 100])
              ->addRow(['Jun14', 117])
              ->addRow(['Jun15', 66])
              ->addRow(['Jun16', 103])
              ->addRow(['Jun17', 100])
              ->addRow(['Jun18', 117])
              ->addRow(['Jun19', 66])
              ->addRow(['Jun20', 103])
              ->addRow(['Jun21', 100])
              ->addRow(['Jun22', 117])
              ->addRow(['Jun23', 66])
              ->addRow(['Jun24', 103])
              ->addRow(['Jun25', 100])
              ->addRow(['Jun26', 117])
              ->addRow(['Jun27', 66])
              ->addRow(['Jun28', 103])
              ->addRow(['Jun29', 66])
              ->addRow(['Jun30', 66]);

        Lava::ColumnChart('Chart', $chart, [
            'legend'             => 'top',
            'height'             => 400,
        ]);
        
        return view('admin/modules/accounts/statistics', compact('pagetitle', 'accountcount', 'todayaccountcount', 'accountcountrycount', 'stats', 'accountcompliances', 'breadcrumb_level1', 'breadcrumb_level2', 'account_id'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }
    
    public function store(Request $request, $id)
    {
    
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    
    }
}
