<?php

namespace App\Http\Controllers\Admin\Modules\Accounts;

use Auth;
use App\Role;
use Validator;
use App\Activity;
use App\Account;
use App\Role_user;
use App\Http\Helper\Common;
use Illuminate\Http\Request;
use App\Entities\AccountUser;
use App\Libraries\EncryptDecrypt;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class AccountUsersController extends Controller
{
    protected $common;
    protected $encdec;

    public function __construct()
    {
        $this->middleware('admin');
        $this->common = new Common;
        $this->encdec = new EncryptDecrypt('paypies');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $account_id)
    {
        $data['item_per_page'] = $item_per_page = !empty($request->rows)?$request->rows:ROW_PER_PAGE;


        $data['accounts'] =  $accounts = AccountUser::where('account_id', '=', $account_id)
            ->where(function ($query) use ($request) {
                //filter by keyword
                if (($search = $request->get('search'))) {
                    $query->orWhere('first_name', 'like', '%' . $search . '%');
                    $query->orWhere('last_name', 'like', '%' . $search . '%');
                    $query->orWhere('name', 'like', '%' . $search . '%');
                    $query->orWhere('email', 'like', '%' . $search . '%');
                    $query->orWhere('status', 'like', $search . '%');
                }
            })
        ->orderBy('created_at', 'desc')
        ->paginate($data['item_per_page']);

        $data['roles'] = Role::pluck('display_name', 'id')->toArray();



        $data['pagetitle'] = $accounts[0]->name??'Company Name';
        $data['account_id'] = $account_id;
       

        $data['subpagetitle'] = 'Users';
        $data['breadcrumb_level1'] = '<a href="/admin/accounts">Accounts</a>';
        $data['breadcrumb_level2'] = '<a href="/admin/accounts/'.$account_id.'">Account</a>';
        $data['breadcrumb_level3'] = '<a href="/admin/accounts/'.$account_id.'/users">User Management</a>';
        $data['breadcrumb_level4'] = '<a href="/admin/accounts/'.$account_id.'/users">Users</a>';
        return view('admin/modules/accounts/users', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $account_id)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:account_users',
            //'password' => 'required|confirmed|min:6',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#add")
                ->withErrors($validator)
                ->withInput();
        }

        $mUser = new AccountUser($request->all());



        $code = $this->encdec->makeRandomPassword2(50);
        $mUser['account_id'] = $account_id;
        $mUser['verify_token'] = $code;
        $mUser['token_time'] = time();
        $mUser['name'] = $request->last_name;
        $mUser['is_primary'] = 0;
        $mUser['password'] = $this->common->crypto_encrypt($code, 'key_account');
        $mUser['status'] = 'Inactive';
        $mUser['added_by'] = Auth::guard('admin')->user()->name;
        $mUser->save();
        /*
                $role_user = new Role_user;
                $role_user->user_id = $account_id;
                $role_user->role_id = $request->role??'admin';
                $role_user->added_by = Auth::guard('admin')->user()->name;
                $role_user->provider = 'account';
                $role_user->save();
        */


        $data['subject'] = config('global.company').' - Account Activation';
        $data['preview_text'] = 'Just one more step ...';
        $data['main_message'] = 'Just one more step ...';
        $data['p1'] = 'Click the button below to activate your account.';
        $data['button_link'] = url('account/verify-email/'.$code);
        $data['button'] = 'Activate Account';
        $data['email'] = $request->email;
        $data['first_name'] = $request->name??$request->email;
        @Mail::send('emails.app_email', $data, function ($message) use ($data) {
            $message->to($data['email'], $data['first_name'])->subject(config('global.company') .' - Account Activation');
        });

        $activity = new Activity();
        $activity['account_id'] = Auth::guard('admin')->user()->account_id;
        $activity['added_by'] = Auth::guard('admin')->user()->name;
        $activity['interface'] = 'admin';
        $activity['module'] = 'Usermanagement';
        $activity['activity'] = 'New Account '.$request->first_name.' '.$request->last_name.' has been added by '. Auth::guard('admin')->user()->name .'.';
        $activity->save();

        session()->flash('toastr_alert', '<b>Added!</b> New Account '.$request->first_name.' '.$request->last_name.' has been added by '. Auth::guard('admin')->user()->name .'.');
        session()->flash('success', true);

        return Redirect::to(URL::previous());
    }

    public function password_reset_link_send($user)
    {
        $data['first_name'] = $user->first_name;

        $token = str_random(100); // Generate random string values - limit 100

        $password_resets = new PasswordResets;

        $password_resets->email      = $user->email;
        $password_resets->token      = $data['token'];
        $password_resets->created_at = date('Y-m-d H:i:s');

        $password_resets->save(); // Insert a generated token and email in password_resets table

        $data['subject'] = config('global.company'). ' - Set Password';
        $data['preview_text'] = 'Set your account password';
        $data['main_message'] = 'Set your account password';
        $data['p1'] = 'Click the button below to set your account password.';
        $data['button_link'] = url('account/set-password/'.$code);
        $data['button'] = 'Set Password';
        $data['email'] = $user->email;
        $data['first_name'] = $user->first_name;
        @Mail::send('emails.app_email', $data, function ($message) use ($data) {
            $message->to($data['email'], $data['first_name'])->subject(config('global.company'). ' - Set Password');
        });

        return true;
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($account_id, $id)
    {
        $data['account'] = $account =  AccountUser::where(['id' => $id, 'account_id' => $account_id])->first();
        $data['roles'] = Role::pluck('display_name', 'id')->toArray();

        $data['pagetitle'] = $account->name??'';
        $data['account_id'] = $account_id;
        $data['terminal_id'] = 0;

        $data['subpagetitle'] = 'Users';
        $data['breadcrumb_level1'] = '<a href="/admin/accounts">Accounts</a>';
        $data['breadcrumb_level2'] = '<a href="/admin/accounts/'.$account_id.'">Account</a>';
        $data['breadcrumb_level3'] = '<a href="/admin/accounts/'.$account_id.'/users">User Management</a>';
        $data['breadcrumb_level4'] = '<a href="/admin/accounts/'.$account_id.'/users">Users</a>';

        return view('admin/modules/accounts/user', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $account_id, $id)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'is_primary' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#edit")
                ->withErrors($validator)
                ->withInput();
        }

        $account = AccountUser::where(['id' => $id, 'account_id' => $account_id])->firstOrFail();

        if ($request->is_primary == '0') {
            $primaryCheck = AccountUser::where(['account_id'=> $account->account_id, 'is_primary' => '1'])->where('id', '<>', $id)->first();
            if (count($primaryCheck) == 0) {
                $validator->errors()->add('is_primary', 'One active User should be primary!');
                return Redirect::to(URL::previous() . "#edit")
                ->withErrors($validator)
                ->withInput();
            }
        }


        if ($request->is_primary == '1') {
            if ($account->status == 'Inactive') {
                $validator->errors()->add('is_primary', 'Only Active user are allow to set primary!');
                return Redirect::to(URL::previous() . "#edit")
                ->withErrors($validator)
                ->withInput();
            }
            $update =  AccountUser::where('account_id', '=', $account->account_id)->update(['is_primary' => '0']);
        }

        $account = AccountUser::where(['id' => $id, 'account_id' => $account_id])->firstOrFail();
        $account->name = $request->last_name;
        $account->is_primary = $request->is_primary;
        $account->update($request->all());

        session()->flash('toastr_alert', '<b>Updated!</b> Account User '.$request->first_name.' '.$request->last_name.' has been Updated by '. Auth::guard('admin')->user()->name .'.');
        session()->flash('info', true);

        return Redirect::to(URL::previous());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function lockunlock(Request $request, $account_id, $id)
    {
        $account = AccountUser::where(['id' => $id, 'account_id' => $account_id])->firstOrFail();
        $account->status = $request->status;
        $account->bad_login = 0;
        $account->updated_by = Auth::guard('admin')->user()->name;
        $account->update($request->all());


        $activity = new Activity();
        $activity['account_id'] = Auth::guard('admin')->user()->account_id;
        $activity['added_by'] = Auth::guard('admin')->user()->name;
        $activity['interface'] = 'Admin';
        $activity['module'] = 'Accounts';
        if ($request->status === 'Active') {
            $activity['activity'] = 'Account '. $account->first_name .' '. $account->last_name .' has been unlocked by '. Auth::guard('admin')->user()->name .'.';
        } elseif ($request->status === 'Locked') {
            $activity['activity'] = 'Account '. $account->first_name .' '. $account->last_name .' has been locked by '. Auth::guard('admin')->user()->name .'.';
        }
        $activity->save();

        if ($request->status === 'Active') {
            session()->flash('toastr_alert', '<b>Unlocked!</b> Account '. $account->first_name .' '. $account->last_name .' has been unlocked by '. Auth::guard('admin')->user()->name .'.');
            session()->flash('info', true);
        } elseif ($request->status === 'Locked') {
            session()->flash('toastr_alert', '<b>Locked!</b> Account '. $account->first_name .' '. $account->last_name .' has been locked by '. Auth::guard('admin')->user()->name .'.');
            session()->flash('info', true);
        }

        return Redirect::to(URL::previous());
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($account_id, $id)
    {
        $account = AccountUser::where(['id' => $id, 'account_id' => $account_id])->firstOrFail();
        $account->deleted_by = Auth::guard('admin')->user()->name;
        $account->forceDelete();


        $activity = new Activity();
        $activity['account_id'] = Auth::guard('admin')->user()->account_id;
        $activity['added_by'] = Auth::guard('admin')->user()->name;
        $activity['interface'] = 'Admin';
        $activity['module'] = 'Accounts';
        $activity['activity'] = 'Account '. $account->first_name .' '. $account->last_name .' has been deleted by '. Auth::guard('admin')->user()->name .'.';

        $activity->save();


        session()->flash('toastr_alert', '<b>Deleted!</b> Account '. $account->first_name .' '. $account->last_name .' has been deleted by '. Auth::guard('admin')->user()->name .'.');
        session()->flash('info', true);


        return Redirect::to(URL::previous());
    }
}
