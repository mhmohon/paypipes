<?php

namespace App\Http\Controllers\Admin\Modules\Accounts;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Input;

use DB;
use Validator;
use App\Compliance;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ComplianceController extends Controller
{   
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $compliances = DB::table('compliances')
            ->join('compliance_switches', 'compliances.compliance_id', '=', 'compliance_switches.compliance_id')    
			->where('compliance_switches.account_id','!=','""')	
            ->select('compliances.*','compliance_switches.account_id')
            ->paginate(10);

        $pagetitle = 'Accounts Compliance';
        $breadcrumb_level1 = '<a href="/admin/accounts">Accounts</a>';
        $breadcrumb_level2 = '<a href="/admin/accountscompliance">Compliance</a>';
        
        $account_id = '0';
        
        return view('admin/modules/accounts/compliance', compact('pagetitle', 'compliances', 'compliance_switches', 'breadcrumb_level1', 'breadcrumb_level2', 'account_id'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }
    public function store()
    {
       
    }
	/*
    public function storeX(Request $request, $id)
    {
		  // getting all of the post data
		 $file = array('upload_file' => Input::file('upload_file'));
  
		  $validator = Validator::make($request->all(), [
            'name' => 'required|max:60',
            'description' => 'required|max:100',
			'upload_file' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#add")
                ->withErrors($validator)
                ->withInput();
        }
        
		echo "hello 3";
			die();
		
		
		
		// checking file is valid.
		if (Input::file('upload_file')->isValid()) {
			
		  $destinationPath = 'uploads/compliance'; // upload path
		  $extension = Input::file('image')->getClientOriginalExtension(); // getting image extension
		  $fileName = rand(11111,99999).'.'.$extension; // renameing image
		  Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
		  // sending back with message
		  Session::flash('success', 'Upload successfully'); 
		  return Redirect::to('upload');
		}
		else {
			
			echo "hello 2";
			die();
			
		  // sending back with error message.
		  Session::flash('error', 'uploaded file is not valid');
		  return Redirect::to('upload');
		}
	  
		
       	$compliance = new Compliance($request->all());
		$compliance['compliance_id'] = rand(11111,99999);
		$compliance['file_name'] = $fileName;
		$compliance['file_path'] = $destinationPath.$fileName;
		$compliance['status'] = 'Inactive';
		$compliance['added_by'] = Auth::user()->name;	
		
		$account = new Customer($request->all());
		
        $activity_log = new Activity_log ();
        $activity_log['user_id'] = Auth::user()->id;
		$activity_log['added_by'] = Auth::user()->name;
		$activity_log['interface'] = 'Admin';
        $activity_log['module'] = 'Customer';
        $activity_log['description'] = 'New Customer '. $account->first_name .' '. $account->last_name .' has been added by <a href="/admin/usermanagement/users/'. Auth::user()->id .'">'. Auth::user()->name .'</a>.';
        
        Auth::user()->compliance()->save($compliance);
        Auth::user()->activity_logs()->save($activity_log);
        
        Toastr::success('Added!','New Document Compliance '. $account->first_name .' '. $account->last_name .' has been added by <a href="/admin/usermanagement/users/'. Auth::user()->id .'">'. Auth::user()->name .'</a>.')->push();
		
        return redirect ('admin/accounts');
		
    }
    */
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    
    }
}
