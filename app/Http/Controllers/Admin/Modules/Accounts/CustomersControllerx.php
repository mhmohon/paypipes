<?php

namespace App\Http\Controllers\Admin\Modules\Accounts;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

use Mail;
use DB;
use Validator;
use Auth;
use App\Activity;
use App\Notification;
use App\Account;
use App\Country;
use App\City;
use App\Compliance;
use App\Wallettype;
use App\Http\Controllers\Controller;
use App\Http\Helper\Common;
use App\Libraries\EncryptDecrypt;

class AccountsControllerx extends Controller
{
    protected $common;
    protected $encdec;
    public function __construct()
    {
        $this->middleware('admin');
        $this->common = new Common;
        $this->encdec = new EncryptDecrypt('paypies');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $item_per_page = ROW_PER_PAGE;
        if (isset($_GET['rows'])) {
            $item_per_page = $request->get('rows');
        }

        $accounts = Account::where(function ($query) use ($request) {
            //filter by keyword
            if (($search = $request->get('search'))) {
                $query->orWhere('account_id', 'like', '%' . $search . '%');
                $query->orWhere('first_name', 'like', '%' . $search . '%');
                $query->orWhere('last_name', 'like', '%' . $search . '%');
                $query->orWhere('email', 'like', '%' . $search . '%');
                $query->orWhere('phone', 'like', '%' . $search . '%');
                $query->orWhere('compliance', 'like', '%' . $search . '%');
                $query->orWhere('status', 'like', $search . '%');
            }
        })
        ->orderBy('created_at', 'desc')
        ->paginate($item_per_page);

        //$accounts = DB::select('select accounts.*, countries.country_name from accounts left join
        //countries on countries.country_id=accounts.country');
        $countries = ['0'=>'Select a Country'] + Country::pluck('country_name', 'country_code')->all();

        $pagetitle = 'Accounts';
        $breadcrumb_level1 = '<a href="/admin/account">Accounts</a>';

        $account_id = '0';
        $search_accounts = '0';
        $item_per_page_url = url('admin/accounts');

        return view('admin/modules/accounts/accounts', compact('accounts', 'countries', 'pagetitle', 'breadcrumb_level1', 'account_id', 'search_accounts', 'item_per_page', 'item_per_page_url'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|max:60',
            'last_name' => 'required|max:60',
            'email' => 'required|email|max:100|unique:accounts',
            'phone' => 'required|max:20',
            'street' => 'required|max:100',
            'city' => 'required|max:40',
            'country' => 'required|max:20',
            'post_code' => 'required|max:20',
            //'password' => 'required|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#add")
                ->withErrors($validator)
                ->withInput();
        }

        $account = new Customer($request->all());

        $account_id=(int)$account_id=Account::orderBy('account_id', 'desc')->value('account_id');
        if ($account_id < 1) {
            $account_id=1010;
        } else {
            $account_id = $account_id + 1;
        }

        $code = $this->common->verification_code();
        $account['account_id'] = $account_id;
        $account['verify_token'] = $code;
        $account['token_time'] = time();
        $account['name'] = $request->first_name.' '.$request->last_name;
        $account['password'] = $this->common->crypto_encrypt($code, 'key_account');
        $account['status'] = 'Inactive';
        $account['compliance'] = 'Not Required';
        $account['added_by'] = Auth::guard('admin')->user()->name;
        $account->save();

        $notification = new Notification($request->all());

        $notification_id=(int)$notification_id=DB::table('notifications')->orderBy('notification_id', 'desc')->value('notification_id');
        if ($notification_id < 1) {
            $notification_id=1010;
        } else {
            $notification_id = $notification_id + 1;
        }

        $notification['notification_id'] = $notification_id;
        $notification['added_by'] = Auth::guard('admin')->user()->name;
        $notification->save();

        $data['subject'] = 'PayPipes - Account Activation';
        $data['preview_text'] = 'Just one more step ...';
        $data['main_message'] = 'Just one more step ...';
        $data['p1'] = 'Click the button below to activate your account.';
        $data['button_link'] = url('account/verify-email/'.$code);
        $data['button'] = 'Activate Account';
        $data['email'] = $request->email;
        $data['first_name'] = $request->name;
        @Mail::send('emails.app_email', $data, function ($message) use ($data) {
            $message->to($data['email'], $data['first_name'])->subject('PayPipes - Account Activation');
        });

        $activity = new Activity();
        $activity['admin_id'] = Auth::guard('admin')->user()->admin_id;
        $activity['added_by'] = Auth::guard('admin')->user()->name;
        $activity['interface'] = 'Admin';
        $activity['module'] = 'Customer';
        $activity['activity'] = 'New Customer <a href="/admin/accounts/'.$account_id.'">'. $account->first_name .' '. $account->last_name .'</a> has been added by <a href="/admin/usermanagement/users/'. Auth::guard('admin')->user()->id .'">'. Auth::guard('admin')->user()->name .'</a>.';
        $activity->save();


        session()->flash('toastr_alert', '<b>Added!</b> New Customer <a href="/admin/accounts/'.$account_id.'">'. $account->first_name .' '. $account->last_name .'</a> has been added by <a href="/admin/usermanagement/users/'. Auth::guard('admin')->user()->id .'">'. Auth::guard('admin')->user()->name .'</a>.');
        session()->flash('success', true);

        return Redirect::to(URL::previous());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($account_id)
    {
        $account = Account::where('account_id', '=', $account_id)->first();

        $countries = Country::pluck('country_name', 'country_code');
        $wallettypes = Wallettype::pluck('name', 'wallet_type_id');

        $pagetitle = $account->first_name . ' ' . $account->last_name;
        $breadcrumb_level1 = '<a href="/admin/accounts">Accounts</a>';
        $breadcrumb_level2 = '<a href="/admin/accounts/'.$account_id.'">Customer</a>';

        return view('admin/modules/accounts/account', compact('account', 'countries', 'wallettypes', 'countryname', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'account_id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response

    public function edit($account_id)
    {
        $account = Account::where('account_id', '=', $account_id)->first();

        $countries = Country::pluck('country_name', 'country_name');
        $wallettypes = Wallettype::pluck('name', 'wallet_type_id');

        $pagetitle = $account->first_name . ' ' . $account->last_name;
        $breadcrumb_level1 = '<a href="/admin/accounts">Accounts</a>';
        $breadcrumb_level2 = '<a href="/admin/accounts/'.$account_id.'">Customer</a>';

        return view('admin/modules/accounts/account', compact('account','countries', 'wallettypes', 'countryname', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'account_id' ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $account_id)
    {
        $validator = Validator::make($request->all(), [
            'compliance' => 'required',
            'first_name' => 'required|max:60',
            'last_name' => 'required|max:60',
            'email' => 'required|email|max:100',
            'phone' => 'required|max:20',
            'street' => 'required|max:100',
            'city' => 'required|max:40',
            'country' => 'required|max:20',
            'post_code' => 'required|max:20',
            'wallet_type_id' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#edit")
                ->withErrors($validator)
                ->withInput();
        }

        $account = Account::where('account_id', '=', $account_id)->firstOrFail();
        $account->update($request->all());

        $activity = new Activity();
        $activity['admin_id'] = Auth::guard('admin')->user()->id;
        $activity['added_by'] = Auth::guard('admin')->user()->name;
        $activity['interface'] = 'Admin';
        $activity['module'] = 'Customer';
        $activity['activity'] = 'Customer <a href="/admin/accounts/'. $account_id .'">'. $account->first_name .' '. $account->last_name .'</a> has been updated by <a href="/admin/usermanagement/users/'. Auth::guard('admin')->user()->id .'">'. Auth::guard('admin')->user()->name .'</a>.';

        $activity->save();


        session()->flash('toastr_alert', '<b>Updated!</b> Customer <a href="/admin/accounts/'. $account_id .'">'. $account->first_name .' '. $account->last_name .'</a> has been updated by <a href="/admin/usermanagement/users/'. Auth::guard('admin')->user()->id .'">'. Auth::guard('admin')->user()->name .'</a>.');
        session()->flash('info', true);

        return Redirect::to(URL::previous());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function lockunlock(Request $request, $account_id)
    {
        $account = Account::where('account_id', '=', $account_id)->firstOrFail();
        $account->update($request->all());

        $activity = new Activity();
        $activity['admin_id'] = Auth::guard('admin')->user()->id;
        $activity['added_by'] = Auth::guard('admin')->user()->name;
        $activity['interface'] = 'Admin';
        $activity['module'] = 'Customer';
        if ($account->status === 'Active') {
            $activity['activity'] = 'Customer <a href="/admin/accounts/'. $account_id .'">'. $account->first_name .' '. $account->last_name .'</a> has been unlocked by <a href="/admin/usermanagement/users/'. Auth::guard('admin')->user()->id .'">'. Auth::guard('admin')->user()->name .'</a>.';
        } elseif ($account->status === 'Locked') {
            $activity['activity'] = 'Customer <a href="/admin/accounts/'. $account_id .'">'. $account->first_name .' '. $account->last_name .'</a> has been locked by <a href="/admin/usermanagement/users/'. Auth::guard('admin')->user()->id .'">'. Auth::guard('admin')->user()->name .'</a>.';
        }
        $activity->save();


        if ($account->status === 'Active') {
            session()->flash('toastr_alert', '<b>Unlocked!</b> Customer <a href="/admin/accounts/'. $account_id .'">'. $account->first_name .' '. $account->last_name .'</a> has been unlocked by <a href="/admin/usermanagement/users/'. Auth::guard('admin')->user()->id .'">'. Auth::guard('admin')->user()->name .'</a>.');
            session()->flash('info', true);
        } elseif ($account->status === 'Locked') {
            session()->flash('toastr_alert', '<b>Locked!</b> Customer <a href="/admin/accounts/'. $account_id .'">'. $account->first_name .' '. $account->last_name .'</a> has been locked by <a href="/admin/usermanagement/users/'. Auth::guard('admin')->user()->id .'">'. Auth::guard('admin')->user()->name .'</a>.');
            session()->flash('info', true);
        }

        return Redirect::to(URL::previous());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
    }

    public function pdf_convert(Request $request)
    {
        $skip = isset($request->page)?($request->page-1)*ROW_PER_PAGE:0;
        $take = ROW_PER_PAGE;
        $pagetitle = 'Accounts';
        $queryResult = Account::skip($skip)->take($take)->get()->toArray();
        $fields = ['account_id', 'first_name', 'last_name', 'email', 'compliance', 'status'];
        $headers = ['Customer ID', 'First Name', 'Last Name', 'Email', 'Compliance', 'Status'];
        $pdf_name = 'Accounts_'.time();
        return $this->common->dataToPdf($queryResult, $fields, $pdf_name, $headers, $pagetitle);
    }

    public function csv_convert(Request $request)
    {
        $skip = isset($request->page)?($request->page-1)*ROW_PER_PAGE:0;
        $take = ROW_PER_PAGE;
        $queryResult = Account::skip($skip)->take($take)->get()->toArray();
        $fields = ['account_id', 'first_name', 'last_name', 'email', 'compliance', 'status'];
        $headers = ['Customer ID', 'First Name', 'Last Name', 'Email', 'Compliance', 'Status'];
        $csv_name = 'Accounts_'.time();
        $this->common->dataToCSV($queryResult, $fields, $csv_name, $headers);
    }

    public function xml_convert(Request $request)
    {
        $skip = isset($request->page)?($request->page-1)*ROW_PER_PAGE:0;
        $take = ROW_PER_PAGE;
        $queryResult = Account::skip($skip)->take($take)->get()->toArray();
        $fields = ['account_id', 'first_name', 'last_name', 'email', 'compliance', 'status'];
        $rootElementName = 'accounts';
        $childElementName = 'account';

        $filename = $rootElementName.'.xml';
        header('Content-type: text/xml');
        header('Content-Disposition: attachment; filename="'.$filename.'"');

        echo $this->common->sqlToXml($fields, $queryResult, $rootElementName, $childElementName);
    }
}
