<?php

namespace App\Http\Controllers\Admin\Modules\Accounts;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Input;

use DB;
use Auth; 
use Validator;
use App\Activity;
use App\Account;
use App\Compliance;
use App\Compliance_switch;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AccountComplianceController extends Controller
{   
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($account_id, $compliance_id)
    {
        $account = Account::where('account_id', '=', $account_id)->first();
		$compliances = DB::table('compliances')
            ->join('compliance_switches', 'compliances.compliance_id', '=', 'compliance_switches.compliance_id')
            ->where('compliance_switches.account_id', '=', $account->account_id)
            ->select('compliances.*')
			->orderBy('created_at','desc')
            ->paginate(10);
	
        $pagetitle = $account->first_name . ' ' . $account->last_name;
        $breadcrumb_level1 = '<a href="/admin/accounts">Accounts</a>';
        $breadcrumb_level2 = '<a href="/admin/accounts/'.$account_id.'">Customer</a>';
        $breadcrumb_level3 = '<a href="/admin/accounts/'.$account_id.'/accountcompliance">Compliance</a>';
        
        return view('admin/modules/accounts/accountcompliances', compact('pagetitle', 'account', 'compliances', 'breadcrumb_level1', 'breadcrumb_level2', 'breadcrumb_level3', 'account_id', 'compliance_id'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($account_id)
    {
        $account = Account::where('account_id', '=', $account_id)->first();
		$compliances = DB::table('compliances')
            ->join('compliance_switches', 'compliances.compliance_id', '=', 'compliance_switches.compliance_id')
            ->where('compliance_switches.account_id', '=', $account->account_id)
            ->select('compliances.*')
			->orderBy('created_at','desc')
            ->paginate(10);
	
        $pagetitle = $account->first_name . ' ' . $account->last_name;
        $breadcrumb_level1 = '<a href="/admin/accounts">Accounts</a>';
        $breadcrumb_level2 = '<a href="/admin/accounts/'.$account_id.'">Customer</a>';
        $breadcrumb_level3 = '<a href="/admin/accounts/'.$account_id.'/accountcompliance">Compliance</a>';
        
        return view('admin/modules/accounts/accountcompliances', compact('pagetitle', 'account', 'compliances', 'breadcrumb_level1', 'breadcrumb_level2', 'breadcrumb_level3', 'account_id', 'compliance_id'));
    }
	
	public function store(Request $request, $account_id)
    {
		// getting all of the post data
		$file = array('upload_file' => Input::file('upload_file'));
 
		$validator = Validator::make($request->all(), [
            'compliance_name' => 'required|max:60',
            'description' => 'required|max:100',
			'upload_file' => 'required',
			'status' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#add")
                ->withErrors($validator)
                ->withInput();
        }
			
		$account = Account::where('account_id', '=', $account_id)->firstOrFail();
		
		// checking file is valid.
		if (Input::file('upload_file')->isValid()) {
			
		$destinationPath = 'uploads/compliance'; // upload path
		$extension = Input::file('upload_file')->getClientOriginalExtension(); // getting image extension
		$fileName = rand(11111,99999).'.'.$extension; // renameing image
		Input::file('upload_file')->move($destinationPath, $fileName); // uploading file to given path
		// sending back with message
		// Session::flash('success', 'Upload successfully'); 
		// return Redirect::to('admin/accounts');
		}
		else {
			
		// sending back with error message.
		// Session::flash('error', 'uploaded file is not valid');
		// return Redirect::to('admin/accounts');
		  
		}
		$compliance_id=rand(11111,99999);
		$compliance_switch = new Compliance_switch();
		$compliance_switch['compliance_id'] = $compliance_id;
		$compliance_switch['account_id'] = $account->account_id;
		$compliance_switch['added_by'] = Auth::guard('admin')->user()->name;
		$compliance_switch->save();
		
       	$compliance = new Compliance($request->all());
		$compliance['compliance_id'] = $compliance_id;
		$compliance['name'] = $request->compliance_name;
		$compliance['file_name'] = $fileName;
		$compliance['file_path'] = $destinationPath.'/'.$fileName;
		$compliance['file_type'] = $extension;
		$compliance['added_by'] = Auth::guard('admin')->user()->name;
		$compliance->save();
		
        $activity = new Activity ();
        $activity['admin_id'] = Auth::guard('admin')->user()->id;
		$activity['added_by'] = Auth::guard('admin')->user()->name;
		$activity['interface'] = 'Admin';
        $activity['module'] = 'Customer';
        $activity['activity'] = 'New Compliance document '. $compliance->name .' has been added by '. Auth::guard('admin')->user()->name .'.';
        $activity->save();
		
        session()->flash('toastr_alert','<b>Added!</b> New Compliance document '. $compliance->name .' has been added by '. Auth::guard('admin')->user()->name .'.');
		session()->flash('success', true);
        
        return Redirect::to(URL::previous());
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($account_id, $compliance_id)
    {
        $account = Account::where('account_id', '=', $account_id)->first();
        $compliance = DB::table('compliances')->where('compliance_id', '=', $compliance_id)->first();
        $pagetitle = $account->first_name . ' ' . $account->last_name;
        $subpagetitle = $compliance->name;
        $breadcrumb_level1 = '<a href="/admin/accounts">Customer</a>';
        $breadcrumb_level2 = '<a href="/admin/accounts/'.$account_id.'">Customer</a>';
        $breadcrumb_level3 = '<a href="/admin/accounts/'.$account_id.'/accountcompliances">Compliance</a>';
        $breadcrumb_level4 = '<a href="/admin/accounts/'.$account_id.'/accountcompliances/'.$compliance_id.'">'.$compliance->name.'</a>';
        
        return view('admin/modules/accounts/accountcompliance', compact('account', 'compliance', 'pagetitle', 'subpagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'breadcrumb_level3', 'breadcrumb_level4', 'account_id', 'compliance_id'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($account_id, $compliance_id)
    {
        $account = Account::where('account_id', '=', $account_id)->first();
        $compliance = DB::table('compliances')->where('compliance_id', '=', $compliance_id)->first();
        $pagetitle = $account->first_name . ' ' . $account->last_name;
        $subpagetitle = $compliance->name;
        $breadcrumb_level1 = '<a href="/admin/accounts">Customer</a>';
        $breadcrumb_level2 = '<a href="/admin/accounts/'.$account_id.'">Customer</a>';
        $breadcrumb_level3 = '<a href="/admin/accounts/'.$account_id.'/accountcompliances">Compliance</a>';
        $breadcrumb_level4 = '<a href="/admin/accounts/'.$account_id.'/accountcompliances/'.$compliance_id.'">'.$compliance->name.'</a>';
        
        return view('admin/modules/accounts/accountcompliance', compact('account', 'compliance', 'pagetitle', 'subpagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'breadcrumb_level3', 'breadcrumb_level4', 'account_id', 'compliance_id'));
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $account_id, $compliance_id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'status' => 'required',
			'description' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#edit")
                ->withErrors($validator)
                ->withInput();
        }
		
		$compliance = Compliance::where('compliance_id', '=', $compliance_id)->firstOrFail();
        $compliance->update($request->all());
		$compliance->save();

        $activity = new Activity ();
        $activity['admin_id'] = Auth::guard('admin')->user()->admin_id;
		$activity['added_by'] = Auth::guard('admin')->user()->name;
		$activity['interface'] = 'Admin';
        $activity['module'] = 'Customer';
        $activity['activity'] = 'Compliance document '. $compliance->name .' has been updated by '. Auth::guard('admin')->user()->name .'.';
		$activity->save();
		
		session()->flash('toastr_alert','<b>Updated!</b> Compliance document '. $compliance->name .' has been updated by '. Auth::guard('admin')->user()->name .'.');
		session()->flash('info', true);
		
        return Redirect::to(URL::previous());
    }
}
