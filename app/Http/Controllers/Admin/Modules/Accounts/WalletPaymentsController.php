<?php

namespace App\Http\Controllers\Admin\Modules\Accounts;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

use DB;
use Input;
use Validator;
use App\Account;
use App\Wallet;
use App\Payment;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class WalletPaymentsController extends Controller
{   
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($account_id, $wallet_id)
    {
        $account = Account::where('account_id', '=', $account_id)->first();
		$wallet = DB::table('wallets')->where('wallet_id', '=', $wallet_id)->first();
        $payments = DB::table('payments')->where('wallet_id', [$wallet_id])->orderBy('created_at','desc')->paginate(5);
        $pagetitle = $account->first_name . ' ' . $account->last_name;
        $subpagetitle = '+Wallet ('.$wallet->currency.')';
        $breadcrumb_level1 = '<a href="/accounts">Accounts</a>';
        $breadcrumb_level2 = '<a href="/accounts/'.$account_id.'">Customer</a>';
        $breadcrumb_level3 = '<a href="/accounts/'.$account_id.'/wallets">+Wallets</a>';
        $breadcrumb_level4 = '<a href="/accounts/'.$account_id.'/wallets/'.$wallet_id.'">+Wallet</a>';
        $breadcrumb_level5 = '<a href="/accounts/'.$account_id.'/wallets/'.$wallet_id.'/payments">Payments</a>';
        
        return view('admin/modules/accounts/walletpayments', compact('account', 'wallet', 'payments', 'pagetitle', 'subpagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'breadcrumb_level3', 'breadcrumb_level4', 'breadcrumb_level5', 'account_id', 'wallet_id'));
    }
}
