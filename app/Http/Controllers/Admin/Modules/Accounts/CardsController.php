<?php

namespace App\Http\Controllers\Admin\Modules\Accounts;

use DB;
use App\Account;
use App\Http\Controllers\Controller;

class CardsController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($account_id)
    {
        $account = Account::where('account_id', '=', $account_id)->first();

        $linked_cards = DB::table('linked_cards')
            ->leftjoin('card_types', 'linked_cards.card_type', '=', 'card_types.short_code')
            ->where('account_id', '=', $account_id)
            ->select('linked_cards.*', 'card_types.file_name')
            ->get();

        $pagetitle = $account->first_name . ' ' . $account->last_name;
        $breadcrumb_level1 = '<a href="/admin/accounts">Accounts</a>';
        $breadcrumb_level2 = '<a href="/admin/accounts/'.$account_id.'">Customer</a>';
        $breadcrumb_level3 = '<a href="/admin/accounts/'.$account_id.'/paymentmethods">Payment Methods</a>';

        return view('admin/modules/accounts/cards', compact('account', 'linked_cards', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'breadcrumb_level3', 'account_id'));
    }
}
