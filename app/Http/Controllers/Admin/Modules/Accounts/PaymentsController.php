<?php

namespace App\Http\Controllers\Admin\Modules\Accounts;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

use DB;
use Input;
use Validator;
use App\Account;
use App\Payment;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class PaymentsController extends Controller
{   
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($account_id)
    {
        $account = Account::where('account_id', '=', $account_id)->first();
		$payments = DB::table('payments')
					->where('account_id', '=', $account_id)
					->orWhere('from', '=', $account->email)
					->orWhere('to', '=', $account->email)
					->orderBy('created_at','desc')
					->paginate(5);
        $pagetitle = $account->first_name . ' ' . $account->last_name;
        $breadcrumb_level1 = '<a href="/admin/accounts">Accounts</a>';
        $breadcrumb_level2 = '<a href="/admin/accounts/'.$account_id.'">Customer</a>';
        $breadcrumb_level3 = '<a href="/admin/accounts/'.$account_id.'/payments">Payments</a>';
        
        return view('admin/modules/accounts/payments', compact('payments', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'breadcrumb_level3', 'account_id'));
    }
	
	
}
