<?php

namespace App\Http\Controllers\Admin\Modules\Accounts;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

use DB;
use Input;
use Validator;
use App\Account;
use App\Wallet;
use App\Walletlimit;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class WalletLimitsController extends Controller
{   
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($account_id, $wallet_id)
    {
        $account = Account::where('account_id', '=', $account_id)->first();
		$wallet = DB::table('wallets')->where('wallet_id', '=', $wallet_id)->first();
        $walletlimit = DB::table('walletlimits')->where('wallet_limit_id', [$wallet->wallet_limit_id])->first();

        $current_turnover = DB::table('payments')->where('wallet_id', [$wallet_id])->where('total_amount', '>', 0)->sum('total_amount');
        $current_turnover_bar =  ($current_turnover / $walletlimit->max_turnover) * 100;
        $current_balance_bar =  ($wallet->balance / $walletlimit->max_balance) * 100;
        
        $pagetitle = $account->first_name . ' ' . $account->last_name;
        $subpagetitle = '+Wallet';
        $breadcrumb_level1 = '<a href="/admin/accounts">Accounts</a>';
        $breadcrumb_level2 = '<a href="/admin/accounts/'.$account_id.'">Customer</a>';
        $breadcrumb_level3 = '<a href="/admin/accounts/'.$account_id.'/wallets">+Wallets</a>';
        $breadcrumb_level4 = '<a href="/admin/accounts/'.$account_id.'/wallets/'.$wallet_id.'">+Wallet</a>';
        $breadcrumb_level5 = '<a href="/admin/accounts/'.$account_id.'/wallets/'.$wallet_id.'/limits">Limits</a>';
        
        return view('admin/modules/accounts/walletlimits', 
                compact('account', 
                        'wallet', 
                        'walletlimit', 
                        'current_turnover', 
                        'current_turnover_bar', 
                        'current_balance_bar',
                        'pagetitle', 
                        'subpagetitle', 
                        'breadcrumb_level1', 
                        'breadcrumb_level2', 
                        'breadcrumb_level3', 
                        'breadcrumb_level4', 
                        'breadcrumb_level5', 
                        'account_id', 
                        'wallet_id'
                        )
                );
    }
}
