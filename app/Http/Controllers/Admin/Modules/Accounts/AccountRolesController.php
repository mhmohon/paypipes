<?php

namespace App\Http\Controllers\Admin\Modules\Accounts;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

use DB;
use Validator;
use Auth;
use App\Activity;
use App\Account;
use App\Whitelistcustomer;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AccountRolesController extends Controller
{   
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function index(Request $request,$account_id)
    {       
		$data['item_per_page'] = $item_per_page = !empty($request->rows)?$request->rows:ROW_PER_PAGE;
		$where['whitelistcustomers.account_id'] = $account_id;	
		$data['whitelistcustomers'] = Whitelistcustomer::search($request,$where);	
		$data['account'] = $account = DB::table('accounts')->where('account_id', '=', $account_id)->first();
        $data['pagetitle'] = $account->name;	
		$data['account_id'] = $account_id;
		
		
        $data['subpagetitle'] = 'Roles';
        $data['breadcrumb_level1'] = '<a href="/admin/accounts">Accounts</a>';
        $data['breadcrumb_level2'] = '<a href="/admin/accounts/'.$account_id.'">Account</a>';
        $data['breadcrumb_level3'] = '<a href="/admin/accounts/'.$account_id.'/users">User Management</a>';
        $data['breadcrumb_level4'] = '<a href="/admin/accounts/'.$account_id.'/roles">Role</a>';
        return view('admin/modules/accounts/roles', $data);
    }     

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $account_id)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:whitelistcustomers,email,NULL,id,account_id,'.$account_id,
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#add")
                ->withErrors($validator)
                ->withInput();
        }
        
        $account = Account::where('account_id', '=', $account_id)->firstOrFail();
        
        $whitelistcustomer = new Whitelistcustomer($request->all());
        $whitelistcustomer['account_id'] = $account_id;
        $whitelistcustomer['type'] = 'Custom';
        $whitelistcustomer['status'] = 'Active';
		$whitelistcustomer->save();
        
		$activity = new Activity ();
        $activity['admin_id'] = Auth::guard('admin')->user()->id;
		$activity['added_by'] = Auth::guard('admin')->user()->name;
		$activity['interface'] = 'Admin';
        $activity['module'] = 'RiskManagement';
        $activity['activity'] = 'Email '. $whitelistcustomer->email .' has been added to <a href="/admin/whitelistcustomers">Customers Whitelist</a> by '. Auth::guard('admin')->user()->name .'.';
        $activity->save();
		
		session()->flash('toastr_alert','<b>Added!</b> Email '. $whitelistcustomer->email .' has been added to <a href="/admin/whitelistcustomers">Customers Whitelist</a> by '. Auth::guard('admin')->user()->name .'.');
		session()->flash('success', true);
        
        return Redirect::to(URL::previous());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($account_id)
    {
        $account = DB::table('accounts')->where('account_id', '=', $account_id)->first();

        $pagetitle = $account->company_name;
        $subpagetitle = 'Role';
        $breadcrumb_level1 = '<a href="/admin/accounts">Accounts</a>';
        $breadcrumb_level2 = '<a href="/admin/accounts/'.$account_id.'">Account</a>';
        $breadcrumb_level3 = '<a href="/admin/accounts/'.$account_id.'/users">User Management</a>';
        $breadcrumb_level4 = '<a href="/admin/accounts/'.$account_id.'/roles/1">Role</a>';
        
        $terminal_id = '0';
        $lead_id = '0';
        $wallet_id = '0';
        
        return view('admin/modules/accounts/role', compact('account', 'pagetitle', 'subpagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'breadcrumb_level3', 'breadcrumb_level4', 'account_id', 'terminal_id', 'lead_id', 'wallet_id'));
    }
}
