<?php

namespace App\Http\Controllers\Admin\Modules\Accounts;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

use DB;
use Validator;
use Auth;
use App\Activity;
use App\Account;
use App\Wallet;
use App\Wallet_switch;
use App\Wallettype;
use App\Walletfee;
use App\Walletlimit;
use App\Http\Controllers\Controller;

class WalletsController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($account_id)
    {
        $account = Account::where('account_id', '=', $account_id)->first();
        $item_per_page = !empty($request->rows)?$request->rows:ROW_PER_PAGE;

        $wallets = DB::table('wallets')
            ->leftjoin('walletfees', 'wallets.wallet_fee_id', '=', 'walletfees.wallet_fee_id')
            ->leftjoin('walletlimits', 'wallets.wallet_limit_id', '=', 'walletlimits.wallet_limit_id')
            ->leftjoin('wallet_switches', 'wallets.wallet_id', '=', 'wallet_switches.wallet_id')
            ->leftjoin('wallettypes', 'wallets.wallet_type_id', '=', 'wallettypes.wallet_type_id')
            ->where('wallet_switches.account_id', '=', $account_id)
            ->select('wallets.*', 'wallettypes.name as type_name', 'walletlimits.name as limit_name', 'walletfees.name as fee_name')
            ->paginate($item_per_page);




        $currencies = DB::table('currencies')->where('active', '=', 1)->orderBy('cy_code', 'asc')->pluck('cy_code', 'cy_code');

        $account_currency = DB::select('select cy_code from currencies where cy_code not in(
        select wallets.currency from accounts INNER JOIN wallet_switches on wallet_switches.account_id=accounts.account_id
        INNER JOIN wallets on wallets.wallet_id=wallet_switches.wallet_id where accounts.account_id='.$account_id.') and active=1');

        $currency_arr=[];
        foreach ($account_currency as $currency) {
            $currency_arr[$currency->cy_code]=$currency->cy_code;
        }

        $wallets_currency = DB::table('wallets')
            ->join('wallet_switches', 'wallets.wallet_id', '=', 'wallet_switches.wallet_id')
            ->where('wallet_switches.account_id', '=', $account_id)
            ->pluck('currency');

        $wallet_id = 0;

        $wallettypes = Wallettype::pluck('name', 'wallet_type_id');
        $walletfees = Walletfee::pluck('name', 'wallet_fee_id');
        $walletlimits = Walletlimit::pluck('name', 'wallet_limit_id');
        $pagetitle = $account->first_name . ' ' . $account->last_name;
        $breadcrumb_level1 = '<a href="/admin/accounts">Accounts</a>';
        $breadcrumb_level2 = '<a href="/admin/accounts/'.$account_id.'">Customer</a>';
        $breadcrumb_level3 = '<a href="/admin/accounts/'.$account_id.'/wallets">+Wallets</a>';

        return view('admin/modules/accounts/wallets', compact('account', 'wallets', 'currencies', 'currency_arr', 'wallets_currency', 'wallettypes', 'walletfees', 'walletlimits', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'breadcrumb_level3', 'account_id', 'wallet_id'));
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $account_id)
    {
        $validator = Validator::make($request->all(), [
            'currency' => 'required',
            'wallet_fee_id' => 'required',
            'wallet_limit_id' => 'required',
            'wallet_type_id' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#add")
                ->withErrors($validator)
                ->withInput();
        }

        $account = Account::where('account_id', '=', $account_id)->first();
        $wallet_id=(int)$wallet_id=DB::table('wallets')->orderBy('wallet_id', 'desc')->value('wallet_id');
        if ($wallet_id < 1) {
            $wallet_id=101011;
        } else {
            $wallet_id = $wallet_id + 1;
        }

        $wallets_rec = DB::table('wallets')->where('email', $account->email)->select('wallet_id')->first();

        if (count($wallets_rec) > 0) {
            $primary=0;
        } else {
            $primary=1;
        }

        $wallet = new Wallet($request->all());
        $wallet['wallet_id'] = $wallet_id;
        $wallet['primary'] = $primary;
        $wallet['email'] = $account->email;
        $wallet['status'] = 'Pending';
        $wallet->save();

        $wallets = DB::table('wallets')
                ->join('wallet_switches', 'wallets.wallet_id', '=', 'wallet_switches.wallet_id')
                ->where('wallet_switches.account_id', '=', $account_id)
                ->update(['wallets.wallet_type_id' => $request->wallet_type_id]);

        $wallet_switch = new Wallet_switch();
        $wallet_switch ['wallet_id'] = $wallet_id;
        $wallet_switch ['account_id'] = $account_id;
        $wallet_switch->save();

        $activity = new Activity();
        $activity['admin_id'] = Auth::guard('admin')->user()->id;
        $activity['added_by'] = Auth::guard('admin')->user()->name;
        $activity['interface'] = 'Admin';
        $activity['module'] = 'Wallet';
        $activity['activity'] = 'New Wallet for '. $account->first_name .' '. $account->last_name .' ('.$account->email.') has been added by '. Auth::guard('admin')->user()->name .'.';

        session()->flash('toastr_alert', '<b>Added!</b> New Wallet for '. $account->first_name .' '. $account->last_name .' ('.$account->email.') has been added by '. Auth::guard('admin')->user()->name .'.');
        session()->flash('success', true);

        return Redirect::to(URL::previous());
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($account_id, $wallet_id)
    {

        //$account = Account::findOrFail($id = $account_id);
        $account = Account::where('account_id', '=', $account_id)->first();
        //$wallet = Wallet::findOrFail($id = $wallet_id);
        $wallet = DB::table('wallets')->where('wallet_id', '=', $wallet_id)->first();
        $wallettypes = Wallettype::pluck('name', 'wallet_type_id');
        $walletfees = Walletfee::pluck('name', 'wallet_fee_id');
        $walletlimits = Walletlimit::pluck('name', 'wallet_limit_id');
        $pagetitle = $account->first_name . ' ' . $account->last_name;
        $subpagetitle = '+Wallet ('.$wallet->currency.')';
        $breadcrumb_level1 = '<a href="/accounts">Accounts</a>';
        $breadcrumb_level2 = '<a href="/accounts/'.$account_id.'">Customer</a>';
        $breadcrumb_level3 = '<a href="/accounts/'.$account_id.'/wallets">+Wallets</a>';
        $breadcrumb_level4 = '<a href="/accounts/'.$account_id.'/wallets/'.$wallet_id.'">+Wallet</a>';

        return view('admin/modules/accounts/wallet', compact('account', 'wallet', 'wallettypes', 'walletfees', 'walletlimits', 'pagetitle', 'subpagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'breadcrumb_level3', 'breadcrumb_level4', 'account_id', 'wallet_id'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $account_id, $wallet_id)
    {
        $validator = Validator::make($request->all(), [
            'wallet_fee_id' => 'required',
            'wallet_limit_id' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#edit")
                ->withErrors($validator)
                ->withInput();
        }

        $account = Account::where('account_id', '=', $account_id)->firstOrFail();

        $wallet = Wallet::where('wallet_id', '=', $wallet_id)->firstOrFail();
        $wallet->update($request->all());
        $wallet->save();

        $wallets = DB::table('wallets')
                ->join('wallet_switches', 'wallets.wallet_id', '=', 'wallet_switches.wallet_id')
                ->where('wallet_switches.account_id', '=', $account_id)
                ->update(['wallets.wallet_type_id' => $request->wallet_type_id]);

        $activity = new Activity();
        $activity['admin_id'] = Auth::guard('admin')->user()->id;
        $activity['added_by'] = Auth::guard('admin')->user()->name;
        $activity['interface'] = 'Admin';
        $activity['module'] = 'Wallet';
        $activity['activity'] = 'Wallet of '. $account->first_name .' '. $account->last_name .' ('.$account->email.') has been updated by '. Auth::guard('admin')->user()->name .'.';
        $activity->save();

        session()->flash('toastr_alert', '<b>Updated!</b> Wallet of '. $account->first_name .' '. $account->last_name .' ('.$account->email.') has been updated by '. Auth::guard('admin')->user()->name .'.');
        session()->flash('info', true);

        return Redirect::to(URL::previous());
    }
}
