<?php

namespace App\Http\Controllers\Admin\Modules\Accounts;

use DB;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StatementsController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $account_id)
    {
        $item_per_page = !empty($request->rows)?$request->rows:ROW_PER_PAGE;
        $account = DB::table('accounts')->where('account_id', '=', $account_id)->first();
        $statements = DB::table('account_statements')->where('account_id', [$account_id])->orderBy('created_at', 'desc')->paginate($item_per_page);
        $pagetitle = $account->first_name.' '.$account->last_name;
        $breadcrumb_level1 = '<a href="/admin/accounts">Accounts</a>';
        $breadcrumb_level2 = '<a href="/admin/accounts/'.$account_id.'">Account</a>';
        $breadcrumb_level3 = '<a href="/admin/accounts/'.$account_id.'/statements">Statements</a>';

        $wallet_id = '0';

        return view('admin/modules/accounts/statements', compact('statements', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'breadcrumb_level3', 'account', 'account_id', 'wallet_id'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($account_id, $statement_id)
    {
        $account = DB::table('accounts')->where('account_id', '=', $account_id)->first();

        $statement = DB::table('account_statements')->where('account_statement_id', '=', $statement_id)->first();


        $firstDayofPreviousMonth = Carbon::createFromDate($statement->year, $statement->month)->startOfMonth()->toDateString().' 00:00:00';
        $lastDayofPreviousMonth =Carbon::createFromDate($statement->year, $statement->month)->endOfMonth()->toDateString().' 23:59:59';

        $payments = DB::select('select * from payments where  `settlement_date` >="'.$firstDayofPreviousMonth.'" and `settlement_date` <="'.$lastDayofPreviousMonth.'" and ( `from` = "'.$account->email.'" or `to` = "'.$account->email.'" ) and `currency`="'.$statement->currency.'" order by `created_at` ASC') ;

        $pagetitle = $account->name;
        $breadcrumb_level1 = '<a href="/admin/accounts">Accounts</a>';
        $breadcrumb_level2 = '<a href="/admin/accounts/'.$account_id.'">Account</a>';
        $breadcrumb_level3 = '<a href="/admin/accounts/'.$account_id.'/statements">Statements</a>';
        $breadcrumb_level2 = '<a href="/admin/accounts/'.$account_id.'/statements/'.$statement_id.'">'.Carbon::createFromDate($statement->year, $statement->month)->format('F').' '.$statement->year.'</a>';

        $wallet_id = '0';

        return view('admin/modules/accounts/statement', compact('payments', 'pagetitle', 'account', 'breadcrumb_level1', 'breadcrumb_level2', 'breadcrumb_level3', 'breadcrumb_level4', 'account_id', 'account_id', 'wallet_id'));
    }
}
