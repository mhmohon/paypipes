<?php

namespace App\Http\Controllers\Admin\Modules\Accounts;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

use DB;
use Validator;
use App\Account;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AccountStatisticsController extends Controller
{   
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($account_id)
    {
        $account = Account::where('account_id', '=', $account_id)->first();
        $approved = DB::table('payments')->where('status', '=', 'Approved')->count('status');
        $declined = DB::table('payments')->where('status', '=', 'Declined')->count('status');
        $total = $approved + $declined;
        $paymentapproval = 0; /* ($approved / $total) * 100; */
        $pagetitle = $account->first_name . ' ' . $account->last_name;
        $breadcrumb_level1 = '<a href="/admin/accounts">Accounts</a>';
        $breadcrumb_level2 = '<a href="/admin/accounts/'.$account_id.'">Customer</a>';
        $breadcrumb_level3 = '<a href="/admin/accounts/'.$account_id.'/accountstatistics">Statistics</a>';
        
        $paymentcount = DB::table('payments')->where('account_id', [$account_id])->count();
        $paymentturnovereur = DB::table('payments')->where('account_id', [$account_id])->sum('amount');
        
        return view('admin/modules/accounts/accountstatistics', compact('pagetitle', 'paymentapproval', 'breadcrumb_level1', 'breadcrumb_level2', 'breadcrumb_level3', 'paymentcount', 'paymentturnovereur',  'account_id'));
    }
}
