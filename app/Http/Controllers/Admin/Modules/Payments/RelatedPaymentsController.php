<?php

namespace App\Http\Controllers\Modules\Payments;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

use Validator;
use DB;
use App\Payment;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class RelatedPaymentsController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $payment = Payment::findOrFail($id);
        $parentid = DB::table('payments')->where('id', [$id])->value('parentid');
        $childid = DB::table('payments')->where('id', [$id])->value('childid');
        $parentpayment = DB::table('payments')->where('id', [$parentid])->get();
        $childpayment = DB::table('payments')->where('id', [$childid])->get();
        $payments = DB::select("select * FROM payments WHERE id = '$id' OR id = '$parentid' OR id = '$childid' ORDER BY created_at DESC");
        $pagetitle = 'Related Payments';
        $breadcrumb_level1 = '<a href="/payments">Payments</a>';
        $breadcrumb_level2 = '<a href="/payments/'.$id.'">Payment</a>';
        $breadcrumb_level3 = '<a href="/payments/'.$id.'/relatedpayments">Related Payments</a>';
        
        return view('modules/payments/relatedpayments', compact('payment', 'payments',  'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'breadcrumb_level3','id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
