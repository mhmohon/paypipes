<?php

namespace App\Http\Controllers\Admin\Modules\Payments;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Collection;

use Validator;
use DB;
use Auth;
use Carbon\Carbon;
use App\Activity;
use App\Payment;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class WithdrawController extends Controller
{   
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$payments = Payment::where(function($query) use ($request) {
			//filter by keyword
			if (($search = $request->get('search'))) {
				$query->orWhere('payment_id', 'like', '%' . $search . '%');
				$query->orWhere('from', 'like', '%' . $search . '%');
				$query->orWhere('to', 'like', '%' . $search . '%');
				$query->orWhere('description', 'like', '%' . $search . '%');
				$query->orWhere('payment_method', 'like', '%' . $search . '%');
				$query->orWhere('payment_type', 'like', '%' . $search . '%');
				$query->orWhere('amount', 'like', '%' . $search . '%');
				$query->orWhere('total_amount', 'like', '%' . $search . '%');
				$query->orWhere('currency', 'like', '%' . $search . '%');
				$query->orWhere('status', 'like', $search . '%');
			}
		})
		->where([
			['status', '=', 'Pending'],
			['payment_method', '=', '+Wallet'],
			['payment_type', '=', 'Withdraw'],
		])
		->orderBy('created_at','desc')
		->paginate(10);
			
        $pagetitle = 'Withdraw Requests';
        $breadcrumb_level1 = '<a href="/admin/payments">Payments</a>';
        $breadcrumb_level2 = '<a href="/admin/payments/withdrawpayments">Withdraw Requests</a>';
		
		$payment_id='0';
        
        return view('admin/modules/payments/withdrawpayments', compact('payments',  'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'breadcrumb_level3','payment_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
