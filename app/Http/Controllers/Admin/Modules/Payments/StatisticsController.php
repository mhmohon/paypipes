<?php

namespace App\Http\Controllers\Modules\Payments;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Khill\Lavacharts\Lavacharts;

use DB;
use Lava;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class StatisticsController extends Controller
{   
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $paymentcount = DB::table('payments')->count();
        $paymentturnovereur = DB::table('payments')->where('currency', '=', 'EUR')->sum('amount');
        $approved = DB::table('payments')->where('status', '=', 'Approved')->count('status');
        $declined = DB::table('payments')->where('status', '=', 'Declined')->count('status');
        $total = $approved + $declined;
        $paymentapproval =  ($approved / $total) * 100;       
        $paymentcountrycount = '5';
        $pagetitle = 'Payments Statistics';
        $breadcrumb_level1 = '<a href="/payments">Payments</a>';
        $breadcrumb_level2 = '<a href="/paymentsstatistics">Statistics</a>';
        
        $id = '0';
        
        $chart = Lava::DataTable();

        $chart->addDateColumn('Year')
              ->addNumberColumn('Turnover in EUR')
              ->addNumberColumn('Turnover in USD')
              ->addNumberColumn('Turnover in GBP')
              ->addRow(['Jun1', 100000, 65412, 7845])
              ->addRow(['Jun2', 11712, 54782, 7541])
              ->addRow(['Jun3', 66035, 69874, 12478])
              ->addRow(['Jun4', 103547, 56241, 8564])
              ->addRow(['Jun5', 100421, 75423, 9547])
              ->addRow(['Jun6', 117354, 64874, 3654])
              ->addRow(['Jun7', 66742, 35412, 6784])
              ->addRow(['Jun8', 103547, 56241, 8564])
              ->addRow(['Jun9', 100421, 75423, 9547])
              ->addRow(['Jun10', 11712, 54782, 7541])
              ->addRow(['Jun11', 66742, 35412, 6784])
              ->addRow(['Jun12', 103547, 56241, 8564])
              ->addRow(['Jun13', 100421, 75423, 9547])
              ->addRow(['Jun14', 11712, 54782, 7541])
              ->addRow(['Jun15', 66742, 35412, 6784])
              ->addRow(['Jun16', 103547, 56241, 8564])
              ->addRow(['Jun17', 100421, 75423, 9547])
              ->addRow(['Jun18', 11712, 54782, 7541])
              ->addRow(['Jun19', 17354, 64874, 3654])
              ->addRow(['Jun20', 103547, 56241, 8564])
              ->addRow(['Jun21', 100421, 75423, 9547])
              ->addRow(['Jun22', 17354, 64874, 3654])
              ->addRow(['Jun23', 66742, 35412, 6784])
              ->addRow(['Jun24', 103547, 56241, 8564])
              ->addRow(['Jun25', 100421, 75423, 9547])
              ->addRow(['Jun26', 11712, 54782, 7541])
              ->addRow(['Jun27', 66742, 35412, 6784])
              ->addRow(['Jun28', 103547, 56241, 8564])
              ->addRow(['Jun29', 66742, 35412, 6784])
              ->addRow(['Jun30', 66742, 35412, 6784]);

        Lava::AreaChart('Chart', $chart, [
            'legend'             => 'top',
            'axisTitlesPosition' => 'string',
            'lineWidth'          => 1,
            'pointSize'          => 5,
            'height'             => 400,
            'hAxis'              => ['y'],   //HorizontalAxis Options
        ]);
        
        return view('modules/payments/statistics', compact('approved', 'declined', 'total', 'pagetitle', 'paymentcount', 'paymentturnovereur', 'paymentapproval', 'paymentcountrycount', 'breadcrumb_level1', 'breadcrumb_level2', 'id'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }
    
    public function store(Request $request, $id)
    {
    
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    
    }
}
