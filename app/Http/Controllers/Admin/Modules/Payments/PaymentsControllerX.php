<?php

namespace App\Http\Controllers\Admin\Modules\Payments;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Input;

use DB;
use Validator;
use Auth;
use Mail;
use Response;
use Carbon\Carbon;
use App\Activity;
use App\Notification;
use App\Payment;
use App\Currency;
use App\Merchant;
use App\Terminal;
use App\Document;
use App\Wallet;
use App\Dispute;
use App\Helper;
use App\Disputecomment;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Helper\Common;


class PaymentsControllerX extends Controller
{   
    public function __construct()
    {
       $this->middleware('admin');
	   $this->common = new Common;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
		$data['item_per_page'] = $item_per_page = !empty($request->rows)?$request->rows:ROW_PER_PAGE;	
		$data['pg']='?'.http_build_query($request->all());
	
		$data['payments']=Payment::search($request,$data);	
		
		$data['pagetitle'] = 'Bank Payments';
        $data['breadcrumb_level1'] = '<a href="/admin/payments">Bank Payments</a>';        
		$data['item_per_page_url'] = url('admin/payments');
        $data['currency'] = Currency::currencylist();        
		$data['merchantlist'] = ['0'=>'Select a Marchant'] + Merchant::merchantlist();
		$data['terminallist'] = ["0" => "Select a Merchant"]; 
		
		//$terminalcurrlist=Terminal::terminalcurrlist(22123);
		
		
		//$data['terminallist'] = ["0" => "Select a Terminal"] + Terminal::terminallist(1011); 
		
		$data['status'] = ['Approved', 'Pending', 'Declined', 'Refund', 'Reverse', 'Chargeback'];
		
		$data['payment_id'] = 0;
	 
        $request->flash();
		return view('admin/modules/payments/payments', $data);
    }
	public function download(Request $request)
    {
		
		$filepath =	public_path()."/downloads/batch_payment.csv";
        
		$headers = [
         'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0'
        ,'Content-type'        => 'text/csv'
        ,'Content-Disposition' => 'attachment; filename=batch_payment.csv'
        ,'Expires'             => '0'
        ,'Pragma'              => 'public'
		];
      
		
		return Response::download($filepath, 'batch_payment.csv', $headers);
		
	}
	public function terminallist(Request $request)
    {
		$terminallist=Terminal::terminallist($request->merchant_id);
		$terminalid=$terminal_id=0;
		$selection_data='';
		$list=[];
		$i=0;
		foreach ($terminallist as $key=>$value)
        {	
			if($i==0)$terminalid=$key; $i=1;
              $selection_data.= '<option value="'.$key.'" '.(!empty($key) && $key == $terminal_id ? "selected='selected'" : '' ).' >'.$value.' </option>';
        }
		$list['terminallist']=$selection_data;
		$list['currencylist']=Terminal::terminalcurrlist($terminalid);
		echo json_encode($list);
       
    }
	
	public function terminalcurrlist(Request $request)
    {
		$terminalcurrlist=Terminal::terminalcurrlist($request->terminal_id);
		echo json_encode($terminalcurrlist);
       
    }
	
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		//
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
			
		$rule=[
		    'orginal_date' => 'required|date',
            'reference_id' => 'required|max:50',            
            'merchant_id' => 'required|max:11',
            'terminal_id' => 'required|max:11',
			'transfer_type'=>'required',
            'description' => 'required|max:150',
            'currency' => 'required|max:3',
            'amount' => 'required|max:20',
            'fee' => 'required|max:20'];
			
		if($request->transfer_type=='TransferOut')	
		{
			$rule['to']='required|max:150';
		}
		else if($request->transfer_type=='TransferIn')	
		{
			$rule['from']='required|max:150';
		}	
		
		$validator = Validator::make($request->all(), $rule);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#add")
                ->withErrors($validator)
                ->withInput();
        }
		$terminals = DB::table('terminals')	
					->where('terminal_id', '=', $request->terminal_id)
					->where('merchant_id', '=', $request->merchant_id)					
					->first();
					
		if(count($terminals) == 0) 
		{
			$validator->errors()->add('merchant_id', 'Please check your information again!');
			return Redirect::to(URL::previous() . "#add")
                ->withErrors($validator)
                ->withInput();
		}
			
			
			
		  
        $payment = new Payment($request->all());
		$helper = new Helper;
		$payment_id=$helper->uniqidReal(16);
		
		//$payment_id=(int)$payment_id=DB::table('payments')->orderBy('payment_id','desc')->value('payment_id');
		//if($payment_id < 1) $payment_id=93101;
		//else $payment_id = $payment_id + 1;
		
		
		
		$payment['payment_id'] =$payment_id;
		$payment['payment_method'] ='Bank';
		$payment['payment_type'] =$request->transfer_type;
		$payment['from'] =!empty($request->from)?$request->from:null;
		$payment['to'] =!empty($request->to)?$request->to:null;
		$payment['status'] ='Approved';
		$payment['added_by'] = Auth::guard('admin')->user()->name;
		$payment->save();
		
		
        $activity = new Activity ();
        $activity['admin_id'] = Auth::guard('admin')->user()->admin_id;
		$activity['added_by'] = Auth::guard('admin')->user()->name;
		$activity['interface'] = 'Admin';
        $activity['module'] = 'Payment';
        $activity['activity'] = 'New Payment <a href="/admin/payments/'.$payment_id.'">'. $payment->reference_id .' - '. $payment->currency. $payment->amount .'</a> has been added by <a href="/admin/usermanagement/users/'. Auth::guard('admin')->user()->id .'">'. Auth::guard('admin')->user()->name .'</a>.';
        $activity->save();
        
		
		
		if (Input::hasFile('upload_file')) {
			
		$destinationPath = 'uploads/documents'; // upload path
		$extension = Input::file('upload_file')->getClientOriginalExtension(); // getting image extension
		$fileName = rand(11111,99999).'.'.$extension; // renameing image
		Input::file('upload_file')->move($destinationPath, $fileName); // uploading file to given path
		
		
       	$document = new Document($request->all());
		$document['payment_id'] = $payment_id;
		$document['status'] = 'Pending';
		$document['name'] = $fileName;
		$document['file_name'] = $fileName;
		$document['file_path'] = $destinationPath.'/'.$fileName;
		$document['file_type'] = $extension;
		$document['added_by'] = Auth::guard('admin')->user()->name;
		$document->save();	
		
		}
		
		
		session()->flash('toastr_alert','<b>Added!</b> New Payment <a href="/admin/payments/'.$payment_id.'">'. $payment->reference_id .' - '. $payment->currency. $payment->amount .'</a> has been added by <a href="/admin/usermanagement/users/'. Auth::guard('admin')->user()->id .'">'. Auth::guard('admin')->user()->name .'</a>');
		session()->flash('success', true);
		
        return Redirect::to(URL::previous());
		
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($payment_id)
    {
		$data['payment']=$payment = DB::table('payments')->where('payments.payment_id', '=', $payment_id)->select('payments.*')->first();
					
		$data['terminal']=$terminal = Terminal::details($payment->terminal_id);
		$data['currencies'] = Currency::currencylist(); 
		$data['documents'] =$documents = DB::table('documents')->where('payment_id', '=', $payment_id)->orderBy('created_at','desc')->paginate(10);
		
		$data['total']= number_format($payment->fee + $payment->amount,2,'.','');
       
        $data['pagetitle'] = 'Payment';
        $data['breadcrumb_level1'] = '<a href="/admin/payments">Bank Payments</a>';
        $data['breadcrumb_level2'] ='<a href="/admin/payments/'.$payment_id.'">Payment</a>';
		
		$data['payment_id'] = $payment_id;
    
		return view('admin/modules/payments/payment', $data);
        
    }

	public function document(Request $request, $payment_id)
    {
        // getting all of the post data
		 $file = array('upload_file' => Input::file('upload_file'));
 
		  $validator = Validator::make($request->all(), [
           			'name' => 'required',
					'upload_file' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#document")
                ->withErrors($validator)
                ->withInput();
        }
		
		// checking file is valid.
		if (Input::file('upload_file')->isValid()) {
			
		$destinationPath = 'uploads/documents'; // upload path
		$extension = Input::file('upload_file')->getClientOriginalExtension(); // getting image extension
		$fileName = rand(11111,99999).'.'.$extension; // renameing image
		Input::file('upload_file')->move($destinationPath, $fileName); // uploading file to given path
		
		}
		
		$document_id=(int)$document_id=DB::table('documents')->orderBy('document_id','desc')->value('document_id');
		if($document_id < 1) $document_id=1010;
		else $document_id = $document_id + 1;
		
       	$document = new Document($request->all());
		$document['document_id'] = $document_id;
		$document['payment_id'] = $payment_id;
		$document['name'] = $request->name;
		$document['file_name'] = $fileName;
		$document['file_path'] = $destinationPath.'/'.$fileName;
		$document['file_type'] = $extension;
		$document['added_by'] = Auth::guard('admin')->user()->name;
		$document->save();	
		
        $activity = new Activity ();
        $activity['admin_id'] = Auth::guard('admin')->user()->admin_id;
		$activity['added_by'] = Auth::guard('admin')->user()->name;
		$activity['interface'] = 'Admin';
        $activity['module'] = 'payments';
        $activity['activity'] = 'New document '. $document->name .' has been added to Payment ID '.$payment_id.' by '. Auth::guard('admin')->user()->name .'';
		$activity->save();
        
        session()->flash('toastr_alert','<b>Added!</b> New document '. $document->name .' has been added to Payment ID '.$payment_id.' by '. Auth::guard('admin')->user()->name .'');
		session()->flash('success', true);
		
        return Redirect::to(URL::previous());
    }

	public function documentcsv(Request $request)
    {
		
        // getting all of the post data
		// $file = array('upload_file' => Input::file('upload_file'));
 
		  $validator = Validator::make($request->all(), [
           			'upload_file' => 'required',
        ]);
		
        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#documentcsv")
                ->withErrors($validator)
                ->withInput();
        }

		 # Open the File.
		if (($handle = fopen(Input::file('upload_file'), "r")) !== FALSE) {
			# Set the parent multidimensional array key to 0.
			$nn = 0;
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				# Count the total keys in the row.
				$c = count($data);
				# Populate the multidimensional array.
				for ($x=0;$x<$c;$x++)
				{
					$csvarray[$nn][$x] = $data[$x];
				}
				$nn++;
			}
			# Close the File.
			fclose($handle);
		}
		# Print the contents of the multidimensional array.
		
	
		if(count($csvarray) >0 )
		{
			$payment_id=(int)$payment_id=DB::table('payments')->orderBy('payment_id','desc')->value('payment_id');
			
			foreach($csvarray as $key=>$val)
			{
				if($val[1]!='orginal_date')
				{
					
					$orginal_date	=date('Y-m-d H:i:s',strtotime($val[1]));
					$reference_id	=$val[2];
					$from			=$val[3];
					$merchant_id	=$val[4];
					$terminal_id	=$val[5];
					$description	=$val[6];
					$currency		=$val[7];
					$amount			=$val[8];
					$fee			=$val[9];
					$settlement_date=date('Y-m-d H:i:s',strtotime($val[10]));

					
					$terminals = DB::table('terminals')	
					->where('terminal_id', '=', $terminal_id)
					->where('merchant_id', '=', $merchant_id)
					->whereIn('processing_currency', '=', $currency)
					->first();
			
			
					if(count($terminals) == 1) 
					{
													
						$payment = new Payment();
								
						if($payment_id < 1) $payment_id=1010;
						else $payment_id = $payment_id + 1;
						$payment['payment_id'] =$payment_id;
						
						$chkpayment=DB::table('payments')
						->where('terminal_id', '=', $terminal_id)
						->where('merchant_id', '=', $merchant_id)
						//->where('orginal_date', '=', $orginal_date)
						->where('reference_id', '=', $reference_id)
						->first();
						
						if(count($chkpayment) == 0) 
						{						
								
							$payment['orginal_date'] =$orginal_date;
							$payment['reference_id'] =$reference_id;
							$payment['from'] =$from;
							$payment['merchant_id'] =$merchant_id;
							$payment['terminal_id'] =$terminal_id;
							$payment['description'] =$description;
							$payment['currency'] =$currency;
							$payment['amount'] =$amount;
							$payment['fee'] =$fee;
							$payment['settlement_date'] =$settlement_date;						
							$payment['payment_method'] ='Bank';
							$payment['payment_type'] ='TransferIn';
							$payment['status'] ='Approved';
							$payment['added_by'] = Auth::guard('admin')->user()->name;
							$payment->save();
							
						}
					}					
				}				
			}
			$activity = new Activity ();
			$activity['admin_id'] = Auth::guard('admin')->user()->admin_id;
			$activity['added_by'] = Auth::guard('admin')->user()->name;
			$activity['interface'] = 'Admin';
			$activity['module'] = 'Payment';
			$activity['activity'] = 'New Payment batch Upload has been added by <a href="/admin/usermanagement/users/'. Auth::guard('admin')->user()->id .'">'. Auth::guard('admin')->user()->name .'</a>.';
			$activity->save();	
			
			
		}		
			
		 return Redirect::to(URL::previous());
	}
	/**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function documentview($payment_id, $document_id)
    {
		$document = DB::table('documents')->where('document_id', '=', $document_id)->where('payment_id', '=', $payment_id)->first();
		
        $pagetitle = 'Payment';
        $breadcrumb_level1 = '<a href="/admin/payments">Bank Payments</a>';
        $breadcrumb_level2 = '<a href="/admin/payments/'.$payment_id.'">Payment</a>';
		$breadcrumb_level3 = '<a href="/admin/payments/'.$payment_id.'/document/'.$document_id.'">Document</a>';
        
        return view('admin/modules/payments/document', compact('document', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'breadcrumb_level3', 'payment_id', 'document_id'));

    }
	
	public function pdf_convert(Request $request){  
		$data['item_per_page'] = $item_per_page = !empty($request->rows)?$request->rows:ROW_PER_PAGE;	
		$data['pg']='?'.http_build_query($request->all());
		
	   $queryResult=Payment::search($request,$data)->toArray();	  
	   			
        $fields = ['merchant_id', 'terminal_id','payment_id', 'orginal_date', 'reference_id', 'payment_type', 'description', 'currency', 'amount', 'fee',  'created_at'];
        $headers = ['Merchant ID', 'Terminal ID', 'Payment ID', 'Reference ID', 'Transfer Type', 'Description', 'Currency', 'Amount', 'Fee',  'Datetime'];
	   $pagetitle = 'Bank Payments';
	   $print_type='landscape';
       $pdf_name = 'bankpayments_'.time();
       return $this->common->dataToPdf($queryResult['data'], $fields, $pdf_name, $headers, $pagetitle,$print_type);
    }

    public function csv_convert(Request $request){
        	
		$data['item_per_page'] = $item_per_page = !empty($request->rows)?$request->rows:ROW_PER_PAGE;	
		$data['pg']='?'.http_build_query($request->all());
		
        $queryResult = Payment::search($request,$data)->toArray();
        $fields = ['merchant_id', 'terminal_id','payment_id', 'orginal_date', 'reference_id', 'payment_type', 'description', 'currency', 'amount', 'fee',  'created_at'];
        $headers = ['Merchant ID', 'Terminal ID', 'Payment ID', 'Reference ID', 'Transfer Type', 'Description', 'Currency', 'Amount', 'Fee',  'Datetime'];
        $csv_name = 'bankpayments_'.time();
        $this->common->dataToCSV($queryResult['data'], $fields, $csv_name, $headers);
    }
}
