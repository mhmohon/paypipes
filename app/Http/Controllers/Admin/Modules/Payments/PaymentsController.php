<?php

namespace App\Http\Controllers\Admin\Modules\Payments;

use DB;
use Auth;
use Mail;
use Validator;
use App\Wallet;
use App\Account;
use App\Dispute;
use App\Payment;
use App\Activity;
use App\Currency;
use App\Document;
use Carbon\Carbon;
use App\Notification;
use App\Disputecomment;
use App\Payment_method;
use App\Http\Helper\Common;
use Illuminate\Http\Request;
use App\Entities\PaymentNote;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class PaymentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
        $this->common = new Common;
    }



    public function indexX(Request $request)
    {
        $data['item_per_page'] = $item_per_page = !empty($request->rows)?$request->rows:ROW_PER_PAGE;
        $data['pg']='?'.http_build_query($request->all());

        $payment = new Payment;
        $data['payments']=$payments=$payment->search($request, $data);
        $data['payment_id']='0';

        $data['pagetitle'] = 'Payments';
        $data['breadcrumb_level1'] = '<a href="/admin/payments">Payments</a>';
        $data['item_per_page_url'] = url('admin/payments');
        $data['currency'] = Currency::currencylist();
        $data['gateway'] = Payment_method::gatewaylist();

        $data['status'] = ['Approved', 'Pending', 'Declined', 'Refund', 'Reverse', 'Chargeback'];
        $request->flash();
        return view('admin/modules/payments/payments', $data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $pg='?'.http_build_query($request->all());
        $item_per_page = !empty($request->rows)?$request->rows:ROW_PER_PAGE;

        $payments = Payment::where(function ($query) use ($request) {
            //filter by keyword
            if (($search = $request->get('search'))) {
                $query->orWhere('payment_id', 'like', '%' . $search . '%');
                $query->orWhere('from', 'like', '%' . $search . '%');
                $query->orWhere('to', 'like', '%' . $search . '%');
                $query->orWhere('description', 'like', '%' . $search . '%');
                $query->orWhere('payment_method', 'like', '%' . $search . '%');
                $query->orWhere('payment_type', 'like', '%' . $search . '%');
                $query->orWhere('amount', 'like', '%' . $search . '%');
                $query->orWhere('total_amount', 'like', '%' . $search . '%');
                $query->orWhere('currency', 'like', '%' . $search . '%');
                $query->orWhere('status', 'like', $search . '%');
            }
        })
        ->orderBy('created_at', 'desc')
        ->paginate($item_per_page);

        $pagetitle = 'Payments';
        $breadcrumb_level1 = '<a href="/admin/payments">Payments</a>';

        $payment_id='0';

        return view('admin/modules/payments/payments', compact('payments', 'pagetitle', 'breadcrumb_level1', 'payment_id', 'pg'));
    }




    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($payment_id)
    {
        $data['payment'] = $payment = DB::table('payments')->where('payment_id', '=', $payment_id)->first();
        $data['account_id'] =$account_id = $payment->account_id;
        $data['account'] = $account = Account::where('account_id', '=', $payment->account_id)->first();

        switch ($payment->status) {
            case "Approved":
                $payment_status =  'label-success'; break;
            case "Pending":
                $payment_status =  'label-info';    break;
            case "Requested":
                $payment_status =  'label-info';    break;
            case "Refunded":
                $payment_status =  'label-purple';  break;
            case "Canceled":
                $payment_status =  'label-warning'; break;
            case "Chargeback":
                $payment_status =  'label-black';   break;
            case "Declined":
                $payment_status =  'label-danger';  break;
            default:
                $payment_status =  'label-default'; break;
        }

        if (Account::where('email', '=', $payment->from)->exists()) {
            $from = Account::where('email', '=', $payment->from)->first();
        } else {
            $from = $account;
        }

        if (Account::where('email', '=', $payment->to)->exists()) {
            $to = Account::where('email', '=', $payment->to)->first();
        } else {
            $to = $account;
        }

        $data['payment_status'] = $payment_status;
        $data['from'] = $from;
        $data['to'] = $to;

        $data['documents'] = $documents = DB::table('documents')->where('payment_id', '=', $payment_id)->orderBy('created_at', 'desc')->paginate(10);

        $data['notes'] = $notes = PaymentNote::where(['payment_id' => $payment->payment_id])->orderBy('id', 'desc')->paginate(10);


        $data['dispute_id'] = $dispute_id = DB::table('disputes')->where('payment_id', '=', $payment_id)->value('id');
        $data['pagetitle'] = $pagetitle = 'Payment';
        $data['breadcrumb_level1'] = $breadcrumb_level1 = '<a href="/admin/payments">Payments</a>';
        $data['breadcrumb_level2'] = $breadcrumb_level2 = '<a href="/admin/payments/'.$payment_id.'">Payment</a>';

        return view('admin/modules/payments/payment', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($payment_id)
    {
        $payment = DB::table('payments')->where('payments.payment_id', '=', $payment_id)->first();

        $currencies = DB::table('currencies')->where('active', '=', 1)->orderBy('cy_code', 'asc')->pluck('cy_code', 'cy_code');
        $documents = DB::table('documents')->where('payment_id', '=', $payment_id)->orderBy('created_at', 'desc')->paginate(10);


        $dispute_id = DB::table('disputes')->where('payment_id', '=', $payment_id)->value('id');
        $pagetitle = 'Payment';
        $breadcrumb_level1 = '<a href="/admin/payments">Payments</a>';
        $breadcrumb_level2 = '<a href="/admin/payments/'.$payment_id.'">Payment</a>';

        return view('admin/modules/payments/payment', compact('payment', 'documents', 'currencies', 'dispute_id', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'payment_id'));
    }

    public function comment(Request $request, $payment_id)
    {
        $validator = Validator::make($request->all(), [
             'notes' =>         'required|min:5|max:255',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#comment")->withErrors($validator)->withInput();
        }

        $note = new PaymentNote($request->all());
        $note['payment_id'] = $payment_id;
        $note['added_by'] = Auth::guard('admin')->user()->name;
        $note->save();

        $activity = new Activity();
        $activity['added_by'] = Auth::guard('admin')->user()->name;
        $activity['interface'] = 'Admin';
        $activity['module'] = 'Payments - bank';
        $activity['activity'] = 'Comment ('. $payment_id .') has been added by '. Auth::guard('admin')->user()->name .'.';
        $activity->save();


        session()->flash('toastr_alert', '<b>Added!</b> comment ('. $payment_id .') has been added by '. Auth::guard('admin')->user()->name .'.');
        session()->flash('success', true);

        return Redirect::to(URL::previous());
    }


    public function commentUpdate(Request $request, $payment_id, $id)
    {
        $validator = Validator::make($request->all(), [
              'notes' =>         'required|min:5|max:255',
         ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#editcomment".$id)->withErrors($validator)->withInput();
        }

        $note = PaymentNote::where(['payment_id' => $payment_id, 'id' => $id])->firstOrFail();
        if (Auth::guard('admin')->user()->name == $note->added_by) {
            $note['updated_by'] = Auth::guard('admin')->user()->name;
            $note->update($request->all());

            $activity = new Activity();
            $activity['added_by'] = Auth::guard('admin')->user()->name;
            $activity['interface'] = 'Admin';
            $activity['module'] = 'Payments - bank';
            $activity['activity'] = 'Comment ('. $payment_id .') has been updated by '. Auth::guard('admin')->user()->name .'.';
            $activity->save();


            session()->flash('toastr_alert', '<b>Updated!</b> comment ('. $payment_id .') has been updated by '. Auth::guard('admin')->user()->name .'.');
            session()->flash('info', true);
        } else {
            session()->flash('toastr_alert', '<b>Error!</b> Invalid Access by '. Auth::guard('admin')->user()->name .'.');
            session()->flash('error', true);
        }

        return Redirect::to(URL::previous());
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function dispute(Request $request, $payment_id)
    {
        $validator = Validator::make($request->all(), [
            'reason' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#dispute")
                ->withErrors($validator)
                ->withInput();
        }

        $payment = Payment::findOrFail($payment_id);

        $dispute = $request->all();
        $dispute['payment_id'] = $payment_id;
        $dispute['merchant_id'] = $payment->merchant_id;
        $dispute['terminal_id'] = $payment->terminal_id;
        $dispute['status'] = 'Open';

        Dispute::create($dispute);

        return Redirect::to(URL::previous() . "#disputecomment");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function disputecomment(Request $request, $payment_id)
    {
        $validator = Validator::make($request->all(), [
            'comment' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#disputecomment")
                ->withErrors($validator)
                ->withInput();
        }

        $payment = Payment::findOrFail($payment_id);
        $dispute_id = DB::table('disputes')->where('payment_id', [$payment_id])->value('id');

        $disputecomment = new Disputecomment($request->all());
        $disputecomment['dispute_id'] = $dispute_id;

        return Redirect::to('disputes');
    }
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function refund()
    {
        $validator = Validator::make($request->all(), [
            'amount' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#edit")
                ->withErrors($validator)
                ->withInput();
        }

        $payment = Payment::findOrFail($id = $payment_id);
        $payment['status']='Refund';
        $payment->update($request->all());
        /*
        $customer = Customer::findOrFail($id = $customer_id);
        $activity = new Activity ();
        $activity['admin_id'] = Auth::guard('admin')->user()->id;
        $activity['added_by'] = Auth::guard('admin')->user()->name;
        $activity['interface'] = 'Admin';
        $activity['module'] = 'Customer';
        $activity['activity'] = 'Customer <a href="/admin/payments/'. $payment_id .'">'. $customer->first_name .' '. $customer->last_name .'</a> has been updated by <a href="/admin/usermanagement/users/'. Auth::guard('admin')->user()->id .'">'. Auth::guard('admin')->user()->name .'</a>.';

       Auth::guard('admin')->user()->activities()->save($activity);
        */
        return Redirect::to(URL::previous());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function cancel()
    {
        $payment = Payment::findOrFail($id = $payment_id);
        $payment['status']='Cancel';
        $payment->update($request->all());
        /*
        $customer = Customer::findOrFail($id = $customer_id);
        $activity = new Activity ();
        $activity['admin_id'] = Auth::guard('admin')->user()->id;
        $activity['added_by'] = Auth::guard('admin')->user()->name;
        $activity['interface'] = 'Admin';
        $activity['module'] = 'Customer';
        $activity['activity'] = 'Customer <a href="/admin/payments/'. $payment_id .'">'. $customer->first_name .' '. $customer->last_name .'</a> has been updated by <a href="/admin/usermanagement/users/'. Auth::guard('admin')->user()->id .'">'. Auth::guard('admin')->user()->name .'</a>.';


        Auth::guard('admin')->user()->activities()->save($activity);
        */

        return Redirect::to(URL::previous());
    }

    /**
     * Payment Forms.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function forms(Request $request, $payment_id)
    {
        if ($request->has('approve')) {
            $validator = Validator::make($request->all(), [
                'amount' => 'required',
                'fee' => 'required',
                'reference_id' => 'required',
            ]);

            if ($validator->fails()) {
                return Redirect::to(URL::previous() . "#approve")
                    ->withErrors($validator)
                    ->withInput();
            }

            $current_date = Carbon::now();
            $payment = Payment::where('payment_id', '=', $payment_id)->firstOrFail();
            /*
             if ($payment->payment_type == 'Withdraw') {
                 $total_amount = $request->amount + $request->fee;
             } else {
                 $total_amount = $request->amount - $request->fee;
             }*/
            $total_amount = $request->amount - $request->fee;

            $payment['total_amount'] = $total_amount;
            $payment['fee'] = $request->fee;
            $payment['reference_id'] = $request->reference_id;
            $payment['status'] = 'Approved';
            $payment['settlement_date'] = $current_date;
            $payment->save();

            $wallet_id = $payment->wallet_id;
            $wallet = Wallet::where('wallet_id', '=', $wallet_id)->firstOrFail();
            if ($payment->payment_type == 'Withdraw') {
                $wallet['balance'] = $wallet->balance - $request->amount;
            } else {
                $wallet['balance'] = $wallet->balance + ($request->amount - $request->fee);
            }
            $wallet->save();

            $activity = new Activity();
            $activity['admin_id'] = Auth::guard('admin')->user()->admin_id;
            $activity['added_by'] = Auth::guard('admin')->user()->name;
            $activity['interface'] = 'Admin';
            $activity['module'] = 'Payment';
            if ($payment->payment_type == 'TopUp') {
                $activity['activity'] = 'TopUp request of '. $payment->amount .' '. $payment->currency .' has been approved by '. Auth::guard('admin')->user()->name .'.';
            }
            if ($payment->payment_type == 'Withdraw') {
                $activity['activity'] = 'Withdraw request of '. $payment->amount .' '. $payment->currency .' has been approved by '. Auth::guard('admin')->user()->name .'.';
            }
            $activity->save();

            if ($payment->payment_type == 'TopUp') {
                session()->flash('toastr_alert', '<b>Approved!</b> Payment request of '. $payment->amount .' '. $payment->currency .' has been approved by '. Auth::guard('admin')->user()->name .'.');
                session()->flash('success', true);
            }
            if ($payment->payment_type == 'Withdraw') {
                session()->flash('toastr_alert', '<b>Approved!</b> Payment request of '. $payment->amount .' '. $payment->currency .' has been approved by '. Auth::guard('admin')->user()->name .'.');
                session()->flash('success', true);
            }

            //email notification receive
            if ($payment->payment_type == 'TopUp') {
                $notification_to = DB::table('notifications')->where('email', '=', $payment->to)->first();

                if ($notification_to->approve_decline == 1) {
                    $greeting = 'Hi there,';
                    $p1 = 'your TopUp request of <b>'.$payment->amount.' '.$payment->currency.'</b> has been <b>approved</b> on '.$current_date.'.';
                    $p2 = 'Reference number for this payment is <b>'.$payment->payment_id.'</b>.';

                    Mail::send('emails.notification', [
                        'payment' => $payment,
                        'greeting' => $greeting,
                        'p1' => $p1,
                        'p2' => $p2
                        ], function ($message) use ($payment) {
                            $message->to(''.$payment->to.'');
                            $message->subject('+Wallet TopUp');
                        });
                }
            }
            if ($payment->payment_type == 'Withdraw') {
                $notification_to = DB::table('notifications')->where('email', '=', $payment->from)->first();

                if ($notification_to->approve_decline == 1) {
                    $greeting = 'Hi there,';
                    $p1 = 'your Withdraw request of <b>'.$payment->amount.' '.$payment->currency.'</b> has been <b>approved</b> on '.$current_date.'.';
                    $p2 = 'Reference number for this payment is <b>'.$payment->payment_id.'</b>.';

                    Mail::send('emails.notification', [
                        'payment' => $payment,
                        'greeting' => $greeting,
                        'p1' => $p1,
                        'p2' => $p2
                        ], function ($message) use ($payment) {
                            $message->to(''.$payment->from.'');
                            $message->subject('+Wallet Withdraw');
                        });
                }
            }

            return Redirect::to(URL::previous());
        }
        if ($request->has('decline')) {
            $current_date = Carbon::now();
            $payment = Payment::where('payment_id', '=', $payment_id)->firstOrFail();

            $payment['status'] = 'Declined';
            $payment['settlement_date'] = $current_date;
            $payment->save();

            $activity = new Activity();
            $activity['admin_id'] = Auth::guard('admin')->user()->admin_id;
            $activity['added_by'] = Auth::guard('admin')->user()->name;
            $activity['interface'] = 'Admin';
            $activity['module'] = 'Payment';
            if ($payment->payment_type == 'TopUp') {
                $activity['activity'] = 'TopUp request of '. $payment->amount .' '. $payment->currency .' has been declined by '. Auth::guard('admin')->user()->name .'.';
            }
            if ($payment->payment_type == 'Withdraw') {
                $activity['activity'] = 'Withdraw request of '. $payment->amount .' '. $payment->currency .' has been declined by '. Auth::guard('admin')->user()->name .'.';
            }
            $activity->save();

            if ($payment->payment_type == 'TopUp') {
                session()->flash('toastr_alert', '<b>Declined!</b> TopUp request of '. $payment->amount .' '. $payment->currency .' has been declined by '. Auth::guard('admin')->user()->name .'.');
                session()->flash('error', true);
            }
            if ($payment->payment_type == 'Withdraw') {
                session()->flash('toastr_alert', '<b>Declined!</b> Withdraw request of '. $payment->amount .' '. $payment->currency .' has been declined by '. Auth::guard('admin')->user()->name .'.');
                session()->flash('error', true);
            }

            //email notification receive
            if ($payment->payment_type == 'TopUp') {
                $notification_to = DB::table('notifications')->where('email', '=', $payment->to)->first();

                if ($notification_to->approve_decline == 1) {
                    $greeting = 'Hi there,';
                    $p1 = 'your TopUp request of <b>'.$payment->amount.' '.$payment->currency.'</b> has been <b>declined</b> on '.$current_date.'.';
                    $p2 = 'Reference number for this payment is <b>'.$payment->payment_id.'</b>.';

                    Mail::send('emails.notification', [
                        'payment' => $payment,
                        'greeting' => $greeting,
                        'p1' => $p1,
                        'p2' => $p2
                        ], function ($message) use ($payment) {
                            $message->to(''.$payment->to.'');
                            $message->subject('+Wallet TopUp');
                        });
                }
            }
            if ($payment->payment_type == 'Withdraw') {
                $notification_to = DB::table('notifications')->where('email', '=', $payment->from)->first();

                if ($notification_to->approve_decline == 1) {
                    $greeting = 'Hi there,';
                    $p1 = 'your Withdraw request of <b>'.$payment->amount.' '.$payment->currency.'</b> has been <b>declined</b> on '.$current_date.'.';
                    $p2 = 'Reference number for this payment is <b>'.$payment->payment_id.'</b>.';

                    Mail::send('emails.notification', [
                        'payment' => $payment,
                        'greeting' => $greeting,
                        'p1' => $p1,
                        'p2' => $p2
                        ], function ($message) use ($payment) {
                            $message->to(''.$payment->from.'');
                            $message->subject('+Wallet Withdraw');
                        });
                }
            }

            return Redirect::to(URL::previous());
        }
    }
    public function document(Request $request, $payment_id)
    {
        // getting all of the post data
        $file = ['upload_file' => Input::file('upload_file')];

        $validator = Validator::make($request->all(), [
                       'name' => 'required',
                    'upload_file' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#document")
                ->withErrors($validator)
                ->withInput();
        }

        // checking file is valid.
        if (Input::file('upload_file')->isValid()) {
            $destinationPath = 'uploads/documents'; // upload path
        $extension = Input::file('upload_file')->getClientOriginalExtension(); // getting image extension
        $fileName = rand(11111, 99999).'.'.$extension; // renameing image
        Input::file('upload_file')->move($destinationPath, $fileName); // uploading file to given path
        }

        $document_id=(int)$document_id=DB::table('documents')->orderBy('document_id', 'desc')->value('document_id');
        if ($document_id < 1) {
            $document_id=1010;
        } else {
            $document_id = $document_id + 1;
        }

        $document = new Document($request->all());
        $document['document_id'] = $document_id;
        $document['payment_id'] = $payment_id;
        $document['name'] = $request->name;
        $document['file_name'] = $fileName;
        $document['file_path'] = $destinationPath.'/'.$fileName;
        $document['file_type'] = $extension;
        $document['added_by'] = Auth::guard('admin')->user()->name;
        $document->save();

        $activity = new Activity();
        $activity['admin_id'] = Auth::guard('admin')->user()->admin_id;
        $activity['added_by'] = Auth::guard('admin')->user()->name;
        $activity['interface'] = 'Admin';
        $activity['module'] = 'payments';
        $activity['activity'] = 'New document '. $document->name .' has been added to Payment ID '.$payment_id.' by '. Auth::guard('admin')->user()->name .'';
        $activity->save();

        session()->flash('toastr_alert', '<b>Added!</b> New document '. $document->name .' has been added to Payment ID '.$payment_id.' by '. Auth::guard('admin')->user()->name .'');
        session()->flash('success', true);

        return Redirect::to(URL::previous());
    }

    public function documentcsv(Request $request)
    {

        // getting all of the post data
        // $file = array('upload_file' => Input::file('upload_file'));

        $validator = Validator::make($request->all(), [
                       'upload_file' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#documentcsv")
                ->withErrors($validator)
                ->withInput();
        }

        # Open the File.
        if (($handle = fopen(Input::file('upload_file'), "r")) !== false) {
            # Set the parent multidimensional array key to 0.
            $nn = 0;
            while (($data = fgetcsv($handle, 1000, ",")) !== false) {
                # Count the total keys in the row.
                $c = count($data);
                # Populate the multidimensional array.
                for ($x=0;$x<$c;$x++) {
                    $csvarray[$nn][$x] = $data[$x];
                }
                $nn++;
            }
            # Close the File.
            fclose($handle);
        }
        # Print the contents of the multidimensional array.

        if (count($csvarray) >0) {
            $payment_id=(int)$payment_id=DB::table('payments')->orderBy('payment_id', 'desc')->value('payment_id');

            foreach ($csvarray as $key=>$val) {
                if ($val[1]!='orginal_date') {
                    $orginal_date   =$val[1];
                    $reference_id   =$val[2];
                    $from           =$val[3];
                    $merchant_id    =$val[4];
                    $terminal_id    =$val[5];
                    $description    =$val[6];
                    $currency       =$val[7];
                    $amount         =$val[8];
                    $fee            =$val[9];
                    $settlement_date=$val[10];


                    $terminals = DB::table('terminals')
                    ->where('terminal_id', '=', $terminal_id)
                    ->where('merchant_id', '=', $merchant_id)
                    ->where('settlement_currency', '=', $currency)
                    ->first();


                    if (count($terminals) == 1) {
                        $payment = new Payment();


                        if ($payment_id < 1) {
                            $payment_id=1010;
                        } else {
                            $payment_id = $payment_id + 1;
                        }
                        $payment['payment_id'] =$payment_id;

                        $chkpayment=DB::table('payments')
                        ->where('terminal_id', '=', $terminal_id)
                        ->where('merchant_id', '=', $merchant_id)
                        //->where('orginal_date', '=', $orginal_date)
                        ->where('reference_id', '=', $reference_id)
                        ->first();

                        if (count($chkpayment) == 0) {
                            $payment['orginal_date'] =$orginal_date;
                            $payment['reference_id'] =$reference_id;
                            $payment['from'] =$from;
                            $payment['merchant_id'] =$merchant_id;
                            $payment['terminal_id'] =$terminal_id;
                            $payment['description'] =$description;
                            $payment['currency'] =$currency;
                            $payment['amount'] =$amount;
                            $payment['fee'] =$fee;
                            $payment['settlement_date'] =$settlement_date;
                            $payment['payment_method'] ='Bank';
                            $payment['payment_type'] ='TransferIn';
                            $payment['status'] ='Approved';
                            $payment['added_by'] = Auth::guard('admin')->user()->name;
                            $payment->save();
                        }
                    }
                }
            }
            $activity = new Activity();
            $activity['admin_id'] = Auth::guard('admin')->user()->admin_id;
            $activity['added_by'] = Auth::guard('admin')->user()->name;
            $activity['interface'] = 'Admin';
            $activity['module'] = 'Payment';
            $activity['activity'] = 'New Payment batch Upload has been added by <a href="/admin/usermanagement/users/'. Auth::guard('admin')->user()->id .'">'. Auth::guard('admin')->user()->name .'</a>.';

            Auth::guard('admin')->user()->activities()->save($activity);
        }

        return Redirect::to(URL::previous());
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function documentview($payment_id, $document_id)
    {
        $document = DB::table('documents')->where('document_id', '=', $document_id)->first();

        $pagetitle = 'Payment';
        $breadcrumb_level1 = '<a href="/admin/payments">Payments</a>';
        $breadcrumb_level2 = '<a href="/admin/payments/'.$payment_id.'">Payment</a>';
        $breadcrumb_level3 = '<a href="/admin/payments/'.$payment_id.'/document/'.$document_id.'">Document</a>';

        return view('admin/modules/payments/document', compact('document', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'breadcrumb_level3', 'payment_id', 'document_id'));
    }


    public function pdf_convert(Request $request)
    {
        $data['item_per_page'] = $item_per_page = !empty($request->rows)?$request->rows:ROW_PER_PAGE;
        $data['pg']='?'.http_build_query($request->all());

        $queryResult=Payment::search($request, $data)->toArray();

        $fields = ['payment_id', 'orginal_date','reference_id', 'payment_type', 'description', 'currency', 'amount', 'fee',  'created_at'];
        $headers = ['Payment ID','Orginal Date', 'Reference ID', 'Transfer Type', 'Description', 'Currency', 'Amount', 'Fee',  'Datetime'];
        $pagetitle = 'Payments';
        $print_type='landscape';
        $pdf_name = 'payments_'.time();
        return $this->common->dataToPdf($queryResult['data'], $fields, $pdf_name, $headers, $pagetitle, $print_type);
    }

    public function csv_convert(Request $request)
    {
        $data['item_per_page'] = $item_per_page = !empty($request->rows)?$request->rows:ROW_PER_PAGE;
        $data['pg']='?'.http_build_query($request->all());

        $queryResult = Payment::search($request, $data)->toArray();
        $fields = ['payment_id', 'orginal_date','reference_id', 'payment_type', 'description', 'currency', 'amount', 'fee',  'created_at'];
        $headers = ['Payment ID','Orginal Date', 'Reference ID', 'Transfer Type', 'Description', 'Currency', 'Amount', 'Fee',  'Datetime'];
        $csv_name = 'payments_'.time();
        $this->common->dataToCSV($queryResult['data'], $fields, $csv_name, $headers);
    }
}
