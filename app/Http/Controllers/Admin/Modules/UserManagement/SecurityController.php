<?php

namespace App\Http\Controllers\Admin\Modules\Usermanagement;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

use DB;
use Validator;
use Auth;
use App\Admin;
use App\Bin;
use App\Http\Controllers\Controller;
use App\Http\Helper\Common;

use App\Traits\auth\CustomResetPassword;
use App\Traits\auth\CustomTwoStepAuthenticates;

class SecurityController extends Controller
{
    use CustomResetPassword, CustomTwoStepAuthenticates;
    
    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    public $redirectTo = '/admin';
    public $userToRedirect = 'admin';
    protected $common;

    public function __construct()
    {
        $this->middleware('admin');
        $this->common = new Common;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($admin_id)
    {
        $admin_user_id=Auth::guard('admin')->user()->admin_id;

        if ($admin_user_id!=$admin_id) {
            return Redirect::to('admin/usermanagement/users')->withErrors("Invalid Access")->withInput();
        }
        $pagetitle = 'Security';
        $breadcrumb_level1 = 'User Management';
        $breadcrumb_level2 = '<a href="/admin/usermanagement/users">Users</a>';
        $breadcrumb_level3 = '<a href="/admin/usermanagement/users/'.$admin_id.'">User</a>';
        $breadcrumb_level4 = '<a href="/admin/usermanagement/users/'.$admin_id.'/security">Security</a>';

        return view('admin/modules/usermanagement/security', compact('pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'breadcrumb_level3', 'breadcrumb_level4', 'admin_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($admin_id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($admin_id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $admin_id)
    {

        $admin = Admin::where('admin_id', '=', $admin_id)->first();
        
        if ($this->changePassValidation($request) !== true) {

            return Redirect::to(url('admin/usermanagement/users/'.$admin_id.'/security#password'))->withErrors($this->changePassValidation($request))->withInput();

        } else {

            return $this->attemptChangePassword($request, $admin, $admin_id);
            
        }

    }

    public function two_step(Request $request)
    {
        $admin_id = $request->admin_id;
        $admin = Admin::where('admin_id', $admin_id)->first();

        $this->setTwoStep($request, $admin);
        
    }
}
