<?php

namespace App\Http\Controllers\Admin\Modules\Usermanagement;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

use DB;
use Validator;
use App\User;
use App\Activity;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserActivityController extends Controller
{   
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($admin_id)
    {
        $activities =  DB::table('activities')->where('admin_id', '=', $admin_id)->orderBy('created_at','desc')->paginate(10);
        $name = DB::table('admins')->where('admin_id', [$admin_id])->value('name');
        $pagetitle = $name;
        $breadcrumb_level1 = 'User Management';
		$breadcrumb_level2 = '<a href="/admin/usermanagement/users">Users</a>';
        $breadcrumb_level3 = '<a href="/admin/usermanagement/users/'. $admin_id .'">User</a>';
        $breadcrumb_level4 = '<a href="/admin/usermanagement/users/'. $admin_id .'/useractivity">User Activity</a>';
        
        return view('admin/modules/usermanagement/useractivity', compact('activities', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'breadcrumb_level3', 'breadcrumb_level4', 'admin_id'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
          
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
  
    }
}
