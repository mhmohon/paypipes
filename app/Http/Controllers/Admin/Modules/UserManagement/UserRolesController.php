<?php

namespace App\Http\Controllers\Admin\Modules\UserManagement;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use DB;
use Validator;
use Auth;
use App\Activity;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserRolesController extends Controller
{   
    public function __construct()
    {
        $this->middleware('admin');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles =  Role::latest()->get();
        $pagetitle = 'User Roles';
        $breadcrumb_level1 = 'User Management';
        $breadcrumb_level2 = '<a href="/admin/usermanagement/userroles">User Roles</a>';
        
        return view('admin/modules/usermanagement/userroles', compact('roles', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles =  Role::latest()->get();
        $pagetitle = 'User Roles';
        $breadcrumb_level1 = 'User Management';
        $breadcrumb_level2 = '<a href="/admin/usermanagement/userroles">User Roles</a>';
        
        return view('admin/modules/usermanagement/userroles', compact('roles', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#add")
                ->withErrors($validator)
                ->withInput();
        }
        
		$role = Role::create([
			'name' => $request->name,
			'status' => 'Active',
			'added_by' => Auth::guard('admin')->user()->name,
			]);
		
		$activity = new Activity();
        $activity['admin_id'] = Auth::guard('admin')->user()->id;
		$activity['added_by'] = Auth::guard('admin')->user()->name;
		$activity['interface'] = 'Admin';
        $activity['module'] = 'UserManagement';
        $activity['activity'] = 'New Role '. $role->name .' has been added to <a href="/admin/usermanagement/userroles">User Roles</a> by <a href="/admin/usermanagement/users/'.Auth::guard('admin')->user()->id.'">'. Auth::guard('admin')->user()->name .'</a>.';
        
        Auth::guard('admin')->user()->activities()->save($activity);
        
		session()->flash('toastr_alert', '<b>Added!</b> New Role '. $role->name .' has been added to <a href="/admin/usermanagement/userroles">User Roles</a> by <a href="/admin/usermanagement/users/'. Auth::guard('admin')->user()->id .'">'. Auth::guard('admin')->user()->name .'</a>.');
		session()->flash('success', true);
		
        return Redirect::to(URL::previous());
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($rid)
    {
		$role = Role::findOrFail($id = $rid);
        $pagetitle = $role->name;
        $breadcrumb_level1 = 'User Management';
        $breadcrumb_level2 = '<a href="/admin/usermanagement/userroles">User Roles</a>';
        $breadcrumb_level3 = '<a href="/admin/usermanagement/userrole/'.$rid.'">User Role</a>';
        
        return view('admin/modules/usermanagement/userrole', compact('role', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'breadcrumb_level3', 'rid'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($rid)
    {
        $role = Role::findOrFail($id = $rid);
        $pagetitle = $role->name;
        $breadcrumb_level1 = 'User Management';
        $breadcrumb_level2 = '<a href="/admin/usermanagement/userroles">User Roles</a>';
        $breadcrumb_level3 = '<a href="/admin/usermanagement/userrole/'.$rid.'">User Role</a>';
        
        return view('admin/modules/usermanagement/userrole', compact('role', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'breadcrumb_level3', 'rid'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $rid)
    {
        $validator = Validator::make($request->all(), [
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'street' => 'required',
            'city' => 'required',
            'country' => 'required',
            'postcode' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#edit")
                ->withErrors($validator)
                ->withInput();
        }
        
        $customer = Customer::findOrFail($id = $rid);
        $pagetitle = 'Customer';
        $breadcrumb_level1 = 'User Management';
        $breadcrumb_level2 = '<a href="/admin/usermanagement/userroles">User Roles</a>';
        $breadcrumb_level3 = '<a href="/admin/usermanagement/userrole/'.$rid.'">User Role</a>';
        
        $customer->update($request->all());
        
        return view('admin/modules/customers/customer', compact('customer', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'breadcrumb_level3', 'rid'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
