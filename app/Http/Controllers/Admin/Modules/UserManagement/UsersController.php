<?php

namespace App\Http\Controllers\Admin\Modules\Usermanagement;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

use Mail;
use DB;
use Validator;
use Auth;
use App\Activity;
use App\Admin;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Helper\Common;
use Password;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Foundation\Auth\ResetsPasswords;
use App\Libraries\EncryptDecrypt;
use App\PasswordResets;
use App\Role;
use App\Role_user;

class UsersController extends Controller
{   
    //use SendsPasswordResetEmails;
    protected $common, $encdec;
    public function __construct()
    {
        $this->middleware('admin');
        $this->common = new Common;
        $this->encdec = new EncryptDecrypt('paypies');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $admins = Admin::where(function($query) use ($request) {
			if (($search = $request->get('search'))) {
				$query->orWhere('admin_id', 'like', '%' . $search . '%');
				$query->orWhere('first_name', 'like', '%' . $search . '%');
				$query->orWhere('last_name', 'like', '%' . $search . '%');
				$query->orWhere('name', 'like', '%' . $search . '%');
				$query->orWhere('email', 'like', '%' . $search . '%');
				$query->orWhere('status', 'like', $search . '%');
			}
		})
		->orderBy('created_at','desc')
		->paginate(10);
		
		$pagetitle = 'Users';
        $breadcrumb_level1 = 'User Management';
        $breadcrumb_level2 = '<a href="/admin/usermanagement/users">Users</a>';
        $uid = '0';
        $rid = '0';
        
        return view('admin/modules/usermanagement/users', compact('admins', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'uid', 'rid'));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
    	/*$req = new Request;
        $req->email = 'sami.cseseu@gmail.com';
        $user = Admin::where('email', 'sami.cseseu@gmail.com')->first();
    	$passToken = Password::getRepository()->create($user);
    	echo $passToken;
    	echo \Hash::make($passToken);
    	exit;*/

        $admins = Admin::where(function($query) use ($request) {
			//filter by keyword
			if (($search = $request->get('search'))) {
				$query->orWhere('admin_id', 'like', '%' . $search . '%');
				$query->orWhere('first_name', 'like', '%' . $search . '%');
				$query->orWhere('last_name', 'like', '%' . $search . '%');
				$query->orWhere('name', 'like', '%' . $search . '%');
				$query->orWhere('email', 'like', '%' . $search . '%');
				$query->orWhere('status', 'like', $search . '%');
			}
		})
		->orderBy('created_at','desc')
		->paginate(10);
		
		$pagetitle = 'Users';
        $breadcrumb_level1 = 'User Management';
        $breadcrumb_level2 = '<a href="/admin/usermanagement/users">Users</a>';
        $uid = '0';
        $rid = '0';
        $roles = Role::pluck('display_name', 'id')->toArray();

        return view('admin/modules/usermanagement/users', compact('admins', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'uid', 'rid', 'roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:admins',
            //'password' => 'required|confirmed|min:6',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#add")
                ->withErrors($validator)
                ->withInput();
        }
        
        $admin = new Admin($request->all());
		
		$admin_id=(int)$admin_id=DB::table('admins')->orderBy('admin_id','desc')->value('admin_id');
		if($admin_id < 1) $admin_id=1010;
		else $admin_id = $admin_id + 1;
		
        $code = $this->encdec->makeRandomPassword2(50);
		$admin['admin_id'] = $admin_id;
        $admin['verify_token'] = $code;
        $admin['token_time'] = time();
		$admin['name'] = $request->first_name.' '.$request->last_name;
		$admin['password'] = $this->common->crypto_encrypt($code, 'key_user');		
        $admin['status'] = 'Inactive';
		$admin['added_by'] = Auth::guard('admin')->user()->name;	
		$admin->save();

        $role_user = new Role_user;
        $role_user->user_id = $admin_id;
        $role_user->role_id = $request->role;
        $role_user->added_by = Auth::guard('admin')->user()->admin_id;
        $role_user->save();

        /*$passToken = Password::getRepository()->create($admin);
		$data['greeting'] = 'Thank you for register';
        $data['p1'] = 'You have registered account on PayPipes.Please set you password by clicking  below link';
        $data['p2'] = url('admin/password/reset/'.$passToken);
        $data['email'] = $request->email;
        $data['first_name'] = $request->name;
        @Mail::send('emails.merchent_register', $data, function($message) use($data) {
            $message->to($data['email'], $data['first_name'])->subject('PayPipes Registration Successful');
        });*/

        $data['subject'] = 'PayPipes - Account Activation';
        $data['preview_text'] = 'Just one more step ...';
        $data['main_message'] = 'Just one more step ...';
        $data['p1'] = 'Click the button below to activate your account.';
        $data['button_link'] = url('admin/verify-email/'.$code);
        $data['button'] = 'Activate Account';
        $data['email'] = $request->email;
        $data['first_name'] = $request->name;
        @Mail::send('emails.app_email', $data, function($message) use($data) {
            $message->to($data['email'], $data['first_name'])->subject('PayPipes - Account Activation');
        });
		
		$activity = new Activity ();
        $activity['admin_id'] = Auth::guard('admin')->user()->admin_id;
		$activity['added_by'] = Auth::guard('admin')->user()->name;
		$activity['interface'] = 'Admin';
        $activity['module'] = 'Usermanagement';
        $activity['activity'] = 'New Admin '.$request->first_name.' '.$request->last_name.' has been added by '. Auth::guard('admin')->user()->name .'.';
        $activity->save();
        
		session()->flash('toastr_alert','<b>Added!</b> New Admin '.$request->first_name.' '.$request->last_name.' has been added by '. Auth::guard('admin')->user()->name .'.');
		session()->flash('success', true);
		
        return Redirect::to(URL::previous());
    }

    public function password_reset_link_send($user)
    {
        $data['first_name'] = $user->first_name;

        $token = str_random(100); // Generate random string values - limit 100

        $password_resets = new PasswordResets;

        $password_resets->email      = $user->email;
        $password_resets->token      = $data['token'];
        $password_resets->created_at = date('Y-m-d H:i:s');
        
        $password_resets->save(); // Insert a generated token and email in password_resets table

        $data['subject'] = 'PayPipes - Set Password';
        $data['preview_text'] = 'Set your account password';
        $data['main_message'] = 'Set your account password';
        $data['p1'] = 'Click the button below to set your account password.';
        $data['button_link'] = url('admin/set-password/'.$code);
        $data['button'] = 'Set Password';
        $data['email'] = $user->email;
        $data['first_name'] = $user->first_name;
        @Mail::send('emails.app_email', $data, function($message) use($data) {
            $message->to($data['email'], $data['first_name'])->subject('PayPipes - Set Password');
        });
        
        return true;
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($admin_id)
    {

		$admin = DB::table('admins')->where('admin_id', '=', $admin_id)->first();
        $pagetitle = $admin->name;
        $breadcrumb_level1 = 'User Management';
        $breadcrumb_level2 = '<a href="/admin/usermanagement/users">Users</a>';
        $breadcrumb_level3 = '<a href="/admin/usermanagement/users/'.$admin_id.'">User</a>';
        
        return view('admin/modules/usermanagement/user', compact('admin', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'breadcrumb_level3', 'admin_id'));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($admin_id)
    {
        $admin = DB::table('admins')->where('admin_id', '=', $admin_id)->first();
        $pagetitle = $admin->name;
        $breadcrumb_level1 = 'User Management';
        $breadcrumb_level2 = '<a href="/admin/usermanagement/users">Users</a>';
        $breadcrumb_level3 = '<a href="/admin/usermanagement/users/'.$admin_id.'">User</a>';
        
        return view('admin/modules/usermanagement/user', compact('admin', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'breadcrumb_level3', 'admin_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $admin_id)
    {
        //
    }
	
	/**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function lockunlock(Request $request, $admin_id)
    {
        $admin = Admin::where('admin_id', '=', $admin_id)->firstOrFail(); 
		$admin->status = $request->status;		
		$admin->bad_login = 0;
		$admin->updated_by = Auth::guard('admin')->user()->name;
        $admin->update($request->all());
		
		
		
        
        $activity = new Activity ();
        $activity['admin_id'] = Auth::guard('admin')->user()->admin_id;
		$activity['added_by'] = Auth::guard('admin')->user()->name;
		$activity['interface'] = 'Admin';
        $activity['module'] = 'Usermanagement';      
        if ($request->status === 'Active') {$activity['activity'] = 'Admin '. $admin->first_name .' '. $admin->last_name .' has been unlocked by '. Auth::guard('admin')->user()->name .'.';}
        elseif ($request->status === 'Locked') {$activity['activity'] = 'Admin '. $admin->first_name .' '. $admin->last_name .' has been locked by '. Auth::guard('admin')->user()->name .'.';}
        $activity->save();
        
		if ($request->status === 'Active') {
			session()->flash('toastr_alert','<b>Unlocked!</b> Admin '. $admin->first_name .' '. $admin->last_name .' has been unlocked by '. Auth::guard('admin')->user()->name .'.');
			session()->flash('info', true);
		}
		elseif ($request->status === 'Locked') {
			session()->flash('toastr_alert','<b>Locked!</b> Admin '. $admin->first_name .' '. $admin->last_name .' has been locked by '. Auth::guard('admin')->user()->name .'.');
			session()->flash('info', true);
		}
		 
        return Redirect::to(URL::previous());
    }
}
