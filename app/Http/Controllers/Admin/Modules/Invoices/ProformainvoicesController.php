<?php

namespace App\Http\Controllers\Admin\Modules\Invoices;

use DB;
use Auth;
use Mail;
use App\Psp;
use Validator;
use App\Invoice;
use App\Setting;
use App\Activity;
use App\Currency;
use App\Merchant;
use App\Bankaccount;
use App\Proformainvoice;
use App\Http\Helper\Common;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class ProformainvoicesController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
        $this->common = new Common;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['item_per_page']=$item_per_page = !empty($request->rows)?$request->rows:ROW_PER_PAGE;
        $data['proformas']=Proformainvoice::searchAdmin($request);

        $data['currencies'] = Currency::currencylist();
        $data['bankaccounts'] = Bankaccount::accountlist();

        $data['pagetitle']  = 'Pro Forma';
        $data['breadcrumb_level1']= '<a href="/admin/proformainvoices">Proforma</a>';

        $data['item_per_page_url'] = url('admin/proformainvoices');
        $data['pg']='?'.http_build_query($request->all());
        $request->flash();

        return view('admin/modules/invoices/proformainvoices', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'tax.required' => 'The vat field is required!',
            'invoice_date.required' => 'The proforma_date field is required!',
        ];


        $validator = Validator::make($request->all(), [
            'description' => 'required',
            'tax' => 'required|numeric',
            'invoice_date' => 'required',
            'due_date' => 'required',
            'item_others_fee' => 'required',
            'amount_others_fee' => 'required|numeric',
            'qty_others_fee' => 'required|numeric',
            'currency' => 'required',
            'bank_account_id' => 'required',
        ], $messages);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#add")
                ->withErrors($validator)
                ->withInput();
        }

        $proforma = new Proformainvoice($request->all());

        $proforma_id= Proformainvoice::proformaid();

        $others_fee = $request->amount_others_fee * $request->qty_others_fee;
        $tax = $others_fee * ($proforma->tax/100);
        $total = $others_fee + $tax;

        $proforma['proforma_id'] = $proforma_id;
        $proforma['merchant_id'] = $request->merchant_id;
        $proforma['psp_id'] = $request->psp_id;
        $proforma['invoice_date'] = date('Y-m-d', strtotime($request->invoice_date));
        $proforma['due_date'] = date('Y-m-d', strtotime($request->due_date));
        $proforma['total_others_fee'] = $others_fee;
        $proforma['total_tax'] = $tax;
        $proforma['status'] = 'Pending';
        $proforma['added_by'] = Auth::guard('admin')->user()->name;
        $proforma['total'] = $total;
        $proforma->save();

        $activity = new Activity();
        $activity['added_by'] = Auth::guard('admin')->user()->name;
        $activity['interface'] = 'Admin';
        $activity['module'] = 'Proforma';
        $activity['activity'] = 'New Proforma #'. $proforma->proforma_id .' has been added by '. Auth::guard('admin')->user()->name .'.';
        $activity->save();

        session()->flash('toastr_alert', '<b>Added!</b> New Proforma #'. $proforma->proforma_id .' has been added by '. Auth::guard('admin')->user()->name .'.');
        session()->flash('success', true);

        return Redirect::to(URL::previous());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view(request $request, $id)
    {
        $data['item_per_page']=$item_per_page = !empty($request->rows)?$request->rows:ROW_PER_PAGE;
        $data['proforma']=$proforma = Proformainvoice::where('id', '=', $id)->first();
        $data['setting']=Setting::where('id', '=', 1)->first();
        $data['bankaccount']=DB::table('bankaccounts')->where('id', '=', $proforma->bank_account_id)->first();
        $data['bankaccounts'] = Bankaccount::accountlist();

        if (!empty($proforma->merchant_id)) {
            $data['client']=Merchant::where('merchant_id', '=', $proforma->merchant_id)->first();
        } else {
            $data['client']=Psp::where('psp_id', '=', $proforma->psp_id)->first();
        }

        $data['currencies'] = Currency::currencylist();



        $data['item_per_page_url'] = url('admin/proformainvoices');
        $data['pg']='?'.http_build_query($request->all());
        $request->flash();
        $data['pagetitle']  = 'Pro Forma';
        $data['breadcrumb_level1']= '<a href="/admin/proformainvoices">Proforma</a>';
        $data['breadcrumb_level2']= '<a href="/admin/proformainvoices/'.$id.'">Proforma Details</a>';

        return view('admin/modules/invoices/proformainvoice', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'tax.required' => 'The vat field is required!',
            'invoice_date.required' => 'The proforma_date field is required!',
        ];

        $validator = Validator::make($request->all(), [
            'description' => 'required',
            'tax' => 'required|numeric',
            'invoice_date' => 'required',
            'due_date' => 'required',
            'item_others_fee' => 'required',
            'amount_others_fee' => 'required|numeric',
            'qty_others_fee' => 'required|numeric',
            'currency' => 'required',
            'bank_account_id' => 'required',
        ], $messages);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#edit")
                ->withErrors($validator)
                ->withInput();
        }

        $proforma = Proformainvoice::where('id', '=', $id)->first();

        $creation_fee = $proforma->amount_creation_fee * $proforma->qty_creation_fee;
        $monthly_fee = $proforma->amount_monthly_fee * $proforma->qty_monthly_fee;
        $merchant_fee = $proforma->amount_merchant_fee * $proforma->merchant_fee;
        $terminal_fee = $proforma->amount_terminal_fee * $proforma->qty_terminal_fee;
        $gateway_fee = $proforma->amount_gateway_fee * $proforma->qty_gateway_fee;
        $transaction_fee = $proforma->amount_transaction_fee * $proforma->qty_transaction_fee;
        $others_fee = $request->amount_others_fee * $request->qty_others_fee;

        $sub_total = $creation_fee + $monthly_fee + $merchant_fee + $terminal_fee + $gateway_fee + $transaction_fee + $others_fee;
        $tax = $sub_total * ($request->tax/100);
        $total = $sub_total + $tax;

        //$proforma = Proformainvoice::findOrFail($id = $id);
        $proforma['merchant_id'] = $request->merchant_id;
        $proforma['psp_id'] = $request->psp_id;
        $proforma['invoice_date'] = date('Y-m-d', strtotime($request->invoice_date));
        $proforma['due_date'] = date('Y-m-d', strtotime($request->due_date));
        $proforma['description'] = $request->description;
        $proforma['tax'] = $request->tax;
        $proforma['total_tax'] = $tax;
        $proforma['item_others_fee'] = $request->item_others_fee;
        $proforma['amount_others_fee'] = $request->amount_others_fee;
        $proforma['qty_others_fee'] = $request->qty_others_fee;
        $proforma['total_others_fee'] = $others_fee;
        $proforma['currency'] = $request->currency;
        $proforma['total'] = $total;
        $proforma->update();

        $activity = new Activity();
        $activity['admin_id'] = Auth::guard('admin')->user()->id;
        $activity['added_by'] = Auth::guard('admin')->user()->name;
        $activity['interface'] = 'Admin';
        $activity['module'] = 'Proforma';
        $activity['activity'] = 'Proforma '. #$proforma->proforma_id .' has been updated by '. Auth::guard('admin')->user()->name .'.';
        $activity->save();

        session()->flash('toastr_alert', '<b>Updated!</b> Proforma #'. $proforma->proforma_id .' has been updated by '. Auth::guard('admin')->user()->name .'.');
        session()->flash('info', true);

        return Redirect::to(URL::previous());
    }


    /**
        * Store a newly created resource in storage.
        *
        * @param  \Illuminate\Http\Request  $request
        * @return \Illuminate\Http\Response
        */
    public function createInvoice($proforma_id)
    {
        $invoice_id= Invoice::invoiceid();

        $proforma = Proformainvoice::where('proforma_id', '=', $proforma_id)->firstOrFail();
        $invoice = Invoice::where('proforma_id', '=', $proforma_id)->first();

        if (count($invoice) > 0) {
            session()->flash('toastr_alert', '<b>Create Invoice #</b>'. $proforma->proforma_id .' Already exists.');
            session()->flash('error', true);
            return Redirect::to(URL::previous());
        } else {
            $invoice_copy = new Invoice($proforma->toArray());
            $invoice_copy->invoice_id = $invoice_id;
            $invoice_copy->added_by =Auth::guard('admin')->user()->name;
            $invoice_copy->save();

            $proforma->copy_proforma = 1;
            $proforma->update();

            session()->flash('toastr_alert', '<b>create Invoice #</b>'. $proforma->proforma_id .' has been Added by '. Auth::guard('admin')->user()->name .'.');
            session()->flash('info', true);
            return Redirect::to(URL::previous());
        }
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function review(Request $request, $id)
    {
        $proforma = Proformainvoice::where('id', '=', $id)->firstOrFail();

        $proforma['review'] = 'Reviewed';
        $proforma->save();

        $activity = new Activity();
        $activity['admin_id'] = Auth::guard('admin')->user()->id;
        $activity['added_by'] = Auth::guard('admin')->user()->name;
        $activity['interface'] = 'Admin';
        $activity['module'] = 'Proforma';
        $activity['activity'] = 'Proforma #'. $proforma->proforma_id .' has been reviewed by '. Auth::guard('admin')->user()->name .'.';
        $activity->save();

        session()->flash('toastr_alert', '<b>Reviewed!</b> Proforma #'. $proforma->proforma_id .' has been reviewed by '. Auth::guard('admin')->user()->name .'.');
        session()->flash('info', true);

        return Redirect::to(URL::previous());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function send(Request $request, $id)
    {
        $proforma = Proformainvoice::where('id', '=', $id)->firstOrFail();

        $proforma['email'] = 1;
        $proforma->save();

        if (!empty($proforma->merchant_id)) {
            $data['client']=$client=Merchant::where('merchant_id', '=', $proforma->merchant_id)->first();
        } else {
            $data['client']=$client=Psp::where('psp_id', '=', $proforma->psp_id)->first();
        }

        $data['subject'] = config('global.company').' - Proforma';
        $data['preview_text'] = $proforma->description;
        $data['main_message'] = 'Proforma';
        $data['p1'] = 'Proforma <b>#'.$proforma->proforma_id.'</b> for <b>'.$proforma->description.'</b> has been created. Please login to your '.config('global.company').' to view it.';
        $data['note'] = 'Proforma Number: '.$proforma->proforma_id.'<br>
                        Proforma Date: '.$proforma->invoice_date.'<br>
                        Due Date: '.$proforma->due_date.'<br>
                        Total: '.$proforma->total.' '.$proforma->currency;
        $data['email'] = $client->email;
        $data['name'] = $client->company_name;
        @Mail::send('emails.app_email', $data, function ($message) use ($data) {
            $message->to($data['email'], $data['name'])->subject(config('global.company').' - Proforma');
        });

        $activity = new Activity();
        $activity['admin_id'] = Auth::guard('admin')->user()->id;
        $activity['added_by'] = Auth::guard('admin')->user()->name;
        $activity['interface'] = 'Admin';
        $activity['module'] = 'Proforma';
        $activity['activity'] = 'Proforma #'. $proforma->proforma_id .' has been sent by '. Auth::guard('admin')->user()->name .' to '.$client->email.'.';
        $activity->save();

        session()->flash('toastr_alert', '<b>Sent!</b> Proforma #'. $proforma->proforma_id .' has been sent by '. Auth::guard('admin')->user()->name .' to '.$client->email.'.');
        session()->flash('info', true);

        return Redirect::to(URL::previous());
    }

    public function pdf_convert(Request $request, $proforma_id)
    {
        $data['invoice']=$proforma = Proformainvoice::where('proforma_id', '=', $proforma_id)->first();
        $data['setting']=Setting::where('id', '=', 1)->first();
        $data['bankaccount']=DB::table('bankaccounts')->where('id', '=', $proforma->bank_account_id)->first();

        if (!empty($proforma->merchant_id)) {
            $data['client']=Merchant::where('merchant_id', '=', $proforma->merchant_id)->first();
        } else {
            $data['client']=Psp::where('psp_id', '=', $proforma->psp_id)->first();
        }

        $pagetitle = $proforma->description;
        $print_type='portrait';
        $pdf_name = 'Proforma_'.$proforma_id.'.'.time();
        return $this->common->invoicePdf($data, $pdf_name, $pagetitle, $print_type, 'Proforma');
    }
}
