<?php

namespace App\Http\Controllers\Admin\Modules\Invoices;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

use Mail;
use DB;
use Validator;
use Auth;
use App\Activity;
use App\Invoice;
use App\Invoice_log;
use App\Currency;
use App\Bankaccount;
use App\Merchant;
use App\Setting;
use App\Psp;
use App\Http\Controllers\Controller;
use App\Http\Helper\Common;

class InvoicesController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
        $this->common = new Common;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['item_per_page']=$item_per_page = !empty($request->rows)?$request->rows:ROW_PER_PAGE;
        $data['invoices']=Invoice::searchAdmin($request);

        $data['currencies'] = Currency::currencylist();
        $data['bankaccounts'] = Bankaccount::accountlist();

        $data['pagetitle']  = 'Invoices';
        $data['breadcrumb_level1']= '<a href="/admin/invoices">Invoices</a>';

        $data['item_per_page_url'] = url('admin/invoices');
        $data['pg']='?'.http_build_query($request->all());
        $request->flash();

        return view('admin/modules/invoices/invoices', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'tax.required' => 'The vat field is required!',
        ];

        $validator = Validator::make($request->all(), [
            'description' => 'required',
            'tax' => 'required|numeric',
            'invoice_date' => 'required',
            'due_date' => 'required',
            'item_others_fee' => 'required',
            'amount_others_fee' => 'required|numeric',
            'qty_others_fee' => 'required|numeric',
            'currency' => 'required',
            'bank_account_id' => 'required',
        ], $messages);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#add")
                ->withErrors($validator)
                ->withInput();
        }

        $invoice = new Invoice($request->all());

        $invoice_id=date('Y');
        $invoiceid =(int)$invoiceid=DB::table('invoices')->where('invoice_id', 'like', $invoice_id . '%')->orderBy('invoice_id', 'desc')->value('invoice_id');
        if ($invoiceid < 1) {
            $invoice_id.='001';
        } else {
            $invoice_id = $invoiceid + 1;
        }

        $others_fee = $request->amount_others_fee * $request->qty_others_fee;
        $tax = $others_fee * ($invoice->tax/100);
        $total = $others_fee + $tax;

        $invoice['invoice_id'] = $invoice_id;
        $invoice['merchant_id'] = $request->merchant_id;
        $invoice['psp_id'] = $request->psp_id;
        $invoice['invoice_date'] = date('Y-m-d', strtotime($request->invoice_date));
        $invoice['due_date'] = date('Y-m-d', strtotime($request->due_date));
        $invoice['total_others_fee'] = $others_fee;
        $invoice['total_tax'] = $tax;
        $invoice['status'] = 'Pending';
        $invoice['added_by'] = Auth::guard('admin')->user()->name;
        $invoice['total'] = $total;
        $invoice->save();

        $activity = new Activity();
        $activity['added_by'] = Auth::guard('admin')->user()->name;
        $activity['interface'] = 'Admin';
        $activity['module'] = 'Invoice';
        $activity['activity'] = 'New Invoice #'. $invoice->invoice_id .' has been added by '. Auth::guard('admin')->user()->name .'.';
        $activity->save();

        session()->flash('toastr_alert', '<b>Added!</b> New Invoice #'. $invoice->invoice_id .' has been added by '. Auth::guard('admin')->user()->name .'.');
        session()->flash('success', true);

        return Redirect::to(URL::previous());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view(request $request, $id)
    {
        $data['item_per_page']=$item_per_page = !empty($request->rows)?$request->rows:ROW_PER_PAGE;
        $data['invoice']=$invoice = Invoice::where('id', '=', $id)->first();
        $data['invoice_logs'] = $invoice_logs = Invoice_log::where('invoice_id', '=', $invoice->invoice_id)->orderBy('created_at', 'desc')->paginate(10);
        $data['setting']=Setting::where('id', '=', 1)->first();
        $data['bankaccount']=DB::table('bankaccounts')->where('id', '=', $invoice->bank_account_id)->first();
        $data['bankaccounts'] = Bankaccount::accountlist();

        if (!empty($invoice->merchant_id)) {
            $data['client']=Merchant::where('merchant_id', '=', $invoice->merchant_id)->first();
        } else {
            $data['client']=Psp::where('psp_id', '=', $invoice->psp_id)->first();
        }

        $data['currencies'] = Currency::currencylist();



        $data['item_per_page_url'] = url('admin/invoices');
        $data['pg']='?'.http_build_query($request->all());
        $request->flash();
        $data['pagetitle']  = 'Invoice';
        $data['breadcrumb_level1']= '<a href="/admin/invoices">Invoices</a>';
        $data['breadcrumb_level2']= '<a href="/admin/invoices/'.$id.'">Invoice</a>';

        return view('admin/modules/invoices/invoice', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'tax.required' => 'The vat field is required!',
        ];

        $validator = Validator::make($request->all(), [
            'description' => 'required',
            'tax' => 'required|numeric',
            'invoice_date' => 'required',
            'due_date' => 'required',
            'item_others_fee' => 'required',
            'amount_others_fee' => 'required|numeric',
            'qty_others_fee' => 'required|numeric',
            'currency' => 'required',
            'bank_account_id' => 'required',
        ], $messages);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#edit")
                ->withErrors($validator)
                ->withInput();
        }

        $invoice = Invoice::where('id', '=', $id)->first();

        $creation_fee = $invoice->amount_creation_fee * $invoice->qty_creation_fee;
        $monthly_fee = $invoice->amount_monthly_fee * $invoice->qty_monthly_fee;
        $merchant_fee = $invoice->amount_merchant_fee * $invoice->merchant_fee;
        $terminal_fee = $invoice->amount_terminal_fee * $invoice->qty_terminal_fee;
        $gateway_fee = $invoice->amount_gateway_fee * $invoice->qty_gateway_fee;
        $transaction_fee = $invoice->amount_transaction_fee * $invoice->qty_transaction_fee;
        $others_fee = $request->amount_others_fee * $request->qty_others_fee;

        $sub_total = $creation_fee + $monthly_fee + $merchant_fee + $terminal_fee + $gateway_fee + $transaction_fee + $others_fee;
        $tax = $sub_total * ($request->tax/100);
        $total = $sub_total + $tax;

        $invoice = Invoice::findOrFail($id = $id);
        $invoice['merchant_id'] = $request->merchant_id;
        $invoice['psp_id'] = $request->psp_id;
        $invoice['invoice_date'] = date('Y-m-d', strtotime($request->invoice_date));
        $invoice['due_date'] = date('Y-m-d', strtotime($request->due_date));
        $invoice['description'] = $request->description;
        $invoice['tax'] = $request->tax;
        $invoice['total_tax'] = $tax;
        $invoice['item_others_fee'] = $request->item_others_fee;
        $invoice['amount_others_fee'] = $request->amount_others_fee;
        $invoice['qty_others_fee'] = $request->qty_others_fee;
        $invoice['total_others_fee'] = $others_fee;
        $invoice['currency'] = $request->currency;
        $invoice['total'] = $total;
        $invoice->update();

        $activity = new Activity();
        $activity['admin_id'] = Auth::guard('admin')->user()->id;
        $activity['added_by'] = Auth::guard('admin')->user()->name;
        $activity['interface'] = 'Admin';
        $activity['module'] = 'Invoice';
        $activity['activity'] = 'Invoice '. #$invoice->invoice_id .' has been updated by '. Auth::guard('admin')->user()->name .'.';
        $activity->save();

        session()->flash('toastr_alert', '<b>Updated!</b> Invoice #'. $invoice->invoice_id .' has been updated by '. Auth::guard('admin')->user()->name .'.');
        session()->flash('info', true);

        return Redirect::to(URL::previous());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function logpayment(Request $request, $invoice_id)
    {
        $validator = Validator::make($request->all(), [
            'payment_date' => 'required',
            'paid_amount' => 'required|numeric',
            'currency' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#$request->invoice_id")
                ->withErrors($validator)
                ->withInput();
        }

        $invoice = Invoice::where('invoice_id', '=', $invoice_id)->firstOrFail();
        $total_amount=$invoice->total;
        $paidamount = DB::table('invoice_logs')->where('invoice_id', '=', $invoice_id)->sum('amount');
        $amount=number_format($request->paid_amount, 2, '.', '')+$paidamount;
        $paymentstatus=$txtstatus="";

        $epsilon = 0.00001;
        $amount = (float)$amount;
        $total_amount = (float)$total_amount;

        if (abs($total_amount - $amount) < $epsilon) {
            $paymentstatus="Paid";
            $txtstatus="Full Paid";
        } elseif ($total_amount > $amount) {
            $paymentstatus="Partial";
            $txtstatus="Partially Paid";
        } elseif ($total_amount < $amount) {
            $validator->errors()->add('paid_amount', 'Invalid Amount!');
            return Redirect::to(URL::previous() . "#$request->invoice_id")
                ->withErrors($validator)
                ->withInput();
        }

        $invoice['status'] = $paymentstatus;
        $invoice->save();


        $invlog=[];
        $invlog['payment_date']=date('Y-m-d H:i:s', strtotime($request->payment_date));
        $invlog['amount']=number_format($request->paid_amount, 2, '.', '');
        $invlog['currency']=$request->currency;
        $invlog['invoice_id']=$request->invoice_id;
        $invlog['status'] = $paymentstatus;
        DB::table('invoice_logs')->insert($invlog);

        $activity = new Activity();
        $activity['admin_id'] = Auth::guard('admin')->user()->id;
        $activity['added_by'] = Auth::guard('admin')->user()->name;
        $activity['interface'] = 'Admin';
        $activity['module'] = 'Invoice';
        $activity['activity'] = 'Invoice #'. $invoice->invoice_id . ' has been ( '.$txtstatus.' ) on '. date(' F,d Y', strtotime($request->payment_date)) .' added by '. Auth::guard('admin')->user()->name .'.';
        $activity->save();

        session()->flash('toastr_alert', '<b>Payment Log!</b> Invoice #'. $invoice->invoice_id . ' has been ( '.$txtstatus.' ) on '. date(' F,d Y', strtotime($request->payment_date)) .' added by '. Auth::guard('admin')->user()->name .'.');
        session()->flash('info', true);

        return Redirect::to(URL::previous());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function review(Request $request, $id)
    {
        $invoice = Invoice::where('id', '=', $id)->firstOrFail();

        $invoice['review'] = 'Reviewed';
        $invoice->save();

        $activity = new Activity();
        $activity['admin_id'] = Auth::guard('admin')->user()->id;
        $activity['added_by'] = Auth::guard('admin')->user()->name;
        $activity['interface'] = 'Admin';
        $activity['module'] = 'Invoice';
        $activity['activity'] = 'Invoice #'. $invoice->invoice_id .' has been reviewed by '. Auth::guard('admin')->user()->name .'.';
        $activity->save();

        session()->flash('toastr_alert', '<b>Reviewed!</b> Invoice #'. $invoice->invoice_id .' has been reviewed by '. Auth::guard('admin')->user()->name .'.');
        session()->flash('info', true);

        return Redirect::to(URL::previous());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function send(Request $request, $id)
    {
        $invoice = Invoice::where('id', '=', $id)->firstOrFail();

        $invoice['email'] = 1;
        $invoice->save();

        if (!empty($invoice->merchant_id)) {
            $data['client']=$client=Merchant::where('merchant_id', '=', $invoice->merchant_id)->first();
        } else {
            $data['client']=$client=Psp::where('psp_id', '=', $invoice->psp_id)->first();
        }

        $data['subject'] = config('global.company').' - Invoice';
        $data['preview_text'] = $invoice->description;
        $data['main_message'] = 'Invoice';
        $data['p1'] = 'Invoice <b>#'.$invoice->invoice_id.'</b> for <b>'.$invoice->description.'</b> has been created. Please login to your '.config('global.company').' to view it.';
        $data['note'] = 'Invoice Number: '.$invoice->invoice_id.'<br>
                        Invoice Date: '.$invoice->invoice_date.'<br>
                        Due Date: '.$invoice->due_date.'<br>
                        Total: '.$invoice->total.' '.$invoice->currency;
        $data['email'] = $client->email;
        $data['name'] = $client->company_name;
        @Mail::send('emails.app_email', $data, function ($message) use ($data) {
            $message->to($data['email'], $data['name'])->subject(config('global.company').' - Invoice');
        });

        $activity = new Activity();
        $activity['admin_id'] = Auth::guard('admin')->user()->id;
        $activity['added_by'] = Auth::guard('admin')->user()->name;
        $activity['interface'] = 'Admin';
        $activity['module'] = 'Invoice';
        $activity['activity'] = 'Invoice #'. $invoice->invoice_id .' has been sent by '. Auth::guard('admin')->user()->name .' to '.$client->email.'.';
        $activity->save();

        session()->flash('toastr_alert', '<b>Sent!</b> Invoice #'. $invoice->invoice_id .' has been sent by '. Auth::guard('admin')->user()->name .' to '.$client->email.'.');
        session()->flash('info', true);

        return Redirect::to(URL::previous());
    }

    public function pdf_convert(Request $request, $invoice_id)
    {
        $data['invoice']=$invoice = Invoice::where('invoice_id', '=', $invoice_id)->first();
        $data['setting']=Setting::where('id', '=', 1)->first();
        $data['bankaccount']=DB::table('bankaccounts')->where('id', '=', $invoice->bank_account_id)->first();

        if (!empty($invoice->merchant_id)) {
            $data['client']=Merchant::where('merchant_id', '=', $invoice->merchant_id)->first();
        } else {
            $data['client']=Psp::where('psp_id', '=', $invoice->psp_id)->first();
        }

        $pagetitle = $invoice->description;
        $print_type='portrait';
        $pdf_name = 'Invoice_'.$invoice_id.'.'.time();
        return $this->common->invoicePdf($data, $pdf_name, $pagetitle, $print_type, 'Invoice');
    }
}
