<?php

namespace App\Http\Controllers\Admin\Modules\Dashboard;

use DB;
use Auth;
use Carbon\Carbon;
use App\Activity;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redis;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*Redis::set('name',"Kazi");
        $name = Redis::get('name');
        dd($name);
        */


        $pagetitle = 'Welcome '.Auth::guard('admin')->user()->first_name;
        $accountscount = DB::table('accounts')->count();

        $paymentscount = DB::table('payments')->count();
        $gatewayscount = DB::table('payment_methods')->count();

        $today = Carbon::now();
        Carbon::setToStringFormat('jS \o\f F, Y');

        $activities =  Activity::latest()->take(10)->get();

        return view('admin/modules/dashboard/dashboard', compact('pagetitle', 'accountscount', 'paymentscount', 'gatewayscount', 'today', 'activities'));
    }
}
