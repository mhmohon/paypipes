<?php
namespace App\Http\Controllers\Admin\Modules\Roles;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

use Auth;
use App\Http\Controllers\Controller;
use App\Role;
use App\Permission;
use App\Permission_role;
use App\Activity;
use App\Notification;
use Validator;
use Form;
use DB;

class RolesController extends Controller
{
    public function index(Request $request)
    {
        $roles = Role::where(function ($query) use ($request) {
            //filter by keyword
            if (($search = $request->get('search'))) {
                $query->orWhere('name', 'like', '%' . $search . '%');
                $query->orWhere('display_name', 'like', '%' . $search . '%');
                $query->orWhere('description', 'like', '%' . $search . '%');
                $query->orWhere('status', 'like', $search . '%');
            }
        })
        ->orderBy('created_at', 'desc')
        ->paginate(10);

        //$customers = DB::select('select customers.*, countries.country_name from customers left join
        //countries on countries.country_id=customers.country');

        $pagetitle = 'User Roles';
        $breadcrumb_level1 = 'User Management';
        $breadcrumb_level2 = '<a href="/admin/roles">User Roles</a>';

        $permissions = Permission::get()->groupBy('module')->toArray();

        return view('admin/modules/roles/roles', compact('roles', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'permissions'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:50|unique:roles',
            'display_name' => 'required|max:50',
            'description' => 'max:100'
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#add")
                ->withErrors($validator)
                ->withInput();
        }

        $role = new Role;
        $role->name = $request->name;
        $role->display_name = $request->display_name;
        if ($role->description != '') {
            $role->description = $request->description;
        }
        $role->added_by = Auth::guard('admin')->user()->admin_id;
        $role->status = 'Active';
        $role->save();

        foreach ($request->permission as $key => $value) {
            Permission_role::firstOrCreate(['permission_id' => $value, 'role_id' => $role->id]);
        }



        /*$notification = new Notification($request->all());

        $notification_id=(int)$notification_id=DB::table('notifications')->orderBy('notification_id','desc')->value('notification_id');
        if($notification_id < 1) $notification_id=1010;
        else $notification_id = $notification_id + 1;

        $notification['notification_id'] = $notification_id;
        $notification['added_by'] = Auth::guard('admin')->user()->name;
        $notification->save();*/

        $activity = new Activity();
        $activity['added_by'] = Auth::guard('admin')->user()->name;
        $activity['interface'] = 'Admin';
        $activity['module'] = 'Roles';
        $activity['activity'] = 'New Role <a href="/admin/roles/'.$role->id.'">'. $role->display_name .'</a> has been added by <a href="/admin/usermanagement/users/'. Auth::guard('admin')->user()->id .'">'. Auth::guard('admin')->user()->name .'</a>.';
        $activity->save();



        session()->flash('toastr_alert', '<b>Added!</b> New Role <a href="/admin/roles/'.$role->id.'">'. $role->display_name .'</a> has been added by <a href="/admin/usermanagement/users/'. Auth::guard('admin')->user()->id .'">'. Auth::guard('admin')->user()->name .'</a>.');
        session()->flash('success', true);

        return Redirect::to(URL::previous());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($role_id)
    {
        $role = DB::table('roles')->where('id', '=', $role_id)->first();

        $permissions = Permission::get()->groupBy('module')->toArray();

        $role_permissions = Permission_role::where('role_id', $role_id)->pluck('permission_id', 'id')->toArray();

        $pagetitle = $role->display_name;
        $breadcrumb_level1 = 'User Management';
        $breadcrumb_level2 = '<a href="/admin/roles">User Roles</a>';
        $breadcrumb_level3 = '<a href="/admin/roles/'. $role_id .'">User Role</a>';

        return view('admin/modules/roles/role', compact('role', 'permissions', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'breadcrumb_level3', 'role_id', 'role_permissions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $role_id)
    {
        $validator = Validator::make($request->all(), [
            'display_name' => 'required|max:50',
            'description' => 'max:100'
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#edit")
                ->withErrors($validator)
                ->withInput();
        }

        $role = Role::where('id', $role_id)->first();
        $role->display_name = $request->display_name;
        $role->description = $request->description;
        $role->updated_by = Auth::guard('admin')->user()->admin_id;
        $role->status = $request->status;
        $role->save();

        $stored_permissions = Permission_role::where('role_id', $role_id)->pluck('permission_id')->toArray();
        foreach ($stored_permissions as $key => $value) {
            if (!in_array($value, $request->permission)) {
                Permission_role::where(['permission_id' => $value, 'role_id' => $role_id])->delete();
            }
        }

        foreach ($request->permission as $key => $value) {
            Permission_role::firstOrCreate(['permission_id' => $value, 'role_id' => $role_id]);
        }



        $activity = new Activity();
        $activity['added_by'] = Auth::guard('admin')->user()->name;
        $activity['interface'] = 'Admin';
        $activity['module'] = 'Role';
        $activity['activity'] = 'Role <a href="/admin/roles/'. $role_id .'">'. $role->display_name .'</a> has been updated by <a href="/admin/usermanagement/users/'. Auth::guard('admin')->user()->id .'">'. Auth::guard('admin')->user()->name .'</a>.';
        $activity->save();

        session()->flash('toastr_alert', '<b>Updated!</b> Role <a href="/admin/roles/'. $role_id .'">'. $role->display_name .'</a> has been updated by <a href="/admin/usermanagement/users/'. Auth::guard('admin')->user()->id .'">'. Auth::guard('admin')->user()->name .'</a>.');
        session()->flash('info', true);

        return Redirect::to(URL::previous());
    }
}
