<?php

namespace App\Http\Controllers\AdminAuth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Password;
use Illuminate\Http\Request;
use App\Admin;
use App\Bin;
use App\Http\Helper\Common;
use Validator;
use DB;
use Session;
use App\Traits\auth\CustomResetPassword;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords, CustomResetPassword;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    public $redirectTo = '/admin';
    public $userToRedirect = 'admin';
    protected $common;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->common = new Common;
    }

    /**
     * Display the password reset view for the given token.
     *
     * If no token is present, display the link request form.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string|null  $token
     * @return \Illuminate\Http\Response
     */
    public function showResetForm(Request $request, $token = null)
    {
        $token_test = DB::table('password_resets')->where('token', $request->token)
                    ->count();
        if ($token_test) {
            return view('admin.auth.passwords.reset')->with(
            ['token' => $token, 'email' => $request->email]
        );
        } else {
            $this->common->flash_message('danger', 'Invalid Request');
            return view('admin.auth.passwords.invalid');
        }
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function reset(Request $request)
    {
        
        if ($this->resetValidation($request) !== true) {

            return back()->withErrors($this->resetValidation($request))->withInput(); // Form calling with Errors and Input values

        } else {
            $token = DB::table('password_resets')->where('email', $request->email)
                    ->where('token', $request->token)
                    ->count();
            $admin = Admin::where('email', $request->email)->first();


            if ($token && isset($admin->admin_id)) {

                $this->attemptResetPassword($request, $admin, $admin->admin_id);
                
            } else {

                $this->common->flash_message('danger', 'Invalid Request');
                return redirect('admin/password/reset/'.$request->token);
            }
        }
    }

    public function changeView()
    {
        if (!Session::has('uid')) {
            abort(403);
        }
        return view('admin.auth.passwords.change');
    }

    /**
    * Reset the given user's password.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\RedirectResponse
    */
    public function change(Request $request)
    {
        $admin_id = Session::get('uid');
        $admin = DB::table('admins')->where('admin_id', '=', $admin_id)->first();

        if ($this->changeValidation($request) !== true) {

            return back()->withErrors($this->changeValidation($request))->withInput(); // Form calling with Errors and Input values

        } else {

            $this->attemptChangePassword($request, $admin, $admin_id);
            
        }
    }


    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function broker()
    {
        return Password::broker('admins');
    }

    /**
     * Get the guard to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('admin');
    }
}
