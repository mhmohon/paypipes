<?php

namespace App\Http\Controllers\AdminAuth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Admin;
use App\Http\Helper\Common;
use Illuminate\Http\Request;
use App\Libraries\EncryptDecrypt;
use Session;
use Mail;
use Validator;
use App\PasswordResets;
use DateTime;
use DB;
use App\Traits\auth\CustomAuthenticatesUser;
use App\Traits\auth\CustomTwoStepAuthenticates;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use CustomAuthenticatesUser, CustomTwoStepAuthenticates;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    public $redirectTo = '/admin';
    public $redirectToLogin = '/admin/login';
    public $userToRedirect = 'admin';

    protected $common;
    protected $encdec;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
        $this->common = new Common;
        $this->encdec = new EncryptDecrypt('paypies');
    }

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showLoginForm()
    {
        return view('admin.auth.login');
    }
    

    public function login(Request $request)
    {

        $admin = Admin::where('email', $request->email)->first();

        if(!empty($admin)){
            
            return $this->checkUserLogin($request, $admin, $admin->admin_id);

        }else {

            $this->common->flash_message('danger', 'Log in Failed. Please Check Your Email/Password');
            return redirect()->back();
        }

    }


    public function logout()
    {
        Auth::guard('admin')->logout();
        return redirect('admin/login');
    }

    public function twoStepInfoSend(Request $request)
    {
        $admin_id = Session::get('uid');
        $admin = Admin::where('admin_id', '=', $admin_id)->first();

        if(!empty($admin)){

            return $this->sendTwoStepCode($admin);

        }else {
            abort(403);
        }

    }

    public function twoStepView()
    {
        if (!Session::has('uid')) {
            abort(403);
        }

        return view('admin.auth.two_step');
    }

    public function twoStep(Request $request)
    {
        $admin_id = Session::get('uid');
        $admin = Admin::where('admin_id', '=', $admin_id)->first();

        if ($this->fieldValidation($request) !== true) {

            return back()->withErrors($this->fieldValidation($request))->withInput(); // Form calling with Errors and Input values

        } else {

            return $this->checkTwoFactorCode($request, $admin);
            
        }
        
    }

    public function password_reset_link_send($user)
    {
        $data['first_name'] = $user->first_name;

        $token = str_random(100); // Generate random string values - limit 100

        $password_resets = new PasswordResets;

        $password_resets->email      = $user->email;
        $password_resets->token      = $data['token'];
        $password_resets->created_at = date('Y-m-d H:i:s');

        $password_resets->save(); // Insert a generated token and email in password_resets table

        $data['subject'] = config('global.company').' - Set Password';
        $data['preview_text'] = 'Set your account password';
        $data['main_message'] = 'Set your account password';
        $data['p1'] = 'Click the button below to set your account password.';
        $data['button_link'] = url('admin/set-password/'.$code);
        $data['button'] = 'Set Password';
        $data['email'] = $user->email;
        $data['first_name'] = $user->first_name;
        @Mail::send('emails.app_email', $data, function ($message) use ($data) {
            $message->to($data['email'], $data['first_name'])->subject(config('global.company').' - Set Password');
        });

        return true;
    }

    /*public function reset_password(Request $request)
    {
        if(!$_POST)
        {
            $password_resets = PasswordResets::whereToken($request->secret);

            if($password_resets->count())
            {
                $password_result = $password_resets->first();

                $datetime1 = new DateTime();
                $datetime2 = new DateTime($password_result->created_at);
                $interval  = $datetime1->diff($datetime2);
                $hours     = $interval->format('%h');

                if($hours >= 1)
                {
                    // Delete used token from password_resets table
                    $password_resets->delete();

                    $this->helper->flash_message('error', trans('messages.login.token_expired')); // Call flash message function
                    return redirect('login');
                }

                $data['result'] = User::whereEmail($password_result->email)->first();
                $data['token']  = $request->secret;

                return view('home.reset_password', $data);
            }
            else
            {
                $this->helper->flash_message('error', trans('messages.login.invalid_token')); // Call flash message function
                return redirect('login');
            }
        }
        else
        {
            // Password validation rules
            $rules = array(
            'password'              => 'required|min:6|max:30',
            'password_confirmation' => 'required|same:password'
            );

            // Password validation custom Fields name
            $niceNames = array(
            'password'              => 'New Password',
            'password_confirmation' => 'Confirm Password'
            );

            $validator = Validator::make($request->all(), $rules);
            $validator->setAttributeNames($niceNames);

            if ($validator->fails())
            {
                return back()->withErrors($validator)->withInput(); // Form calling with Errors and Input values
            }
            else
            {
                // Delete used token from password_resets table
                $password_resets = PasswordResets::whereToken($request->token)->delete();

                $user = User::find($request->id);

                $user->password = bcrypt($request->password);

                $user->save(); // Update Password in users table

                $this->helper->flash_message('success', trans('messages.login.pwd_changed')); // Call flash message function
                return redirect('login');
            }
        }
    }*/

    public function email_verify(Request $request)
    {
        $code = $request->code;
        $admin_has = Admin::where('verify_token', $code);

        if ($admin_has->count()) {
            $admin = $admin_has->first();

            $token = str_random(100);

            $password_resets = new PasswordResets;
            $password_resets->email      = $admin->email;
            $password_resets->token      = $token;
            $password_resets->created_at = date('Y-m-d H:i:s');
            $password_resets->save();

            $data['subject'] = config('global.company').' - Admin Account Registration';
            $data['preview_text'] = 'Thank you for joining us ...';
            $data['main_message'] = 'Welcome to PayPipes!';
            $data['p1'] = 'Thank you for joining us, '.$admin->name.'.';
            $data['p4'] = 'If you already setup you password please <a href="'.url('admin/login').'">login</a>. Otherwise click on the button below.';
            $data['button_link'] = url('admin/password/setup/'.$token);
            $data['button'] = 'Setup Password';
            $data['email'] = $admin->email;
            $data['first_name'] = $admin->name;
            @Mail::send('emails.app_email', $data, function ($message) use ($data) {
                $message->to($data['email'], $data['first_name'])->subject(config('global.company').' - Admin Account Registration');
            });

            Admin::where('verify_token', $code)->update(['status'=> 'Active', 'verify_token' => '', 'token_time' => 0]);
            $this->common->flash_message('success', 'Email successfully verified. Please Setup your password.');
            return redirect('admin/password/setup/'.$token);
        } else {
            $this->common->flash_message('danger', 'Invalid Code.');
            return redirect('admin/login');
        }
    }

    public function setup_password(Request $request)
    {
        if (!$_POST) {
            $password_resets = PasswordResets::whereToken($request->token);

            if ($password_resets->count()) {
                $password_result = $password_resets->first();

                $datetime1 = new DateTime();
                $datetime2 = new DateTime($password_result->created_at);
                $interval  = $datetime1->diff($datetime2);
                $hours     = $interval->format('%h');

                if ($hours >= 24) {
                    $password_resets->delete();

                    $this->common->flash_message('danger', 'Your token got expired');
                    return redirect('admin/login');
                }

                $data['result'] = Admin::whereEmail($password_result->email)->first();
                $data['token']  = $request->token;

                return view('admin.auth.setup_password', $data);
            } else {
                $this->common->flash_message('danger', 'Invalid Request');
                return redirect('admin/login');
            }
        } else {
            $rules = [
            'password'              => 'required|confirmed|regex:/^.*(?=.{3,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$/|between:8,16',
            'password_confirmation' => 'required'
            ];

            $niceNames = [
            'password'              => 'New Password',
            'password_confirmation' => 'Confirm Password'
            ];

            $customMessages = [
            "password.between" => "The password must contain at least 1 capital letter, 1 small letter and 1 number. The password must be 8 to 16 characters long.",
            "password.regex" => "The password must contain at least 1 capital letter, 1 small letter and 1 number. The password must be 8 to 16 characters long.",
            'password.required' => 'The password must contain at least 1 capital letter, 1 small letter and 1 number. The password must be 8 to 16 characters long.',
            ];

            $validator = Validator::make($request->all(), $rules, $customMessages);
            $validator->setAttributeNames($niceNames);

            if ($validator->fails()) {
                return back()->withErrors($validator)->withInput();
            } else {
                $password_resets = PasswordResets::whereToken($request->token)->first();

                $user_password=$this->common->crypto_encrypt($request->password, 'key_user');

                DB::table('admins')->where('email', $password_resets->email)->update(['password' => $user_password]);

                $user=Admin::whereEmail($password_resets->email)->first();

                DB::table('bin')->insert(['user_id' => $user->admin_id, 'field' => 'password', 'value' => $user_password, 'user_type' => 'admin', 'date' => date('Y-m-d')]);

                /*$user = Admin::where('email', $password_resets->email)->first();
                $user->password = $this->common->crypto_encrypt($request->password, 'key_user');
                $user->save();*/

                PasswordResets::whereToken($request->token)->delete();
                $this->common->flash_message('success', 'Password successfully set. You can now login');
                return redirect('admin/login');
            }
        }
    }
    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('admin');
    }
}
