<?php

namespace App\Http\Controllers\Account\Modules\Cards;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

use DB;
use Validator;
use Auth;
use Carbon\Carbon;
use App\Activity;
use App\Account;
use App\Linked_card;
use App\Http\Controllers\Controller;
use App\Libraries\EncryptDecrypt;

class CardsController extends Controller
{
    public function __construct()
    {
        $this->middleware('account');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $account_id = Auth::guard('account')->user()->account_id;
        $account = Account::where('account_id', '=', $account_id)->first();
        $countries = DB::table('countries')->where('active', '=', 1)->orderBy('country_name', 'asc')->pluck('country_name', 'country_code');

        $start_exp_year=Carbon::now()->year;
        $exp_year=[];
        for ($i=0;$i<5;$i++) {
            $exp_year[$start_exp_year]=$start_exp_year;
            $start_exp_year=$start_exp_year+1;
        }

        $exp_month=[];
        for ($i=1;$i<=12;$i++) {
            if ($i<10) {
                $expm="0$i";
            } else {
                $expm=$i;
            }
            $exp_month[$expm]=$expm;
        }

        $linked_cards = DB::table('linked_cards')
            ->leftjoin('card_types', 'linked_cards.card_type', '=', 'card_types.short_code')
            ->where('account_id', '=', $account_id)
            ->select('linked_cards.*', 'card_types.file_name')
            ->get();



        $pagetitle = 'Cards';
        $breadcrumb_level1 = '<a href="/account/cards">Cards</a>';

        return view('account/modules/cards/cards', compact('account', 'linked_cards', 'countries', 'exp_year', 'exp_month', 'pagetitle', 'breadcrumb_level1'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'card_holder' => 'required|min:3|max:30',
            'card_number' => 'required|min:16|max:16',
            'exp_month' => 'required|min:2|max:2',
            'exp_year' => 'required|min:4|max:4',
            'street' => 'required',
            'city' => 'required|min:2|max:30',
            'country' => 'required|min:2|max:2',
            'post_code' => 'required|min:2|max:15',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#addcard")->withErrors($validator)->withInput();
        }

        $account_id = Auth::guard('account')->user()->account_id;
        $linked_card = new Linked_card($request->all());

        $linked_card_id=(int)$linked_card_id=DB::table('linked_cards')->orderBy('linked_card_id', 'desc')->value('linked_card_id');
        if ($linked_card_id < 1) {
            $linked_card_id=1011;
        } else {
            $linked_card_id = $linked_card_id + 1;
        }

        $card_digit=substr($request->card_number, -4);

        $card_type=substr($request->card_number, 0, 1);
        if ($card_type=='5') {
            $card_type='MC';
        } elseif ($card_type=='4') {
            $card_type='VI';
        } else {
            $card_type='XX';
        }


        $main_key = DB::table('main_key')->where('status', '=', 'Active')->orderBy('id', 'DESC')->first();
        $crypto_key = DB::table('crypto_key')->where('status', '=', 'Active')->orderBy('id', 'DESC')->first();


        $crypto = new EncryptDecrypt($main_key->main_key);
        $card_encrypt=$crypto->encrypt($request->card_number, $crypto_key->key_card);
        //$card_decrypt=$crypto->decrypt($request->card_number,$crypto_key->key_card);

        $linked_card['linked_card_id'] = $linked_card_id;
        $linked_card['account_id'] = $account_id;
        $linked_card['card_type'] = $card_type;
        $linked_card['card_digit'] = $card_digit;
        $linked_card['card_number'] = $card_encrypt;

        $linked_card['primary'] = '0';
        $linked_card['added_by'] = Auth::guard('account')->user()->name;
        $linked_card['status'] = 'Active';
        $linked_card->save();


        $activity = new Activity();
        $activity['account_id'] = Auth::guard('account')->user()->account_id;
        $activity['added_by'] = Auth::guard('account')->user()->name;
        $activity['interface'] = 'Account';
        $activity['module'] = 'Wallet';
        $activity['activity'] = 'Linked card addedd by account '.Auth::guard('account')->user()->name.'.';
        $activity->save();

        session()->flash('toastr_alert', '<b>Great!</b> Linked card addedd by account '.Auth::guard('account')->user()->name.'.');
        session()->flash('success', true);

        return Redirect::to(URL::previous());
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($linked_card_id)
    {
        $linked_card = DB::table('linked_cards')->where('linked_card_id', '=', $linked_card_id)->first();
        $currencies = DB::table('currencies')->where('active', '=', 1)->orderBy('cy_code', 'asc')->pluck('cy_code', 'cy_code');
        $countries = DB::table('countries')->where('active', '=', 1)->orderBy('country_name', 'asc')->pluck('country_name', 'country_code');

        $main_key = DB::table('main_key')->where('status', '=', 'Active')->orderBy('id', 'DESC')->first();
        $crypto_key = DB::table('crypto_key')->where('status', '=', 'Active')->orderBy('id', 'DESC')->first();


        $crypto = new EncryptDecrypt($main_key->main_key);
        $card_decrypt=$crypto->decrypt($linked_card->card_number, $crypto_key->key_card);
        $linked_card->card_number=$card_decrypt;


        $start_exp_year=Carbon::now()->year;
        $exp_year=[];
        for ($i=0;$i<5;$i++) {
            $exp_year[$start_exp_year]=$start_exp_year;
            $start_exp_year=$start_exp_year+1;
        }

        $exp_month=[];
        for ($i=1;$i<=12;$i++) {
            if ($i<10) {
                $expm="0$i";
            } else {
                $expm=$i;
            }
            $exp_month[$expm]=$expm;
        }

        $pagetitle = 'Edit Linked Card';
        $breadcrumb_level1 = '<a href="/account/cards">Cards</a>';
        $breadcrumb_level2 = '<a href="/account/cards/'.$linked_card_id.'">Card</a>';

        return view('account/modules/cards/card', compact('linked_card', 'currencies', 'countries', 'exp_year', 'exp_month', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'linked_card_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $linked_card_id)
    {
        $validator = Validator::make($request->all(), [
                'card_holder' => 'required|min:3|max:30',
                'card_number' => 'required|min:16|max:16',
                'exp_month' => 'required|min:2|max:2',
                'exp_year' => 'required|min:4|max:4',
                'street' => 'required',
                'city' => 'required|min:2|max:30',
                'country' => 'required|min:2|max:2',
                'post_code' => 'required|min:2|max:15',
            ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#editcard")
                ->withErrors($validator)
                ->withInput();
        }

        $linked_card = Linked_card::where('linked_card_id', '=', $linked_card_id)->firstOrFail();
        $linked_card->update($request->all());

        $activity = new Activity();
        $activity['account_id'] = Auth::guard('account')->user()->account_id;
        $activity['added_by'] = Auth::guard('account')->user()->name;
        $activity['interface'] = 'Account';
        $activity['module'] = 'Wallet';
        $activity['activity'] = 'Linked card has been updated.';
        $activity->save();

        session()->flash('toastr_alert', '<b>Great!</b> Linked card has been updated.');
        session()->flash('success', true);

        return Redirect::to('account/cards');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
