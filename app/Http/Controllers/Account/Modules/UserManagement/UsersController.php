<?php

namespace App\Http\Controllers\Account\Modules\Usermanagement;

use Auth;
use Mail;
use App\Role;
use Password;
use Validator;
use App\Activity;
use App\Account;
use App\Role_user;
use App\PasswordResets;
use App\Http\Helper\Common;
use Illuminate\Http\Request;
use App\Entities\AccountUser;
use App\Libraries\EncryptDecrypt;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Facades\Redirect;

class UsersController extends Controller
{
    //use SendsPasswordResetEmails;
    protected $common;
    protected $encdec;
    public function __construct()
    {
        $this->middleware('account');
        $this->common = new Common;
        $this->encdec = new EncryptDecrypt('paypies');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $account_id = Auth::guard('account')->user()->account_id;

        $accounts = AccountUser::where('account_id', '=', $account_id)
            ->where(function ($query) use ($request) {
                //filter by keyword
                if (($search = $request->get('search'))) {
                    $query->orWhere('first_name', 'like', '%' . $search . '%');
                    $query->orWhere('last_name', 'like', '%' . $search . '%');
                    $query->orWhere('name', 'like', '%' . $search . '%');
                    $query->orWhere('email', 'like', '%' . $search . '%');
                    $query->orWhere('status', 'like', $search . '%');
                }
            })
        ->orderBy('created_at', 'desc')
        ->paginate(10);

        $pagetitle = 'Users';
        $breadcrumb_level1 = 'User Management';
        $breadcrumb_level2 = '<a href="/account/usermanagement/users">Users</a>';
        $uid = '0';
        $rid = '0';
        $roles = Role::pluck('display_name', 'id')->toArray();

        return view('account/modules/usermanagement/users', compact('accounts', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'uid', 'rid', 'roles'));
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:account_users',
            //'password' => 'required|confirmed|min:6',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#add")
                ->withErrors($validator)
                ->withInput();
        }

        $mUser = new AccountUser($request->all());

        $account_id = Auth::guard('account')->user()->account_id;

        $code = $this->encdec->makeRandomPassword2(50);
        $mUser['account_id'] = $account_id;
        $mUser['verify_token'] = $code;
        $mUser['token_time'] = time();
        $mUser['name'] = $request->last_name;
        $mUser['is_primary'] = 0;
        $mUser['password'] = $this->common->crypto_encrypt($code, 'key_account');
        $mUser['status'] = 'Inactive';
        $mUser['added_by'] = Auth::guard('account')->user()->name;
        $mUser->save();

        $role_user = new Role_user;
        $role_user->user_id = $account_id;
        $role_user->role_id = $request->role??'admin';
        $role_user->added_by = Auth::guard('account')->user()->name;
        $role_user->provider = 'account';
        $role_user->save();



        $data['subject'] = config('global.company').' - Account Activation';
        $data['preview_text'] = 'Just one more step ...';
        $data['main_message'] = 'Just one more step ...';
        $data['p1'] = 'Click the button below to activate your account.';
        $data['button_link'] = url('account/verify-email/'.$code);
        $data['button'] = 'Activate Account';
        $data['email'] = $request->email;
        $data['first_name'] = $request->name;
        @Mail::send('emails.app_email', $data, function ($message) use ($data) {
            $message->to($data['email'], $data['first_name'])->subject(config('global.company') .' - Account Activation');
        });

        $activity = new Activity();
        $activity['account_id'] = Auth::guard('account')->user()->account_id;
        $activity['added_by'] = Auth::guard('account')->user()->name;
        $activity['interface'] = 'Account';
        $activity['module'] = 'Usermanagement';
        $activity['activity'] = 'New Account '.$request->first_name.' '.$request->last_name.' has been added by '. Auth::guard('account')->user()->name .'.';
        $activity->save();

        session()->flash('toastr_alert', '<b>Added!</b> New Account '.$request->first_name.' '.$request->last_name.' has been added by '. Auth::guard('account')->user()->name .'.');
        session()->flash('success', true);

        return Redirect::to(URL::previous());
    }

    public function password_reset_link_send($user)
    {
        $data['first_name'] = $user->first_name;

        $token = str_random(100); // Generate random string values - limit 100

        $password_resets = new PasswordResets;

        $password_resets->email      = $user->email;
        $password_resets->token      = $data['token'];
        $password_resets->created_at = date('Y-m-d H:i:s');

        $password_resets->save(); // Insert a generated token and email in password_resets table

        $data['subject'] = config('global.company'). ' - Set Password';
        $data['preview_text'] = 'Set your account password';
        $data['main_message'] = 'Set your account password';
        $data['p1'] = 'Click the button below to set your account password.';
        $data['button_link'] = url('account/set-password/'.$code);
        $data['button'] = 'Set Password';
        $data['email'] = $user->email;
        $data['first_name'] = $user->first_name;
        @Mail::send('emails.app_email', $data, function ($message) use ($data) {
            $message->to($data['email'], $data['first_name'])->subject(config('global.company'). ' - Set Password');
        });

        return true;
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
        $account = AccountUser::where('id', '=', $id)->first();
        $roles = Role::pluck('display_name', 'id')->toArray();
        $pagetitle = $account->name??'';
        $breadcrumb_level1 = 'User Management';
        $breadcrumb_level2 = '<a href="/account/usermanagement/users">Users</a>';
        $breadcrumb_level3 = '<a href="/account/usermanagement/users/'.$id.'">User</a>';

        return view('account/modules/usermanagement/user', compact('account', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'breadcrumb_level3', 'account_id', 'roles'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'is_primary' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#edit")
                ->withErrors($validator)
                ->withInput();
        }

        $account = AccountUser::where('id', '=', $id)->firstOrFail();

        if ($request->is_primary == '0') {
            $primaryCheck = AccountUser::where(['account_id'=> $account->account_id, 'is_primary' => '1'])->where('id', '<>', $id)->first();
            if (count($primaryCheck) == 0) {
                $validator->errors()->add('is_primary', 'One active User should be primary!');
                return Redirect::to(URL::previous() . "#edit")
                ->withErrors($validator)
                ->withInput();
            }
        }


        if ($request->is_primary == '1') {
            if ($account->status == 'Inactive') {
                $validator->errors()->add('is_primary', 'Only Active user are allow to set primary!');
                return Redirect::to(URL::previous() . "#edit")
                ->withErrors($validator)
                ->withInput();
            }
            $update =  AccountUser::where('account_id', '=', $account->account_id)->update(['is_primary' => '0']);
        }

        $account = AccountUser::where('id', '=', $id)->firstOrFail();
        $account->name = $request->last_name;
        $account->is_primary = $request->is_primary;
        $account->update($request->all());

        session()->flash('toastr_alert', '<b>Updated!</b> Account User '.$request->first_name.' '.$request->last_name.' has been Updated by '. Auth::guard('account')->user()->name .'.');
        session()->flash('info', true);

        return Redirect::to(URL::previous());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function lockunlock(Request $request, $id)
    {
        $account = AccountUser::where('id', '=', $id)->firstOrFail();
        $account->status = $request->status;
        $account->bad_login = 0;
        $account->updated_by = Auth::guard('account')->user()->name;
        $account->update($request->all());


        $activity = new Activity();
        $activity['account_id'] = Auth::guard('account')->user()->account_id;
        $activity['added_by'] = Auth::guard('account')->user()->name;
        $activity['interface'] = 'Account';
        $activity['module'] = 'Usermanagement';
        if ($request->status === 'Active') {
            $activity['activity'] = 'Account '. $account->first_name .' '. $account->last_name .' has been unlocked by '. Auth::guard('account')->user()->name .'.';
        } elseif ($request->status === 'Locked') {
            $activity['activity'] = 'Account '. $account->first_name .' '. $account->last_name .' has been locked by '. Auth::guard('account')->user()->name .'.';
        }
        $activity->save();

        if ($request->status === 'Active') {
            session()->flash('toastr_alert', '<b>Unlocked!</b> Account '. $account->first_name .' '. $account->last_name .' has been unlocked by '. Auth::guard('account')->user()->name .'.');
            session()->flash('info', true);
        } elseif ($request->status === 'Locked') {
            session()->flash('toastr_alert', '<b>Locked!</b> Account '. $account->first_name .' '. $account->last_name .' has been locked by '. Auth::guard('account')->user()->name .'.');
            session()->flash('info', true);
        }

        return Redirect::to(URL::previous());
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $account = AccountUser::where('id', '=', $id)->firstOrFail();
        $account->deleted_by = Auth::guard('account')->user()->name;
        $account->forceDelete();


        $activity = new Activity();
        $activity['account_id'] = Auth::guard('account')->user()->account_id;
        $activity['added_by'] = Auth::guard('account')->user()->name;
        $activity['interface'] = 'Account';
        $activity['module'] = 'Usermanagement';
        $activity['activity'] = 'Account '. $account->first_name .' '. $account->last_name .' has been deleted by '. Auth::guard('account')->user()->name .'.';

        $activity->save();


        session()->flash('toastr_alert', '<b>Deleted!</b> Account '. $account->first_name .' '. $account->last_name .' has been deleted by '. Auth::guard('account')->user()->name .'.');
        session()->flash('info', true);


        return Redirect::to(URL::previous());
    }
}
