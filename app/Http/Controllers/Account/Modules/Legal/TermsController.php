<?php

namespace App\Http\Controllers\Account\Modules\Legal;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

use DB;
use Validator;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class TermsController extends Controller
{   
    public function __construct()
    {
        $this->middleware('account');
    }
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $account_id = Auth::guard('account')->user()->account_id;
			
        $pagetitle = 'Terms & Conditions';
        $breadcrumb_level1 = '<a href="/account/terms">Terms & Conditions</a>';
        
        return view('account/modules/legal/terms', compact('pagetitle', 'breadcrumb_level1'));
    }
}
