<?php

namespace App\Http\Controllers\Account\Modules\Dashboard;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

use DB;
use Validator;
use Auth;
use Mail;
use Carbon\Carbon;
use App\Activity;
use App\Notification;
use App\Account;
use App\Wallet;
use App\Wallet_switch;
use App\Payment;
use App\Currency;
use App\Crossrate;
use App\Linked_card;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Libraries\EncryptDecrypt;

class DashboardControllerX extends Controller
{     
    public function __construct()
    {
        $this->middleware('account');
    }
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		
		
		$account_id = Auth::guard('account')->user()->account_id;
		$account_email = Auth::guard('account')->user()->email;
		$account = Account::where('account_id', '=', $account_id)->first();
		$wallettype = DB::table('wallettypes')->where('wallet_type_id', '=', $account->wallet_type_id)->first();
		
		$currencies = DB::table('currencies')->where('active', '=', 1)->orderBy('cy_code','asc')->pluck('cy_code','cy_code');
		
		$countries = DB::table('countries')->where('active', '=', 1)->orderBy('country_name','asc')->pluck('country_name','country_name');

		$wallets = DB::table('wallets')
            ->join('wallet_switches', 'wallets.wallet_id', '=', 'wallet_switches.wallet_id')
            ->where('wallet_switches.account_id', '=', $account_id)
            ->select('wallets.*')
            ->get();
			
		$wallets_currency = DB::table('wallets')
            ->join('wallet_switches', 'wallets.wallet_id', '=', 'wallet_switches.wallet_id')
            ->where('wallet_switches.account_id', '=', $account_id)        
            ->pluck('currency','currency');
			
		$primary_currency = DB::table('wallets')
            ->join('wallet_switches', 'wallets.wallet_id', '=', 'wallet_switches.wallet_id')
            ->where('wallet_switches.account_id', '=', $account_id) 
			->where('wallets.primary', '=', '1') 			
            ->select('wallets.currency')->first();	
		/*	
		if(count($primary_currency)< 1)
		{
			$primary_currency= new Currency();
			$primary_currency->currency="EUR";
		}	*/
		
			
		$start_exp_year=Carbon::now()->year;
		$exp_year=array();
		for($i=0;$i<5;$i++)
		{
			$exp_year[$start_exp_year]=$start_exp_year;
			$start_exp_year=$start_exp_year+1;			
		}		
		
		$exp_month=array();
		for($i=1;$i<=12;$i++)
		{
			if($i<10) $expm="0$i";else $expm=$i;
			$exp_month[$expm]=$expm;		
		}
		
		$account_currency = DB::select('select cy_code from currencies where cy_code not in(
		select wallets.currency from accounts INNER JOIN wallet_switches on wallet_switches.account_id=accounts.account_id 
		INNER JOIN wallets on wallets.wallet_id=wallet_switches.wallet_id where accounts.account_id='.$account_id.') and active=1');
		
		$currency_arr=array();
		foreach($account_currency as $currency)
		{
			$currency_arr[$currency->cy_code]=$currency->cy_code;
		}		
	
		$payments = DB::table('payments')
					->where('account_id', '=', $account_id)
					->orWhere('from', '=', $account_email)
					->orWhere('to', '=', $account_email)
					->orderBy('created_at','desc')
					->take(5)
					->get();
		
		$wallet_statements = DB::table('wallet_statements')
			->join('wallet_switches', 'wallet_statements.wallet_id', '=', 'wallet_switches.wallet_id')
			->where('wallet_switches.account_id', '=', $account_id)
			->select('wallet_statements.*')
			->orderBy('created_at','desc')
			->take(5)
			->get();
					
		$request_payments = DB::table('payments')->where([
				['from', '=', $account_email],
				['status', '=', 'Pending'],
				['payment_method', '=', '+Wallet'],
				['payment_type', '=', 'Request'],
			])
			->get();
		
        $pagetitle = 'Summary';
        $breadcrumb_level1 = '<a href="/account">Summary</a>';
		
		$today = Carbon::now();
        Carbon::setToStringFormat('jS \o\f F, Y');
        
        return view('account/modules/dashboard/dashboard', compact('account', 'wallettype', 'currency_arr', 'primary_currency','activities', 'request_payments', 'currencies', 'countries','exp_year', 'exp_month', 'wallets', 'wallet_statements', 'wallets_currency', 'payments', 'pagetitle', 'today'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }
	
	 /**
     * Store a newly created resource in crossrate.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function crossrate(Request $request)
    {
		
		$crossrates = DB::table('crossrates')->where('from_currency', '=', "$request->fromcurrency")->where('to_currency', '=', "$request->tocurrency")->select('crossrate')->first();
	
		//print_r($request);
		//$crossrates = array('USDGBP'=>0.75,'USDEUR'=>'0.95','GBPUSD'=>1.5,'GBPEUR'=>1.25,'EURUSD'=>1.10,'EURGBP'=>0.85);

		//$rate = $crossrates[$request->fromcurrency.$request->tocurrency];
		
		$toamount=number_format(floatval($request->fromamount * $crossrates->crossrate),2,'.','');
		echo $toamount;
    }
	
	/**
     * Pay or Decline Request Payment.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function pay(Request $request, $payment_id)
    {
		$current_date = Carbon::now();	
		if ($request->has('pay')) {
			
			$payment = Payment::where('payment_id', '=', $payment_id)->firstOrFail();
			
			$account_id = Auth::guard('account')->user()->account_id;
			$account = Account::where('account_id', '=', $account_id)->first();
			
			$wallets = DB::table('wallets')
				->join('wallet_switches', 'wallets.wallet_id', '=', 'wallet_switches.wallet_id')
				->where('wallet_switches.account_id', '=', $account_id)
				->where('wallets.currency', '=',$payment['currency'])
				->select('wallets.wallet_id')
				->get();
				
			$wallet_id_from = $wallets[0]->wallet_id;
			$wallet_from = Wallet::where('wallet_id', '=', $wallet_id_from)->firstOrFail();
			
			$validator = Validator::make($request->all(), [
				'amount' => 'required|numeric|between:0.00,'. $wallet_from->balance .'',
			]);

			if ($validator->fails()) {
				return Redirect::to(URL::previous() . "#pay")
					->withErrors($validator)
					->withInput();
			}

			$payment['status'] = 'Approved';
			$payment['settlement_date'] = $current_date;
			
					
			$wallet_id_to = $payment->wallet_id;
			$wallet_to = Wallet::where('wallet_id', '=', $wallet_id_to)->firstOrFail();
			//$wallet_from = DB::table('wallets')->where('wallet_id', '=', $wallet_id_to)->first();	
			$wallet_to['balance'] = $wallet_to->balance + $payment->total_amount;
			$wallet_to->save();

			$wallet_from['balance'] = $wallet_from->balance - $payment->amount;

			$payment->save();
			$wallet_to->save();
			$wallet_from->save();	
			
			$activity = new Activity ();
			$activity['account_id'] = Auth::guard('account')->user()->account_id;
			$activity['added_by'] = Auth::guard('account')->user()->name;
			$activity['interface'] = 'Account';
			$activity['module'] = 'Payment';
			$activity['activity'] = 'Payment request of '. $payment->amount .' '. $payment->currency .' for '. $payment->description .' from '. $payment->added_by .' ('. $payment->to .') has been <span class="label label-sm label-success">Approved</span>.';
			$activity->save();
			
			session()->flash('toastr_alert','<b>Approved!</b> Payment request of '. $payment->amount .' '. $payment->currency .' for '. $payment->description .' from '. $payment->added_by .' ('. $payment->to .') has been <span class="label label-sm label-success">Approved</span>.');
			session()->flash('success', true);
			
			//email notification
			$notification = DB::table('notifications')->where('email', '=', $payment->from)->first();
			
			if ($notification->approve_decline == 1) {
				$greeting = 'Hi '.$account->first_name.',';
				$p1 = 'you have approved payment request of <b>'.$payment->amount.' '.$payment->currency.'</b> to <b>'.$payment->to.'</b> on '.$payment->created_at.'.';
				$p2 = 'Reference number for this payment is <b>'.$payment->payment_id.'</b>.';
				
				Mail::send('emails.notification', [
					'payment' => $payment,
					'greeting' => $greeting, 
					'p1' => $p1,
					'p2' => $p2
					], function ($message) use ($payment)
				{
					$message->to(''.$payment->from.'');
					$message->subject('+Wallet Request');
				});
			}
			
			//email notification receive
			$notification_to = DB::table('notifications')->where('email', '=', $payment->to)->first();
			
			if ($notification_to->approve_decline == 1) {
				$greeting = 'Hi '.$account->first_name.',';
				$p1 = 'your payment request of <b>'.$payment->amount.' '.$payment->currency.'</b> from <b>'.$payment->from.'</b> on '.$payment->created_at.' has been <b>approved</b>.';
				$p2 = 'Reference number for this payment is <b>'.$payment->payment_id.'</b>.';
				
				Mail::send('emails.notification', [
					'payment' => $payment,
					'greeting' => $greeting, 
					'p1' => $p1,
					'p2' => $p2
					], function ($message) use ($payment)
				{
					$message->to(''.$payment->to.'');
					$message->subject('+Wallet Request');
				});
			}
			
			return Redirect::to(URL::previous());	
		}
		
		if ($request->has('decline')) {
			
			$payment = Payment::where('payment_id', '=', $payment_id)->firstOrFail();
			
			$account_id = Auth::guard('account')->user()->account_id;
			$account = Account::where('account_id', '=', $account_id)->first();
			
			$payment['status'] = 'Declined';
			$payment['settlement_date'] = $current_date;
			$payment->save();
			
			$activity = new Activity ();
			$activity['account_id'] = Auth::guard('account')->user()->account_id;
			$activity['added_by'] = Auth::guard('account')->user()->name;
			$activity['interface'] = 'Account';
			$activity['module'] = 'Payment';
			$activity['activity'] = 'Payment request of '. $payment->amount .' '. $payment->currency .' for '. $payment->description .' from '. $payment->added_by .' ('. $payment->to .') has been <span class="label label-sm label-danger">Declined</span>.';
			$activity->save();
			
			session()->flash('toastr_alert','<b>Declined!</b> Payment request of '. $payment->amount .' '. $payment->currency .' for '. $payment->description .' from '. $payment->added_by .' ('. $payment->to .') has been <span class="label label-sm label-danger">Declined</span>.');
			session()->flash('error', true);
			
			//email notification decline
			$notification = DB::table('notifications')->where('email', '=', $payment->from)->first();
			
			if ($notification->approve_decline == 1) {
				$greeting = 'Hi '.$account->first_name.',';
				$p1 = 'you have declined payment request of <b>'.$payment->amount.' '.$payment->currency.'</b> to <b>'.$payment->to.'</b> on '.$payment->created_at.'.';
				$p2 = 'Reference number for this payment is <b>'.$payment->payment_id.'</b>.';
				
				Mail::send('emails.notification', [
					'account' => $account,
					'greeting' => $greeting, 
					'p1' => $p1,
					'p2' => $p2
					], function ($message) use ($account)
				{
					$message->to(''.$account->email.'');
					$message->subject('+Wallet Request');
				});
			}
			
			//email notification receive
			$notification_to = DB::table('notifications')->where('email', '=', $payment->to)->first();
			
			if ($notification_to->approve_decline == 1) {
				$greeting = 'Hi '.$account->first_name.',';
				$p1 = 'your payment request of <b>'.$payment->amount.' '.$payment->currency.'</b> from <b>'.$payment->from.'</b> on '.$payment->created_at.' has been <b>declined</b>.';
				$p2 = 'Reference number for this payment is <b>'.$payment->payment_id.'</b>.';
				
				Mail::send('emails.notification', [
					'payment' => $payment,
					'greeting' => $greeting, 
					'p1' => $p1,
					'p2' => $p2
					], function ($message) use ($payment)
				{
					$message->to(''.$payment->to.'');
					$message->subject('+Wallet Request');
				});
			}
		
			return Redirect::to(URL::previous());
		}
    }
	
	/**
     * Dashboard Forms.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function forms(Request $request)
    {
		$current_date = Carbon::now();	
		
		if ($request->has('addwallet')) {
			
			$validator = Validator::make($request->all(), [
			'currency' => 'required',
			]);

			if ($validator->fails()) {
				return Redirect::to(URL::previous() . "#addwallet")
					->withErrors($validator)
					->withInput();
			}
			
			$account_id = Auth::guard('account')->user()->account_id;
			$account = Account::where('account_id', '=', $account_id)->first();
			$account_email = Auth::guard('account')->user()->email;
			
			$wallet_id=(int)$payment_id=DB::table('wallets')->orderBy('wallet_id','desc')->value('wallet_id');
			if($wallet_id < 1) $wallet_id=101011;
			else $wallet_id = $wallet_id + 1;
			
			$wallets_rec = DB::table('wallets')->where('email', $account_email)->select('wallet_id')->first();				
			if(count($wallets_rec) > 0)
			{
				$primary=0;
			}	
			else $primary=1;			
			
			$wallet = new Wallet($request->all());
			$wallet['wallet_id'] = $wallet_id;
			$wallet['wallet_type_id'] = 1; //need to get wallet_type_id of account primary wallet
			$wallet['wallet_fee_id'] = 2;
			$wallet['wallet_limit_id'] = 2;
			$wallet['primary'] = $primary;
			$wallet['email'] = $account_email;
			$wallet['status'] = 'Pending';
			$wallet->save();	
			
			$wallet_switch = new Wallet_switch();
			$wallet_switch ['wallet_id'] = $wallet_id;
			$wallet_switch ['account_id'] = $account_id;
			$wallet_switch->save();
			
			$activity = new Activity ();
			$activity['account_id'] = Auth::guard('account')->user()->account_id;
			$activity['added_by'] = Auth::guard('account')->user()->name;
			$activity['interface'] = 'Account';
			$activity['module'] = 'Wallet';
			$activity['activity'] = 'New Wallet for '. Auth::guard('account')->user()->first_name .' '. Auth::guard('account')->user()->last_name .' has been created.';
			$activity->save();
			
			session()->flash('toastr_alert','<b>Great!</b> New Wallet for '. Auth::guard('account')->user()->first_name .' '. Auth::guard('account')->user()->last_name .' has been created.');
			session()->flash('success', true);
			
			//email notification
			$notification = DB::table('notifications')->where('email', '=', $account->email)->first();
			
			if ($notification->send_money == 1) {
				$greeting = 'Hi '.$account->first_name.',';
				$p1 = 'you have created new +Wallet in <b>'.$request->currency.'</b> currency on '.$wallet->created_at.'.';
				
				Mail::send('emails.notification', [
					'account' => $account,
					'greeting' => $greeting, 
					'p1' => $p1
					], function ($message) use ($account)
				{
					$message->to(''.$account->email.'');
					$message->subject('+Wallet Created');
				});
			}
			
			return Redirect::to(URL::previous());
		}
		if ($request->has('send')) {
			
			$account_id = Auth::guard('account')->user()->account_id;
			$account = Account::where('account_id', '=', $account_id)->first();
			
			$payment = new Payment($request->all());
			
			$wallets_from = DB::table('wallets')
				->join('wallet_switches', 'wallets.wallet_id', '=', 'wallet_switches.wallet_id')
				->where('wallet_switches.account_id', '=', $account_id)
				->where('wallets.currency', '=', $payment['currency'])
				->select('wallets.wallet_id')
				->get();
				
			$wallet_id_from = $wallets_from[0]->wallet_id;		
			$wallet_from = Wallet::where('wallet_id', '=', $wallet_id_from)->firstOrFail();
			
			$validator = Validator::make($request->all(), [
				'to_email' => 'required|email',
				'amount' => 'required|numeric|between:0.00,'. $wallet_from->balance .'',
				'currency' => 'required',
				'description' => 'required',
			]);

			if ($validator->fails()) {
				return Redirect::to(URL::previous() . "#send")
					->withErrors($validator)
					->withInput();
			}
			
			$payment_id=(int)$payment_id=DB::table('payments')->orderBy('payment_id','desc')->value('payment_id');
			if($payment_id < 1) $payment_id=101011;
			else $payment_id = $payment_id + 1;
			
			$payment['payment_id'] = $payment_id;
			$payment['account_id'] = $account_id;
			$payment['from'] = Auth::guard('account')->user()->email;
			$payment['to'] = $request->to_email;
			$payment['payment_method'] = '+Wallet';
			$payment['payment_type'] = 'Send';
			$payment['fee'] = 0;
			$payment['total_amount'] = $request->amount;
			$payment['added_by'] = Auth::guard('account')->user()->name;
			$payment['status'] = 'Approved';
			$payment['settlement_date'] = $current_date;
			
			$wallet_from['balance'] = $wallet_from->balance - $payment->amount;
			
			$payment['wallet_id'] = $wallet_id_from;
				
			$wallet_to = Wallet::where('email', '=', $request->to_email)->firstOrFail();
			//print_r($wallet_to);exit;
			$wallet_id_to = $wallet_to->wallet_id;		
			$wallet_to['balance'] = $wallet_to->balance + $payment->amount;
			
			$payment->save();
			$wallet_from->save();
			$wallet_to->save();
			
			$activity = new Activity ();
			$activity['account_id'] = Auth::guard('account')->user()->account_id;
			$activity['added_by'] = Auth::guard('account')->user()->name;
			$activity['interface'] = 'Account';
			$activity['module'] = 'Payment';
			$activity['activity'] = 'Payment of '.$request->amount.' '.$request->currency.' has been sent to '.$request->to_email.'.';
			$activity->save();
			
			session()->flash('toastr_alert','<b>Great!</b> Payment of '.$request->amount.' '.$request->currency.' has been sent to '.$request->to_email.'.');
			session()->flash('success', true);
			
			//email notification send
			$notification = DB::table('notifications')->where('email', '=', $account->email)->first();
			
			if ($notification->send_money == 1) {
				$greeting = 'Hi '.$account->first_name.',';
				$p1 = 'you have sent payment of  <b>'.$payment->amount.' '.$payment->currency.'</b> to <b>'.$payment->to.'</b> on '.$payment->created_at.'.';
				$p2 = 'Reference number for this payment is <b>'.$payment->payment_id.'</b>.';
				
				Mail::send('emails.notification', [
					'account' => $account,
					'greeting' => $greeting, 
					'p1' => $p1,
					'p2' => $p2
					], function ($message) use ($account)
				{
					$message->to(''.$account->email.'');
					$message->subject('+Wallet Send');
				});
			}
			
			//email notification receive			
			$notification_to = DB::table('notifications')->where('email', '=', $request->to_email)->first();
			$email_to = $request->to_email;
			
			if ($notification_to->send_money == 1) {
				$greeting = 'Hi there,';
				$p1 = 'you have received payment of  <b>'.$payment->amount.' '.$payment->currency.'</b> from <b>'.$payment->from.'</b> on '.$payment->created_at.'.';
				$p2 = 'Reference number for this payment is <b>'.$payment->payment_id.'</b>.';
				
				Mail::send('emails.notification', [
					'email_to' => $email_to,
					'greeting' => $greeting, 
					'p1' => $p1,
					'p2' => $p2
					], function ($message) use ($email_to)
				{
					$message->to(''.$email_to.'');
					$message->subject('+Wallet Send');
				});
			}
			
			return Redirect::to(URL::previous());
		}
		if ($request->has('pay')) {
			
		
			$validator = Validator::make($request->all(), [
				'to_email' => 'required|email',
				'amount' => 'required',
				'currency' => 'required',
				'description' => 'required',
				'payment_method' => 'required',				
			]);

			if ($validator->fails()) {
				return Redirect::to(URL::previous() . "#pay")
					->withErrors($validator)
					->withInput($request->all());
			}
			
			$account_id = Auth::guard('account')->user()->account_id;
			$account = Account::where('account_id', '=', $account_id)->first();
			
			$payment = new Payment($request->all());
			
			$wallets_terminal = DB::table('wallets')
				->join('wallet_switches', 'wallets.wallet_id', '=', 'wallet_switches.wallet_id')
				->where('wallet_switches.terminal_id', '<>', '')
				->where('wallets.email', '=',$request->to_email)
				->select('wallets.email')
				->first();
				
			if(count($wallets_terminal) < 1)
			{
				$validator->errors()->add('to_email', 'There is no Merchant with this email!');
				return Redirect::to(URL::previous() . "#pay")->withErrors($validator)->withInput($request->all());				
			}				
			
			
			$payment_id=(int)$payment_id=DB::table('payments')->orderBy('payment_id','desc')->value('payment_id');
			if($payment_id < 1) $payment_id=101011;
			else $payment_id = $payment_id + 1;
			
			
			
			
			
			if($request->payment_method=='WalletBalance')
			{				
				$payment_method='+Wallet';
				$payment_type='Pay';
				
				$wallets = DB::table('wallets')
				->join('wallet_switches', 'wallets.wallet_id', '=', 'wallet_switches.wallet_id')
				->where('wallet_switches.account_id', '=', $account_id)
				->where('wallets.currency', '=',$request->currency)
				->select('wallets.*')
				->first();
				
				if(count($wallets) == 0  || $request->amount > $wallets->balance)
				{
					$validator->errors()->add('amount', 'Invalid Amount!');
					return Redirect::to(URL::previous() . "#pay")->withErrors($validator)->withInput($request->all());
				}
					
				
				
				
				$wallet_id = $wallets->wallet_id;			
				$wallet_fee_id = $wallets->wallet_fee_id;
				
				$walletfees = DB::table('walletfees')->where('wallet_fee_id', [$wallet_fee_id])->select('receive_fee','receive_flat_fee')->first();
				$receive_fee=$walletfees->receive_fee;			 
				$receive_flat_fee = $walletfees->receive_flat_fee;
				
				$fee = ($request->amount * $receive_fee) / 100 +  $receive_flat_fee;
				$total_amount = $request->amount - $fee;	
			
			}
			else if($request->payment_method=='NewCard')
			{
				$validator = Validator::make($request->all(), [
					'card_holder' => 'required|min:3|max:30',
					'card_number' => 'required|min:16|max:16',
					'exp_month' => 'required|min:2|max:2',
					'exp_year' => 'required|min:4|max:4',
					'bill_street' => 'required',
					'bill_city' => 'required|min:2|max:30',
					'bill_country' => 'required|min:2|max:2',
					'bill_post_code' => 'required|min:2|max:15',
				]);

				if ($validator->fails()) {
					return Redirect::to(URL::previous() . "#pay")
						->withErrors($validator)
						->withInput($request->all());
				}
				
				
				

				
				
				
				
				$card_digit=substr($request->card_number,-4);
				
				$card_type=substr($request->card_number,0,1);
				if($card_type=='5')$card_type='MC';
				else if($card_type=='4')$card_type='VI';
				else $card_type='XX';
				
				
				$main_key = DB::table('main_key')->where('status', '=', 'Active')->orderBy('id','DESC')->first();
				$crypto_key = DB::table('crypto_key')->where('status', '=', 'Active')->orderBy('id','DESC')->first();
				
				
				$crypto = new EncryptDecrypt($main_key->main_key);  
				$card_encrypt=$crypto->encrypt($request->card_number,$crypto_key->key_card);
				//$card_decrypt=$crypto->decrypt($request->card_number,$crypto_key->key_card);
				
				
				$linked_card_rec = DB::table('linked_cards')->where('account_id', $account_id)->where('card_number', $card_encrypt)->select('linked_card_id','id')->first();
				
				if(count($linked_card_rec) > 0)
				{
					$linked_card = Linked_card::where('linked_card_id', '=', $linked_card_rec->linked_card_id)->firstOrFail();					
					$linked_card['updated_by'] = Auth::guard('account')->user()->name;				
				}
				else
				{
					$linked_card_id=(int)$linked_card_id=DB::table('linked_cards')->orderBy('linked_card_id','desc')->value('linked_card_id');
					if($linked_card_id < 1) $linked_card_id=1011;
					else $linked_card_id = $linked_card_id + 1;
					
					$linked_card = new Linked_card();
					$linked_card['primary'] = '0';
					$linked_card['added_by'] = Auth::guard('account')->user()->name;
					$linked_card['linked_card_id'] = $linked_card_id;
					$linked_card['account_id'] = $account_id;					
				}
				
				
				$linked_card['card_type'] = $card_type;
				$linked_card['card_digit'] = $card_digit;
				$linked_card['card_number'] = $card_encrypt;
				$linked_card['card_holder'] = $request->card_holder;
				$linked_card['exp_month'] = $request->exp_month;
				$linked_card['exp_year'] = $request->exp_year;
				$linked_card['street'] = $request->bill_street;
				$linked_card['city'] = $request->bill_city;
				$linked_card['country'] = $request->bill_country;
				$linked_card['post_code'] = $request->bill_post_code;	
				$linked_card['status'] = 'Active';	
				if(count($linked_card_rec) > 0)
				{				
					$linked_card->update();
				}
				else{
					$linked_card->save();
				}
				
				$payment_method='Card';
				$payment_type='Pay';
				$wallet_id=null;
				
				$discount_fee=2.5;			 
				$flat_fee = 0.50;
				
				$fee = ($request->amount * $discount_fee) / 100 +  $flat_fee;
				$total_amount = $request->amount - $fee;
				
			}
						
			$payment['payment_id'] = $payment_id;
			$payment['account_id'] = $account_id;
			$payment['wallet_id'] = $wallet_id;
			$payment['from'] = Auth::guard('account')->user()->email;
			$payment['to'] = $request->to_email;
			$payment['payment_method'] = $payment_method;
			$payment['payment_type'] = $payment_type;
			$payment['fee'] = $fee;
			$payment['total_amount'] = $total_amount;
			$payment['added_by'] = Auth::guard('account')->user()->name;
			$payment['status'] = 'Approved';
			$payment['settlement_date'] = $current_date;
			$payment->save();
			
			if($request->payment_method=='WalletBalance')
			{
				$wallet_from = Wallet::where('wallet_id', '=', $wallet_id)->firstOrFail();
				$wallet_from['balance'] = $wallet_from->balance - $request->amount;
				$wallet_from->save();
			}
					
			$activity = new Activity ();
			$activity['account_id'] = Auth::guard('account')->user()->account_id;
			$activity['added_by'] = Auth::guard('account')->user()->name;
			$activity['interface'] = 'Account';
			$activity['module'] = 'Payment';
			$activity['activity'] = 'Payment request of '.$request->amount.' '.$request->currency.' has been sent to '.$request->from_email.'.';
			$activity->save();
			
			session()->flash('toastr_alert','<b>Great!</b> Payment request of '.$request->amount.' '.$request->currency.' has been sent to '.$request->from_email.'.');
			session()->flash('success', true);
			
			//email notification pay
			$notification = DB::table('notifications')->where('email', '=', $account->email)->first();
			
			if ($notification->pay_money == 1) {
				$greeting = 'Hi '.$account->first_name.',';
				$p1 = 'you have sent payment of <b>'.$payment->amount.' '.$payment->currency.'</b> to <b>'.$payment->to.'</b> on '.$payment->created_at.'.';
				$p2 = 'Reference number for this payment is <b>'.$payment->payment_id.'</b>.';
				
				Mail::send('emails.notification', [
					'account' => $account,
					'greeting' => $greeting, 
					'p1' => $p1,
					'p2' => $p2
					], function ($message) use ($account)
				{
					$message->to(''.$account->email.'');
					$message->subject('+Wallet Pay');
				});
			}
			
			//email notification receive
			$notification_to = DB::table('notifications')->where('email', '=', $request->to_email)->first();
			$email_to = $request->to_email;
			
			if ($notification_to->pay_money == 1) {
				$greeting = 'Hi '.$account->first_name.',';
				$p1 = 'you have received payment of <b>'.$payment->amount.' '.$payment->currency.'</b> from <b>'.$payment->from.'</b> on '.$payment->created_at.'.';
				$p2 = 'Reference number for this payment is <b>'.$payment->payment_id.'</b>.';
				
				Mail::send('emails.notification', [
					'email_to' => $email_to,
					'greeting' => $greeting, 
					'p1' => $p1,
					'p2' => $p2
					], function ($message) use ($email_to)
				{
					$message->to(''.$email_to.'');
					$message->subject('+Wallet Pay');
				});
			}
			
			return Redirect::to(URL::previous());		
		}
		if ($request->has('request')) {
			
			$validator = Validator::make($request->all(), [
				'from_email' => 'required|email',
				'amount' => 'required',
				'currency' => 'required',
				'description' => 'required',
			]);

			if ($validator->fails()) {
				return Redirect::to(URL::previous() . "#request")
					->withErrors($validator)
					->withInput();
			}
			
			$account_id = Auth::guard('account')->user()->account_id;
			$account = Account::where('account_id', '=', $account_id)->first();
			
			$payment = new Payment($request->all());
			
			$wallets = DB::table('wallets')
				->join('wallet_switches', 'wallets.wallet_id', '=', 'wallet_switches.wallet_id')
				->where('wallet_switches.account_id', '=', $account_id)
				->where('wallets.currency', '=',$payment['currency'])
				->select('wallets.wallet_id')
				->get();
				
			$wallet_id = $wallets[0]->wallet_id;

			$wallet_fee_id = DB::table('wallets') ->where('wallet_id', '=', $wallet_id) ->value('wallet_fee_id');
			$payment_id=(int)$payment_id=DB::table('payments')->orderBy('payment_id','desc')->value('payment_id');
			if($payment_id < 1) $payment_id=101011;
			else $payment_id = $payment_id + 1;
			
			$receive_fee = DB::table('walletfees')->where('wallet_fee_id', [$wallet_fee_id])->value('receive_fee');
			$receive_flat_fee = DB::table('walletfees')->where('wallet_fee_id', [$wallet_fee_id])->value('receive_flat_fee');
			$fee = ($request->amount * $receive_fee) / 100 +  $receive_flat_fee;
			$total_amount = $request->amount - $fee;
			
			$payment['payment_id'] = $payment_id;
			$payment['account_id'] = $account_id;
			$payment['wallet_id'] = $wallet_id;
			$payment['from'] = $request->from_email;
			$payment['to'] = Auth::guard('account')->user()->email;
			$payment['payment_method'] = '+Wallet';
			$payment['payment_type'] = 'Request';
			$payment['fee'] = $fee;
			$payment['total_amount'] = $total_amount;
			$payment['added_by'] = Auth::guard('account')->user()->name;
			$payment['status'] = 'Pending';
			$payment->save();
			
			$activity = new Activity ();
			$activity['account_id'] = Auth::guard('account')->user()->account_id;
			$activity['added_by'] = Auth::guard('account')->user()->name;
			$activity['interface'] = 'Account';
			$activity['module'] = 'Payment';
			$activity['activity'] = 'Payment request of '.$request->amount.' '.$request->currency.' has been sent to '.$request->from_email.'.';
			$activity->save();
			
			session()->flash('toastr_alert','<b>Great!</b> Payment request of '.$request->amount.' '.$request->currency.' has been sent to '.$request->from_email.'.');
			session()->flash('success', true);
			
			//email notification request
			$notification = DB::table('notifications')->where('email', '=', $account->email)->first();
			
			if ($notification->request_money == 1) {
				$greeting = 'Hi '.$account->first_name.',';
				$p1 = 'you have sent payment request of  <b>'.$payment->amount.' '.$payment->currency.'</b> to <b>'.$payment->from.'</b> on '.$payment->created_at.'.';
				$p2 = 'Reference number for this payment is <b>'.$payment->payment_id.'</b>.';
				
				Mail::send('emails.notification', [
					'account' => $account,
					'greeting' => $greeting, 
					'p1' => $p1,
					'p2' => $p2
					], function ($message) use ($account)
				{
					$message->to(''.$account->email.'');
					$message->subject('+Wallet Request');
				});
			}
			
			//email notification received
			$notification_to = DB::table('notifications')->where('email', '=', $request->from_email)->first();
			$email_from = $request->from_email;
			
			if ($notification_to->request_money == 1) {
				$greeting = 'Hi there,';
				$p1 = 'you have received payment request of  <b>'.$payment->amount.' '.$payment->currency.'</b> from <b>'.$payment->to.'</b> on '.$payment->created_at.'.';
				$p2 = 'Reference number for this payment is <b>'.$payment->payment_id.'</b>.';
				$p3 = 'Please login to your <a href="'.url('/account/login').'">+Wallet</a> and approve or decline the payment.';
				
				Mail::send('emails.notification', [
					'email_from' => $email_from,
					'greeting' => $greeting, 
					'p1' => $p1,
					'p2' => $p2,
					'p3' => $p3
					], function ($message) use ($email_from)
				{
					$message->to(''.$email_from.'');
					$message->subject('+Wallet Request');
				});
			}
			
			return Redirect::to(URL::previous());
		}		
		if ($request->has('primary')) {
			
			
			$validator = Validator::make($request->all(), [
			'currency' => 'required',
			]);
			
			
			if ($validator->fails()) {
				return Redirect::to(URL::previous() . "#primary")
					->withErrors($validator)
					->withInput();
			}
			
			
			
			$account_id = Auth::guard('account')->user()->account_id;
			$account = Account::where('account_id', '=', $account_id)->first();
			
			$wallets_primary = DB::table('wallets')
				->join('wallet_switches', 'wallets.wallet_id', '=', 'wallet_switches.wallet_id')
				->where('wallet_switches.account_id', '=', $account_id)
				->update(['wallets.primary' => '0']);
			

			$wallets = DB::table('wallets')
				->join('wallet_switches', 'wallets.wallet_id', '=', 'wallet_switches.wallet_id')
				->where('wallet_switches.account_id', '=', $account_id)
				->where('wallets.currency', '=', $request->currency)
				->update(['wallets.primary' => '1']);
				
			
			
			$activity = new Activity ();
			$activity['account_id'] = Auth::guard('account')->user()->account_id;
			$activity['added_by'] = Auth::guard('account')->user()->name;
			$activity['interface'] = 'Account';
			$activity['module'] = 'Wallet';
			$activity['activity'] = 'Primary currency has been updated.';
			$activity->save();
			
			session()->flash('toastr_alert','<b>Great!</b> Primary currency has been updated.');
			session()->flash('success', true);
			
			//email notification
			$notification = DB::table('notifications')->where('email', '=', $account->email)->first();
			
			if ($notification->add_edit_settings == 1) {
				$greeting = 'Hi '.$account->first_name.',';
				$p1 = 'you have changed primary currency of your +Wallet to <b>'.$request->currency.'</b> currency on '.$activity->created_at.'.';
				
				Mail::send('emails.notification', [
					'account' => $account,
					'greeting' => $greeting, 
					'p1' => $p1
					], function ($message) use ($account)
				{
					$message->to(''.$account->email.'');
					$message->subject('+Wallet Primary Currency');
				});
			}
			
			return Redirect::to(URL::previous());
			
		}
		if ($request->has('addcurrency')) {
			
				$validator = Validator::make($request->all(), [
				'currency' => 'required',
			]);

			if ($validator->fails()) {
				return Redirect::to(URL::previous() . "#addcurrency")
					->withErrors($validator)
					->withInput();
			}
			
			$account_id = Auth::guard('account')->user()->account_id;
			$account = Account::where('account_id', '=', $account_id)->first();
			$account_email = Auth::guard('account')->user()->email;
			
			$wallet_id=(int)$payment_id=DB::table('wallets')->orderBy('wallet_id','desc')->value('wallet_id');
			if($wallet_id < 1) $wallet_id=101011;
			else $wallet_id = $wallet_id + 1;
			
			
			$wallet = new Wallet($request->all());
			$wallet['wallet_id'] = $wallet_id;
			$wallet['wallet_type_id'] = 1; //need to get wallet_type_id of account primary wallet
			$wallet['wallet_fee_id'] = 2;
			$wallet['wallet_limit_id'] = 2;
			$wallet['email'] = $account_email;
			$wallet['status'] = 'Pending';
			$wallet->save();	
			
			$wallet_switch = new Wallet_switch();
			$wallet_switch ['wallet_id'] = $wallet_id;
			$wallet_switch ['account_id'] = $account_id;
			$wallet_switch->save();
			
			$activity = new Activity ();
			$activity['account_id'] = Auth::guard('account')->user()->account_id;
			$activity['added_by'] = Auth::guard('account')->user()->name;
			$activity['interface'] = 'Account';
			$activity['module'] = 'Wallet';
			$activity['activity'] = ''.$request->currency.' currency has been added.';
			$activity->save();
			
			session()->flash('toastr_alert','<b>Great!</b> '.$request->currency.' currency has been added.');
			session()->flash('success', true);
			
			//email notification
			$notification = DB::table('notifications')->where('email', '=', $account->email)->first();
			
			if ($notification->add_edit_settings == 1) {
				$greeting = 'Hi '.$account->first_name.',';
				$p1 = 'you have added <b>'.$request->currency.'</b> currency to your +Wallet on '.$activity->created_at.'.';
				
				Mail::send('emails.notification', [
					'account' => $account,
					'greeting' => $greeting, 
					'p1' => $p1
					], function ($message) use ($account)
				{
					$message->to(''.$account->email.'');
					$message->subject('+Wallet Currency');
				});
			}
			
			return Redirect::to(URL::previous());
		}	
		if ($request->has('topup')) {
			$validator = Validator::make($request->all(), [
            'amount' => 'required',
			'currency' => 'required',
			]);

			if ($validator->fails()) {
				return Redirect::to(URL::previous() . "#topup")
					->withErrors($validator)
					->withInput();
			}
			
			$payment = new Payment($request->all());
				
			$account_id = Auth::guard('account')->user()->account_id;
			$account = Account::where('account_id', '=', $account_id)->first(); 
			
			$wallets = DB::table('wallets')
				->join('wallet_switches', 'wallets.wallet_id', '=', 'wallet_switches.wallet_id')
				->where('wallet_switches.account_id', '=', $account_id)
				->where('wallets.currency', '=',$payment['currency'])
				->select('wallets.wallet_id')
				->get();
				
			$wallet_id = $wallets[0]->wallet_id;	
			
			//$wallet_id = 101012;// DB::table('wallet_switches') ->where('account_id', '=', $account_id) ->value('wallet_id');
			$wallet_fee_id = DB::table('wallets') ->where('wallet_id', '=', $wallet_id) ->value('wallet_fee_id');
			$payment_id=(int)$payment_id=DB::table('payments')->orderBy('payment_id','desc')->value('payment_id');
			if($payment_id < 1) $payment_id=101011;
			else $payment_id = $payment_id + 1;
			
			$fee = 0;

			$payment['payment_id'] = $payment_id;
			$payment['account_id'] = $account_id;
			$payment['wallet_id'] = $wallet_id;
			$payment['from'] = Auth::guard('account')->user()->name;
			$payment['to'] = Auth::guard('account')->user()->email;
			$payment['description'] = '+Wallet TopUp Balance';
			$payment['payment_method'] = '+Wallet';
			$payment['payment_type'] = 'TopUp';
			$payment['fee'] = $fee;
			$payment['total_amount'] = $request->amount - $fee;
			$payment['added_by'] = Auth::guard('account')->user()->name;
			$payment['status'] = 'Pending';		
			$payment->save();
			
			$activity = new Activity ();
			$activity['account_id'] = Auth::guard('account')->user()->account_id;
			$activity['added_by'] = Auth::guard('account')->user()->name;
			$activity['interface'] = 'Account';
			$activity['module'] = 'Payment';
			$activity['activity'] = 'TopUp has been recorded. Please transfer the amount of '. $payment->total_amount .' '. $payment->currency .' to your +Wallet account. Instructions has been sent by email.';
			$activity->save();
			
			session()->flash('toastr_alert','<b>Great!</b> TopUp has been recorded. Please transfer the amount of '. $payment->total_amount .' '. $payment->currency .' to your +Wallet account. Instructions has been sent by email.');
			session()->flash('success', true);
			
			//email notification
			$notification = DB::table('notifications')->where('email', '=', $account->email)->first();
			
			if ($notification->add_money == 1) {
				$greeting = 'Hi '.$account->first_name.',';
				$p1 = 'you have requested a TopUp of <b>'.$payment->amount.' '.$payment->currency.'</b> on '.$payment->created_at.'.';
				$p2 = 'Reference number for this payment is <b>'.$payment->payment_id.'</b>.';
				$p3 = 'Please make a payment of <b>'.$payment->amount.' '.$payment->currency.'</b> with reference number <b>'.$payment->payment_id.'</b> to following bank account.';
				$t1r1c1 = 'Account holder:'; 		$t1r1c2 = '<b>PAYPIPES S.R.O.</b>';
				$t1r2c1 = 'Bank account number:'; 	$t1r2c2 = '1932735263/0800';
				$t1r3c1 = 'IBAN:'; 					$t1r3c2 = '<b>CZ8908000000001932735263</b>';
				$t1r4c1 = 'Swift Code / BIC:'; 		$t1r4c2 = '<b>GIBACZPX</b>';
				$t1r5c1 = 'Bank Name:'; 			$t1r5c2 = 'CESKA SPORITELNA A.S.';
				$t1r6c1 = 'Bank Address:'; 			$t1r6c2 = 'OLBRACHTOVA 62, 14000 PRAGUE, CZECH REPUBLIC';
				$f1 = 'It is important to use reference number '.$payment->payment_id.' in your payment for allocation of your funds to your +Wallet. ';
				
				Mail::send('emails.notification', [
					'account' => $account,
					'greeting' => $greeting, 
					'p1' => $p1,
					'p2' => $p2,
					'p3' =>	$p3,
					't1r1c1' =>	$t1r1c1,
					't1r1c2' =>	$t1r1c2,
					't1r2c1' =>	$t1r2c1,
					't1r2c2' =>	$t1r2c2,
					't1r3c1' =>	$t1r3c1,
					't1r3c2' =>	$t1r3c2,
					't1r4c1' =>	$t1r4c1,
					't1r4c2' =>	$t1r4c2,
					't1r5c1' =>	$t1r5c1,
					't1r5c2' =>	$t1r5c2,
					't1r6c1' =>	$t1r6c1,
					't1r6c2' =>	$t1r6c2,
					'f1' =>	$f1
					], function ($message) use ($account)
				{
					$message->to(''.$account->email.'');
					$message->subject('+Wallet TopUp');
				});
			}
			
			return Redirect::to(URL::previous());
		}
		if ($request->has('withdraw')) {
			$validator = Validator::make($request->all(), [
            'amount' => 'required',
			'currency' => 'required',
			'account_holder_name' => 'required',
			'account_number' => 'required',
			'iban' => 'required',
			'swift' => 'required',
			'bank_name' => 'required',
			'bank_address' => 'required',
			]);

			if ($validator->fails()) {
				return Redirect::to(URL::previous() . "#withdraw")
					->withErrors($validator)
					->withInput();
			}
			
			$payment = new Payment($request->all());
				
			$account_id = Auth::guard('account')->user()->account_id;
			$account = Account::where('account_id', '=', $account_id)->first();
			
			$wallets = DB::table('wallets')
				->join('wallet_switches', 'wallets.wallet_id', '=', 'wallet_switches.wallet_id')
				->where('wallet_switches.account_id', '=', $account_id)
				->where('wallets.currency', '=',$payment['currency'])
				->select('wallets.wallet_id')
				->get();
				
			$wallet_id = $wallets[0]->wallet_id;	
			
			//$wallet_id = 101012;// DB::table('wallet_switches') ->where('account_id', '=', $account_id) ->value('wallet_id');
			$wallet_fee_id = DB::table('wallets') ->where('wallet_id', '=', $wallet_id) ->value('wallet_fee_id');
			$payment_id=(int)$payment_id=DB::table('payments')->orderBy('payment_id','desc')->value('payment_id');
			if($payment_id < 1) $payment_id=101011;
			else $payment_id = $payment_id + 1;
			
			$fee = 0;

			$payment['payment_id'] = $payment_id;
			$payment['account_id'] = $account_id;
			$payment['wallet_id'] = $wallet_id;
			$payment['from'] = Auth::guard('account')->user()->email;
			$payment['to'] = Auth::guard('account')->user()->name;
			$payment['description'] = '+Wallet Withdraw Balance';
			$payment['payment_method'] = '+Wallet';
			$payment['payment_type'] = 'Withdraw';
			$payment['fee'] = $fee;
			$payment['total_amount'] = $request->amount - $fee;
			$payment['added_by'] = Auth::guard('account')->user()->name;
			$payment['status'] = 'Pending';		
			$payment->save();
			
			$activity = new Activity ();
			$activity['account_id'] = Auth::guard('account')->user()->account_id;
			$activity['added_by'] = Auth::guard('account')->user()->name;
			$activity['interface'] = 'Account';
			$activity['module'] = 'Payment';
			$activity['activity'] = 'Withdraw has been recorded. The amount of '. $payment->total_amount .' '. $payment->currency .' will be transferd to your bank account.';
			$activity->save();
			
			session()->flash('toastr_alert','<b>Great!</b> Withdraw has been recorded. The amount of '. $payment->total_amount .' '. $payment->currency .' will be transferd to your bank account.');
			session()->flash('success', true);
			
			//email notification
			$notification = DB::table('notifications')->where('email', '=', $account->email)->first();
			
			if ($notification->withdraw_money == 1) {
				$greeting = 'Hi '.$account->first_name.',';
				$p1 = 'you have requested a Withdraw of <b>'.$payment->amount.' '.$payment->currency.'</b> on '.$payment->created_at.'.';
				$p2 = 'Reference number for this payment is <b>'.$payment->payment_id.'</b>.';
				$p3 = 'Payment of <b>'.$payment->amount.' '.$payment->currency.'</b> with reference number <b>'.$payment->payment_id.'</b> will be send to following bank account.';
				$t1r1c1 = 'Account holder:'; 		$t1r1c2 = $request->account_holder_name;
				$t1r2c1 = 'Bank account number:'; 	$t1r2c2 = $request->account_number;
				$t1r3c1 = 'IBAN:'; 					$t1r3c2 = $request->iban;
				$t1r4c1 = 'Swift Code / BIC:'; 		$t1r4c2 = $request->swift;
				$t1r5c1 = 'Bank Name:'; 			$t1r5c2 = $request->bank_name;
				$t1r6c1 = 'Bank Address:'; 			$t1r6c2 = $request->bank_address;
				
				Mail::send('emails.notification', [
					'account' => $account,
					'greeting' => $greeting, 
					'p1' => $p1,
					'p2' => $p2,
					'p3' =>	$p3,
					't1r1c1' =>	$t1r1c1,
					't1r1c2' =>	$t1r1c2,
					't1r2c1' =>	$t1r2c1,
					't1r2c2' =>	$t1r2c2,
					't1r3c1' =>	$t1r3c1,
					't1r3c2' =>	$t1r3c2,
					't1r4c1' =>	$t1r4c1,
					't1r4c2' =>	$t1r4c2,
					't1r5c1' =>	$t1r5c1,
					't1r5c2' =>	$t1r5c2,
					't1r6c1' =>	$t1r6c1,
					't1r6c2' =>	$t1r6c2
					], function ($message) use ($account)
				{
					$message->to(''.$account->email.'');
					$message->subject('+Wallet Withdraw');
				});
			}
			
			return Redirect::to(URL::previous());
		}
		if ($request->has('upgrade')) {
			
			$account_id = Auth::guard('account')->user()->account_id;
			$account = Account::where('account_id', '=', $account_id)->first();
			
			$account = Account::where('account_id', '=', $account_id)->firstOrFail();
			$account['compliance'] = 'Pending';
			$account->save();
			
			$activity = new Activity ();
			$activity['account_id'] = Auth::guard('account')->user()->account_id;
			$activity['added_by'] = Auth::guard('account')->user()->name;
			$activity['interface'] = 'Account';
			$activity['module'] = 'Wallet';
			$activity['activity'] = 'Your request for upgrade has been submited. Please upload personal documents in settings under compliance tab.';
			$activity->save();
			
			session()->flash('toastr_alert','<b>Great!</b> Your request for upgrade has been submited. Please upload personal documents in settings under compliance tab.');
			session()->flash('success', true);
			
			//email notification
			$notification = DB::table('notifications')->where('email', '=', $account->email)->first();
			
			if ($notification->add_edit_settings == 1) {
				$greeting = 'Hi '.$account->first_name.',';
				$p1 = 'you have requested upgrade of your +Wallet on '.$activity->created_at.'.';
				$p2 = 'Please login to your <a href="'.url('/account/login').'">+Wallet</a> and upload personal documents in settings under compliance tab.';
				
				Mail::send('emails.notification', [
					'account' => $account,
					'greeting' => $greeting, 
					'p1' => $p1,
					'p2' => $p2
					], function ($message) use ($account)
				{
					$message->to(''.$account->email.'');
					$message->subject('+Wallet Upgrade');
				});
			}
			
			return Redirect::to(URL::previous());
		}
		if ($request->has('transfer')) {
		
			$validator = Validator::make($request->all(), [
            'from_amount' => 'required',
			'from_currency' => 'required',
			'to_amount' => 'required',
			'to_currency' => 'required',
			]);

			if ($validator->fails()) {
				return Redirect::to(URL::previous() . "#transfer")
					->withErrors($validator)
					->withInput();
			}
						
			$account_id = Auth::guard('account')->user()->account_id;
			$account = Account::where('account_id', '=', $account_id)->first();
			
			$payment = new Payment($request->all());
			
			$wallets_from = DB::table('wallets')
				->join('wallet_switches', 'wallets.wallet_id', '=', 'wallet_switches.wallet_id')
				->where('wallet_switches.account_id', '=', $account_id)
				->where('wallets.currency', '=', $request->from_currency)
				->select('wallets.wallet_id','wallets.balance')
				->first();
			
				if(count($wallets_from) == 0  || $request->from_amount > $wallets_from->balance)
				{
					$validator->errors()->add('from_amount', 'Invalid Amount!');
					return Redirect::to(URL::previous() . "#transfer")->withErrors($validator)->withInput($request->all());
				}
				
				if($request->from_currency == $request->to_currency)
				{
					$validator->errors()->add('to_currency', 'Invalid Currency!');
					return Redirect::to(URL::previous() . "#transfer")->withErrors($validator)->withInput($request->all());
				}
				
			$wallet_id_from = $wallets_from->wallet_id;		
			
			$payment_id=(int)$payment_id=DB::table('payments')->orderBy('payment_id','desc')->value('payment_id');
			if($payment_id < 1) $payment_id=101011;
			else $payment_id = $payment_id + 1;
			
			$payment['payment_id'] = $payment_id;
			$payment['account_id'] = $account_id;
			$payment['wallet_id'] = $wallet_id_from;
			$payment['from'] = Auth::guard('account')->user()->email;
			$payment['to'] =  Auth::guard('account')->user()->email;
			$payment['payment_method'] = '+Wallet';
			$payment['payment_type'] = 'TransferOut';
			$payment['currency'] = $request->from_currency;
			$payment['fee'] = 0;
			$payment['amount'] = $request->from_amount;
			$payment['total_amount'] = $request->from_amount;
			$payment['added_by'] = Auth::guard('account')->user()->name;
			$payment['status'] = 'Approved';
			$payment['settlement_date'] = $current_date;
			$payment->save();			
			
			$wallet_from = Wallet::where('wallet_id', '=', $wallet_id_from)->firstOrFail();
			$wallet_from['balance'] = $wallets_from->balance - $request->from_amount;
			$wallet_from['updated_by'] = Auth::guard('account')->user()->name;
			$wallet_from->save();
			
			$wallets_to = DB::table('wallets')
				->join('wallet_switches', 'wallets.wallet_id', '=', 'wallet_switches.wallet_id')
				->where('wallet_switches.account_id', '=', $account_id)
				->where('wallets.currency', '=', $request->to_currency)
				->select('wallets.wallet_id','wallets.balance')
				->first();
				
			$wallet_id_to = $wallets_to->wallet_id;		
			
			$payment_id=(int)$payment_id=DB::table('payments')->orderBy('payment_id','desc')->value('payment_id');
			if($payment_id < 1) $payment_id=101011;
			else $payment_id = $payment_id + 1;
			
			$payment_to = new Payment($request->all());
			$payment_to['payment_id'] = $payment_id;
			$payment_to['account_id'] = $account_id;
			$payment_to['wallet_id'] = $wallet_id_to;
			$payment_to['from'] = Auth::guard('account')->user()->email;
			$payment_to['to'] =  Auth::guard('account')->user()->email;
			$payment_to['payment_method'] = '+Wallet';
			$payment_to['payment_type'] = 'TransferIn';
			$payment_to['currency'] = $request->to_currency;
			$payment_to['fee'] = 0;
			$payment_to['amount'] = $request->to_amount;
			$payment_to['total_amount'] = $request->to_amount;
			$payment_to['added_by'] = Auth::guard('account')->user()->name;
			$payment_to['status'] = 'Approved';
			$payment_to['settlement_date'] = $current_date;			
			$payment_to->save();
						
			$wallet_to = Wallet::where('wallet_id', '=', $wallet_id_to)->firstOrFail();
			$wallet_to['balance'] = $wallet_to->balance + $request->to_amount;
			$wallet_to['updated_by'] = Auth::guard('account')->user()->name;
			$wallet_to->save();
			
			$activity = new Activity ();
			$activity['account_id'] = Auth::guard('account')->user()->account_id;
			$activity['added_by'] = Auth::guard('account')->user()->name;
			$activity['interface'] = 'Account';
			$activity['module'] = 'Payment';
			$activity['activity'] = 'Payment of '.$request->amount.' '.$request->currency.' has been sent to '.$request->to_email.'.';
			$activity->save();
			
			session()->flash('toastr_alert','<b>Great!</b> Transfer Payment of '.$request->from_amount.' '.$request->from_currency.' has been topup to '.$request->to_amount.' '.$request->to_currency.'.');
			session()->flash('success', true);
			
			//email notification
			$notification = DB::table('notifications')->where('email', '=', $account->email)->first();
			
			if ($notification->transfer_money == 1) {
				$greeting = 'Hi '.$account->first_name.',';
				$p1 = 'you have make a transfer of <b>'.$request->from_amount.' '.$request->from_currency.'</b> to <b>'.$request->to_amount.' '.$request->to_currency.'</b> on '.$payment->created_at.'.';
				$p2 = 'Reference number for this payment is <b>'.$payment->payment_id.'</b>.';
				
				Mail::send('emails.notification', [
					'account' => $account,
					'greeting' => $greeting, 
					'p1' => $p1,
					'p2' => $p2
					], function ($message) use ($account)
				{
					$message->to(''.$account->email.'');
					$message->subject('+Wallet Transfer');
				});
			}
			
			return Redirect::to(URL::previous());
		}
	}
}
