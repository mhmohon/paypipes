<?php

namespace App\Http\Controllers\Account\Modules\Payments;

use DB;
use Auth;
use Mail;
use Validator;
use App\Wallet;
use App\Payment;
use App\Activity;
use App\Currency;
use App\Account;
use Carbon\Carbon;
use App\Notification;
use App\Http\Helper\Common;
use Illuminate\Http\Request;
use App\Repositories\Payments;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class PaymentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('account');
        $this->common = new Common;
        $this->payments=new Payments();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $account_id = Auth::guard('account')->user()->account_id;
        $account = Account::where('account_id', '=', $account_id)->first();
        $account_email = $account->email;

        //$payments = Payment::where(function($query) use ($request) {
        $payments = DB::table('payments')->where(function ($query) use ($request) {
            //filter by keyword
            if (($search = $request->get('search'))) {
                $query->orWhere('payment_id', 'like', '%' . $search . '%');
                $query->orWhere('from', 'like', '%' . $search . '%');
                $query->orWhere('to', 'like', '%' . $search . '%');
                $query->orWhere('description', 'like', '%' . $search . '%');
                $query->orWhere('payment_method', 'like', '%' . $search . '%');
                $query->orWhere('payment_type', 'like', '%' . $search . '%');
                $query->orWhere('amount', 'like', '%' . $search . '%');
                $query->orWhere('total_amount', 'like', '%' . $search . '%');
                $query->orWhere('currency', 'like', '%' . $search . '%');
                $query->orWhere('status', 'like', $search . '%');
            }
        })
        /*->where(function($query) use ($request) {
            //filter by keyword
                //$query->orWhere('account_id', '=', $account_id);
                $query->orWhere('from', '=', $account_email);
                $query->orWhere('to', '=', $account_email);
        })*/
        ->where('account_id', '=', $account_id)
        ->orWhere('from', '=', $account_email)
        ->orWhere('to', '=', $account_email)
        ->orderBy('id', 'desc')
        //->take(5)
        ->paginate(10);

        $pagetitle = 'Payments';
        $breadcrumb_level1 = '<a href="/account/payments">Payments</a>';

        $pg='?'.http_build_query($request->all());
        $id = '0';

        return view('account/modules/payments/payments', compact('payments', 'pagetitle', 'breadcrumb_level1', 'id', 'pg', 'account'));
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($payment_id)
    {
        $payment = DB::table('payments')->where('payment_id', '=', $payment_id)->first();
        $account_id = Auth::guard('account')->user()->account_id;
        $account = Account::where('account_id', '=', $account_id)->first();


        switch ($payment->status) {
            case "Approved":
                $payment_status =  'label-success'; break;
            case "Pending":
                $payment_status =  'label-info';    break;
            case "Requested":
                $payment_status =  'label-info';    break;
            case "Refunded":
                $payment_status =  'label-purple';  break;
            case "Canceled":
                $payment_status =  'label-warning'; break;
            case "Chargeback":
                $payment_status =  'label-black';   break;
            case "Declined":
                $payment_status =  'label-danger';  break;
            default:
                $payment_status =  'label-default'; break;
        }

        if (Account::where('email', '=', $payment->from)->exists()) {
            $from = Account::where('email', '=', $payment->from)->first();
        } else {
            $from = $account;
        }

        if (Account::where('email', '=', $payment->to)->exists()) {
            $to = Account::where('email', '=', $payment->to)->first();
        } else {
            $to = $account;
        }
        //dd($payment, $from, $to);

        $pagetitle = ''.$payment->description.' ('.$payment->amount.' '.$payment->currency.') <span class="label label-sm '.$payment_status.'">'.$payment->status.'</span>';
        $breadcrumb_level1 = '<a href="/account/payments">Payments</a>';
        $breadcrumb_level2 = '<a href="/account/payments/'.$payment_id.'">Payment</a>';

        return view('account/modules/payments/payment', compact('payment', 'from', 'to', 'account', 'merchant', 'terminal', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'payment_id'));
    }



    /**
     * Pay or Decline Request Payment.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function pay(Request $request, $payment_id)
    {
        $current_date = Carbon::now();
        if ($request->has('pay')) {
            $payment = Payment::where('payment_id', '=', $payment_id)->firstOrFail();

            $account_id = Auth::guard('account')->user()->account_id;
            $account = Account::where('account_id', '=', $account_id)->first();

            $wallets = DB::table('wallets')
                ->join('wallet_switches', 'wallets.wallet_id', '=', 'wallet_switches.wallet_id')
                ->where('wallet_switches.account_id', '=', $account_id)
                ->where('wallets.currency', '=', $payment['currency'])
                ->select('wallets.wallet_id')
                ->get();

            $wallet_id_from = $wallets[0]->wallet_id;
            $wallet_from = Wallet::where('wallet_id', '=', $wallet_id_from)->firstOrFail();

            $validator = Validator::make($request->all(), [
                'amount' => 'required|numeric|between:0.00,'. $wallet_from->balance .'',
            ]);

            if ($validator->fails()) {
                return Redirect::to(URL::previous() . "#pay")
                    ->withErrors($validator)
                    ->withInput();
            }

            $payment['status'] = 'Approved';
            $payment['settlement_date'] = $current_date;


            $wallet_id_to = $payment->wallet_id;
            $wallet_to = Wallet::where('wallet_id', '=', $wallet_id_to)->firstOrFail();
            //$wallet_from = DB::table('wallets')->where('wallet_id', '=', $wallet_id_to)->first();
            $wallet_to['balance'] = $wallet_to->balance + $payment->total_amount;
            $wallet_to->save();

            $wallet_from['balance'] = $wallet_from->balance - $payment->amount;

            $payment->save();
            $wallet_to->save();
            $wallet_from->save();

            $activity = new Activity();
            $activity['account_id'] = Auth::guard('account')->user()->account_id;
            $activity['added_by'] = Auth::guard('account')->user()->name;
            $activity['interface'] = 'Account';
            $activity['module'] = 'Payment';
            $activity['activity'] = 'Payment request of '. $payment->amount .' '. $payment->currency .' for '. $payment->description .' from '. $payment->added_by .' ('. $payment->to .') has been <span class="label label-sm label-success">Approved</span>.';
            $activity->save();

            session()->flash('toastr_alert', '<b>Approved!</b> Payment request of '. $payment->amount .' '. $payment->currency .' for '. $payment->description .' from '. $payment->added_by .' ('. $payment->to .') has been <span class="label label-sm label-success">Approved</span>.');
            session()->flash('success', true);

            //email notification
            $notification = DB::table('notifications')->where('email', '=', $payment->from)->first();

            if ($notification->approve_decline == 1) {
                $greeting = 'Hi '.$account->first_name.',';
                $p1 = 'you have approved payment request of <b>'.$payment->amount.' '.$payment->currency.'</b> to <b>'.$payment->to.'</b> on '.$payment->created_at.'.';
                $p2 = 'Reference number for this payment is <b>'.$payment->payment_id.'</b>.';

                Mail::send('emails.notification', [
                    'payment' => $payment,
                    'greeting' => $greeting,
                    'p1' => $p1,
                    'p2' => $p2
                    ], function ($message) use ($payment) {
                        $message->to(''.$payment->from.'');
                        $message->subject('+Wallet Request');
                    });
            }

            //email notification receive
            $notification_to = DB::table('notifications')->where('email', '=', $payment->to)->first();

            if ($notification_to->approve_decline == 1) {
                $greeting = 'Hi '.$account->first_name.',';
                $p1 = 'your payment request of <b>'.$payment->amount.' '.$payment->currency.'</b> from <b>'.$payment->from.'</b> on '.$payment->created_at.' has been <b>approved</b>.';
                $p2 = 'Reference number for this payment is <b>'.$payment->payment_id.'</b>.';

                Mail::send('emails.notification', [
                    'payment' => $payment,
                    'greeting' => $greeting,
                    'p1' => $p1,
                    'p2' => $p2
                    ], function ($message) use ($payment) {
                        $message->to(''.$payment->to.'');
                        $message->subject('+Wallet Request');
                    });
            }

            return Redirect::to(URL::previous());
        }

        if ($request->has('decline')) {
            $payment = Payment::where('payment_id', '=', $payment_id)->firstOrFail();

            $account_id = Auth::guard('account')->user()->account_id;
            $account = Account::where('account_id', '=', $account_id)->first();

            $payment['status'] = 'Declined';
            $payment['settlement_date'] = $current_date;
            $payment->save();

            $activity = new Activity();
            $activity['account_id'] = Auth::guard('account')->user()->account_id;
            $activity['added_by'] = Auth::guard('account')->user()->name;
            $activity['interface'] = 'Account';
            $activity['module'] = 'Payment';
            $activity['activity'] = 'Payment request of '. $payment->amount .' '. $payment->currency .' for '. $payment->description .' from '. $payment->added_by .' ('. $payment->to .') has been <span class="label label-sm label-danger">Declined</span>.';
            $activity->save();

            session()->flash('toastr_alert', '<b>Declined!</b> Payment request of '. $payment->amount .' '. $payment->currency .' for '. $payment->description .' from '. $payment->added_by .' ('. $payment->to .') has been <span class="label label-sm label-danger">Declined</span>.');
            session()->flash('error', true);

            //email notification decline
            $notification = DB::table('notifications')->where('email', '=', $payment->from)->first();

            if ($notification->approve_decline == 1) {
                $greeting = 'Hi '.$account->first_name.',';
                $p1 = 'you have declined payment request of <b>'.$payment->amount.' '.$payment->currency.'</b> to <b>'.$payment->to.'</b> on '.$payment->created_at.'.';
                $p2 = 'Reference number for this payment is <b>'.$payment->payment_id.'</b>.';

                Mail::send('emails.notification', [
                    'account' => $account,
                    'greeting' => $greeting,
                    'p1' => $p1,
                    'p2' => $p2
                    ], function ($message) use ($account) {
                        $message->to(''.$account->email.'');
                        $message->subject('+Wallet Request');
                    });
            }

            //email notification receive
            $notification_to = DB::table('notifications')->where('email', '=', $payment->to)->first();

            if ($notification_to->approve_decline == 1) {
                $greeting = 'Hi '.$account->first_name.',';
                $p1 = 'your payment request of <b>'.$payment->amount.' '.$payment->currency.'</b> from <b>'.$payment->from.'</b> on '.$payment->created_at.' has been <b>declined</b>.';
                $p2 = 'Reference number for this payment is <b>'.$payment->payment_id.'</b>.';

                Mail::send('emails.notification', [
                    'payment' => $payment,
                    'greeting' => $greeting,
                    'p1' => $p1,
                    'p2' => $p2
                    ], function ($message) use ($payment) {
                        $message->to(''.$payment->to.'');
                        $message->subject('+Wallet Request');
                    });
            }

            return Redirect::to(URL::previous());
        }
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function dispute(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'reason' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#dispute")
                ->withErrors($validator)
                ->withInput();
        }

        $payment = Payment::findOrFail($id);

        $dispute = $request->all();
        $dispute['payment_id'] = $id;
        $dispute['mid'] = $payment->mid;
        $dispute['tid'] = $payment->tid;
        $dispute['status'] = 'Open';

        Dispute::create($dispute);

        return Redirect::to(URL::previous() . "#disputecomment");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function disputecomment(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'comment' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#disputecomment")
                ->withErrors($validator)
                ->withInput();
        }

        $dispute_id = DB::table('disputes')->where('payment_id', [$id])->value('id');

        $disputecomment = new Disputecomment($request->all());
        $disputecomment['dispute_id'] = $dispute_id;

        return Redirect::to('disputes');
    }

    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function refund()
    {
        //
    }



    public function data_return()
    {
        $data['account_id']= Auth::guard('account')->user()->account_id;
        return $data;
    }
    public function pdf_convert(Request $request)
    {
        $data=$this->data_return();
        return $this->payments->download($request, $this->common, 0, $data);
    }

    public function csv_convert(Request $request)
    {
        $data=$this->data_return();
        return $this->payments->download($request, $this->common, 1, $data);
    }
}
