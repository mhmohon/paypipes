<?php

namespace App\Http\Controllers\Account\Modules\Statements;

use DB;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Entities\AccountStatement;
use App\Http\Controllers\Controller;

class statementsController extends Controller
{
    public function __construct()
    {
        $this->middleware('account');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $account_id = Auth::guard('account')->user()->account_id;
        $account = DB::table('accounts')->where('account_id', '=', $account_id)->first();

        //$statements = DB::table('account_statements')->where('account_id', '=', $account_id)->orderBy('created_at','desc')->paginate(10);

        $statements = AccountStatement::where(function ($query) use ($request) {
            //filter by keyword
            if (($search = $request->get('search'))) {
                $query->orWhere('month', 'like', '%' . $search . '%');
                $query->orWhere('year', 'like', '%' . $search . '%');
                $query->orWhere('currency', 'like', '%' . $search . '%');
            }
        })
        ->where('account_id', '=', $account_id)
        ->orderBy('created_at', 'desc')
        ->paginate(10);

        $pagetitle = 'Statements';
        $breadcrumb_level1 = '<a href="/account/statements">Statements</a>';

        return view('account/modules/statements/statements', compact('account', 'statements', 'pagetitle', 'breadcrumb_level1'));
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($statement_id)
    {
        $account_id = Auth::guard('account')->user()->account_id;
        $account = DB::table('accounts')->where('account_id', '=', $account_id)->first();
        $statement = DB::table('account_statements')->where('account_statement_id', '=', $statement_id)->first();



        $firstDayofPreviousMonth = Carbon::createFromDate($statement->year, $statement->month)->startOfMonth()->toDateString().' 00:00:00';
        $lastDayofPreviousMonth = Carbon::createFromDate($statement->year, $statement->month)->endOfMonth()->toDateString().' 23:59:59';

        $payments = DB::select('select * from payments where  `settlement_date` >="'.$firstDayofPreviousMonth.'" and `settlement_date` <="'.$lastDayofPreviousMonth.'" and ( `from` = "'.$account->email.'" or `to` = "'.$account->email.'" ) and `currency`="'.$statement->currency.'"  order by `created_at` DESC');
        //and `status`="Approved"

        $pagetitle = Carbon::createFromDate($statement->year, $statement->month)->format('F').' '.$statement->year.' ('. $statement->currency .')';
        $breadcrumb_level1 = '<a href="/account/statements">Statements</a>';
        $breadcrumb_level2 = '<a href="/account/statements/'.$statement_id.'">'.Carbon::createFromDate($statement->year, $statement->month)->format('F').' '.$statement->year.' ('. $statement->currency .')</a>';

        return view('account/modules/statements/statement', compact('statement', 'account', 'payments', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'terminals_email', 'terminalemail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
