<?php

namespace App\Http\Controllers\Account\Modules\Settings;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

use DB;
use Validator;
use Auth;
use App\Entities\AccountUser;
use App\Bin;
use App\Http\Controllers\Controller;
use App\Http\Helper\Common;
use PragmaRX\Google2FA\Google2FA;
use Mail;

class SecurityController extends Controller
{
    protected $common;
    public function __construct()
    {
        $this->middleware('account');
        $this->common = new Common;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pagetitle = 'Settings';
        $breadcrumb_level1 = '<a href="/account/settings/account">Settings</a>';
        $breadcrumb_level2 = '<a href="/account/settings/security">Security</a>';

        return view('account/modules/settings/security', compact('pagetitle', 'breadcrumb_level1', 'breadcrumb_level2'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $account_id = Auth::guard('account')->user()->account_id;
        $account_email = Auth::guard('account')->user()->email;
        $account = AccountUser::where('account_id', '=', $account_id)->first();


        $rules = [
            'old_password' => 'required|between:6,16',
            'password' => 'required|regex:/^.*(?=.{3,})(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).*$/|between:8,16|confirmed'
            ];

        $customMessages = [
            "password.between" => "The password must contain at least 1 capital letter, 1 small letter and 1 number. The password must be 8 to 16 characters long.",
            "password.regex" => "The password must contain at least 1 capital letter, 1 small letter and 1 number. The password must be 8 to 16 characters long.",
            'password.required' => 'The password must contain at least 1 capital letter, 1 small letter and 1 number. The password must be 8 to 16 characters long.',
        ];

        $validator = Validator::make($request->all(), $rules, $customMessages);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#password")
                    ->withErrors($validator)
                    ->withInput();
        } else {
            $old_password = $this->common->crypto_encrypt($request->old_password, 'key_account');
            if (trim($old_password) != trim($account->password)) {
                session()->flash('toastr_alert', '<b>Failed!</b> Incorrect old password');
                session()->flash('failed', true);
                return Redirect()->back()->withError('Incorrect old password');
            } else {
                $prv_pass = Bin::where(['user_id' => Auth::guard('account')->user()->account_id, 'user_type'=> 'account', 'field'=>'password'])->orderBy('id', 'desc')->take(5)->pluck('value')->toArray();

                $password = $this->common->crypto_encrypt($request->password, 'key_account');
                if (!in_array($password, $prv_pass)) {
                    $account = AccountUser::where('account_id', '=', $account_id)
                     ->update(['password' => $password,'bad_login' => 0, 'password_date' => date('Y-m-d')]);

                    DB::table('bin')->insert(['user_id' => $account_id, 'field' => 'password', 'value' => $password, 'user_type' => 'account', 'date' => date('Y-m-d')]);

                    session()->flash('toastr_alert', '<b>Great!</b> Password have been changed');
                    session()->flash('success', true);
                } else {
                    session()->flash('toastr_alert', '<b>Failed!</b> Password previously used');
                    session()->flash('failed', true);
                    return Redirect()->back()->withError('Password previously used');
                }

                return Redirect::to(URL::previous());
            }
        }
    }

    public function two_step(Request $request)
    {
        $account_id = Auth::guard('account')->user()->account_id;
        $account = AccountUser::where('account_id', $account_id)->first();
        $account->two_step = $request->two_step_type;
        if ($request->two_step_type == 'ip') {
            $account->ip = $request->ip_address;
        } elseif ($request->two_step_type == 'qr') {
            $google2fa = new Google2FA();
            $secret = $google2fa->generateSecretKey();//32
            $google2fa_url = $google2fa->getQRCodeGoogleUrl(
                config('global.company'),
                $account->email,
                $secret
            );

            //echo $google2fa_url;exit;
            $data['subject'] = config('global.company').' - Google Authenticator';
            $data['preview_text'] = 'Setting up Google Authenticator';
            $data['main_message'] = '<img src="'.$google2fa_url.'" alt="">';
            $data['p1'] = 'Google Authenticator';
            $data['note'] = '<b>Setting up Google Authenticator</b><br>1. Visit the Apple Store or Google Play<br>2. Search for Google Authenticator<br>3. Downoad and install the application<br>4. Open the application and scan above QR Code<br>5. When login and asked to enter verification code open the application to get the 6 digit number from the application';
            $data['email'] = $account->email;
            $data['first_name'] = $account->first_name;
            @Mail::send('emails.app_email', $data, function ($message) use ($data) {
                $message->to($data['email'], $data['first_name'])->subject(config('global.company').' - Google Authenticator');
            });
            $account->secret = $secret;
        }
        $account->save();

        session()->flash('toastr_alert', '<b>Great!</b> Two Factor Authentication successfully set.');
        session()->flash('success', true);
        return Redirect::to(URL::previous());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
