<?php

namespace App\Http\Controllers\Account\Modules\Settings;

use DB;
use Auth;
use App\Account;
use App\Crossrate;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LimitsController extends Controller
{
    public function __construct()
    {
        $this->middleware('account');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $account_id = Auth::guard('account')->user()->account_id;
        $account = Account::where('account_id', '=', $account_id)->first();
        $account_email = $account->email;

        $firstDayofMonth = Carbon::now()->startOfMonth()->toDateString().' 00:00:00';
        $lastDayofMonth = Carbon::now()->endOfMonth()->toDateString().' 23:59:59';
        /*
        $dt1=Carbon::createFromFormat('Y-n-j G:i:s','2015-09-23 20:11:08');
        $dt2 = Carbon::now()->toDateString();
        $datediff=$dt2->diffInYears($dt1->addYear(0));  // 1
        if($datediff==0)
        {
            $dt_year=Carbon::createFromFormat('Y-n-j G:i:s',$account->created_at);
            $startYear=$account->created_at;
            $endYear=$dt1->addYear(1);
            echo "hello";
        }
        else{

            $startYear=$dt1->subYear($datediff);
            $datediff=$datediff+1;
            $endYear=$dt1->addYear($datediff);

        }
        echo $startYear.'==='.$endYear;
        //echo "<br />".$dt->year();
        //echo "<br />".$dt->copy()->addYear();
        exit();
*/

        $dt1=Carbon::createFromFormat('Y-n-j G:i:s', $account->created_at);
        $startYear=$account->created_at;
        $endYear=$dt1->addYear(1);

        $dt_year=Carbon::createFromFormat('Y-n-j G:i:s', $account->created_at);
        $startYear=$account->created_at;
        $endYear=$dt_year->addYear(1);
        //exit();



        $firstDayofMonth = Carbon::now()->startOfMonth()->toDateString().' 00:00:00';
        $lastDayofMonth = Carbon::now()->endOfMonth()->toDateString().' 23:59:59';

        $primaryCurrency = DB::table('wallets')
            ->join('wallet_switches', 'wallets.wallet_id', '=', 'wallet_switches.wallet_id')
            ->where('wallet_switches.account_id', '=', $account_id)
            ->where('wallets.primary', '=', '1')
            ->select('wallets.currency', 'wallets.wallet_limit_id', 'wallets.wallet_id')->first();

        if (count($primaryCurrency) < 1) {
            $wallet_primary = DB::table('wallets')
            ->join('wallet_switches', 'wallets.wallet_id', '=', 'wallet_switches.wallet_id')
            ->where('wallet_switches.account_id', '=', $account_id)
            ->where('wallets.primary', '=', '0')->select('wallets.*')->first();
            if (count($wallet_primary) > 0) {
                $wallet_primary = DB::table('wallets')->where('wallets.wallet_id', '=', $wallet_primary->wallet_id)->update(['wallets.primary' => 1]);


                $primaryCurrency = DB::table('wallets')
                ->join('wallet_switches', 'wallets.wallet_id', '=', 'wallet_switches.wallet_id')
                ->where('wallet_switches.account_id', '=', $account_id)
                ->where('wallets.primary', '=', '1')
                ->select('wallets.currency')->first();
            }
        }
        $primary_currency=$primaryCurrency->currency;
        $primary_limit=$primaryCurrency->wallet_limit_id;
        $wallet_id=$primaryCurrency->wallet_id;
        $wallet_limit =$wallet_EUR_limit = DB::table('walletlimits')->where(['wallet_limit_id' => $primary_limit])->first();


        $primaryCurrency_symbol = DB::table('currencies')->where('active', '=', 1)->where('cy_code', '=', $primary_currency) ->select('cy_symbol')->first();
        $primary_symbol=$primaryCurrency_symbol->cy_symbol;


        $fixed_currency="EUR";
        if ($fixed_currency!=$primary_currency) {
            $crossrate = Crossrate::where(['from_currency' =>"EUR", 'to_currency' => $primary_currency])->value('crossrate');
            //$wallet->balance=;
            $wallet_limit->max_balance=number_format($wallet_limit->max_balance * $crossrate, 2, '.', '');
            $wallet_limit->max_turnover_per_month=number_format($wallet_limit->max_turnover_per_month * $crossrate, 2, '.', '');
            $wallet_limit->max_turnover=number_format($wallet_limit->max_turnover * $crossrate, 2, '.', '');
            $wallet_limit->max_withdraw_amount=number_format($wallet_limit->max_withdraw_amount * $crossrate, 2, '.', '');
        }


        $walletCurrency = DB::table('wallets')
            ->join('wallet_switches', 'wallets.wallet_id', '=', 'wallet_switches.wallet_id')
            ->where('wallet_switches.account_id', '=', $account_id)
            ->select('wallets.currency', 'wallets.balance', 'wallets.wallet_id')->get();
        $convarted_balance=0;
        $current_turnover=0;
        $current_withdrawal=0;
        $monthly_turnover=0;
        $payment_credits=0;
        foreach ($walletCurrency as $walletBalance) {
            $currency=$walletBalance->currency;
            $balance= $walletBalance->balance;
            $walletid=$walletBalance->wallet_id;

            $monthly_payment_credits = DB::table('payments')
                                //->select(DB::raw('SUM(total_amount) as credits'))
                                ->whereDate('settlement_date', '>=', "$firstDayofMonth")
                                ->whereDate('settlement_date', '<=', "$lastDayofMonth")
                                ->where('status', '=', "Approved")
                                ->whereIn('payment_type', ['Send','TopUp','Request','Pay'])
                                ->where('to', '=', "$account_email")
                                ->where('currency', '=', "$currency")
                                ->groupBy('currency')
                                ->orderBy('created_at', 'ASC')
                                ->sum('total_amount');


            $payment_credits = DB::table('payments')
                                ->where('status', '=', "Approved")
                                ->whereDate('settlement_date', '>=', "$startYear")
                                ->whereDate('settlement_date', '<=', "$endYear")
                                ->whereIn('payment_type', ['Send','TopUp','Request','Pay'])
                                ->where('to', '=', "$account_email")
                                ->where('currency', '=', "$currency")
                                ->groupBy('currency')
                                ->orderBy('created_at', 'ASC')
                                ->sum('total_amount');

            $current_withdraw = DB::table('payments')
                ->whereDate('settlement_date', '>=', "$startYear")
                ->whereDate('settlement_date', '<=', "$endYear")
                ->where('payment_type', '=', 'Withdraw')
                ->where('from', '=', "$account_email")
                ->where('status', '=', 'Approved')
                ->where('currency', '=', "$currency")
                ->groupBy('currency')
                ->sum('total_amount');

            if ($currency==$primary_currency) {
                $convarted_balance=$convarted_balance + $balance;
                $current_turnover=$current_turnover+$payment_credits;
                $current_withdrawal=$current_withdrawal+$current_withdraw;
                $monthly_turnover=$monthly_turnover+$monthly_payment_credits;
            } else {
                $crossrate = Crossrate::where(['from_currency'=>$currency, 'to_currency' => $primary_currency])->value('crossrate');

                $convarted_balance = $convarted_balance + ($balance * $crossrate);

                $current_turnover=$current_turnover + ($payment_credits * $crossrate);
                $current_withdrawal=$current_withdrawal + ($current_withdraw * $crossrate);
                $monthly_turnover=$monthly_turnover + ($monthly_payment_credits * $crossrate);
            }

            $convarted_balance=number_format($convarted_balance, 2, '.', '');
            $current_turnover=number_format($current_turnover, 2, '.', '');
            $current_withdrawal=number_format($current_withdrawal, 2, '.', '');
            $monthly_turnover=number_format($monthly_turnover, 2, '.', '');
        }


        $wallet= DB::table('wallets')
            ->join('wallet_switches', 'wallets.wallet_id', '=', 'wallet_switches.wallet_id')
            ->where('wallet_switches.account_id', '=', $account_id)
            ->where('wallets.currency', '=', 'EUR')
            ->select('wallets.*')
            ->first();
        /*  $wallet_id = $wallet->wallet_id;

        //TopUp, Send (credit only), Request (credit only) - turnover

            $current_turnover = DB::table('payments')
                ->where([
                    ['wallet_id', '=', $wallet_id],
                    ['payment_type', '=', 'Send'],
                    ['status', '=', 'Approved'],
                    ['to', '=', Auth::guard('account')->user()->email],
                ])
                ->sum('total_amount');
        */
        //$current_withdrawal=100.00;
        $current_turnover_bar = $current_balance_bar = $current_withdrawal_bar = $monthly_turnover_bar = 0;
        if ($current_turnover > 0 && $wallet_limit->max_turnover > 0) {
            $current_turnover_bar =  ($current_turnover / $wallet_limit->max_turnover) * 100;
        }
        if ($convarted_balance > 0 && $wallet_limit->max_balance > 0) {
            $current_balance_bar =  ($convarted_balance / $wallet_limit->max_balance) * 100;
        }
        if ($current_withdrawal > 0 && $wallet_limit->max_withdraw_amount > 0) {
            $current_withdrawal_bar =  ($current_withdrawal / $wallet_limit->max_withdraw_amount) * 100;
        }
        if ($monthly_turnover > 0 && $wallet_limit->max_turnover_per_month > 0) {
            $monthly_turnover_bar =  ($monthly_turnover / $wallet_limit->max_turnover_per_month) * 100;
        }






        //echo $current_withdrawal_bar;

        /*

        $wallet->balance;
        $wallet_limit->max_balance
        $wallet_limit->max_turnover_per_mont
        $wallet_limit->max_turnover

        $wallet_limit->max_withdraw_amount



        $wallet= DB::table('wallets')
            ->join('wallet_switches', 'wallets.wallet_id', '=', 'wallet_switches.wallet_id')
            ->where('wallet_switches.account_id', '=', $account_id)
            ->where('wallets.currency', '=', 'EUR')
            ->select('wallets.*')
            ->first();
        $wallet_id = $wallet->wallet_id;


        //$wallet_limit = DB::table('walletlimits')->where('wallet_limit_id', [$wallet->wallet_limit_id])->first();

        $current_turnover = DB::table('payments')
            ->where([
                ['wallet_id', '=', $wallet_id],
                ['payment_type', '=', 'Send'],
                ['status', '=', 'Approved'],
                ['to', '=', Auth::guard('account')->user()->email],
            ])
            ->sum('total_amount');

        $current_turnover_bar =  ($current_turnover / $wallet_limit->max_turnover) * 100;
        $current_balance_bar =  ($wallet->balance / $wallet_limit->max_balance) * 100;
        $current_withdrawal = DB::table('payments')
            ->where('wallet_id', [$wallet_id])
            ->where('payment_type', '=', 'Withdraw')
            ->where('status', '=', 'Approved')
            ->sum('total_amount');
        $current_withdrawal_bar =  ($current_withdrawal / $wallet_limit->max_withdraw_amount) * 100;


        */
        $pagetitle = 'Settings';
        $breadcrumb_level1 = '<a href="/account/settings/account">Settings</a>';
        $breadcrumb_level2 = '<a href="/account/settings/limits">Limits</a>';

        return view(
            'account/modules/settings/limits',
            compact(
            //'account',
                    'wallet',
                    'wallet_limit',
                    'convarted_balance',
                    'current_turnover',
                    'current_turnover_bar',
                    'current_balance_bar',
                    'current_withdrawal',
                    'current_withdrawal_bar',
                    'monthly_turnover',
                    'monthly_turnover_bar',
                    'pagetitle',
                    'breadcrumb_level1',
                    'breadcrumb_level2',
                    'primary_symbol'
                    )
            );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
