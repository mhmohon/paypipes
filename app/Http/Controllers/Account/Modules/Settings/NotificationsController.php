<?php

namespace App\Http\Controllers\Account\Modules\Settings;

use DB;
use Auth;
use App\Account;
use App\Activity;
use App\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class NotificationsController extends Controller
{
    public function __construct()
    {
        $this->middleware('account');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $account_id = Auth::guard('account')->user()->account_id;
        $account = Account::where('account_id', '=', $account_id)->first();

        $notification = DB::table('notifications')->where('email', '=', $account->email)->first();
        $account = Account::where('account_id', '=', $account_id)->first();

        if (!isset($notification->notification_id)) {
            $notification_id = Notification::notificationid();

            $notification = new Notification();
            $notification['email'] = Auth::guard('account')->user()->email;
            $notification['added_by'] = Auth::guard('account')->user()->name;
            $notification['notification_id'] = $notification_id;
            $notification['add_money'] =        1;
            $notification['withdraw_money'] =   1;
            $notification['send_money'] =       1;
            $notification['request_money'] =    1;
            $notification['receive_money'] =    1;
            $notification['transfer_money'] =   1;
            $notification['approve_decline'] = 1;
            $notification['add_currency'] =     1;
            $notification['add_edit_card'] =    1;
            $notification['add_edit_settings']=1;
            $notification['receive_message'] = 1;
            $notification['receive_offer'] =    1;

            $notification->save();
            $notification = DB::table('notifications')->where('account_id', '=', $account_id)->first();
        }


        $pagetitle = 'Settings';
        $breadcrumb_level1 = '<a href="/account/settings/account">Settings</a>';
        $breadcrumb_level2 = '<a href="/account/settings/notifications">Notifications</a>';

        return view('account/modules/settings/notifications', compact('notification', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function notifications(Request $request, $notification_id)
    {
        $notification = Notification::where('notification_id', '=', $notification_id)->firstOrFail();

        if (isset($request->add_money)) {
            $notification['add_money'] =        $request->add_money;
        }
        if (isset($request->withdraw_money)) {
            $notification['withdraw_money'] =   $request->withdraw_money;
        }
        if (isset($request->send_money)) {
            $notification['send_money'] =       $request->send_money;
        }
        if (isset($request->request_money)) {
            $notification['request_money'] =    $request->request_money;
        }
        if (isset($request->receive_money)) {
            $notification['receive_money'] =    $request->receive_money;
        }
        if (isset($request->transfer_money)) {
            $notification['transfer_money'] =   $request->transfer_money;
        }
        if (isset($request->approve_decline)) {
            $notification['approve_decline'] = $request->approve_decline;
        }
        if (isset($request->add_currency)) {
            $notification['add_currency'] =     $request->add_currency;
        }
        if (isset($request->add_edit_card)) {
            $notification['add_edit_card'] =    $request->add_edit_card;
        }
        if (isset($request->add_edit_settings)) {
            $notification['add_edit_settings']= $request->add_edit_settings;
        }
        if (isset($request->receive_message)) {
            $notification['receive_message'] = $request->receive_message;
        }
        if (isset($request->receive_offer)) {
            $notification['receive_offer'] =    $request->receive_offer;
        }

        $notification->save();


        $activity = new Activity();
        $activity['account_id'] = Auth::guard('account')->user()->account_id;
        $activity['added_by'] = Auth::guard('account')->user()->name;
        $activity['interface'] = 'Account';
        $activity['module'] = 'Settings';
        $activity['activity'] = 'Settings has been update by '. Auth::guard('account')->user()->name .'.';

        session()->flash('toastr_alert', '<b>Great!</b> Settings has been update by '. Auth::guard('account')->user()->name .'.');
        session()->flash('info', true);

        return Redirect::to(URL::previous());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
