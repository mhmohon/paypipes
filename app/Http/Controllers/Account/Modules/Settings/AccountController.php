<?php

namespace App\Http\Controllers\Account\Modules\Settings;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

use Validator;
use Auth;
use App\Activity;
use App\Account;
use App\Country;
use App\Http\Controllers\Controller;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('account');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['account_id'] = Auth::guard('account')->user()->account_id;
        $data['account'] = Account::where('account_id', '=', $data['account_id'])->first();
        $data['countries'] = ['0'=>'Select a Country'] + Country::pluck('country_name', 'country_code')->all();
        $data['pagetitle'] = 'Settings';
        $data['breadcrumb_level1'] = '<a href="/account/settings/account">Settings</a>';
        $data['breadcrumb_level2'] = '<a href="/account/settings/account">Account</a>';

        return view('account/modules/settings/account', $data);
    }


    /**
     * Account Forms.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $account_id)
    {
        $validator = Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name' => 'required',
                'street' => 'required',
                'city' => 'required',
                'country' => 'required',
                'post_code' => 'required',
                'phone' => 'required',
            ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#request")
                    ->withErrors($validator)
                    ->withInput();
        }

        $account_id = Auth::guard('account')->user()->account_id;

        $account = Account::where('account_id', '=', $account_id)->firstOrFail();
        $account->first_name    =   $request->first_name;
        $account->last_name     =   $request->last_name;
        $account->name         =   $request->first_name.' '.$request->last_name;
        $account->phone        =   $request->phone;
        $account->street       =   $request->street;
        $account->city         =   $request->city;
        $account->state        =   $request->state;
        $account->country      =   $request->country;
        $account->post_code    =   $request->post_code;
        $account->save();

        $activity = new Activity();
        $activity['account_id'] = Auth::guard('account')->user()->account_id;
        $activity['added_by'] = Auth::guard('account')->user()->name;
        $activity['interface'] = 'Account';
        $activity['module'] = 'Settings';
        $activity['activity'] = 'Your phone number has been updated.';
        $activity->save();

        session()->flash('toastr_alert', '<b>Great!</b> Your phone number has been updated.');
        session()->flash('success', true);

        return Redirect::to(URL::previous());
    }
}
