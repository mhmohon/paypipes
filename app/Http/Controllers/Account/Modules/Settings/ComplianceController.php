<?php

namespace App\Http\Controllers\Account\Modules\Settings;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Input;

use DB;
use Auth; 
use Validator;
use App\Activity;
use App\Account;
use App\Compliance;
use App\Compliance_switch;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ComplianceController extends Controller
{    
    public function __construct()
    {
        $this->middleware('account');
    }
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $account_id = Auth::guard('account')->user()->account_id;
		$account = Account::where('account_id', '=', $account_id)->first();
		$compliances = DB::table('compliances')
            ->join('compliance_switches', 'compliances.compliance_id', '=', 'compliance_switches.compliance_id')
            ->where('compliance_switches.account_id', '=', $account_id)
            ->select('compliances.*')
			->orderBy('created_at','desc')
            ->paginate(5);
		
		$pagetitle = 'Settings';
        $breadcrumb_level1 = '<a href="/account/settings/account">Settings</a>';
        $breadcrumb_level2 = '<a href="/account/settings/compliances">Compliance</a>';
        
        return view('account/modules/settings/compliances', compact('account', 'compliances', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$account_id = Auth::guard('account')->user()->account_id;
		$account = Account::where('account_id', '=', $account_id)->first();
		$compliances = DB::table('compliances')
            ->join('compliance_switches', 'compliances.compliance_id', '=', 'compliance_switches.compliance_id')
            ->where('compliance_switches.account_id', '=', $account_id)
            ->select('compliances.*')
			->orderBy('created_at','desc')
            ->paginate(5);
		
		$pagetitle = 'Settings';
        $breadcrumb_level1 = '<a href="/account/settings/account">Settings</a>';
        $breadcrumb_level2 = '<a href="/account/settings/compliances">Compliance</a>';
        
        return view('account/modules/settings/compliances', compact('account', 'compliances', 'pagetitle', 'breadcrumb_level1', 'breadcrumb_level2'));
    }
	
	public function store(Request $request)
    {
		 // getting all of the post data
		 $file = array('upload_file' => Input::file('upload_file'));
 
		  $validator = Validator::make($request->all(), [
            'compliance_name' => 'required|max:60',
            'description' => 'required|max:100',
			'upload_file' => 'required',
        ]);

        if ($validator->fails()) {
            return Redirect::to(URL::previous() . "#add")
                ->withErrors($validator)
                ->withInput();
        }
		
		$account_id = Auth::guard('account')->user()->account_id;		
		$account = Account::where('account_id', '=', $account_id)->first();
		
		// checking file is valid.
		if (Input::file('upload_file')->isValid()) {
			
		$destinationPath = 'uploads/compliance'; // upload path
		$extension = Input::file('upload_file')->getClientOriginalExtension(); // getting image extension
		$fileName = rand(11111,99999).'.'.$extension; // renameing image
		Input::file('upload_file')->move($destinationPath, $fileName); // uploading file to given path
		// sending back with message
		//Session::flash('success', 'Upload successfully'); 
		//  return Redirect::to('admin/accounts');
		}
		else {
			
		// sending back with error message.
		//Session::flash('error', 'uploaded file is not valid');
		// return Redirect::to('admin/accounts');
		  
		}
		$compliance_id=rand(11111,99999);
		$compliance_switch = new Compliance_switch();
		$compliance_switch['compliance_id'] = $compliance_id;
		$compliance_switch['account_id'] = $account->account_id;
		$compliance_switch['added_by'] = Auth::guard('account')->user()->name;
		$compliance_switch->save();
		
       	$compliance = new Compliance($request->all());
		$compliance['compliance_id'] = $compliance_id;
		$compliance['name'] = $request->compliance_name;
		$compliance['file_name'] = $fileName;
		$compliance['file_path'] = $destinationPath.'/'.$fileName;
		$compliance['file_type'] = $extension;
		$compliance['added_by'] = Auth::guard('account')->user()->name;
		$compliance['status'] = 'Pending';
		$compliance->save();	
		
        $activity = new Activity ();
        $activity['account_id'] = Auth::guard('account')->user()->account_id;
		$activity['added_by'] = Auth::guard('account')->user()->name;
		$activity['interface'] = 'Account';
        $activity['module'] = 'Account';
        $activity['activity'] = 'New compliance document '. $compliance->name .' has been added by '. Auth::guard('account')->user()->name .'';
		$activity->save();
        
        session()->flash('toastr_alert','<b>Added!</b> New compliance document '. $compliance->name .' has been added by '. Auth::guard('account')->user()->name .'');
		session()->flash('success', true);
		
        return Redirect::to(URL::previous());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function view($compliance_id)
    {
        $account_id = Auth::guard('account')->user()->account_id;
		$account = Account::where('account_id', '=', $account_id)->first();
        $compliance = DB::table('compliances')->where('compliance_id', '=', $compliance_id)->first();
        $pagetitle = 'Settings';
		$subpagetitle = $compliance->name;
        $breadcrumb_level1 = '<a href="/account/settings/account">Settings</a>';
        $breadcrumb_level2 = '<a href="/account/settings/compliances">Compliance</a>';
		$breadcrumb_level3 = '<a href="/account/settings/compliances/'.$compliance->compliance_id.'">'.$compliance->name.'</a>';
        
        return view('account/modules/settings/compliance', compact('account', 'compliance', 'pagetitle', 'subpagetitle', 'breadcrumb_level1', 'breadcrumb_level2', 'breadcrumb_level3', 'compliance_id'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
