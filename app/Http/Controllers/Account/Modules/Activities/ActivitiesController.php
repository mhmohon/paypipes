<?php

namespace App\Http\Controllers\Account\Modules\Activities;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\URL;

use DB;
use Validator;
use Auth;
use App\Activity;
use App\Payment;
use App\Wallet;
use App\Dispute;
use App\Disputecomment;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class ActivitiesController extends Controller
{   
    public function __construct()
    {
        $this->middleware('account');
    }
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $account_id = Auth::guard('account')->user()->account_id;
		$account_email = Auth::guard('account')->user()->email;
		
		$activities = DB::table('activities')->where(function($query) use ($request) {	
			//filter by keyword
			if (($search = $request->get('search'))) {
				$query->orWhere('activity_id', 'like', '%' . $search . '%');
				$query->orWhere('activity', 'like', '%' . $search . '%');
				$query->orWhere('interface', 'like', '%' . $search . '%');
				$query->orWhere('module', 'like', '%' . $search . '%');
			}
		})
		->where('account_id', '=', $account_id)
		->orderBy('created_at','desc')
		->paginate(10);
			
        $pagetitle = 'Activities';
        $breadcrumb_level1 = '<a href="/account/activities">Activities</a>';
        
        return view('account/modules/activities/activities', compact('activities', 'pagetitle', 'breadcrumb_level1'));
    }
}
