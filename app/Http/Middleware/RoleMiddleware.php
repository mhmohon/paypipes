<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
	
	public function handle($request, Closure $next, $role, $permission)
	{
		if (Auth::guard('admin')->guest()) {
			$request->session()->flush();
			return redirect('/logout');
		}

		if (! Auth::guard('admin')->user()->hasRole($role)) {
		   abort(403);
		}

		if (! Auth::guard('admin')->user()->can($permission)) {
		   abort(403);
		}

		return $next($request);
	}
}
