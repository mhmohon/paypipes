<?php

namespace App\Http\Middleware;

use Closure;

use App\Http\Helper\RolePermission;
use Auth;
class CheckPermission
{
    protected $helper;

    /**
     * Creates a new instance of the middleware.
     *
     * @param Guard $auth
     */
    public function __construct(RolePermission $helper)
    {
        $this->helper = $helper;
    }
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permissions)
    {
		if(isset(Auth::guard('admin')->user()->admin_id))
		{
			if($this->helper->has_permission(\Auth::guard('admin')->user()->admin_id, $permissions)){
				return $next($request);
			}
			else
			{
				abort(403, 'Unauthorized action.');
			}
		
		}	
        else{
            abort(403, 'Unauthorized action.');            
        }
        
    }
}
