<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Carbon\Carbon;
use Illuminate\Encryption\Encrypter;

class updatecryptokey extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'updatecryptokey';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'execute command to update main key and crypto key in database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        umask(0007);
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $clipher='AES-256-CBC';
        $len=openssl_cipher_iv_length($clipher);
        $dt=Carbon::now();

        $main_key     =bin2hex(random_bytes($len));
        $key_user     =bin2hex(random_bytes($len));
        $key_merchant =bin2hex(random_bytes($len));
        $key_agent    =bin2hex(random_bytes($len));
        $key_psp      =bin2hex(random_bytes($len));
        $key_customer =bin2hex(random_bytes($len));
        $key_card     =bin2hex(random_bytes($len));
        $key_api      =bin2hex(random_bytes($len));

        $genkey = new Encrypter($main_key, $clipher);

        $key_user_enc=$genkey->encryptString($key_user);
        $key_merchant_enc=$genkey->encryptString($key_merchant);
        $key_agent_enc=$genkey->encryptString($key_agent);
        $key_psp_enc=$genkey->encryptString($key_psp);
        $key_customer_enc=$genkey->encryptString($key_customer);
        $key_card_enc=$genkey->encryptString($key_card);
        $key_api_enc=$genkey->encryptString($key_api);


        $key_user_dec=$genkey->decryptString($key_user_enc);
        $key_merchant_dec=$genkey->decryptString($key_merchant_enc);
        $key_agent_dec=$genkey->decryptString($key_agent_enc);
        $key_psp_dec=$genkey->decryptString($key_psp_enc);
        $key_customer_dec=$genkey->decryptString($key_customer_enc);
        $key_card_dec=$genkey->decryptString($key_card_enc);
        $key_api_dec=$genkey->decryptString($key_api_enc);

        $main_key_lastid=DB::table('main_key')->orderBy('version', 'desc')->first();

        $mainkey= [];
        $mainkey['description']='Version update';
        $mainkey['version']=$main_key_lastid->version + 1;
        $mainkey['main_key']=$main_key;
        $mainkey['status']='Active';
        $mainkey['created_at']=$dt->toDateTimeString();
        $mainkey['added_by']='SYSTEM';
        $mainkeyid=DB::table('main_key')->insertGetId($mainkey);
        DB::table('main_key')->where('id', '<', $mainkeyid)->update(['status'=>'Inactive','updated_by'=>'SYSTEM','updated_at'=>$dt->toDateTimeString()]);


        $crypto_key_lastid=DB::table('crypto_key')->orderBy('version', 'desc')->first();

        $cryptokey= [];
        $cryptokey['description']='Version update';
        $cryptokey['version']=$crypto_key_lastid->version + 1;
        $cryptokey['key_user']=$key_user_enc;
        $cryptokey['key_merchant']=$key_merchant_enc;
        $cryptokey['key_agent']=$key_agent_enc;
        $cryptokey['key_psp']=$key_psp_enc;
        $cryptokey['key_customer']=$key_customer_enc;
        $cryptokey['key_card']=$key_card_enc;
        $cryptokey['key_api']=$key_api_enc;
        $cryptokey['status']='Active';
        $cryptokey['created_at']=$dt->toDateTimeString();
        $cryptokey['added_by']='SYSTEM';
        $cryptokeyid=DB::table('crypto_key')->insertGetId($cryptokey);
        DB::table('crypto_key')->where('id', '<', $cryptokeyid)->update(['status'=>'Inactive','updated_by'=>'SYSTEM','updated_at'=>$dt->toDateTimeString()]);
    }
}
