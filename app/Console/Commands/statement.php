<?php
namespace App\Console\Commands;

use DB;
use App\Wallet;
use Carbon\Carbon;
use App\Entities\AccountStatement;
use App\Wallet_statements;
use Illuminate\Console\Command;

class Statement extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'statement';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create monthly statement for Merchant/Terminal/Wallet description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        umask(0007);
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ismonth=1;
        $week=0;
        $day=0;
        //exit();
        //echo $year.' -- '.$month.'<br/>';
        //if($ismonth==1)
        //{
        $dt=Carbon::now();
        $dt->subMonths(1);
        $year=$dt->year;
        $month=$dt->month;

        $firstDayofPreviousMonth=$dt->startOfMonth()->toDateString().' 00:00:00';
        $lastDayofPreviousMonth =$dt->endOfMonth()->toDateString().' 23:59:59';
        $executation_datetime=$dt->startOfMonth()->addMonth()->toDateString().' 00:30:00';

        //$firstDayofPreviousMonth = Carbon::now()->subMonth()->startOfMonth()->toDateString().' 00:00:00';
        //$lastDayofPreviousMonth = Carbon::now()->subMonth()->endOfMonth()->toDateString().' 23:59:59';
        //$firstDayofPreviousMonth = Carbon::create(2016, 9, 1, 0, 0, 0);
        //$lastDayofPreviousMonth = Carbon::create(2016, 9, 30, 23, 59, 59);
        //}

        //echo $firstDayofPreviousMonth.' - '.$lastDayofPreviousMonth.' -- '.$executation_datetime;exit();



        //**************Terminal Statement**********************************//

        $account=DB::table('accounts')->whereIn('status', ['Active','Inactive'])->get()->toArray();
        $account = json_decode(json_encode((array) $account), true);
        for ($i=0;$i<count($account);$i++) {
            $account_currency = DB::table('accounts')
                                ->select('wallets.currency')
                                ->join('wallet_switches', 'accounts.account_id', '=', 'wallet_switches.account_id')
                                ->join('wallets', 'wallet_switches.wallet_id', '=', 'wallets.wallet_id')
                                ->where('accounts.account_id', '=', $account[$i]['account_id'])
                                ->get()->toArray();
            $account_currency = json_decode(json_encode((array) $account_currency), true);
            $account_currency_count=count($account_currency);

            if ($account_currency_count>0) {
                for ($j=0;$j<$account_currency_count;$j++) {
                    $payment_debits = DB::table('payments')
                                        ->select(DB::raw('SUM(total_amount) as debits'), 'currency')
                                        ->whereDate('settlement_date', '>=', "$firstDayofPreviousMonth")
                                        ->whereDate('settlement_date', '<=', "$lastDayofPreviousMonth")
                                        ->where('from', '=', $account[$i]['email'])
                                        ->where('currency', '=', $account_currency[$j]['currency'])
                                        ->groupBy('currency')
                                        ->orderBy('created_at', 'ASC')
                                        ->get()->toArray();
                    $payment_debits = json_decode(json_encode((array) $payment_debits), true);
                    if (count($payment_debits)>0 && $payment_debits[0]['debits']!='') {
                        $debits=$payment_debits[0]['debits'];
                    } else {
                        $debits=0;
                    }



                    $payment_credits = DB::table('payments')
                                        ->select(DB::raw('SUM(total_amount) as credits'), 'currency')
                                        ->whereDate('settlement_date', '>=', "$firstDayofPreviousMonth")
                                        ->whereDate('settlement_date', '<=', "$lastDayofPreviousMonth")
                                        ->where('to', '=', $account[$i]['email'])
                                        ->where('currency', '=', $account_currency[$j]['currency'])
                                        ->groupBy('currency')
                                        ->orderBy('created_at', 'ASC')
                                        ->get()->toArray();
                    $payment_credits = json_decode(json_encode((array) $payment_credits), true);
                    if (count($payment_credits)>0 && $payment_credits[0]['credits']!='') {
                        $credits=$payment_credits[0]['credits'];
                    } else {
                        $credits=0;
                    }

                    //print_r($payment_credits);
                    //exit();


                    $account_statement=DB::table('account_statements')->select('account_statement_id')->orderBy('account_statement_id', 'desc')->take(1)->get();
                    if (count($account_statement)>0) {
                        $account_statement_id=$account_statement[0]->account_statement_id;
                    } else {
                        $account_statement_id=0;
                    }

                    if ($account_statement_id < 1) {
                        $account_statement_id=1001;
                    } else {
                        $account_statement_id = $account_statement_id + 1;
                    }



                    $account_statements=DB::table('account_statements')
                                            ->where('account_id', '=', $account[$i]['account_id'])
                                            ->where('currency', '=', $account_currency[$j]['currency'])
                                            ->orderBy('id', 'desc')->first();

                    if (count($account_statements)>0) {
                        $ending_balance=$account_statements->ending_balance;
                    } else {
                        $ending_balance=0;
                    }

                    $ter_state = new AccountStatement();
                    $ter_state['account_statement_id'] = $account_statement_id;
                    $ter_state['account_id'] = $account[$i]['account_id'];
                    $ter_state['month'] = $month;
                    $ter_state['year'] = $year;
                    $ter_state['credits'] = $credits;
                    $ter_state['debits'] = $debits;
                    $ter_state['ending_balance'] = $ending_balance+$credits-$debits;
                    $ter_state['currency'] =  $account_currency[$j]['currency'];
                    $ter_state['created_at'] = "$executation_datetime";
                    $ter_state['updated_at'] = "$executation_datetime";
                    $ter_state['status'] = 'Active';
                    $ter_state['added_by'] = 'SYSTEM';
                    $ter_state['updated_by'] = 'SYSTEM';

                    $ter_state->save();
                }
            }
        }



        //**************Wallet Statement************************************//

        $wallets=DB::table('wallets')->whereIn('status', ['Active','Inactive'])->get()->toArray();
        $wallets = json_decode(json_encode((array) $wallets), true);
        $wallets_count=count($wallets);

        for ($i=0;$i<$wallets_count;$i++) {
            $payment_debits = DB::table('payments')
                                        ->select(DB::raw('SUM(total_amount) as debits'), 'currency')
                                        ->whereDate('settlement_date', '>=', "$firstDayofPreviousMonth")
                                        ->whereDate('settlement_date', '<=', "$lastDayofPreviousMonth")
                                        ->where('from', '=', $wallets[$i]['email'])
                                        ->where('currency', '=', $wallets[$i]['currency'])
                                        ->groupBy('currency')
                                        ->orderBy('created_at', 'ASC')
                                        ->get()->toArray();
            $payment_debits = json_decode(json_encode((array) $payment_debits), true);
            if (count($payment_debits)>0 && $payment_debits[0]['debits']!='') {
                $debits=$payment_debits[0]['debits'];
            } else {
                $debits=0;
            }



            $payment_credits = DB::table('payments')
                                        ->select(DB::raw('SUM(total_amount) as credits'), 'currency')
                                        ->whereDate('settlement_date', '>=', "$firstDayofPreviousMonth")
                                        ->whereDate('settlement_date', '<=', "$lastDayofPreviousMonth")
                                        ->where('to', '=', $wallets[$i]['email'])
                                        ->where('currency', '=', $wallets[$i]['currency'])
                                        ->groupBy('currency')
                                        ->orderBy('created_at', 'ASC')
                                        ->get()->toArray();
            $payment_credits = json_decode(json_encode((array) $payment_credits), true);
            if (count($payment_credits)>0 && $payment_credits[0]['credits']!='') {
                $credits=$payment_credits[0]['credits'];
            } else {
                $credits=0;
            }


            $wallet_statement=DB::table('wallet_statements')->select('wallet_statement_id')->orderBy('wallet_statement_id', 'desc')->take(1)->get();
            if (count($wallet_statement)>0) {
                $wallet_statement_id=$wallet_statement[0]->wallet_statement_id;
            } else {
                $wallet_statement_id=0;
            }

            if ($wallet_statement_id < 1) {
                $wallet_statement_id=1001;
            } else {
                $wallet_statement_id = $wallet_statement_id + 1;
            }



            $wallet_statements=DB::table('wallet_statements')
                                            ->where('wallet_id', '=', $wallets[$i]['wallet_id'])
                                            ->where('currency', '=', $wallets[$i]['currency'])
                                            ->orderBy('id', 'desc')->first();

            if (count($wallet_statements)>0) {
                $ending_balance=$wallet_statements->ending_balance;
            } else {
                $ending_balance=0;
            }

            $wallet_state = new Wallet_statements();
            $wallet_state['wallet_id'] =  $wallets[$i]['wallet_id'];
            $wallet_state['wallet_statement_id'] = $wallet_statement_id;
            $wallet_state['month'] = $month;
            $wallet_state['year'] = $year;
            $wallet_state['credits'] = $credits;
            $wallet_state['debits'] = $debits;
            $wallet_state['ending_balance'] = $ending_balance+$credits-$debits;
            $wallet_state['currency'] =  $wallets[$i]['currency'];
            $wallet_state['created_at'] = "$executation_datetime";
            $wallet_state['updated_at'] = "$executation_datetime";
            $wallet_state['status'] = 'Active';
            $wallet_state['added_by'] = 'SYSTEM';
            $wallet_state['updated_by'] = 'SYSTEM';

            $wallet_state->save();
        }
    }
}
