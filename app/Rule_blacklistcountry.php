<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use App\Rule_country;

class Rule_blacklistcountry extends Model
{
    protected $fillable =[
        'customer_id',
        'merchant_id',
        'terminal_id',
        'country',
        'type',
        'status',
    ];
    
    protected $connection = 'mysqlfraud';
	
	public static function search($request,$where)
    {
	   $item_per_page = !empty($request->rows)?$request->rows:ROW_PER_PAGE;
	   
	   $country_code=$request->get('search');
	   if(!empty($country_code))$country_code= Rule_country::countrycode($country_code);	  
       $search = Rule_blacklistcountry::leftjoin('rule_countries', 'rule_blacklistcountries.country', '=', 'rule_countries.country_code')
			->where(function($query) use ($request,$country_code) {
		
			if (($search = $request->get('search'))) {
				if(!empty($country_code))$query->orWhere('country', 'like', '%' . $country_code . '%');
				$query->orWhere('type', 'like', $search . '%');
				$query->orWhere('status', 'like', $search . '%');
			}
		})		
		->where($where)		
		->select('rule_blacklistcountries.*','rule_countries.country_name')
		->orderBy('created_at','desc')
		->paginate($item_per_page);		
		return $search;
    }
	
	public static function searchadmin($request)
    {
	   $item_per_page = !empty($request->rows)?$request->rows:ROW_PER_PAGE;
	   
	   $country_code=$request->get('search');	   
	   if(!empty($country_code))$country_code= Rule_country::countrycode($country_code);
	   
       $search = Rule_blacklistcountry::leftjoin('rule_countries', 'rule_blacklistcountries.country', '=', 'rule_countries.country_code')
	   ->where(function($query) use ($request,$country_code) {
			//filter by keyword
			if (($search = $request->get('search'))) {
				if(!empty($country_code))$query->orWhere('country', 'like', '%' . $country_code . '%');
				$query->orWhere('psp_id', 'like', '%' . $search . '%');
				$query->orWhere('merchant_id', 'like', '%' . $search . '%');				
				$query->orWhere('type', 'like', $search . '%');
				$query->orWhere('status', 'like', $search . '%');
			}
		})
		->select('rule_blacklistcountries.*','rule_countries.country_name')
		->orderBy('created_at','desc')
		->paginate($item_per_page);
		
		return $search;
    }

    public function getCountryNameAttribute(){
        $code = $this->attributes['country'];
        $country = DB::table('rule_countries')->where('country_code', $code)->first();
        if(isset($country->country_name)) return $country->country_name;
        else return '';
    }
}
