<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paybylink extends Model
{
    protected $fillable =[
        'merchant_id',
        'terminal_id',
        'amount',
        'currency',
        'email',
        'first_name',
		'last_name',
		'phone',
		'order_id',
		'expire_link',
    ];	
	
	public static function  details($token)
    {
		return Paybylink::where('token', '=', $token)->orderBy('id','desc')->first();
	}
	public static function  detailsbyid($order_id)
    {
		return Paybylink::where('order_id', '=', $order_id)->orderBy('id','desc')->first();
	}
}