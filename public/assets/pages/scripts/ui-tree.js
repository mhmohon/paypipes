var UITree = function () {

    var handleSample1 = function () {

        $('#tree_1').jstree({
            "core" : {
                "themes" : {
                    "responsive": false
                }            
            },
            "types" : {
                "default" : {
                    "icon" : "fa fa-folder icon-state-warning icon-lg"
                },
                "file" : {
                    "icon" : "fa fa-file icon-state-warning icon-lg"
                }
            },
            "plugins": ["types"]
        });

        // handle link clicks in tree nodes(support target="_blank" as well)
        $('#tree_1').on('select_node.jstree', function(e,data) { 
            var link = $('#' + data.selected).find('a');
            if (link.attr("href") != "#" && link.attr("href") != "javascript:;" && link.attr("href") != "") {
                if (link.attr("target") == "_blank") {
                    link.attr("href").target = "_blank";
                }
                document.location.href = link.attr("href");
                return false;
            }
        });
    }

    var handleSample2 = function () {
        $('#tree_2').jstree({
            'plugins': ["wholerow", "checkbox", "types"],
            'core': {
                "themes" : {
                    "responsive": false
                },    
                'data': [{
						"text": "Permissions",
						"icon": "fa fa-check-square",
						"state": {
									"opened": true
								},
                        "children": [{								
							"text": "Dashboard",
							"icon": "fa fa-dashboard",
							"children": [{
								"text": "Edit",
								"icon": "fa fa-pencil",
							}, {
								"text": "Message",
								"icon": "fa fa-envelope-o"
							}]
						}, {
							"text": "Customers",
							"icon": "icon-users",
							"children": [{
								"text": "Edit",
								"icon": "fa fa-pencil",
							}, {
								"text": "Message",
								"icon": "fa fa-envelope-o"
							}]	
						}, {
							"text": "Merchants",
							"icon": "icon-screen-desktop",
							"children": [{
								"text": "Merchants",
								"icon": false,
								"children": [{
									"text": "Edit",
									"icon": "fa fa-pencil",
								}, {
									"text": "Message",
									"icon": "fa fa-envelope-o"
								}],
							}, {
								"text": "Terminals",
								"icon": false,
								"children": [{
									"text": "Edit",
									"icon": "fa fa-pencil",
								}, {
									"text": "Message",
									"icon": "fa fa-envelope-o"
								}],
							}]	
						}, {
							"text": "Payments",
							"icon": "fa fa-credit-card",
							"children": [{
								"text": "Payments Activity",
								"icon": false,
								"children": [{
									"text": "Edit",
									"icon": "fa fa-pencil",
								}, {
									"text": "Message",
									"icon": "fa fa-envelope-o"
								}],
							}, {
								"text": "Payment Methods",
								"icon": false,
								"children": [{
									"text": "Edit",
									"icon": "fa fa-pencil",
								}, {
									"text": "Message",
									"icon": "fa fa-envelope-o"
								}],
							}]
						}, {
							"text": "Statistics",
							"icon": "fa fa-line-chart",
							"children": [{
								"text": "Customers",
								"icon": false,
								"children": [{
									"text": "Edit",
									"icon": "fa fa-pencil",
								}, {
									"text": "Message",
									"icon": "fa fa-envelope-o"
								}],
							}, {
								"text": "Merchants",
								"icon": false,
								"children": [{
									"text": "Edit",
									"icon": "fa fa-pencil",
								}, {
									"text": "Message",
									"icon": "fa fa-envelope-o"
								}],
							}, {
								"text": "Payments",
								"icon": false,
								"children": [{
									"text": "Edit",
									"icon": "fa fa-pencil",
								}, {
									"text": "Message",
									"icon": "fa fa-envelope-o"
								}],
							}]
						}, {
							"text": "Risk Management",
							"icon": "icon-shield",
							"children": [{
								"text": "Customers Whitelist",
								"icon": false,
								"children": [{
									"text": "Edit",
									"icon": "fa fa-pencil",
								}, {
									"text": "Message",
									"icon": "fa fa-envelope-o"
								}],
							}, {
								"text": "Email Blacklist",
								"icon": false,
								"children": [{
									"text": "Edit",
									"icon": "fa fa-pencil",
								}, {
									"text": "Message",
									"icon": "fa fa-envelope-o"
								}],
							}, {
								"text": "Card Blacklist",
								"icon": false,
								"children": [{
									"text": "Edit",
									"icon": "fa fa-pencil",
								}, {
									"text": "Message",
									"icon": "fa fa-envelope-o"
								}],
							}, {
								"text": "BIN Blacklist",
								"icon": false,
								"children": [{
									"text": "Edit",
									"icon": "fa fa-pencil",
								}, {
									"text": "Message",
									"icon": "fa fa-envelope-o"
								}],
							}, {
								"text": "IP Blacklist",
								"icon": false,
								"children": [{
									"text": "Edit",
									"icon": "fa fa-pencil",
								}, {
									"text": "Message",
									"icon": "fa fa-envelope-o"
								}],
							}, {
								"text": "Country Blacklist",
								"icon": false,
								"children": [{
									"text": "Edit",
									"icon": "fa fa-pencil",
								}, {
									"text": "Message",
									"icon": "fa fa-envelope-o"
								}],
							}, {
								"text": "Velocity Limits",
								"icon": false,
								"children": [{
									"text": "Edit",
									"icon": "fa fa-pencil",
								}, {
									"text": "Message",
									"icon": "fa fa-envelope-o"
								}],
							}, {
								"text": "Scoring System",
								"icon": false,
								"children": [{
									"text": "Edit",
									"icon": "fa fa-pencil",
								}, {
									"text": "Message",
									"icon": "fa fa-envelope-o"
								}],
							}, {
								"text": "Patterns",
								"icon": false,
								"children": [{
									"text": "Edit",
									"icon": "fa fa-pencil",
								}, {
									"text": "Message",
									"icon": "fa fa-envelope-o"
								}],
							}]							
						}, {
							"text": "Compliance",
							"icon": "icon-paper-clip",
							"children": [{
								"text": "Customers",
								"icon": false,
								"children": [{
									"text": "Add",
									"icon": "fa fa-plus",
								}, {
									"text": "Edit",
									"icon": "fa fa-pencil",
								}, {
									"text": "Message",
									"icon": "fa fa-envelope-o"
								}],
							}, {
								"text": "Merchants",
								"icon": false,
								"children": [{
									"text": "Edit",
									"icon": "fa fa-pencil",
								}, {
									"text": "Message",
									"icon": "fa fa-envelope-o"
								}],
							}]
						}, {
							"text": "Accounts",
							"icon": "fa fa-file-text",
							"children": [{
								"text": "Edit",
								"icon": "fa fa-pencil",
							}, {
								"text": "Message",
								"icon": "fa fa-envelope-o"
							}]
						}, {
							"text": "Disputes",
							"icon": "fa fa-commenting",
							"children": [{
								"text": "Edit",
								"icon": "fa fa-pencil",
							}, {
								"text": "Message",
								"icon": "fa fa-envelope-o"
							}]
						}, {
							"text": "Inbox",
							"icon": "icon-envelope-letter",
							"children": [{
								"text": "Edit",
								"icon": "fa fa-pencil",
							}, {
								"text": "Message",
								"icon": "fa fa-envelope-o"
							}]
						}, {
							"text": "API",
							"icon": "fa fa-code",
							"children": [{
								"text": "API Management",
								"icon": false,
								"children": [{
									"text": "Edit",
									"icon": "fa fa-pencil",
								}, {
									"text": "Message",
									"icon": "fa fa-envelope-o"
								}],
							}, {
								"text": "API Merchant Menu",
								"icon": false,
								"children": [{
									"text": "Edit",
									"icon": "fa fa-pencil",
								}, {
									"text": "Message",
									"icon": "fa fa-envelope-o"
								}],
							}]
						}, {
							"text": "Gateways (3rd Parties)",
							"icon": "fa fa-plug",
							"children": [{
								"text": "Credit/Debit Cards",
								"icon": false,
								"children": [{
									"text": "Edit",
									"icon": "fa fa-pencil",
								}, {
									"text": "Message",
									"icon": "fa fa-envelope-o"
								}],
							}, {
								"text": "Alternative Payments",
								"icon": false,
								"children": [{
									"text": "Edit",
									"icon": "fa fa-pencil",
								}, {
									"text": "Message",
									"icon": "fa fa-envelope-o"
								}],
							}, {
								"text": "Fraud Prevention",
								"icon": false,
								"children": [{
									"text": "Edit",
									"icon": "fa fa-pencil",
								}, {
									"text": "Message",
									"icon": "fa fa-envelope-o"
								}],
							}, {
								"text": "3D Secure",
								"icon": false,
								"children": [{
									"text": "Edit",
									"icon": "fa fa-pencil",
								}, {
									"text": "Message",
									"icon": "fa fa-envelope-o"
								}],
							}, {
								"text": "Compliance",
								"icon": false,
								"children": [{
									"text": "Edit",
									"icon": "fa fa-pencil",
								}, {
									"text": "Message",
									"icon": "fa fa-envelope-o"
								}],
							}]
						}, {
							"text": "Settings",
							"icon": "fa fa-cogs",
							"children": [{
								"text": "General",
								"icon": false,
								"children": [{
									"text": "Edit",
									"icon": "fa fa-pencil",
								}, {
									"text": "Message",
									"icon": "fa fa-envelope-o"
								}],
							}, {
								"text": "User Management",
								"icon": false,
								"children": [{
									"text": "Users",
									"icon": false,
									"children": [{
										"text": "Edit",
										"icon": "fa fa-pencil",
									}, {
										"text": "Message",
										"icon": "fa fa-envelope-o"
									}],
								}, {
									"text": "User Roles",
									"icon": false,
									"children": [{
										"text": "Edit",
										"icon": "fa fa-pencil",
									}, {
										"text": "Message",
										"icon": "fa fa-envelope-o"
									}],
								}],
							}, {
								"text": "Appearance",
								"icon": false,
								"children": [{
									"text": "Customer",
									"icon": false,
									"children": [{
										"text": "Edit",
										"icon": "fa fa-pencil",
									}, {
										"text": "Message",
										"icon": "fa fa-envelope-o"
									}],
								}, {
									"text": "Merchant",
									"icon": false,
									"children": [{
										"text": "Edit",
										"icon": "fa fa-pencil",
									}, {
										"text": "Message",
										"icon": "fa fa-envelope-o"
									}],
								}, {
									"text": "Admin",
									"icon": false,
									"children": [{
										"text": "Edit",
										"icon": "fa fa-pencil",
									}, {
										"text": "Message",
										"icon": "fa fa-envelope-o"
									}],
								}],
							}]
						},
					],
					},
				],
            },
            "types" : {
                "default" : {
                    "icon" : "fa fa-folder icon-state-warning icon-lg"
                },
                "file" : {
                    "icon" : "fa fa-file icon-state-warning icon-lg"
                }
            }
        });
    }

    var contextualMenuSample = function() {

        $("#tree_3").jstree({
            "core" : {
                "themes" : {
                    "responsive": false
                }, 
                // so that create works
                "check_callback" : true,
                'data': [{
                        "text": "Parent Node",
                        "children": [{
                            "text": "Initially selected",
                            "state": {
                                "selected": true
                            }
                        }, {
                            "text": "Custom Icon",
                            "icon": "fa fa-warning icon-state-danger"
                        }, {
                            "text": "Initially open",
                            "icon" : "fa fa-folder icon-state-success",
                            "state": {
                                "opened": true
                            },
                            "children": [
                                {"text": "Another node", "icon" : "fa fa-file icon-state-warning"}
                            ]
                        }, {
                            "text": "Another Custom Icon",
                            "icon": "fa fa-warning icon-state-warning"
                        }, {
                            "text": "Disabled Node",
                            "icon": "fa fa-check icon-state-success",
                            "state": {
                                "disabled": true
                            }
                        }, {
                            "text": "Sub Nodes",
                            "icon": "fa fa-folder icon-state-danger",
                            "children": [
                                {"text": "Item 1", "icon" : "fa fa-file icon-state-warning"},
                                {"text": "Item 2", "icon" : "fa fa-file icon-state-success"},
                                {"text": "Item 3", "icon" : "fa fa-file icon-state-default"},
                                {"text": "Item 4", "icon" : "fa fa-file icon-state-danger"},
                                {"text": "Item 5", "icon" : "fa fa-file icon-state-info"}
                            ]
                        }]
                    },
                    "Another Node"
                ]
            },
            "types" : {
                "default" : {
                    "icon" : "fa fa-folder icon-state-warning icon-lg"
                },
                "file" : {
                    "icon" : "fa fa-file icon-state-warning icon-lg"
                }
            },
            "state" : { "key" : "demo2" },
            "plugins" : [ "contextmenu", "dnd", "state", "types" ]
        });
    
    }

     var ajaxTreeSample = function() {

        $("#tree_4").jstree({
            "core" : {
                "themes" : {
                    "responsive": false
                }, 
                // so that create works
                "check_callback" : true,
                'data' : {
                    'url' : function (node) {
                      return '../demo/jstree_ajax_data.php';
                    },
                    'data' : function (node) {
                      return { 'parent' : node.id };
                    }
                }
            },
            "types" : {
                "default" : {
                    "icon" : "fa fa-folder icon-state-warning icon-lg"
                },
                "file" : {
                    "icon" : "fa fa-file icon-state-warning icon-lg"
                }
            },
            "state" : { "key" : "demo3" },
            "plugins" : [ "dnd", "state", "types" ]
        });
    
    }


    return {
        //main function to initiate the module
        init: function () {

            handleSample1();
            handleSample2();
            contextualMenuSample();
            ajaxTreeSample();

        }

    };

}();

if (App.isAngularJsApp() === false) {
    jQuery(document).ready(function() {    
       UITree.init();
    });
}