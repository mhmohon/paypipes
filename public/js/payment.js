$(document).ready(function(){
		
 $('.payment_transaction_type').on('change',function(event) {
		var v = $(this).val();								 
		if ( v == 'TransferOut') {							
			$("#from_id").addClass("hidden");
			$("#to_id").removeClass("hidden");							
		} 
		else if ( v == 'TransferIn'){
			$("#from_id").removeClass("hidden");
			$("#to_id").addClass("hidden");
		}
	});	

	$('.merchant_terminal_list').on('change',function(event) {
						
					
		var merchant_id=$(this).val();
		var token = $("#token").val();
		$.ajax({
			method: "POST",
			url: SITE_URL+"/admin/payments/terminallist", 
			data: { "_token": token, "merchant_id": merchant_id },			
			cache: false,
			dataType: "json",
			success: function(data)
			{								
				$('#terminal_list').html('');
				$('#currency_list').html('');				
				$('#terminal_list').html(data['terminallist']);
				$('#currency_list').html(data['currencylist']);
			}
		});

	});	

	$('.terminal_currency_list').on('change',function(event) {
							 
		var terminal_id=$(this).val();
		var token = $("#token").val();
		
		$.ajax({
			method: "POST",
			url: SITE_URL+"/admin/payments/terminalcurrlist", 
			data: { "_token": token, "terminal_id": terminal_id },			
			cache: false,
			dataType: "json",
			success: function(data)
			{						
				$('#currency_list').html('');
				$('#currency_list').html(data);
			}
		});
		
		 e.preventDefault();		
	});		
})


