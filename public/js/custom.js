$(document).ready(function(){
		
    $('.amount').on('keyup',function(event) {
         this.value  = this.value.replace (/(\.\d\d)\d+|([\d.]*)[^\d.]/, '$1$2');
    });

    $('.amount').on('blur',function(event) {
        var amount = $(this).val();
        if( amount.indexOf(".")==-1){
          this.value +='.00';
        }

        if($(this).val().length <3){
        	this.value +='00';
        }

    });


    $(".textField").limitkeypress({ rexp: /^[a-zA-Z\s]+$/ });
    $(".address").limitkeypress({ rexp: /^[a-zA-Z0-9\s.,:-;]+$/ });
    $(".phone").limitkeypress({ rexp: /^(?=.*[0-9])[- +()0-9]+$/ });
    $(".card").limitkeypress({ rexp: /^[0-9]+$/ });

     

     			
})


